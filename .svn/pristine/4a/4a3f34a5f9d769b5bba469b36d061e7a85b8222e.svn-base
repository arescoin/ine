/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.constants;

import ru.effts.ine.core.*;
import ru.effts.ine.core.constants.ParametrizedConstantValue;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueAccessImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class ParametrizedConstantValueAccessImpl<T extends ParametrizedConstantValue>
        extends ConstantValueAccessImpl<T> implements ParametrizedConstantValueAccess<T> {

    /** Название таблицы со списком значений констант */
    public static final String TABLE = "CONSTANT_P_LIST";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_PARAM = "param";

    /** Запрос на выборку */
    public static final String ALL_CONST_VALS_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_CONST_N +
            ", " + TABLE_PREF + COLUMN_PARAM +
            ", " + TABLE_PREF + COLUMN_CONST_VAL +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.constants.ParametrizedConstantValue} */
    public static final String CACHE_REGION_PARAMS_CONSTANT_VALUSES = ParametrizedConstantValue.class.getName();

    private static final Map<String, BigDecimal> ID_COLUMNS_MAP = new HashMap<String, BigDecimal>(3);

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(ParametrizedConstantValue.PARAM, COLUMN_PARAM);
        setInstanceFieldsMapping(ParametrizedConstantValueAccessImpl.class, map);

        setIdColumns(ParametrizedConstantValueAccessImpl.class, new String[]{COLUMN_CONST_N, COLUMN_PARAM});

        registerConstantValueHelpers(ParametrizedConstantValueAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    protected void checkIdParam(BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(2, "Requiered: id[0] - value.coreId, id[1] - value.param ", id);
    }

    @Override
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(id);
        identifiable.setCoreId(id[0]);
        identifiable.setParam(id[1]);
    }

    @Override
    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {
        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(getColumnId(COLUMN_CONST_N), identifiable.getCoreId());
        syntheticId.addIdEntry(getColumnId(COLUMN_PARAM), identifiable.getParam());

        return syntheticId;
    }

    private static BigDecimal getColumnId(String columnName) throws GenericSystemException {
        if (!ID_COLUMNS_MAP.containsKey(columnName)) {
            ID_COLUMNS_MAP.put(columnName, FieldAccess.getColumnId(TABLE, columnName));
        }

        return ID_COLUMNS_MAP.get(columnName);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"})
        T result = (T) IdentifiableFactory.getImplementation(ParametrizedConstantValue.class);

        this.setVersionableFields(resultSet, result, COLUMN_CONST_N);

        result.setParam(this.getId(resultSet, COLUMN_PARAM));
        result.setValue(this.getString(resultSet, COLUMN_CONST_VAL));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_PARAMS_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_PARAMS_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_PARAMS_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_PARAMS_CONSTANT_VALUSES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_PARAMS_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT);
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_PARAMS_CONSTANT_VALUSES;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ParametrizedConstantValue.class;
    }

    @Override
    public String getTableName() {
        return TABLE;
    }
}
