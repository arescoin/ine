package ru.effts.ine.ejbs.billing.services;

import org.junit.Assert;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceDelegateTest.java 3867 2014-10-16 07:34:01Z DGomon $"
 */
public class ServicePriceDelegateTest extends BaseDelegateTest {

    @Override
    protected void testDelegate() {
        try {

            final ServicePrice object = IdentifiableFactory.getImplementation(ServicePrice.class);

            object.setPriceListID(BigDecimal.ONE);
            object.setServiceBundleItemID(new BigDecimal(2));
            object.setPrice(new BigDecimal(30));

            ServicePriceDelegate<ServicePrice> bean = DelegateFactory.obtainDelegateByInterface(ServicePrice.class);
            final ServicePrice object2 = bean.createObject(user, object);

            Assert.assertEquals(object.getPrice(), object2.getPrice());
            Assert.assertEquals(object.getPriceListID(), object2.getPriceListID());
            Assert.assertEquals(object.getServiceBundleItemID(), object2.getServiceBundleItemID());

            final ServicePrice object3 = bean.getObjectById(user, object2.getCoreId());

            Assert.assertEquals(object2.getPrice(), object3.getPrice());
            Assert.assertEquals(object2.getPriceListID(), object3.getPriceListID());
            Assert.assertEquals(object2.getServiceBundleItemID(), object3.getServiceBundleItemID());

            object3.setPriceListID(new BigDecimal(3));
            object3.setServiceBundleItemID(new BigDecimal(4));
            object3.setPrice(new BigDecimal(18));

            final ServicePrice object4 = bean.updateObject(user, object3);

            Assert.assertEquals(object3.getPrice(), object4.getPrice());
            Assert.assertEquals(object3.getPriceListID(), object4.getPriceListID());
            Assert.assertEquals(object3.getServiceBundleItemID(), object4.getServiceBundleItemID());
            //удаление
            bean.deleteObject(user, object4);

            if (bean.getObjectById(user, object4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }

    }
}
