package ru.effts.ine.billing.services;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class ServicePriceImpl extends AbstractVersionable implements ServicePrice {

    private BigDecimal priceListID;
    private BigDecimal serviceBundleItemID;
    private BigDecimal price;
    private BigDecimal dayPrice;


    @Override
    public BigDecimal getPriceListID() {
        return this.priceListID;
    }

    @Override
    public void setPriceListID(BigDecimal priceListID) throws IllegalIdException {

        if (null == priceListID) {
            throw new IllegalIdException("PriceList ID can't be null");
        }

        this.priceListID = priceListID;
    }

    @Override
    public BigDecimal getServiceBundleItemID() {
        return this.serviceBundleItemID;
    }

    @Override
    public void setServiceBundleItemID(BigDecimal serviceBundleItemID) throws IllegalIdException {

        if (null == serviceBundleItemID) {
            throw new IllegalIdException("ServiceBundle Item ID can't be null");
        }

        this.serviceBundleItemID = serviceBundleItemID;
    }

    @Override
    public BigDecimal getPrice() {
        return this.price;
    }

    @Override
    public void setPrice(BigDecimal price) throws IneIllegalArgumentException {

        if (null == price) {
            throw new IllegalIdException("The Price can't be null");
        }

        BigDecimal dayPrice = price.divide(new BigDecimal(30), 4, BigDecimal.ROUND_DOWN);
        BigDecimal decimal = dayPrice.stripTrailingZeros();
        decimal = decimal.subtract(decimal.setScale(0, RoundingMode.DOWN));
        String s = decimal.toPlainString();
        if (s.indexOf('.') > -1) {
            if (decimal.toPlainString().substring(2).length() > 2) {
                throw new IneIllegalArgumentException("The Price value must be divided on 30 with two decimal digits");
            }
        }

        this.price = price;
        this.dayPrice = dayPrice.stripTrailingZeros();

    }

    @Override
    public BigDecimal getDayPrice() {
        return this.dayPrice;
    }

    @Override
    public void setDayPrice(BigDecimal dayPrice) throws IneIllegalArgumentException {
        if (null == dayPrice) {
            throw new IllegalIdException("The Price can't be null");
        }

        dayPrice = dayPrice.stripTrailingZeros();
        if (!dayPrice.equals(this.dayPrice)) {
            throw new IneIllegalArgumentException("DayPrice has incorrect value fo Price");
        }
        this.dayPrice = dayPrice;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(PRICE_LIST_ID, this.getPriceListID())
                + fieldToString(SERVICE_BUNDLE_ITEM_ID, this.getServiceBundleItemID())
                + fieldToString(PRICE, this.getPrice())
                + fieldToString(DAY_PRICE, this.getDayPrice());
    }

}