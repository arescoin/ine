package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemBean.java 3872 2014-10-22 12:46:44Z DGomon $"
 */
public interface BalanceItemBean<T extends BalanceItem> extends VersionableBean<T> {

    Collection<T> getOldVersionsByAccId(UserProfile user, BigDecimal accId, Date fd, Date td)
            throws GenericSystemException;

    BalanceItem getBalanceItemForAccount(UserProfile userProfile, BigDecimal accountId) throws GenericSystemException;
}
