package ru.effts.ine.ejbs.billing.business.rest.core;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;
import ru.effts.ine.ejbs.core.dic.DictionaryBean;
import ru.effts.ine.ejbs.core.dic.DictionaryEntryBean;
import ru.effts.ine.ejbs.core.dic.DictionaryTermBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: DictionaryService.java 3893 2014-12-09 13:06:52Z DGomon $"
 */
@Path("/dictionary")
@Stateless
public class DictionaryService {

    @EJB(mappedName = "stateless.DictionaryBean", name = "DictionaryBean")
    DictionaryBean<Dictionary> dictionaryBean;

    @EJB(mappedName = "stateless.DictionaryTermBean", name = "DictionaryTermBean")
    DictionaryTermBean<DictionaryTerm> dictionaryTermBean;

    @EJB(mappedName = "stateless.DictionaryEntryBean", name = "DictionaryEntryBean", beanName = "DictionaryEntryBean")
    DictionaryEntryBean<DictionaryEntry> dictionaryEntryBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    dictionaryBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    @Path("/all")
    public Response getAll() {

        try {
            Collection<Dictionary> dictionaries = dictionaryBean.getAllObjects(UserHolder.getUser());
            Map<BigDecimal, Dictionary> dictionaryMap = new HashMap<>();

            for (Dictionary dictionary : dictionaries) {
                dictionaryMap.put(dictionary.getCoreId(), dictionary);
            }

            return SrvUtils.createMapResponse(dictionaryMap);
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    @Path("/getAllTerms")
    public Response getAllTerms(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createCollectionResponse(dictionaryTermBean.getAllEntriesForDictionary(
                    UserHolder.getUser(), new BigDecimal(id))
            );
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    @Path("/addTerms")
    public Response addTerms(@FormParam("terms") Set<String> details) {

        try {
            Collection<DictionaryTerm> dictionaryTerms = new HashSet<>();
            ObjectMapper objectMapper = new ObjectMapper();

            for (String detail : details) {
                ObjectReader objectReader = objectMapper.reader(
                        IdentifiableFactory.getImplementation(DictionaryTerm.class).getClass());
                DictionaryTerm dictionaryTerm = objectReader.readValue(detail);

                dictionaryTerms.add(dictionaryTerm);
            }
            dictionaryTermBean.addDictionaryTerms(UserHolder.getUser(), dictionaryTerms);

            return Response.status(Response.Status.OK).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    @Path("/createTerms")
    public Response createTerms(@FormParam("terms") Set<String> details) {

        try {
            Collection<DictionaryTerm> dictionaryTerms = new HashSet<>();
            ObjectMapper objectMapper = new ObjectMapper();

            for (String detail : details) {
                UserHolder.getUser();
                ObjectReader objectReader = objectMapper.reader(
                        IdentifiableFactory.getImplementation(DictionaryTerm.class).getClass());
                DictionaryTerm dictionaryTerm = objectReader.readValue(detail);

                dictionaryTerms.add(dictionaryTerm);
            }
            dictionaryTermBean.createDictionaryTerms(UserHolder.getUser(), dictionaryTerms);

            return Response.status(Response.Status.OK).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    @Path("/deleteTerms")
    public Response deleteTerms(@FormParam("terms") Set<String> details) {

        try {
            ObjectReader objectReader = new ObjectMapper().reader(
                    IdentifiableFactory.getImplementation(DictionaryTerm.class).getClass());

            for (String detail : details) {
                DictionaryTerm dictionaryTerm = objectReader.readValue(detail);
                dictionaryTermBean.deleteObject(UserHolder.getUser(), dictionaryTerm);
            }

            return Response.status(Response.Status.OK).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    @Path("/deleteEntry")
    public Response deleteEntry(@FormParam("id") Set<Long> ids) {

        try {
            for (Long id : ids) {
                BigDecimal entryId = BigDecimal.valueOf(id);
                dictionaryEntryBean.deleteObject(
                        UserHolder.getUser(),
                        dictionaryEntryBean.getObjectById(UserHolder.getUser(), entryId)
                );
            }

            return Response.status(Response.Status.OK).build();
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
