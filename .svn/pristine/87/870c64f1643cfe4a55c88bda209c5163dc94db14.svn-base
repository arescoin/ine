/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.funcswitch;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.utils.BaseIdentifiableTest;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
@SuppressWarnings({"unchecked"})
public class FuncSwitchNameAccessTest extends BaseIdentifiableTest {

    private static FuncSwitchNameAccess<FuncSwitchName> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (FuncSwitchNameAccess<FuncSwitchName>) AccessFactory.getImplementation(FuncSwitchName.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            FuncSwitchName obj1 = IdentifiableFactory.getImplementation(FuncSwitchName.class);
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setName("TEST_SWITCH_NAME");

            obj1 = access.createObject(obj1);
            FuncSwitchName obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getName(), obj2.getName());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            FuncSwitchName obj1 = (FuncSwitchName) identifiable;
            FuncSwitchName obj2 = IdentifiableFactory.getImplementation(FuncSwitchName.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setName(obj1.getName() + "_UPDATED");

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.getName(), obj2.getName());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((FuncSwitchName) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }
}
