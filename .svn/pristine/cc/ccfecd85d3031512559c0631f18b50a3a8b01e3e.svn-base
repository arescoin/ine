/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class UserPermitTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return UserPermit.class;
    }

    @Test(expected = IllegalIdException.class)
    public void testNullUserId() {
        try {
            IdentifiableFactory.getImplementation(UserPermit.class).setUserId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroUserId() {
        try {
            IdentifiableFactory.getImplementation(UserPermit.class).setUserId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeUserId() {
        try {
            IdentifiableFactory.getImplementation(UserPermit.class).setUserId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullRoleId() {
        try {
            IdentifiableFactory.getImplementation(UserPermit.class).setRoleId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroRoleId() {
        try {
            IdentifiableFactory.getImplementation(UserPermit.class).setRoleId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeRoleId() {
        try {
            IdentifiableFactory.getImplementation(UserPermit.class).setRoleId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        UserPermit value1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        UserPermit value2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() == value2.hashCode());
        Assert.assertTrue("Equals operation failed.", value1.equals(value2));

        UserPermit value3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        value3.setCoreId(value1.getCoreId().add(value2.getCoreId()));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() != value3.hashCode());
        Assert.assertTrue("Equals operation failed.", !value1.equals(value3));
    }

    protected UserPermit fillBasicFields(Identifiable identifiable) {
        UserPermit value = (UserPermit) super.fillBasicFields(identifiable);
        value.setUserId(Identifiable.MIN_ALLOWABLE_VAL);
        value.setRoleId(Identifiable.MIN_ALLOWABLE_VAL);
        return value;
    }

    @Test
    public void testSetFields() {
        BigDecimal userId = Identifiable.MIN_ALLOWABLE_VAL;
        BigDecimal roleId = Identifiable.MIN_ALLOWABLE_VAL;
        BigDecimal[] values = new BigDecimal[]{new BigDecimal(1)};

        boolean actrive = true;
        try {
            UserPermit userPermit = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            userPermit.setUserId(userId);
            Assert.assertSame("Set operation failed.", userId, userPermit.getUserId());

            userPermit.setRoleId(roleId);
            Assert.assertSame("Set operation failed.", roleId, userPermit.getRoleId());

            userPermit.setValues(values);
            Assert.assertSame("Set operation failed.", values, userPermit.getValues());

            userPermit.setActive(actrive);
            Assert.assertSame("Set operation failed.", actrive, userPermit.isActive());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
