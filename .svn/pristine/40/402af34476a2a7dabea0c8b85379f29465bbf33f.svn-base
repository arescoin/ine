/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import ru.xr.ine.core.GenericSystemException;

import java.io.*;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Контейнер поискового критерия
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class SearchCriterion<T> implements Externalizable {

    private static final long serialVersionUID = 1526472295622776142L;

    private static final transient HashMap<String, Extractor> GETTER_CLASS_MAP = new HashMap<>();

    private String extractorName;

    private transient Extractor extractor;

    /** Имя сравниваемого атрибута */
    private String fieldName;
    /** Метод доступа к значению атрибута */
    private transient Method method;
    /** Наименование метода доступа */
    private String methodName;

    /** Поисковой объект */
    private Class<?> searchClass;

    private ArrayList<Serializable> additionalParams = new ArrayList<>();

    /** Признак инвертации результата */
    private boolean invert;
    /** Правило сравнения */
    private CriteriaRule criterionRule;

    /** Набор патернов сравнения, используется для набивки, после, перегоняем в массив */
    private ArrayList<T> criterionVals = new ArrayList<>();

    /** Окончательный набор патернов сравнения */
    private T[] criterionArray = null;

    /** Общее перечисление правил для типа сравнения */
    private AllowableRules allowableRules;

    private boolean useRuleMatcher = true;

    /**
     * Конструктор недопустимо использовать в разработке!!!
     * Требуется исключительно для сериализации
     */
    @Deprecated
    public SearchCriterion() {
//        System.out.println("Construct class...");
    }

    /**
     * Конструирует новый критерий
     *
     * @param fieldName   Имя сравниваемого атрибута
     * @param searchClass Поисковой объект
     * @param method      Наименование метода доступа
     */
    SearchCriterion(String fieldName, Class searchClass, Method method) {
        this.fieldName = fieldName;
        this.method = method;
        this.methodName = method.getName();
        this.searchClass = searchClass;
        this.extractorName = this.searchClass.getName() + "_" + methodName + "_Extractor";
    }

    /** @return Имя сравниваемого атрибута */
    @SuppressWarnings({"UnusedDeclaration"})
    public String getFieldName() {
        return fieldName;
    }

    /** @return Правило сравнения */
    public CriteriaRule getCriterionRule() {
        return criterionRule;
    }

    /** @param criterionRule Правило сравнения */
    public void setCriterionRule(CriteriaRule criterionRule) {
        this.criterionRule = criterionRule;
    }

    /** @return Набор патернов сравнения */
    @SuppressWarnings({"unchecked"})
    public T[] getCriterionVals() {

        if (criterionArray == null) {
            criterionArray = (T[]) criterionVals.toArray();
        }

        return criterionArray;
    }

    /** @param criteriaVal Набор патернов сравнения */
    public void addCriteriaVal(T criteriaVal) {
        this.criterionVals.add(criteriaVal);
    }

    /** @return Общее перечисление правил для типа сравнения */
    public AllowableRules getCriteriaRuleSet() {
        return allowableRules;
    }

    /** @param allowableRules Общее перечисление правил для типа сравнения */
    public void setCriteriaRuleSet(AllowableRules allowableRules) {
        this.allowableRules = allowableRules;
    }

    /** @return Признак инвертации результата */
    public boolean isInvert() {
        return invert;
    }

    /** @param invert Признак инвертации результата */
    public void setInvert(boolean invert) {
        this.invert = invert;
    }

    /**
     * Возвращаает признак возможности (необходимости) использования предустановленного матчера.
     * <p/>
     * По-умолчанию - юзаем (<code>true</code>)
     *
     * @return если false то лучше не юзать его
     */
    public boolean isUseRuleMatcher() {
        return this.useRuleMatcher;
    }

    /**
     * Сетит признак возможности (необходимости) использования предустановленного матчера.
     * <p/>
     * По-умолчанию - юзаем (<code>true</code>)
     *
     * @param useRuleMatcher если false то лучше не юзать его
     */
    @SuppressWarnings("UnusedDeclaration")
    public void setUseRuleMatcher(boolean useRuleMatcher) {
        this.useRuleMatcher = useRuleMatcher;
    }

    /** @return Метод доступа к значению атрибута */
    @SuppressWarnings({"unchecked"})
    public Method getMethod() {

        if (method == null) {
            try {
                this.method = searchClass.getMethod(methodName);
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e);
            }
        }

        return method;
    }

    /** @return Класс поискового объекта */
    public Class getSearchClass() {
        return searchClass;
    }

    /**
     * Добавляет расширяющий атрибут поиска, который используется клиентской
     * частью при формировании или выполнении запроса
     *
     * @param param расширяющий атрибут
     */
    public void addAdditionalParam(Serializable param) {
        additionalParams.add(param);
    }

    /**
     * Возвращает расширяющий атрибут поиска, который используется клиентской
     * частью при формировании или выполнении запроса
     *
     * @return расширяющий атрибут
     */
    public List<Serializable> getAdditionalParams() {
        return Collections.unmodifiableList(additionalParams);
    }


    public Object extractValue(Object object) throws Exception {

        if (this.extractor != null) {// проверка != null проходит быстрее, чем == null
            return this.extractValueNoCheck(object);
        }

        this.makeExtractor();

        return this.extractValueNoCheck(object);
    }

    private Object extractValueNoCheck(Object object) throws Exception {
        return this.extractor.extractValue(object);
    }

    /**
     * Генерирует класс специфического экстрактора
     *
     * @throws ru.xr.ine.core.GenericSystemException при невозможности сгенерировать класс или загрузить его в jvm
     */
    private void makeExtractor() throws GenericSystemException {
        try {
            if (!GETTER_CLASS_MAP.containsKey(this.extractorName)) {
                synchronized (GETTER_CLASS_MAP) {
                    if (!GETTER_CLASS_MAP.containsKey(this.extractorName)) {
                        GETTER_CLASS_MAP.put(this.extractorName, this.constructClass().newInstance());
                    }
                }
            }
            this.extractor = (Extractor) GETTER_CLASS_MAP.get(this.extractorName).clone();
        } catch (Exception e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    @SuppressWarnings({"unchecked"})
    private Class<Extractor> constructClass() throws GenericSystemException {

        ClassPool classPool = ClassPool.getDefault();
        try {
            CtClass ctClass = classPool.makeClass(this.extractorName,
                    classPool.get("ru.xr.ine.utils.searchCriteria.Extractor"));

            String objectMethodCall = "((" + searchClass.getName() + ") o)." + methodName + "()";

            String methodBody;
            Class<?> returnType = this.getMethod().getReturnType();
            if (returnType.isPrimitive()) {
                if (returnType.getCanonicalName().equals("boolean")) {
                    methodBody = objectMethodCall + " ? Boolean.TRUE : Boolean.FALSE";
                } else if (returnType.getCanonicalName().equals("char")) {
                    methodBody = "new String(" + objectMethodCall + ")";
                } else if (returnType.getCanonicalName().equals("byte")) {
                    methodBody = "new Byte(" + objectMethodCall + ")";
                } else if (returnType.getCanonicalName().equals("int")) {
                    methodBody = "new Integer(" + objectMethodCall + ")";
                } else if (returnType.getCanonicalName().equals("long")) {
                    methodBody = "new Long(" + objectMethodCall + ")";
                } else if (returnType.getCanonicalName().equals("short")) {
                    methodBody = "new Short(" + objectMethodCall + ")";
                } else if (returnType.getCanonicalName().equals("double")) {
                    methodBody = "new Double(" + objectMethodCall + ")";
                } else if (returnType.getCanonicalName().equals("float")) {
                    methodBody = "new Float(" + objectMethodCall + ")";
                } else {
                    throw new GenericSystemException("Unsupported primitive type: " + returnType.getCanonicalName());
                }
            } else {
                methodBody = objectMethodCall;
            }

            CtMethod ctMethod = CtMethod.make(
                    "public java.lang.Object extractValue(Object o) { return " + methodBody + "; }", ctClass);

            ctClass.addMethod(ctMethod);

            return ctClass.toClass();

        } catch (Exception e) {
            throw GenericSystemException.toGeneric("Can't construct Extractor: [" + extractorName
                    + "]; [" + methodName + "]", e);
        }

    }

    // для для протягивания через сеть метода

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        Object[] fields = new Object[10];

        fields[0] = this.fieldName;
        fields[1] = this.methodName;
        fields[2] = this.invert;
        fields[3] = this.criterionRule;
        fields[4] = this.criterionVals;
        fields[5] = this.allowableRules;
        fields[6] = this.searchClass;
        fields[7] = this.additionalParams;
        fields[8] = this.extractorName;
        fields[9] = this.useRuleMatcher;

        out.writeObject(fields);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        Object[] fields = (Object[]) in.readObject();

        this.fieldName = (String) fields[0];
        this.methodName = (String) fields[1];

        this.invert = (Boolean) fields[2];
        this.criterionRule = (CriteriaRule) fields[3];
        this.criterionVals = (ArrayList<T>) fields[4];
        this.allowableRules = (AllowableRules) fields[5];
        this.searchClass = (Class<Object>) fields[6];
        this.additionalParams = (ArrayList<Serializable>) fields[7];
        this.extractorName = (String) fields[8];
        this.useRuleMatcher = (Boolean) fields[9];

//        try {
//            this.method = searchClass.getMethod(methodName);
//        } catch (NoSuchMethodException e) {
//            throw new IllegalStateException(e);
//        }
    }

    @Override
    public String toString() {
        return this.fieldName + "; " + this.methodName + "; " + this.useRuleMatcher + " <|"
                + this.criterionVals.toString() + "|>; <|" + this.additionalParams + "|>";
    }
}
