/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class FuncSwitchNameBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testFuncSwitchBeanLocal() {
        try {
            FuncSwitchNameBean<FuncSwitchName> bean = new FuncSwitchNameBeanImpl<FuncSwitchName>();
            //создаем новый объект
            FuncSwitchName object = IdentifiableFactory.getImplementation(FuncSwitchName.class);
            object.setCoreDsc("Description");
            object.setName("TestFuncSwitchName");
            BigDecimal createdId = bean.createObject(user, object).getCoreId();
            //получаем созданный объект и провреям данные
            object = bean.getObjectById(user, createdId);
            Assert.assertEquals("TestFuncSwitchName", object.getName());

            //обновление
            FuncSwitchName cUpdated = bean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            bean.updateObject(user, cUpdated);
            //получаем обновленный объект и провреям данные
            object = bean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", object.getName());

            //удаление
            bean.deleteObject(user, object);
            if (bean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove FuncSwitchName");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
