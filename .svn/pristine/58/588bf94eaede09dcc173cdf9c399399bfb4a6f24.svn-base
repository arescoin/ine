/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.funcswitch;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.funcswitch.FuncSwitch;
import ru.xr.ine.core.history.VersionableHistory;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.history.VersionableHistoryAccess;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class FuncSwitchAccessTest extends BaseVersionableTest {

    private static FuncSwitchAccess<FuncSwitch> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (FuncSwitchAccess<FuncSwitch>) AccessFactory.getImplementation(FuncSwitch.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public Identifiable testCreate() {
        try {
            FuncSwitch obj1 = IdentifiableFactory.getImplementation(FuncSwitch.class);
            obj1.setCoreId(BigDecimal.ONE); // TEST_SWITCH
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setMark(BigDecimal.ONE);
            obj1.setStateCode("Test stateCode");
            obj1.setChangeDate(FieldAccess.getCurrentDate());
            obj1.setEnabled(false);

            obj1 = access.createObject(obj1);
            FuncSwitch obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", null, obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getMark(), obj2.getMark());
            Assert.assertEquals("Failed to save new object in DB", obj1.getStateCode(), obj2.getStateCode());
            Assert.assertEquals("Failed to save new object in DB", obj1.getChangeDate(), obj2.getChangeDate());
            Assert.assertEquals("Failed to save new object in DB", obj1.isEnabled(), obj2.isEnabled());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            FuncSwitch obj1 = (FuncSwitch) identifiable;
            FuncSwitch obj2 = IdentifiableFactory.getImplementation(FuncSwitch.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setMark(obj1.getMark());
            obj2.setStateCode(obj1.getStateCode());
            obj2.setChangeDate(obj2.getCoreFd());
            obj2.setEnabled(!obj1.isEnabled());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.isEnabled(), obj2.isEnabled());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((FuncSwitch) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test
    public void testHistorySwitches() {
        boolean histrySwitchEnable = true;
        boolean actionsSwitchEnable = true;
        try {
            FuncSwitch historySwitch = FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_HISTORY);
            histrySwitchEnable = historySwitch.isEnabled();
            FuncSwitch actionsSwitch = FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS);
            actionsSwitchEnable = actionsSwitch.isEnabled();

            @SuppressWarnings({"unchecked"}) VersionableHistoryAccess<VersionableHistory> historyAccess =
                    (VersionableHistoryAccess) AccessFactory.getImplementation(VersionableHistory.class);

            BigDecimal switchTableId = FieldAccess.getTableId(FuncSwitch.class);

            FuncSwitch testSwitch = (FuncSwitch) testCreate();
            Collection<VersionableHistory> actions1;
            Collection<VersionableHistory> actions2;
            Collection<FuncSwitch> history1;
            Collection<FuncSwitch> history2;

            // Проверяем, что изменённые объекты пишутся в историю и регистрируются действия
            historySwitch.setEnabled(true);
            FuncSwitchUtils.saveSearchedSwitch(FuncSwitchUtils.SAVE_HISTORY, historySwitch);
            actionsSwitch.setEnabled(true);
            FuncSwitchUtils.saveSearchedSwitch(FuncSwitchUtils.REGISTER_ACTIONS, actionsSwitch);

            actions1 = historyAccess.getActionsHistoryByIds(switchTableId, access.getSyntheticId(testSwitch));
            history1 = access.getOldVersions(testSwitch);

            testSwitch = (FuncSwitch) testUpdate(testSwitch);

            actions2 = historyAccess.getActionsHistoryByIds(switchTableId, access.getSyntheticId(testSwitch));
            history2 = access.getOldVersions(testSwitch);

            Assert.assertTrue(actions2.size() - actions1.size() == 1);
            Assert.assertTrue(history2.size() - history1.size() == 1);

            // Выключим запись истории
            historySwitch.setEnabled(false);
            FuncSwitchUtils.saveSearchedSwitch(FuncSwitchUtils.SAVE_HISTORY, historySwitch);

            history1 = access.getOldVersions(testSwitch);
            testSwitch = (FuncSwitch) testUpdate(testSwitch);
            history2 = access.getOldVersions(testSwitch);

            Assert.assertTrue(history2.size() - history1.size() == 0);

            // Выключим регистрацию действий
            actionsSwitch.setEnabled(false);
            FuncSwitchUtils.saveSearchedSwitch(FuncSwitchUtils.REGISTER_ACTIONS, actionsSwitch);

            actions1 = historyAccess.getActionsHistoryByIds(switchTableId, access.getSyntheticId(testSwitch));
            testSwitch = (FuncSwitch) testUpdate(testSwitch);
            actions2 = historyAccess.getActionsHistoryByIds(switchTableId, access.getSyntheticId(testSwitch));

            Assert.assertTrue(actions2.size() - actions1.size() == 0);

            testDelete(testSwitch);
        } catch (Exception e) {
            fail(e);
        } finally {
            setDefaultSwitchValues(histrySwitchEnable, actionsSwitchEnable);
        }
    }

    private void setDefaultSwitchValues(boolean historySwitchEnable, boolean actionsSwitchEnable) {
        try {
            FuncSwitch historySwitch = FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_HISTORY);
            historySwitch.setEnabled(historySwitchEnable);
            FuncSwitchUtils.saveSearchedSwitch(FuncSwitchUtils.SAVE_HISTORY, historySwitch);

            FuncSwitch actionsSwitch = FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS);
            actionsSwitch.setEnabled(actionsSwitchEnable);
            FuncSwitchUtils.saveSearchedSwitch(FuncSwitchUtils.REGISTER_ACTIONS, actionsSwitch);
        } catch (Exception e) {
            fail(e);
        }
    }
}
