/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core;

import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.cache.Producer;
import ru.effts.ine.utils.searchCriteria.AllowableRules;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * Утилитный класс, обеспечивает разгрузку основного {@link ru.effts.ine.dbe.da.core.AbstractIdentifiableAccess}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableGetAccess.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
class IdentifiableGetAccess<T extends Identifiable> {

    private AbstractIdentifiableAccess<T> access;

    IdentifiableGetAccess(AbstractIdentifiableAccess<T> identifiableAccess) {
        this.access = identifiableAccess;
    }

    @SuppressWarnings({"unchecked"})
    public T getObjectById(String region, Producer producer, SyntheticId id) throws CoreException {
        return (T) Cache.get(region, id, new DefaultSystemProducer(region, producer));
    }

    public SearchCriteriaProfile getSearchCriteriaProfile() throws GenericSystemException {

        SearchCriteriaProfile result;
        @SuppressWarnings({"unchecked"}) Class<T> subjectClass = this.access.getSubjectClass();

        Class impl = IdentifiableFactory.getImplementation(subjectClass).getClass();
        Map<String, Method> methodMap = this.access.obtainFieldMethodMapping(impl);
        result = new SearchCriteriaProfile(impl);

        for (String fieldName : methodMap.keySet()) {
            Method method = methodMap.get(fieldName);
            Class aClass = method.getReturnType();
            DataType dataType = DataType.getTypeByClass(aClass);
            if (dataType != null) {
                result.addRule(fieldName, AllowableRules.getCriteriaRules(dataType), method);
            } else {
                result.addMethod(fieldName, method);
            }
        }

        return result;
    }

    class DefaultSystemProducer implements Producer {

        private final String region;
        private final Producer producer;

        public DefaultSystemProducer(String region, Producer producer) {
            this.region = region;
            this.producer = producer;
        }

        @Override
        public Object get(Object key) throws CoreException {

            Object result = null;
            if (key != null && Cache.getKeys(this.region).isEmpty()) {
                if (Cache.getRegionContent(this.region, this.producer).keySet().contains(key)) {
                    result = Cache.get(this.region, key, NULL_PRODUCER);
                }
            }

            return result;
        }
    }

}
