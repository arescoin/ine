package ru.effts.ine.dbe.da.billing.balance;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillAccessTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
public class LastAccountBillAccessTest extends BaseVersionableTest {

    private static LastAccountBillAccess<LastAccountBill> access;

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (LastAccountBillAccess<LastAccountBill>)
                    AccessFactory.getImplementation(LastAccountBill.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            LastAccountBill obj1 = IdentifiableFactory.getImplementation(LastAccountBill.class);
            obj1.setCoreId(BigDecimal.ONE);
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, -1);
            obj1.setBillDate(calendar.getTime());
            obj1.setCoreDsc("TestObject (LastAccountBill) for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            LastAccountBill obj2 = access.createObject(obj1);

            Assert.assertEquals("Failed to save new object in DB", obj1.getBillDate(), obj2.getBillDate());

            return obj2;

        } catch (CoreException e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            LastAccountBill obj1 = (LastAccountBill) identifiable;
            LastAccountBill obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertNotNull("Failed to save new object in DB", obj2);

            return obj2;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {

        try {
            LastAccountBill obj1 = (LastAccountBill) identifiable;
            LastAccountBill obj2 = access.getObjectById(obj1.getCoreId());

            obj2.setBillDate(new Date());
            obj2.setCoreDsc(obj1.getCoreDsc() + " update test");
            obj2.setCoreFd(obj1.getCoreFd());
            obj2.setCoreTd(obj1.getCoreTd());

            LastAccountBill obj3 = access.updateObject(obj1, obj2);

            Assert.assertEquals("Failed to save new object in DB", obj2.getBillDate(), obj3.getBillDate());
            Assert.assertEquals("Failed to save new object in DB", obj2.getCoreDsc(), obj3.getCoreDsc());

            return obj2;

        } catch (CoreException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((LastAccountBill) identifiable);
            LastAccountBill obj2 = access.getObjectById(identifiable.getCoreId());
            Assert.assertNull(obj2);
        } catch (Exception e) {
            fail(e);
        }
    }
}
