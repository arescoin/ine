package ru.effts.ine.billing.services;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Описывает элемент цены услуги в тарифе
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePrice.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public interface ServicePrice extends Versionable {

    String PRICE_LIST_ID = "priceListID";
    String SERVICE_BUNDLE_ITEM_ID = "serviceBundleItemID";
    String PRICE = "price";
    String DAY_PRICE = "dayPrice";


    BigDecimal getPriceListID();

    void setPriceListID(BigDecimal priceListID) throws IllegalIdException;

    BigDecimal getServiceBundleItemID();

    void setServiceBundleItemID(BigDecimal serviceBundleItemID) throws IllegalIdException;

    BigDecimal getPrice();

    void setPrice(BigDecimal price) throws IneIllegalArgumentException;

    BigDecimal getDayPrice();

    void setDayPrice(BigDecimal price) throws IneIllegalArgumentException;
}
