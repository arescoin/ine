package ru.effts.ine.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueTest.java 227 2010-05-17 11:10:05Z ikulkov $"
 */
public class ConstantValueTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ConstantValue.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullValue() {
        try {
            ConstantValue constantValue = (ConstantValue) IdentifiableFactory.getImplementation(idClass);
            constantValue.setValue(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testZeroValue() {
        try {
            ConstantValue constantValue = (ConstantValue) IdentifiableFactory.getImplementation(idClass);
            constantValue.setValue(" ");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        try {
            String value = "Test Value";
            String valueWS = "  Test Value  ";

            ConstantValue constantValue = (ConstantValue) IdentifiableFactory.getImplementation(idClass);

            constantValue.setValue(value);
            Assert.assertSame("Set operation failed.", value, constantValue.getValue());

            constantValue.setValue(valueWS);
            Assert.assertEquals("Set operation failed.", value, constantValue.getValue());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
