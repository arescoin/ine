package ru.effts.ine.billing.accounting;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class AccountImpl extends AbstractVersionable implements Account {

    private BigDecimal priceListId;
    private BigDecimal discount;
    private BigDecimal clientType;
    private BigDecimal clientId;

    @Override
    public BigDecimal getPriceListId() {
        return this.priceListId;
    }

    @Override
    public void setPriceListId(BigDecimal priceListID) throws IllegalIdException {
        if (null == priceListID) {
            throw new IllegalIdException("PriceList ID can't be null");
        }

        this.priceListId = priceListID;
    }

    @Override
    public BigDecimal getDiscount() {
        return this.discount;
    }

    @Override
    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    @Override
    public BigDecimal getClientType() {
        return this.clientType;
    }

    @Override
    public void setClientType(BigDecimal type) throws IllegalIdException {
        this.clientType = type;
    }

    @Override
    public BigDecimal getClientId() {
        return this.clientId;
    }

    @Override
    public void setClientId(BigDecimal clientId) throws IllegalIdException {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(PRICE_LIST_ID, getPriceListId())
                + fieldToString(DISCOUNT, getDiscount())
                + fieldToString(CL_TYPE, getClientType())
                + fieldToString(CL_ID, getClientId());
    }

}
