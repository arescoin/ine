package ru.effts.ine.dbe.da.billing.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountAccessImpl.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
public class AccountAccessImpl<T extends Account> extends CommonAccessImpl<T> implements AccountAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "BILL_ACCOUNT";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_PL_ID = "pl_id";
    public static final String COLUMN_DISCOUNT = "discount";
    public static final String COLUMN_CL_TYP = "cl_typ";
    public static final String COLUMN_CL_ID = "cl_id";

    /** Запрос на выборку */
    public static final String ACCOUNT_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_PL_ID +
            ", " + TABLE_PREF + COLUMN_CL_TYP +
            ", " + TABLE_PREF + COLUMN_CL_ID +
            ", " + TABLE_PREF + COLUMN_DISCOUNT +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.accounting.Account} */
    public static final String CACHE_REGION_BILL_ACCOUNT = Account.class.getName();


    static {
        setInstanceFieldsMapping(AccountAccessImpl.class, new HashMap<String, String>());
        registerSSAHelpers(AccountAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<>();
        map.put(Account.PRICE_LIST_ID, COLUMN_PL_ID);
        map.put(Account.CL_TYPE, COLUMN_CL_TYP);
        map.put(Account.CL_ID, COLUMN_CL_ID);
        map.put(Account.DISCOUNT, COLUMN_DISCOUNT);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerSSAHelpers(Class<? extends AccountAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_BILL_ACCOUNT;
    }

    @Override
    protected String getCommonSelect() {
        return ACCOUNT_SELECT;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Account.class;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(Account.class);

        this.setVersionableFields(resultSet, result);

        result.setPriceListId(this.getId(resultSet, COLUMN_PL_ID));
        result.setClientType(this.getId(resultSet, COLUMN_CL_TYP));
        result.setClientId(this.getId(resultSet, COLUMN_CL_ID));
        result.setDiscount(this.getBigDecimal(resultSet, COLUMN_DISCOUNT));

        return result;
    }

}
