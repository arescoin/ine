/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.item;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.Versionable;
import ru.xr.ine.oss.inventory.item.Item;
import ru.xr.ine.oss.inventory.item.PropertyData;
import ru.xr.ine.vaadin.AdminUiServlet;

import java.util.*;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class ItemView extends VerticalLayout implements View {

    public static final String VIEW_KEY = Item.class.getName();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();
        getUI().getSession().setAttribute(ItemCreateView.PARENT_ID_KEY, null);
        Collection<Item> itms = ((AdminUiServlet) VaadinServlet.getCurrent()).getItems();

        SortedSet<Item> items = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        items.addAll(itms);

//        Collection<DictionaryTerm> terms =
//                ((AdminUiServlet) VaadinServlet.getCurrent()).getDictionaryTermsForDic(new BigDecimal(49));

        Grid<PropertyData> propsGrid = new Grid<>(PropertyData.class);
        Grid<Item> grid = new Grid<>();
        grid.setSizeFull();
        grid.setItems(items);
        grid.addColumn(Identifiable::getCoreId).setCaption("Id"); //, BigDecimal.class);
        grid.addColumn(Item::getName).setCaption("Name"); // , String.class);
        grid.addColumn(Item::getParentId).setCaption("Parent"); // , BigDecimal.class);
        grid.addColumn(Item::getType).setCaption("Type"); // , BigDecimal.class);
        grid.addColumn(Item::getItemProfile).setCaption("Profile"); //, BigDecimal.class);
        grid.addColumn(Item::getStatus).setCaption("Status"); // , BigDecimal.class);
        grid.addColumn(Identifiable::getCoreDsc).setCaption("Dsc"); // , String.class);
        grid.addColumn(Versionable::getCoreFd).setCaption("fd"); // , Date.class);
        grid.addColumn(Versionable::getCoreTd).setCaption("td"); // , Date.class);

        GridContextMenu<Item> gridContextMenu = new GridContextMenu<>(grid);
        gridContextMenu.addGridBodyContextMenuListener(event1 -> {
            event1.getContextMenu().removeItems();
            Item item = (Item) event1.getItem();
            if (item != null) {
                grid.select(item);
                event1.getContextMenu().addItem("Edit Item", VaadinIcons.EDIT, selectedItem -> {
                    Notification.show(
                            ((AdminUiServlet) AdminUiServlet.getCurrent()).getPattern(itms.iterator().next()).toString()
                            , Notification.Type.TRAY_NOTIFICATION);
                });
                event1.getContextMenu().addItem("Add child... ", VaadinIcons.PLUS, selectedItem -> {
                    getUI().getSession().setAttribute(ItemCreateView.PARENT_ID_KEY, item.getCoreId());
                    UI.getCurrent().getNavigator().navigateTo(ItemCreateView.VIEW_KEY);
                });
            } else {
                event1.getContextMenu().addItem("Add row", VaadinIcons.PLUS, selectedItem -> {
                });
            }
        });

        this.addComponent(grid);
        propsGrid.setSizeFull();
        this.addComponent(propsGrid);

        Collection<PropertyData> datas =
                new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        grid.setSelectionMode(Grid.SelectionMode.SINGLE)
                .addSelectionListener(event1 -> {
                    datas.clear();
                    Optional<Item> firstSelectedItem = event1.getFirstSelectedItem();
                    if (firstSelectedItem != null && firstSelectedItem.isPresent()) {
                        Item item = firstSelectedItem.get();
                        datas.addAll(((AdminUiServlet) AdminUiServlet.getCurrent()).getPropertiesForItem(item.getCoreId()));
                        System.out.println(datas);
                        propsGrid.setItems(datas);
                    }
                });

        Button button = new Button("Add...");
        button.addClickListener(event1 -> UI.getCurrent().getNavigator().navigateTo(ItemCreateView.VIEW_KEY));
        this.addComponent(button);
    }

    private void updateGridBodyMenu(GridContextMenu.GridContextMenuOpenListener.GridContextMenuOpenEvent<Item> event) {
        event.getContextMenu().removeItems();
        Item item = (Item) event.getItem();
        if (item != null) {
            event.getContextMenu().addItem("Edit Item", VaadinIcons.EDIT, selectedItem -> {
                Notification.show("Edit... " + item, Notification.Type.TRAY_NOTIFICATION);
            });
            event.getContextMenu().addItem("Edit Item", VaadinIcons.PLUS, selectedItem -> {
                Notification.show("Add child... ", Notification.Type.TRAY_NOTIFICATION);
                getUI().getSession().setAttribute(ItemCreateView.PARENT_ID_KEY, item.getCoreId());
                UI.getCurrent().getNavigator().navigateTo(ItemCreateView.VIEW_KEY);
            });
        } else {
            event.getContextMenu().addItem("Add row", VaadinIcons.PLUS, selectedItem -> {
//                userData.add(new UserData("John", "Doe", "john.doe[at]lost.com"));
//                grid.setItems(userData);
            });
        }
    }

}
