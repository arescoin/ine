/*
"$Id: 009_INE_CORE_LINKS.sql 3852 2014-07-04 10:38:54Z DGomon $"
*/


CREATE TABLE LINKS
(
  N          NUMERIC                  NOT NULL
, DSC        CHARACTER VARYING(1)
, LEFT_OBJ   CHARACTER VARYING(255)   NOT NULL
, RIGHT_OBJ  CHARACTER VARYING(255)   NOT NULL
, TYP        NUMERIC                  NOT NULL  -- есть паттерн
, FD         TIMESTAMP WITH TIME ZONE NOT NULL
, TD         TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT UK_LINKS_ID   UNIQUE (N)
);

CREATE INDEX IND_LINKS_FDTD  ON   LINKS(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_LINKS INCREMENT BY 1 START WITH 1;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'LINKS', null, 'Содержит описание существующих в системе связей');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.core.links.Link', system_ID, 'Интерфейс описания связи');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Системный номер описателя связи');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание-комментарий описателя связи (не используется)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LEFT_OBJ', system_ID, 'Паттерн для описания объекта (типа) участника связи (sys_obj)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'RIGHT_OBJ', system_ID, 'Паттерн для описания объекта (типа) участника связи (sys_obj)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID, 'Тип ассоциации, ее кратность',
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':41]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE'));


  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

    -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_LINKS', system_ID, 'Последовательность для идентификатора');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('LINKS');

END$$;

CREATE TABLE LINK_DATA
(
  N          NUMERIC                   NOT NULL
, DSC        CHARACTER VARYING(1)
, LINK_N     NUMERIC                   NOT NULL -- есть паттерн
, LEFT_ID    NUMERIC                   NOT NULL
, RIGHT_ID   NUMERIC                   NOT NULL
, FD         TIMESTAMP WITH TIME ZONE  NOT NULL
, TD         TIMESTAMP WITH TIME ZONE  NOT NULL
, CONSTRAINT UK_LINK_DATA_ID  UNIQUE (N)
, CONSTRAINT LINK_DATA_LR_ID  UNIQUE (LINK_N, LEFT_ID, RIGHT_ID)
);

-- CREATE INDEX  LINK_DATA_LID     ON  LINK_DATA(LEFT_ID);
-- CREATE INDEX  LINK_DATA_RID     ON  LINK_DATA(RIGHT_ID);
-- CREATE INDEX  LINK_DATA_FDTD    ON  LINK_DATA(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_LINK_DATA INCREMENT BY 1 START WITH 100;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'LINK_DATA', null, 'Содержит данные связывания объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.core.links.LinkData', system_ID, 'Интерфейс данных связи');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Идентификатор конкретной связи');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание-комментарий связи (не используется)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LINK_N', system_ID, 'Ссылка на номер описателя связи', ''||INE_CORE_SYS_OBJ.get_Column_N('LINKS', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LEFT_ID', system_ID, 'Ссылка на идентификатор "родителя" связи');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'RIGHT_ID', system_ID, 'Ссылка на идентификатор связанного "подчиненного"');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

    -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_LINK_DATA', system_ID, 'Последовательность для идентификатора');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('LINK_DATA');

END$$;


-- Связи словарных статей
DO $$
DECLARE
  VAR1 CHARACTER VARYING(20);
  VAR2 CHARACTER VARYING(20);

  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  --связь названий и типов населенных пунктов
  VAR1 := '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':11]'
             ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
  VAR2 := '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':10]'
             ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
  INSERT INTO LINKS (N, LEFT_OBJ, RIGHT_OBJ, TYP, FD, TD) VALUES (nextval('SEQ_LINKS'), VAR2, VAR1, 1, VFD, VTD);

  --связь названий и типов улиц
  VAR1 := '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':13]'
             ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
  VAR2 := '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':12]'
             ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
  INSERT INTO LINKS (N, LEFT_OBJ, RIGHT_OBJ, TYP, FD, TD) VALUES (nextval('SEQ_LINKS'), VAR2, VAR1, 2, VFD, VTD);

END$$;