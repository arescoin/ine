/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.links;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.core.links.Link;
import ru.xr.ine.core.links.LinkData;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.ejbs.core.constants.ConstantBean;
import ru.xr.ine.ejbs.core.constants.ConstantDelegate;
import ru.xr.ine.ejbs.core.structure.FieldAccessDelegate;
import ru.xr.ine.ejbs.core.structure.SystemObjectPatternBean;
import ru.xr.ine.ejbs.core.structure.SystemObjectPatternDelegate;
import ru.xr.ine.ejbs.oss.entity.CompanyBean;
import ru.xr.ine.ejbs.oss.entity.CompanyDelegate;
import ru.xr.ine.oss.entity.Company;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class LinkBeanAndLinkDataDelegateTest extends BaseTest {

    private BigDecimal testId;

    @Before
    public void init() throws Exception {
        testId = new FieldAccessDelegate().getFieldId(TestObject.class, "coreId");
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void testBeansRemote() {

        try {
            //создаем линк
            logger.info("test CRUD for LinkBean");
            SystemObjectPatternBean systemObjectBean = new SystemObjectPatternDelegate();
            LinkDelegate<Link> linkDelegate = new LinkDelegate();
            LinkDataDelegate<LinkData> linkDataDelegate = new LinkDataDelegate();

            //исходное количество описания линков тестовых объектов
            int before = linkDelegate.getLinksByObjType(user, TestObject.class).size();

            Link newLink = IdentifiableFactory.getImplementation(Link.class);
            newLink.setTypeId(BigDecimal.ONE);
            newLink.setLeftType(systemObjectBean.checkPattern(user, new SystemObjectPattern(testId)));
            newLink.setRightType(systemObjectBean.checkPattern(user, new SystemObjectPattern(testId)));

            newLink = linkDelegate.createObject(user, newLink);

            Link link = linkDelegate.getObjectById(user, newLink.getCoreId());
            Assert.assertEquals(before + 1, linkDelegate.getLinksByObjType(user, TestObject.class).size());
            Assert.assertTrue(BigDecimal.ONE.equals(link.getTypeId()));

            //создаем фактическую связь(LinkData)
            logger.info("test CRUD for LinkDataBean");
            LinkData linkData = IdentifiableFactory.getImplementation(LinkData.class);
            linkData.setLinkId(link.getCoreId());
            linkData.setLeftId(BigDecimal.ONE);
            linkData.setRightId(BigDecimal.ONE);
            LinkData created = linkDataDelegate.createObject(user, linkData);

            //сейчас удаление описания линка стало невозможным
            try {
                linkDelegate.deleteObject(user, link);
                Assert.fail();
            } catch (GenericSystemException ex) {
            } catch (Exception ex) {
                fail(ex);
            }
            //изменение невозможно в принципе
            try {
                linkDelegate.updateObject(user, link);
                Assert.fail();
            } catch (GenericSystemException e) {
            } catch (Exception ex) {
                fail(ex);
            }
            //проверка что нельзя обновить устаревшей версией
            try {
                linkDataDelegate.updateObject(user, linkData);
                Assert.fail();
            } catch (GenericSystemException e) {
            } catch (Exception ex) {
                fail(ex);
            }
            SyntheticId id = linkDataDelegate.getSyntheticId(user, created);

            linkDataDelegate.deleteObject(user, created);
            if (linkDataDelegate.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove LinkData");
            }
            //теперь данных нет - можно удалять описание линка
            linkDelegate.deleteObject(user, link);
            if (linkDelegate.getObjectById(user, link.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Link");
            }
            Assert.assertEquals(before, linkDelegate.getLinksByObjType(user, TestObject.class).size());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void createLinkDataTest() {

        try {
            LinkBean<Link> linkDelegate = new LinkDelegate<Link>();
            LinkDataBean<LinkData> linkDataDelegate = (LinkDataBean<LinkData>) new LinkDataDelegate();
            CompanyBean<Company> partyDelegate = (CompanyBean<Company>) new CompanyDelegate<Company>();
            ConstantBean<Constant> beanConstant = (ConstantBean<Constant>) new ConstantDelegate();

            //проверка что между ossj сущностями нужно создавать ассоциацию а не link
            //UserValueBean<IneUserValue> userDelegate = (UserValueBean<IneUserValue>) new UserValueDelegate();
            /*
            try {
                linkDataDelegate.createLinkData(user, userDelegate.getObjectById(user, BigDecimal.ONE),
                        partyDelegate.getObjectById(user, BigDecimal.ONE));
                Assert.fail();
            } catch (GenericSystemException e) {
                Assert.assertEquals("EntityValue objects have to be connected by means of AssociationValue object",
                        e.getMessage());
            } catch (Exception ex) {
                fail(ex);
            }
            */
            //проверка что нельзя создать связь(линк) если описания не существует
            try {
                linkDataDelegate.createLinkData(user, partyDelegate.getObjectById(user, BigDecimal.ONE),
                        beanConstant.getObjectById(user, BigDecimal.ONE));
            } catch (GenericSystemException e) {
                Assert.assertTrue(e.getMessage().startsWith("Failed to find out link between"));
            } catch (Exception ex) {
                fail(ex);
            }

            //создаем описание линка
            FieldAccessDelegate beanSys = new FieldAccessDelegate();
            SystemObjectPatternBean patternBean = new SystemObjectPatternDelegate();
            Link newLink = IdentifiableFactory.getImplementation(Link.class);
            newLink.setTypeId(BigDecimal.ONE);
            newLink.setLeftType(patternBean.checkPattern(user,
                    new SystemObjectPattern(beanSys.getFieldId(Company.class, "coreId"))));
            newLink.setRightType(patternBean.checkPattern(user,
                    new SystemObjectPattern(beanSys.getFieldId(Constant.class, "coreId"))));
            newLink = linkDelegate.createObject(user, newLink);

            //создаем связь между константой и user
            LinkData ld = linkDataDelegate.createLinkData(user, partyDelegate.getObjectById(user, BigDecimal.ONE),
                    beanConstant.getObjectById(user, BigDecimal.ONE));
            Assert.assertNotNull(ld);

            //удаление связи и описания связи
            linkDataDelegate.deleteObject(user, ld);
            linkDelegate.deleteObject(user, newLink);
        } catch (Exception e) {
            fail(e);
        }
    }

//    @Test
//    @SuppressWarnings({"unchecked"})
//    public void getLinkIdTest() {
//
//        try {
//            LinkBean<Link> linkDelegate = new LinkDelegate<Link>();
//
//            поиск идентификатора описания связи между продуктами и модифицированными типами
//            BigDecimal id = linkDelegate.getLinkId(user, ProductValue.class, CustomType.class, 1);
//            Assert.assertNotNull(id);
//
//        } catch (Exception e) {
//            fail(e);
//        }
//    }

}
