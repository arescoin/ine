/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import java.math.BigDecimal;

/**
 * Обертка для применения в недрах системы
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class PermitView {

    private final BigDecimal userId;
    private final BigDecimal permitId;
    private final BigDecimal roleId;
    private final BigDecimal[] values;
    private final boolean active;
    private final int crudMask;


    public PermitView(BigDecimal userId, BigDecimal permitId,
            BigDecimal roleId, BigDecimal[] values, boolean active, int crudMask) {
        this.userId = userId;
        this.permitId = permitId;
        this.roleId = roleId;
        this.values = values;
        this.active = active;
        this.crudMask = crudMask;
    }

    public BigDecimal getUserId() {
        return userId;
    }

    public BigDecimal getPermitId() {
        return permitId;
    }

    public BigDecimal getRoleId() {
        return roleId;
    }

    public BigDecimal[] getValues() {
        return values;
    }

    public boolean isActive() {
        return active;
    }

    public int getCrudMask() {
        return crudMask;
    }
}
