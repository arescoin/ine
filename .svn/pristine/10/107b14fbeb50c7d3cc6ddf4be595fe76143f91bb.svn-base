/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.userpermits.InvalidUserNameException;
import ru.xr.ine.core.userpermits.NoSuchUserException;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.ejbs.core.VersionableDelegate;
import ru.xr.ine.utils.MD5;

import javax.ejb.EJBException;
import java.security.NoSuchAlgorithmException;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link SystemUserBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
@SuppressWarnings({"unchecked"})
public class SystemUserDelegate<T extends SystemUser> extends VersionableDelegate<T> implements SystemUserBean<T> {

    public SystemUserDelegate() {
        init("stateless.SystemUserBean");
    }

    /**
     * Производит проверку существования системного пользователя по его системному имени и паролю,
     * в случае подтверждения корректности комбинации возвращается инстанс системного пользователя.
     *
     * @param login    системное имя пользователя
     * @param password пароль пользователя в открытом виде
     * @return пользователь
     * @throws ru.xr.ine.core.userpermits.NoSuchUserException если пользователь с указанными данными не найден
     * @throws ru.xr.ine.core.GenericSystemException при ошщибках доступа к данным
     */
    @Override
    public T login(String login, String password) throws GenericSystemException, NoSuchUserException {

        if (login == null || login.length() < 1) {
            throw new InvalidUserNameException("UserName (login) [" + login + "]");
        }

        try {
            String md5 = MD5.calculate(password);
            return ((SystemUserBean<T>) lookup()).login(login, md5);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
        catch (NoSuchAlgorithmException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }
}
