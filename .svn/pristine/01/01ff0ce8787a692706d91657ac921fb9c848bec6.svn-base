package ru.xr.ine.ejbs.billing.business.rest.core;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.core.constants.ConstantValue;
import ru.xr.ine.core.constants.ParametrizedConstant;
import ru.xr.ine.core.constants.ParametrizedConstantValue;
import ru.xr.ine.ejbs.billing.business.rest.SrvUtils;
import ru.xr.ine.ejbs.core.constants.ConstantBean;
import ru.xr.ine.ejbs.core.constants.ConstantValueBean;
import ru.xr.ine.ejbs.core.constants.ParametrizedConstantBean;
import ru.xr.ine.ejbs.core.constants.ParametrizedConstantValueBean;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id$"
 */
@Path("/constant")
@Stateless
public class ConstantService {

    private static final Logger logger = Logger.getLogger(ConstantService.class.getName());

    @EJB(mappedName = "stateless.ConstantBean", name = "ConstantBean")
    ConstantBean<Constant> constantBean;

    @EJB(mappedName = "stateless.ConstantValueBean", name = "ConstantValueBean")
    ConstantValueBean<ConstantValue> constantValueBean;

    @EJB(mappedName = "stateless.ParametrizedConstantBean", name = "ParametrizedConstantBean")
    ParametrizedConstantBean<ParametrizedConstant> parametrizedConstantBean;

    @EJB(mappedName = "stateless.ParametrizedConstantValueBean", name = "ParametrizedConstantValueBean")
    ParametrizedConstantValueBean<ParametrizedConstantValue> parametrizedConstantValueBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getById")
    @GET
    public Response getById(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    constantBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getList")
    @GET
    public Response getList() {

        try {
            return SrvUtils.createCollectionResponse(constantBean.getAllObjects(UserHolder.getUser()));
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getValue")
    @GET
    public Response getValue(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    constantValueBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/p")
    @GET
    public Response getParamById(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    parametrizedConstantBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/p/getList")
    @GET
    public Response getParamList() {

        try {
            return SrvUtils.createCollectionResponse(
                    parametrizedConstantBean.getAllObjects(UserHolder.getUser()));
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/p/getValue")
    @GET
    public Response getParamValue(@QueryParam("id") Long id, @QueryParam("param") Long param) {

        try {
            return SrvUtils.createObjectResponse(parametrizedConstantValueBean.getObjectById(
                            UserHolder.getUser(), BigDecimal.valueOf(id), BigDecimal.valueOf(param))
            );
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/p/getValueById")
    @GET
    public Response getParamValueById(@QueryParam("id") Long id) {

        try {

            SearchCriteriaProfile profile =
                    parametrizedConstantValueBean.getSearchCriteriaProfile(UserHolder.getUser());

            Set<SearchCriterion> criteria = new LinkedHashSet<>();
            criteria.add(profile.getSearchCriterion(
                            ParametrizedConstantValue.CORE_ID, CriteriaRule.equals, new BigDecimal(id))
            );

            return SrvUtils.createCollectionResponse(
                    parametrizedConstantValueBean.searchObjects(UserHolder.getUser(), criteria));
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
