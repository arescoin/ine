/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.language;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.language.CountryDetails;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к кодам стран по iso3166.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public interface CountryDetailsAccess<T extends CountryDetails> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент коды стран
     *
     * @return коллекция описаний словарей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
