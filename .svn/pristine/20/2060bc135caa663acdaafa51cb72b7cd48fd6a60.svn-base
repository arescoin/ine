/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Предоставляет статические методы для конвертации бинарной маски в значения и обратно
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: BinaryMaskUtil.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public final class BinaryMaskUtil {

    private BinaryMaskUtil() {
    }

    public static long valuesToMask(long[] values) {

        long mask = 0;

        for (long value : values) {
            mask += value;
        }

        return mask;
    }

    public static Set<Long> maskToValues(long mask) {

        HashSet<Long> values = new HashSet<Long>();
        int order = log2(mask);

        for (int i = 0; i <= order; i++) {
            if ((mask & (1 << i)) > 0) {
                values.add((long) 1 << i);
            }
        }
        return values;
    }

    private static int log2(long value) {
        return (int) doubleLog2(value);
    }

    private static double doubleLog2(long value) {
        return Math.log(value) / Math.log(2);
    }
}
