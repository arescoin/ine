/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.language;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.utils.BaseIdentifiableTest;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangNameAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class LangNameAccessTest extends BaseIdentifiableTest {

    private static LangNameAccess<LangName> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (LangNameAccess<LangName>) AccessFactory.getImplementation(LangName.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public Identifiable testCreate() {
        try {
            LangName obj1 = IdentifiableFactory.getImplementation(LangName.class);

            obj1.setCoreId(new BigDecimal(3));
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setName("LANG NAME");

            obj1 = access.createObject(obj1);
            LangName obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getName(), obj2.getName());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            LangName obj1 = (LangName) identifiable;
            LangName obj2 = IdentifiableFactory.getImplementation(LangName.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setName(obj1.getName() + " UPD");
            obj2.setCoreDsc(obj1.getCoreDsc());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to update object on DB.", obj1.getName(), obj2.getName());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((LangName) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testLangName() {
        try {
            for (LangName Name : access.getAllObjects()) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + Name);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testSCP() {
        try {
            SearchCriteriaProfile searchCriteriaProfile = access.getSearchCriteriaProfile();
            @SuppressWarnings({"unchecked"}) SearchCriterion<BigDecimal> searchCriterion =
                    (SearchCriterion<BigDecimal>) searchCriteriaProfile.getSearchCriterion(LangName.CORE_ID);

            searchCriterion.setCriterionRule(CriteriaRule.greater);
            searchCriterion.setInvert(false);
            searchCriterion.addCriteriaVal(new BigDecimal(0));

            Set<SearchCriterion> searchCriterias = new HashSet<SearchCriterion>();
            searchCriterias.add(searchCriterion);
            long start = System.currentTimeMillis();
            Collection collection = access.searchObjects(searchCriterias);
            long end = System.currentTimeMillis();
            System.out.println("res: size [" + collection.size() + "]; time[" + (end - start) + "] ...");
        } catch (GenericSystemException e) {
            e.printStackTrace();
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }
}

