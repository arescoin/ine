/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.item;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.oss.inventory.item.*;
import ru.xr.ine.vaadin.AdminUiServlet;
import ru.xr.ine.vaadin.inventory.utils.ItemVO;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class ItemCreateView extends VerticalLayout implements View {

    public static final String VIEW_KEY = ItemCreateView.class.getName();
    public static final String PARENT_ID_KEY = ItemCreateView.class.getName() + "_PARENT_ID";
    private Collection<DictionaryTerm> itemTypeTerms;
    private Collection<DictionaryTerm> subTerms;
    private Collection<ItemProfile> itemProfiles;
    private Collection<PropertyDescriptorValue> descriptors1;


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSizeFull();
        VerticalLayout itemLayout = new VerticalLayout();
        VerticalLayout propLayout = new VerticalLayout();

        this.setId(ItemCreateView.class.getName());
        this.addStyleName("ItemCreateView");
        this.removeAllComponents();
        this.setSizeFull();
        this.setMargin(true);
        this.setSpacing(true);

        TextField name = new TextField("Item Name");
        itemLayout.addComponent(name);

        Object parentId = getUI().getSession().getAttribute(ItemCreateView.PARENT_ID_KEY);

        TextField parent = new TextField("Parent ID");
        String parentIdPrompt = "Set parent Item ID";
        parent.setPlaceholder(parentIdPrompt);
        if (parentId != null) {
            parent.setValue(parentId.toString());
        }
        itemLayout.addComponent(parent);

        itemTypeTerms = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        itemTypeTerms.clear();

        subTerms = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        subTerms.clear();

        itemProfiles = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        itemProfiles.clear();

        itemTypeTerms.addAll(
                ((AdminUiServlet) VaadinServlet.getCurrent()).getDictionaryTermsForDic(new BigDecimal(47)));

        itemProfiles.addAll(((AdminUiServlet) VaadinServlet.getCurrent()).getItemProfiles());

        ComboBox<ItemProfile> profileComboBox = new ComboBox<>("Item Profile", itemProfiles);
        profileComboBox.setItemCaptionGenerator(item -> item != null ? item.getCoreId() + " :  " + item.getName() : "");
        profileComboBox.setPlaceholder("No Profiles...");
        profileComboBox.addValueChangeListener(event1 -> createPropContent(propLayout, profileComboBox.getValue()));

        ComboBox<DictionaryTerm> subTypesComboBox = new ComboBox<>("Item Subtypes", subTerms);
        subTypesComboBox.setItemCaptionGenerator(item -> item != null ? item.getCoreId() + " : " + item.getTerm() : "");
        subTypesComboBox.setEmptySelectionAllowed(false);
        subTypesComboBox.addValueChangeListener(event1 ->
                setItemProfile(event1.getValue().getCoreId(), profileComboBox, propLayout));

        ComboBox<DictionaryTerm> typesComboBox = new ComboBox<>("Item types", itemTypeTerms);
        itemLayout.addComponent(typesComboBox);
        typesComboBox.setEmptySelectionAllowed(false);
        typesComboBox.setItemCaptionGenerator(item -> item.getCoreId() + " : " + item.getTerm());
        typesComboBox.addValueChangeListener(event1 -> {
//            Notification.show(((AdminUiServlet) AdminUiServlet.getCurrent()).getPattern(event1.getValue()).keySet().iterator().next().toString());
            setItemSubType(event1.getValue().getCoreId(), subTypesComboBox);
        });
        typesComboBox.setSelectedItem(itemTypeTerms.iterator().next());

        itemLayout.addComponent(subTypesComboBox);
        itemLayout.addComponent(profileComboBox);

        Collection<DictionaryTerm> terms =
                ((AdminUiServlet) VaadinServlet.getCurrent()).getDictionaryTermsForDic(new BigDecimal(49));
        ComboBox<DictionaryTerm> statusComboBox = new ComboBox<>("Item status", terms);
        statusComboBox.setEmptySelectionAllowed(false);
        statusComboBox.setItemCaptionGenerator(item -> item.getCoreId() + " :  " + item.getTerm());
        statusComboBox.setSelectedItem(terms.iterator().next());

        itemLayout.addComponent(statusComboBox);

        TextArea dsc = new TextArea("Item Description");
        itemLayout.addComponent(dsc);

        Button button = new Button("Create...");
        button.addClickListener(event1 -> {

            Item item = ((AdminUiServlet) VaadinServlet.getCurrent()).createItem(
                    new ItemVO(subTypesComboBox.getValue().getCoreId(), name.getValue(), parent.getValue(),
                            profileComboBox.getValue().getCoreId(), statusComboBox.getValue().getCoreId(), dsc.getValue()
                    )
            );



            for (PropertyDescriptorValue propertyDescriptorValue : descriptors1) {
                System.out.println(propertyDescriptorValue.getId() +
                        " : " +
                        propertyDescriptorValue.getItemProfileProperty().getPropertyDescriptorId() +
                        " : " +
                        propertyDescriptorValue.getItemProfileProperty().getItemProfileId() +
                        " : " +
                        propertyDescriptorValue.getName() +
                        " : " +
                        propertyDescriptorValue.getPropertyDescriptor().getType() +
                        " : " +
                        propertyDescriptorValue.getValue());

                try {
                    PropertyDataLong dataLong = IdentifiableFactory.getImplementation(PropertyDataLong.class);
                    dataLong.setData(new Long(propertyDescriptorValue.getValue()));
                    dataLong.setItemId(item.getCoreId());
                    dataLong.setDescriptorId(propertyDescriptorValue.getPropertyDescriptor().getCoreId());
                    dataLong.setCoreDsc("created from GUI");
                    ((AdminUiServlet) VaadinServlet.getCurrent()).createPropertyValue(dataLong);
                } catch (GenericSystemException e) {
                    e.printStackTrace();
                }


            }



            UI.getCurrent().getNavigator().navigateTo(ItemView.VIEW_KEY);
        });
        itemLayout.addComponent(button);

        horizontalLayout.addComponent(itemLayout);
        horizontalLayout.addComponent(propLayout);
        this.addComponent(horizontalLayout);

    }

    private void setItemSubType(BigDecimal parenId, ComboBox<DictionaryTerm> box) {

        box.setItems(Collections.emptyList());
        subTerms.clear();
        Collection<DictionaryTerm> terms =
                ((AdminUiServlet) VaadinServlet.getCurrent()).getDictionaryTermsForDic(new BigDecimal(48));
        for (DictionaryTerm term : terms) {
            if (term.getParentTermId().equals(parenId)) {
                subTerms.add(term);
            }
        }
        box.setItems(subTerms);
        box.setSelectedItem(subTerms.iterator().next());
    }

    private void setItemProfile(BigDecimal typeId, ComboBox<ItemProfile> box, VerticalLayout propLayout) {

        Collection<ItemProfile> profiles = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        for (ItemProfile itemProfile : itemProfiles) {
            if (itemProfile.getItemType().equals(typeId)) {
                profiles.add(itemProfile);
            }
        }

        box.setItems(profiles);

        if (!profiles.isEmpty()) {
            box.setEmptySelectionAllowed(false);
            box.setSelectedItem(profiles.iterator().next());
            createPropContent(propLayout, box.getValue());
        } else {
            box.setEmptySelectionAllowed(true);
            box.setValue(null);
            propLayout.removeAllComponents();
        }
    }

    private void createPropContent(VerticalLayout propLayout, ItemProfile profile) {

        propLayout.removeAllComponents();
        propLayout.setSizeFull();

        Collection<ItemProfileProperty> properties = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        properties.addAll(((AdminUiServlet) VaadinServlet.getCurrent()).getItemProfileProperties());

        Collection<PropertyDescriptor> descriptors = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        descriptors.addAll(((AdminUiServlet) VaadinServlet.getCurrent()).getPropertyDescriptors());

        TextField taskField = new TextField();

        Grid<PropertyDescriptorValue> grid = new Grid<>("Item Property ...");
        grid.setSizeFull();
        grid.addColumn(PropertyDescriptorValue::getId).setCaption("ID");
        grid.addColumn(PropertyDescriptorValue::getName).setCaption("Name");
        grid.addColumn(PropertyDescriptorValue::getType).setCaption("Type");
        grid.addColumn(PropertyDescriptorValue::getValue)
                .setEditorComponent(taskField, PropertyDescriptorValue::setValue)
                .setCaption("Value")
                .setEditable(true);
        grid.addColumn(PropertyDescriptorValue::getDsc).setCaption("Description");

        descriptors1 = new TreeSet<>(Comparator.comparing(PropertyDescriptorValue::getId));

        // [63:51][67:1]64

        for (ItemProfileProperty property : properties) {
            if (property.isEditable() && property.getItemProfileId().equals(profile.getCoreId())) {
                for (PropertyDescriptor descriptor : descriptors) {
                    if (descriptor.getCoreId().equals(property.getPropertyDescriptorId())) {
                        descriptors1.add(new PropertyDescriptorValue(descriptor, property));
                        break;
                    }
                }
            }
        }

        grid.getEditor().setEnabled(true);
        grid.setItems(descriptors1);
        propLayout.addComponent(grid);
    }

    private static class PropertyDescriptorValue {
        private PropertyDescriptor propertyDescriptor;
        private ItemProfileProperty itemProfileProperty;
        private String value = "";
        private DataType type;

        public PropertyDescriptorValue(PropertyDescriptor propertyDescriptor, ItemProfileProperty itemProfileProperty) {
            this.propertyDescriptor = propertyDescriptor;
            this.itemProfileProperty = itemProfileProperty;
            if (itemProfileProperty.getDefaultValue() != null) {
                this.value = itemProfileProperty.getDefaultValue();
            }
            this.type = propertyDescriptor.getType();
        }

        public BigDecimal getId() {
            return propertyDescriptor.getCoreId();
        }

        public String getDsc() {
            return propertyDescriptor.getCoreDsc();
        }

        public DataType getType() {
            return type;
        }

        public ItemProfileProperty getItemProfileProperty() {
            return itemProfileProperty;
        }

        public void setItemProfileProperty(ItemProfileProperty itemProfileProperty) {
            this.itemProfileProperty = itemProfileProperty;
        }

        public PropertyDescriptor getPropertyDescriptor() {
            return propertyDescriptor;
        }

        public void setPropertyDescriptor(PropertyDescriptor propertyDescriptor) {
            this.propertyDescriptor = propertyDescriptor;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return propertyDescriptor.getName();
        }
    }

}
