/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.links;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.links.Link} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkBean.java 1641 2011-03-14 09:07:26Z sfilatov $"
 */
public interface LinkBean<T extends Link> extends VersionableBean<T> {

    /**
     * Метод возвращает все описания связей, в которых участвует объект указанного типа.
     *
     * @param user  пользователь, запрашивающий описание связей
     * @param clazz класс объекта, для которого получаются описания связей
     * @return коллекция описаний связей
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибках в работе кеша или при некорректных данных
     */
    Collection<T> getLinksByObjType(UserProfile user, Class clazz) throws GenericSystemException;

    /**
     * Возвращает идентифкатор описания линка
     *
     * @param user      пользователь, выполняющий запрос
     * @param leftType  класс первого объекта линка
     * @param rightType класс второго объекта линка
     * @param type      тип линка, характеризующий взаимосвязь объектов
     * @return BigDecimal идентификатор описания линка или null если связь не найдена
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибках в данных объекта или в мета-информации
     */
    public BigDecimal getLinkId(UserProfile user, Class leftType, Class rightType, long type)
            throws GenericSystemException;

}
