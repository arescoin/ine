package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Реализует {@link BankingDetailsBean}
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "BankingDetailsBean", mappedName = "stateless.BankingDetailsBean")
@Remote(BankingDetailsBean.class)
public class BankingDetailsBeanImpl<T extends BankingDetails>
        extends VersionableBeanImpl<T> implements BankingDetailsBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    public void init() throws GenericSystemException {
        init(BankingDetails.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}