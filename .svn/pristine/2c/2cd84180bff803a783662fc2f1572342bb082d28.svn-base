/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.funcswitch;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.funcswitch.FuncSwitch;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к значениям переключателей функциональности
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public interface FuncSwitchAccess<T extends FuncSwitch> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент значения переключателей функциональности
     *
     * @return коллекция значений переключателей функциональности
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
