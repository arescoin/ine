/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Описывает интерфейс доступа к системным объектам
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public interface SystemObjectAccess<T extends SystemObject> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действытельные, на текущий момент, системные объекты
     *
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.CorruptedIdException
     *          при обнаружении некоррекного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException
     *          при некорректых значениях в датах системного объекта
     */
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Возвращает маппинг "идентификатор таблицы - имя класса"
     *
     * @return маппинг "идентификатор таблицы - имя класса"
     * @throws GenericSystemException при возникновении ошибок в механизме доступа к мета
     */
    Map<BigDecimal, String> getInterfaces() throws GenericSystemException;

    BigDecimal getColumnId(String tableName, String columnName) throws GenericSystemException;

    Collection<T> getChildren(BigDecimal parentId) throws CoreException;

    BigDecimal getTableId(Class identifiableClass) throws GenericSystemException;

    BigDecimal getSequenceIdByClass(Class identifiableClass) throws CoreException;

    Collection<T> getPatternObjects(BigDecimal tableId) throws GenericSystemException;

    public Collection<T> getPatternsForType(BigDecimal tableId) throws GenericSystemException;

    Date getCurrentDate() throws GenericSystemException;

    Date getSystemFd() throws GenericSystemException;

    Date getSystemTd() throws GenericSystemException;
}
