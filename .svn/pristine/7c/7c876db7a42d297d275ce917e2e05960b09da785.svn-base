/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.dic;

import ru.effts.ine.core.*;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.dbe.da.core.AbstractCRUDHelper;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.dbe.da.core.IdentifiableAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermAccessImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class DictionaryTermAccessImpl<T extends DictionaryTerm>
        extends DictionaryEntryAccessImpl<T> implements DictionaryTermAccess<T> {

    /** Перечисление колонок таблицы */
    public static final String COLUMN_LANG = "lang";
    public static final String COLUMN_TERM = "term";

    /** Запрос на выборку всех словарных статей */
    public static final String ALL_DICTIONARY_TERMS_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_DIC_N +
            ", " + TABLE_PREF + COLUMN_CODE +
            ", " + TABLE_PREF + COLUMN_LANG +
            ", " + TABLE_PREF + COLUMN_TERM +
            ", " + TABLE_PREF + COLUMN_UP +
            ", NULL AS " + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.dic.DictionaryTerm} */
    public static final String CACHE_REGION_DICTIONARY_TERMS = DictionaryTerm.class.getName();

    /** Кешируем локально идентификатор столбца, чтоб не лазать лишний раз по сети в кеш */
    private static BigDecimal LANG_COLUMN_ID;

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(DictionaryTerm.LANG, COLUMN_LANG);
        map.put(DictionaryTerm.TERM, COLUMN_TERM);
        setInstanceFieldsMapping(DictionaryTermAccessImpl.class, map);

        setIdColumns(DictionaryTermAccessImpl.class, new String[]{COLUMN_DIC_N, COLUMN_CODE, COLUMN_LANG});

        Set<CRUDHelper> dictionaryTernHelpers = new LinkedHashSet<CRUDHelper>();
        dictionaryTernHelpers.add(new DictionaryTermHelper());
        registerDictionaryEntryHelpers(DictionaryTermAccessImpl.class, dictionaryTernHelpers);
    }

    @Override
    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {
        SyntheticId syntheticId = super.getSyntheticId(identifiable);
        syntheticId.addIdEntry(getColumnId(), identifiable.getLangCode());

        return syntheticId;
    }

    private static BigDecimal getColumnId() throws GenericSystemException {
        if (LANG_COLUMN_ID == null) {
            LANG_COLUMN_ID = FieldAccess.getColumnId(TABLE, COLUMN_LANG);
        }

        return LANG_COLUMN_ID;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_DICTIONARY_TERMS;
    }

    @Override
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {
        checkIdParam(id);
        identifiable.setDictionaryId(id[0]);
        identifiable.setCoreId(id[1]);
        identifiable.setLangCode(id[2]);
    }

    @Override
    protected void checkIdParam(BigDecimal... id) throws IneIllegalArgumentException {
        super.checkIdParam(3, "id[0] - term.dictionaryId, id[1] - term.id, id[2] - term.langCode", id);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(DictionaryTerm.class);

        value = super.fillObject(resultSet, value);

        value.setLangCode(this.getId(resultSet, COLUMN_LANG));
        value.setTerm(this.getString(resultSet, COLUMN_TERM));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_DICTIONARY_TERMS, ALL_DICTIONARY_TERMS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_DICTIONARY_TERMS, ALL_DICTIONARY_TERMS_SELECT, id);
    }

    @Override
    public Collection<T> getAllTermsForDictionaryAndLanguage(BigDecimal dictionaryId, BigDecimal languageId)
            throws GenericSystemException {

        SearchCriteriaProfile profile = getSearchCriteriaProfile();

        Set<SearchCriterion> searchCriteria = new HashSet<SearchCriterion>();
        searchCriteria.add(profile.getSearchCriterion(DictionaryEntry.DIC_ID, CriteriaRule.equals, dictionaryId));
        searchCriteria.add(profile.getSearchCriterion(DictionaryTerm.LANG, CriteriaRule.equals, languageId));

        return this.searchObjects(searchCriteria);
    }

    @Override
    public Collection<T> getAllTermsForDictionaryAndEntry(BigDecimal dictionaryId, BigDecimal entryId)
            throws GenericSystemException {

        SearchCriteriaProfile profile = getSearchCriteriaProfile();

        Set<SearchCriterion> searchCriteria = new HashSet<SearchCriterion>();
        searchCriteria.add(profile.getSearchCriterion(DictionaryEntry.DIC_ID, CriteriaRule.equals, dictionaryId));
        searchCriteria.add(profile.getSearchCriterion(Identifiable.CORE_ID, CriteriaRule.equals, entryId));

        return this.searchObjects(searchCriteria);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_DICTIONARY_TERMS, ALL_DICTIONARY_TERMS_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_DICTIONARY_TERMS);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_DICTIONARY_TERMS, ALL_DICTIONARY_TERMS_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return DictionaryTerm.class;
    }


    public static final class DictionaryTermHelper<E extends DictionaryTerm> extends AbstractCRUDHelper<E> {

        @SuppressWarnings({"unchecked"})
        private DictionaryTermAccessImpl<E> getTermAccess() throws GenericSystemException {
            return (DictionaryTermAccessImpl) AccessFactory.getImplementation(DictionaryTerm.class);
        }

        @SuppressWarnings({"unchecked"})
        private DictionaryEntryAccess<DictionaryEntry> getEntryAccess() throws GenericSystemException {
            return (DictionaryEntryAccess) AccessFactory.getImplementation(DictionaryEntry.class);
        }

        @Override
        public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {
            this.checkUpperTerm(identifiable);
        }

        @Override
        public void afterCreate(E identifiable) throws GenericSystemException {
            this.putEntryInCache(identifiable);
        }

        @Override
        public void beforeUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {
            this.checkUpperTerm(identifiable);
        }

        @Override
        public void afterUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {
            this.putEntryInCache(newIdentifiable);
        }

        @Override
        public void afterDelete(E identifiable) throws GenericSystemException {
            DictionaryEntryAccess<DictionaryEntry> access = this.getEntryAccess();
            Cache.remove(access.getMainRegionKey(), access.getSyntheticId(identifiable));
        }

        private void checkUpperTerm(E entry) throws GenericSystemException {
            try {

                Dictionary dict = this.getDictionary(entry.getDictionaryId());
                if (dict.getParentId() != null) {

                    // Если есть родительский словарь, то надо требовать заполнения верхнего термина в статье
                    if (entry.getParentTermId() == null) {
                        throw new IneIllegalArgumentException("Dictionary [" + dict.getCoreId() + ", '" +
                                dict.getName() + "] have parent dictionary, but entry misses parentTermId value");
                    }

                    // Также проверяем, что указанный верхний термин есть в родительском словаре
                    DictionaryEntry parentEntry =
                            this.getEntryAccess().getObjectById(dict.getParentId(), entry.getParentTermId());
                    if (parentEntry == null) {
                        throw new IneIllegalArgumentException("Incorrect parentTermId specified. Value [" +
                                entry.getParentTermId() + "] doesn't exists in dictionary #" + dict.getParentId());
                    }

                } else {
                    // Если нет родительского словаря, то надо требовать, чтоб верхний термин был пустой
                    if (entry.getParentTermId() != null) {
                        throw new IneIllegalArgumentException("Dictionary [" + dict.getCoreId() + ", '" +
                                dict.getName() + "'] haven't parent dictionary: parentTermId must not be specified");
                    }
                }

            } catch (CoreException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

        private Dictionary getDictionary(BigDecimal dictionaryId) throws GenericSystemException {
            try {
                @SuppressWarnings({"unchecked"}) DictionaryAccess<Dictionary> dictionaryAccess =
                        (DictionaryAccess<Dictionary>) AccessFactory.getImplementation(Dictionary.class);
                return dictionaryAccess.getObjectById(dictionaryId);
            } catch (CoreException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

        private void putEntryInCache(E identifiable) throws GenericSystemException {

            DictionaryEntry entry = IdentifiableFactory.getImplementation(DictionaryEntry.class);
            entry.setCoreId(identifiable.getCoreId());
            entry.setDictionaryId(identifiable.getDictionaryId());
            entry.setParentTermId(identifiable.getParentTermId());
            entry.setCoreDsc(identifiable.getCoreDsc());
            entry.setCoreFd(FieldAccess.getSystemFd());
            entry.setCoreTd(FieldAccess.getSystemTd());

            DictionaryEntryAccess<DictionaryEntry> access = this.getEntryAccess();
            Cache.put(access.getMainRegionKey(), access.getSyntheticId(entry), entry);
        }

    }

}
