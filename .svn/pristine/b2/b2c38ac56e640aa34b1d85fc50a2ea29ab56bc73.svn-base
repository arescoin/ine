/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import javassist.*;
import ru.xr.ine.core.*;
import ru.xr.ine.core.userpermits.CrudCode;
import ru.xr.ine.core.userpermits.SystemUser;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Предоставляет метод для получения реализации {@link ObjectAccess} по классу интерфейса, расширяющего {@link
 * Identifiable} и имеющего конкретную раедизацию, в соответствие с соглашением об именовании.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class AccessFactory {

    private static final String PCG_PREF_RU_XR_INE = "ru.xr.ine.";

    private static final String PCG_PREF_DBE_DA = "dbe.da.";

    private static final String DOT = ".";

    private static final String ACCESS_IMPL_SFX = "AccessImpl";

    public static final String PCG_PREF_JAVAX_OSS = "javax.oss.";

    /** Локальные карты */

    /** Простые Identifiable-классы */
    private static final Map<Class<? extends Identifiable>, Class<? extends ObjectAccess>> MAP = new HashMap<>();

    /** Identifiable-классы с реализацией OSS/J-интерфейсов */
    private static final Map<Class, Class<? extends ObjectAccess>> CBE_MAP = new HashMap<>();

    /** Просто маркеры */
    private static final Map<Class<? extends SystemMarker>, Class<? extends ObjectAccess>> SM_MAP = new HashMap<>();

    private static Logger logger = Logger.getLogger(AccessFactory.class.getName());

    private static ClassPool classPool;

    static {
        classPool = ClassPool.getDefault();
        classPool.appendClassPath(new LoaderClassPath(AccessFactory.class.getClassLoader()));
    }

    private AccessFactory() {
    }

    /**
     * Получает конкретный класс предоставляющий методы доступа к данным
     *
     * @param systemClass класс-наследник {@link Identifiable}, или {@link SystemMarker} или из иерархии OSS/J классов.
     * @return конкретная реализация {@link ObjectAccess}
     * @throws ru.xr.ine.core.GenericSystemException
     *          при возникновении ошибки в процессе поиска и инстанционирования
     */
    @SuppressWarnings({"unchecked"})
    public static ObjectAccess getImplementation(Class systemClass) throws GenericSystemException {

        if (systemClass == null) {
            throw new IneIllegalArgumentException("systemClass argument is null");
        }

        checkUserIsSet();
        ObjectAccess result;
        Class targetClass = extractInterface(systemClass);

        try {
            if (targetClass.getName().startsWith(PCG_PREF_JAVAX_OSS)) {
                result = getImplForCBE(targetClass);
            } else if (Identifiable.class.isAssignableFrom(targetClass)) {
                result = getImplForIdentifiable(targetClass);
            } else if (SystemMarker.class.isAssignableFrom(targetClass)) {
                result = getImplForSysMarker(targetClass);
            } else {
                throw new IneIllegalArgumentException("Received class ["
                        + systemClass.getName() + "] is not implements one of OSS/J classes "
                        + " or " + Identifiable.class.getName()
                        + " or " + SystemMarker.class.getName());
            }

        } catch (IllegalAccessException | InstantiationException e) {
            throw GenericSystemException.toGeneric(
                    "Can't Instantiate an Implementation for: [" + systemClass.getName() + "]", e);
        }

        return result;
    }

    /**
     * Возвращает класс интерфейса, класс реализация которого передана методу, проверяя на интерфейс исходный аргумент
     *
     * @param aClass исходная реализация
     * @return интерфейс реализации
     * @throws ru.xr.ine.core.GenericSystemException
     *          при некорректном параметре, невозможности получения класса
     *          интейфейса
     */
    public static Class extractInterface(Class aClass) throws GenericSystemException {

        Class result;

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Extract Interface for: " + aClass.getName());
        }

        if (aClass.isInterface()) {
            logger.log(Level.FINE, "Received value is already an Interface.");
            result = aClass;
        } else {
            logger.log(Level.FINE, "Getting stored Interface.");
            result = IdentifiableFactory.getInterface(aClass);
        }

        return result;
    }

    /**
     * Получает акцесс для OSSJ классов
     *
     * @param targetClass источник
     * @return акцесс
     * @throws IllegalAccessException при недоступности класса в JVM
     * @throws InstantiationException если тип класса не позволяет создавать новые инстансы
     * @throws ru.xr.ine.core.GenericSystemException
     *                                при возникновении ошибки в процессе поиска и инстанционирования
     */
    private static ObjectAccess getImplForCBE(Class targetClass)
            throws GenericSystemException, InstantiationException, IllegalAccessException {

        for (Class ifClass : targetClass.getInterfaces()) {
            try {
                if (!ifClass.getPackage().getName().equals(
                        PCG_PREF_JAVAX_OSS.substring(0, PCG_PREF_JAVAX_OSS.length() - 1))) {
                    getImplementation(ifClass);
                }
            } catch (GenericSystemException | IneIllegalArgumentException e) {
                logger.log(Level.FINE, "Interface is not supported: {0}", ifClass);
            }
        }

        Class<? extends ObjectAccess> clazz;
        if (!CBE_MAP.containsKey(targetClass)) {
            synchronized (CBE_MAP) {
                if (!CBE_MAP.containsKey(targetClass)) {
                    clazz = counstructOssJClass(targetClass, true);
                    CBE_MAP.put(targetClass, clazz);
                }
            }
        }

        clazz = CBE_MAP.get(targetClass);

        return clazz.newInstance();
    }

    /**
     * Получает акцесс для классов-маркеров
     *
     * @param targetClass источник
     * @return акцесс
     * @throws IllegalAccessException при недоступности класса в JVM
     * @throws InstantiationException если тип класса не позволяет создавать новые инстансы
     * @throws ru.xr.ine.core.GenericSystemException
     *                                при возникновении ошибки в процессе поиска и инстанционирования
     */
    private static ObjectAccess getImplForSysMarker(Class<? extends SystemMarker> targetClass)
            throws GenericSystemException, InstantiationException, IllegalAccessException {

        Class<? extends ObjectAccess> clazz;
        if (!SM_MAP.containsKey(targetClass)) {
            synchronized (SM_MAP) {
                if (!SM_MAP.containsKey(targetClass)) {
                    clazz = counstructIneClass(targetClass, true);
                    SM_MAP.put(targetClass, clazz);
                }
            }
        }

        clazz = SM_MAP.get(targetClass);

        return clazz.newInstance();
    }

    /**
     * Получает акцесс для основных классов-контейнеров
     *
     * @param targetClass источник
     * @return акцесс
     * @throws IllegalAccessException при недоступности класса в JVM
     * @throws InstantiationException если тип класса не позволяет создавать новые инстансы
     * @throws ru.xr.ine.core.GenericSystemException
     *                                при возникновении ошибки в процессе поиска и инстанционирования
     */
    private static ObjectAccess getImplForIdentifiable(Class<? extends Identifiable> targetClass)
            throws GenericSystemException, InstantiationException, IllegalAccessException {

        Class<? extends ObjectAccess> clazz;
        if (!MAP.containsKey(targetClass)) {
            synchronized (MAP) {
                if (!MAP.containsKey(targetClass)) {
                    clazz = counstructIneClass(targetClass, true);
                    MAP.put(targetClass, clazz);
                }
            }
        }

        clazz = MAP.get(targetClass);

        return clazz.newInstance();
    }

    /**
     * Конструирует класс
     *
     * @param systemClass источник для преобразования имени
     * @param inject      признак необходимости внедрять код
     * @return датаакцесс
     * @throws ru.xr.ine.core.GenericSystemException
     *          при невозможности сконструировать класс
     */
    private static Class<? extends ObjectAccess> counstructIneClass(Class systemClass, boolean inject)
            throws GenericSystemException {

        String packageName = systemClass.getPackage().getName();

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Construct for: " + systemClass.getName());
        }
        String targetPackage = PCG_PREF_RU_XR_INE + PCG_PREF_DBE_DA
                + packageName.substring(PCG_PREF_RU_XR_INE.length());

        return constructClass(targetPackage + DOT + systemClass.getSimpleName(), inject);
    }

    /**
     * Конструирует класс
     *
     * @param systemClass источник для преобразования имени
     * @param inject      признак необходимости внедрять код
     * @return датаакцесс
     * @throws ru.xr.ine.core.GenericSystemException
     *          при невозможности сконструировать класс
     */
    private static Class<? extends ObjectAccess> counstructOssJClass(Class systemClass, boolean inject)
            throws GenericSystemException {

        String packageName = systemClass.getPackage().getName();

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Construct for: " + systemClass.getName());
        }
        String targetPackage = PCG_PREF_RU_XR_INE + PCG_PREF_DBE_DA
                + packageName.substring(PCG_PREF_JAVAX_OSS.length());

        return constructClass(targetPackage + DOT + systemClass.getSimpleName(), inject);
    }

    /**
     * Конструирует класс
     *
     * @param classNamePref префикс - имя класса для конструиро
     * @param inject        признак необходимости внедрять код
     * @return датаакцесс
     * @throws ru.xr.ine.core.GenericSystemException
     *          при невозможности сконструировать класс
     */
    @SuppressWarnings({"unchecked"})
    private static Class<? extends ObjectAccess> constructClass(String classNamePref, boolean inject)
            throws GenericSystemException {

        Class result;

        String fullClassName = classNamePref + ACCESS_IMPL_SFX;
        if (inject) {
            result = obtainPreparedClass(fullClassName);
        } else {
            try {
                result = Class.forName(fullClassName);
            } catch (ClassNotFoundException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

        return (Class<? extends ObjectAccess>) result;
    }

    /**
     * Конструирует класс и встраивает необходимый код
     *
     * @param className имя класса
     * @return датаакцесс
     * @throws ru.xr.ine.core.GenericSystemException
     *          при невозможности сконструировать класс
     */
    private static synchronized Class obtainPreparedClass(String className) throws GenericSystemException {

        Class result;

        if (className == null || className.length() < 1) {
            throw new IneIllegalArgumentException("Class name can't be null or zero length");
        }

        logger.log(Level.FINE, "Start injection of registrator for: {0}", className);

        CtClass ctClass;
        try {
            ctClass = classPool.get(className);
        } catch (NotFoundException e) {
            throw GenericSystemException.toGeneric("Can't find class by name: " + className, e);
        }

        @SuppressWarnings({"unchecked"}) Collection<String> refClasses = ctClass.getRefClasses();
        refClasses.remove(className);
        processRefClasses(refClasses);

        if (!ctClass.isFrozen() && !Modifier.isAbstract(ctClass.getModifiers())
                && !className.contains("SystemObjectAccessImpl")) {
            processMethod(ctClass, CrudCode.create);
            processMethod(ctClass, CrudCode.update);
            processMethod(ctClass, CrudCode.delete);

            try {
                result = ctClass.toClass();

                for (CtClass aClass : ctClass.getNestedClasses()) {
                    aClass.toClass();
                }

                return result;
            } catch (CannotCompileException | NotFoundException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

        try {
            result = Class.forName(className);
        } catch (ClassNotFoundException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return result;
    }

    /**
     * Проверяем все переданные импорты на наличие других акцессов. Если акцессы есть, то в них тоже надо инжектить
     * код.
     *
     * @param refClasses список импортов
     * @throws GenericSystemException в случае ошибок загрузки классов в JVM
     */
    private static void processRefClasses(Collection<String> refClasses) throws GenericSystemException {

        // убираем из списка все классы, не являющиеся акцессами
        for (Iterator<String> iterator = refClasses.iterator(); iterator.hasNext(); ) {
            String refClass = iterator.next();
            if (!(refClass.startsWith(PCG_PREF_RU_XR_INE + PCG_PREF_DBE_DA) && refClass.endsWith(ACCESS_IMPL_SFX))) {
                iterator.remove();
            }
        }

        // обрабатываем оставшиеся акцессы
        for (String refClass : refClasses) {
            // если класс содержится в одной из мап, то мы его уже обрабатывали. иначе инжектим код и подгружаем его
            // в JVM, чтобы он уже был, а в соответствующую мапу попадёт при первом своём вызове.
            if (!contains(MAP.values(), refClass) && !contains(CBE_MAP.values(), refClass)
                    && !contains(SM_MAP.values(), refClass)) {
                try {
                    if (!Modifier.isAbstract(classPool.get(refClass).getModifiers())) {
                        obtainPreparedClass(refClass);
                    }
                } catch (NotFoundException e) {
                    throw GenericSystemException.toGeneric("Can't find class by name: " + refClass, e);
                }
            }
        }
    }

    /**
     * Проверяем наличие в коллекции класса с заданным именем. Проверяем именно так, чтобы не подгружать класс в JVM.
     *
     * @param classes   коллекция акцессов
     * @param className проверяемое имя
     * @return <code>true</code> если класс с заданным именем есть в коллекции, <code>false</code> иначе.
     */
    private static boolean contains(Collection<Class<? extends ObjectAccess>> classes, String className) {
        for (Class<? extends ObjectAccess> clazz : classes) {
            if (clazz.getName().equals(className)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Инжектим код в метод метод по модификации объекта.
     *
     * @param ctClass  класс, для которого обрабатываем метод
     * @param crudCode тип модифицируемого метода. Метод получается добавлением строки "Object" к типу метода. Например,
     *                 если возьмём тип {@link CrudCode#create}, то будет искаться метод
     *                 createObject.
     */
    private static void processMethod(CtClass ctClass, CrudCode crudCode) {
        try {
            CtMethod ctMethod = ctClass.getDeclaredMethod(crudCode + "Object");
            if (!ctMethod.hasAnnotation(CRUDIgnore.class)) {
                ctMethod.insertBefore("checkPermissions(identifiable, \"" + crudCode + "\");\n");
                ctMethod.insertAfter("constructAction(\"" + crudCode + "\"); registerAction(identifiable);\n");
            }
        } catch (CannotCompileException | NotFoundException e) {
            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, e.getMessage());
            }
        }
    }

    /**
     * Проверяет наличие инициализированного пользователя
     *
     * @throws ru.xr.ine.core.IneIllegalAccessException
     *          если пользователь не установлен
     */
    private static void checkUserIsSet() throws IneIllegalAccessException {
        boolean success = false;
        UserProfile userProfile = UserHolder.getUser();

        if (userProfile != null) {
            SystemUser systemUser = userProfile.getSystemUser();
            success = systemUser != null && systemUser.isActive();
        }

        if (!success) {
            throw new IneIllegalAccessException("User not initialised");
        }
    }

}
