/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.oss.entity.Company;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class CompanyBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testCompanyBeanLocal() {
        try {
            //проверяемый checkedBean
            CompanyBean<Company> checkedBean = new CompanyBeanImpl<Company>();
            //получение пустого объекта
            Company company = IdentifiableFactory.getImplementation(Company.class);
            //наполнение тестовыми данными
            company.setCoreDsc("Description");
            company.setName("Name");
            company.setType(BigDecimal.ONE);
            company.setPropertyType(BigDecimal.ONE);

            //создание
            BigDecimal createdId = checkedBean.createObject(user, company).getCoreId();
            //получение и проверка данных
            company = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", company.getName());

            //обновление
            Company cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            company = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", company.getName());

            //удаление
            checkedBean.deleteObject(user, company);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Company object");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
