/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.constants.ParametrizedConstantValue;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует
 * {@link ru.effts.ine.ejbs.core.constants.ParametrizedConstantValueBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "ParametrizedConstantValueBean", mappedName = "stateless.ParametrizedConstantValueBean")
@Remote(ParametrizedConstantValueBean.class)
@SuppressWarnings({"unchecked"})
public class ParametrizedConstantValueBeanImpl<T extends ParametrizedConstantValue> extends VersionableBeanImpl<T>
        implements ParametrizedConstantValueBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ParametrizedConstantValue.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
