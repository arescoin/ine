/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.funcswitch.FuncSwitch;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public class FuncSwitchDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            FuncSwitchDelegate<FuncSwitch> funcSwitchDelegate =
                    DelegateFactory.obtainDelegateByInterface(FuncSwitch.class);

            FuncSwitch funcSwitch = IdentifiableFactory.getImplementation(FuncSwitch.class);
            funcSwitch.setCoreDsc("Description");
            funcSwitch.setEnabled(true);
            funcSwitch.setChangeDate(new Date());
            funcSwitch.setMark(BigDecimal.ONE);
            funcSwitch.setStateCode("test");
            funcSwitch.setCoreId(BigDecimal.ONE);

            FuncSwitch created = funcSwitchDelegate.createObject(user, funcSwitch);

            SyntheticId id = funcSwitchDelegate.getSyntheticId(user, created);
            funcSwitch = funcSwitchDelegate.getObjectById(user, id.getIdValues());
            Assert.assertEquals("test", funcSwitch.getStateCode());
            funcSwitch.setStateCode("New test");

            funcSwitch = funcSwitchDelegate.updateObject(user, funcSwitch);

            Assert.assertEquals("New test", funcSwitch.getStateCode());

            //проверка что нельзя обновить устаревшей версией
            try {
                created.setCoreDsc("");
                funcSwitchDelegate.updateObject(user, created);
                Assert.fail();
            } catch (Exception ignored) {
            }

            funcSwitchDelegate.deleteObject(user, funcSwitch);
            if (funcSwitchDelegate.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove FuncSwitch");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
