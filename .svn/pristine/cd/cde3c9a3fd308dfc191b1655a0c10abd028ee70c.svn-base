/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.constants;

import ru.effts.ine.core.*;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueAccessImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ConstantValueAccessImpl<T extends ConstantValue>
        extends AbstractVersionableAccess<T> implements ConstantValueAccess<T> {

    /** Название таблицы со списком значений констант */
    public static final String TABLE = "CONSTANT_LIST";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_CONST_N = "const_n";
    public static final String COLUMN_CONST_VAL = "const_val";

    /** Запрос на выборку */
    public static final String ALL_CONST_VALS_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_CONST_N +
            ", " + TABLE_PREF + COLUMN_CONST_VAL +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.constants.ConstantValue} */
    public static final String CACHE_REGION_CONSTANT_VALUSES = ConstantValue.class.getName();

    static {
        setInstanceFieldsMapping(ConstantValueAccessImpl.class, new HashMap<String, String>());

        setIdColumns(ConstantValueAccessImpl.class, new String[]{COLUMN_CONST_N});

        registerConstantValueHelpers(ConstantValueAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {

        Map<String, String> map = new HashMap<String, String>();
        map.put(ConstantValue.CORE_ID, COLUMN_CONST_N);
        map.put(ConstantValue.VALUE, COLUMN_CONST_VAL);
        map.putAll(instanceFields);
        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerConstantValueHelpers(Class<? extends ConstantValueAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<CRUDHelper>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    @Override
    public String getTableName() {
        return ConstantValueAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_CONSTANT_VALUSES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(ConstantValue.class);

        this.setVersionableFields(resultSet, result, COLUMN_CONST_N);
        result.setValue(this.getString(resultSet, COLUMN_CONST_VAL));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_CONSTANT_VALUSES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_CONSTANT_VALUSES, ALL_CONST_VALS_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ConstantValue.class;
    }

}
