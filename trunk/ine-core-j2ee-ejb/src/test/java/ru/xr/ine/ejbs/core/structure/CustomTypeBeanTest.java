/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomTypeBeanTest extends BaseTest {

    private static final String CORE_DSC = "CustomType CRUD-test via bean-local";
    private static final String TYPE_NAME = "BeanTest TypeName";
    private BigDecimal testObjId;

    @Before
    public void init() throws Exception {
        testObjId = FieldAccess.getTableId(TestObject.class);
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void testCustomBeanLocal() {
        try {
            //проверяемый bean
            CustomTypeBean<CustomType> checkedBean = new CustomTypeBeanImpl<CustomType>();

            //исходное количество модифицированных типов
            int before = checkedBean.getTypesForBaseObject(user, testObjId).size();

            //создание нового модифицированного типа
            CustomType customType = IdentifiableFactory.getImplementation(CustomType.class);
            customType.setCoreDsc(CORE_DSC);
            customType.setBaseObject(testObjId);
            customType.setTypeName(TYPE_NAME);
            Collection<CustomAttribute> col = new ArrayList<CustomAttribute>();
            //создание описания настраиваемого атрибута и добавление его в модифицированный тип
            CustomAttribute objectAttr = IdentifiableFactory.getImplementation(CustomAttribute.class);
            objectAttr.setCoreDsc(CORE_DSC);
            objectAttr.setSystemObjectId(testObjId);
            objectAttr.setAttributeName("CustomTypeBeanTest AttributeName");
            objectAttr.setDataType(DataType.stringType);
            objectAttr.setRequired(false);
            col.add(objectAttr);
            customType.setCustomAttributes(col);
            customType = checkedBean.createObject(user, customType);

            //проверяем что объект создан и есть в кеше
            Assert.assertEquals(before + 1, checkedBean.getTypesForBaseObject(user, testObjId).size());

            //обновление
            CustomType cUpdated = checkedBean.getObjectById(user, customType.getCoreId());
            Assert.assertEquals(TYPE_NAME, cUpdated.getTypeName());
            cUpdated.setCoreDsc(CORE_DSC + " updated");
            checkedBean.updateObject(user, cUpdated);
            customType = checkedBean.getObjectById(user, customType.getCoreId());
            Assert.assertEquals(CORE_DSC + " updated", customType.getCoreDsc());
            try {
                customType.setBaseObject(BigDecimal.ONE);
                Assert.fail();
            } catch (Exception e) {
                Assert.assertEquals("baseObject has already been set", e.getMessage());
            }

            //удаление модифицированного типа и описания настраиваемого атрибута
            checkedBean.deleteObject(user, customType);
            if (checkedBean.getObjectById(user, customType.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove CustomType");
            }
            Assert.assertEquals(before, checkedBean.getTypesForBaseObject(user, testObjId).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
