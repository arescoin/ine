/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RoleBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RoleBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testRoleBeanLocal() {
        try {
            //проверяемый bean
            RoleBean<Role> checkedBean = new RoleBeanImpl<Role>();
            //получение пустого объекта
            Role role = IdentifiableFactory.getImplementation(Role.class);
            //наполнение тестовыми данными
            role.setCoreDsc("Description");
            role.setName("Name");

            //создание
            BigDecimal createdId = checkedBean.createObject(user, role).getCoreId();
            //получение и проверка данных
            role = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", role.getName());

            //обновление
            Role cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            role = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", role.getName());

            //удаление
            checkedBean.deleteObject(user, role);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove RoleBean");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
