/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.utils.BaseTest;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс для тестирования соответствующего EJB
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryEntryBeanTest extends BaseTest {

    @Test
    public void testDictionaryEntryBeanLocal() {
        try {
            DictionaryEntryBean<DictionaryEntry> bean = new DictionaryEntryBeanImpl<DictionaryEntry>();
            Collection<DictionaryEntry> entries = bean.getAllEntriesForDictionary(user, BigDecimal.ONE);

            DictionaryEntry dicEntry = entries.iterator().next();
            try {
                bean.createObject(user, dicEntry);
            } catch (EJBException e) {
                Assert.assertTrue(e.getCausedByException() instanceof UnsupportedOperationException);
            }
            try {
                bean.updateObject(user, dicEntry);
            } catch (EJBException e) {
                Assert.assertTrue(e.getCausedByException() instanceof UnsupportedOperationException);
            }
            int before = entries.size();
            dicEntry.setCoreId(new BigDecimal(9999));
            bean.deleteObject(user, dicEntry);
            Assert.assertEquals(before, bean.getAllEntriesForDictionary(user, BigDecimal.ONE).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
