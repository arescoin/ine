/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testConstantBeanLocal() {
        try {
            ConstantBean<Constant> bean = new ConstantBeanImpl<Constant>();
            
            //создание нового описания константы
            Constant constant = IdentifiableFactory.getImplementation(Constant.class);
            constant.setCoreDsc("Description");
            constant.setName("TestConstant");
//            constant.setType(BigDecimal.ONE);
            constant.setModifiable(true);
            constant.setNullable(true);
            constant.setDefaultValue("Test");
            BigDecimal createdId = bean.createObject(user, constant).getCoreId();

            //получение созданной константы и проверка данных  в ней
            constant = bean.getObjectById(user, createdId);
            Assert.assertEquals("TestConstant", constant.getName());

            //обновление
            Constant cUpdated = bean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            bean.updateObject(user, cUpdated);
            //получение обновленной константы и проверка данных  в ней
            constant = bean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", constant.getName());

            //удаление
            bean.deleteObject(user, constant);
            if (bean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Constant");
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testFields() {
        //проврека количества собственных полей в константе
        try {
            ConstantBean<Constant> bean = new ConstantBeanImpl<Constant>();
            Assert.assertEquals(8, bean.getInstanceFields(user).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
