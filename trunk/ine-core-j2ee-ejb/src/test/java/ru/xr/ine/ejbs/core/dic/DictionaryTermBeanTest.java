/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.utils.BaseTest;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class DictionaryTermBeanTest extends BaseTest {

    private static final BigDecimal RU = BigDecimal.ONE;
    private static final BigDecimal EN = new BigDecimal(2);

    @Test
    public void testDictionaryTermBeanLocal() {
//        final BigDecimal dictionaryId = BigDecimal.ONE;
        try {

            DictionaryBean<Dictionary> dictionaryBean = new DictionaryBeanImpl<Dictionary>();
            //создание нового словаря
            Dictionary dictionary = IdentifiableFactory.getImplementation(Dictionary.class);
            dictionary.setCoreDsc("Description");
            dictionary.setDicType(EN);
            dictionary.setEntryCodePolicy(BigDecimal.ONE);
            dictionary.setLocalizationPolicy(EN);
            dictionary.setName("TEST_DTB");
            BigDecimal createdId = dictionaryBean.createObject(user, dictionary).getCoreId();

            DictionaryTermBean<DictionaryTerm> bean = new DictionaryTermBeanImpl<DictionaryTerm>();
            //исходное количество терминов
            int before = bean.getAllTermsForDictionaryAndLanguage(user, createdId, EN).size();

            //создание нового
            DictionaryTerm dicTerm = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            dicTerm.setDictionaryId(createdId);
            dicTerm.setLangCode(EN);
            dicTerm.setTerm("TestTerm for bean test 1");
            dicTerm.setCoreDsc("Description");
            DictionaryTerm created = bean.createObject(user, dicTerm);

            //получение созданного и проверка данных
            SyntheticId id = bean.getSyntheticId(user, created);
            Assert.assertEquals(before + 1, bean.getAllTermsForDictionaryAndLanguage(user, createdId, EN).size());
            dicTerm = bean.getObjectById(user, id.getIdValues());
            Assert.assertEquals("TestTerm for bean test 1", dicTerm.getTerm());

            //обновление
            dicTerm.setTerm("New TestTerm for bean test 1");
            DictionaryTerm dicTermUpdated = bean.updateObject(user, dicTerm);
            Assert.assertEquals("New TestTerm for bean test 1", dicTermUpdated.getTerm());

            //удаление
            bean.deleteObject(user, dicTermUpdated);
            Assert.assertEquals(before, bean.getAllTermsForDictionaryAndLanguage(user, createdId, EN).size());
            if (bean.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove DictionaryTerm");
            }

            dictionaryBean.deleteObject(user, dictionary);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testCreateFullyFillDicTerms() {
        final BigDecimal dictionaryId = new BigDecimal(8);
        try {
            DictionaryTermBean<DictionaryTerm> bean = new DictionaryTermBeanImpl<DictionaryTerm>();

            int termsRU = bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, RU).size();
            int termsEN = bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size();

            Collection<DictionaryTerm> terms = new ArrayList<DictionaryTerm>();

            DictionaryTerm term = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            term.setDictionaryId(dictionaryId);
            term.setLangCode(RU);
            term.setTerm("TestTerm for bean test 2");
            terms.add(term);

            try {
                bean.createDictionaryTerms(user, terms);
            } catch (EJBException e) {
                Assert.assertTrue("Failed to catch expected exception",
                        e.getCausedByException() instanceof GenericSystemException);
            }

            term = IdentifiableFactory.getImplementation((DictionaryTerm.class));
            term.setDictionaryId(dictionaryId);
            term.setLangCode(EN);
            term.setTerm("TestTerm for bean test 2");
            terms.add(term);

            BigDecimal newCode = bean.createDictionaryTerms(user, terms);

            Assert.assertEquals("Failed to create dictionary terms", termsRU + 1,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, RU).size());
            Assert.assertEquals("Failed to create dictionary terms", termsEN + 1,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size());

            try {
                bean.deleteObject(user, term);
            } catch (EJBException e) {
                Assert.assertTrue("Failed to catch expected exception. Need manual delete of dictionary term[" +
                        newCode + "] in dictionary " + dictionaryId,
                        e.getCausedByException() instanceof GenericSystemException);
            }
            new DictionaryEntryBeanImpl<DictionaryEntry>().deleteObject(user, term);

            Assert.assertEquals("Failed to delete dictionary terms. Need manual delete of dictionary term[" +
                    newCode + "] in dictionary " + dictionaryId, termsRU,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, RU).size());
            Assert.assertEquals("Failed to delete dictionary terms. Need manual delete of dictionary term[" +
                    newCode + "] in dictionary " + dictionaryId, termsEN,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size());

        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testAddTerm() {
        final BigDecimal dictionaryId = BigDecimal.ONE;
        try {
            DictionaryTermBean<DictionaryTerm> bean = new DictionaryTermBeanImpl<DictionaryTerm>();

            DictionaryTerm termRU = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            termRU.setDictionaryId(dictionaryId);
            termRU.setLangCode(RU);
            termRU.setTerm("TestTerm for bean test 3");

            termRU = bean.createObject(user, termRU);

            Assert.assertEquals("Failed to create dictionary term", 1,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, termRU.getCoreId()).size());

            DictionaryTerm termEN = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            termEN.setCoreId(termRU.getCoreId());
            termEN.setDictionaryId(dictionaryId);
            termEN.setLangCode(EN);
            termEN.setTerm("TestTerm for bean test 3");

            termEN = bean.addObject(user, termEN);

            Assert.assertEquals("Failed to add dictionary term", 2,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, termEN.getCoreId()).size());

            new DictionaryEntryBeanImpl<DictionaryEntry>().deleteObject(user, termRU);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testAddTerms() {
        final BigDecimal dictionaryId = BigDecimal.ONE;
        try {
            DictionaryTermBean<DictionaryTerm> bean = new DictionaryTermBeanImpl<DictionaryTerm>();

            Collection<DictionaryTerm> terms = new ArrayList<DictionaryTerm>();

            DictionaryTerm term = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            term.setDictionaryId(dictionaryId);
            term.setLangCode(RU);
            term.setTerm("TestTerm for bean test 4");
            terms.add(term);

            BigDecimal newCode = bean.createDictionaryTerms(user, terms);

            Assert.assertEquals("Failed to create dictionary terms", 1,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, term.getCoreId()).size());

            term = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            term.setCoreId(newCode);
            term.setDictionaryId(dictionaryId);
            term.setLangCode(EN);
            term.setTerm("TestTerm for bean test 4");
            terms.add(term);

            bean.addDictionaryTerms(user, terms);

            Assert.assertEquals("Failed to add dictionary term", 2,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, term.getCoreId()).size());

            new DictionaryEntryBeanImpl<DictionaryEntry>().deleteObject(user, term);
        } catch (Exception e) {
            fail(e);
        }
    }
}
