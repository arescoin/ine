/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.UserRole;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserRoleBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testUserRoleBeanLocal() {
        try {
            //проверяемый bean
            UserRoleBean<UserRole> checkedBean = new UserRoleBeanImpl<UserRole>();
            //получение пустого объекта
            UserRole userRole = IdentifiableFactory.getImplementation(UserRole.class);
            //наполнение тестовыми данными
            userRole.setCoreId(new BigDecimal(2));
            userRole.setCoreDsc("Description");
            userRole.setActive(true);
            userRole.setUserId(new BigDecimal(2));

            //создание
            userRole = checkedBean.createObject(user, userRole);
            //получение и проверка данных
            SyntheticId createdId = checkedBean.getSyntheticId(user, userRole);
            userRole = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(true, userRole.isActive());

            //обновление
            UserRole cUpdated = checkedBean.getObjectById(user, createdId.getIdValues());
            cUpdated.setActive(false);
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            userRole = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(false, userRole.isActive());

            //удаление
            checkedBean.deleteObject(user, userRole);
            if (checkedBean.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove UserRole");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
