/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.utils.BaseTest;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class SystemObjectBeanTest extends BaseTest {

    @Test
    public void testInterfaces() {
        try {
            //проверяемый bean
            SystemObjectBean<SystemObject> bean = (SystemObjectBean<SystemObject>) new SystemObjectBeanImpl();
            Assert.assertTrue(bean.getSupportedIntefaces(user).contains(TestObject.class.getName()));
        } catch (Exception e) {
            fail(e);
        }
    }
}
