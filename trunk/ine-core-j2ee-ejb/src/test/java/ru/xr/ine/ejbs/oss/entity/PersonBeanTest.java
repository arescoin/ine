/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.oss.entity.Person;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PersonBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testPersonBeanLocal() {

        try {
            //проверяемый bean
            PersonBean<Person> checkedBean = new PersonBeanImpl<Person>();
            //получение пустого объекта
            Person person = IdentifiableFactory.getImplementation(Person.class);
            //наполнение тестовыми данными
            person.setCoreDsc("Description");
            person.setName("Name");
            person.setFamilyName("familyName");
            person.setMiddleName("middleName");
            person.setAlias("alias");
            person.setFamilyNamePrefixCode(BigDecimal.ONE);
            person.setFamilyGeneration("familyGeneration");
            person.setFormOfAddress("formOfAddress");

            //создание
            BigDecimal createdId = checkedBean.createObject(user, person).getCoreId();
            //получение и проверка данных
            person = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", person.getName());

            //обновление
            Person cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            person = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", person.getName());

            //удаление
            checkedBean.deleteObject(user, person);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Person object");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
