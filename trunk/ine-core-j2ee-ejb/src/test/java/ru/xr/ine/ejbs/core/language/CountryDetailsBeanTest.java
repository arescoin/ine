/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.ConstantValue;
import ru.xr.ine.core.language.CountryDetails;
import ru.xr.ine.ejbs.core.constants.ConstantValueBean;
import ru.xr.ine.ejbs.core.constants.ConstantValueBeanImpl;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CountryDetailsBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testCountryDetailsBeanLocal() {
        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4);
        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");

            //проверяемый bean
            CountryDetailsBean<CountryDetails> checkedBean = new CountryDetailsBeanImpl<CountryDetails>();

            //создание
            CountryDetails countryDetails = IdentifiableFactory.getImplementation(CountryDetails.class);
            countryDetails.setCoreId(BigDecimal.valueOf(8888l));
            countryDetails.setCodeA2("YY");
            countryDetails.setCodeA3("YY1");
            countryDetails.setCoreDsc("Description");
            countryDetails.setName("TestCountryDetails");
            //получение созданного и проверка данных
            BigDecimal createdId = checkedBean.createObject(user, countryDetails).getCoreId();
            countryDetails = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("TestCountryDetails", countryDetails.getName());

            //обновление
            CountryDetails cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение обновленного и проверка данных
            countryDetails = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", countryDetails.getName());

            //удаление
            checkedBean.deleteObject(user, countryDetails);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove CountryDetails");
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            ConstantValueBean<ConstantValue> bean = new ConstantValueBeanImpl<ConstantValue>();
            ConstantValue constantValue = bean.getObjectById(user, constantId);
            constantValue.setValue(value);
            bean.updateObject(user, constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }
}
