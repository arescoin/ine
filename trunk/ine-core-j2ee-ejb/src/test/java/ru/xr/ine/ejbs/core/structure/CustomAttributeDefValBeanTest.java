/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.structure.CustomAttributeDefVal;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomAttributeDefValBeanTest extends BaseTest {

    @Test
    public void testCustomAttributeDefValBeanLocal() {
        BigDecimal id = new BigDecimal(5);

        try {
            CustomAttributeDefValBean<CustomAttributeDefVal> bean =
                    new CustomAttributeDefValBeanImpl<CustomAttributeDefVal>();

            CustomAttributeDefVal object = IdentifiableFactory.getImplementation(CustomAttributeDefVal.class);
            object.setCoreId(id);
            object.setValueByType(Boolean.TRUE, DataType.booleanType);
            bean.createObject(user, object);
            object = bean.getObjectById(user, id);
            Assert.assertEquals(Boolean.TRUE, object.getValueByType(DataType.booleanType));

            object.setValueByType(Boolean.FALSE, DataType.booleanType);
            bean.updateObject(user, object);
            object = bean.getObjectById(user, id);
            Assert.assertEquals(Boolean.FALSE, object.getValueByType(DataType.booleanType));

            bean.deleteObject(user, object);
            if (bean.getObjectById(user, id) != null) {
                Assert.fail("method deleteObject() has failed to remove CustomAttributeDefVal");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
