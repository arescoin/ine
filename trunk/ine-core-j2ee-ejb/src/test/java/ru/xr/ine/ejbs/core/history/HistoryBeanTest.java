/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.core.history.VersionableHistory;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.core.structure.test.TestObjectAccess;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: HistoryBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class HistoryBeanTest extends BaseTest {

    private BigDecimal typeId;
    private TestObjectAccess<TestObject> access;

    @SuppressWarnings({"unchecked"})
    @Before
    public void init() throws Exception {
        typeId = FieldAccess.getTableId(TestObject.class);
        access = (TestObjectAccess<TestObject>) AccessFactory.getImplementation(TestObject.class);
    }

    @Test
    public void testHistoryBeanLocal() {
        try {
            if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS).isEnabled()) {
                return;
            }
            //создание тестового объекта непосредственно через access
            TestObject object = IdentifiableFactory.getImplementation(TestObject.class);
            object.setValue("Test");
            object.setCoreDsc("Description");
            object.setCoreFd(FieldAccess.getCurrentDate());
            object.setCoreTd(FieldAccess.getSystemTd());
            object = access.createObject(object);

            //ьестируемые бины
            IdentifiableHistoryBean<IdentifiableHistory> bean1 = new IdentifiableHistoryBeanImpl<IdentifiableHistory>();
            VersionableHistoryBean<VersionableHistory> bean2 = new VersionableHistoryBeanImpl<VersionableHistory>();
            SyntheticId id = access.getSyntheticId(object);
            //проверяем что запись о создании появилась в истории объекта
            Assert.assertEquals(1, bean1.getActionsHistoryByIds(user, typeId, id).size());
            Assert.assertEquals(1, bean2.getActionsHistoryByIds(user, typeId, id).size());

            TestObject retrieved = IdentifiableFactory.getImplementation(TestObject.class);
            retrieved.setCoreId(object.getCoreId());
            retrieved.setValue(object.getValue());
            retrieved.setCoreTd(object.getCoreTd());
            retrieved.setCoreDsc("");
            retrieved.setCoreFd(FieldAccess.getCurrentDate());
            TestObject updated = access.updateObject(object, retrieved);

            //проверяем что запись об обновлении появилась в истории объекта
            Assert.assertEquals(2, bean1.getActionsHistoryByIds(user, typeId, id).size());
            Collection<VersionableHistory> actions = bean2.getActionsHistoryByIds(user, typeId, id);
            Assert.assertEquals(2, actions.size());
            if (FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_HISTORY).isEnabled()) {
                for (VersionableHistory action : actions) {
                    Assert.assertNotNull(bean2.getObjectVersion(user, action));
                }
            }
            //удаляем
            access.deleteObject(updated);
            //проверяем что запись об удалении появилась в истории объекта
            Assert.assertEquals(3, bean1.getActionsHistoryByIds(user, typeId, id).size());
            Assert.assertEquals(3, bean2.getActionsHistoryByIds(user, typeId, id).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
