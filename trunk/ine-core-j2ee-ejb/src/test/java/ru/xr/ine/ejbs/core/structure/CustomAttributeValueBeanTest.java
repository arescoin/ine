/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomAttributeValue;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.core.structure.test.TestObjectAccess;
import ru.xr.ine.utils.BaseTest;

import javax.ejb.EJBException;
import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueBeanTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomAttributeValueBeanTest extends BaseTest {

    private BigDecimal testObjId;

    @Before
    public void init() throws Exception {
        testObjId = FieldAccess.getTableId(TestObject.class);
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void testCustomAttributeValueBeanLocal() {
        try {
            //создаем описание настраиваемого атрибута
            CustomAttributeBean<CustomAttribute> customAttributeBean = new CustomAttributeBeanImpl<CustomAttribute>();
            CustomAttribute object = IdentifiableFactory.getImplementation(CustomAttribute.class);
            object.setCoreDsc("Description");
            object.setSystemObjectId(testObjId);
            object.setAttributeName("TestCustomAttribute");
            object.setDataType(DataType.stringType);
            object.setRequired(false);
            BigDecimal createdCAId = customAttributeBean.createObject(user, object).getCoreId();

            //создаем тестовый объект
            TestObject objectTest = IdentifiableFactory.getImplementation(TestObject.class);
            objectTest.setValue("Attribute keeper");
            objectTest.setCoreTd(FieldAccess.getSystemTd());
            objectTest.setCoreFd(FieldAccess.getCurrentDate());
            TestObjectAccess<TestObject> access = (TestObjectAccess<TestObject>)
                    AccessFactory.getImplementation(TestObject.class);
            objectTest = access.createObject(objectTest);

            //создаем модифицированный атрибут для тестового объекта
            CustomAttributeValueBean<CustomAttributeValue> customAttributeValueBean =
                    new CustomAttributeValueBeanImpl<CustomAttributeValue>();
            CustomAttributeValue objectValue = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            objectValue.setCoreDsc(null);
            objectValue.setCoreId(createdCAId);
            objectValue.setValue("test");
            objectValue.setEntityId(objectTest.getCoreId());
            objectValue = customAttributeValueBean.createObject(user, objectValue);
            SyntheticId syntheticId = customAttributeValueBean.getSyntheticId(user, objectValue);
            objectValue = customAttributeValueBean.getObjectById(user, syntheticId.getIdValues());
            Assert.assertEquals("test", objectValue.getValue());
            //проверим что можно изменить значение атрибута
            objectValue.setValue("new value");
            customAttributeValueBean.updateObject(user, objectValue);
            CustomAttributeValue objectValueBad = customAttributeValueBean.getObjectById(
                    user, syntheticId.getIdValues());
            Assert.assertEquals("new value", objectValueBad.getValue());
            //проверим что нельзя изменить сущность которой принадлежит модифицированный аттрибут
            try {
                objectValueBad.setEntityId(BigDecimal.ONE);
                customAttributeValueBean.updateObject(user, objectValueBad);
                Assert.fail();
            }
            catch (Exception e) {
            }

            //проверим что нельзя удалить описание
            Assert.assertTrue(customAttributeBean.isCustomAttributeUsed(user, object));
            try {
                customAttributeBean.deleteObject(user, object);
                Assert.fail();
            } catch (EJBException e) {
                IneIllegalArgumentException ee = (IneIllegalArgumentException) e.getCausedByException();
                Assert.assertEquals("Attribute description can not be deleted." +
                        " There are some CustomAttributeValue objects of this CustomAttribute type", ee.getMessage());
            } catch (Exception e) {
                fail(e);
            }

            //удаляем созданное
            customAttributeValueBean.deleteObject(user, objectValue);
            access.deleteObject(objectTest);
            customAttributeBean.deleteObject(user, object);
        } catch (Exception e) {
            fail(e);
        }
    }
}
