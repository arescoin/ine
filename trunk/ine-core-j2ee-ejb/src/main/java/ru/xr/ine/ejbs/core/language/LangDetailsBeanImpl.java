/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.language.LangDetails;
import ru.xr.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link LangDetailsBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "LangDetailsBean", mappedName = "stateless.LangDetailsBean")
@Remote(LangDetailsBean.class)
public class LangDetailsBeanImpl<T extends LangDetails> extends IdentifiableBeanImpl<T>
        implements LangDetailsBean<T> {
    
    @Override
    public void init() throws GenericSystemException {
        init(LangDetails.class);
    }
}
