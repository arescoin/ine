/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.dbe.da.core.structure.PatternUtils;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link SystemObjectPatternBean}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectPatternBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "SystemObjectPatternBean", mappedName = "stateless.SystemObjectPatternBean")
@Remote(SystemObjectPatternBean.class)
public class SystemObjectPatternBeanImpl implements SystemObjectPatternBean {

    @Override
    public SystemObjectPattern checkPattern(UserProfile user, SystemObjectPattern pattern) throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.checkPattern(pattern);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Identifiable getObjectByPattern(UserProfile user, SystemObjectPattern pattern, BigDecimal value)
            throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getObjectByPattern(pattern, value);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<Identifiable> getObjectsByPattern(
            UserProfile user, SystemObjectPattern pattern, BigDecimal value) throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getObjectsByPattern(pattern, value);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<Identifiable> getAvailableObjects(UserProfile user, SystemObjectPattern pattern)
            throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getAvailableObjects(pattern);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Map<SystemObjectPattern, BigDecimal> getPatternByObject(UserProfile user, Identifiable object)
            throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getPatternByObject(object);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
