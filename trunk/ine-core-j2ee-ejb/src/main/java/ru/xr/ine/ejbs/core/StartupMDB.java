/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

import ru.xr.ine.core.GenericSystemException;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.*;
import javax.naming.InitialContext;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean слушает командную {@link Queue очередь} <code>(mappedName = "IneStartupQueue")</code>.<br>
 * При получении {@link SystemState#START_CMD команды на запуск} достаёт получает
 * {@link SystemState#getInterfacesToProceed()} порцию интерфейсов}, для которых нужно загрузить данные,
 * и рассылает задания в {@link Topic топик} <code>(mappedName = "InECommandTopic")</code>.<br>
 * Если список пуст, значит обработаны все интерфейсы и надо выставить состояние запуска системы.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: StartupMDB.java 29 2017-04-04 15:32:19Z xerror $"
 */
@MessageDriven(mappedName = "IneStartupQueue")
public class StartupMDB implements MessageListener {

    private static final Logger logger = Logger.getLogger(StartupMDB.class.getName());

    @Resource(mappedName = "jms/InETopicCF")
    private TopicConnectionFactory topicConnectionFactory;

    @Resource(mappedName = "InECommandTopic")
    private Topic commandTopic;

    @Override
    public void onMessage(Message message) {
        synchronized (SystemBeanImpl.systemState) {
            try {
                if (message instanceof TextMessage) {
                    String cmd = ((TextMessage) message).getText();
                    if (cmd.equals(SystemState.START_CMD)) {

                        // Получаем порцию интерфейсов, для которых нужно загрузить данные
                        Collection<String> interfaces = SystemBeanImpl.systemState.getInterfacesToProceed();
                        logger.log(Level.INFO, "Processing interfaces: {0}", interfaces.toString());
                        if (!interfaces.isEmpty()) {
                            // Если список не пустой, рассылаем задания на обработку
                            this.processInterfaces(interfaces);
                        } else {
                            // Если же пустой, значит всё обработали и нужно выставить состояние загрузки системы
                            SystemBeanImpl.systemState.setState();

                            logger.log(Level.INFO, "System state: {0}", SystemBeanImpl.systemState.getState());
                            if (SystemBeanImpl.systemState.getState() == StartupState.START_FAILED) {
                                logger.log(Level.SEVERE, "System startup failed\n{0}",
                                        SystemBeanImpl.systemState.getErrorMessage());
                            }
                        }
                    } else if (cmd.equals(SystemState.START_CMD_LCL)) {
                        InitialContext context = new InitialContext();
                        SystemBean systemBean = (SystemBean) context.lookup("stateless.SystemBean");
                        systemBean.startSystem();
                        context.close();
                    } else {
                        logger.log(Level.WARNING, "Wrong command received [{0}], but expected [{1}]",
                                new Object[]{cmd, SystemState.START_CMD});
                    }
                } else {
                    logger.log(Level.WARNING, "Wrong message type [{0}], but expected [{1}]",
                            new Object[]{message.getJMSType(), TextMessage.class.getName()});
                }
            } catch (Throwable e) {
                logger.log(Level.WARNING, e.getMessage(), e);
                SystemBeanImpl.systemState.setState();
            }
        }
    }

    private void processInterfaces(Collection<String> interfaces) throws GenericSystemException, JMSException {

        TopicConnection topicConnection = topicConnectionFactory.createTopicConnection();
        topicConnection.start();
        TopicSession topicSession = topicConnection.createTopicSession(true, Session.AUTO_ACKNOWLEDGE);
        TopicPublisher topicPublisher = topicSession.createPublisher(commandTopic);

        for (String iFace : interfaces) {
            topicPublisher.publish(topicSession.createTextMessage(iFace));
        }

        topicPublisher.close();
        topicSession.close();
        topicConnection.close();
    }
}
