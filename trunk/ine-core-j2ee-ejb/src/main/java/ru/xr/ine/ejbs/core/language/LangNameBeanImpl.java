/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.language.LangName;
import ru.xr.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link LangNameBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangNameBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "LangNameBean", mappedName = "stateless.LangNameBean")
@Remote(LangNameBean.class)
public class LangNameBeanImpl<T extends LangName> extends IdentifiableBeanImpl<T>
        implements LangNameBean<T> {
    
    @Override
    public void init() throws GenericSystemException {
        init(LangName.class);
    }
}
