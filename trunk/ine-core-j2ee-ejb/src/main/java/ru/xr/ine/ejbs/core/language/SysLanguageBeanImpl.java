/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.language.SysLanguage;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.language.SysLanguageBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "SysLanguageBean", mappedName = "stateless.SysLanguageBean")
@Remote(SysLanguageBean.class)
public class SysLanguageBeanImpl<T extends SysLanguage> extends VersionableBeanImpl<T> implements SysLanguageBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(SysLanguage.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
