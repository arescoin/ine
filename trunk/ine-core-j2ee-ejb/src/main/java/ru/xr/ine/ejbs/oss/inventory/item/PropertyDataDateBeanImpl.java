/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.inventory.item;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;
import ru.xr.ine.oss.inventory.item.PropertyDataDate;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataDateBeanImpl.java 41 2017-04-10 08:25:52Z xerror $"
 */
@Stateless(name = "PropertyDataDateBean", mappedName = "stateless.PropertyDataDateBean")
@Remote(PropertyDataDateBean.class)
public class PropertyDataDateBeanImpl<T extends PropertyDataDate>
        extends VersionableBeanImpl<T> implements PropertyDataDateBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    protected void init() throws GenericSystemException {
        init(PropertyDataDate.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
