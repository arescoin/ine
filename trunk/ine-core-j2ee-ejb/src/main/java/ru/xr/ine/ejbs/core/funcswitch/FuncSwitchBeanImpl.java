/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.funcswitch.FuncSwitch;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link FuncSwitchBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "FuncSwitchBean", mappedName = "stateless.FuncSwitchBean")
@Remote(FuncSwitchBean.class)
public class FuncSwitchBeanImpl<T extends FuncSwitch> extends VersionableBeanImpl<T> implements FuncSwitchBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(FuncSwitch.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
