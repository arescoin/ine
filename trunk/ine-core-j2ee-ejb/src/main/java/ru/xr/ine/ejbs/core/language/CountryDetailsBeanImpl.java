/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.language.CountryDetails;
import ru.xr.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link CountryDetailsBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "CountryDetailsBean", mappedName = "stateless.CountryDetailsBean")
@Remote(CountryDetailsBean.class)
public class CountryDetailsBeanImpl<T extends CountryDetails> extends IdentifiableBeanImpl<T>
        implements CountryDetailsBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(CountryDetails.class);
    }
}
