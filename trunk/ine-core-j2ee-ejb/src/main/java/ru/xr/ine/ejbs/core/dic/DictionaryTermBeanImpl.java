/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import ru.xr.ine.core.*;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.core.language.SysLanguage;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.dic.DictionaryAccess;
import ru.xr.ine.dbe.da.core.dic.DictionaryTermAccess;
import ru.xr.ine.dbe.da.core.language.LangUtils;
import ru.xr.ine.dbe.da.core.language.SysLanguageAccess;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
@Stateless(name = "DictionaryTermBean", mappedName = "stateless.DictionaryTermBean")
@Remote(DictionaryTermBean.class)
public class DictionaryTermBeanImpl<T extends DictionaryTerm>
        extends DictionaryEntryBeanImpl<T> implements DictionaryTermBean<T> {

    private DictionaryAccess<Dictionary> dictionaryAccess;
    private SysLanguageAccess<SysLanguage> sysLangAccess;

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    private static final int ENTRY_POLICY_SIMPLE = 1;
    private static final int ENTRY_POLICY_BINARY = 2;

    private static final int LOCALIZATION_POLICY_FULL = 1;

    @Override
    public void init() throws GenericSystemException {
        init(DictionaryTerm.class);
    }

    @Override
    public Collection<T> getAllTermsForDictionaryAndLanguage(
            UserProfile user, BigDecimal dictionaryId, BigDecimal languageId) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((DictionaryTermAccess<T>) this.getAccess()).
                    getAllTermsForDictionaryAndLanguage(dictionaryId, languageId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<T> getAllTermsForDictionaryAndEntry(UserProfile user, BigDecimal dictionaryId, BigDecimal entryId)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((DictionaryTermAccess<T>) this.getAccess()).getAllTermsForDictionaryAndEntry(dictionaryId, entryId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;

    }

    @Override
    protected void updateChecking(T versionable, T newVersionable) throws GenericSystemException {
        super.updateChecking(versionable, newVersionable);
        if (!versionable.getDictionaryId().equals(newVersionable.getDictionaryId())) {
            throw new GenericSystemException(
                    "Incorrect object modification. DictionaryId can not be modified.");
        }
    }

    @Override
    public T createObject(UserProfile user, T identifiable) throws GenericSystemException {
        this.checkLocalizationPolicy(identifiable.getDictionaryId());
        identifiable.setCoreId(generateId(identifiable.getDictionaryId()));
        return super.createObject(user, identifiable);
    }

    @Override
    public T addObject(UserProfile user, T identifiable) throws GenericSystemException {
        this.checkLocalizationPolicy(identifiable.getDictionaryId());

        return this.addObjectNoCheck(user, identifiable);
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {

        this.checkLocalizationPolicy(identifiable.getDictionaryId());

        try {
            if (identifiable.getLangCode().equals(LangUtils.getSystemLanguageId())) {
                throw new GenericSystemException("Can't delete dictionary term on system language. " +
                        "Use deleteObject method on DictionaryEntryBean to delete all terms.");
            }
        } catch (GenericSystemException e) {
            throw new EJBException(e.getMessage(), e);
        }

        super.deleteObject(user, identifiable);
    }

    private Dictionary getDictionary(BigDecimal dictionaryId) throws GenericSystemException {
        try {
            return this.getDictionaryAccess().getObjectById(dictionaryId);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    private void checkLocalizationPolicy(BigDecimal dictionaryId) {

        try {
            Dictionary dictionary = this.getDictionary(dictionaryId);
            if (dictionary.getLocalizationPolicy().intValue() == LOCALIZATION_POLICY_FULL) {
                throw new GenericSystemException("Method not supported for dictionary[" + dictionary.getCoreId() +
                        "] with localizationPolicy = " + dictionary.getLocalizationPolicy().intValue() +
                        ". Use appropriate method on DictionaryEntryBean.");
            }
        } catch (GenericSystemException e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public BigDecimal createDictionaryTerms(UserProfile user, Collection<T> terms) throws GenericSystemException {

        try {
            if (terms == null || terms.isEmpty()) {
                throw new GenericSystemException("DictionaryTerms not specified");
            }
            UserHolder.setUserProfile(user);
            this.checkDictionary(terms);
            this.checkUpperTerms(terms);
            this.checkLanguages(terms);

            BigDecimal newCode = this.generateId(terms.iterator().next().getDictionaryId());

            for (T term : terms) {
                term.setCoreId(newCode);
                super.createObject(user, term);
            }

            return newCode;
        } catch (EJBException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    private void checkLanguages(Collection<T> terms) throws GenericSystemException {

        Dictionary dictionary = this.getDictionary(terms.iterator().next().getDictionaryId());
        if (dictionary.getLocalizationPolicy().intValue() == LOCALIZATION_POLICY_FULL) {
            Collection sysLangIds = this.getSysLangIds();
            for (T term : terms) {
                if (sysLangIds.contains(term.getLangCode())) {
                    sysLangIds.remove(term.getLangCode());
                } else {
                    throw new GenericSystemException("Unsupported system language: " + term.getLangCode());
                }
            }
            if (!sysLangIds.isEmpty()) {
                throw new GenericSystemException("Localization policy for dictionary " + dictionary.getCoreId() +
                        " requires terms for all system languages. Specify terms for languages: " + sysLangIds);
            }
        }
    }

    @Override
    public void addDictionaryTerms(UserProfile user, Collection<T> terms) throws GenericSystemException {

        try {
            if (terms == null || terms.isEmpty()) {
                throw new GenericSystemException("DictionaryTerms not specified");
            }
            UserHolder.setUserProfile(user);
            this.checkDictionary(terms);
            this.checkLocalizationPolicy(terms.iterator().next().getDictionaryId());
            this.checkUpperTerms(terms);

            for (T term : terms) {
                this.addObjectNoCheck(user, term);
            }
        } catch (EJBException e) {
            throw e;
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    private void checkDictionary(Collection<T> terms) throws GenericSystemException {
        Dictionary dictionary = this.getDictionary(terms.iterator().next().getDictionaryId());
        for (T term : terms) {
            if (!term.getDictionaryId().equals(dictionary.getCoreId())) {
                throw new GenericSystemException("Dictionary terms must be from same dictionary");
            }
        }
    }

    private void checkUpperTerms(Collection<T> terms) throws GenericSystemException {
        Map<BigDecimal, BigDecimal> upperTerms = new HashMap<BigDecimal, BigDecimal>();
        for (T term : terms) {
            if (!upperTerms.containsKey(term.getCoreId())) {
                upperTerms.put(term.getCoreId(), term.getParentTermId());
            }
            BigDecimal upTermId = upperTerms.get(term.getCoreId());
            if (upTermId == null ? term.getParentTermId() != null : !term.getParentTermId().equals(upTermId)) {
                throw new GenericSystemException("parentTermId must be the same for dictionaryTerms in one entry");
            }
        }
    }

    private Collection<BigDecimal> getSysLangIds() throws GenericSystemException {
        try {
            Collection<BigDecimal> result = new ArrayList<BigDecimal>();
            for (SysLanguage language : this.getSysLangAccess().getAllObjects()) {
                result.add(language.getCoreId());
            }
            return result;
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    private BigDecimal generateId(BigDecimal dictionaryId) throws GenericSystemException {

        try {
            BigDecimal lastEntry = this.getLastEntry(dictionaryId);
            Dictionary dictionary = this.getDictionary(dictionaryId);

            switch (dictionary.getEntryCodePolicy().intValue()) {

            case ENTRY_POLICY_SIMPLE:
                return lastEntry.add(BigDecimal.ONE);

            case ENTRY_POLICY_BINARY:
                return lastEntry.multiply(new BigDecimal(2));

            default:
                throw new GenericSystemException("Incorrect data were found for dictionary[" + dictionaryId +
                        "]: wrong entryPolicy - " + dictionary.getEntryCodePolicy().intValue());
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    private BigDecimal getLastEntry(BigDecimal dictionaryId) throws GenericSystemException {

        BigDecimal lastEntry = DictionaryEntry.MIN_ALLOWABLE_VAL;
        for (T entry : ((DictionaryTermAccess<T>) this.getAccess()).getAllEntriesForDictionary(dictionaryId)) {
            if (entry.getCoreId().compareTo(lastEntry) == 1) {
                lastEntry = entry.getCoreId();
            }
        }

        return lastEntry;
    }

    private T addObjectNoCheck(UserProfile user, T identifiable) throws GenericSystemException {
        try {
            SyntheticId id = this.getAccess().getSyntheticId(identifiable);
            T entry = this.getAccess().getObjectById(id.getIdValues());
            if (entry == null) {
                return super.createObject(user, identifiable);
            } else {
                return entry;
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    private DictionaryAccess<Dictionary> getDictionaryAccess() throws GenericSystemException {
        if (dictionaryAccess == null) {
            dictionaryAccess = (DictionaryAccess<Dictionary>) AccessFactory.getImplementation(Dictionary.class);
        }
        return dictionaryAccess;
    }

    private SysLanguageAccess<SysLanguage> getSysLangAccess() throws GenericSystemException {
        if (sysLangAccess == null) {
            sysLangAccess = (SysLanguageAccess<SysLanguage>) AccessFactory.getImplementation(SysLanguage.class);
        }
        return sysLangAccess;
    }
}
