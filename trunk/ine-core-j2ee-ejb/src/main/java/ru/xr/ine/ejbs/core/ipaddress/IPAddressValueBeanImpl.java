/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.ipaddress;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.ipaddress.IPAddressValue;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс, представлющий собой stateless ejb. Реализует {@link IPAddressValueBean}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "IPAddressValueBean", mappedName = "stateless.IPAddressValueBean")
@Remote(IPAddressValueBean.class)
public class IPAddressValueBeanImpl<T extends IPAddressValue>
        extends VersionableBeanImpl<T> implements IPAddressValueBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(IPAddressValue.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
