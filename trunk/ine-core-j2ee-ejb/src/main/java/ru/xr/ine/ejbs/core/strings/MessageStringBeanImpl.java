/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.strings;

import ru.xr.ine.core.*;
import ru.xr.ine.core.strings.MessageString;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.strings.MessageStringAccess;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link MessageStringBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "MessageStringBean", mappedName = "stateless.MessageStringBean")
@Remote(MessageStringBean.class)
public class MessageStringBeanImpl<T extends MessageString> implements MessageStringBean<T> {

    private final MessageStringAccess<T> access;

    @SuppressWarnings({"unchecked"})
    public MessageStringBeanImpl() throws GenericSystemException {
        this.access = (MessageStringAccess<T>) AccessFactory.getImplementation(MessageString.class);
    }

    @Override
    public Collection<T> getAllObjects(UserProfile user) throws CoreException {
        
        try {
            UserHolder.setUserProfile(user);
            return access.getAllObjects();

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T getStringByKeyAndLang(UserProfile user, String stringKey, BigDecimal langCode) throws CoreException {

        try {
            UserHolder.setUserProfile(user);
            return access.getStringByKeyAndLang(stringKey, langCode);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Map<BigDecimal, T> getStringsByKey(UserProfile user, String stringKey) throws CoreException {

        try {
            UserHolder.setUserProfile(user);
            return access.getStringsByKey(stringKey);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T createObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            return access.createObject(identifiable);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T updateObject(UserProfile user, T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            return access.updateObject(identifiable, newIdentifiable);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            access.deleteObject(identifiable);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
