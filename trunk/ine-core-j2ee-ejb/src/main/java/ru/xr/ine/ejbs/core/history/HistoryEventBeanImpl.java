/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.history.HistoryEvent;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;
import ru.xr.ine.dbe.da.core.history.HistoryEventAccess;

import javax.annotation.Resource;
import javax.ejb.*;
import javax.jms.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventBeanImpl.java 67 2017-05-19 09:55:27Z xerror $"
 */
@Stateless(name = "HistoryEventBean", mappedName = "stateless.HistoryEventBean")
@Remote(HistoryEventBean.class)
public class HistoryEventBeanImpl<T extends HistoryEvent>
        extends VersionableHistoryBeanImpl<T> implements HistoryEventBean<T>, TimedObject {

    private static final Logger logger = Logger.getLogger(HistoryEventBeanImpl.class.getName());

    @Resource
    private SessionContext sessionCtx;

    @Resource(mappedName = "jms/InETopicCF")
    private TopicConnectionFactory topicConnectionFactory;
    @Resource(mappedName = "InETopicTop")
    private Topic topic;

    @Override
    public void init() throws GenericSystemException {
        init(HistoryEvent.class);
    }

    @Override
    protected IdentifiableAccess<T> getAccess() throws GenericSystemException {
        if (access == null) {
            init();
        }
        return access;
    }

    @Override
    public Collection<T> getEventsForProcess() throws CoreException {
        try {
            UserHolder.setUserProfile(new UserProfile(SYSTEM_USER_SYS));
            return ((HistoryEventAccess<T>) getAccess()).getEventsForProcess();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<T> getProcessedEvents() throws CoreException {
        try {
            UserHolder.setUserProfile(new UserProfile(SYSTEM_USER_SYS));
            return ((HistoryEventAccess<T>) getAccess()).getProcessedEvents();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void markAsProcessed(T historyEvent) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(new UserProfile(SYSTEM_USER_SYS));
            ((HistoryEventAccess<T>) getAccess()).markAsProcessed(historyEvent);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void startTimer() {

        logger.log(Level.FINE, "Start HistoryEvent Timer");
        boolean timerStarted = false;
        TimerService timerService = sessionCtx.getTimerService();

        Collection collection = timerService.getTimers();
        for (Object o : collection) {
            Timer timer = (Timer) o;
            if (TIMER_INFO.equals(timer.getInfo())) {
                timerStarted = true;
                break;
            }
        }

        if (!timerStarted) {
            timerService.createTimer(1000, 500, TIMER_INFO);
        }
    }

    @Override
    public boolean isTimerStarted() {
        return isTimerStarted(TIMER_INFO);
    }

    @Override
    public boolean isTimerStarted(Serializable timerInfo) {
        if (timerInfo == null) {
            throw new IllegalArgumentException("Null timer info not allowable");
        }

        boolean timerStarted = false;
        TimerService timerService = sessionCtx.getTimerService();

        Collection collection = timerService.getTimers();
        for (Object o : collection) {
            Timer timer = (Timer) o;
            if (timerInfo.equals(timer.getInfo())) {
                timerStarted = true;
                break;
            }
        }

        return timerStarted;
    }

    @Override
    public void stopTimer() {
        TimerService timerService = sessionCtx.getTimerService();

        Collection collection = timerService.getTimers();
        for (Object o : collection) {
            Timer timer = (Timer) o;
            if (TIMER_INFO.equals(timer.getInfo())) {
                timer.cancel();
                break;
            }
        }
    }

    @Override
    public void ejbTimeout(Timer timer) {

        logger.log(Level.FINE, "Timer timeout...");

        try {

            Collection<T> tCollection = getEventsForProcess();

            if (!tCollection.isEmpty()) {
                TopicConnection topicConnection = topicConnectionFactory.createTopicConnection();
                topicConnection.start();
                TopicSession topicSession = topicConnection.createTopicSession(true, Session.AUTO_ACKNOWLEDGE);
                TopicPublisher topicPublisher = topicSession.createPublisher(topic);

                for (T t : tCollection) {
                    ObjectMessage msg = topicSession.createObjectMessage();
                    markAsProcessed(t);
                    msg.setStringProperty(HistoryEvent.CORE_ID, t.getCoreId().toString());
                    msg.setObject(t);
                    topicPublisher.publish(msg);
                }

                topicPublisher.close();
                topicSession.close();
                topicConnection.close();
            }

        } catch (JMSException | CoreException e) {
            e.printStackTrace();
        }
    }

}
