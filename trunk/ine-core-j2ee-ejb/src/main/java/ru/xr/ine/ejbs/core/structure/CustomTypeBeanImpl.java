/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.dbe.da.core.structure.CustomTypeAccess;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.structure.CustomTypeBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "CustomTypeBean", mappedName = "stateless.CustomTypeBean")
@Remote(CustomTypeBean.class)
public class CustomTypeBeanImpl<T extends CustomType> extends VersionableBeanImpl<T>
        implements CustomTypeBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(CustomType.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @Override
    public Collection<String> getBaseObjects(UserProfile user) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);

            return ((CustomTypeAccess<T>) getAccess()).getBaseObjects();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<T> getTypesForBaseObject(UserProfile user, BigDecimal baseObject) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);

            return ((CustomTypeAccess<T>) getAccess()).getTypesForBaseObject(baseObject);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public boolean isCustomTypeUsed(UserProfile user, T customType) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);

            return ((CustomTypeAccess<T>) getAccess()).isCustomTypeUsed(customType.getCoreId());
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<CustomAttribute> getAllAttributes(UserProfile user, T customType) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);

            return ((CustomTypeAccess<T>) getAccess()).getAllAttributes(customType);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    protected void updateChecking(T versionable, T newVersionable) throws GenericSystemException {

        super.updateChecking(versionable, newVersionable);

        if (!versionable.getBaseObject().equals(newVersionable.getBaseObject())) {
            throw new GenericSystemException(
                    "Incorrect object modification. Base Object can not be modified.");
        }
        try {
            //проверка что основа предка не поменялась
            if (newVersionable.getParentTypeId() != null) {
                T parent = getAccess().getObjectById(newVersionable.getParentTypeId());
                if (!versionable.getBaseObject().equals(parent.getBaseObject())) {
                    throw new GenericSystemException(
                            "Incorrect object modification. Parent must have the same Base Object.");
                }
            }
        } catch (CoreException e) {
            throw new EJBException(e);
        }
    }

    @Override
    public Collection<CustomAttribute> getParentAttributes(UserProfile user, T customType)
            throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);

            return ((CustomTypeAccess<T>) getAccess()).getParentAttributes(customType);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
