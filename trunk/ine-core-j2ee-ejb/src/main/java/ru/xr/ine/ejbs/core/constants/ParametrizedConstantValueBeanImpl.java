/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.constants.ParametrizedConstantValue;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует
 * {@link ru.xr.ine.ejbs.core.constants.ParametrizedConstantValueBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "ParametrizedConstantValueBean", mappedName = "stateless.ParametrizedConstantValueBean")
@Remote(ParametrizedConstantValueBean.class)
@SuppressWarnings({"unchecked"})
public class ParametrizedConstantValueBeanImpl<T extends ParametrizedConstantValue> extends VersionableBeanImpl<T>
        implements ParametrizedConstantValueBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ParametrizedConstantValue.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
