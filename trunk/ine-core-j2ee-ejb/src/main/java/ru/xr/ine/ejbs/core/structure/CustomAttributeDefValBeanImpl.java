/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.structure.CustomAttributeDefVal;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "CustomAttributeDefValBean", mappedName = "stateless.CustomAttributeDefValBean")
@Remote(CustomAttributeDefValBean.class)
public class CustomAttributeDefValBeanImpl<T extends CustomAttributeDefVal> extends VersionableBeanImpl<T>
        implements CustomAttributeDefValBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(CustomAttributeDefVal.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
