/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link FuncSwitchNameBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "FuncSwitchNameBean", mappedName = "stateless.FuncSwitchNameBean")
@Remote(FuncSwitchNameBean.class)
public class FuncSwitchNameBeanImpl<T extends FuncSwitchName> extends IdentifiableBeanImpl<T>
        implements FuncSwitchNameBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(FuncSwitchName.class);
    }
}
