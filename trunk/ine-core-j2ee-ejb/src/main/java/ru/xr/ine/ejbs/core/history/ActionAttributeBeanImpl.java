/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.history.ActionAttribute;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.dbe.da.core.history.ActionAttributeAccess;
import ru.xr.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Collection;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.history.ActionAttributeBean}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "ActionAttributeBean", mappedName = "stateless.ActionAttributeBean")
@Remote(ActionAttributeBean.class)
public class ActionAttributeBeanImpl<T extends ActionAttribute> extends IdentifiableBeanImpl<T>
        implements ActionAttributeBean<T> {

    @Override
    protected void init() throws GenericSystemException {
        init(ActionAttribute.class);
    }

    @Override
    public Collection<T> getActionAttributes(UserProfile user, IdentifiableHistory history)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((ActionAttributeAccess<T>) getAccess()).getActionAttributes(history);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
