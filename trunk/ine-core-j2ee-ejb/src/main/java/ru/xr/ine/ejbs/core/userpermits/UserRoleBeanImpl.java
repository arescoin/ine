/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.*;
import ru.xr.ine.core.userpermits.UserRole;
import ru.xr.ine.dbe.da.core.userpermits.UserRoleAccess;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link UserRoleBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "UserRoleBean", mappedName = "stateless.UserRoleBean")
@Remote(UserRoleBean.class)
public class UserRoleBeanImpl<T extends UserRole> extends VersionableBeanImpl<T> implements UserRoleBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(UserRole.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @Override
    public Collection<T> getRolesByUserId(UserProfile user, BigDecimal userId) throws CoreException {

        try {
            UserHolder.setUserProfile(user);
            return new ArrayList<T>(((UserRoleAccess<T>) getAccess()).getRolesByUserId(userId));

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
