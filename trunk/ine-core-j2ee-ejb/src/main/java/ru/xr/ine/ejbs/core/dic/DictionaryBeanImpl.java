/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.dic.DictionaryBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
@Stateless(name = "DictionaryBean", mappedName = "stateless.DictionaryBean")
@Remote(DictionaryBean.class)
public class DictionaryBeanImpl<T extends Dictionary> extends VersionableBeanImpl<T>
        implements DictionaryBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Dictionary.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
