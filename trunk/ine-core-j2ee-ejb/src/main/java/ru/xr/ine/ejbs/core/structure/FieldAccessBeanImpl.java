/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.CustomAttributeValue;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.ObjectAccess;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.ejbs.core.history.HistoryEventBean;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: FieldAccessBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "FieldAccessBean", mappedName = "stateless.FieldAccessBean")
@Remote(FieldAccessBean.class)
public class FieldAccessBeanImpl implements FieldAccessBean {

    @Override
    public String getInterface(BigDecimal tableId) throws CoreException {
        try {
            UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));
            return FieldAccess.getInterface(tableId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public BigDecimal getTableId(Class identifiableClass) throws CoreException {
        try {
            UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));
            return FieldAccess.getTableId(identifiableClass);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public BigDecimal getFieldId(Class interfaceName, String fieldName) throws GenericSystemException {
        try {
            if (CustomAttributeValue.class.equals(interfaceName)) {
                throw new GenericSystemException("It's impossible to get id for CustomAttributeValue interface");
            }
            UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));

            ObjectAccess access = AccessFactory.getImplementation(interfaceName);
            return FieldAccess.getColumnId(access.getTableName(), access.getInstanceFields().get(fieldName));
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

}
