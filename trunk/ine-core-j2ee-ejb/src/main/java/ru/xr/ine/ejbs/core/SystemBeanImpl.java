/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.ejbs.core.history.HistoryEventBean;

import javax.annotation.Resource;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jms.Queue;
import javax.jms.QueueConnectionFactory;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Базовый класс InE-бинов, отвечающий за подъём системы и наполнение кеша данными.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemBeanImpl.java 66 2017-05-19 09:46:08Z xerror $"
 */
@Stateless(name = "SystemBean", mappedName = "stateless.SystemBean")
@Remote(SystemBean.class)
public class SystemBeanImpl implements SystemBean {

    private static final Logger logger = Logger.getLogger(SystemBeanImpl.class.getName());

    @Resource(mappedName = "jms/InEQueueCF")
    QueueConnectionFactory queueConnectionFactory;

    @Resource(mappedName = "IneStartupQueue")
    private Queue startupQueue;

    static final SystemState systemState = new SystemState();

//    static {
//        Timer timer = new Timer();
//        timer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//
//                try {
//                    InitialContext context = new InitialContext();
//
//                    // Запускаем обработку зарегистрированных действий над данными
//                    HistoryEventBean bean = (HistoryEventBean) context.lookup("stateless.HistoryEventBean");
//                    bean.startTimer();
//
//                    // Запускаем загрузку данных в систему
////                    SystemBean sBean = (SystemBean) context.lookup("stateless.SystemBean");
////                    sBean.startSystem();
//                } catch (NamingException e) {
//                    logger.log(Level.WARNING, e.getMessage(), e);
//                }
//
//            }
//        }, 15000);
//    }

    @Override
    public StartupState getState() {
        if (systemState.getState().equals(StartupState.START_FAILED)) {
            logger.warning(systemState.getErrorMessage());
        }
        return systemState.getState();
    }

    /**
     * Метод для запуска системы.<br>
     * В запуске системы участвуют 3 MD-бина: {@link StartupMDB}, {@link GetterMDB} и {@link ReceiverMDB}.
     * <br>
     * После загрузки класса состояние запуска системы равно {@link StartupState#IN_PROGRESS}.
     * Метод осуществляет посылку {@link SystemState#START_CMD управляющей команды} в {@link Queue очередь},
     * которую слушает StartupMDB.<br>
     * Получив команду, бин берёт из SystemState коллекцию интерфейсов, объекты которых необходимо загрузить,
     * и посылает их по одному в {@link Topic топик}, который слушают GetterMDB.<br>
     * Получив интерфейс, GetterMDB загружает все объекты данного интерфейса в кеш, после чего отсылает ответ
     * об успешном (или не успешном) завершении операции в очередь, которую слушает ReceiverMDB.<br>
     * ReceiverMDB регистрирует результат выполнения операции в SystemState. После получения последнего ответа
     * для выбранной порции интерфейсов, SystemState достаёт следующую порцию интерфейсов и сообщает ReceiverMDB,
     * что необходимо послать управляющую команду для StartupMDB.<br>
     * После обработки всех интерфейсов, StartupMDB выставляет результат запуска системы.<br>
     * <br>
     * Метод отрабатывает только ОДИН раз!
     */
    @Override
    public void startSystem() {
        logger.log(Level.INFO, "startSystem entered");
        // Если работаем вне сервера приложений, или не настроены JMS-ресурсы, то метод работать не должен
//        if (queueConnectionFactory == null) {
//            logger.log(Level.WARNING, "queueConnectionFactory is not initialized");
//            return;
//        }
        synchronized (systemState) {

            if (systemState.getState().equals(StartupState.STARTED)) {
                return;
            }

            // Если один раз уже отрабатывали, то прекращаем работу
//            if (systemState.isStartCmdSent()) {
//                return;
//            }

            try {
                // Инициализируем список интерфейсов всех объектов системы.
                SystemBeanImpl.systemState.initInterfaces();

                UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));

                Collection<String> interfaces = SystemBeanImpl.systemState.getInterfacesToProceed();

                while (!interfaces.isEmpty()) {
                    String iFace = interfaces.iterator().next();

                    Class clazz = Class.forName(iFace);
                    this.loadObjects(clazz);

                    if (DictionaryEntry.class.isAssignableFrom(clazz)) {
                        this.loadObjects(DictionaryTerm.class);
                    }
                    SystemBeanImpl.systemState.registerResponce(iFace, true, "");
                    interfaces = SystemBeanImpl.systemState.getInterfacesToProceed();
                }

                SystemBeanImpl.systemState.setState();


                // Запускаем обработку первой порции интерфейсов.
//                if (SystemBeanImpl.systemState.getState() == StartupState.IN_PROGRESS) {
//                    SystemState.sendStartCommand(queueConnectionFactory, startupQueue);
//                }
//            } catch (JMSException e) {
//                logger.log(Level.WARNING, e.getMessage(), e);
            } catch (ClassNotFoundException | GenericSystemException e) {
                e.printStackTrace();
            }
            systemState.setStartCmdSent(true);
        }


        try {
            InitialContext context = new InitialContext();

            // Запускаем обработку зарегистрированных действий над данными
            HistoryEventBean bean = (HistoryEventBean) context.lookup("stateless.HistoryEventBean");
            bean.startTimer();
            context.close();
        } catch (NamingException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }


    private void loadObjects(Class clazz) throws GenericSystemException {
        try {
            Collection objects = AccessFactory.getImplementation(clazz).getAllObjects();
            logger.log(Level.SEVERE, String.format("Loaded %1d %2s objects", objects.size(), clazz.getName()));
        } catch (CoreException e) {
            logger.log(Level.WARNING, String.format("Failed to load object for %1s", clazz));
            throw GenericSystemException.toGeneric(e);
        }
    }
}
