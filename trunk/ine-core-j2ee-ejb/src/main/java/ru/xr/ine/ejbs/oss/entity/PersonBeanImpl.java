/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;
import ru.xr.ine.oss.entity.Person;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link PersonBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "PersonBean", mappedName = "stateless.PersonBean")
@Remote(PersonBean.class)
public class PersonBeanImpl<T extends Person> extends VersionableBeanImpl<T> implements PersonBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Person.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
