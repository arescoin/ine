/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.*;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.dbe.da.core.history.IdentifiableHistoryAccess;
import ru.xr.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.history.IdentifiableHistoryBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked", "ThrowableInstanceNeverThrown"})
@Stateless(name = "IdentifiableHistoryBean", mappedName = "stateless.IdentifiableHistoryBean")
@Remote(IdentifiableHistoryBean.class)
public class IdentifiableHistoryBeanImpl<T extends IdentifiableHistory> extends IdentifiableBeanImpl<T>
        implements IdentifiableHistoryBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(IdentifiableHistory.class);
    }

    @Override
    public Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((IdentifiableHistoryAccess<T>) getAccess()).getActionsHistoryByIds(sysObjId, entityId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T createObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));

    }

    @Override
    public T updateObject(UserProfile user, T newIdentifiable) throws GenericSystemException,
            IneNotActualVersionModificationException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }
}
