/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.UserPermit;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link UserPermitBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "UserPermitBean", mappedName = "stateless.UserPermitBean")
@Remote(UserPermitBean.class)
public class UserPermitBeanImpl<T extends UserPermit> extends VersionableBeanImpl<T> implements UserPermitBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(UserPermit.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
