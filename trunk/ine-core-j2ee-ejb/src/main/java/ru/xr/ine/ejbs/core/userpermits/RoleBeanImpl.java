/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link RoleBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RoleBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "RoleBean", mappedName = "stateless.RoleBean")
@Remote(RoleBean.class)
public class RoleBeanImpl<T extends Role> extends VersionableBeanImpl<T> implements RoleBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Role.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
