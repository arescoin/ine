/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.*;
import ru.xr.ine.core.history.VersionableHistory;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.history.VersionableHistoryAccess;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;

import javax.ejb.EJBException;
import javax.ejb.ObjectNotFoundException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.history.VersionableHistoryBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked", "ThrowableInstanceNeverThrown"})
@Stateless(name = "VersionableHistoryBean", mappedName = "stateless.VersionableHistoryBean")
@Remote(VersionableHistoryBean.class)
public class VersionableHistoryBeanImpl<T extends VersionableHistory> extends IdentifiableHistoryBeanImpl<T>
        implements VersionableHistoryBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(VersionableHistory.class);
    }

    @Override
    public Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((VersionableHistoryAccess<T>) getAccess()).getActionsHistoryByIds(sysObjId, entityId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Versionable getObjectVersion(UserProfile user, T versionableHistory) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            getAccess();
            String interfaceName = FieldAccess.getInterface(versionableHistory.getCoreId());

            AbstractVersionableAccess versionableAccess = (AbstractVersionableAccess) AccessFactory.getImplementation(
                    Class.forName(interfaceName));

            Collection<Versionable> objectVersions = versionableAccess.getOldVersions(
                    (Versionable) versionableAccess.getObjectById(versionableHistory.getEntityId().getIdValues()));
            if (versionableHistory.getTd().equals(FieldAccess.getSystemTd())) {
                //изначально созданный объект имеет TD равный 3010 году поэтому при запросе его данных выдаем данные
                //на первое изменение, сравнивая только FD
                for (Versionable oldVersion : objectVersions) {
                    if (oldVersion.getCoreFd().equals(versionableHistory.getFd())) {
                        return oldVersion;
                    }
                }
            } else {
                for (Versionable oldVersion : objectVersions) {
                    if ((oldVersion.getCoreFd().equals(versionableHistory.getFd())) &&
                            (oldVersion.getCoreTd().equals(versionableHistory.getTd()))) {
                        return oldVersion;
                    }
                }
            }
            
            throw new EJBException(new ObjectNotFoundException("No version is found for " + versionableHistory));
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T createObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    public T updateObject(UserProfile user, T newIdentifiable) throws GenericSystemException,
            IneNotActualVersionModificationException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }
}
