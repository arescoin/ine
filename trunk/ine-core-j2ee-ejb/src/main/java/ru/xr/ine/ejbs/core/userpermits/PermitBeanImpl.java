/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.Permit;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link PermitBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "PermitBean", mappedName = "stateless.PermitBean")
@Remote(PermitBean.class)
public class PermitBeanImpl<T extends Permit> extends VersionableBeanImpl<T> implements PermitBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Permit.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
