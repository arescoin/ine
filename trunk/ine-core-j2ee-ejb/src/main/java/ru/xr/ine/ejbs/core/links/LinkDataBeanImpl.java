/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.links;

import ru.xr.ine.core.*;
import ru.xr.ine.core.links.LinkData;
import ru.xr.ine.dbe.da.core.links.LinkDataAccess;
import ru.xr.ine.dbe.da.core.links.LinkUtils;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link LinkDataBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"ThrowableInstanceNeverThrown"})
@Stateless(name = "LinkDataBean", mappedName = "stateless.LinkDataBean")
@Remote(LinkDataBean.class)
public class LinkDataBeanImpl<T extends LinkData> extends VersionableBeanImpl<T> implements LinkDataBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(LinkData.class);
    }

    @SuppressWarnings({"DuplicateThrows"})
    @Override
    public T updateObject(UserProfile user, T newVersionable)
            throws GenericSystemException, IneNotActualVersionModificationException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    protected Map getSyncMap() {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    public <E extends Identifiable> Collection<E> getLinkedObjects(UserProfile user, Identifiable objectToLink,
            BigDecimal linkId) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            return ((LinkDataAccess<T>) getAccess()).getLinkedObjects(objectToLink, linkId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<T> getLinkDataByObject(UserProfile user, Identifiable linkedObject)
            throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            return ((LinkDataAccess<T>) getAccess()).getLinkDataByObject(linkedObject);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<T> getLinkDataByObject(UserProfile user, Identifiable linkedObject, BigDecimal linkId)
            throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            return ((LinkDataAccess<T>) getAccess()).getLinkDataByObject(linkedObject, linkId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T createLinkData(UserProfile user, Versionable linkedObjectLeft, Versionable linkedObjectRight)
            throws GenericSystemException {
        return createLinkData(user, linkedObjectLeft, linkedObjectRight, BigDecimal.ONE);
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public T createLinkData(UserProfile user, Versionable linkedObjectLeft, Versionable linkedObjectRight,
            BigDecimal type) throws GenericSystemException {

        try {

            UserHolder.setUserProfile(user);
            BigDecimal linkId = LinkUtils.getLinkId(linkedObjectLeft.getClass(), linkedObjectRight.getClass(),
                    type.longValue());
            if (linkId == null) {
                throw new GenericSystemException("Failed to find out link between "
                        + linkedObjectLeft.getClass().getName() + " and " + linkedObjectRight.getClass().getName());
            }

            T linkData = (T) IdentifiableFactory.getImplementation(LinkData.class);
            linkData.setLinkId(linkId);
            linkData.setLeftId(linkedObjectLeft.getCoreId());
            linkData.setRightId(linkedObjectRight.getCoreId());
            linkData.setCoreFd(FieldAccess.getCurrentDate());
            linkData.setCoreTd(linkedObjectLeft.getCoreTd().before(
                    linkedObjectRight.getCoreTd()) ? linkedObjectLeft.getCoreTd() : linkedObjectRight.getCoreTd());

            return getAccess().createObject(linkData);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
