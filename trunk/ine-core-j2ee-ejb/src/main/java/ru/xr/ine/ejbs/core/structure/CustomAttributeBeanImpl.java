/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomAttributeValue;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.AttributesAccess;
import ru.xr.ine.dbe.da.core.structure.CustomAttributeAccess;
import ru.xr.ine.dbe.da.core.structure.CustomAttributeValueAccess;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.*;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.structure.CustomAttributeBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "CustomAttributeBean", mappedName = "stateless.CustomAttributeBean")
@Remote(CustomAttributeBean.class)
public class CustomAttributeBeanImpl<T extends CustomAttribute> extends VersionableBeanImpl<T>
        implements CustomAttributeBean<T> {

    private static final WeakHashMap<SyntheticId, Object> SYNC_MAP = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(CustomAttribute.class);
    }

    @Override
    protected Map getSyncMap() {
        return SYNC_MAP;
    }

    @Override
    public Collection<T> getAttributesByObject(UserProfile user, Class<? extends Identifiable> identifiableClass)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((CustomAttributeAccess<T>) getAccess()).getAttributesByObject(identifiableClass);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<CustomAttribute> getAttributes(UserProfile user, Class identifiableClass, CustomType customType)
            throws GenericSystemException {
        try{
            UserHolder.setUserProfile(user);
            return AttributesAccess.getAttributes(identifiableClass, customType);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public boolean isCustomAttributeUsed(UserProfile user, T t) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            getAccess();

            //проверка что нет CustomAttributeValue объектов с указанным описанием CustomAttribute
            CustomAttributeValueAccess<CustomAttributeValue> customAttributeValueAccess =
                    (CustomAttributeValueAccess) AccessFactory.getImplementation(CustomAttributeValue.class);

            SearchCriterion<BigDecimal> criterion = customAttributeValueAccess.getSearchCriteriaProfile().
                    getSearchCriterion(T.CORE_ID, CriteriaRule.equals, t.getCoreId());
            criterion.addAdditionalParam(FieldAccess.getInterface(t.getSystemObjectId()));

            Set<SearchCriterion> searchCriteria = new HashSet<SearchCriterion>();
            searchCriteria.add(criterion);

            return customAttributeValueAccess.containsObjects(searchCriteria);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

}
