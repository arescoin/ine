/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.constants.ParametrizedConstant;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.xr.ine.ejbs.core.constants.ParametrizedConstantBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "ParametrizedConstantBean", mappedName = "stateless.ParametrizedConstantBean")
@Remote(ParametrizedConstantBean.class)
@SuppressWarnings({"unchecked"})
public class ParametrizedConstantBeanImpl<T extends ParametrizedConstant> extends VersionableBeanImpl<T>
        implements ParametrizedConstantBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ParametrizedConstant.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
