/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.dbe.da.core.structure.SystemObjectAccess;
import ru.xr.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.*;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link SystemObjectBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectBeanImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Stateless(name = "SystemObjectBean", mappedName = "stateless.SystemObjectBean")
@Remote(SystemObjectBean.class)
public class SystemObjectBeanImpl<T extends SystemObject> extends VersionableBeanImpl<T>
        implements SystemObjectBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(SystemObject.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @Override
    public Set<String> getSupportedIntefaces(UserProfile user) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return new HashSet<String>(((SystemObjectAccess<T>) getAccess()).getInterfaces().values());
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Date getCurrentDate(UserProfile user) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((SystemObjectAccess<T>) getAccess()).getCurrentDate();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Date getSystemFd(UserProfile user) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((SystemObjectAccess<T>) getAccess()).getSystemFd();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Date getSystemTd(UserProfile user) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((SystemObjectAccess<T>) getAccess()).getSystemTd();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

}
