/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CustomTypeBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class CustomTypeDelegate<T extends CustomType> extends VersionableDelegate<T>
        implements CustomTypeBean<T> {

    public CustomTypeDelegate() {
        init("stateless.CustomTypeBean");
    }

    @Override
    public Collection<String> getBaseObjects(UserProfile user) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getBaseObjects(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<T> getTypesForBaseObject(UserProfile user, BigDecimal baseObject) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getTypesForBaseObject(user, baseObject);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public boolean isCustomTypeUsed(UserProfile user, T customType) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).isCustomTypeUsed(user, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<CustomAttribute> getAllAttributes(UserProfile user, T customType) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getAllAttributes(user, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<CustomAttribute> getParentAttributes(UserProfile user, T customType) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getParentAttributes(user, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
