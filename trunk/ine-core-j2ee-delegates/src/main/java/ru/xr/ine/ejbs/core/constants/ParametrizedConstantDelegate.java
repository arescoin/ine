/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import ru.xr.ine.core.constants.ParametrizedConstant;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link ParametrizedConstantBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ParametrizedConstantDelegate<T extends ParametrizedConstant> extends VersionableDelegate<T>
        implements ParametrizedConstantBean<T> {

    public ParametrizedConstantDelegate() {
        init("stateless.ParametrizedConstantBean");
    }
}
