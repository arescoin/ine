/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link ConstantBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantDelegate<T extends Constant> extends VersionableDelegate<T>
        implements ConstantBean<T> {

    public ConstantDelegate() {
        init("stateless.ConstantBean");
    }
}
