/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import ru.xr.ine.core.funcswitch.FuncSwitch;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link FuncSwitchBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FuncSwitchDelegate<T extends FuncSwitch> extends VersionableDelegate<T>
        implements FuncSwitchBean<T> {

    public FuncSwitchDelegate() {
        init("stateless.FuncSwitchBean");
    }
}
