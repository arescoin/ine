/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.ejbs.core.AbstractDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link FieldAccessBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FieldAccessDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FieldAccessDelegate extends AbstractDelegate implements FieldAccessBean {

    public FieldAccessDelegate() {
        init("stateless.FieldAccessBean");
    }

    @Override
    public String getInterface(BigDecimal tableId) throws CoreException {
        try {
            return ((FieldAccessBean) lookup()).getInterface(tableId);
        } catch (
                EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public BigDecimal getTableId(Class identifiableClass) throws CoreException {
        try {
            return ((FieldAccessBean) lookup()).getTableId(identifiableClass);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public BigDecimal getFieldId(Class interfaceName, String fieldName) throws GenericSystemException {
        try {
            return ((FieldAccessBean) lookup()).getFieldId(interfaceName, fieldName);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

}
