/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link DictionaryEntryBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class DictionaryEntryDelegate<T extends DictionaryEntry> extends VersionableDelegate<T>
        implements DictionaryEntryBean<T> {

    public DictionaryEntryDelegate() {
        init("stateless.DictionaryEntryBean");
    }

    @Override
    public Collection<T> getAllEntriesForDictionary(UserProfile user, BigDecimal dictionaryId)
            throws GenericSystemException {
        try {
            return ((DictionaryEntryBean<T>) lookup()).getAllEntriesForDictionary(user, dictionaryId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
