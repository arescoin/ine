/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.ejbs.core.IdentifiableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link IdentifiableHistoryBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class IdentifiableHistoryDelegate<T extends IdentifiableHistory> extends IdentifiableDelegate<T>
        implements IdentifiableHistoryBean<T> {

    public IdentifiableHistoryDelegate() {
        init("stateless.IdentifiableHistoryBean");
    }

    @Override
    public Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException {
        try {
            return ((IdentifiableHistoryBean<T>) lookup()).getActionsHistoryByIds(user, sysObjId, entityId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
