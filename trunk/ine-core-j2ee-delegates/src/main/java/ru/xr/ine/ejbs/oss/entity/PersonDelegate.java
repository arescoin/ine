/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import ru.xr.ine.ejbs.core.VersionableDelegate;
import ru.xr.ine.oss.entity.Person;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link PersonBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PersonDelegate<T extends Person> extends VersionableDelegate<T> implements PersonBean<T> {

    public PersonDelegate() {
        init("stateless.PersonBean");
    }
}
