/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.userpermits.RolePermit;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link RolePermitBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RolePermitDelegate<T extends RolePermit> extends VersionableDelegate<T> implements RolePermitBean<T> {

    public RolePermitDelegate() {
        init("stateless.RolePermitBean");
    }
}
