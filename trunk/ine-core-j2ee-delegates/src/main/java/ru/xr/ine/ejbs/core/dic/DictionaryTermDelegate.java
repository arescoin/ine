/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class DictionaryTermDelegate<T extends DictionaryTerm> extends VersionableDelegate<T> implements DictionaryTermBean<T> {

    public DictionaryTermDelegate() {
        init("stateless.DictionaryTermBean");
    }

    @Override
    public Collection<T> getAllTermsForDictionaryAndLanguage(
            UserProfile user, BigDecimal dictionaryId, BigDecimal languageId) throws GenericSystemException {
        try {
            return ((DictionaryTermBean<T>) lookup()).getAllTermsForDictionaryAndLanguage(user, dictionaryId, languageId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<T> getAllTermsForDictionaryAndEntry(UserProfile user, BigDecimal dictionaryId, BigDecimal entryId)
            throws GenericSystemException {
        try {
            return ((DictionaryTermBean<T>) lookup()).getAllTermsForDictionaryAndEntry(user, dictionaryId, entryId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public T addObject(UserProfile user, T identifiable) throws GenericSystemException {
        try {
            return ((DictionaryTermBean<T>) lookup()).addObject(user, identifiable);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public BigDecimal createDictionaryTerms(UserProfile user, Collection<T> terms) throws GenericSystemException {
        try {
            return ((DictionaryTermBean<T>) lookup()).createDictionaryTerms(user, terms);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public void addDictionaryTerms(UserProfile user, Collection<T> terms) throws GenericSystemException {
        try {
            ((DictionaryTermBean<T>) lookup()).addDictionaryTerms(user, terms);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<T> getAllEntriesForDictionary(UserProfile user, BigDecimal dictionaryId)
            throws GenericSystemException {
        try {
            return ((DictionaryEntryBean<T>) lookup()).getAllEntriesForDictionary(user, dictionaryId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
