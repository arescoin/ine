/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.language.LangName;
import ru.xr.ine.ejbs.core.IdentifiableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link LangNameBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangNameDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class LangNameDelegate<T extends LangName> extends IdentifiableDelegate<T>
        implements LangNameBean<T> {

    public LangNameDelegate() {
        init("stateless.LangNameBean");
    }
}
