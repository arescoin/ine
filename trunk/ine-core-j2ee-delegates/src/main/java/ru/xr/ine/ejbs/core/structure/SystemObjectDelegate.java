/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.util.Date;
import java.util.Set;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link SystemObjectBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class SystemObjectDelegate<T extends SystemObject> extends VersionableDelegate<T>
        implements SystemObjectBean<T> {

    public SystemObjectDelegate() {
        init("stateless.SystemObjectBean");
    }

    @Override
    public Set<String> getSupportedIntefaces(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getSupportedIntefaces(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Date getCurrentDate(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getCurrentDate(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Date getSystemFd(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getSystemFd(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Date getSystemTd(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getSystemTd(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

}
