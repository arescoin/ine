/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.Versionable;
import ru.xr.ine.core.history.VersionableHistory;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link VersionableHistoryBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class VersionableHistoryDelegate<T extends VersionableHistory> extends IdentifiableHistoryDelegate<T>
        implements VersionableHistoryBean<T> {

    public VersionableHistoryDelegate() {
        init("stateless.VersionableHistoryBean");
    }

    @Override
    public Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException {
        try {
            return ((VersionableHistoryBean<T>) lookup()).getActionsHistoryByIds(user, sysObjId, entityId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Versionable getObjectVersion(UserProfile user, T versionableHistory) throws GenericSystemException {
        try {
            return ((VersionableHistoryBean<T>) lookup()).getObjectVersion(user, versionableHistory);
        } catch (EJBException e) {
            if (GenericSystemException.class.isInstance(e.getCause())) {
                throw (GenericSystemException) e.getCause();
            }
            throw GenericSystemException.toGeneric(e.getCause());
        }
    }
}
