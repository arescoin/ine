/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.inventory.item;

import ru.xr.ine.ejbs.core.VersionableDelegate;
import ru.xr.ine.oss.inventory.item.PropertyDataDate;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataDateDelegate.java 46 2017-04-13 16:05:04Z xerror $"
 */
public class PropertyDataDateDelegate<T extends PropertyDataDate>
        extends VersionableDelegate<T> implements PropertyDataDateBean<T> {

    public PropertyDataDateDelegate() {
        init("stateless.PropertyDataDateBean");
    }
}