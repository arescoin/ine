/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.ipaddress;

import ru.xr.ine.core.ipaddress.IPAddressValue;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link IPAddressValueBean}.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IPAddressValueDelegate<T extends IPAddressValue>
        extends VersionableDelegate<T> implements IPAddressValueBean<T> {

    public IPAddressValueDelegate() {
        init("stateless.IPAddressValueBean");
    }
}
