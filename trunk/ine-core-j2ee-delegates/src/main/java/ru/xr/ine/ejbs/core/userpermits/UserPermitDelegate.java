/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.userpermits.UserPermit;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link UserPermitBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserPermitDelegate<T extends UserPermit> extends VersionableDelegate<T> implements UserPermitBean<T> {

    public UserPermitDelegate() {
        init("stateless.UserPermitBean");
    }
}
