/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CustomAttributeBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class CustomAttributeDelegate<T extends CustomAttribute> extends VersionableDelegate<T>
        implements CustomAttributeBean<T> {

    public CustomAttributeDelegate() {
        init("stateless.CustomAttributeBean");
    }

    @Override
    public Collection<T> getAttributesByObject(UserProfile user, Class<? extends Identifiable> identifiableClass)
            throws GenericSystemException {
        try {
            return ((CustomAttributeBean<T>) lookup()).getAttributesByObject(user, identifiableClass);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<CustomAttribute> getAttributes(UserProfile user, Class identifiableClass, CustomType customType)
            throws GenericSystemException {
        try {
            return ((CustomAttributeBean<T>) lookup()).getAttributes(user, identifiableClass, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public boolean isCustomAttributeUsed(UserProfile user, T t) throws GenericSystemException {
        try {
            return ((CustomAttributeBean<T>) lookup()).isCustomAttributeUsed(user, t);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

}
