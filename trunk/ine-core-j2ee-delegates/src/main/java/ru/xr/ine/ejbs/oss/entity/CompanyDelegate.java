/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import ru.xr.ine.ejbs.core.VersionableDelegate;
import ru.xr.ine.oss.entity.Company;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CompanyBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CompanyDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CompanyDelegate<T extends Company> extends VersionableDelegate<T> implements CompanyBean<T> {

    public CompanyDelegate() {
        init("stateless.CompanyBean");
    }
}
