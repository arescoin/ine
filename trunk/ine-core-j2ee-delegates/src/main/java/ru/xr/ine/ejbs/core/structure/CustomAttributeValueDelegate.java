/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomAttributeValue;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CustomAttributeValueBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class CustomAttributeValueDelegate<T extends CustomAttributeValue> extends VersionableDelegate<T>
        implements CustomAttributeValueBean<T> {

    public CustomAttributeValueDelegate() {
        init("stateless.CustomAttributeValueBean");
    }

    @Override
    public T getValue(
            UserProfile user, BigDecimal identifiableId, CustomAttribute attribute) throws GenericSystemException {
        try {
            return ((CustomAttributeValueBean<T>) lookup()).getValue(user, identifiableId, attribute);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Map<CustomAttribute, CustomAttributeValue> getValues(
            UserProfile user, Identifiable obj, CustomType customType) throws GenericSystemException {
        try {
            return ((CustomAttributeValueBean<T>) lookup()).getValues(user, obj, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public T getObjectById(UserProfile user, BigDecimal... id) throws GenericSystemException {
        try {
            return ((CustomAttributeValueBean<T>) lookup()).getObjectById(user, id);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public void createValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException {
        try {
            ((CustomAttributeValueBean<T>) lookup()).createValues(user, obj, customType, values);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public void updateValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException {
        try {
            ((CustomAttributeValueBean<T>) lookup()).updateValues(user, obj, customType, values);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public void deleteValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException {
        try {
            ((CustomAttributeValueBean<T>) lookup()).deleteValues(user, obj, customType, values);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
