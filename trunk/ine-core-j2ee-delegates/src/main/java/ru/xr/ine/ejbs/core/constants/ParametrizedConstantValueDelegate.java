/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import ru.xr.ine.core.constants.ParametrizedConstantValue;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link ParametrizedConstantValueBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ParametrizedConstantValueDelegate<T extends ParametrizedConstantValue> extends VersionableDelegate<T>
        implements ConstantValueBean<T> {

    public ParametrizedConstantValueDelegate() {
        init("stateless.ParametrizedConstantValueBean");
    }
}
