/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link DictionaryBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryDelegate<T extends Dictionary> extends VersionableDelegate<T>
        implements DictionaryBean<T> {

    public DictionaryDelegate() {
        init("stateless.DictionaryBean");
    }
}
