/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.userpermits.Permit;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link PermitBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PermitDelegate<T extends Permit> extends VersionableDelegate<T> implements PermitBean<T> {

    public PermitDelegate() {
        init("stateless.PermitBean");
    }
}
