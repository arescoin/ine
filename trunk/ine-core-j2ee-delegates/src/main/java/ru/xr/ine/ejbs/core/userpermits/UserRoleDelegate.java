/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.userpermits.UserRole;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link UserRoleBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserRoleDelegate<T extends UserRole> extends VersionableDelegate<T> implements UserRoleBean<T> {

    public UserRoleDelegate() {
        init("stateless.UserRoleBean");
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public Collection<T> getRolesByUserId(UserProfile user, BigDecimal userId) throws CoreException {
        try {
            return ((UserRoleBean<T>) lookup()).getRolesByUserId(user, userId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
