/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.RuntimeCoreException;

import javax.ejb.EJBException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.transaction.UserTransaction;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Общий абстрактный предок для делегатов
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: AbstractDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class AbstractDelegate {

    private static final Logger logger = Logger.getLogger(AbstractDelegate.class.getName());

    //имя свойства для пути к файлу конфигурации библиотеки  делегатов
    public static final String PROPERTY_NAME = "java.initialcontext.file.path";
    private static final String PROPERTIES_FILE_NAME = "initialcontext.properties";

    /** JNDI-имя для UserTransaction по-умолчанию */
    private static final String USER_TRANSACTION_JNDI_NAME = "UserTransaction";

    private static final InitialContext initialContext = initCtx();

    protected String mappedName;
    protected Object remoteObject;

    private static InitialContext initCtx() {

        InitialContext result = null;

        InputStream is = null;
        try {
            Properties props = new Properties();

            logger.config("Context PATH: " + System.getProperty(PROPERTY_NAME));

            if (System.getProperty(PROPERTY_NAME) != null) {

                //если переменная оружения установлена открываем указанный в ней путь
                File propFile = new File(System.getProperty(PROPERTY_NAME));
                logger.config("Context by file: " + propFile.getCanonicalPath());
                props.load(new FileReader(propFile));
            } else {
                is = AbstractDelegate.class.getResourceAsStream("/" + PROPERTIES_FILE_NAME);
                if (is != null) {
                    //иначе открываем из jar файла бибилотеки делегатов
                    props.load(is);
                } else {

                    //если переменная окружения не установлена и файл внутри jar не найден то бибилиотека вызвана из
                    // web-приложения внутри glassfish сервера, используем InitialContext по умолчанию
                    props = null;
                }
            }

            logger.config("Calling context by: " + props);

            result = new InitialContext(props);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NamingException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    protected void init(String mappedName) {
        this.mappedName = mappedName;
    }

    protected Object lookup() {

        synchronized (PROPERTY_NAME) {
            if (remoteObject == null) {
                remoteObject = lookup(mappedName);
            }
        }

        return remoteObject;
    }

    /**
     * Возвращает объект для работы с UserTransaction, расположенный в JNDI-реестре.
     * <p/>
     * Используется JNDI-имя по-умолчанию {@link #USER_TRANSACTION_JNDI_NAME}
     *
     * @return локальная транзакция
     */
    @SuppressWarnings("UnusedDeclaration")
    public static UserTransaction lookupUserTransaction() {
        return lookupUserTransaction(USER_TRANSACTION_JNDI_NAME);
    }

    /**
     * Возвращает объект для работы с UserTransaction, расположенный в JNDI-реестре.
     * <p/>
     *
     * @param jndiName JNDI-имя для получения имени транзакции
     *
     * @return локальная транзакция
     * @throws ClassCastException при получении неверного типа объекта
     */
    public static UserTransaction lookupUserTransaction(String jndiName) throws ClassCastException {
        return (UserTransaction) lookup(jndiName);
    }

    protected static Object lookup(String name) {
        try {
            return initialContext.lookup(name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Возвращает в действительности случившееся исключение, упакованное в EJBException
     *
     * @param e исключение-обертка, выбрасываемое EJB
     *
     * @return случившееся исключение
     */
    protected GenericSystemException getCauseException(EJBException e) {

        Throwable causedByException;
        try {
            causedByException = e.getCausedByException();
        } catch (Exception e1) {
            causedByException = e.getCause();
        }

        if (causedByException == null) {
            causedByException = e.getCause();
        }

        if (causedByException != null) {

            Throwable original = causedByException;
            original = extract(original);

            GenericSystemException gse = GenericSystemException.toGeneric(original);
            gse.setAdditionalException(e);

            return gse;
        } else {
            return new GenericSystemException("Unknown exception in RMI layer: "
                    + e.getClass() + ": " + e.getMessage(), e);
        }

    }

    private Throwable extract(Throwable original) {

        while (original.getCause() != null) {//пока причинное исключение установлено
            original = original.getCause();
            if (EJBException.class.isInstance(original)) {
                //для EJBException исходное исключение достается другим методом
                Throwable throwable = ((EJBException) original).getCausedByException();

                if (throwable == null) {
                    throwable = original.getCause();
                }

                original = throwable;
            } else if (RuntimeCoreException.class.isInstance(original) || CoreException.class.isInstance(original)) {
                break;
            }
        }

        return original;
    }

}
