/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link RoleBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RoleDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RoleDelegate<T extends Role> extends VersionableDelegate<T> implements RoleBean<T> {

    public RoleDelegate() {
        init("stateless.RoleBean");
    }
}
