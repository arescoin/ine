/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.inventory.item;

import ru.xr.ine.ejbs.core.VersionableDelegate;
import ru.xr.ine.oss.inventory.item.ItemProfileValue;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileValueDelegate.java 46 2017-04-13 16:05:04Z xerror $"
 */
public class ItemProfileValueDelegate<T extends ItemProfileValue>
        extends VersionableDelegate<T> implements ItemProfileValueBean<T> {

    public ItemProfileValueDelegate() {
        init("stateless.ItemProfileValueBean");
    }
}