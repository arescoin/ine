/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.links;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.links.Link;
import ru.xr.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link LinkBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class LinkDelegate<T extends Link> extends VersionableDelegate<T> implements LinkBean<T> {

    public LinkDelegate() {
        init("stateless.LinkBean");
    }

    @Override
    public Collection<T> getLinksByObjType(UserProfile user, Class clazz) throws GenericSystemException {
        try {
            return ((LinkBean<T>) lookup()).getLinksByObjType(user, clazz);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public BigDecimal getLinkId(UserProfile user, Class leftType, Class rightType, long type)
            throws GenericSystemException {
        try {
            return ((LinkBean<T>) lookup()).getLinkId(user, leftType, rightType, type);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
