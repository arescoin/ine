/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.history.ActionAttribute;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.ejbs.core.IdentifiableDelegate;

import javax.ejb.EJBException;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class ActionAttributeDelegate<T extends ActionAttribute> extends IdentifiableDelegate<T>
        implements ActionAttributeBean<T> {

    public ActionAttributeDelegate() {
        init("stateless.ActionAttributeBean");
    }

    @Override
    public Collection<T> getActionAttributes(UserProfile user, IdentifiableHistory history)
            throws GenericSystemException {
        try {
            return ((ActionAttributeBean<T>) lookup()).getActionAttributes(user, history);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
