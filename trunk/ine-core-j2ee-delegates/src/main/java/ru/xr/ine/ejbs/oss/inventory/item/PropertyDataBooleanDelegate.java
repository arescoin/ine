/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.inventory.item;

import ru.xr.ine.ejbs.core.VersionableDelegate;
import ru.xr.ine.oss.inventory.item.PropertyDataBoolean;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataBooleanDelegate.java 46 2017-04-13 16:05:04Z xerror $"
 */
public class PropertyDataBooleanDelegate<T extends PropertyDataBoolean>
        extends VersionableDelegate<T> implements PropertyDataBooleanBean<T> {

    public PropertyDataBooleanDelegate() {
        init("stateless.PropertyDataBooleanBean");
    }
}