/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.Versionable;

import javax.ejb.EJBException;
import java.util.Collection;
import java.util.Date;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public abstract class VersionableDelegate<T extends Versionable> extends IdentifiableDelegate<T>
        implements VersionableBean<T> {

    protected void init(String mappedName) {
        super.init(mappedName);
    }

    public Collection<T> getOldVersions(UserProfile user, T versionable)
            throws GenericSystemException {
        try {
            return ((VersionableBean<T>) lookup()).getOldVersions(user, versionable);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    public Collection<T> getOldVersions(UserProfile user, T versionable, Date fd, Date td)
            throws GenericSystemException {

        try {
            return ((VersionableBean<T>) lookup()).getOldVersions(user, versionable, fd, fd);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
