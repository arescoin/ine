/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.ejbs.core.IdentifiableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link FuncSwitchNameBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FuncSwitchNameDelegate<T extends FuncSwitchName> extends IdentifiableDelegate<T>
        implements FuncSwitchNameBean<T> {

    public FuncSwitchNameDelegate() {
        init("stateless.FuncSwitchNameBean");
    }
}
