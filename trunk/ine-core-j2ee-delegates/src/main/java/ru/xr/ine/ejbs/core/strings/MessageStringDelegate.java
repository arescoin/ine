/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.strings;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.strings.MessageString;
import ru.xr.ine.ejbs.core.AbstractDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link MessageStringBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class MessageStringDelegate<T extends MessageString> extends AbstractDelegate implements MessageStringBean<T> {

    public MessageStringDelegate() {
        init("stateless.MessageStringBean");
    }

    @Override
    public Collection<T> getAllObjects(UserProfile user) throws CoreException {

        try {
            return ((MessageStringBean) lookup()).getAllObjects(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public T getStringByKeyAndLang(UserProfile user, String stringKey, BigDecimal langCode) throws CoreException {

        try {
            return (T)((MessageStringBean) lookup()).getStringByKeyAndLang(user, stringKey, langCode);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Map<BigDecimal, T> getStringsByKey(UserProfile user, String stringKey) throws CoreException {

        try {
            return ((MessageStringBean) lookup()).getStringsByKey(user, stringKey);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public T createObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        try {
            return (T)((MessageStringBean) lookup()).createObject(user, identifiable);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public T updateObject(UserProfile user, T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {

        try {
            return (T)((MessageStringBean) lookup()).updateObject(user, identifiable, newIdentifiable);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        try {
            ((MessageStringBean) lookup()).deleteObject(user, identifiable);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
