/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.language.CountryDetails;
import ru.xr.ine.ejbs.core.IdentifiableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CountryDetailsBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CountryDetailsDelegate<T extends CountryDetails> extends IdentifiableDelegate<T>
        implements CountryDetailsBean<T> {

    public CountryDetailsDelegate() {
        init("stateless.CountryDetailsBean");
    }
}
