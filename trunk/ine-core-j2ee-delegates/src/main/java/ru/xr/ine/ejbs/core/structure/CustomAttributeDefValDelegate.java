/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.structure.CustomAttributeDefVal;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CustomAttributeDefValDelegate<T extends CustomAttributeDefVal>
        extends VersionableDelegate<T> implements CustomAttributeDefValBean<T> {

    public CustomAttributeDefValDelegate() {
        init("stateless.CustomAttributeDefValBean");
    }
}
