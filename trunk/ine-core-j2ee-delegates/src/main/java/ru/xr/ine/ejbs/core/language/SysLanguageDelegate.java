/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.language.SysLanguage;
import ru.xr.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link SysLanguageBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SysLanguageDelegate<T extends SysLanguage> extends VersionableDelegate<T>
        implements SysLanguageBean<T> {

    public SysLanguageDelegate() {
        init("stateless.SysLanguageBean");
    }
}
