/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs;

/**
 * Класс - точка входа для загрузки делегатов в дебаге.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: LoadPoint.java 29 2017-04-04 15:32:19Z xerror $"
 */
class LoadPoint {
}
