/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.userpermits.Permit;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PermitDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            PermitDelegate<Permit> testedDelegate = DelegateFactory.obtainDelegateByInterface(Permit.class);

            Permit permit = IdentifiableFactory.getImplementation(Permit.class);
            permit.setCoreDsc("Description");
            permit.setName("Name");
            permit.setMaskValue("Mask");
            permit.setValueRequired(false);

            BigDecimal createdId = testedDelegate.createObject(user, permit).getCoreId();
            permit = testedDelegate.getObjectById(user, createdId);

            Assert.assertEquals("Name", permit.getName());

            Permit cUpdated = testedDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Name", cUpdated.getName());
            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, permit);
                Assert.fail();
            } catch (Exception ignored) {
            }

            testedDelegate.deleteObject(user, permit);
            if (testedDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Permit");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
