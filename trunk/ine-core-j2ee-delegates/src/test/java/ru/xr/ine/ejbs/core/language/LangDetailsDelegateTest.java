/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.ConstantValue;
import ru.xr.ine.core.language.LangDetails;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;
import ru.xr.ine.ejbs.core.constants.ConstantValueDelegate;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class LangDetailsDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4);
        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");
            LangDetailsDelegate<LangDetails> langDetailsDelegate =
                    DelegateFactory.obtainDelegateByInterface(LangDetails.class);

            LangDetails langDetails = IdentifiableFactory.getImplementation(LangDetails.class);
            langDetails.setCoreDsc("Description");
            langDetails.setCoreId(BigDecimal.valueOf(8888l));
            langDetails.setCode("rr");
            langDetails.setName("TestLangDetails");

            BigDecimal createdId = langDetailsDelegate.createObject(user, langDetails).getCoreId();

            langDetails = langDetailsDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestLangDetails", langDetails.getName());
            LangDetails cUpdated = langDetailsDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            langDetailsDelegate.updateObject(user, cUpdated);

            langDetails = langDetailsDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", langDetails.getName());

            langDetailsDelegate.deleteObject(user, langDetails);
            if (langDetailsDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove LangDetails");
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            ConstantValueDelegate<ConstantValue> bean = DelegateFactory.obtainDelegateByInterface(ConstantValue.class);
            ConstantValue constantValue = bean.getObjectById(user, constantId);
            constantValue.setValue(value);
            bean.updateObject(user, constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }
}
