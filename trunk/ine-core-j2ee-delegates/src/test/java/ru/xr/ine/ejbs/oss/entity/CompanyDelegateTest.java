/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;
import ru.xr.ine.oss.entity.Company;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CompanyDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CompanyDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            CompanyDelegate<Company> testedDelegate = DelegateFactory.getDelegateForInterface(Company.class.getName());

            Company company = IdentifiableFactory.getImplementation(Company.class);
            company.setCoreDsc("Description");
            company.setName("Name");
            company.setType(BigDecimal.ONE);
            company.setPropertyType(BigDecimal.ONE);

            BigDecimal createdId = testedDelegate.createObject(user, company).getCoreId();

            company = testedDelegate.getObjectById(user, createdId);
            Assert.assertEquals("Name", company.getName());

            Company cUpdated = testedDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Name", cUpdated.getName());

            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, company);
                Assert.fail();
            } catch (Exception ex) {
            }

            testedDelegate.deleteObject(user, company);
            if (testedDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Company");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
