/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.utils.BaseTest;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestHistoryEventDelegate.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class TestHistoryEventDelegate extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testStartStopTimer() {
        HistoryEventDelegate delegate = new HistoryEventDelegate();
        try {
            if (delegate.isTimerStarted()) {
                delegate.stopTimer();
            }
            Assert.assertFalse("Timer must be stopped", delegate.isTimerStarted());

            delegate.startTimer();
            Assert.assertTrue("Timer must be started", delegate.isTimerStarted());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void startTimer() {
        HistoryEventDelegate delegate = new HistoryEventDelegate();
        try {
            delegate.startTimer();
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void stopTimer() {
        HistoryEventDelegate delegate = new HistoryEventDelegate();
        try {
            delegate.stopTimer();
        } catch (Exception e) {
            fail(e);
        }
    }

}
