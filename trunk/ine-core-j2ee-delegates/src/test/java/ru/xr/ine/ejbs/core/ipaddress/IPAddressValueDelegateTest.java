/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.ipaddress;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.ipaddress.IPAddress;
import ru.xr.ine.core.ipaddress.IPAddressValue;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IPAddressValueDelegateTest extends BaseDelegateTest {

    @Override
    protected void testDelegate() {
        try {
            final IPAddress address1 = IPAddress.parse("192.168.1.50");
            final IPAddress address2 = IPAddress.parse("192.168.1.50");

            IPAddressValueDelegate<IPAddressValue> bean =
                    DelegateFactory.obtainDelegateByInterface(IPAddressValue.class);
            //создаем новый объект
            IPAddressValue value = IdentifiableFactory.getImplementation(IPAddressValue.class);
            value.setCoreDsc("CRUD-test via delegateS");
            value.setIpAddress(address1);

            IPAddressValue created = bean.createObject(user, value);

            //получаем созданный объект и провреям данные
            value = bean.getObjectById(user, created.getCoreId());
            Assert.assertEquals(address1, value.getIpAddress());

            //обновляем
            value.setIpAddress(address2);
            value = bean.updateObject(user, value);
            //получаем обновленный объект и провреям данные
            Assert.assertEquals(address2, value.getIpAddress());

            //удаление
            try {
                bean.deleteObject(user, value);
            } catch (Exception e) {
                //такая фигня получается из-за того, что IPAddressValue используется в кастомном атрибуте
                //ossj-объекта, который на момент теста мавеном не может быть загружен (нет нужной либы)
                bean.deleteObject(user, value);
            }
            if (bean.getObjectById(user, value.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove IPAddressValue");
            }
        } catch (Exception e) {
            fail(e);
        }

    }
}
