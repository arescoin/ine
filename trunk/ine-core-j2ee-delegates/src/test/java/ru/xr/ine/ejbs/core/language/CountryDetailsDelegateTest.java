/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.ConstantValue;
import ru.xr.ine.core.language.CountryDetails;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;
import ru.xr.ine.ejbs.core.constants.ConstantValueDelegate;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CountryDetailsDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4);

        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");
            CountryDetailsDelegate<CountryDetails> countryDetailsDelegate =
                    DelegateFactory.obtainDelegateByInterface(CountryDetails.class);

            CountryDetails countryDetails = IdentifiableFactory.getImplementation(CountryDetails.class);
            countryDetails.setCoreDsc("Description");
            countryDetails.setCoreId(BigDecimal.valueOf(8888l));
            countryDetails.setCodeA2("YY");
            countryDetails.setCodeA3("YY1");
            countryDetails.setName("TestCountryDetails");

            BigDecimal createdId = countryDetailsDelegate.createObject(user, countryDetails).getCoreId();

            countryDetails = countryDetailsDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestCountryDetails", countryDetails.getName());
            CountryDetails cUpdated = countryDetailsDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            countryDetailsDelegate.updateObject(user, cUpdated);

            countryDetails = countryDetailsDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", countryDetails.getName());

            countryDetailsDelegate.deleteObject(user, countryDetails);
            if (countryDetailsDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove CountryDetails");
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            ConstantValueDelegate<ConstantValue> bean = DelegateFactory.obtainDelegateByInterface(ConstantValue.class);
            ConstantValue constantValue = bean.getObjectById(user, constantId);
            constantValue.setValue(value);
            bean.updateObject(user, constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }
}
