/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryDelegateTest extends BaseDelegateTest {

    private class Counter {

        private int count = 0;

        void inc() {
            ++count;
        }

        void dec() {
            --count;
        }

        boolean isNetral() {
            return count == 0;
        }
    }

    @SuppressWarnings({"unchecked"})
//    @Test
    public void testThreads() {

        try {



            final Counter counter = new Counter();
            counter.inc();

            Runnable creator = new Runnable() {
                @Override
                public void run() {
                    try {
                        counter.inc();
                        UserHolder.setUser(user.getSystemUser());
                        UserHolder.getUser().setLastAction(new UserProfile.Action(null, "for testing purposes"));

                        Dictionary dictionary = IdentifiableFactory.getImplementation(Dictionary.class);
                        dictionary.setCoreDsc("Description");
                        dictionary.setDicType(BigDecimal.ONE);
                        dictionary.setEntryCodePolicy(BigDecimal.ONE);
                        dictionary.setLocalizationPolicy(BigDecimal.ONE);
                        dictionary.setName("Name");

                        DictionaryDelegate<Dictionary> dictionaryDelegate =
                                            DelegateFactory.obtainDelegateByInterface(Dictionary.class);

                        BigDecimal createdId = dictionaryDelegate.createObject(user, dictionary).getCoreId();
                        System.out.println(createdId);
                    } catch (Exception e) {
                        fail(e);
                    } finally {
                        counter.dec();
                    }

                }
            };


            Runnable searcher = new Runnable() {
                @Override
                public void run() {

                    try {
                        counter.inc();

                        DictionaryDelegate<Dictionary> dictionaryDelegate =
                                            DelegateFactory.obtainDelegateByInterface(Dictionary.class);

                        UserHolder.setUser(user.getSystemUser());
                        UserHolder.getUser().setLastAction(new UserProfile.Action(null, "for testing purposes"));

                        SearchCriteriaProfile searchCriteriaProfile =
                                dictionaryDelegate.getSearchCriteriaProfile(user);
                        SearchCriterion<BigDecimal> searchCriterion = (SearchCriterion<BigDecimal>)
                                searchCriteriaProfile.getSearchCriterion(Dictionary.CORE_ID);

                        searchCriterion.setCriterionRule(CriteriaRule.in);
                        searchCriterion.addCriteriaVal(new BigDecimal(1));
                        searchCriterion.addCriteriaVal(new BigDecimal(2));
                        searchCriterion.addCriteriaVal(new BigDecimal(3));

                        Set<SearchCriterion> searchCriteria = new HashSet<SearchCriterion>();
                        searchCriteria.add(searchCriterion);
                        Collection collection = dictionaryDelegate.searchObjects(user, searchCriteria);

                        System.out.println(collection);
                    } catch (Exception e) {
                        fail(e);
                    } finally {
                        counter.dec();
                    }

                }
            };


            for (int i = 0; i < 100; i++) {
                Thread thread1 = new Thread(creator);
                Thread thread3 = new Thread(creator);
                Thread thread2 = new Thread(searcher);

                thread1.start();
                thread2.start();
                thread3.start();
            }

            counter.dec();

            while (!counter.isNetral()) {
                Thread.sleep(20);
            }

        } catch (Exception e) {
            fail(e);
        }


    }


    protected void testDelegate() {

        try {
            DictionaryDelegate<Dictionary> dictionaryDelegate =
                    DelegateFactory.obtainDelegateByInterface(Dictionary.class);

            Dictionary dictionary = IdentifiableFactory.getImplementation(Dictionary.class);
            dictionary.setCoreDsc("Description");
            dictionary.setDicType(BigDecimal.ONE);
            dictionary.setEntryCodePolicy(BigDecimal.ONE);
            dictionary.setLocalizationPolicy(BigDecimal.ONE);
            dictionary.setName("Name");

            BigDecimal createdId = dictionaryDelegate.createObject(user, dictionary).getCoreId();

            dictionary = dictionaryDelegate.getObjectById(user, createdId);
            Assert.assertEquals("Name", dictionary.getName());

            Dictionary dicUpdated = dictionaryDelegate.getObjectById(user, createdId);
            dicUpdated.setName("New Name");

            dictionaryDelegate.updateObject(user, dicUpdated);

            dictionary = dictionaryDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", dictionary.getName());

            dictionaryDelegate.deleteObject(user, dictionary);
            if (dictionaryDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Dictionary");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}