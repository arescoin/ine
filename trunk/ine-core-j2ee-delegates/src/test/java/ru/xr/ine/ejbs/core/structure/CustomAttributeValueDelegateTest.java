/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Ignore;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomAttributeValueDelegateTest /*extends BaseDelegateTest*/ {

//    private BigDecimal testObjId;
//
//    @Before
//    public void init() throws Exception {
//        testObjId = ((FieldAccessDelegate)
//                Class.forName("ru.xr.ine.ejbs.core.structure.FieldAccessDelegate").newInstance()
//        ).getTableId(Company.class);
//    }
//
//    protected void testDelegate() {
//
//        try {
//            CustomAttributeDelegate<CustomAttribute> customAttributeDelegate =
//                    DelegateFactory.getDelegateForInterface(CustomAttribute.class.getName());
//            CustomAttributeValueDelegate<CustomAttributeValue> customAttributeValueDelegate =
//                    DelegateFactory.getDelegateForInterface(CustomAttributeValue.class.getName());
//
//            //создаем описание настраиваемого атрибута
//            CustomAttribute object = IdentifiableFactory.getImplementation(CustomAttribute.class);
//            object.setCoreDsc("Description");
//            object.setSystemObjectId(testObjId);
//            object.setAttributeName("TestCustomAttribute");
//            object.setDataType(DataType.stringType);
//            object.setRequired(false);
//
//            object = customAttributeDelegate.createObject(user, object);
//
//            //создаем user - тестовый объект
//            Company company = (Company) IdentifiableFactory.getImplementation(Company.class);
//            company.setName("Attribute keeper");
//            company.setPropertyType(new BigDecimal(1));
//            company.setType(new BigDecimal(1));
//            CompanyDelegate<Company> companyDelegate =
//                    DelegateFactory.getDelegateForInterface(Company.class.getName());
////            company.setCharacterizedBy(companyDelegate.getCharacteristics(user, company));
//            company = companyDelegate.createObject(user, company);
//
//            //создаем значение настраиваемого атрибута для тестового объекта
//            CustomAttributeValue attributeValue = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
//            attributeValue.setCoreId(object.getCoreId());
//            attributeValue.setValue("test");
//            attributeValue.setEntityId(company.getCoreId());
//
//            attributeValue = customAttributeValueDelegate.createObject(user, attributeValue);
//
//            SyntheticId syntheticId = customAttributeValueDelegate.getSyntheticId(user, attributeValue);
//            attributeValue = customAttributeValueDelegate.getObjectById(user, syntheticId.getIdValues());
//            Assert.assertEquals("test", attributeValue.getValue());
//
//            //проверим что можно изменить значение атрибута
//            attributeValue.setValue("new value");
//            customAttributeValueDelegate.updateObject(user, attributeValue);
//            CustomAttributeValue objectValueBad = customAttributeValueDelegate.getObjectById(user, syntheticId.getIdValues());
//            Assert.assertEquals("new value", attributeValue.getValue());
//
//            //проверим что нельзя изменить сущность которой принадлежит модифицированный аттрибут
//            try {
//                objectValueBad.setEntityId(BigDecimal.ONE);
//                customAttributeValueDelegate.updateObject(user, objectValueBad);
//                Assert.fail();
//            } catch (Exception ignored) {
//            }
//            //проверим что нельзя удалить описание
//            Assert.assertTrue(customAttributeDelegate.isCustomAttributeUsed(user, object));
//            try {
//                customAttributeDelegate.deleteObject(user, object);
//                Assert.fail();
//            } catch (GenericSystemException e) {
//                IneIllegalArgumentException ee = (IneIllegalArgumentException) e.getCause();
//                Assert.assertEquals("Attribute description can not be deleted." +
//                        " There are some CustomAttributeValue objects of this CustomAttribute type", ee.getMessage());
//            } catch (Exception e) {
//                fail(e);
//            }
//            //получим атрибут для тестового объекта
//            CustomAttributeValue cav = customAttributeValueDelegate.getValue(user, company.getCoreId(), object);
//            Assert.assertEquals("new value", cav.getValue());
//
//            //удаляем созданное
//            //проверим что удалился и объект и значение его атрибута
//            companyDelegate.deleteObject(user, company);
//            if (companyDelegate.getObjectById(user, company.getCoreId()) != null) {
//                Assert.fail("method deleteObject() has failed to remove UserValue");
//            }
//            if (customAttributeValueDelegate.getObjectById(user, syntheticId.getIdValues()) != null) {
//                Assert.fail("method deleteObject() has failed to remove CustomAttributeValue");
//            }
//            customAttributeDelegate.deleteObject(user, object);
//        } catch (Exception e) {
//            fail(e);
//        }
//    }
}
