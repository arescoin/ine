/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Ignore;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.ParametrizedConstant;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;
import ru.xr.ine.ejbs.core.structure.FieldAccessDelegate;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class ParametrizedConstantDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            ParametrizedConstantDelegate<ParametrizedConstant> parametrizedConstantDelegate
                    = DelegateFactory.getDelegateForInterface("ru.xr.ine.core.constants.ParametrizedConstant");

            ParametrizedConstant parametrizedConstant =
                    IdentifiableFactory.getImplementation(ParametrizedConstant.class);
            parametrizedConstant.setCoreDsc("Description");
            parametrizedConstant.setName("TestParametrizedConstant");
//            parametrizedConstant.setType(BigDecimal.ONE);
            parametrizedConstant.setModifiable(true);
            parametrizedConstant.setNullable(true);
            parametrizedConstant.setMask(new SystemObjectPattern(
                    new FieldAccessDelegate().getFieldId(ParametrizedConstant.class, ParametrizedConstant.CORE_ID)));
            parametrizedConstant.setDefaultValue("1");

            parametrizedConstant = parametrizedConstantDelegate.createObject(user, parametrizedConstant);

            ParametrizedConstant returned = parametrizedConstantDelegate.getObjectById(
                    user, parametrizedConstant.getCoreId());
            Assert.assertEquals("TestParametrizedConstant", returned.getName());

            ParametrizedConstant cUpdated = parametrizedConstantDelegate.getObjectById(user, returned.getCoreId());
            cUpdated.setName("New Name");

            parametrizedConstantDelegate.updateObject(user, cUpdated);

            parametrizedConstant = parametrizedConstantDelegate.getObjectById(user, parametrizedConstant.getCoreId());
            Assert.assertEquals("New Name", parametrizedConstant.getName());

            //проверка что нельзя обновить устаревшей версией
            try {
                parametrizedConstant.setCoreDsc("");
                parametrizedConstantDelegate.updateObject(user, returned);
                Assert.fail();
            } catch (Exception ignored) {
            }

            parametrizedConstantDelegate.deleteObject(user, parametrizedConstant);
            if (parametrizedConstantDelegate.getObjectById(user, parametrizedConstant.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove ParametrizedConstant");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
