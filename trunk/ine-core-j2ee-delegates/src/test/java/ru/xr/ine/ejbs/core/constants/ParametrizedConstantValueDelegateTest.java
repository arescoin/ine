/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Ignore;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.constants.ParametrizedConstant;
import ru.xr.ine.core.constants.ParametrizedConstantValue;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class ParametrizedConstantValueDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {

            ParametrizedConstant obj02 = IdentifiableFactory.getImplementation(ParametrizedConstant.class);

            obj02.setCoreDsc("TestObject for CRUD-operations testing");
//            obj02.setCoreFd(FieldAccess.getSystemFd());
//            obj02.setCoreTd(FieldAccess.getSystemTd());
            obj02.setName("TEST_PARAMETRIZED_CONSTANT2");
            obj02.setModifiable(true);
            obj02.setNullable(true);
//            obj02.setType(BigDecimal.ONE);
            obj02.setDefaultValue("1");
            obj02.setMask(new SystemObjectPattern(FieldAccess.getColumnId("TEST_TABLE", "n")));


            ParametrizedConstantDelegate<ParametrizedConstant> constantDelegate =
                    DelegateFactory.obtainDelegateByInterface(ParametrizedConstant.class);
            obj02 = constantDelegate.createObject(user, obj02);


            ParametrizedConstantValueDelegate<ParametrizedConstantValue> parametrizedConstantValueDelegate =
                    DelegateFactory.obtainDelegateByInterface(ParametrizedConstantValue.class);

            ParametrizedConstantValue parametrizedConstantValue =
                    IdentifiableFactory.getImplementation(ParametrizedConstantValue.class);
            parametrizedConstantValue.setCoreDsc("Description");
            parametrizedConstantValue.setValue("Test");
            parametrizedConstantValue.setCoreId(obj02.getCoreId());
            parametrizedConstantValue.setParam(Identifiable.MIN_ALLOWABLE_VAL);

            ParametrizedConstantValue createdValue =
                    parametrizedConstantValueDelegate.createObject(user, parametrizedConstantValue);

            SyntheticId createdId = parametrizedConstantValueDelegate.getSyntheticId(user, createdValue);
            parametrizedConstantValue = parametrizedConstantValueDelegate.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("Test", parametrizedConstantValue.getValue());

            ParametrizedConstantValue updatedValue =
                    parametrizedConstantValueDelegate.getObjectById(user, createdId.getIdValues());
            updatedValue.setValue("New Value");

            parametrizedConstantValueDelegate.updateObject(user, updatedValue);

            parametrizedConstantValue = parametrizedConstantValueDelegate.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("New Value", parametrizedConstantValue.getValue());

            //проверка что нельзя обновить устаревшей версией
            try {
                parametrizedConstantValue.setCoreDsc("");
                parametrizedConstantValueDelegate.updateObject(user, createdValue);
                Assert.fail();
            } catch (Exception ignored) {
            }

            parametrizedConstantValueDelegate.deleteObject(user, parametrizedConstantValue);
            if (parametrizedConstantValueDelegate.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove ParametrizedConstantValue");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
