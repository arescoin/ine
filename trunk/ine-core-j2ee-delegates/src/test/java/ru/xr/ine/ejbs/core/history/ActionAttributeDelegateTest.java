/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.history.ActionAttribute;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;
import ru.xr.ine.ejbs.core.dic.DictionaryDelegate;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ActionAttributeDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        String attrName = "TestAttributeName";
        String attrValue = "TestAttributeValue";
        try {
            if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS).isEnabled() ||
                    !FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_ACTION_ATTRIBUTES).isEnabled()) {
                return;
            }

            BigDecimal tableId = FieldAccess.getTableId(Dictionary.class);

            DictionaryDelegate<Dictionary> dictionaryBean = DelegateFactory.obtainDelegateByInterface(Dictionary.class);

            Dictionary dictionary = IdentifiableFactory.getImplementation(Dictionary.class);
            dictionary.setCoreDsc("Description");
            dictionary.setDicType(BigDecimal.ONE);
            dictionary.setEntryCodePolicy(BigDecimal.ONE);
            dictionary.setLocalizationPolicy(BigDecimal.ONE);
            dictionary.setName("ActionAttributeDelegateTestName");

            user.setUserAttribute(attrName, attrValue);

            dictionary = dictionaryBean.createObject(user, dictionary);

            IdentifiableHistoryDelegate<IdentifiableHistory> historyBean =
                    DelegateFactory.obtainDelegateByInterface(IdentifiableHistory.class);

            Collection<IdentifiableHistory> history = historyBean.getActionsHistoryByIds(
                    user, tableId, dictionaryBean.getSyntheticId(user, dictionary));
            Assert.assertEquals(1, history.size());

            ActionAttributeDelegate<ActionAttribute> attributeBean =
                    DelegateFactory.obtainDelegateByInterface(ActionAttribute.class);

            Collection<ActionAttribute> attributes = attributeBean.getActionAttributes(user, history.iterator().next());
            Assert.assertEquals(1, attributes.size());
            ActionAttribute attribute = attributes.iterator().next();
            Assert.assertEquals(attrName, attribute.getAttributeName());
            Assert.assertEquals(attrValue, attribute.getAttributeValue());

            dictionaryBean.deleteObject(user, dictionary);
        } catch (Exception e) {
            fail(e);
        }
    }
}
