/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.ejbs.core.IdentifiableDelegate;
import ru.xr.ine.ejbs.core.constants.ConstantDelegate;
import ru.xr.ine.utils.BaseTest;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DelegateFactoryTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DelegateFactoryTest extends BaseTest {

    @Test
    public void testFactory() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        IdentifiableDelegate delegate = DelegateFactory.getDelegateForInterface(Constant.class.getName());
        Assert.assertTrue(ConstantDelegate.class.isInstance(delegate));

//        delegate = DelegateFactory.getDelegateForInterface(AssociationRule.class.getName());
//        Assert.assertTrue(AssociationRuleDelegate.class.isInstance(delegate));
    }
}
