/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.ejbs.core.StartupState;
import ru.xr.ine.ejbs.core.SystemBean;
import ru.xr.ine.utils.BaseTest;
import ru.xr.ine.utils.config.ConfigurationManager;

import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * Базовый класс для тестирования делегатов
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: BaseDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class BaseDelegateTest extends BaseTest {

    static {
        String propValue = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                SystemBean.class.getName() + "." + SystemBean.CHECK_SYSTEM_START_STATE_PROP_NAME, "true");

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.getProperties().list(System.out);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        if (Boolean.parseBoolean(propValue)) {
            try {
                InitialContext context;

                if (System.getProperty("java.initialcontext.file.path") != null) {
                    //если переменная оружения установлена открываем указанный в ней путь
                    File propFile = new File(System.getProperty("java.initialcontext.file.path"));
                    Properties props = new Properties();
                    props.load(new FileReader(propFile));
                    context = new InitialContext(props);
                } else {
                    context = new InitialContext();
                }

                SystemBean bean = (SystemBean) context.lookup("stateless.SystemBean");
                bean.startSystem();

                while (true) {
                    StartupState state = bean.getState();
                    if (state == StartupState.STARTED) {
                        break;
                    } else if (state == StartupState.START_FAILED) {
                        Assert.fail("Startup failed. See server log-file for details.");
                    } else {
                        Thread.sleep(1000);
                    }
                }
            } catch (Exception e) {
                fail(e);
            }
        }
    }

    protected boolean testingRequired = true;//для отключения в наследниках тестирования

    protected abstract void testDelegate();

    @Test
    public void test() {

        if (!testingRequired) {
            return;
        }

        testDelegate();
    }
}
