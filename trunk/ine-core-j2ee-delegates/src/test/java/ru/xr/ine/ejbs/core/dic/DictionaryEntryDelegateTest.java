/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import org.junit.Assert;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс для тестирования соответствующего EJB
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryEntryDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            DictionaryEntryDelegate<DictionaryEntry> dictionaryEntryDelegate =
                    DelegateFactory.getDelegateForInterface("ru.xr.ine.core.dic.DictionaryEntry");
            Collection<DictionaryEntry> entries =
                    dictionaryEntryDelegate.getAllEntriesForDictionary(user, BigDecimal.ONE);

            DictionaryEntry dicEntry = entries.iterator().next();
            try {
                dictionaryEntryDelegate.createObject(user, dicEntry);
            } catch (Exception e) {
                Assert.assertTrue(e.getCause() instanceof UnsupportedOperationException);
            }
            try {
                dictionaryEntryDelegate.updateObject(user, dicEntry);
            } catch (Exception e) {
                Assert.assertTrue(e.getCause() instanceof UnsupportedOperationException);
            }

            int before = entries.size();
            dicEntry.setCoreId(new BigDecimal(9999));

            dictionaryEntryDelegate.deleteObject(user, dicEntry);
            Assert.assertEquals(before, dictionaryEntryDelegate.getAllEntriesForDictionary(
                    user, BigDecimal.ONE).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
