/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.language.LangName;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangNameDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class LangNameDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            LangNameDelegate<LangName> langNameDelegate = DelegateFactory.obtainDelegateByInterface(LangName.class);

            LangName langName = IdentifiableFactory.getImplementation(LangName.class);
            langName.setCoreDsc("Description");
            langName.setCoreId(BigDecimal.valueOf(8888l));
            langName.setName("TestLangNameDelegateTest");
            BigDecimal createdId = langNameDelegate.createObject(user, langName).getCoreId();

            langName = langNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestLangName", langName.getName());

            LangName cUpdated = langNameDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            langNameDelegate.updateObject(user, cUpdated);

            langName = langNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", langName.getName());

            langNameDelegate.deleteObject(user, langName);
            if (langNameDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove LangName");
            }
        } catch (Exception e) {
            fail(e);
        }
    }

}
