/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.UserRole;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserRoleDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            UserRoleDelegate<UserRole> testedDelegate = DelegateFactory.obtainDelegateByInterface(UserRole.class);

            UserRole userRole = IdentifiableFactory.getImplementation(UserRole.class);
            userRole.setCoreId(new BigDecimal(2));
            userRole.setCoreDsc("Description");
            userRole.setActive(true);
            userRole.setUserId(new BigDecimal(2));

            userRole = testedDelegate.createObject(user, userRole);

            SyntheticId createdId = testedDelegate.getSyntheticId(user, userRole);
            userRole = testedDelegate.getObjectById(user, createdId.getIdValues());

            Assert.assertEquals(true, userRole.isActive());

            UserRole cUpdated = testedDelegate.getObjectById(user, createdId.getIdValues());
            cUpdated.setActive(false);

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals(false, cUpdated.isActive());

            try {
                testedDelegate.updateObject(user, userRole);
                Assert.fail();
            } catch (Exception ignored) {
            }

            //проверка что нельзя обновить устаревшей версией
            testedDelegate.deleteObject(user, cUpdated);
            if (testedDelegate.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove UserRole");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
