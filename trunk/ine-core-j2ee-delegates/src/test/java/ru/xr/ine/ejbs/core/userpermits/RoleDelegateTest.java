/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RoleDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RoleDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            RoleDelegate<Role> delegate = DelegateFactory.obtainDelegateByInterface(Role.class);

            Role role = IdentifiableFactory.getImplementation(Role.class);
            role.setCoreDsc("Description");
            role.setName("Name");

            BigDecimal createdId = delegate.createObject(user, role).getCoreId();

            role = delegate.getObjectById(user, createdId);
            Assert.assertEquals("Name", role.getName());

            Role cUpdated = delegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            cUpdated = delegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Name", cUpdated.getName());

            //проверка что нельзя обновить устаревшей версией
            try {
                delegate.updateObject(user, role);
                Assert.fail();
            } catch (Exception ignored) {
            }
            delegate.deleteObject(user, role);
            if (delegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Role");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
