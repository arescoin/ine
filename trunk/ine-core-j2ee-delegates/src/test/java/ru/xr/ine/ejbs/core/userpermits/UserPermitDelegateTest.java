/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.UserPermit;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserPermitDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            UserPermitDelegate<UserPermit> testedDelegate = DelegateFactory.obtainDelegateByInterface(UserPermit.class);

            UserPermit userPermit = IdentifiableFactory.getImplementation(UserPermit.class);
            userPermit.setCoreId(BigDecimal.ONE);
            userPermit.setCoreDsc("Description");
            userPermit.setActive(true);
            userPermit.setUserId(new BigDecimal(2)); // тестовый юзверь Test User
            userPermit.setRoleId(new BigDecimal(2)); // тестовая роль NOBODY
            userPermit.setValues(new BigDecimal[]{new BigDecimal(1)});

            userPermit = testedDelegate.createObject(user, userPermit);

            SyntheticId createdId = testedDelegate.getSyntheticId(user, userPermit);
            userPermit = testedDelegate.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(true, userPermit.isActive());

            UserPermit cUpdated = testedDelegate.getObjectById(user, createdId.getIdValues());
            cUpdated.setActive(false);

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals(false, cUpdated.isActive());
            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, userPermit);
                Assert.fail();
            } catch (Exception ignored) {
            }

            testedDelegate.deleteObject(user, cUpdated);
            if (testedDelegate.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove UserPermit");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
