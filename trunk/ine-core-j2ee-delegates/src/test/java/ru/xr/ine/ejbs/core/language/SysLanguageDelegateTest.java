/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.language.LangName;
import ru.xr.ine.core.language.SysLanguage;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SysLanguageDelegateTest extends BaseDelegateTest {

    private static final BigDecimal LANG_ID = BigDecimal.valueOf(9999l);

    protected void testDelegate() {

        final BigDecimal countryDetailsAndorra = BigDecimal.valueOf(20l);//андорра

        try {
            LangNameDelegate<LangName> langNameDelegate = DelegateFactory.obtainDelegateByInterface(LangName.class);
            SysLanguageDelegate<SysLanguage> sysLanguageDelegate =
                    DelegateFactory.obtainDelegateByInterface(SysLanguage.class);

            //создание языка удаленным бином
            LangName langName = IdentifiableFactory.getImplementation(LangName.class);
            langName.setCoreDsc("Description");
            langName.setCoreId(LANG_ID);
            langName.setName("TestLangName");

            langNameDelegate.createObject(user, langName);

            SysLanguage sysLanguage = IdentifiableFactory.getImplementation(SysLanguage.class);
            sysLanguage.setLangDetails(new BigDecimal(107));  // sq Албанский
            sysLanguage.setCountryDetails(new BigDecimal(8)); // AL Албания
            sysLanguage.setCoreId(LANG_ID);

            SysLanguage created = sysLanguageDelegate.createObject(user, sysLanguage);

            SyntheticId id = sysLanguageDelegate.getSyntheticId(user, created);
            sysLanguage = sysLanguageDelegate.getObjectById(user, id.getIdValues());
            sysLanguage.setCountryDetails(countryDetailsAndorra);

            sysLanguage = sysLanguageDelegate.updateObject(user, sysLanguage);

            Assert.assertEquals(countryDetailsAndorra, sysLanguage.getCountryDetails());

            //проверка что нельзя обновить устаревшей версией
            try {
                sysLanguageDelegate.updateObject(user, created);
                Assert.fail();
            } catch (Exception ignored) {
            }

            //удаление
            sysLanguageDelegate.deleteObject(user, sysLanguage);
            if (sysLanguageDelegate.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove SysLanguage");
            }
            langNameDelegate.deleteObject(user, langName);
        } catch (Exception e) {
            fail(e);
        }
    }
}
