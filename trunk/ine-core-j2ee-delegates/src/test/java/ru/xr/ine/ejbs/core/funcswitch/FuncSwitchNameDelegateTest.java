/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FuncSwitchNameDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            FuncSwitchNameDelegate<FuncSwitchName> funcSwitchNameDelegate =
                    DelegateFactory.obtainDelegateByInterface(FuncSwitchName.class);

            FuncSwitchName funcSwitchName = IdentifiableFactory.getImplementation(FuncSwitchName.class);
            funcSwitchName.setCoreDsc("Description");
            funcSwitchName.setName("TestFuncSwitchName");

            BigDecimal createdId = funcSwitchNameDelegate.createObject(user, funcSwitchName).getCoreId();

            funcSwitchName = funcSwitchNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestFuncSwitchName", funcSwitchName.getName());

            FuncSwitchName cUpdated = funcSwitchNameDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            funcSwitchNameDelegate.updateObject(user, cUpdated);

            funcSwitchName = funcSwitchNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", funcSwitchName.getName());

            funcSwitchNameDelegate.deleteObject(user, funcSwitchName);
            if (funcSwitchNameDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove FuncSwitchName");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
