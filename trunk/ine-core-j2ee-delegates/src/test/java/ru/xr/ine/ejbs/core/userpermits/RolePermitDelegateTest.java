/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.RolePermit;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RolePermitDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            RolePermitDelegate<RolePermit> testedDelegate = DelegateFactory.obtainDelegateByInterface(RolePermit.class);

            RolePermit rolePermit = IdentifiableFactory.getImplementation(RolePermit.class);
            rolePermit.setCoreDsc("Description");
            rolePermit.setCoreId(new BigDecimal(2)); // тестовая роль NOBODY
            rolePermit.setPermitId(new BigDecimal(2)); // тестовый доступ System's Objects Access
            rolePermit.setPermitValues(new BigDecimal[]{new BigDecimal(1)}); // некое тестовое значение

            rolePermit = testedDelegate.createObject(user, rolePermit);

            SyntheticId createdId = testedDelegate.getSyntheticId(user, rolePermit);
            rolePermit = testedDelegate.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("Description", rolePermit.getCoreDsc());

            RolePermit cUpdated = testedDelegate.getObjectById(user, createdId.getIdValues());
            cUpdated.setCoreDsc("New Description");

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Description", cUpdated.getCoreDsc());

            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, rolePermit);
                Assert.fail();
            } catch (Exception ignored) {
            }

            testedDelegate.deleteObject(user, rolePermit);
            if (testedDelegate.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove RolePermit");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
