/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.strings;

import ru.xr.ine.ejbs.BaseDelegateTest;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class MessageStringDelegateTest extends BaseDelegateTest {

    @SuppressWarnings({"unchecked"})
    protected void testDelegate() {

        /*
        try {
            MessageStringBean<MessageString> delegate = (MessageStringBean<MessageString>)
                    DelegateFactory.obtainDelegateByInterface(MessageString.class);

            MessageString newOne = IdentifiableFactory.getImplementation(MessageString.class);

            newOne.setKey("ru.xr.ine.testString");
            newOne.setLanguageCode(new BigDecimal(1));
            newOne.setMessage("Test message for CRUD-operations");

            MessageString oldOne = delegate.createObject(user, newOne);
            newOne = delegate.getStringByKeyAndLang(user, oldOne.getKey(), oldOne.getLanguageCode());

            Assert.assertEquals("Failed to save object on DB.", oldOne.getKey(), newOne.getKey());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getMessage(), newOne.getMessage());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getLanguageCode(), newOne.getLanguageCode());

            newOne = delegate.getStringsByKey(user, oldOne.getKey()).get(oldOne.getLanguageCode());

            Assert.assertEquals("Failed to save object on DB.", oldOne.getKey(), newOne.getKey());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getMessage(), newOne.getMessage());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getLanguageCode(), newOne.getLanguageCode());

            String newMessage = oldOne.getMessage() + " updated";
            newOne.setMessage(newMessage);

            oldOne = delegate.updateObject(user, oldOne, newOne);
            newOne = delegate.getStringByKeyAndLang(user, oldOne.getKey(), oldOne.getLanguageCode());

            Assert.assertEquals("Failed to update object on DB.", newMessage, oldOne.getMessage());
            Assert.assertEquals("Failed to update object on DB.", newMessage, newOne.getMessage());

            delegate.deleteObject(user, newOne);
            oldOne = delegate.getStringByKeyAndLang(user, oldOne.getKey(), oldOne.getLanguageCode());

            Assert.assertNull("Failed to delete object from DB.", oldOne);
        } catch (Exception e) {
            fail(e);
        }
        */
    }
}
