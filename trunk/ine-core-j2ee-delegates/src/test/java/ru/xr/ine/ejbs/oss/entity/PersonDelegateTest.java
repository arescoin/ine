/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;
import ru.xr.ine.oss.entity.Person;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */

public class PersonDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            Person person = IdentifiableFactory.getImplementation(Person.class);
            person.setCoreDsc("Description");
            person.setName("Name");
            person.setFamilyName("familyName");
            person.setMiddleName("middleName");
            person.setAlias("alias");
            person.setFamilyNamePrefixCode(BigDecimal.ONE);
            person.setFamilyGeneration("familyGeneration");
            person.setFormOfAddress("formOfAddress");

            PersonDelegate<Person> personDelegate = DelegateFactory.getDelegateForInterface(Person.class.getName());

            BigDecimal createdId = personDelegate.createObject(user, person).getCoreId();
            person = personDelegate.getObjectById(user, createdId);
            Assert.assertEquals("Name", person.getName());
            Person cUpdated = personDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            cUpdated = personDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Name", cUpdated.getName());

            //проверка что нельзя обновить устаревшей версией
            try {
                personDelegate.updateObject(user, person);
                Assert.fail();
            } catch (Exception ex) {
            }
            personDelegate.deleteObject(user, person);
            if (personDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Person");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
