/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Ignore;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomAttributeDelegateTest /*extends BaseDelegateTest*/ {

//    private static final String CORE_DSC = "CustomAttribute CRUD-test via delegate";
//    private static final String ATTRIBUTE_NAME = "CustomAttributeDelegateTest AttributeName";
//    private BigDecimal testObjId;
//
//    @Before
//    public void init() throws Exception {
//        testObjId = ((FieldAccessDelegate)
//                Class.forName("ru.xr.ine.ejbs.core.structure.FieldAccessDelegate").newInstance()
//        ).getTableId(TestObject.class);
//    }
//
//    protected void testDelegate() {
//
//        try {
//            CustomAttributeDelegate<CustomAttribute> customAttributeDelegate =
//                    DelegateFactory.obtainDelegateByInterface(CustomAttribute.class);
//
//            CustomAttribute customAttribute = IdentifiableFactory.getImplementation(CustomAttribute.class);
//            customAttribute.setCoreDsc(CORE_DSC);
//            customAttribute.setSystemObjectId(testObjId);
//            customAttribute.setAttributeName(ATTRIBUTE_NAME);
//            customAttribute.setDataType(DataType.stringType);
//            customAttribute.setRequired(false);
//
//            //исходное количество описаний атрибутов тестовых объектов
//            int before = customAttributeDelegate.getAttributesByObject(user, TestObject.class).size();
//
//            customAttribute = customAttributeDelegate.createObject(user, customAttribute);
//
//            Assert.assertEquals(before + 1, customAttributeDelegate.getAttributesByObject(user, TestObject.class).size());
//            CustomAttribute returned = customAttributeDelegate.getObjectById(user, customAttribute.getCoreId());
//            Assert.assertEquals(ATTRIBUTE_NAME, returned.getAttributeName());
//            CustomAttribute cUpdated = customAttributeDelegate.getObjectById(user, returned.getCoreId());
//            cUpdated.setLimitations("TEST");
//
//            customAttributeDelegate.updateObject(user, cUpdated);
//
//            //проверка что нельзя обновить устаревшей версией
//            try {
//                customAttribute.setCoreDsc("");
//                customAttributeDelegate.updateObject(user, returned);
//                Assert.fail();
//            } catch (Exception ignored) {
//            }
//
//            //получение и проверка обновленного описания
//            customAttribute = customAttributeDelegate.getObjectById(user, customAttribute.getCoreId());
//            Assert.assertEquals("TEST", customAttribute.getLimitations());
//            Assert.assertFalse(customAttributeDelegate.isCustomAttributeUsed(user, customAttribute));
//
//            customAttributeDelegate.deleteObject(user, customAttribute);
//            if (customAttributeDelegate.getObjectById(user, customAttribute.getCoreId()) != null) {
//                Assert.fail("method deleteObject() has failed to remove CustomAttribute");
//            }
//            Assert.assertEquals(before, customAttributeDelegate.getAttributesByObject(user, TestObject.class).size());
//        } catch (Exception e) {
//            fail(e);
//        }
//    }
}
