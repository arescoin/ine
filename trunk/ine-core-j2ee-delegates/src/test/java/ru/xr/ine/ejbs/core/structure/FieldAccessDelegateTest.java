/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Assert;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.ejbs.BaseDelegateTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FieldAccessDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FieldAccessDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            FieldAccessDelegate testedDelegate = (FieldAccessDelegate)
                    Class.forName("ru.xr.ine.ejbs.core.structure.FieldAccessDelegate").newInstance();


            BigDecimal id = testedDelegate.getTableId(TestObject.class);
            Assert.assertTrue(id.longValue() > 0);

            Assert.assertEquals(TestObject.class.getName(), testedDelegate.getInterface(id));

        } catch (Exception e) {
            fail(e);
        }

    }
}
