/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            ConstantDelegate<Constant> constantDelegate = DelegateFactory.obtainDelegateByInterface(Constant.class);

            Constant constant = IdentifiableFactory.getImplementation(Constant.class);
            constant.setCoreDsc("Description");
            constant.setName("TestConstant");
//            constant.setType(BigDecimal.ONE);
            constant.setModifiable(true);
            constant.setNullable(true);
            constant.setDefaultValue("Test");

            BigDecimal createdId = constantDelegate.createObject(user, constant).getCoreId();

            constant = constantDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestConstant", constant.getName());
            Constant cUpdated = constantDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            constantDelegate.updateObject(user, cUpdated);

            constant = constantDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", constant.getName());

            constantDelegate.deleteObject(user, constant);
            if (constantDelegate.getObjectById(user, createdId) != null) {
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testIllegalUpdate() {
        try {
            ConstantDelegate<Constant> bean = DelegateFactory.obtainDelegateByInterface(Constant.class);
            Constant object1 = bean.getObjectById(user, Identifiable.MIN_ALLOWABLE_VAL);
            Constant object2 = bean.getObjectById(user, Identifiable.MIN_ALLOWABLE_VAL);
            object1.setCoreDsc("");
            bean.updateObject(user, object1);
            //обновление устаревшей версией невозможно
            try {
                bean.updateObject(user, object2);
                Assert.fail();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            fail(e);
        }
    }

}
