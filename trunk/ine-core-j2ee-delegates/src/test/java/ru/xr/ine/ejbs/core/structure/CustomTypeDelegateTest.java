/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CustomTypeDelegateTest /*extends BaseDelegateTest */{

//    private static final String CORE_DSC = "CustomType CRUD-test via delegate";
//    private static final String TYPE_NAME = "DelegateTest TypeName32221";
//    private BigDecimal testObjId;
//
//    @Before
//    public void init() throws Exception {
//        testObjId = ((FieldAccessDelegate)
//                Class.forName("ru.xr.ine.ejbs.core.structure.FieldAccessDelegate").newInstance()
//        ).getTableId(TestObject.class);
//    }
//
//    protected void testDelegate() {
//
//        try {
//            CustomTypeDelegate<CustomType> customTypeDelegate =
//                    DelegateFactory.getDelegateForInterface(CustomType.class.getName());
//
//            //исходное количество модифицированных типов для тестовых объектов
//            int before = customTypeDelegate.getTypesForBaseObject(user, testObjId).size();
//
//            //создание нового модифицированного типа
//            CustomType customType = IdentifiableFactory.getImplementation(CustomType.class);
//            customType.setCoreDsc(CORE_DSC);
//            customType.setBaseObject(testObjId);
//            customType.setTypeName(TYPE_NAME);
//            Collection<CustomAttribute> attributeCollection = new ArrayList<CustomAttribute>();
//
//            //создание описания настраиваемого атрибута и добавление его в модифицированный тип
//            CustomAttribute objectAttr = IdentifiableFactory.getImplementation(CustomAttribute.class);
//            objectAttr.setCoreDsc(CORE_DSC);
//            objectAttr.setSystemObjectId(testObjId);
//            objectAttr.setAttributeName("CustomtypeDelegateTest AttributeName32221");
//            objectAttr.setDataType(DataType.stringType);
//            objectAttr.setRequired(false);
//            attributeCollection.add(objectAttr);
//            customType.setCustomAttributes(attributeCollection);
//
//            CustomType created = customTypeDelegate.createObject(user, customType);
//
//            Assert.assertEquals(1, customTypeDelegate.getAllAttributes(user, created).size());
//            Assert.assertEquals(0, customTypeDelegate.getParentAttributes(user, created).size());
//            Assert.assertEquals(1, created.getCustomAttributes().size());
//            Assert.assertFalse(customTypeDelegate.isCustomTypeUsed(user, created));
//            Assert.assertEquals(before + 1, customTypeDelegate.getTypesForBaseObject(user, testObjId).size());
//
//            CustomType cUpdated = customTypeDelegate.getObjectById(user, created.getCoreId());
//            Assert.assertEquals(TYPE_NAME, cUpdated.getTypeName());
//            cUpdated.setCoreDsc(CORE_DSC + " updated");
//            cUpdated = customTypeDelegate.updateObject(user, cUpdated);
//
//            //нельзя обновить устаревший
//            try {
//                created.setCoreDsc("");
//                customTypeDelegate.updateObject(user, created);
//                Assert.fail();
//            } catch (Exception ignored) {
//            }
//            //нельзя установить в качесте предка объект с другим base object
//            try {
//                cUpdated.setParentTypeId(new BigDecimal(100));
//                Thread.sleep(100);
//                customTypeDelegate.updateObject(user, cUpdated);
//                Assert.fail();
//            } catch (GenericSystemException ex) {
//                Assert.assertEquals("Change of parentTypeId is prohibited",
//                        ex.getMessage());
//            } catch (Exception e) {
//                fail(e);
//            }
//
//            customType = customTypeDelegate.getObjectById(user, cUpdated.getCoreId());
//            Assert.assertEquals(CORE_DSC + " updated", customType.getCoreDsc());
//
//            customTypeDelegate.deleteObject(user, customType);
//            if (customTypeDelegate.getObjectById(user, customType.getCoreId()) != null) {
//                Assert.fail("method deleteObject() has failed to remove CustomType");
//            }
//            Assert.assertEquals(before, customTypeDelegate.getTypesForBaseObject(user, testObjId).size());
//        } catch (Exception e) {
//            fail(e);
//        }
//    }
}
