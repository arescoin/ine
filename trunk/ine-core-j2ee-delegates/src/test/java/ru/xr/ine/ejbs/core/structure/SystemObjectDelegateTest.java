/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import org.junit.Assert;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemObjectDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            SystemObjectDelegate<SystemObject> testedDelegate =
                    DelegateFactory.obtainDelegateByInterface(SystemObject.class);
            Assert.assertTrue(testedDelegate.getSupportedIntefaces(user).contains(TestObject.class.getName()));
        } catch (Exception e) {
            fail(e);
        }
    }
}
