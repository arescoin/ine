/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneNotActualVersionModificationException;
import ru.xr.ine.core.userpermits.NoSuchUserException;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.ejbs.BaseDelegateTest;
import ru.xr.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserDelegateTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemUserDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            SystemUserDelegate<SystemUser> testedDelegate = DelegateFactory.obtainDelegateByInterface(SystemUser.class);

            SystemUser systemUser = IdentifiableFactory.getImplementation(SystemUser.class);
            systemUser.setCoreDsc("Description");
            systemUser.setLogin("test");
            systemUser.setMd5("98f6bcd4621d373cade4e832627b4f6");//от пароля 'test'

            BigDecimal createdId = testedDelegate.createObject(user, systemUser).getCoreId();
            systemUser = testedDelegate.getObjectById(user, createdId);
            Assert.assertEquals("test", systemUser.getLogin());

            //проверка что нельзя получить пользователя передав неправильный пароль/хеш
            try {
                testedDelegate.login("test", "password");
                Assert.fail();
            } catch (NoSuchUserException ignored) {
            } catch (Exception ex) {
                fail(ex);
            }

            SystemUser cUpdated = testedDelegate.getObjectById(user, createdId);
            cUpdated.setLogin("New Login");

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Login", cUpdated.getLogin());

            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, systemUser);
                Assert.fail();
            } catch (IneNotActualVersionModificationException ignored) {
            } catch (Exception ex) {
                fail(ex);
            }
            Assert.assertEquals(cUpdated, testedDelegate.login("New Login", "test"));

            testedDelegate.deleteObject(user, systemUser);
            if (testedDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove SystemUser");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
