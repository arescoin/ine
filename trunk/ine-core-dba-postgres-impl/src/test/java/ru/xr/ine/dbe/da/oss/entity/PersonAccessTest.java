/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.entity;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;
import ru.xr.ine.oss.entity.Person;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PersonAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PersonAccessTest extends BaseVersionableTest {

    private static PersonAccess<Person> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (PersonAccess<Person>) AccessFactory.getImplementation(Person.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetPersons() {
        try {
            Collection<Person> result = access.getAllObjects();
            for (Person person : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + person);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            Person obj1 = IdentifiableFactory.getImplementation(Person.class);
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc("TestObject for CRUD-testing");
            obj1.setFamilyName("Test familyName");
            obj1.setName("Test name");
            obj1.setMiddleName("Test middleName");
            obj1.setAlias("Test alias");
            obj1.setFamilyNamePrefixCode(BigDecimal.ONE);
            obj1.setFamilyGeneration("Test familyGeneration");
            obj1.setFormOfAddress("Test formOfAddress");

            obj1 = access.createObject(obj1);
            Person obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getFamilyName(), obj2.getFamilyName());
            Assert.assertEquals("Failed to save new object in DB", obj1.getName(), obj2.getName());
            Assert.assertEquals("Failed to save new object in DB", obj1.getMiddleName(), obj2.getMiddleName());
            Assert.assertEquals("Failed to save new object in DB", obj1.getAlias(), obj2.getAlias());
            Assert.assertEquals("Failed to save new object in DB",
                    obj1.getFamilyNamePrefixCode(), obj2.getFamilyNamePrefixCode());
            Assert.assertEquals("Failed to save new object in DB",
                    obj1.getFamilyGeneration(), obj2.getFamilyGeneration());
            Assert.assertEquals("Failed to save new object in DB", obj1.getFormOfAddress(), obj2.getFormOfAddress());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Person obj1 = (Person) identifiable;
            Person obj2 = IdentifiableFactory.getImplementation(Person.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setFamilyName(obj1.getFamilyName());
            obj2.setName(obj1.getName());
            obj2.setMiddleName(obj1.getMiddleName());
            obj2.setAlias(obj1.getAlias());
            obj2.setFamilyNamePrefixCode(new BigDecimal(2));
            obj2.setFamilyGeneration(obj1.getFamilyGeneration());
            obj2.setFormOfAddress(obj1.getFormOfAddress());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB",
                    obj1.getFamilyNamePrefixCode(), obj2.getFamilyNamePrefixCode());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Person) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }
}

