/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.constants;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.ConstantValue;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantValueAccessTest extends BaseVersionableTest {

    private static ConstantValueAccess<ConstantValue> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (ConstantValueAccess<ConstantValue>) AccessFactory.getImplementation(ConstantValue.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public Identifiable testCreate() {
        try {
            ConstantValue obj1 = IdentifiableFactory.getImplementation(ConstantValue.class);

            obj1.setCoreId(new BigDecimal(1));
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setValue("TEST_CONSTANT_VALUE");

            obj1 = access.createObject(obj1);
            ConstantValue obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getValue(), obj2.getValue());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            ConstantValue oldOne = (ConstantValue) identifiable;
            ConstantValue newOne = IdentifiableFactory.getImplementation(ConstantValue.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());
            newOne.setValue(oldOne.getValue() + "_UPD");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(oldOne.getCoreId());

            Assert.assertEquals("Failed to modify object on DB.", oldOne.getValue(), newOne.getValue());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((ConstantValue) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getConstantValueValues() {
        try {
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + access.getAllObjects());
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getConstantValueValue() {
        try {
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + access.getObjectById(new BigDecimal(1)));
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
