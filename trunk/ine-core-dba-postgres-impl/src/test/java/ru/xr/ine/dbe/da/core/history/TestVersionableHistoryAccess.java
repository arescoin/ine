/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import org.junit.Test;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.core.history.VersionableHistory;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestVersionableHistoryAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class TestVersionableHistoryAccess extends BaseTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testGetHistoryById() {
        try {
            ((IdentifiableHistoryAccess)
                    AccessFactory.getImplementation(IdentifiableHistory.class)).getObjectById(new BigDecimal(1));
        } catch (UnsupportedOperationException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getAllIdentifiableHistory() {
        try {
            @SuppressWarnings({"unchecked"})
            IdentifiableHistoryAccess<IdentifiableHistory> access = (IdentifiableHistoryAccess<IdentifiableHistory>)
                    AccessFactory.getImplementation(IdentifiableHistory.class);
            Collection<IdentifiableHistory> history = access.getAllObjects();
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }

            if (history.isEmpty()) {
                return;
            }
            IdentifiableHistory item1 = history.iterator().next();
            history = access.getActionsHistoryByIds(item1.getCoreId(), item1.getEntityId());
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getAllVersionableHistory() {
        try {
            @SuppressWarnings({"unchecked"})
            VersionableHistoryAccess<VersionableHistory> access = (VersionableHistoryAccess<VersionableHistory>)
                    AccessFactory.getImplementation(VersionableHistory.class);
            Collection<VersionableHistory> history = access.getAllObjects();
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }

            if (history.isEmpty()) {
                return;
            }
            VersionableHistory item1 = history.iterator().next();
            history = access.getActionsHistoryByIds(item1.getCoreId(), item1.getEntityId());
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }
        } catch (Exception e) {
            fail(e);
        }
    }

}
