/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.dic;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;
import ru.xr.rims.services.StorageFilter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryEntryAccessTest extends BaseVersionableTest {

    private static DictionaryEntryAccess<DictionaryEntry> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(DictionaryEntry.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public void testCRUD() {
        testDelete(testUpdate(testRetrieve(testCreate())));
    }

    @Override
    public Identifiable testCreate() {
        try {
            DictionaryEntry obj1 = IdentifiableFactory.getImplementation(DictionaryEntry.class);
            obj1.setCoreId(new BigDecimal(9999));
            obj1.setDictionaryId(BigDecimal.ONE);

            try {
                obj1 = access.createObject(obj1);
            } catch (UnsupportedOperationException e) {
                return obj1;
            }
            Assert.fail();
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            Assert.assertNull(access.getObjectById(access.getSyntheticId((DictionaryEntry) identifiable).getIdValues()));
        } catch (Exception e) {
            fail(e);
        }
        return identifiable;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            DictionaryEntry oldOne = (DictionaryEntry) identifiable;
            DictionaryEntry newOne = IdentifiableFactory.getImplementation(DictionaryEntry.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setDictionaryId(oldOne.getDictionaryId());

            try {
                newOne = access.updateObject(oldOne, newOne);
            } catch (UnsupportedOperationException e) {
                return newOne;
            }
            Assert.fail();
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            ((DictionaryEntry) identifiable).setCoreFd(FieldAccess.getCurrentDate());
            ((DictionaryEntry) identifiable).setCoreTd(FieldAccess.getSystemTd());
            access.deleteObject((DictionaryEntry) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testDeleteWithCheck() {
        try {
            DictionaryEntry entry = access.getObjectById(new BigDecimal(13), new BigDecimal(1));
            access.deleteObject(entry);
            Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllDictionaryEntries() {
        try {
            Collection<DictionaryEntry> result = access.getAllObjects();
            int totalEntries = result.size();
            logger.info("DicEntries: " + totalEntries);
            @SuppressWarnings({"unchecked"}) DictionaryAccess<Dictionary> accessDictionary =
                    (DictionaryAccess<Dictionary>) AccessFactory.getImplementation(Dictionary.class);
            int sum = 0;
            Collection<Dictionary> dictionaries = accessDictionary.getAllObjects();

            System.out.println(dictionaries.getClass().getName());

            for (Dictionary dictionary : dictionaries) {
                sum += access.getAllEntriesForDictionary(dictionary.getCoreId()).size();
            }
            Assert.assertEquals(totalEntries, sum);
        } catch (Exception e) {
            fail(e);
        }
    }


    @SuppressWarnings({"unchecked"})
    @Test
    public void testSearch() {

        try {
            Collection<DictionaryEntry> manualEntries = new ArrayList<DictionaryEntry>();

            Collection<DictionaryEntry> dictionaryEntries = access.getAllObjects();
            Assert.assertTrue("No dictionary entries found!", !dictionaryEntries.isEmpty());

            for (DictionaryEntry dictionaryEntry : dictionaryEntries) {
                if (dictionaryEntry.getCoreId().equals(BigDecimal.ONE)) {
                    manualEntries.add(dictionaryEntry);
                }
            }

            DictionaryAccess<Dictionary> dictionaryAccess =
                    (DictionaryAccess<Dictionary>) AccessFactory.getImplementation(Dictionary.class);

            SearchCriteriaProfile dicCriteriaProfile = dictionaryAccess.getSearchCriteriaProfile();
            SearchCriterion dicCriterion =
                    dicCriteriaProfile.getSearchCriterion(Dictionary.CORE_ID, DataType.bigDecimalType);

            dicCriterion.setCriterionRule(CriteriaRule.in);
            HashSet<SearchCriterion> dicSearchCriteria = new HashSet<SearchCriterion>();
            StorageFilter dicFilter =
                    dictionaryAccess.createStorageFilter(dicSearchCriteria, dicCriterion, "");

            SearchCriteriaProfile entryProfile = access.getSearchCriteriaProfile();
            SearchCriterion searchCriterion =
                    entryProfile.getSearchCriterion(DictionaryEntry.CORE_ID, CriteriaRule.equals, BigDecimal.ONE);

            Set<SearchCriterion> entryCrit = new HashSet<SearchCriterion>();
            entryCrit.add(searchCriterion);
            StorageFilter entryFilter =
                    access.createStorageFilter(entryCrit, searchCriterion, DictionaryEntry.DIC_ID);


            StorageFilter[] filters = new StorageFilter[2];
            filters[1] = dicFilter;
            filters[0] = entryFilter;

            // подготовим кеш...
            Set<SearchCriterion> initSet = new HashSet<SearchCriterion>();
            initSet.add(dicCriteriaProfile.getSearchCriterion("coreId", CriteriaRule.equals, BigDecimal.ONE));
            dictionaryAccess.searchObjects(initSet);
            initSet.clear();
            initSet.add(searchCriterion);
            access.searchObjects(initSet);

            // сам поисковый запрос...
            Collection<Dictionary> searchResult = dictionaryAccess.searchObjects(filters);

            HashSet<BigDecimal> decimals = new HashSet<BigDecimal>();
            for (DictionaryEntry manualEntry : manualEntries) {
                decimals.add(manualEntry.getDictionaryId());
            }

            Collection<Dictionary> controlDictionaries = new ArrayList<Dictionary>();

            for (BigDecimal decimal : decimals) {
                controlDictionaries.add(dictionaryAccess.getObjectById(decimal));
            }

            int dicCount = dictionaryAccess.getObjectCount(filters);
            Assert.assertTrue(dicCount == controlDictionaries.size()
                    && controlDictionaries.size() == searchResult.size()
                    && controlDictionaries.containsAll(searchResult)
                    && searchResult.containsAll(controlDictionaries));


        } catch (Exception e) {
            fail(e);
        }
    }

}
