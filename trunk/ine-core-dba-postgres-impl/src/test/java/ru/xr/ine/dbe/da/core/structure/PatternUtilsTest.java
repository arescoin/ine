/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.dic.DictionaryEntryAccess;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import static ru.xr.ine.core.structure.SystemObjectPattern.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PatternUtilsTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PatternUtilsTest extends BaseTest {

    private static SystemObjectAccess<SystemObject> objectAccess;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            objectAccess = (SystemObjectAccess<SystemObject>) AccessFactory.getImplementation(SystemObject.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetObjectByPattern() {
        try {
            BigDecimal columnId = FieldAccess.getColumnId(
                    objectAccess.getTableName(), objectAccess.getInstanceFields().get(SystemObject.TYPE));
            SystemObject column = objectAccess.getObjectById(columnId);
            Identifiable object = PatternUtils.getObjectByPattern(column.getPattern(), BigDecimal.ONE);

            Assert.assertTrue("Failed to get object by pattern[" + column.getPattern().toString() +
                    "] in systemObject[" + columnId + "]", DictionaryEntry.class.isAssignableFrom(object.getClass()));
            Assert.assertEquals("Wrong value was obtained by pattern", new BigDecimal(4),
                    ((DictionaryEntry) object).getDictionaryId());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetPatternByObject() {
        try {
            BigDecimal columnId = FieldAccess.getColumnId(
                    objectAccess.getTableName(), objectAccess.getInstanceFields().get(SystemObject.TYPE));
            SystemObject column = objectAccess.getObjectById(columnId);

            @SuppressWarnings({"unchecked"})
            DictionaryEntry entry = ((DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(
                    DictionaryEntry.class)).getObjectById(new BigDecimal(4), new BigDecimal(1));
            Map<SystemObjectPattern, BigDecimal> map = PatternUtils.getPatternByObject(entry);

            Assert.assertEquals("Failed to get pattern from object",
                    column.getPattern(), map.keySet().iterator().next());

        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetAvailableObjects() {
        try {
            @SuppressWarnings({"unchecked"}) DictionaryEntryAccess<DictionaryEntry> dea =
                    (DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(DictionaryEntry.class);
            String tableName = dea.getTableName();
            Map<String, String> fields = dea.getInstanceFields();
            BigDecimal dicColumnId = FieldAccess.getColumnId(tableName, fields.get(DictionaryEntry.DIC_ID));//DIC_N
            BigDecimal codeColumnId = FieldAccess.getColumnId(tableName, fields.get(DictionaryEntry.CORE_ID));//CODE

            String replacement = "#";
            String codeColumn = LEFT_BRACE + codeColumnId + EL_SEPERATOR + replacement + RIGHT_BRACE;
            String dicColumn = LEFT_BRACE + dicColumnId + EL_SEPERATOR + replacement + RIGHT_BRACE;

            // отбираем доступные статьи по паттерну [DIC_N:9][CODE:77]CODE - должна быть 1 статья
            String patternString = dicColumn.replace(replacement, "9") +
                    codeColumn.replace(replacement, "77") + codeColumnId;
            getAvailableObjects(patternString, 1);

            // отбираем доступные статьи по паттерну [DIC_N:9][CODE:1-7]CODE - должно быть 7 статей
            patternString = dicColumn.replace(replacement, "9") + codeColumn.replace(replacement, "1-7") + codeColumnId;
            getAvailableObjects(patternString, 7);

        } catch (Exception e) {
            fail(e);
        }
    }

    private void getAvailableObjects(String patternString, long expectedSize) throws CoreException {
        long time = System.currentTimeMillis();
        Collection entries = PatternUtils.getAvailableObjects(new SystemObjectPattern(patternString));
        System.out.println("Search time: " + (System.currentTimeMillis() - time) + ", pattern: " + patternString);
        Assert.assertEquals("Get available objects by pattern " + patternString + " failed",
                expectedSize, entries.size());
    }

    @Test
    public void testGetObjectsByPattern() {
        try {
            @SuppressWarnings({"unchecked"}) DictionaryEntryAccess<DictionaryEntry> dea =
                    (DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(DictionaryEntry.class);
            String tableName = dea.getTableName();
            Map<String, String> fields = dea.getInstanceFields();
            BigDecimal dicColumnId = FieldAccess.getColumnId(tableName, fields.get(DictionaryEntry.DIC_ID));//DIC_N
            BigDecimal codeColumnId = FieldAccess.getColumnId(tableName, fields.get(DictionaryEntry.CORE_ID));//CODE

            String replacement = "#";
            String codeColumn = LEFT_BRACE + codeColumnId + EL_SEPERATOR + replacement + RIGHT_BRACE;
            String dicColumn = LEFT_BRACE + dicColumnId + EL_SEPERATOR + replacement + RIGHT_BRACE;
            BigDecimal dicId = new BigDecimal(9);

            // отбираем статьи по паттерну [CODE:77]DIC_N - должна быть 1 статья
            String patternString = codeColumn.replace(replacement, "77") + dicColumnId;
            getObjects(patternString, dicId, 1);

            // отбираем статьи по паттерну [CODE:70-80]DIC_N - должно быть 11 статей
            patternString = codeColumn.replace(replacement, "70-80") + dicColumnId;
            getObjects(patternString, dicId, 11);

            // отбираем статьи по паттерну [CODE:70-80,89]DIC_N - должно быть 12 статей
            patternString = codeColumn.replace(replacement, "70-80,89") + dicColumnId;
            getObjects(patternString, dicId, 12);

            // отбираем статьи по паттерну [CODE:70-80,77]DIC_N - должно быть 11 статей
            patternString = codeColumn.replace(replacement, "70-80,77") + dicColumnId;
            getObjects(patternString, dicId, 11);

            // отбираем статьи по паттерну [CODE:70-80,20-25,89]DIC_N - должно быть 18 статей
            patternString = codeColumn.replace(replacement, "70-80,20-25,89") + dicColumnId;
            getObjects(patternString, dicId, 18);

            // отбираем статьи по паттерну [DIC_N:1-7]CODE - должно быть 7 статей с кодом 1
            patternString = dicColumn.replace(replacement, "1-7") + codeColumnId;
            getObjects(patternString, BigDecimal.ONE, 7);

            // отбираем статьи по паттерну [DIC_N:9][CODE:1-5]CODE - должна быть 1 статья с кодом 1
            patternString = dicColumn.replace(replacement, "9") + codeColumn.replace(replacement, "1-5") + codeColumnId;
            getObjects(patternString, BigDecimal.ONE, 1);

            // отбираем статьи по паттерну [DIC_N:9][CODE:2-5]CODE - должно быть 0 статей с кодом 1
            patternString = dicColumn.replace(replacement, "9") + codeColumn.replace(replacement, "2-5") + codeColumnId;
            getObjects(patternString, BigDecimal.ONE, 0);

        } catch (Exception e) {
            fail(e);
        }
    }

    private void getObjects(String patternString, BigDecimal searchVal, long expectedSize) throws CoreException {
        long time = System.currentTimeMillis();
        Collection entries = PatternUtils.getObjectsByPattern(new SystemObjectPattern(patternString), searchVal);
        System.out.println("Search time: " + (System.currentTimeMillis() - time) +
                ", pattern: " + patternString + ", value: " + searchVal);
        Assert.assertEquals("Search by pattern " + patternString + " failed", expectedSize, entries.size());
    }
}
