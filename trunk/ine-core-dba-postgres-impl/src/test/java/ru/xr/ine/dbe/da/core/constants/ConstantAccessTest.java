/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.constants;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ConstantAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantAccessTest extends BaseVersionableTest {

    private static ConstantAccess<Constant> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (ConstantAccess<Constant>) AccessFactory.getImplementation(Constant.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            Constant obj1 = IdentifiableFactory.getImplementation(Constant.class);

            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setName("TEST_CONSTANT");
            obj1.setModifiable(true);
            obj1.setNullable(true);
//            obj1.setType(BigDecimal.ONE);
            obj1.setDefaultValue("TEST");

            obj1 = access.createObject(obj1);
            Constant obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getName(), obj2.getName());
            Assert.assertEquals("Failed to save object in DB.", obj1.isModifiable(), obj2.isModifiable());
            Assert.assertEquals("Failed to save object in DB.", obj1.isNullable(), obj2.isNullable());
            Assert.assertEquals("Failed to save object in DB.", obj1.getType(), obj2.getType());
            Assert.assertEquals("Failed to save object in DB.", obj1.getDefaultValue(), obj2.getDefaultValue());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Constant oldOne = (Constant) identifiable;
            Constant newOne = IdentifiableFactory.getImplementation(Constant.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());
            newOne.setName(oldOne.getName());
            newOne.setModifiable(oldOne.isModifiable());
            newOne.setNullable(oldOne.isNullable());
//            newOne.setType(oldOne.getType());
            newOne.setDefaultValue(oldOne.getDefaultValue() + " UPD");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(oldOne.getCoreId());

            Assert.assertEquals("Failed to modify object on DB.", oldOne.getDefaultValue(), newOne.getDefaultValue());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Constant) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getConstants() {
        try {
            for (Constant constant : access.getAllObjects()) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "constant: " + constant);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
