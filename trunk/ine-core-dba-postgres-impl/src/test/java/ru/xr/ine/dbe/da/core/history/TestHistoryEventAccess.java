/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.history.HistoryEvent;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.utils.BaseTest;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestHistoryEventAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class TestHistoryEventAccess extends BaseTest {

    @Test
    public void testAllProcessed() {
        try {
            @SuppressWarnings({"unchecked"}) HistoryEventAccess<HistoryEvent> access =
                    (HistoryEventAccess<HistoryEvent>) AccessFactory.getImplementation(HistoryEvent.class);

            int totalProcessed = 0;
            Collection<HistoryEvent> eventsForProcess = access.getEventsForProcess();
            while (eventsForProcess.size() > 0) {
                System.out.println("events to proceed: " + eventsForProcess.size());
                for (HistoryEvent historyEvent : eventsForProcess) {
                    access.markAsProcessed(historyEvent);
                }
                totalProcessed += eventsForProcess.size();
                eventsForProcess = access.getEventsForProcess();
            }
            System.out.println("total events processed: " + totalProcessed);

            Collection<HistoryEvent> processed = access.getProcessedEvents();
            System.out.println("all processed events in DB: " + processed.size());

            Assert.assertTrue("There are more events for processing", access.getEventsForProcess().isEmpty());
        } catch (CoreException e) {
            fail(e);
        }
    }
}
