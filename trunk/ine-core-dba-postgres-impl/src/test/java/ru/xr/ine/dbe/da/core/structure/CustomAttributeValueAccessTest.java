/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import org.junit.*;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomAttributeValue;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.dic.DictionaryEntryAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomAttributeValueAccessTest extends BaseVersionableTest {

    private static CustomAttributeValueAccess<CustomAttributeValue> access;
    private static CustomAttribute attribute5;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            attribute5 = ((CustomAttributeAccess<CustomAttribute>)
                    AccessFactory.getImplementation(CustomAttribute.class)).getObjectById(new BigDecimal(5));
            access = (CustomAttributeValueAccess<CustomAttributeValue>)
                    AccessFactory.getImplementation(CustomAttributeValue.class);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
        tableId = attribute5.getSystemObjectId();
    }

    @Override
    public Identifiable testCreate() {
        try {
            CustomAttributeValue obj1 = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            obj1.setCoreId(attribute5.getCoreId());// это булевый атрибут для TestObject'а
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc(null);
            obj1.setEntityId(new BigDecimal(3));
            obj1.setValue(true);

            obj1 = access.createObject(obj1);
            CustomAttributeValue obj2 = access.getObjectById(access.getSyntheticId(obj1).getIdValues());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreId(), obj2.getCoreId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getEntityId(), obj2.getEntityId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getValue(), obj2.getValue());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(access.getSyntheticId((CustomAttributeValue) identifiable).getIdValues());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            CustomAttributeValue oldOne = (CustomAttributeValue) identifiable;
            CustomAttributeValue newOne = IdentifiableFactory.getImplementation(CustomAttributeValue.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());
            newOne.setEntityId(oldOne.getEntityId());
            newOne.setValue(false);

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(access.getSyntheticId(oldOne).getIdValues());

            Assert.assertEquals("Failed to save update object in DB", oldOne.getValue(), newOne.getValue());

            return newOne;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((CustomAttributeValue) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllValues() {
        try {
            for (CustomAttributeValue value : access.getAllObjects()) {
                logger.log(Level.FINEST, "value: " + value);
            }
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test
    public void testErrorChecks() {
        CustomAttribute attribute = this.createAttribute();
        try {
            CustomAttributeValue obj1 = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            obj1.setCoreId(attribute.getCoreId());
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc(null);
            obj1.setEntityId(new BigDecimal(3));

            try {
                access.createObject(obj1);
                Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
            } catch (IneIllegalArgumentException e) {
                //does nothing
            }

            obj1.setValue(new BigDecimal(9999));
            try {
                access.createObject(obj1);
                Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
            } catch (IneIllegalArgumentException e) {
                //does nothing
            }

            obj1.setValue(new BigDecimal(1));
            obj1 = access.createObject(obj1);

            access.deleteObject(obj1);
        } catch (Exception e) {
            fail(e);
        } finally {
            this.deleteAttribute(attribute);
        }
    }

    @SuppressWarnings({"unchecked"})
    private CustomAttribute createAttribute() {
        try {
            BigDecimal testObjId = FieldAccess.getTableId(TestObject.class);

            DictionaryEntryAccess<DictionaryEntry> entryAccess =
                    (DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(DictionaryEntry.class);
            DictionaryEntry entry = entryAccess.getObjectById(new BigDecimal(6), new BigDecimal(1));

            SystemObjectPattern pattern = PatternUtils.getPatternByObject(entry).keySet().iterator().next();

            CustomAttribute attribute = IdentifiableFactory.getImplementation(CustomAttribute.class);
            attribute.setCoreFd(FieldAccess.getSystemFd());
            attribute.setCoreTd(FieldAccess.getSystemTd());
            attribute.setCoreDsc("CustomAttributeValue tests via data-access");
            attribute.setSystemObjectId(testObjId);
            attribute.setAttributeName("CustomAttributeValueAccessTest AttributeName");
            attribute.setDataType(DataType.bigDecimalType);
            attribute.setRequired(true);
            attribute.setPattern(pattern);
            attribute.setLimitations("TEST");

            return ((CustomAttributeAccess<CustomAttribute>)
                    AccessFactory.getImplementation(CustomAttribute.class)).createObject(attribute);
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @SuppressWarnings({"unchecked"})
    private void deleteAttribute(CustomAttribute attribute) {
        try {
            ((CustomAttributeAccess<CustomAttribute>)
                    AccessFactory.getImplementation(CustomAttribute.class)).deleteObject(attribute);
        } catch (Exception e) {
            fail(e);
        }
    }
}
