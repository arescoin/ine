/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.history.ActionAttribute;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.core.structure.test.TestObjectAccess;
import ru.xr.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ActionAttributeAccessTest extends BaseTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testGetHistoryById() {
        try {
            ((ActionAttributeAccess) AccessFactory.getImplementation(ActionAttribute.class)).
                    getObjectById(new BigDecimal(1));
        } catch (UnsupportedOperationException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @SuppressWarnings({"unchecked"})
    @Test
    public void testGetActionAttributes() {
        String attrName = "TestAttributeName";
        String attrValue = "TestAttributeValue";
        try {
            if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS).isEnabled() ||
                    !FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_ACTION_ATTRIBUTES).isEnabled()) {
                return;
            }

            BigDecimal tableId = FieldAccess.getTableId(TestObject.class);

            TestObjectAccess<TestObject> testObjectAccess =
                    (TestObjectAccess) AccessFactory.getImplementation(TestObject.class);

            TestObject testObject = IdentifiableFactory.getImplementation(TestObject.class);
            testObject.setCoreId(tableId);
            testObject.setCoreFd(FieldAccess.getSystemFd());
            testObject.setCoreTd(FieldAccess.getSystemTd());
            testObject.setCoreDsc("Test ActionAttibutes for IdentifiableHistory");

            UserHolder.getUser().setUserAttribute(attrName, attrValue);

            testObject = testObjectAccess.createObject(testObject);

            IdentifiableHistoryAccess<IdentifiableHistory> historyAccess =
                    (IdentifiableHistoryAccess) AccessFactory.getImplementation(IdentifiableHistory.class);
            Collection<IdentifiableHistory> history = historyAccess.getActionsHistoryByIds(
                    tableId, testObjectAccess.getSyntheticId(testObject));
            Assert.assertEquals(1, history.size());

            ActionAttributeAccess<ActionAttribute> attributeAccess =
                    (ActionAttributeAccess) AccessFactory.getImplementation(ActionAttribute.class);

            Collection<ActionAttribute> attributes = attributeAccess.getActionAttributes(history.iterator().next());
            Assert.assertEquals(1, attributes.size());
            ActionAttribute attribute = attributes.iterator().next();
            Assert.assertEquals(attrName, attribute.getAttributeName());
            Assert.assertEquals(attrValue, attribute.getAttributeValue());

            testObjectAccess.deleteObject(testObject);

            UserHolder.getUser().clearUserAttributes();
        } catch (Exception e) {
            fail(e);
        }
    }
}
