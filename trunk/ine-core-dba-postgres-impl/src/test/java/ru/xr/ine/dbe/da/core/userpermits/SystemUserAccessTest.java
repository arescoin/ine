/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;
import ru.xr.ine.utils.MD5;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemUserAccessTest extends BaseVersionableTest {

    private static SystemUserAccess<SystemUser> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (SystemUserAccess<SystemUser>) AccessFactory.getImplementation(SystemUser.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testLogin() {
        try {
            Assert.assertEquals(BigDecimal.ONE, access.login("root", MD5.calculate("root")).getCoreId());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            SystemUser obj1 = IdentifiableFactory.getImplementation(SystemUser.class);
            obj1.setActive(true);
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setLogin("test");
            obj1.setMd5("098f6bcd4621d373cade4e832627b4f6");
            obj1 = access.createObject(obj1);
            SystemUser obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.isActive(), obj2.isActive());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            SystemUser obj1 = (SystemUser) identifiable;
            SystemUser obj2 = IdentifiableFactory.getImplementation(SystemUser.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setActive(!obj1.isActive());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setLogin(obj1.getLogin());
            obj2.setMd5(obj1.getMd5());
            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", false, obj2.isActive());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((SystemUser) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllSystemUsers() {
        try {
            Collection<SystemUser> result = access.getAllObjects();
            for (SystemUser o : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + o);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}

