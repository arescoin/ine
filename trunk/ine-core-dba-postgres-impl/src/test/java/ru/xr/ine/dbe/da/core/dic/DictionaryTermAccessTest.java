/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.dic;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryTermAccessTest extends BaseVersionableTest {

    private static DictionaryTermAccess<DictionaryTerm> access;
    private static DictionaryEntryAccess<DictionaryEntry> entryAccess;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (DictionaryTermAccess<DictionaryTerm>) AccessFactory.getImplementation(DictionaryTerm.class);
            entryAccess = (DictionaryEntryAccess) AccessFactory.getImplementation(DictionaryEntry.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateWithParentTermWithoutParentDictionary() {
        try {
            DictionaryTerm obj1 = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            obj1.setCoreId(new BigDecimal(9999));
            obj1.setCoreDsc("Test dictionaryTerm for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setDictionaryId(BigDecimal.ONE);
            obj1.setLangCode(BigDecimal.ONE);
            obj1.setParentTermId(BigDecimal.ONE);
            obj1.setTerm("test dictionaryTermTerm");

            access.createObject(obj1);

            Assert.fail("Failed to obtain exception on creation with errors");
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateWithoutRequiredParentTerm() {
        try {
            DictionaryTerm obj1 = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            obj1.setCoreId(new BigDecimal(9999));
            obj1.setCoreDsc("Test dictionaryTerm for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setDictionaryId(new BigDecimal(9));
            obj1.setLangCode(BigDecimal.ONE);
            obj1.setTerm("test dictionaryTermTerm");

            access.createObject(obj1);

            Assert.fail("Failed to obtain exception on creation with errors");
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateNonExistentParentTerm() {
        try {
            DictionaryTerm obj1 = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            obj1.setCoreId(new BigDecimal(9999));
            obj1.setCoreDsc("Test dictionaryTerm for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setDictionaryId(new BigDecimal(9));
            obj1.setParentTermId(new BigDecimal(9999));
            obj1.setLangCode(BigDecimal.ONE);
            obj1.setTerm("test dictionaryTermTerm");

            access.createObject(obj1);

            Assert.fail("Failed to obtain exception on creation with errors");
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            DictionaryTerm obj1 = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            obj1.setCoreId(new BigDecimal(9999));
            obj1.setCoreDsc("Test dictionaryTerm for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setDictionaryId(BigDecimal.ONE);
            obj1.setLangCode(BigDecimal.ONE);
            obj1.setTerm("test dictionaryTermTerm");

            obj1 = access.createObject(obj1);
            DictionaryTerm obj2 = access.getObjectById(access.getSyntheticId(obj1).getIdValues());

            Assert.assertEquals("Failed to save object in DB.", null, obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getDictionaryId(), obj2.getDictionaryId());
            Assert.assertEquals("Failed to save object in DB.", obj1.getLangCode(), obj2.getLangCode());
            Assert.assertEquals("Failed to save object in DB.", obj1.getParentTermId(), obj2.getParentTermId());
            Assert.assertEquals("Failed to save object in DB.", obj1.getTerm(), obj2.getTerm());

            DictionaryEntry entry = entryAccess.getObjectById(entryAccess.getSyntheticId(obj2).getIdValues());

            Assert.assertNotNull("Failed to put object in cache", entry);
            Assert.assertEquals("Failed to put object in cache", obj2.getCoreId(), entry.getCoreId());
            Assert.assertEquals("Failed to put object in cache", obj2.getDictionaryId(), entry.getDictionaryId());
            Assert.assertEquals("Failed to put object in cache", obj2.getParentTermId(), entry.getParentTermId());
            Assert.assertEquals("Failed to put object in cache", obj2.getCoreDsc(), entry.getCoreDsc());
            Assert.assertEquals("Failed to put object in cache", FieldAccess.getSystemFd(), entry.getCoreFd());
            Assert.assertEquals("Failed to put object in cache", FieldAccess.getSystemTd(), entry.getCoreTd());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(access.getSyntheticId((DictionaryTerm) identifiable).getIdValues());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            DictionaryTerm oldOne = (DictionaryTerm) identifiable;
            DictionaryTerm newOne = IdentifiableFactory.getImplementation(DictionaryTerm.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc() + " updated");
            newOne.setDictionaryId(oldOne.getDictionaryId());
            newOne.setLangCode(oldOne.getLangCode());
            newOne.setParentTermId(oldOne.getParentTermId());
            newOne.setTerm(oldOne.getTerm() + " updated");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(access.getSyntheticId(oldOne).getIdValues());

            Assert.assertEquals("Failed to update object in DB.", oldOne.getTerm(), newOne.getTerm());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((DictionaryTerm) identifiable);

            Assert.assertNull("Failed to remove object from cache",
                    entryAccess.getObjectById(entryAccess.getSyntheticId((DictionaryEntry) identifiable).getIdValues()));
        } catch (Exception e) {
            fail(e);
        }
    }
}
