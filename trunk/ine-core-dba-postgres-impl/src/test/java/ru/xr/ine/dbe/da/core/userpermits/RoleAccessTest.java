/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RoleAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RoleAccessTest extends BaseVersionableTest {

    private static RoleAccess<Role> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (RoleAccess<Role>) AccessFactory.getImplementation(Role.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            Role obj1 = IdentifiableFactory.getImplementation(Role.class);
            obj1.setName("TEST_ROLE");
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            obj1 = access.createObject(obj1);
            Role obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getName(), obj2.getName());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Role obj1 = (Role) identifiable;
            Role obj2 = IdentifiableFactory.getImplementation(Role.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setName(obj1.getName() + "_UPD");
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.getName(), obj2.getName());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Role) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllRoles() {
        try {
            Collection<Role> result = access.getAllObjects();
            for (Role role : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + role);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
