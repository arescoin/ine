/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import org.junit.*;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.structure.CustomAttributeDefVal;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomAttributeDefValAccessTest extends BaseVersionableTest {

    private static CustomAttributeDefValAccess<CustomAttributeDefVal> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (CustomAttributeDefValAccess<CustomAttributeDefVal>)
                    AccessFactory.getImplementation(CustomAttributeDefVal.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        this.manualSetId = true;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testIncorrectValue() {
        try {
            CustomAttributeDefVal obj1 = IdentifiableFactory.getImplementation(CustomAttributeDefVal.class);
            obj1.setCoreId(new BigDecimal(5));
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc("TestObject for CRUD-tests");
            obj1.setString("TestValue");

            access.createObject(obj1);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {

            CustomAttributeDefVal obj1 = IdentifiableFactory.getImplementation(CustomAttributeDefVal.class);
            obj1.setCoreId(new BigDecimal(5));
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc("TestObject for CRUD-tests");
            obj1.setBoolean(true);

            obj1 = access.createObject(obj1);
            CustomAttributeDefVal obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", null, obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getBoolean(), obj2.getBoolean());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            CustomAttributeDefVal obj1 = (CustomAttributeDefVal) identifiable;
            CustomAttributeDefVal obj2 = IdentifiableFactory.getImplementation(CustomAttributeDefVal.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setBoolean(!obj1.getBoolean());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.getBoolean(), obj2.getBoolean());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((CustomAttributeDefVal) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }
}
