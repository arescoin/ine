/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.ipaddress;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.ipaddress.IPAddress;
import ru.xr.ine.core.ipaddress.IPAddressValue;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.utils.BaseVersionableTest;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueAccessTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IPAddressValueAccessTest extends BaseVersionableTest {

    private static IPAddressValueAccess<IPAddressValue> access;
    private static IPAddress ADDRESS1;
    private static IPAddress ADDRESS2;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            ADDRESS1 = IPAddress.parse("192.168.1.50");
            ADDRESS2 = IPAddress.parse("192.168.1.51");
            access = (IPAddressValueAccess<IPAddressValue>) AccessFactory.getImplementation(IPAddressValue.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            IPAddressValue obj1 = IdentifiableFactory.getImplementation(IPAddressValue.class);
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setIpAddress(ADDRESS1);

            obj1 = access.createObject(obj1);
            IPAddressValue obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getIpAddress(), obj2.getIpAddress());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            IPAddressValue obj1 = (IPAddressValue) identifiable;
            IPAddressValue obj2 = IdentifiableFactory.getImplementation(IPAddressValue.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setIpAddress(ADDRESS2);

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.getIpAddress(), obj2.getIpAddress());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((IPAddressValue) identifiable);
        } catch (Exception e) {
            //такая фигня получается из-за того, что IPAddressValue используется в кастомном атрибуте
            //ossj-объекта, который на момент теста мавеном не может быть загружен (нет нужной либы)
            try {
                access.deleteObject((IPAddressValue) identifiable);
            } catch (Exception e2) {
                fail(e2);
            }
        }
    }
}
