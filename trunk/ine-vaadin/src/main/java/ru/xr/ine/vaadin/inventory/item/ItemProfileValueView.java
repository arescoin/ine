/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.item;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
//import com.vaadin.ui.Table;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.oss.inventory.item.ItemProfileValue;
import ru.xr.ine.vaadin.AdminUiServlet;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileValueView.java 62 2017-05-06 10:12:20Z xerror $"
 */
public class ItemProfileValueView extends VerticalLayout implements View {

    public static final String VIEW_KEY = ItemProfileValue.class.getName();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();

        Collection<ItemProfileValue> itms = ((AdminUiServlet) VaadinServlet.getCurrent()).getItemProfileValues();

        Grid<ItemProfileValue> grid = new Grid<>("Item Profile Value");
        grid.setSizeFull();

        Grid.Column<ItemProfileValue, BigDecimal> column = grid.addColumn(ItemProfileValue::getCoreId);
        column.setCaption("ID");
        grid.addColumn(ItemProfileValue::getItemProfileId).setCaption("Profile");
        grid.addColumn(ItemProfileValue::getValueDescriptorId).setCaption("Descriptor");
        grid.addColumn(ItemProfileValue::getPattern).setCaption("Pattern");
        grid.addColumn(ItemProfileValue::getCoreDsc).setCaption("Description");
        grid.addColumn(ItemProfileValue::getCoreFd).setCaption("Fd");
        grid.addColumn(ItemProfileValue::getCoreTd).setCaption("Td");

        grid.sort(column, SortDirection.ASCENDING);
        grid.setItems(itms);

        this.addComponent(grid);
    }

}