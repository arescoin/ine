/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.*;
import ru.xr.ine.core.*;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.core.language.SysLanguage;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.core.userpermits.UserRole;
import ru.xr.ine.ejbs.core.dic.DictionaryBean;
import ru.xr.ine.ejbs.core.dic.DictionaryEntryBean;
import ru.xr.ine.ejbs.core.dic.DictionaryTermBean;
import ru.xr.ine.ejbs.core.structure.SystemObjectBean;
import ru.xr.ine.ejbs.core.structure.SystemObjectPatternBean;
import ru.xr.ine.ejbs.core.userpermits.RoleBean;
import ru.xr.ine.ejbs.core.userpermits.SystemUserBean;
import ru.xr.ine.ejbs.core.userpermits.UserRoleBean;
import ru.xr.ine.ejbs.oss.inventory.item.*;
import ru.xr.ine.oss.inventory.item.*;
import ru.xr.ine.utils.MD5;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;
import ru.xr.ine.vaadin.inventory.utils.ItemVO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AdminUiServlet.java 62 2017-05-06 10:12:20Z xerror $"
 */
@WebServlet(urlPatterns = "/*", name = "AdminUiServlet")
@VaadinServletConfiguration(ui = AdminUi.class, productionMode = false)
public class AdminUiServlet extends VaadinServlet implements SessionInitListener, SessionDestroyListener {

    @EJB(mappedName = "stateless.DictionaryBean", name = "DictionaryBean")
    private DictionaryBean<Dictionary> dictionaryBean;

    @EJB(mappedName = "stateless.SystemObjectPatternBean", name = "SystemObjectPatternBean")
    private SystemObjectPatternBean systemObjectPatternBean;

    @EJB(mappedName = "stateless.DictionaryTermBean", name = "DictionaryTermBean")
    private DictionaryTermBean<DictionaryTerm> dictionaryTermBean;

    @EJB(mappedName = "stateless.DictionaryEntryBean", name = "DictionaryEntryBean")
    private DictionaryEntryBean<DictionaryEntry> dictionaryEntryBean;

    @EJB(mappedName = "stateless.SystemUserBean", name = "SystemUserBean")
    private SystemUserBean<SystemUser> systemUserBean;

    @EJB(mappedName = "stateless.SystemObjectBean", name = "SystemObjectBean")
    private SystemObjectBean<SystemObject> systemObjectBean;

    @EJB(mappedName = "stateless.UserRoleBean", name = "UserRoleBean")
    private UserRoleBean<UserRole> userRoleBean;

    @EJB(mappedName = "stateless.RoleBean", name = "RoleBean")
    private RoleBean<Role> roleBean;

    @EJB(mappedName = "stateless.ItemBean", name = "ItemBean")
    private ItemBean<Item> itemBean;

    @EJB(mappedName = "stateless.ItemProfileBean", name = "ItemProfileBean")
    private ItemProfileBean<ItemProfile> itemProfileBean;

    @EJB(mappedName = "stateless.ItemProfilePropertyBean", name = "ItemProfilePropertyBean")
    private ItemProfilePropertyBean<ItemProfileProperty> itemProfilePropertyBean;

    @EJB(mappedName = "stateless.ItemProfileValueBean", name = "ItemProfileValueBean")
    private ItemProfileValueBean<ItemProfileValue> itemProfileValueBean;

    @EJB(mappedName = "stateless.PropertyDescriptorBean", name = "PropertyDescriptorBean")
    private PropertyDescriptorBean<PropertyDescriptor> propertyDescriptorBean;

    @EJB(mappedName = "stateless.PropertyDataLongBean", name = "PropertyDataLongBean")
    private PropertyDataLongBean<PropertyDataLong> propertyDataLongBean;


    public static UserProfile getCurrentProfile() {
        if (!checkSessionProfile()) {
            VaadinSession.getCurrent().close();
        }
        return (UserProfile) VaadinSession.getCurrent().getAttribute(UserProfile.class.getName());
    }

    public static boolean checkSessionProfile() {
        Object userProfile = VaadinSession.getCurrent().getAttribute(UserProfile.class.getName());
        return userProfile != null && userProfile instanceof UserProfile;
    }

    public SystemUser login(String userName, String password) throws NoSuchAlgorithmException, GenericSystemException {
        SystemUser login = systemUserBean.login(userName, MD5.calculate(password));
        UserHolder.setUser(login);
        UserHolder.getUser().setUserAttribute(SysLanguage.class.getName(), BigDecimal.ONE.toString());
        VaadinSession.getCurrent().setAttribute(UserProfile.class.getName(), UserHolder.getUser());

        Collection<UserRole> userRoles = this.getUserRoles(login.getCoreId());
        VaadinSession.getCurrent().setAttribute(UserRole.class.getName(), userRoles);

        return login;
    }

    public Map<BigDecimal, Role> getRoles() {
        try {
            Map<BigDecimal, Role> map = new HashMap<>();
            for (Role role : roleBean.getAllObjects(getCurrentProfile())) {
                map.put(role.getCoreId(), role);
            }

            return map;
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyMap();
    }

    public Collection<UserRole> getUserRoles(BigDecimal userId) {
        try {
            UserProfile userProfile = getCurrentProfile();
            return userRoleBean.getRolesByUserId(userProfile, userId);
        } catch (CoreException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Set<String> getSystemObjects() {
        try {
            return systemObjectBean.getSupportedIntefaces(getCurrentProfile());

        } catch (GenericSystemException e) {
            e.printStackTrace();
        }
        return Collections.emptySet();
    }

    public Collection<SystemObject> getAllSystemObjects() {
        try {
            return systemObjectBean.getAllObjects(getCurrentProfile());

        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptySet();
    }

    public Collection<SystemUser> getSystemUsers() {
        try {
            return systemUserBean.getAllObjects(getCurrentProfile());
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<Dictionary> getDictionaries() {
        try {
            return dictionaryBean.getAllObjects(getCurrentProfile());
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<DictionaryEntry> getDictionaryEntries(BigDecimal dicId) {
        try {
            return dictionaryEntryBean.getAllEntriesForDictionary(getCurrentProfile(), dicId);
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<DictionaryTerm> getDictionaryTerms(BigDecimal dicId, BigDecimal entryId) {
        try {
            return dictionaryTermBean.getAllTermsForDictionaryAndEntry(getCurrentProfile(), dicId, entryId);
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<DictionaryTerm> getDictionaryTermsForDic(BigDecimal dicId) {
        try {
            return dictionaryTermBean.getAllTermsForDictionaryAndLanguage(getCurrentProfile(), dicId, BigDecimal.ONE);
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<Item> getItems() {
        try {
            return itemBean.getAllObjects(getCurrentProfile());
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public PropertyDataLong createPropertyValue(PropertyDataLong dataLong) {
        try {
            return propertyDataLongBean.createObject(getCurrentProfile(), dataLong);
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection<PropertyDataLong> getPropertiesForItem(BigDecimal itemId){
        try {
            SearchCriteriaProfile profile = propertyDataLongBean.getSearchCriteriaProfile(getCurrentProfile());
            Set<SearchCriterion> criteria = new LinkedHashSet<>();
            criteria.add(profile.getSearchCriterion(PropertyDataLong.ITEM_ID, CriteriaRule.equals, itemId));
            return propertyDataLongBean.searchObjects(getCurrentProfile(), criteria);
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Map<SystemObjectPattern, BigDecimal> getPattern(Identifiable identifiable) {
        try {
            return systemObjectPatternBean.getPatternByObject(getCurrentProfile(), identifiable);
        } catch (CoreException e) {
            e.printStackTrace();
        }
        return Collections.emptyMap();
    }


    public Collection<Identifiable> checkPattern(String pattern) {
        try {
            return systemObjectPatternBean.getAvailableObjects(getCurrentProfile(), new SystemObjectPattern(pattern));
        } catch (CoreException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Item createItem(ItemVO itemVO) {
        try {
            UserHolder.setUser(getCurrentProfile().getSystemUser());
            Item item = IdentifiableFactory.getImplementation(Item.class);
            item.setType(itemVO.getType());
            item.setName(itemVO.getName());
            item.setParentId(new BigDecimal(itemVO.getParent()));
            item.setItemProfile(itemVO.getItemProfile());
            item.setStatus(itemVO.getStatus());
            item.setCoreDsc(itemVO.getDsc());

            return itemBean.createObject(getCurrentProfile(), item);
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Collection<ItemProfile> getItemProfiles() {
        try {
            return itemProfileBean.getAllObjects(getCurrentProfile());
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<ItemProfileProperty> getItemProfileProperties() {
        try {
            return itemProfilePropertyBean.getAllObjects(getCurrentProfile());
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<ItemProfileValue> getItemProfileValues() {
        try {
            return itemProfileValueBean.getAllObjects(getCurrentProfile());
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }

    public Collection<PropertyDescriptor> getPropertyDescriptors() {
        try {
            return propertyDescriptorBean.getAllObjects(getCurrentProfile());
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }


    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.service(request, response);
    }

    @Override
    protected void servletInitialized() throws ServletException {
        super.servletInitialized();
        getService().addSessionInitListener(this);
        getService().addSessionDestroyListener(this);
    }

    @Override
    public void sessionDestroy(SessionDestroyEvent event) {
        event.getSession().setAttribute(UserProfile.class.getName(), null);
    }

    @Override
    public void sessionInit(SessionInitEvent event) throws ServiceException {
        event.getSession().setAttribute(UserProfile.class.getName(), null);
    }
}
