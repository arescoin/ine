/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.item;

import com.vaadin.contextmenu.GridContextMenu;
import com.vaadin.data.ValueProvider;
import com.vaadin.icons.VaadinIcons;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import ru.xr.ine.oss.inventory.item.ItemProfile;
import ru.xr.ine.oss.inventory.item.ItemProfileProperty;
import ru.xr.ine.oss.inventory.item.PropertyDescriptor;
import ru.xr.ine.vaadin.AdminUiServlet;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//import com.vaadin.ui.Table;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfilePropertyView.java 62 2017-05-06 10:12:20Z xerror $"
 */
public class ItemProfilePropertyView extends VerticalLayout implements View {

    public static final String VIEW_KEY = ItemProfileProperty.class.getName();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();

        Collection<ItemProfileProperty> itms = ((AdminUiServlet) VaadinServlet.getCurrent()).getItemProfileProperties();

        Collection<PropertyDescriptor> descriptors =
                ((AdminUiServlet) VaadinServlet.getCurrent()).getPropertyDescriptors();
        Map<BigDecimal, PropertyDescriptor> descriptorMap = new HashMap<>();
        for (PropertyDescriptor descriptor : descriptors) {
            descriptorMap.put(descriptor.getCoreId(), descriptor);
        }

        Collection<ItemProfile> itemProfiles = ((AdminUiServlet) VaadinServlet.getCurrent()).getItemProfiles();
        Map<BigDecimal, ItemProfile> profileMap = new HashMap<>();
        for (ItemProfile itemProfile : itemProfiles) {
            profileMap.put(itemProfile.getCoreId(), itemProfile);
        }

        Grid<ItemProfileProperty> grid = new Grid<>("ItemProfileProperty");
        grid.setSizeFull();

        Grid.Column<ItemProfileProperty, BigDecimal> column = grid.addColumn(ItemProfileProperty::getCoreId);
        column.setCaption("ID");
        grid.addColumn((ValueProvider<ItemProfileProperty, Object>) itemProfileProperty ->
                itemProfileProperty.getItemProfileId() + " : "
                        + profileMap.get(itemProfileProperty.getItemProfileId()).getName()).
                setCaption("Profile").setDescriptionGenerator(itemProfileProperty ->
                profileMap.get(itemProfileProperty.getItemProfileId()).getCoreDsc());
        grid.addColumn(
                (ValueProvider<ItemProfileProperty, Object>) itemProfileProperty ->
                        itemProfileProperty.getPropertyDescriptorId() + " : "
                                + descriptorMap.get(itemProfileProperty.getPropertyDescriptorId()).getName()).
                setCaption("Property descriptor").
                setDescriptionGenerator(itemProfileProperty ->
                        descriptorMap.get(itemProfileProperty.getPropertyDescriptorId()).getCoreDsc());
        grid.addColumn(ItemProfileProperty::isRequired).setCaption("Required");
        grid.addColumn(ItemProfileProperty::isEditable).setCaption("Editable");
        grid.addColumn(ItemProfileProperty::getDefaultValue).setCaption("Default Value");
        grid.addColumn(ItemProfileProperty::getPattern).setCaption("Pattern");
        grid.addColumn(ItemProfileProperty::getCoreDsc).setCaption("Description");
//        grid.addColumn(ItemProfileProperty::getCoreFd).setCaption("Fd");
//        grid.addColumn(ItemProfileProperty::getCoreTd).setCaption("Td");
        grid.sort(column, SortDirection.ASCENDING);

        grid.setItems(itms);

        GridContextMenu<ItemProfileProperty> gridContextMenu = new GridContextMenu<>(grid);
        gridContextMenu.addGridBodyContextMenuListener(event1 -> {
            event1.getContextMenu().removeItems();

            grid.select((ItemProfileProperty) event1.getItem());

            event1.getContextMenu().addItem("Edit Item", VaadinIcons.EDIT, selectedItem -> {
                getUI().getSession().setAttribute(ItemProfilePropertyEditView.PROF_PROP_KEY, event1.getItem());
                UI.getCurrent().getNavigator().navigateTo(ItemProfilePropertyEditView.VIEW_KEY);
            });


        });

        this.addComponent(grid);
    }

}
