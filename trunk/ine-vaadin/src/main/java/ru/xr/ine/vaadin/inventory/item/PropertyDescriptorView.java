/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.item;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import ru.xr.ine.oss.inventory.item.PropertyDescriptor;
import ru.xr.ine.vaadin.AdminUiServlet;

import java.math.BigDecimal;
import java.util.Collection;

//import com.vaadin.ui.Table;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDescriptorView.java 57 2017-05-03 23:25:32Z xerror $"
 */
public class PropertyDescriptorView extends VerticalLayout implements View {

    public static final String VIEW_KEY = PropertyDescriptor.class.getName();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();

        Collection<PropertyDescriptor> itms = ((AdminUiServlet) VaadinServlet.getCurrent()).getPropertyDescriptors();

        Grid<PropertyDescriptor> grid = new Grid<>("PropertyDescriptors");
        grid.setSizeFull();
        Grid.Column<PropertyDescriptor, BigDecimal> column = grid.addColumn(PropertyDescriptor::getCoreId);
        column.setCaption("ID");
        grid.addColumn(PropertyDescriptor::getName).setCaption("Name");
        grid.addColumn(PropertyDescriptor::getType).setCaption("Type");
        grid.addColumn(PropertyDescriptor::getCoreDsc).setCaption("Description");
        grid.addColumn(PropertyDescriptor::getCoreFd).setCaption("Fd");
        grid.addColumn(PropertyDescriptor::getCoreTd).setCaption("Td");
        grid.sort(column, SortDirection.ASCENDING);

        grid.setItems(itms);
        this.addComponent(grid);

//        Table table = new Table("PropertyDescriptors");
//        table.setSizeFull();
//
//        table.addContainerProperty("ID", BigDecimal.class, null);
//        table.addContainerProperty("Name", String.class, null);
//        table.addContainerProperty("Type", String.class, null);
//        table.addContainerProperty("Dsc", String.class, null);
//        table.addContainerProperty("fd", Date.class, null);
//        table.addContainerProperty("td", Date.class, null);
//
//        SortedSet<PropertyDescriptor> items = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
//
//        items.addAll(itms);
//
//        for (PropertyDescriptor item : items) {
//            table.addItem(new Object[]{
//                    item.getCoreId(),
//                    item.getName(),
//                    item.getType().toString(),
//                    item.getCoreDsc(),
//                    item.getCoreFd(),
//                    item.getCoreTd()
//            }, item.getCoreId());
//        }
//
//        this.addComponent(table);
    }
}
