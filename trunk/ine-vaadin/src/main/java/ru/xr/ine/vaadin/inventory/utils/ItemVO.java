/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.utils;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemVO.java 59 2017-05-04 16:58:36Z xerror $"
 */
public class ItemVO {

    private String id;
    private BigDecimal type;
    private String name;
    private String parent;
    private BigDecimal itemProfile;
    private BigDecimal status;
    private String dsc;
    private String from;
    private String to;

    public ItemVO(BigDecimal type, String name, String parent, BigDecimal itemProfile, BigDecimal status, String dsc) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.parent = parent;
        this.itemProfile = itemProfile;
        this.status = status;
        this.dsc = dsc;
        this.from = from;
        this.to = to;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getType() {
        return type;
    }

    public void setType(BigDecimal type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public BigDecimal getItemProfile() {
        return itemProfile;
    }

    public void setItemProfile(BigDecimal itemProfile) {
        this.itemProfile = itemProfile;
    }

    public BigDecimal getStatus() {
        return status;
    }

    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
