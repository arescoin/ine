/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.core.structure.SystemObject;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Stream;

//import com.vaadin.ui.Table;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DictionariesView.java 57 2017-05-03 23:25:32Z xerror $"
 */
public class DictionariesView extends VerticalLayout implements View {

    public static final String VIEW_KEY = Dictionary.class.getName();


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();


        Collection<Dictionary> dictionaries = ((AdminUiServlet) VaadinServlet.getCurrent()).getDictionaries();

//        Table termsTable = new Table("Terms");
//        termsTable.addContainerProperty("ID", BigDecimal.class, null);
//        termsTable.addContainerProperty("Up", BigDecimal.class, null);
//        termsTable.addContainerProperty("Lang", BigDecimal.class, null);
//        termsTable.addContainerProperty("Term", String.class, null);
//        termsTable.setSizeFull();
//
        Grid<DictionaryTerm> termGrid = new Grid<>();
        termGrid.setSizeFull();

        termGrid.addColumn(DictionaryTerm::getCoreId).setCaption("Code");
        termGrid.addColumn(DictionaryTerm::getLangCode).setCaption("Land");
        termGrid.addColumn(DictionaryTerm::getParentTermId).setCaption("Parent");
        termGrid.addColumn(DictionaryTerm::getTerm).setCaption("Term");
        termGrid.addColumn(DictionaryTerm::getCoreFd).setCaption("Fd");
        termGrid.addColumn(DictionaryTerm::getCoreTd).setCaption("Td");


        Grid<Dictionary> dictionaryGrid = new Grid<>("Dictionaries");
        dictionaryGrid.setItems(dictionaries);
        dictionaryGrid.setSizeFull();
        Grid.Column<Dictionary, BigDecimal> column = dictionaryGrid.addColumn(Dictionary::getCoreId);
        column.setCaption("ID");
        dictionaryGrid.addColumn(Dictionary::getName).setCaption("Name");
        dictionaryGrid.addColumn(Dictionary::getCoreDsc).setCaption("Description");
        dictionaryGrid.addColumn(Dictionary::getParentId).setCaption("Parent");
        dictionaryGrid.addColumn(Dictionary::getDicType).setCaption("Type");
        dictionaryGrid.addColumn(Dictionary::getEntryCodePolicy).setCaption("CodePolicy");
        dictionaryGrid.addColumn(Dictionary::getLocalizationPolicy).setCaption("LocalizationPolicy");
        dictionaryGrid.addColumn(Dictionary::getCoreFd).setCaption("Fd");
        dictionaryGrid.addColumn(Dictionary::getCoreTd).setCaption("Td");

        dictionaryGrid.sort(column, SortDirection.ASCENDING);

        dictionaryGrid.addSelectionListener(event1 -> {
            termGrid.setItems(Stream.empty());

            Optional<Dictionary> firstSelectedItem = event1.getFirstSelectedItem();
            if(firstSelectedItem.isPresent()){
            Collection<DictionaryEntry> dictionaryEntries =
                    ((AdminUiServlet) VaadinServlet.getCurrent())
                            .getDictionaryEntries(firstSelectedItem.get().getCoreId());

            Collection<DictionaryTerm> entries = new ArrayList<>();
            for (DictionaryEntry dictionaryEntry : dictionaryEntries) {
                entries.addAll(((AdminUiServlet) VaadinServlet.getCurrent())
                        .getDictionaryTerms(dictionaryEntry.getDictionaryId(), dictionaryEntry.getCoreId()));
            }
            termGrid.setItems(entries);}

        });

//        Table dictTable = new Table("Dictionaries");
//
//        dictTable.setSizeFull();
//        dictTable.setSelectable(true);
//        dictTable.setImmediate(true);
//
//        dictTable.addContainerProperty("ID", BigDecimal.class, null);
//        dictTable.addContainerProperty("Name", String.class, null);
//        dictTable.addContainerProperty("Parent", BigDecimal.class, null);
//        dictTable.addContainerProperty("Type", BigDecimal.class, null);
//        dictTable.addContainerProperty("CodePolicy", BigDecimal.class, null);
//        dictTable.addContainerProperty("LocalizationPolicy", BigDecimal.class, null);
//        dictTable.addContainerProperty("Dsc", String.class, null);
//        dictTable.addContainerProperty("fd", Date.class, null);
//        dictTable.addContainerProperty("td", Date.class, null);
//
//        dictTable.addValueChangeListener(event1 -> {
//
//            BigDecimal rowId = (BigDecimal) dictTable.getValue();
//
//            Collection<DictionaryEntry> dictionaryEntries =
//                    ((AdminUiServlet) VaadinServlet.getCurrent()).getDictionaryEntries(rowId);
//
//            termsTable.removeAllItems();
//
//            for (DictionaryEntry dictionaryEntry : dictionaryEntries) {
//                Collection<DictionaryTerm> entries =
//                        ((AdminUiServlet) VaadinServlet.getCurrent())
//                                .getDictionaryTerms(rowId, dictionaryEntry.getCoreId());
//
//                for (DictionaryTerm entry : entries) {
//                    termsTable.addItem(new Object[]{entry.getCoreId(),
//                                    entry.getParentTermId(),
//                                    entry.getLangCode(),
//                                    entry.getTerm()},
//                            entry.getCoreId() + ":" + entry.getLangCode());
//                }
//            }
//        });
//
//
//        SortedSet<Dictionary> treeSet = new TreeSet<>(new Comparator<Dictionary>() {
//            @Override
//            public int compare(Dictionary o1, Dictionary o2) {
//                return o1.getCoreId().compareTo(o2.getCoreId());
//            }
//        });
//
//        treeSet.addAll(dictionaries);
//        for (Dictionary dictionary : treeSet) {
//            dictTable.addItem(new Object[]{
//                    dictionary.getCoreId(),
//                    dictionary.getName(),
//                    dictionary.getParentId(),
//                    dictionary.getDicType(),
//                    dictionary.getEntryCodePolicy(),
//                    dictionary.getLocalizationPolicy(),
//                    dictionary.getCoreDsc(),
//                    dictionary.getCoreFd(),
//                    dictionary.getCoreTd()
//            }, dictionary.getCoreId());
//        }
//
//        dictTable.setValue(dictTable.getCurrentPageFirstItemId());
//
//        dictionaryGrid.select();
        this.addComponent(dictionaryGrid);

        this.addComponent(termGrid);
//        this.addComponent(termsTable);
    }

}
