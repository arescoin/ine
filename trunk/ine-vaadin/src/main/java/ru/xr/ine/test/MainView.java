/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.test;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;

/**
 * Main view with a menu (with declarative layout design)
 */
@DesignRoot
public class MainView extends HorizontalLayout {
    public static final String NAME = "main";

    Navigator navigator;

    class AnimalButton extends Button {
        public AnimalButton(String caption, String id) {
            super(caption);
            addStyleName(ValoTheme.MENU_ITEM);
            setIcon(new ThemeResource("img/" + id + "-16px.png"));
            addClickListener(click -> navigator.navigateTo(id));
        }
    }

    Label title;
    CssLayout menu;
    Panel contentArea;
    Button logoutButton;

    public MainView(String username, Runnable logout) {
        setSizeFull();
        addStyleName(ValoTheme.MENU_ROOT);

        title = new Label("Animal Farm");
        title.addStyleName(ValoTheme.MENU_TITLE);

        menu = new CssLayout();
        menu.addStyleName(ValoTheme.MENU_PART);
        addComponent(menu);

        menu.addComponent(new AnimalButton("Pig", "pig"));
        menu.addComponent(new AnimalButton("Cat", "cat"));
        menu.addComponent(new AnimalButton("Dog", "dog"));
        menu.addComponent(new AnimalButton("Reindeer", "reindeer"));
        menu.addComponent(new AnimalButton("Penguin", "penguin"));
        menu.addComponent(new AnimalButton("Sheep", "sheep"));

        contentArea = new Panel("Equals");
        contentArea.addStyleName(ValoTheme.PANEL_BORDERLESS);
        contentArea.setSizeFull();
        addComponent(contentArea);
        setExpandRatio(contentArea, 1.0f);

        // Create a navigator to control the sub-views
        navigator = new Navigator(UI.getCurrent(), contentArea);

        // Create and register the sub-views
        navigator.addView("pig", AnimalView.class);
        navigator.addView("cat", AnimalView.class);
        navigator.addView("dog", AnimalView.class);
        navigator.addView("reindeer", AnimalView.class);
        navigator.addView("penguin", AnimalView.class);
        navigator.addView("sheep", AnimalView.class);

        // Allow going back to the start
        logoutButton = new Button("Log Out");
        logoutButton.addClickListener(click -> // Java 8
                logout.run());
        addComponent(logoutButton);
    }
}
