/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.ejbs.core.dic.DictionaryBean;
import ru.xr.ine.ejbs.core.userpermits.SystemUserBean;
import ru.xr.ine.utils.MD5;

import javax.annotation.ManagedBean;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.annotation.WebServlet;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Collections;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: MyUIServlet.java 29 2017-04-04 15:32:19Z xerror $"
 */
@WebServlet(value = "/1/*")
@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
public class MyUIServlet extends VaadinServlet {


    @EJB(mappedName = "stateless.DictionaryBean", name = "DictionaryBean")
    private DictionaryBean<Dictionary> dictionaryBean;

    @EJB(mappedName = "stateless.SystemUserBean", name = "SystemUserBean")
    private SystemUserBean<SystemUser> systemUserBean;

    public DictionaryBean<Dictionary> getDictionaryBean() {
        return dictionaryBean;
    }

    public SystemUserBean<SystemUser> getSystemUserBean() {
        return systemUserBean;
    }

    public Collection<Dictionary> getDicData() {
        try {
            SystemUser login = systemUserBean.login("root", MD5.calculate("root"));
            UserHolder.setUser(login);
            Collection<Dictionary> dictionaries = dictionaryBean.getAllObjects(UserHolder.getUser());
            return dictionaries;

        } catch (GenericSystemException | NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return Collections.emptyList();
    }
}
