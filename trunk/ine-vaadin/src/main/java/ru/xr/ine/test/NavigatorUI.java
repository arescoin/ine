/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.test;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import java.util.function.Consumer;


/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: NavigatorUI.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class NavigatorUI extends UI {


    @Override
    protected void init(VaadinRequest request) {
        getPage().setTitle("Navigation Example");

        // Enable Valo menu
        addStyleName(ValoTheme.UI_WITH_MENU);
        setResponsive(true);

        // Handle login and logout
        Consumer<String> loginCallback = new Consumer<String>() {
            @Override
            public void accept(String username) {

                Runnable logout = () -> {
                    VaadinSession.getCurrent().close();
                    Page.getCurrent().setLocation(
                            VaadinServlet.getCurrent().getServletContext().getContextPath() + "/navigator");
                };

                NavigatorUI.this.setContent(new MainView(username, logout));
            }
        };


        LoginView content = new LoginView(loginCallback);
        setContent(content);
    }
}
