/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.item;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.data.sort.SortDirection;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.oss.inventory.item.ItemProfile;
import ru.xr.ine.vaadin.AdminUiServlet;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileView.java 57 2017-05-03 23:25:32Z xerror $"
 */
public class ItemProfileView extends VerticalLayout implements View {

    public static final String VIEW_KEY = ItemProfile.class.getName();

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();

        Collection<ItemProfile> itms = ((AdminUiServlet) VaadinServlet.getCurrent()).getItemProfiles();

        Grid<ItemProfile> grid = new Grid<>("Item Profiles");
        grid.setSizeFull();

        Grid.Column<ItemProfile, BigDecimal> column = grid.addColumn(Identifiable::getCoreId);
        column.setCaption("ID");
        grid.addColumn(ItemProfile::getName).setCaption("Name");
        grid.addColumn(ItemProfile::getItemType).setCaption("Type");
        grid.addColumn(ItemProfile::getCoreDsc).setCaption("Description");
        grid.addColumn(ItemProfile::getCoreFd).setCaption("Fd");
        grid.addColumn(ItemProfile::getCoreTd).setCaption("Td");

        grid.setItems(itms);
        grid.sort(column, SortDirection.ASCENDING);
        this.addComponent(grid);

//        Table table = new Table("ItemProfile");
//        table.setSizeFull();
//
//        table.addContainerProperty("ID", BigDecimal.class, null);
//        table.addContainerProperty("Name", String.class, null);
//        table.addContainerProperty("Type", BigDecimal.class, null);
//        table.addContainerProperty("Dsc", String.class, null);
//        table.addContainerProperty("fd", Date.class, null);
//        table.addContainerProperty("td", Date.class, null);
//
//        SortedSet<ItemProfile> items = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
//
//        items.addAll(itms);
//
//        for (ItemProfile profile : items) {
//            table.addItem(new Object[]{
//                    profile.getCoreId(),
//                    profile.getName(),
//                    profile.getItemType(),
//                    profile.getCoreDsc(),
//                    profile.getCoreFd(),
//                    profile.getCoreTd()
//            }, profile.getCoreId());
//        }
//
//        this.addComponent(table);
    }

}
