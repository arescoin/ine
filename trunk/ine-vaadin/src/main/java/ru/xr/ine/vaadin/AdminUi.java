/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AdminUi.java 57 2017-05-03 23:25:32Z xerror $"
 */
@Theme("arescontheme")
public class AdminUi extends UI {

    private MainLayout view;
    private LoginLayout loginView;

    @Override
    protected void init(VaadinRequest request) {
        loginView = new LoginLayout();
        view = new MainLayout();

        if (AdminUiServlet.checkSessionProfile()) {
            setContent(view);
            view.enter();
        } else {
            setContent(loginView);
        }

    }


    protected void changeContent() {

        if (AdminUiServlet.getCurrentProfile().getCurrentRole() != null) {
            this.setContent(view);
            view.enter();
        } else {
            setContent(loginView);
        }
    }
}
