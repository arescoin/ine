package ru.xr.ine;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import ru.xr.ine.core.dic.Dictionary;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.TreeMap;


/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {


    private Button.ClickListener listener;

    @Override
    protected void init(final VaadinRequest vaadinRequest) {

        new Navigator(this, this);
//        getNavigator().addView(SimpleLoginView.NAME, SimpleLoginView.class);


        //
        // Add the main view of the application
        //
        getNavigator().addView(SimpleLoginMainView.NAME, SimpleLoginMainView.class);

        //
        // We use a view change handler to ensure the user is always redirected
        // to the login view if the user is not logged in.
        //
        getNavigator().addViewChangeListener(new ViewChangeListener() {

            @Override
            public boolean beforeViewChange(ViewChangeEvent event) {

                // Check if a user has logged in
                boolean isLoggedIn = getSession().getAttribute("user") != null;
                boolean isLoginView = event.getNewView() instanceof SimpleLoginView;

                if (!isLoggedIn && !isLoginView) {
                    // Redirect to login view always if a user has not yet
                    // logged in
//                    getNavigator().navigateTo(SimpleLoginView.NAME);
                    return false;

                } else if (isLoggedIn && isLoginView) {
                    // If someone tries to access to login view while logged in,
                    // then cancel
                    getNavigator().navigateTo("ddd");
                    return false;
                }

                return true;
            }

            @Override
            public void afterViewChange(ViewChangeListener.ViewChangeEvent event) {

            }
        });


        final VerticalLayout layout = new VerticalLayout();

        final TextField name = new TextField();
        name.setCaption("Type your name here:");

        Button button = new Button("Click Me");
        listener = new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent e) {
                layout.addComponent(new Label("Thanks " + name.getValue()
                        + ", it works! " + vaadinRequest.getRemoteUser()));

                Collection<Dictionary> dictionaries = dictionaries();

                TreeMap<BigDecimal, Dictionary> treeMap = new TreeMap<>();
                for (Dictionary dictionary : dictionaries()) {
                    treeMap.put(dictionary.getCoreId(), dictionary);
                }


                for (Dictionary dictionary : treeMap.values()) {
                    final HorizontalLayout horizontalLayout = new HorizontalLayout();

                    horizontalLayout.addComponents(
                            new Label(dictionary.getCoreId().toPlainString()),
                            new Label(" - "),
                            new Label(dictionary.getDicType().toPlainString()),
                            new Label(" - "),
                            new Label(dictionary.getName()),
                            new Label(" - "),
                            new Label("(" + dictionary.getCoreDsc() + ")")
                    );

                    layout.addComponent(horizontalLayout);
                }
            }
        };

        button.addClickListener(listener);



        layout.addComponents(name, button);
        layout.setMargin(true);
        layout.setSpacing(true);

        setContent(layout);
    }

    public Collection<Dictionary> dictionaries() {
        return ((MyUIServlet) VaadinServlet.getCurrent()).getDicData();
    }


}
