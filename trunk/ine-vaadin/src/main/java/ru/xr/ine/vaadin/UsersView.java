/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import ru.xr.ine.core.userpermits.SystemUser;

import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: UsersView.java 57 2017-05-03 23:25:32Z xerror $"
 */
public class UsersView extends VerticalLayout implements View {

    public static final String VIEW_KEY = "usersList";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();

        Collection<SystemUser> systemObjects = ((AdminUiServlet) VaadinServlet.getCurrent()).getSystemUsers();

        SortedSet<SystemUser> systemUsers = new TreeSet<>((o1, o2) -> o1.getCoreId().compareTo(o2.getCoreId()));
        systemUsers.addAll(systemObjects);

        Grid<SystemUser> grid = new Grid<>();
        grid.setSizeFull();
        grid.setItems(systemUsers);
        grid.addColumn(SystemUser::getCoreId).setCaption("ID");
        grid.addColumn(SystemUser::getLogin).setCaption("Login");
        grid.addColumn(SystemUser::isActive).setCaption("Active");
        grid.addColumn(SystemUser::getCoreDsc).setCaption("Description");
        grid.addColumn(SystemUser::getCoreFd).setCaption("FD");
        grid.addColumn(SystemUser::getCoreTd).setCaption("TD");

        this.addComponent(grid);

//        Table table = new Table("System Users");
//        table.setSizeFull();
//
//        table.addContainerProperty("ID", BigDecimal.class, null);
//        table.addContainerProperty("Name", String.class, null);
//        table.addContainerProperty("Active", Boolean.class, null);
//        table.addContainerProperty("Dsc", String.class, null);
//        table.addContainerProperty("fd", Date.class, null);
//        table.addContainerProperty("td", Date.class, null);
//
//        SortedSet<SystemUser> systemUsers = new TreeSet<>(new Comparator<SystemUser>() {
//            @Override
//            public int compare(SystemUser o1, SystemUser o2) {
//                return o1.getCoreId().compareTo(o2.getCoreId());
//            }
//        });
//
//        systemUsers.addAll(systemObjects);
//
//        System.out.println("System users: " + systemUsers.size());
//
//        for (SystemUser systemUser : systemUsers) {
//            table.addItem(new Object[]{
//                    systemUser.getCoreId(),
//                    systemUser.getLogin(),
//                    systemUser.isActive(),
//                    systemUser.getCoreDsc(),
//                    systemUser.getCoreFd(),
//                    systemUser.getCoreTd()
//            }, systemUser.getCoreId());
//        }
//
//        this.addComponent(table);
    }


}
