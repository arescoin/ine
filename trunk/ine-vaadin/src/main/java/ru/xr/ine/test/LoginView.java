/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.test;

import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;

import java.util.function.Consumer;

/**
 * A start view for navigating to the main view.
 * <p>
 * Not managed by the Navigator
 **/
@DesignRoot
public class LoginView extends VerticalLayout {
    public static final String NAME = "";

    TextField username;
    Button loginButton;

    public LoginView(Consumer<String> loginCallback) {
        setSizeFull();
        Design.read(this);

        username.setValue("demouser");

        loginButton.addClickListener(click ->
                loginCallback.accept(username.getValue()));

        Notification.show("Welcome to the Animal Farm");
    }
}
