/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.vaadin.inventory.item.*;

import java.math.BigDecimal;
import java.util.Map;

//import com.vaadin.ui.themes.Reindeer;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: MainLayout.java 62 2017-05-06 10:12:20Z xerror $"
 */
public class MainLayout extends VerticalLayout {

    private GeneralContentLayout contentLayout;

    private ObjectsView objectsView;

    Navigator navigator;

    public MainLayout() {

        contentLayout = new GeneralContentLayout();

//        Navigator navigator = new Navigator(UI.getCurrent(), contentLayout);
//        navigator.setErrorView(ObjectsView.class);
//        navigator.addView("", ObjectsView.class);
//        navigator.addView(MainLayout.ADMIN_MANI, this);
//        navigator.navigateTo(MainLayout.ADMIN_MANI);


//        objectsView.setSizeFull();
    }

    public void enter() {

        objectsView = new ObjectsView();
        contentLayout.addComponent(objectsView);
        this.addComponent(createHeader());
        this.addComponent(contentLayout);

        this.setMargin(false);
        this.setSpacing(false);

        navigator = new Navigator(UI.getCurrent(), contentLayout);
        navigator.setErrorView(ObjectsView.class);

        navigator.addView("", ObjectsView.class);
        navigator.addView(ObjectsView.VIEW_KEY, objectsView);
        navigator.addView(UsersView.VIEW_KEY, new UsersView());
        navigator.addView(DictionariesView.VIEW_KEY, new DictionariesView());
        navigator.addView(ItemView.VIEW_KEY, new ItemView());
        navigator.addView(ItemProfileView.VIEW_KEY, new ItemProfileView());
        navigator.addView(ItemProfilePropertyView.VIEW_KEY, new ItemProfilePropertyView());
        navigator.addView(ItemProfileValueView.VIEW_KEY, new ItemProfileValueView());
        navigator.addView(PropertyDescriptorView.VIEW_KEY, new PropertyDescriptorView());
        navigator.addView(ItemProfilePropertyEditView.VIEW_KEY, new ItemProfilePropertyEditView());

        navigator.addView(ItemCreateView.VIEW_KEY, new ItemCreateView());

        navigator.navigateTo(ObjectsView.VIEW_KEY);


//        objectsView.enter(event);
//        this.setExpandRatio(objectsView, 1);
    }


    private HorizontalLayout createHeader() {
        HorizontalLayout layout = new HorizontalLayout();

//        Label tempMenuStub = new Label("Menu wee'l grown here");
        Label separator = new Label("- InE Admin UI -");

        Map<BigDecimal, Role> map = ((AdminUiServlet) VaadinServlet.getCurrent()).getRoles();

        UserProfile userProfile = AdminUiServlet.getCurrentProfile();
        Label userLabel = new Label(map.get(userProfile.getCurrentRole()).getName() + " : " + userProfile.getSystemUser().getLogin());
        userLabel.addStyleName("custom-label-align-right");


        layout.addComponent(createObjectsMenu());
        layout.addComponent(separator);
        layout.setComponentAlignment(separator, Alignment.MIDDLE_CENTER);
//        HorizontalLayout horizontalLayout = new HorizontalLayout(userLabel);
//        horizontalLayout.setComponentAlignment();
        layout.addComponent(userLabel);
        layout.setComponentAlignment(userLabel, Alignment.MIDDLE_RIGHT);

        layout.setStyleName("loginView-background");
        layout.setSizeFull();

        layout.setMargin(new MarginInfo(false, false, false, false));
        layout.setSpacing(false);

        return layout;
    }

    public MenuBar createObjectsMenu() {

        MenuBar mainMenu = new MenuBar();

        MenuBar.MenuItem objectsMenu = mainMenu.addItem("Objects", null);
//        MenuBar.MenuItem systemMenu = mainMenu.addItem("System", null);
        MenuBar.MenuItem itemMenu = mainMenu.addItem("Item", null);
//        MenuBar.MenuItem settingsMenu = mainMenu.addItem("Settings", null);
//        MenuBar.MenuItem preferenceMenu = mainMenu.addItem("Preference", null);

        objectsMenu.addItem("List", menuCommand);
        objectsMenu.addItem("Users", menuCommand);
        objectsMenu.addItem("Dictionary", menuCommand);
//        objectsMenu.addItem("Items", menuCommand);

        itemMenu.addItem("Items", itemMenuCommand);
        itemMenu.addItem("PropertyDescriptor", itemMenuCommand);
        itemMenu.addItem("Profiles", itemMenuCommand);
        itemMenu.addItem("ProfileProperties", itemMenuCommand);
        itemMenu.addItem("ProfileValues", itemMenuCommand);

        mainMenu.addStyleName("mainMenu-background");

        return mainMenu;
    }

    private final MenuBar.Command itemMenuCommand = (MenuBar.Command) selectedItem -> {
        System.out.println(selectedItem);
        System.out.println(selectedItem.getText());

        System.out.println(Thread.currentThread().getId());

        if (selectedItem.getText().equals("Items")) {
            UI.getCurrent().getNavigator().navigateTo(ItemView.VIEW_KEY);
        } else if (selectedItem.getText().equals("Profiles")) {
            UI.getCurrent().getNavigator().navigateTo(ItemProfileView.VIEW_KEY);
        } else if (selectedItem.getText().equals("ProfileProperties")) {
            UI.getCurrent().getNavigator().navigateTo(ItemProfilePropertyView.VIEW_KEY);
        } else if (selectedItem.getText().equals("ProfileValues")) {
            UI.getCurrent().getNavigator().navigateTo(ItemProfileValueView.VIEW_KEY);
        } else if (selectedItem.getText().equals("PropertyDescriptor")) {
            UI.getCurrent().getNavigator().navigateTo(PropertyDescriptorView.VIEW_KEY);
        } else {
            UI.getCurrent().getNavigator().navigateTo(ItemView.VIEW_KEY);
        }
    };


    private final MenuBar.Command menuCommand = (MenuBar.Command) selectedItem -> {
        System.out.println(selectedItem);
        System.out.println(selectedItem.getText());

        System.out.println(Thread.currentThread().getId());

        if (selectedItem.getText().equals("List")) {
            UI.getCurrent().getNavigator().navigateTo(ObjectsView.VIEW_KEY);
        } else if (selectedItem.getText().equals("Users")) {
            UI.getCurrent().getNavigator().navigateTo(UsersView.VIEW_KEY);
        } else if (selectedItem.getText().equals("Dictionary")) {
            UI.getCurrent().getNavigator().navigateTo(DictionariesView.VIEW_KEY);
        } else if (selectedItem.getText().equals("Items")) {
            UI.getCurrent().getNavigator().navigateTo(ItemView.VIEW_KEY);
        } else {
            UI.getCurrent().getNavigator().navigateTo(ObjectsView.VIEW_KEY);
        }
    };
}
