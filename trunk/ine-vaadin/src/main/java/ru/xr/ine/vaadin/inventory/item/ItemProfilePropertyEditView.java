/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin.inventory.item;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.oss.inventory.item.ItemProfile;
import ru.xr.ine.oss.inventory.item.ItemProfileProperty;
import ru.xr.ine.oss.inventory.item.PropertyDescriptor;
import ru.xr.ine.vaadin.AdminUiServlet;

import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfilePropertyEditView.java 62 2017-05-06 10:12:20Z xerror $"
 */
public class ItemProfilePropertyEditView extends VerticalLayout implements View {

    public static final String VIEW_KEY = ItemProfileProperty.class.getName() + "Editor";
    public static final String PROF_PROP_KEY = ItemProfilePropertyEditView.class.getName() + "_PROP";

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {

        this.removeAllComponents();
        this.setSizeFull();

        ItemProfileProperty profileProperty =
                (ItemProfileProperty) getUI().getSession().getAttribute(ItemProfilePropertyEditView.PROF_PROP_KEY);

        TextField idText = new TextField("Property ID");
        idText.setValue(profileProperty.getCoreId().toString());
        idText.setEnabled(false);
        this.addComponent(idText);


        // Create ITEM_PROFILE element
        Collection<ItemProfile> itemProfiles = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        itemProfiles.addAll(((AdminUiServlet) VaadinServlet.getCurrent()).getItemProfiles());
        ComboBox<ItemProfile> profileComboBox = new ComboBox<>("Profile");
        profileComboBox.setEmptySelectionAllowed(false);
        profileComboBox.setItemCaptionGenerator(item -> item.getCoreId() + " : " + item.getName());
        profileComboBox.setWidth(370, Unit.PIXELS);
        profileComboBox.setItems(itemProfiles);
        ItemProfile selectedProfile = null;
        for (ItemProfile itemProfile : itemProfiles) {
            if (itemProfile.getCoreId().equals(profileProperty.getItemProfileId())) {
                selectedProfile = itemProfile;
                break;
            }
        }

        if (selectedProfile == null) {
            selectedProfile = itemProfiles.iterator().next();
        }

        profileComboBox.setSelectedItem(selectedProfile);
        this.addComponent(profileComboBox);

        // Create PropertyDescriptor element
        Collection<PropertyDescriptor> descriptors = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
        descriptors.addAll(((AdminUiServlet) VaadinServlet.getCurrent()).getPropertyDescriptors());
        ComboBox<PropertyDescriptor> descriptorComboBox = new ComboBox<>("PropertyDescriptors", descriptors);
        descriptorComboBox.setEmptySelectionAllowed(false);
        descriptorComboBox.setItemCaptionGenerator(item ->
                item.getCoreId() + " : " + item.getType() + " : " + item.getName());
        descriptorComboBox.setWidth(370, Unit.PIXELS);
        for (PropertyDescriptor descriptor : descriptors) {
            if (descriptor.getCoreId().equals(profileProperty.getPropertyDescriptorId())) {
                descriptorComboBox.setValue(descriptor);
                break;
            }
        }
        this.addComponent(descriptorComboBox);

        // Default value
        TextField defValField = new TextField("Default Value");

        if (profileProperty.getDefaultValue() != null) {
            defValField.setValue(profileProperty.getDefaultValue());
        }
        this.addComponent(defValField);

        // Pattern
        TextField patternField = new TextField("Pattern");
        if (profileProperty.getPattern() != null) {
            defValField.setValue(profileProperty.getPattern().toString());
        }
//        this.addComponent(patternField);

        // Check pattern button
        Button button = new Button("Check");
        button.addClickListener(event1 -> {
            String patternString = patternField.getValue();
            if (patternString != null && patternString.length() > 0) {
                Collection<Identifiable> identifiables =
                        ((AdminUiServlet) VaadinServlet.getCurrent()).checkPattern(patternString);

                if(identifiables.size() > 0){
                    Grid<Identifiable> components = new Grid<>(Identifiable.class);
                    components.setCaption("Filtered: " + identifiables.iterator().next().getClass().getSimpleName());
                    components.setItems(identifiables);

                    Window window = new Window("Objects");
                    VerticalLayout verticalLayout = new VerticalLayout(components);
                    window.setContent(verticalLayout);
                    window.center();
                    UI.getCurrent().addWindow(window);
                }
            }
        });

        HorizontalLayout patternLayOut = new HorizontalLayout(patternField, button);
        patternLayOut.setDefaultComponentAlignment(Alignment.BOTTOM_LEFT);
        patternLayOut.setComponentAlignment(button, Alignment.BOTTOM_LEFT);
//        patternLayOut.setSizeFull();
        this.addComponent(patternLayOut);

        // Description
        TextArea descriptionArea = new TextArea("Description");
        descriptionArea.setValue(profileProperty.getCoreDsc());
        this.addComponent(descriptionArea);

        CheckBox reqCheckBox = new CheckBox("Required");
        reqCheckBox.setValue(profileProperty.isRequired());
        this.addComponent(reqCheckBox);

        CheckBox enCheckBox = new CheckBox("Editable");
        enCheckBox.setValue(profileProperty.isEditable());
        this.addComponent(enCheckBox);

        Button saveButton = new Button("Save");
        saveButton.addClickListener(event1 -> {
            UI.getCurrent().getNavigator().navigateTo(ItemProfilePropertyView.VIEW_KEY);
            Notification.show("Save in process");
        });
        Button cancelButton = new Button("Cancel");
        cancelButton.addClickListener(event1 -> {
            UI.getCurrent().getNavigator().navigateTo(ItemProfilePropertyView.VIEW_KEY);
            Notification.show("Cancel");
        });

        HorizontalLayout buttons = new HorizontalLayout(saveButton, cancelButton);
        this.addComponent(buttons);
    }
}
