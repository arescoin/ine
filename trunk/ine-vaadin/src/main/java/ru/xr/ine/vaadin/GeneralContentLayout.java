/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.ui.VerticalLayout;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: GeneralContentLayout.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class GeneralContentLayout extends VerticalLayout {
}
