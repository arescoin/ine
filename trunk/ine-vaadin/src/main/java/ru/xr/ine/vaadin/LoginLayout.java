/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.navigator.Navigator;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.*;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.userpermits.NoSuchUserException;
import ru.xr.ine.core.userpermits.Role;
import ru.xr.ine.core.userpermits.UserRole;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeSet;

//import com.vaadin.ui.themes.Reindeer;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: LoginLayout.java 59 2017-05-04 16:58:36Z xerror $"
 */
public class LoginLayout extends VerticalLayout {
    public static final String NAME = "login";

    private final TextField user;

    private final PasswordField password;

    private final Button loginButton;

    private Navigator navigator;
    private final Panel loginPanel;


    public LoginLayout() {

        // Create the user input field
        user = new TextField("User:");
        user.setPlaceholder("UserName");
        user.setWidth("200px");
        user.setMaxLength(255);
        user.focus();
//        user.setsetRequired(true);
//        user.setInputPrompt("Your username (eg. joe@email.com)");
//        user.addValidator(new RegexpValidator("[а-яА-Яa-zA-Z0-9._&-@]{4,255}", true, "Invalid login format"));

        // Create the password input field
        password = new PasswordField("Password:");
        password.setWidth("200px");
//        password.addValidator(new RegexpValidator("[а-яА-Яa-zA-Z0-9._&-@]{4,255}", true, "Invalid password format"));
//        password.setRequired(true);
        password.setValue("");
//        password.setNullRepresentation("");

        loginPanel = new Panel();

        // Create login button
        loginButton = new Button("Login", (Button.ClickEvent event) -> {

            try {
                ((AdminUiServlet) VaadinServlet.getCurrent()).login(user.getValue(), password.getValue());
            } catch (Exception e) {
                if (e instanceof EJBException && (((EJBException) e).getCausedByException() instanceof NoSuchUserException)) {
                    new Notification("Ooops...", e.getMessage(), Notification.Type.WARNING_MESSAGE).show(Page.getCurrent());
                } else {
                    new Notification("Error", "Sometings wrong...").show(Page.getCurrent());
                }
                return;
            }

            Collection<UserRole> userRoles = new TreeSet<>(Comparator.comparing(Identifiable::getCoreId));
            userRoles.addAll((Collection<UserRole>)
                    VaadinSession.getCurrent().getAttribute(UserRole.class.getName()));

            if (userRoles != null && !userRoles.isEmpty()) {
                if (userRoles.size() == 1) {
                    AdminUiServlet.getCurrentProfile().setCurrentRole(userRoles.iterator().next().getCoreId());
                    ((AdminUi) getUI()).changeContent();
                } else {


                    Map<BigDecimal, Role> map = ((AdminUiServlet) AdminUiServlet.getCurrent()).getRoles();

                    ComboBox<UserRole> roleComboBox = new ComboBox<>("Select role", userRoles);
                    roleComboBox.focus();
                    roleComboBox.setEmptySelectionAllowed(false);

                    roleComboBox.setItemCaptionGenerator(item -> map.get(item.getCoreId()).getName());
                    roleComboBox.addValueChangeListener(event1 ->
                            AdminUiServlet.getCurrentProfile().setCurrentRole(event1.getValue().getCoreId()));

                    if (!userRoles.isEmpty()) {
                        roleComboBox.setSelectedItem(userRoles.iterator().next());
                    }

                    VerticalLayout fields = new VerticalLayout(roleComboBox);
                    fields.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
                    Button roleButton = new Button("Use role");
                    roleButton.addClickListener(event1 -> ((AdminUi) getUI()).changeContent());

                    fields.addComponent(roleButton);
                    loginPanel.setContent(fields);
                }
            }

        });

        VerticalLayout fields = new VerticalLayout(user, password, loginButton);
        fields.setSpacing(true);
        fields.setMargin(new MarginInfo(true, true, true, true));
        fields.setSizeUndefined();
        fields.setComponentAlignment(loginButton, Alignment.MIDDLE_CENTER);

        loginPanel.setContent(fields);
        loginPanel.setWidthUndefined();
        loginPanel.setId("loginGrpPanel");
        loginPanel.addStyleName("loginPanel-background");

        this.addComponent(loginPanel);

        this.setSizeFull();
        this.setComponentAlignment(loginPanel, Alignment.MIDDLE_CENTER);
        this.setStyleName("loginView-background");
    }

}
