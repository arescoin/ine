/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.vaadin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Grid;
import com.vaadin.ui.VerticalLayout;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.structure.SystemObject;

import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ObjectsView.java 62 2017-05-06 10:12:20Z xerror $"
 */
public class ObjectsView extends VerticalLayout implements View {

    public static final String VIEW_KEY = "objectsView";

    private Grid<SystemObject> objectGrid = new Grid<>("System Objects");

//    private final TreeTable treeTable;

    public ObjectsView() {


        Collection<SystemObject> systemObjects = ((AdminUiServlet) VaadinServlet.getCurrent()).getAllSystemObjects();

        SortedSet<SystemObject> sortedSet = new TreeSet<>(new Comparator<SystemObject>() {
            @Override
            public int compare(SystemObject o1, SystemObject o2) {
                return o1.getCoreId().compareTo(o2.getCoreId());
            }
        });

        sortedSet.addAll(systemObjects);

        objectGrid.setSizeFull();

        objectGrid.setItems(sortedSet);

        objectGrid.addColumn(Identifiable::getCoreId).setCaption("ID");
        objectGrid.addColumn(SystemObject::getName).setCaption("Name");
        objectGrid.addColumn(SystemObject::getType).setCaption("Type");
        objectGrid.addColumn(SystemObject::getUp).setCaption("UP Obj.");
        objectGrid.addColumn(SystemObject::getPattern).setCaption("Pattern");
        objectGrid.addColumn(SystemObject::getCoreDsc).setCaption("Description");

//        this.treeTable = new TreeTable("Test TBL");
//        this.treeTable.setSizeFull();
//        this.treeTable.addContainerProperty("ID", BigDecimal.class, null);
//        this.treeTable.addContainerProperty("Name", String.class, null);
//        this.treeTable.addContainerProperty("Type", BigDecimal.class, null);
//        this.treeTable.addContainerProperty("Up", BigDecimal.class, null);
//        this.treeTable.addContainerProperty("Dsc", String.class, null);
//
//        this.treeTable.setPageLength(30);

//        for (SystemObject systemObject : sortedSet) {
//            this.treeTable.addItem(new Object[]{
//                    systemObject.getCoreId(),
//                    systemObject.getName(),
//                    systemObject.getType(),
//                    systemObject.getUp(),
//                    systemObject.getCoreDsc()
//            }, systemObject.getCoreId());
//
//            if (systemObject.getUp() != null) {
//                treeTable.setParent(systemObject.getCoreId(), systemObject.getUp());
//                if (!systemObject.getType().equals(BigDecimal.ONE)) {
//                    treeTable.setCollapsed(systemObject.getCoreId(), false);
//                    treeTable.setChildrenAllowed(systemObject.getCoreId(), false);
//                }
//            }
//        }

//        treeTable.setPageLength(30);
//        treeTable.setHeight(100, Unit.PERCENTAGE);

    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
        this.removeAllComponents();
        this.addComponent(objectGrid);
//        this.addComponent(treeTable);
//        this.setExpandRatio(treeTable, 1);
//        this.setSizeFull();
    }

}
