/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.dic.DictionaryTerm;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.dic.DictionaryTerm} объектам
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface DictionaryTermBean<T extends DictionaryTerm> extends DictionaryEntryBean<T> {

    /**
     * Метод возвращает все действительные словарные термины указанного словаря на указанном языке
     *
     * @param user         пользователь, запрашивающий словарные статьи
     * @param dictionaryId идентификатор словаря
     * @param languageId   идентификатор  языка
     * @return коллекция словарных статей {@link ru.xr.ine.core.dic.DictionaryEntry}
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    Collection<T> getAllTermsForDictionaryAndLanguage(UserProfile user, BigDecimal dictionaryId, BigDecimal languageId)
            throws GenericSystemException;

    /**
     * Метод возвращает все действительные словарные термины c указанным идентификтором словаря и на всех языках
     *
     * @param user         пользователь, запрашивающий словарные статьи
     * @param dictionaryId идентификатор словаря
     * @param entryId      идентификатор словарной статьи
     * @return коллекция словарных статей {@link ru.xr.ine.core.dic.DictionaryEntry}
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    Collection<T> getAllTermsForDictionaryAndEntry(UserProfile user, BigDecimal dictionaryId, BigDecimal entryId)
            throws GenericSystemException;

    /**
     * Метод добавляет словарный термин в словарь без генерации нового кода статьи.
     * Т.е. код должен быть выставлен заранее.
     *
     * @param user         пользователь, добавляющий словарный термин
     * @param identifiable добавляемый словарный термин
     * @return добавленный термин. если термин уже есть, то вернётся существующий, иначе вернётся добавенный
     * @throws GenericSystemException в случае ошибок при работе с базой, некорректных данных и при проверок типа
     * локализации словаря - для полной локализации должны быть статьи на всех системных языках
     */
    T addObject(UserProfile user, T identifiable) throws GenericSystemException;

    /**
     * Метод создаёт переданные словарные термины и возвращает сгенерированный код для них.
     * <br>
     * Необходимо использовать для создания словарной статьи в словаре, требующем заполнения терминов на всех системных
     * языках. Использование метода {@link #createObject(ru.xr.ine.core.UserProfile, ru.xr.ine.core.Identifiable)}
     * возможно только для словаря с частичной локализацией.
     *
     * @param user  пользователь, создающий словарные термины
     * @param terms коллекция создаваемых терминов одной статьи для различных системных языков.
     * @return сгенерированный код словарной статьи
     * @throws GenericSystemException в случае ошибок при работе с базой, некорректных данных и при проверок типа
     * локализации словаря - для полной локализации должны быть статьи на всех системных языках
     */
    BigDecimal createDictionaryTerms(UserProfile user, Collection<T> terms) throws GenericSystemException;

    /**
     * Метод добавляет коллекцию словарных терминов в словарь без генерации нового кода статьи.
     * Т.е. код должен быть выставлен заранее.
     *
     * @param user  пользователь, добавляющий термины
     * @param terms добавляемые термины
     * @throws GenericSystemException в случае ошибок при работе с базой, некорректных данных и при проверок типа
     * локализации словаря - для полной локализации должны быть статьи на всех системных языках
     */
    void addDictionaryTerms(UserProfile user, Collection<T> terms) throws GenericSystemException;

}
