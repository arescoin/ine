/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.Versionable;

import java.util.Collection;
import java.util.Date;

/**
 * Базовый интерфейс для j2ee доступа к Versionable ine-объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface VersionableBean<T extends Versionable> extends IdentifiableBean<T> {

    /**
     * Получает предыдущие версии переданного объекта, оторбражающие историю его изменения.
     *
     * @param user        пользователь который запрашивает историю
     * @param versionable объект, для которого получается история
     *
     * @return предыдущие версии объекта
     * @throws ru.xr.ine.core.GenericSystemException
     *          при ошибках получения и обработки данных
     */
    Collection<T> getOldVersions(UserProfile user, T versionable) throws GenericSystemException;

    Collection<T> getOldVersions(UserProfile user, T versionable, Date fd, Date td) throws GenericSystemException;

}
