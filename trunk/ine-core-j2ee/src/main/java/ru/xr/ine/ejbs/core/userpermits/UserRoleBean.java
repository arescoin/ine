/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.userpermits.UserRole;
import ru.xr.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.userpermits.UserRole} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface UserRoleBean<T extends UserRole> extends VersionableBean<T> {

    /**
     * Метод возвращает список всех ролей пользователя
     *
     * @param userId идентификатор пользователя
     * @return коллекция объектов UserRole
     * @throws ru.xr.ine.core.CoreException
     *          при некорректных значениях в датах системного объекта
     */
    Collection<T> getRolesByUserId(UserProfile user, BigDecimal userId) throws CoreException;

}
