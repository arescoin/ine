/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.language.SysLanguage;
import ru.xr.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.language.SysLanguage} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SysLanguageBean<T extends SysLanguage> extends VersionableBean<T> {
}
