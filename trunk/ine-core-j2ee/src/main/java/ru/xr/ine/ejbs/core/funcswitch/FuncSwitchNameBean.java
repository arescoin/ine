/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.funcswitch;

import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.ejbs.core.IdentifiableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.funcswitch.FuncSwitchName} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface FuncSwitchNameBean<T extends FuncSwitchName> extends IdentifiableBean<T> {
}
