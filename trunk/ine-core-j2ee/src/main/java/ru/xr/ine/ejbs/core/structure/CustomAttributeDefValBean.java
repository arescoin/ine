/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.structure.CustomAttributeDefVal;
import ru.xr.ine.ejbs.core.VersionableBean;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomAttributeDefValBean<T extends CustomAttributeDefVal> extends VersionableBean<T> {
}
