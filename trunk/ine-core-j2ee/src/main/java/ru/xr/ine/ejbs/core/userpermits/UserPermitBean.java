/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.userpermits.UserPermit;
import ru.xr.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.userpermits.UserPermit} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface UserPermitBean<T extends UserPermit> extends VersionableBean<T> {
}
