/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.constants;

import ru.xr.ine.core.constants.ParametrizedConstantValue;
import ru.xr.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.constants.ParametrizedConstantValue} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ParametrizedConstantValueBean<T extends ParametrizedConstantValue> extends VersionableBean<T> {
}
