/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

/**
 * Список состояний запуска системы.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: StartupState.java 29 2017-04-04 15:32:19Z xerror $"
 */
public enum StartupState {
    /** Система в процессе запуска */
    IN_PROGRESS,
    /** Система успешно запущена */
    STARTED,
    /** Запуск системы завершился неуспешно */
    START_FAILED
}
