/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.ejbs.core.IdentifiableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.history.IdentifiableHistory} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IdentifiableHistoryBean<T extends IdentifiableHistory> extends IdentifiableBean<T> {
    /**
     * Метод возвращает все записи об изменениях в идентифицируемого объекта
     *
     * @param user     пользователь, запрашивающий историю объекта
     * @param sysObjId идентификатор системного объекта
     * @param entityId идентификатор сущности
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.GenericSystemException при некорректых значениях
     */
    Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException;

}
