/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.structure.CustomType} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomTypeBean<T extends CustomType> extends VersionableBean<T> {

    /**
     * Метод возвращает базовые интерфейсы которые могут быть расширены
     *
     * @param user пользователь
     * @return коллекция интерфейсов которые могут быть установлены в качестве базовых
     * @throws ru.xr.ine.core.GenericSystemException при некорректных значениях в данных
     */
    Collection<String> getBaseObjects(UserProfile user) throws GenericSystemException;

    /**
     * Возвращает актуальный набор модифицированных типов для указанного базового интерфейса
     *
     * @param user       пользователь
     * @param baseObject базовый интерфейс
     * @return коллекция модифицированные типов
     * @throws ru.xr.ine.core.GenericSystemException при ошибке получения данных
     */
    Collection<T> getTypesForBaseObject(UserProfile user, BigDecimal baseObject) throws GenericSystemException;

    /**
     * Возвращет есть ли объекты указанного модифицированного типа
     *
     * @param user       пользователь
     * @param customType модифицированный тип
     * @return true если есть объекты этого типа
     * @throws ru.xr.ine.core.GenericSystemException при ошибке получения данных
     */
    boolean isCustomTypeUsed(UserProfile user, T customType) throws GenericSystemException;

    /**
     * Возвращает коллекцию всех атрибутов модифицированного типа, включая унаследованные
     *
     * @param user       пользователь
     * @param customType модицированный тип
     * @return полный набор атрибутов
     * @throws GenericSystemException при ошибках доступа к данным или некорректных данных
     */
    Collection<CustomAttribute> getAllAttributes(UserProfile user, T customType) throws GenericSystemException;

    /**
     * Возвращает коллекцию всех унаследованных атрибутов модифицированного типа
     *
     * @param user       пользователь
     * @param customType модицированный тип
     * @return набор унаследованных атрибутов
     * @throws GenericSystemException при ошибках доступа к данным или некорректных данных
     */
    Collection<CustomAttribute> getParentAttributes(UserProfile user, T customType) throws GenericSystemException;

}
