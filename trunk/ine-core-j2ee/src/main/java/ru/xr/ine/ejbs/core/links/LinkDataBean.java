/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.links;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.Versionable;
import ru.xr.ine.core.links.LinkData;
import ru.xr.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.links.LinkData} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface LinkDataBean<T extends LinkData> extends VersionableBean<T> {

    /**
     * Получает связные объекты для переданных {@link ru.xr.ine.core.Identifiable}-объекта и типа связи.
     *
     * @param user         пользователь
     * @param objectToLink один из участников связи, для которого получаются привязанные объекты
     * @param linkId       идентификатор описания связи {@link ru.xr.ine.core.links.Link}
     * @return коллекция идентификаторов объектов, привязанных к переданному
     *         {@link ru.xr.ine.core.Identifiable}-объекту
     * @throws ru.xr.ine.core.GenericSystemException в случае ошибки
     */
    <E extends Identifiable> Collection<E> getLinkedObjects(UserProfile user, Identifiable objectToLink,
            BigDecimal linkId) throws GenericSystemException;

    /**
     * Получает все связи в которых участвует переданный {@link ru.xr.ine.core.Identifiable}-объект.
     *
     * @param user         профайл  с пользователем
     * @param linkedObject один из участников связи
     * @return коллекция связей переданного {@link ru.xr.ine.core.Identifiable}-объекта
     * @throws ru.xr.ine.core.GenericSystemException в случае ошибки
     */
    Collection<T> getLinkDataByObject(UserProfile user, Identifiable linkedObject) throws GenericSystemException;

    /**
     * Получает все связи с указанным идентификатором описателя связи, в которых участвует переданный
     * {@link ru.xr.ine.core.Identifiable}-объект.
     *
     * @param user         профайл  с пользователем
     * @param linkedObject один из участников связи
     * @param linkId       идентификатор описателя связи
     * @return коллекция связей переданного {@link ru.xr.ine.core.Identifiable}-объекта
     * @throws ru.xr.ine.core.GenericSystemException в случае ошибки
     */
    Collection<T> getLinkDataByObject(UserProfile user, Identifiable linkedObject, BigDecimal linkId)
            throws GenericSystemException;

    /**
     * Создает связь между двумя объектами
     *
     * @param user              профайл  с пользователем
     * @param linkedObjectLeft  объект связи
     * @param linkedObjectRight объект связи
     * @return созданный объект связи
     * @throws GenericSystemException в случае некорректных данных или ошибок доступа к данным
     */
    T createLinkData(UserProfile user, Versionable linkedObjectLeft, Versionable linkedObjectRight)
            throws GenericSystemException;

    /**
     * Создает связь между двумя объектами
     *
     * @param user              профайл  с пользователем
     * @param linkedObjectLeft  объект связи
     * @param linkedObjectRight объект связи
     * @param type              тип связи между объктами
     * @return созданный объект связи
     * @throws GenericSystemException в случае некорректных данных или ошибок доступа к данным
     */
    T createLinkData(UserProfile user, Versionable linkedObjectLeft, Versionable linkedObjectRight, BigDecimal type)
            throws GenericSystemException;
}
