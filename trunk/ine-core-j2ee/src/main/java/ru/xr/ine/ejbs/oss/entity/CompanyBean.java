/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import ru.xr.ine.ejbs.core.VersionableBean;
import ru.xr.ine.oss.entity.Company;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.oss.entity.Company} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CompanyBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CompanyBean<T extends Company> extends VersionableBean<T> {
}
