/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.dic;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.dic.DictionaryEntry} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface DictionaryEntryBean<T extends DictionaryEntry> extends VersionableBean<T> {

    /**
     * Метод возвращает все действительные указанные словарные статьи для указанного словаря
     *
     * @param user         пользователь, запрашивающий словарные статьи
     * @param dictionaryId идентификатор словаря
     * @return коллекция словарных статей {@link ru.xr.ine.core.dic.DictionaryEntry}
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    public Collection<T> getAllEntriesForDictionary(UserProfile user, BigDecimal dictionaryId)
            throws GenericSystemException;
}
