/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.userpermits.Permit;
import ru.xr.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.userpermits.Permit} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface PermitBean<T extends Permit> extends VersionableBean<T> {
}
