/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.strings;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.strings.MessageString;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.strings.MessageString} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface MessageStringBean<T extends MessageString> {

    /**
     * Метод возвращает все строки локализации
     *
     * @param user пользователь, выполняющий запрос
     * @return коллекция строк локализации
     * @throws ru.xr.ine.core.CorruptedIdException
     *          при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException
     *          при некорректных значениях в датах системного объекта
     */
    Collection<T> getAllObjects(UserProfile user) throws CoreException;

    /**
     * Возвращает содержимое локализованного сообщения по указанным ключу и коду языка
     *
     * @param user      пользователь, выполняющий запрос
     * @param stringKey ключ сообщения
     * @param langCode  код языка
     * @return локализованное сообщение
     * @throws ru.xr.ine.core.CoreException
     *          при возникновении ошибок в процессе получения данных
     */
    T getStringByKeyAndLang(UserProfile user, String stringKey, BigDecimal langCode) throws CoreException;

    /**
     * Возвращает карту локализованных сообщений по указанным ключу в присутствующих языках
     *
     * @param user      пользователь, выполняющий запрос
     * @param stringKey ключ сообщения
     * @return карта локализованных сообщений
     * @throws ru.xr.ine.core.CoreException
     *          при возникновении ошибок в процессе получения данных
     */
    Map<BigDecimal, T> getStringsByKey(UserProfile user, String stringKey) throws CoreException;

    /**
     * Создает в системе новый объект
     *
     * @param user         пользователь, выполняющий запрос
     * @param identifiable новый объект
     * @return созданный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.xr.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    T createObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Изменяет свойства существующего в системе объекта
     *
     * @param user            пользователь, выполняющий запрос
     * @param identifiable    старый объект
     * @param newIdentifiable новый объект
     * @return модифицированный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.xr.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    T updateObject(UserProfile user, T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Удаляет существующий объект
     *
     * @param user         пользователь, выполняющий запрос
     * @param identifiable удаляемый объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.xr.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    void deleteObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException;

}
