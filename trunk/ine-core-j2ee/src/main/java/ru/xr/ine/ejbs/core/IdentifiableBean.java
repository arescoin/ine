/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;
import ru.xr.rims.services.StorageFilter;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

/**
 * Базовый интерфейс для j2ee доступа к Identifiable ine-объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IdentifiableBean<T extends Identifiable> extends SystemBean {

    /**
     * Возвращает составной идентификатор объекта.
     * <p/>
     * По-умолчанию в составной идентификатор попадает 1 идентификатор, содержащийся в столбце N таблицы объекта. Если
     * идентификатор объекта содержится в другом столбце или в нескольких столбцах - необходимо переопределить метод в
     * соответствующем дата-акцессе.
     *
     * @param user         пользователь, запрашивающий идентифиикатор
     * @param identifiable объект с идентификаторами
     * @return объект составного идентификатора.
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    SyntheticId getSyntheticId(UserProfile user, T identifiable) throws GenericSystemException;

    /**
     * Метод возвращает все действительные на текущий момент значения
     *
     * @param user пользователь выполнящий запрос
     * @return коллекция констант
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    Collection<T> getAllObjects(UserProfile user) throws GenericSystemException;

    /**
     * Возвращает актуальный объект по его идентификатору
     *
     * @param user пользователь, запрашивающий значение константы
     * @param id   перечисление идентификаторов: здесь не составной
     * @return актуальный {@link ru.xr.ine.core.Identifiable} объект
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    T getObjectById(UserProfile user, BigDecimal... id) throws GenericSystemException;

    /**
     * Создает в системе новый объект
     *
     * @param user         пользователь, создающий объект
     * @param identifiable новый объект
     * @return созданный объект
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    T createObject(UserProfile user, T identifiable) throws GenericSystemException;

    /**
     * Изменяет существующий в системе объект
     *
     * @param user            пользователь, модфицирующий объект
     * @param newIdentifiable новый объект
     * @return модифицированный объект
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     * @throws ru.xr.ine.core.IneNotActualVersionModificationException если изменяемый объект имеет неактуальную версию
     */
    T updateObject(UserProfile user, T newIdentifiable) throws GenericSystemException;

    /**
     * Удаляет существующий объект
     *
     * @param user         пользователь, удаляющий объект
     * @param identifiable удаляемый объект
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    void deleteObject(UserProfile user, T identifiable) throws GenericSystemException;

    /**
     * Возвращает множество полей интерфейса
     *
     * @param user пользователь
     * @return множество имен полей интерфейса
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения полей для интерфейса
     */
    Set<String> getInstanceFields(UserProfile user) throws GenericSystemException;

    /**
     * Подготавливает и возвращает SearchCriteriaProfile для типа
     * обрабатываемого конкретной реализацией IdentifiableAccess
     *
     * @param user пользователь системы
     * @return профиль для формирования критериев поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки
     */
    SearchCriteriaProfile getSearchCriteriaProfile(UserProfile user) throws GenericSystemException;

    /**
     * Возвращает идентификатор основного региона размещения в кеше
     *
     * @param user пользователь системы
     * @return идентификатор региона
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки
     */
    String getMainRegionKey(UserProfile user) throws GenericSystemException;

    /**
     * Подготавливает и возвращает StorageFilter для типа
     * обрабатываемого конкретной реализацией IdentifiableAccess
     *
     * @param user      пользователь системы
     * @param criteria  перечисление статических критериев поиска (могут отсутствовать)
     * @param criterion критерий поиска используемый для учета результатов предыдущего поиска
     * @param fieldName название поля используемого для формирования очередного запроса,
     *                  используется в значениях SearchCriterion
     * @return фильтр объектов в кеше
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки
     */
    StorageFilter createStorageFilter(UserProfile user,
            Set<SearchCriterion> criteria, SearchCriterion criterion, String fieldName) throws GenericSystemException;

    /**
     * Подготавливает и возвращает StorageFilter для типа
     * обрабатываемого конкретной реализацией IdentifiableAccess
     *
     * @param user      пользователь системы
     * @param criteria  перечисление статических критериев поиска
     * @param fieldName название поля используемого для формирования очередного запроса,
     *                  используется в значениях SearchCriterion
     * @return фильтр объектов в кеше
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки
     */
    StorageFilter createStorageFilter(UserProfile user, Set<SearchCriterion> criteria, String fieldName)
            throws GenericSystemException;

    /**
     * Производит поиск объектов по переданным критериям
     *
     * @param user           пользователь системы
     * @param searchCriteria критерии поиска
     * @return результат поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе выпонения поискового запроса
     */
    Collection<T> searchObjects(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException;

    /**
     * Производит поиск объектов по переданным критериям
     *
     * @param user           пользователь системы
     * @param searchCriteria критерии поиска
     * @param fieldName      название поля используемого для извлечения данных из найденных объектов
     * @return результат поиска - либо объекты поиска, либо извлечённые данные из объектов поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе выпонения поискового запроса
     */
    Collection searchObjects(UserProfile user, Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException;

    /**
     * Производит поиск объектов по переданным критериям
     *
     * @param user           пользователь системы
     * @param storageFilters критерии поиска
     * @return результат поиска - либо объекты поиска, либо извлечённые данные из объектов поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе выпонения поискового запроса
     */
    Collection searchObjects(UserProfile user, StorageFilter[] storageFilters) throws GenericSystemException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим переданным критериям поиска
     *
     * @param user           пользователь системы
     * @param searchCriteria критерии поиска
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    boolean containsObjects(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param user    пользователь системы
     * @param filters фильтры объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    boolean containsObjects(UserProfile user, StorageFilter[] filters) throws GenericSystemException;

    /**
     * Возвращает количество объектов в хранилище соответствующим переданным критериям поиска
     *
     * @param user           пользователь системы
     * @param searchCriteria критерии поиска
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    int getObjectCount(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param user    пользователь системы
     * @param filters фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    int getObjectCount(UserProfile user, StorageFilter[] filters) throws GenericSystemException;
}
