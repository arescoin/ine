/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.userpermits;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.userpermits.NoSuchUserException;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.userpermits.SystemUser} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SystemUserBean<T extends SystemUser> extends VersionableBean<T> {

    /**
     * Возвращает пользователя системы по его системному имени и хешу пароля
     *
     * @param login системное имя пользователя
     * @param md5   хеш пароля пользователя
     * @return пользователь
     * @throws ru.xr.ine.core.userpermits.NoSuchUserException если пользователь с указанными данными не найден
     * @throws ru.xr.ine.core.GenericSystemException при ошщибках доступа к данным
     */
    T login(String login, String md5) throws GenericSystemException, NoSuchUserException;

}
