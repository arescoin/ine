/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.ejbs.core.VersionableBean;

import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.structure.CustomAttribute} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomAttributeBean<T extends CustomAttribute> extends VersionableBean<T> {

    /**
     * Возвращает список атрибутов конкретного объекта.
     *
     * @param user              пользователь, выполняющий запрос
     * @param identifiableClass класс интерфейса объекта, для которого получаются атрибуты.
     * @return коллекция описаний атрибутов
     * @throws ru.xr.ine.core.GenericSystemException при некорректных значениях в данных
     */
    Collection<T> getAttributesByObject(UserProfile user, Class<? extends Identifiable> identifiableClass)
            throws GenericSystemException;

    /**
     * Возвращает коллекцию {@link ru.xr.ine.core.structure.CustomAttribute настраиваемых атрибутов}
     * для переданного класса Identifiable-объекта, дополненную атрибутами переданного
     * {@link ru.xr.ine.core.structure.CustomType модифицированного типа}.
     *
     * @param user              пользователь, выполняющий запрос
     * @param identifiableClass класс реализации Identifiable-объекта.
     * @param customType        модифицированный тип. Может быть <code>null</code>.
     *                          Тогда вернутся атрибуты только базового объекта
     * @return коллекция {@link ru.xr.ine.core.structure.CustomAttribute настраиваемых атрибутов}
     * @throws ru.xr.ine.core.GenericSystemException в случае возникновения ошибок при доступе к мета-информации
     */
    Collection<CustomAttribute> getAttributes(UserProfile user, Class identifiableClass, CustomType customType)
            throws GenericSystemException;

    /**
     * Проверяет есть ли CustomAttibuteValue для описания CustomAttribute
     *
     * @param user пользователь, выполняющий запрос
     * @param t    объект на проверку
     * @return true если изпользуется
     * @throws GenericSystemException при ошибке доступа к данным
     */
    boolean isCustomAttributeUsed(UserProfile user, T t) throws GenericSystemException;

}
