/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.entity;

import ru.xr.ine.ejbs.core.VersionableBean;
import ru.xr.ine.oss.entity.Person;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.oss.entity.Person} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface PersonBean<T extends Person> extends VersionableBean<T> {
}
