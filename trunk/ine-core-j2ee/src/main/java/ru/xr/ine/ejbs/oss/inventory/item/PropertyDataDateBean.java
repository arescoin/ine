/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.inventory.item;

import ru.xr.ine.ejbs.core.VersionableBean;
import ru.xr.ine.oss.inventory.item.PropertyDataDate;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataDateBean.java 40 2017-04-07 21:30:01Z xerror $"
 */
public interface PropertyDataDateBean<T extends PropertyDataDate> extends VersionableBean<T> {
}
