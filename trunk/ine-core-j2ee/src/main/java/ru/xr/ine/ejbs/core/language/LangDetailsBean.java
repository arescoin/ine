/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.language;

import ru.xr.ine.core.language.LangDetails;
import ru.xr.ine.ejbs.core.IdentifiableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.language.LangDetails} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface LangDetailsBean<T extends LangDetails> extends IdentifiableBean<T> {
}
