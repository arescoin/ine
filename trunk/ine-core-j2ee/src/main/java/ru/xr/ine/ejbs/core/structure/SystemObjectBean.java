/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.ejbs.core.VersionableBean;

import java.util.Date;
import java.util.Set;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.structure.SystemObject} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SystemObjectBean<T extends SystemObject> extends VersionableBean<T> {

    /**
     * Возвращает набор поддерживаемых интерфейсов
     *
     * @param user профайл c пользователем делающим запрос
     * @return набор поддерживаемых интерфейсов
     * @throws GenericSystemException при возникновении ошибок в механизме доступа к мета
     */
    Set<String> getSupportedIntefaces(UserProfile user) throws GenericSystemException;

    /**
     * Возвращает текущее  время в базе данных
     *
     * @param user профайл c пользователем делающим запрос
     * @return текущее время в базе данных
     * @throws GenericSystemException при ошибке получении INE_CORE_SYS.getCurrentDate
     */
    public Date getCurrentDate(UserProfile user) throws GenericSystemException;

    /**
     * Возвращает системную дату начала жизни объекта
     *
     * @param user профайл c пользователем делающим запрос
     * @return системная дата начала жизни объектов
     * @throws GenericSystemException при ошибке получении INE_CORE_SYS.get_System_FromDate
     */
    public Date getSystemFd(UserProfile user) throws GenericSystemException;

    /**
     * Возвращает системную дату закрытия объекта
     *
     * @param user профайл c пользователем делающим запрос
     * @return системная дата начала закрытия объекта
     * @throws GenericSystemException при ошибке получении INE_CORE_SYS.get_System_ToDate
     */
    public Date getSystemTd(UserProfile user) throws GenericSystemException;

}
