/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core;

/**
 * Интерфейс базового InE-бина, отвечающего за подъём системы.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SystemBean {

    /** Системная настройка, отвечающая за необходимость проверки состояния запуска системы */
    String CHECK_SYSTEM_START_STATE_PROP_NAME = "CHECK_SYSTEM_START_STATE";

    /**
     * Получаем {@link StartupState статус запуска} системы.<br>
     * Если статус запуска системы отличен от {@link StartupState#IN_PROGRESS},
     * то бины должны прерывать любые операции.
     *
     * @return статус запуска системы
     */
    StartupState getState();

    /**
     * Метод для запуска системы. Не предназначен для прикладных вызовов!
     */
    void startSystem();
}
