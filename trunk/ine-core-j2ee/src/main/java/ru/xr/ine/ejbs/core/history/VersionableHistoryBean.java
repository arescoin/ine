/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.Versionable;
import ru.xr.ine.core.history.VersionableHistory;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.history.VersionableHistory} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface VersionableHistoryBean<T extends VersionableHistory> extends IdentifiableHistoryBean<T> {

    /**
     * Метод возвращает все записи об изменениях в идентифицируемом объекте
     *
     * @param user     пользователь, запрашивающий историю объекта
     * @param sysObjId идентификатор системного объекта
     * @param entityId идентификатор сущности
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.GenericSystemException при возникновении ошибок
     */
    Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException;

    /**
     * Метод возвращает версию объекта из истории
     *
     * @param user               пользователь запрашивающий версию объекта
     * @param versionableHistory момент истории объекта
     * @return объект
     * @throws GenericSystemException при возникновении ошибок
     */
    Versionable getObjectVersion(UserProfile user, T versionableHistory) throws GenericSystemException;

}
