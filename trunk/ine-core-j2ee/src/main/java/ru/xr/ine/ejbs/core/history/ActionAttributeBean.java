/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.history;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.history.ActionAttribute;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.ejbs.core.IdentifiableBean;

import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.history.ActionAttribute} объектам
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ActionAttributeBean<T extends ActionAttribute> extends IdentifiableBean<T> {

    /**
     * Метод возвращает пользовательские атрибуты для переданной записи об изменениях в идентифицируемых объектах
     *
     * @param user     пользователь, запрашивающий атрибуты истории объекта
     * @param history запись об изменениях в идентифицируемом объекте
     * @return коллекция пользовательских атрибутов
     * @throws ru.xr.ine.core.GenericSystemException в случае ошибок при работе с БД, кешем и метаинформацией
     */
    Collection<T> getActionAttributes(UserProfile user, IdentifiableHistory history) throws GenericSystemException;
}
