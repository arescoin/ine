/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.oss.inventory.item;

import ru.xr.ine.ejbs.core.VersionableBean;
import ru.xr.ine.oss.inventory.item.PropertyDataBoolean;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataBooleanBean.java 40 2017-04-07 21:30:01Z xerror $"
 */
public interface PropertyDataBooleanBean<T extends PropertyDataBoolean> extends VersionableBean<T> {
}
