/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.core.ipaddress;

import ru.xr.ine.core.ipaddress.IPAddressValue;
import ru.xr.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.xr.ine.core.ipaddress.IPAddressValue} объектам
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueBean.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IPAddressValueBean<T extends IPAddressValue> extends VersionableBean<T> {
}
