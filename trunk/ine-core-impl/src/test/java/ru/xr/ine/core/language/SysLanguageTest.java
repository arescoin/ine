/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SysLanguageTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return SysLanguage.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable details = IdentifiableFactory.getImplementation(idClass);
        details.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", null, details.getCoreDsc());
    }

    @Test(expected = IllegalIdException.class)
    public void testNullLangDetails() {
        try {
            IdentifiableFactory.getImplementation(SysLanguage.class).setLangDetails(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroLangDetails() {
        try {
            IdentifiableFactory.getImplementation(SysLanguage.class).setLangDetails(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeLangDetails() {
        try {
            IdentifiableFactory.getImplementation(SysLanguage.class).setLangDetails(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullCountryDetails() {
        try {
            IdentifiableFactory.getImplementation(SysLanguage.class).setCountryDetails(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroCountryDetails() {
        try {
            IdentifiableFactory.getImplementation(SysLanguage.class).setCountryDetails(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeCountryDetails() {
        try {
            IdentifiableFactory.getImplementation(SysLanguage.class).setCountryDetails(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        BigDecimal langDetails = new BigDecimal(1);
        BigDecimal countryDetails = new BigDecimal(1);
        try {
            SysLanguage sysLanguage = (SysLanguage) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            sysLanguage.setLangDetails(langDetails);
            Assert.assertSame("Set operation failed.", langDetails, sysLanguage.getLangDetails());

            sysLanguage.setCountryDetails(countryDetails);
            Assert.assertSame("Set operation failed.", countryDetails, sysLanguage.getCountryDetails());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
