/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.funcswitch;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FuncSwitchNameTest extends IdentifiableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return FuncSwitchName.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            IdentifiableFactory.getImplementation(FuncSwitchName.class).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            IdentifiableFactory.getImplementation(FuncSwitchName.class).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "TestFuncSwitch";
        String nameWS = "  TestFuncSwitch  ";

        try {
            FuncSwitchName swName = (FuncSwitchName) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            swName.setName(name);
            Assert.assertSame("Set operation failed.", name, swName.getName());

            swName.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, swName.getName());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
