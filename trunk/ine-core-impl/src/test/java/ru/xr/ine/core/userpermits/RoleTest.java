/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RoleTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RoleTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Role.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((Role) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((Role) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "Test name";
        String nameWS = "  Test name  ";

        try {
            Role role = (Role) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            role.setName(name);
            Assert.assertSame("Set operation failed.", name, role.getName());

            role.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, role.getName());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
