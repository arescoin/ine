/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemObjectTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return SystemObject.class;
    }

    @Test(expected = IllegalIdException.class)
    public void testNullType() {
        try {
            ((SystemObject) IdentifiableFactory.getImplementation(idClass)).setType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroType() {
        try {
            ((SystemObject) IdentifiableFactory.getImplementation(idClass)).setType(BigDecimal.ZERO);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeType() {
        try {
            ((SystemObject) IdentifiableFactory.getImplementation(idClass)).setType(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((SystemObject) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((SystemObject) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNotNullPatternForNonColumnType() {
        try {
            SystemObject systemObject = (SystemObject) IdentifiableFactory.getImplementation(idClass);
            systemObject.setType(SystemObjectType.table.getTypeCode());
            systemObject.setPattern(new SystemObjectPattern(BigDecimal.ONE));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testWrongTypeForNotNullPattern() {
        try {
            SystemObject systemObject = (SystemObject) IdentifiableFactory.getImplementation(idClass);
            systemObject.setPattern(new SystemObjectPattern(BigDecimal.ONE));
            systemObject.setType(SystemObjectType.table.getTypeCode());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        BigDecimal type = SystemObjectType.column.getTypeCode();
        String name = "Test name";
        String nameWS = "  Test name  ";
        BigDecimal up = BigDecimal.ONE;
        SystemObjectPattern pattern = new SystemObjectPattern(BigDecimal.ONE);

        try {
            SystemObject systemObject = (SystemObject) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            systemObject.setType(type);
            Assert.assertSame("Set operation failed.", type, systemObject.getType());

            systemObject.setName(name);
            Assert.assertSame("Set operation failed.", name, systemObject.getName());

            systemObject.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, systemObject.getName());

            systemObject.setUp(up);
            Assert.assertSame("Set operation failed.", up, systemObject.getUp());

            systemObject.setPattern(pattern);
            Assert.assertSame("Set operation failed.", pattern, systemObject.getPattern());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
