/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Ignore
public class CustomTypeTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return CustomType.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullTypeName() {
        try {
            IdentifiableFactory.getImplementation(CustomType.class).setTypeName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyTypeName() {
        try {
            IdentifiableFactory.getImplementation(CustomType.class).setTypeName("  ");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullBaseObject() {
        try {
            IdentifiableFactory.getImplementation(CustomType.class).setBaseObject(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testWrongBaseObject() {
        try {
            IdentifiableFactory.getImplementation(CustomType.class).setBaseObject(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testDoubleSetBaseObject() {
        try {
            CustomType type = IdentifiableFactory.getImplementation(CustomType.class);
            type.setBaseObject(new BigDecimal(1));
            type.setBaseObject(new BigDecimal(2));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String typeName = "TestName";
        String typeNameWS = "  TestName  ";
        BigDecimal baseObject = Identifiable.MIN_ALLOWABLE_VAL;

        try {
            Collection<CustomAttribute> attributes = new HashSet<CustomAttribute>();
            attributes.add(createCustomAttribute(new BigDecimal(1)));

            CustomType type = (CustomType) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            type.setTypeName(typeName);
            Assert.assertSame("Set operation failed.", typeName, type.getTypeName());

            type.setTypeName(typeNameWS);
            Assert.assertEquals("Set operation failed.", typeName, type.getTypeName());

            type.setBaseObject(baseObject);
            Assert.assertSame("Set operation failed.", baseObject, type.getBaseObject());

            type.setCustomAttributes(attributes);
            Assert.assertSame("Set operation failed.", attributes, type.getCustomAttributes());

            CustomType child = (CustomType) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            child.setParentTypeId(type.getCoreId());
            Assert.assertSame("Set operation failed.", type.getCoreId(), child.getParentTypeId());

            Collection<CustomAttribute> childAttributes = new HashSet<CustomAttribute>();
            childAttributes.add(createCustomAttribute(new BigDecimal(2)));
            child.setCustomAttributes(childAttributes);
            childAttributes.addAll(attributes);
            Assert.assertEquals("Set operation failed.", childAttributes, child.getCustomAttributes());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    private CustomAttribute createCustomAttribute(BigDecimal id) throws GenericSystemException {
        CustomAttribute attribute = (CustomAttribute)
                fillBasicFields(IdentifiableFactory.getImplementation(CustomAttribute.class));
        attribute.setCoreId(id);
        return attribute;
    }
}
