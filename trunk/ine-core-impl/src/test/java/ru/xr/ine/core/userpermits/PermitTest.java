/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PermitTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PermitTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Permit.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((Permit) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((Permit) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "Test name";
        String nameWS = "  Test name  ";
        String maskValue = "Test mask";
        boolean valueRequired = false;

        try {
            Permit permit = (Permit) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            permit.setName(name);
            Assert.assertSame("Set operation failed.", name, permit.getName());

            permit.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, permit.getName());

            permit.setMaskValue(maskValue);
            Assert.assertSame("Set operation failed.", maskValue, permit.getMaskValue());

            permit.setMaskValue(null);
            Assert.assertSame("Set operation failed.", null, permit.getMaskValue());

            permit.setValueRequired(valueRequired);
            Assert.assertSame("Set operation failed.", valueRequired, permit.isValueRequired());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
