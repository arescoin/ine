/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.util.Date;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class VersionableHistoryTest extends IdentifiableHistoryTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return VersionableHistory.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullFd() {
        try {
            ((VersionableHistory) IdentifiableFactory.getImplementation(idClass)).setFd(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullTd() {
        try {
            ((VersionableHistory) IdentifiableFactory.getImplementation(idClass)).setTd(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        Date fromDate = new Date(System.currentTimeMillis() - 20000);
        Date toDate = new Date(System.currentTimeMillis() + 20000);
        try {
            VersionableHistory history = (VersionableHistory) IdentifiableFactory.getImplementation(idClass);

            history.setFd(fromDate);
            Assert.assertSame("Set operation failed.", fromDate, history.getFd());

            history.setTd(toDate);
            Assert.assertSame("Set operation failed.", toDate, history.getTd());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        VersionableHistory history1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        VersionableHistory history2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        VersionableHistory history3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        history3.setCoreId(history1.getCoreId().add(history2.getCoreId()));

        Assert.assertTrue("HashCode operation failed. " + history1.hashCode() + ":" + history2.hashCode(),
                history1.hashCode() == history2.hashCode());
        Assert.assertTrue("Equals operation failed.", history1.equals(history2));
        Assert.assertTrue("Equals operation failed.", !history1.equals(history3));
    }

    @Override
    protected VersionableHistory fillBasicFields(Identifiable identifiable) {
        VersionableHistory versionableHistory = (VersionableHistory) super.fillBasicFields(identifiable);
        versionableHistory.setFd(new Date(20000));
        versionableHistory.setTd(new Date(200001));
        return versionableHistory;
    }
}
