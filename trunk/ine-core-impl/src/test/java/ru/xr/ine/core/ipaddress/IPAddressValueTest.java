/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.ipaddress;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.net.UnknownHostException;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IPAddressValueTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return IPAddressValue.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            IdentifiableFactory.getImplementation(IPAddressValue.class).setIpAddress(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        try {
            IPAddress address = IPAddress.parse("127.0.0.1");

            IPAddressValue value = (IPAddressValue) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            value.setIpAddress(address);
            Assert.assertSame("Set operation failed.", address, value.getIpAddress());
        } catch (UnknownHostException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
