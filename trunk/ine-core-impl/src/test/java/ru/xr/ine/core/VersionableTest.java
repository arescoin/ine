/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class VersionableTest extends IdentifiableTest {

    public static final Date DEFAULT_FD = new Date(System.currentTimeMillis() - 20000);
    public static final Date DEFAULT_TD = new Date(System.currentTimeMillis() + 20000);

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullFd() {
        try {
            ((Versionable) IdentifiableFactory.getImplementation(idClass)).setCoreFd(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullTd() {
        try {
            ((Versionable) IdentifiableFactory.getImplementation(idClass)).setCoreTd(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testFdAfterTd() {
        try {
            Versionable ver = (Versionable) IdentifiableFactory.getImplementation(idClass);
            ver.setCoreFd(new Date());
            ver.setCoreTd(new Date(ver.getCoreFd().getTime() - 1000));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        Versionable versionable1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        Versionable versionable2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

        Assert.assertTrue("HashCode operation failed.", versionable1.hashCode() == versionable2.hashCode());
        Assert.assertTrue("Equals operation failed.", versionable1.equals(versionable2));

        Versionable versionable3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        versionable3.setCoreId(versionable1.getCoreId().add(versionable2.getCoreId()));

        Assert.assertTrue("HashCode operation failed.", versionable1.hashCode() != versionable3.hashCode());
        Assert.assertTrue("Equals operation failed.", !versionable1.equals(versionable3));
    }

    @Test
    public void testSetDate() throws GenericSystemException {
        Versionable versionable = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        versionable.setCoreFd(DEFAULT_FD);
        versionable.setCoreTd(DEFAULT_TD);

        Assert.assertTrue("FromDate access failed", DEFAULT_FD.equals(versionable.getCoreFd()));
        Assert.assertTrue("ToDate access failed", DEFAULT_TD.equals(versionable.getCoreTd()));
    }

    @Override
    protected Versionable fillBasicFields(Identifiable identifiable) {
        Versionable versionable = (Versionable) super.fillBasicFields(identifiable);
        versionable.setCoreFd(DEFAULT_FD);
        versionable.setCoreTd(DEFAULT_TD);
        return versionable;
    }
}
