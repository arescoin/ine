/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class HistoryEventTest extends VersionableHistoryTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return HistoryEvent.class;
    }

    @Test
    public void testProcessed() {
        try {
            HistoryEvent historyEvent = (HistoryEvent) IdentifiableFactory.getImplementation(idClass);

            historyEvent.setProcessed(true);
            Assert.assertTrue(historyEvent.isProcessed());

            historyEvent.setProcessed(false);
            Assert.assertFalse(historyEvent.isProcessed());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
