/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.util.Date;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ActionAttributeTest extends IdentifiableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ActionAttribute.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullEntityId() {
        try {
            IdentifiableFactory.getImplementation(ActionAttribute.class).setEntityId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullActionDate() {
        try {
            IdentifiableFactory.getImplementation(ActionAttribute.class).setActionDate(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullAttributeName() {
        try {
            IdentifiableFactory.getImplementation(ActionAttribute.class).setAttributeName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        SyntheticId entityId = new SyntheticId();
        Date actionDate = new Date();
        String attrName = "TestAttributeName";
        String attrValue = "TestAttributeValue";
        try {
            ActionAttribute history = (ActionAttribute) fillBasicFields(
                    IdentifiableFactory.getImplementation(ActionAttribute.class));

            history.setEntityId(entityId);
            Assert.assertSame("Set operation failed.", entityId, history.getEntityId());

            history.setActionDate(actionDate);
            Assert.assertSame("Set operation failed.", actionDate, history.getActionDate());

            history.setAttributeName(attrName);
            Assert.assertSame("Set operation failed.", attrName, history.getAttributeName());

            history.setAttributeValue(attrValue);
            Assert.assertSame("Set operation failed.", attrValue, history.getAttributeValue());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
