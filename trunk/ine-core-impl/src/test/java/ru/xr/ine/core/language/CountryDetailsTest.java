/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CountryDetailsTest extends IdentifiableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return CountryDetails.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable details = IdentifiableFactory.getImplementation(idClass);
        details.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", details.getCoreDsc(), null);
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullCodeA2() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setCodeA2(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyCodeA2() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setCodeA2("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testLength1CodeA2() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setCodeA2("R");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testLength3CodeA2() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setCodeA2("RUS");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyCodeA3() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setCodeA3("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testLength2CodeA3() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setCodeA3("RU");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testLength4CodeA3() {
        try {
            IdentifiableFactory.getImplementation(CountryDetails.class).setCodeA3("TEST");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "Test name";
        String nameWS = "  Test name  ";
        String codeA2 = "RU";
        String codeA2WS = "  RU  ";
        String codeA3 = "RUS";
        String codeA3WS = "  RUS  ";
        try {
            CountryDetails details = (CountryDetails) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            details.setName(name);
            Assert.assertSame("Set operation failed.", name, details.getName());

            details.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, details.getName());

            details.setCodeA2(codeA2);
            Assert.assertSame("Set operation failed.", codeA2, details.getCodeA2());

            details.setCodeA2(codeA2WS);
            Assert.assertEquals("Set operation failed.", codeA2, details.getCodeA2());

            details.setCodeA3(codeA3);
            Assert.assertSame("Set operation failed.", codeA3, details.getCodeA3());

            details.setCodeA3(codeA3WS);
            Assert.assertEquals("Set operation failed.", codeA3, details.getCodeA3());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
