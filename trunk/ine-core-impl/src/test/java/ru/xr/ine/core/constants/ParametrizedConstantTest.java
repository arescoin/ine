/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ParametrizedConstantTest extends ConstantTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ParametrizedConstant.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullMask() {
        try {
            ((ParametrizedConstant) IdentifiableFactory.getImplementation(idClass)).setMask(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        super.testSetFields();
        try {
            SystemObjectPattern mask = new SystemObjectPattern(BigDecimal.ONE);

            ParametrizedConstant constant = (ParametrizedConstant) IdentifiableFactory.getImplementation(idClass);

            constant.setMask(mask);
            Assert.assertSame("Set operation failed.", mask, constant.getMask());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
