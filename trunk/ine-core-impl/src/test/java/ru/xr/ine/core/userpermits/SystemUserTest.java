/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.VersionableTest;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemUserTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return SystemUser.class;
    }

    @Test
    public void testSetFields() {
        boolean active = true;

        try {
            SystemUser systemUser = (SystemUser) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            systemUser.setActive(active);
            Assert.assertSame("Set operation failed.", active, systemUser.isActive());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
