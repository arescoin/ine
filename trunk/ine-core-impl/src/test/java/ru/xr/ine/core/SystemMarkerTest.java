/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import org.junit.Before;
import ru.xr.ine.utils.BaseTest;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemMarkerTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class SystemMarkerTest extends BaseTest{

    protected Class<? extends SystemMarker> idClass;

    protected abstract Class<? extends SystemMarker> getClassType();

    @Before
    public void setClass() {
        idClass = getClassType();
    }

}
