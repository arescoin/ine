/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RolePermitTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return RolePermit.class;
    }

    @Test(expected = IllegalIdException.class)
    public void testNullPermitId() {
        try {
            IdentifiableFactory.getImplementation(RolePermit.class).setPermitId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroPermitId() {
        try {
            IdentifiableFactory.getImplementation(RolePermit.class).setPermitId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativePermitId() {
        try {
            IdentifiableFactory.getImplementation(RolePermit.class).setPermitId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        RolePermit value1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        RolePermit value2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() == value2.hashCode());
        Assert.assertTrue("Equals operation failed.", value1.equals(value2));

        RolePermit value3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        value3.setCoreId(value1.getCoreId().add(value2.getCoreId()));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() != value3.hashCode());
        Assert.assertTrue("Equals operation failed.", !value1.equals(value3));
    }

    protected RolePermit fillBasicFields(Identifiable identifiable) {
        RolePermit value = (RolePermit) super.fillBasicFields(identifiable);
        value.setPermitId(Identifiable.MIN_ALLOWABLE_VAL);
        return value;
    }

    @Test
    public void testSetFields() {
        BigDecimal permitId = Identifiable.MIN_ALLOWABLE_VAL;
        BigDecimal[] permitValues = new BigDecimal[]{new BigDecimal(1)};
        try {
            RolePermit rolePermit = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            rolePermit.setPermitId(permitId);
            Assert.assertSame("Set operation failed.", permitId, rolePermit.getPermitId());

            rolePermit.setPermitValues(permitValues);
            Assert.assertSame("Set operation failed.", permitValues, rolePermit.getPermitValues());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
