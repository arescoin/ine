/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;
import ru.xr.ine.core.userpermits.CrudCode;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IdentifiableHistoryTest extends IdentifiableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return IdentifiableHistory.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullSysObjId() {
        try {
            IdentifiableFactory.getImplementation(IdentifiableHistory.class).setEntityId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullUserId() {
        try {
            IdentifiableFactory.getImplementation(IdentifiableHistory.class).setUserId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroUserId() {
        try {
            IdentifiableFactory.getImplementation(IdentifiableHistory.class).setUserId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeUserId() {
        try {
            IdentifiableFactory.getImplementation(IdentifiableHistory.class).setUserId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullActionDate() {
        try {
            IdentifiableFactory.getImplementation(IdentifiableHistory.class).setActionDate(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        SyntheticId entityId = new SyntheticId();
        BigDecimal userId = new BigDecimal(1);
        Date actionDate = new Date();
        try {
            IdentifiableHistory history = IdentifiableFactory.getImplementation(IdentifiableHistory.class);

            history.setEntityId(entityId);
            Assert.assertSame("Set operation failed.", entityId, history.getEntityId());

            history.setUserId(userId);
            Assert.assertSame("Set operation failed.", userId, history.getUserId());

            history.setActionDate(actionDate);
            Assert.assertSame("Set operation failed.", actionDate, history.getActionDate());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        IdentifiableHistory history1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        IdentifiableHistory history2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        IdentifiableHistory history3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        history3.setCoreId(history1.getCoreId().add(history2.getCoreId()));

        Assert.assertTrue("HashCode operation failed. " + history1.hashCode() + ":" + history2.hashCode(),
                history1.hashCode() == history2.hashCode());
        Assert.assertTrue("Equals operation failed.", history1.equals(history2));
        Assert.assertTrue("Equals operation failed.", !history1.equals(history3));
    }

    protected IdentifiableHistory fillBasicFields(Identifiable identifiable) {
        IdentifiableHistory identifiableHistory = (IdentifiableHistory) super.fillBasicFields(identifiable);
        identifiableHistory.setUserId(new BigDecimal(1));
        identifiableHistory.setEntityId(new SyntheticId());
        identifiableHistory.setActionDate(new Date(999999999));
        identifiableHistory.setType(CrudCode.create);
        return identifiableHistory;
    }
}
