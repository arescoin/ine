/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantValueTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ConstantValue.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullValue() {
        try {
            ConstantValue constantValue = (ConstantValue) IdentifiableFactory.getImplementation(idClass);
            constantValue.setValue(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testZeroValue() {
        try {
            ConstantValue constantValue = (ConstantValue) IdentifiableFactory.getImplementation(idClass);
            constantValue.setValue(" ");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        try {
            String value = "Test Value";
            String valueWS = "  Test Value  ";

            ConstantValue constantValue = (ConstantValue) IdentifiableFactory.getImplementation(idClass);

            constantValue.setValue(value);
            Assert.assertSame("Set operation failed.", value, constantValue.getValue());

            constantValue.setValue(valueWS);
            Assert.assertEquals("Set operation failed.", value, constantValue.getValue());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
