/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ConstantTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Constant.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((Constant) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((Constant) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

//    @Test(expected = IneIllegalArgumentException.class)
//    public void testNullType() {
//        try {
//            ((Constant) IdentifiableFactory.getImplementation(idClass)).setType(null);
//        } catch (GenericSystemException e) {
//            Assert.fail(e.getMessage() + " ==> " + e.toString());
//        }
//    }

    @Test
    public void testSetFields() {
        try {
            String name = "TestConstant";
            String nameWS = "  TestConstant  ";
            BigDecimal type = new BigDecimal(1);
            String defVal = "Default Value";
            boolean nullable = true;
            boolean modifiable = true;

            Constant constant = (Constant) IdentifiableFactory.getImplementation(idClass);

            constant.setName(name);
            Assert.assertSame("Set operation failed.", name, constant.getName());

            constant.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, constant.getName());

//            constant.setType(type);
//            Assert.assertSame("Set operation failed.", type, constant.getType());

            constant.setDefaultValue(defVal);
            Assert.assertSame("Set operation failed.", defVal, constant.getDefaultValue());

            constant.setNullable(nullable);
            Assert.assertSame("Set operation failed.", nullable, constant.isNullable());

            constant.setModifiable(modifiable);
            Assert.assertSame("Set operation failed.", modifiable, constant.isModifiable());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
