/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.strings;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class MessageStringTest extends SystemMarkerTest {

    @Override
    protected Class<? extends SystemMarker> getClassType() {
        return MessageString.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullKey() {
        try {
            IdentifiableFactory.getImplementationSM(MessageString.class).setKey(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyKey() {
        try {
            IdentifiableFactory.getImplementationSM(MessageString.class).setKey("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullLanguageCode() {
        try {
            IdentifiableFactory.getImplementationSM(MessageString.class).setLanguageCode(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroLanguageCode() {
        try {
            IdentifiableFactory.getImplementationSM(MessageString.class).setLanguageCode(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeLanguageCode() {
        try {
            IdentifiableFactory.getImplementationSM(MessageString.class).setLanguageCode(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullMessage() {
        try {
            IdentifiableFactory.getImplementationSM(MessageString.class).setMessage(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyMessage() {
        try {
            IdentifiableFactory.getImplementationSM(MessageString.class).setMessage("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String key = "Test_key";
        String keyWS = "  Test_key  ";
        BigDecimal languageCode = new BigDecimal(1);
        String message = "Test message";
        String messageWS = "  Test message  ";
        try {
            MessageString messageString = (MessageString) IdentifiableFactory.getImplementationSM(idClass);

            messageString.setKey(key);
            Assert.assertSame("Set operation failed.", key, messageString.getKey());

            messageString.setKey(keyWS);
            Assert.assertEquals("Set operation failed.", key, messageString.getKey());

            messageString.setLanguageCode(languageCode);
            Assert.assertSame("Set operation failed.", languageCode, messageString.getLanguageCode());

            messageString.setMessage(message);
            Assert.assertSame("Set operation failed.", message, messageString.getMessage());

            messageString.setMessage(messageWS);
            Assert.assertEquals("Set operation failed.", message, messageString.getMessage());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
