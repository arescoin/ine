/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.dic;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DictionaryImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryImpl extends AbstractVersionable implements Dictionary {

    /** Название словаря */
    private String name;

    /** тип словаря */
    private BigDecimal dicType;

    /** политика заполнения кодов словарных статей */
    private BigDecimal entryCodePolicy;

    /** политика локализации словарных статей */
    private BigDecimal localizationPolicy;

    /** идентификатор верхнего словаря */
    private BigDecimal parentId;


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Dictionary.NAME);
    }

    @Override
    public BigDecimal getDicType() {
        return this.dicType;
    }

    @Override
    public void setDicType(BigDecimal dicType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(dicType, Dictionary.DIC_TYPE);
        this.dicType = dicType;
    }

    @Override
    public BigDecimal getEntryCodePolicy() {
        return this.entryCodePolicy;
    }

    @Override
    public void setEntryCodePolicy(BigDecimal entryCodePolicy) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkSystemId(entryCodePolicy, Dictionary.CODE_POLICY);
        this.entryCodePolicy = entryCodePolicy;
    }

    @Override
    public BigDecimal getLocalizationPolicy() {
        return this.localizationPolicy;
    }

    @Override
    public void setLocalizationPolicy(BigDecimal localizationPolicy) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkSystemId(localizationPolicy, Dictionary.L10N_POLICY);
        this.localizationPolicy = localizationPolicy;
    }

    @Override
    public BigDecimal getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(BigDecimal parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + Dictionary.NAME + "[" + this.getName() + "], "
                + Dictionary.DIC_TYPE + "[" + this.getDicType() + "], "
                + Dictionary.PARENT + "[" + this.getParentId() + "], "
                + Dictionary.CODE_POLICY + "[" + this.getEntryCodePolicy() + "], "
                + Dictionary.L10N_POLICY + "[" + this.getLocalizationPolicy() + "]";
    }
}
