/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.AbstractVersionable;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataImpl.java 36 2017-04-06 00:05:15Z xerror $"
 */
public abstract class PropertyDataImpl<T> extends AbstractVersionable implements PropertyData<T> {

    private BigDecimal itemId;
    private BigDecimal descriptorId;
    protected T data;

    public BigDecimal getItemId() {
        return itemId;
    }

    public void setItemId(BigDecimal itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getDescriptorId() {
        return descriptorId;
    }

    public void setDescriptorId(BigDecimal descriptorId) {
        this.descriptorId = descriptorId;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + PropertyData.ITEM_ID + "[" + this.getItemId() + "], "
                + PropertyData.DESCRIPTOR_ID + "[" + this.getDescriptorId() + "], "
                + PropertyData.DATA + "[" + this.getData() + "]";
    }
}
