/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataStringImpl.java 41 2017-04-10 08:25:52Z xerror $"
 */
public class PropertyDataStringImpl extends PropertyDataImpl<String> implements PropertyDataString {

    @Override
    public void setData(String data) {
        this.data = data;
    }
}
