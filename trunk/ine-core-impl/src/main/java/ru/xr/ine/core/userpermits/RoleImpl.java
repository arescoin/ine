/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RoleImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RoleImpl extends AbstractVersionable implements Role {

    /** Название роли */
    private String name;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Role.NAME);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + Role.NAME + '[' + this.getName() + ']';
    }

}
