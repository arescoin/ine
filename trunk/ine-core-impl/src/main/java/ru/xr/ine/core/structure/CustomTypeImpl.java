/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * реализация описания модифицированного типа
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CustomTypeImpl extends AbstractVersionable implements CustomType {

    /** Идентификтор предка типа, null если тип ни от кого не наследуется */
    private BigDecimal parentTypeId = null;

    /** Наменование модифицированного типа */
    private String typeName;

    //Идентификатор базового типа объектов(сервис, продукт и пр) для модифицированного типа
    private BigDecimal baseObject = null;//не thread safe

    /** Настраиваемые аттрибуты в данном типе */
    private Collection<CustomAttribute> customAttributes;


    @Override
    public BigDecimal getBaseObject() {
        return baseObject;
    }

    @Override
    public void setBaseObject(BigDecimal baseObject) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkSystemId(baseObject, CustomType.BASE_OBJECT);

        if (this.baseObject == null) {
            this.baseObject = baseObject;
        } else {
            throw new IneIllegalArgumentException("baseObject has already been set", CustomType.BASE_OBJECT);
        }
    }

    @Override
    public BigDecimal getParentTypeId() {
        return this.parentTypeId;
    }

    @Override
    public void setParentTypeId(BigDecimal parentId) throws IneIllegalArgumentException {
        this.parentTypeId = parentId;
    }

    @Override
    public String getTypeName() {
        return this.typeName;
    }

    @Override
    public void setTypeName(String typeName) throws IneIllegalArgumentException {
        this.typeName = AbstractIdentifiable.checkEmpty(typeName, CustomType.TYPE_NAME);
    }

    @Override
    public Collection<CustomAttribute> getCustomAttributes() {
        return this.customAttributes;
    }

    @Override
    public void setCustomAttributes(Collection<CustomAttribute> attributes) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(attributes, "attributes");
        this.customAttributes = attributes;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + CustomType.BASE_OBJECT + '[' + this.getBaseObject() + "], "
                + CustomType.PARENT_TYPE + '[' + this.getParentTypeId() + "], "
                + CustomType.TYPE_NAME + '[' + this.getTypeName()
                + "], customAttributes[" + this.getCustomAttributes() + ']';
    }

}
