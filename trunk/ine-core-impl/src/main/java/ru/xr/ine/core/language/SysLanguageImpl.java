/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SysLanguageImpl extends AbstractVersionable implements SysLanguage {

    /** Описание языка (iso639) */
    private BigDecimal langDetails;

    /** Описание страны (iso3166) */
    private BigDecimal countryDetails;


    @Override
    public BigDecimal getLangDetails() {
        return this.langDetails;
    }

    @Override
    public void setLangDetails(BigDecimal langDetails) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(langDetails, SysLanguage.LANG_DETAILS);
        this.langDetails = langDetails;
    }

    @Override
    public BigDecimal getCountryDetails() {
        return this.countryDetails;
    }

    @Override
    public void setCountryDetails(BigDecimal countryDetails) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(countryDetails, SysLanguage.COUNTRY_DETAILS);
        this.countryDetails = countryDetails;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + SysLanguage.LANG_DETAILS + "[" + this.getLangDetails() + ']' + ", "
                + SysLanguage.COUNTRY_DETAILS + "[" + this.getCountryDetails() + ']';
    }

}
