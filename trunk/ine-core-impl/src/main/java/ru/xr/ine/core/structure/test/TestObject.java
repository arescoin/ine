/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure.test;

import ru.xr.ine.core.Versionable;

/**
 * Интерфейс для тестирования функциональности дополнительных атрибутов. Работает с таблицей TEST_TABLE.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: TestObject.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface TestObject extends Versionable {

    String VALUE = "value";

    /**
     * Получает тестовое поле.
     *
     * @return значение тестового поля.
     */
    String getValue();

    /**
     * Устанавливает тестовое поле
     *
     * @param value значение тестового поля
     */
    void setValue(String value);
}
