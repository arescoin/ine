/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class UserPermitImpl extends AbstractVersionable implements UserPermit {

    /** Идентификатор системного пользователя, обладателя доступа */
    private BigDecimal userId;

    /** Идентификатор роли, настраеваемой для пользователя */
    private BigDecimal roleId;

    /** Список значений для доступа */
    private BigDecimal[] values;

    /** Признак активности данного доступа */
    private boolean active;

    /** Маска CRUD */
    private int crudMask;


    @Override
    public BigDecimal getUserId() {
        return this.userId;
    }

    @Override
    public void setUserId(BigDecimal userId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(userId, UserPermit.USER_ID);
        this.userId = userId;
    }

    @Override
    public BigDecimal getRoleId() {
        return this.roleId;
    }

    @Override
    public void setRoleId(BigDecimal roleId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(roleId, UserPermit.ROLE_ID);
        this.roleId = roleId;
    }

    @Override
    public BigDecimal[] getValues() {
        return this.values;
    }

    @Override
    public void setValues(BigDecimal[] values) throws IneIllegalArgumentException {
        this.values = values;
    }

    @Override
    public boolean isActive() {
        return this.active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int getCrudMask() {
        return this.crudMask;
    }

    @Override
    public void setCrudMask(int crudMask) {

        int[] ints = CrudCode.getByMask(crudMask);
        CrudCode.toCodes(ints);

        this.crudMask = crudMask;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof UserPermitImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        UserPermitImpl that = (UserPermitImpl) o;

        if (this.getRoleId() != null ? !this.getRoleId().equals(that.getRoleId()) : that.getRoleId() != null) {
            return false;
        }
        //noinspection RedundantIfStatement
        if (this.getUserId() != null ? !this.getUserId().equals(that.getUserId()) : that.getUserId() != null) {
            return false;
        }

        return true;
    }

    @Override
    public int buildHashCode() {

        int result = super.hashCode();
        result = 31 * result + (this.getUserId() != null ? this.getUserId().hashCode() : 0);
        result = 31 * result + (this.getRoleId() != null ? this.getRoleId().hashCode() : 0);

        return result;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + UserPermit.USER_ID + '[' + this.getUserId() + "], "
                + UserPermit.ROLE_ID + '[' + this.getRoleId() + "], "
                + UserPermit.CRUD_MASK + '[' + this.getCrudMask() + "], "
                + UserPermit.VALUES + '[' + Arrays.toString(this.getValues()) + "], "
                + UserPermit.ACTIVE + '[' + this.isActive() + ']';
    }

}
