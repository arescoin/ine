/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantValueImpl
        extends AbstractVersionable implements ConstantValue {

    private String value;

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public void setValue(String value) throws IneIllegalArgumentException {
        this.value = AbstractIdentifiable.checkEmpty(value, ConstantValue.VALUE);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + ConstantValue.VALUE + "[" + this.getValue() + "]";
    }
}
