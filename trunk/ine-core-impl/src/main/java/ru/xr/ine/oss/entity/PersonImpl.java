/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.entity;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PersonImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PersonImpl extends AbstractVersionable implements Person {

    /** Фамилия */
    private String familyName;

    /** Имя */
    private String name;

    /** Отчество */
    private String middleName;

    /** Псевдоним */
    private String alias;

    /** Код словарной статьи предпочтительного обращения (Словарь 19) */
    private BigDecimal familyNamePrefixCode;

    /** Семейное поколение */
    private String familyGeneration;

    /** Тип прописки */
    private String formOfAddress;


    @Override
    public String getFamilyName() {
        return this.familyName;
    }

    @Override
    public void setFamilyName(String familyName) throws IneIllegalArgumentException {
        this.familyName = AbstractIdentifiable.checkEmpty(familyName, Person.FAMILY_NAME);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Person.NAME);
    }

    @Override
    public String getMiddleName() {
        return this.middleName;
    }

    @Override
    public void setMiddleName(String middleName) throws IneIllegalArgumentException {
        this.middleName = AbstractIdentifiable.checkEmpty(middleName, Person.MIDDLE_NAME);
    }

    @Override
    public String getAlias() {
        return this.alias;
    }

    @Override
    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public BigDecimal getFamilyNamePrefixCode() {
        return this.familyNamePrefixCode;
    }

    @Override
    public void setFamilyNamePrefixCode(BigDecimal familyNamePrefixCode) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(familyNamePrefixCode, Person.FAMILY_NAME_PREFIX);
        this.familyNamePrefixCode = familyNamePrefixCode;
    }

    @Override
    public String getFamilyGeneration() {
        return this.familyGeneration;
    }

    @Override
    public void setFamilyGeneration(String familyGeneration) {
        this.familyGeneration = familyGeneration;
    }

    @Override
    public String getFormOfAddress() {
        return this.formOfAddress;
    }

    @Override
    public void setFormOfAddress(String formOfAddress) {
        this.formOfAddress = formOfAddress;
    }

}
