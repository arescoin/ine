/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.AbstractVersionable;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemUserImpl extends AbstractVersionable implements SystemUser {

    /** Признак активности учетной записи пользователя */
    private boolean active;

    private String login;

    private String md5;

    private String fio;


    @Override
    public boolean isActive() {
        return this.active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String getLogin() {
        return this.login;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getMd5() {
        return this.md5;
    }

    @Override
    public void setMd5(String md5) {
        this.md5 = md5;
    }

    @Override
    public String toString() {
        return getClass().getName() + ": " +
                SystemUser.ACTIVE + '[' + this.isActive() + "], " +
                SystemUser.SYS_LOGIN + '[' + this.getLogin() + ']';
    }

}
