/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileValueImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ItemProfileValueImpl extends AbstractVersionable implements ItemProfileValue {

    private BigDecimal itemProfileId;
    private BigDecimal valueDescriptorId;
    private SystemObjectPattern pattern;
    private boolean required;

    public BigDecimal getItemProfileId() {
        return itemProfileId;
    }

    public void setItemProfileId(BigDecimal itemProfileId) {
        this.itemProfileId = itemProfileId;
    }

    public BigDecimal getValueDescriptorId() {
        return valueDescriptorId;
    }

    public void setValueDescriptorId(BigDecimal valueDescriptorId) {
        this.valueDescriptorId = valueDescriptorId;
    }

    public SystemObjectPattern getPattern() {
        return pattern;
    }

    public void setPattern(SystemObjectPattern pattern) {
        this.pattern = pattern;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + ItemProfileValue.ITEM_PROFILE_ID + "[" + this.getItemProfileId() + "], "
                + ItemProfileValue.VALUE_DESCRIPTOR_ID + "[" + this.getValueDescriptorId() + "], "
                + ItemProfileValue.PATTERN + "[" + this.getPattern() + "], "
                + ItemProfileValue.REQUIRED + "[" + this.isRequired() + "]";
    }

}
