/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.constants.Constant;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ItemProfileImpl extends AbstractVersionable implements ItemProfile {

    private String name;
    private BigDecimal itemType;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public BigDecimal getItemType() {
        return this.itemType;
    }

    @Override
    public void setItemType(BigDecimal itemType) {
        this.itemType = itemType;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + ItemProfile.PROFILE_NAME + "[" + this.getName() + "], "
                + ItemProfile.ITEM_TYPE + "[" + this.getItemType() + "]";
    }
}
