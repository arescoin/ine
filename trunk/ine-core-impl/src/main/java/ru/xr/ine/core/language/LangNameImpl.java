/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangNameImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class LangNameImpl extends AbstractIdentifiable implements LangName {

    /** Системное название языка */
    private String name;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, LangName.NAME);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + LangName.NAME + "[" + this.getName() + "]";
    }

}
