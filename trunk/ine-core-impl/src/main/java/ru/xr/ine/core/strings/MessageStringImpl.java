/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.strings;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class MessageStringImpl implements MessageString, Serializable {

    /** Уникальный строковый ключ (идентификатор) для данного локализационного ресурса */
    private String key;

    /** Системный идентификатор языка */
    private BigDecimal languageCode;

    /** Локализованный строковый ресурс */
    private String message;


    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public void setKey(String key) throws IneIllegalArgumentException {
        this.key = AbstractIdentifiable.checkEmpty(key, MessageString.KEY);
    }

    @Override
    public BigDecimal getLanguageCode() {
        return this.languageCode;
    }

    @Override
    public void setLanguageCode(BigDecimal languageCode) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(languageCode, MessageString.LANG);
        this.languageCode = languageCode;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(String message) {
        this.message = AbstractIdentifiable.checkEmpty(message, MessageString.MSG);
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + MessageString.KEY + '[' + this.getKey() + "], "
                + MessageString.LANG + '[' + this.getLanguageCode() + "], "
                + MessageString.MSG + '[' + this.getMessage() + ']';
    }

}
