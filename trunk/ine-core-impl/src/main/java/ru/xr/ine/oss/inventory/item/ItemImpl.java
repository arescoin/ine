/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.AbstractVersionable;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemImpl.java 41 2017-04-10 08:25:52Z xerror $"
 */
public class ItemImpl extends AbstractVersionable implements Item {

    private BigDecimal type;
    private BigDecimal parentId;
    private BigDecimal itemProfile;
    private BigDecimal status;
    private String name;


    @Override
    public BigDecimal getType() {
        return this.type;
    }

    @Override
    public void setType(BigDecimal type) {
        this.type = type;
    }

    @Override
    public BigDecimal getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(BigDecimal parentId) {
        this.parentId = parentId;
    }

    @Override
    public BigDecimal getItemProfile() {
        return this.itemProfile;
    }

    @Override
    public void setItemProfile(BigDecimal itemProfile) {
        this.itemProfile = itemProfile;
    }

    @Override
    public BigDecimal getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(BigDecimal status) {
        this.status = status;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + Item.SELF_NAME + "[" + this.getName() + "], "
                + Item.ITEM_TYPE + "[" + this.getType() + "], "
                + Item.PARENT_ID + "[" + this.getParentId() + "], "
                + Item.ITEM_PROFILE + "[" + this.getItemProfile() + "], "
                + Item.STATUS + "[" + this.getStatus() + "]";
    }
}
