/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class LangDetailsImpl extends AbstractIdentifiable implements LangDetails {

    /** Буквенный код языка в соответствие с iso639 */
    private String code;

    /** Название языка в соответствие с iso639 */
    private String name;


    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public void setCode(String code) throws IneIllegalArgumentException {
        code = AbstractIdentifiable.checkEmpty(code, LangDetails.CODE);

        if (code.length() != 2) {
            throw new IneIllegalArgumentException("Length of code must be only 2 symbols.", LangDetails.CODE);
        }

        this.code = code;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, LangDetails.NAME);
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + LangDetails.CODE + "[" + this.getCode() + ']' + ", "
                + LangDetails.NAME + "[" + this.getName() + ']';
    }

}
