/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataLongImpl.java 45 2017-04-11 16:52:40Z xerror $"
 */
public class PropertyDataLongImpl extends PropertyDataImpl<Long> implements PropertyDataLong {

    @Override
    public void setData(Long data) {
        this.data = data;
    }
}
