/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IllegalIdException;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RolePermitImpl extends AbstractVersionable implements RolePermit {

    /** Идентификатор доступа включенного в данную роль */
    private BigDecimal permitId;

    /** Значение доступа включенного в данную роль */
    private BigDecimal[] permitValues;

    private int crudMask;


    @Override
    public BigDecimal getPermitId() {
        return this.permitId;
    }

    @Override
    public void setPermitId(BigDecimal permitId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(permitId, RolePermit.PERMIT_ID);
        this.permitId = permitId;
    }

    @Override
    public BigDecimal[] getPermitValues() {
        return this.permitValues;
    }

    @Override
    public void setPermitValues(BigDecimal[] permitValues) {
        this.permitValues = permitValues;
    }

    @Override
    public int getCrudMask() {
        return this.crudMask;
    }

    @Override
    public void setCrudMask(int crudMask) {

        int[] ints = CrudCode.getByMask(crudMask);
        CrudCode.toCodes(ints);

        this.crudMask = crudMask;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof RolePermitImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        RolePermitImpl that = (RolePermitImpl) o;

        //noinspection RedundantIfStatement
        if (permitId != null ? !permitId.equals(that.permitId) : that.permitId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int buildHashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.getPermitId() != null ? this.getPermitId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + RolePermit.PERMIT_ID + '[' + this.getPermitId() + "], "
                + RolePermit.PERMIT_VALUES + '[' + Arrays.toString(this.getPermitValues()) + ']';
    }

}
