/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.structure.DataType;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDescriptorImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PropertyDescriptorImpl extends AbstractVersionable implements PropertyDescriptor {

    private String name;
    private DataType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataType getType() {
        return type;
    }

    public void setType(DataType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + PropertyDescriptor.NAME + "[" + this.getName() + "], "
                + PropertyDescriptor.TYPE + "[" + this.getType() + "]";
    }
}
