/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CustomAttributeValueImpl extends AbstractVersionable implements CustomAttributeValue {

    /** Идентификатор экземпляра {@link ru.xr.ine.core.Versionable объекта} к которому привязан данный атрибут. */
    private BigDecimal entityId;

    /** Значение атрибута */
    private Object value;


    @Override
    public BigDecimal getEntityId() {
        return entityId;
    }

    @Override
    public void setEntityId(BigDecimal entityId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(entityId, CustomAttributeValue.ENT_N);

        this.entityId = entityId;
        this.setHashCode();
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomAttributeValueImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        CustomAttributeValueImpl that = (CustomAttributeValueImpl) o;

        //noinspection RedundantIfStatement
        if (entityId != null ? !entityId.equals(that.entityId) : that.entityId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.getEntityId() != null ? this.getEntityId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + CustomAttributeValue.ENT_N + '[' + this.getEntityId() + "], "
                + CustomAttributeValue.VALUE + '[' + this.getValue() + ']';
    }

}
