/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.links;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class LinkDataImpl extends AbstractVersionable implements LinkData {

    /** Идентификатор описателя линка {@link ru.xr.ine.core.links.Link} */
    private BigDecimal linkId;

    /** Идентификатор левого участника ассоциации */
    private BigDecimal leftId;

    /** Bдентификатор правого участника ассоциации */
    private BigDecimal rightId;


    @Override
    public BigDecimal getLinkId() {
        return this.linkId;
    }

    @Override
    public void setLinkId(BigDecimal linkId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(linkId, LinkData.LINK_ID);
        this.linkId = linkId;
    }

    @Override
    public BigDecimal getLeftId() {
        return this.leftId;
    }

    @Override
    public void setLeftId(BigDecimal leftId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(leftId, LinkData.LEFT_ID);
        this.leftId = leftId;
    }

    @Override
    public BigDecimal getRightId() {
        return this.rightId;
    }

    @Override
    public void setRightId(BigDecimal rightId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(rightId, LinkData.RIGHT_ID);
        this.rightId = rightId;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() +
                ", " + LinkData.LINK_ID + "[" + this.getLinkId() + ']' + ", "
                + LinkData.LEFT_ID + "[" + this.getLeftId() + ']' + ", "
                + LinkData.RIGHT_ID + "[" + this.getRightId() + ']';
    }

}
