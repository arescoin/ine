/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.funcswitch;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FuncSwitchNameImpl extends AbstractIdentifiable implements FuncSwitchName {

    /** Наименование переключателя */
    private String name;


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, FuncSwitchName.NAME);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + FuncSwitchName.NAME + "['" + name + "'] ";
    }

}
