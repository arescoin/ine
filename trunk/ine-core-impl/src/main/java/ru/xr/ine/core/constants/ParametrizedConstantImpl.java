/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ParametrizedConstantImpl extends ConstantImpl implements ParametrizedConstant {

    /** Маска параметризации */
    private SystemObjectPattern mask;

    @Override
    public BigDecimal getType() {
        return ParametrizedConstant.TYPE;
    }

    @Override
    public SystemObjectPattern getMask() {
        return this.mask;
    }

    @Override
    public void setMask(SystemObjectPattern mask) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(mask, ParametrizedConstant.MASK);
        this.mask = mask;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + ParametrizedConstant.MASK + "[" + this.getMask() + "]";
    }
}
