/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CustomAttributeDefValImpl extends AbstractVersionable implements CustomAttributeDefVal {

    /** строковое значение */
    private String aString;

    /** целое числовое значение */
    private Long aLong;

    /** дробное числовое значение */
    private BigDecimal aBigDecimal;

    /** булевое значение */
    private Boolean aBoolean;

    /** значение дата */
    private Date aDate;


    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String getString() {
        return this.aString;
    }

    @Override
    public void setString(String aString) {
        this.aString = aString;
        if (aString != null) {
            this.nullifyFields(CustomAttributeDefVal.STRING);
        }
    }

    @Override
    public Long getLong() {
        return this.aLong;
    }

    @Override
    public void setLong(Long aLong) throws IneIllegalArgumentException {
        this.aLong = aLong;
        if (aLong != null) {
            this.nullifyFields(CustomAttributeDefVal.LONG);
        }
    }

    @Override
    public BigDecimal getBigDecimal() {
        return this.aBigDecimal;
    }

    @Override
    public void setBigDecimal(BigDecimal aBigDecimal) {
        this.aBigDecimal = aBigDecimal;
        if (aBigDecimal != null) {
            this.nullifyFields(CustomAttributeDefVal.BIGDECIMAL);
        }
    }

    @Override
    public Boolean getBoolean() {
        return this.aBoolean;
    }

    @Override
    public void setBoolean(Boolean aBoolean) {
        this.aBoolean = aBoolean;
        if (aBoolean != null) {
            this.nullifyFields(CustomAttributeDefVal.BOOLEAN);
        }
    }

    @Override
    public Date getDate() {
        return this.aDate;
    }

    @Override
    public void setDate(Date aDate) {
        this.aDate = aDate;
        if (aDate != null) {
            this.nullifyFields(CustomAttributeDefVal.DATE);
        }
    }

    private void nullifyFields(String exceptOne) {
        if (!exceptOne.equals(CustomAttributeDefVal.STRING)) {
            this.aString = null;
        }
        if (!exceptOne.equals(CustomAttributeDefVal.LONG)) {
            this.aLong = null;
        }
        if (!exceptOne.equals(CustomAttributeDefVal.BIGDECIMAL)) {
            this.aBigDecimal = null;
        }
        if (!exceptOne.equals(CustomAttributeDefVal.BOOLEAN)) {
            this.aBoolean = null;
        }
        if (!exceptOne.equals(CustomAttributeDefVal.DATE)) {
            this.aDate = null;
        }
    }

    @Override
    public Object getValueByType(DataType type) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(type, SystemObject.TYPE);

        switch (type) {
        case stringType:
            return aString;
        case longType:
            return aLong;
        case bigDecimalType:
            return aBigDecimal;
        case booleanType:
            return aBoolean;
        case dateType:
            return aDate;
        default:
            throw new IneIllegalArgumentException("Unsupported DataType: " + type);
        }
    }

    @Override
    public void setValueByType(Object value, DataType type) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkNull(value, CustomAttributeValue.VALUE);
        AbstractIdentifiable.checkNull(type, SystemObject.TYPE);

        switch (type) {
        case stringType:
            this.setString((String) value);
            break;
        case longType:
            this.setLong((Long) value);
            break;
        case bigDecimalType:
            this.setBigDecimal((BigDecimal) value);
            break;
        case booleanType:
            this.setBoolean((Boolean) value);
            break;
        case dateType:
            this.setDate((Date) value);
            break;
        default:
            throw new IneIllegalArgumentException("Unsupported DataType: " + type);
        }
    }

}
