/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import java.util.Date;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataDateImpl.java 48 2017-04-28 09:45:10Z xerror $"
 */
public class PropertyDataDateImpl extends PropertyDataImpl<Date> implements PropertyDataDate {

    @Override
    public void setData(Date data) {
        this.data = data;
    }
}
