/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.ipaddress;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IPAddressValueImpl extends AbstractVersionable implements IPAddressValue {

    IPAddress ipAddress;


    @Override
    public IPAddress getIpAddress() {
        return ipAddress;
    }

    @Override
    public void setIpAddress(IPAddress ipAddress) {
        AbstractIdentifiable.checkNull(ipAddress, IPAddressValue.IP_ADDRESS);
        this.ipAddress = ipAddress;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + IPAddressValue.IP_ADDRESS + "[" + ipAddress + "] ";
    }

}
