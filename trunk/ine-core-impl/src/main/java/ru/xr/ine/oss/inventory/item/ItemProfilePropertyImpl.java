/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfilePropertyImpl.java 59 2017-05-04 16:58:36Z xerror $"
 */
public class ItemProfilePropertyImpl extends AbstractVersionable implements ItemProfileProperty {


    private BigDecimal itemProfileId;
    private BigDecimal propertyDescriptorId;
    private String defaultValue;
    private SystemObjectPattern pattern;
    private boolean required;
    private boolean editable;

    public BigDecimal getItemProfileId() {
        return itemProfileId;
    }

    public void setItemProfileId(BigDecimal itemProfileId) {
        this.itemProfileId = itemProfileId;
    }

    public BigDecimal getPropertyDescriptorId() {
        return propertyDescriptorId;
    }

    public void setPropertyDescriptorId(BigDecimal propertyDescriptorId) {
        this.propertyDescriptorId = propertyDescriptorId;
    }

    @Override
    public String getDefaultValue() {
        return this.defaultValue;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public SystemObjectPattern getPattern() {
        return pattern;
    }

    public void setPattern(SystemObjectPattern pattern) {
        this.pattern = pattern;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public boolean isEditable() {
        return this.editable;
    }

    @Override
    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + ItemProfileProperty.ITEM_PROFILE_ID + "[" + this.getItemProfileId() + "], "
                + ItemProfileProperty.PROPERTY_DESCRIPTOR_ID + "[" + this.getPropertyDescriptorId() + "], "
                + ItemProfileProperty.DEFAULT_VALUE + "[" + this.getDefaultValue() + "], "
                + ItemProfileProperty.PATTERN + "[" + this.getPattern() + "], "
                + ItemProfileProperty.REQUIRED + "[" + this.isRequired() + "], "
                + ItemProfileProperty.EDITABLE + "[" + this.isEditable() + "]";
    }
}
