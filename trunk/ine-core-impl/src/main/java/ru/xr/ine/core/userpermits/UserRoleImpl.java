/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserRoleImpl extends AbstractVersionable implements UserRole {

    /** Идентификатор системного пользователя, обладателя роли */
    private BigDecimal userId;

    /** Признак активности роли */
    private boolean active;


    @Override
    public BigDecimal getUserId() {
        return this.userId;
    }

    @Override
    public void setUserId(BigDecimal userId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(userId, UserRole.USER_ID);
        this.userId = userId;
    }

    @Override
    public boolean isActive() {
        return this.active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int buildHashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.getUserId() != null ? this.getUserId().hashCode() : 0);

        return result;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof UserRoleImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        UserRoleImpl userRole = (UserRoleImpl) o;

        //noinspection RedundantIfStatement
        if (this.getUserId() != null
                ? !this.getUserId().equals(userRole.getUserId())
                : userRole.getUserId() != null) {

            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + UserRole.USER_ID + '[' + this.getUserId() + "], "
                + UserRole.ACTIVE + '[' + this.isActive() + ']';
    }

}
