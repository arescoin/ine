/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.links;

import ru.xr.ine.core.AbstractIdentifiable;
import ru.xr.ine.core.AbstractVersionable;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class LinkImpl extends AbstractVersionable implements Link {

    /** Паттерн, по которому будет идентифицироваться объект левого плеча связи */
    private SystemObjectPattern leftType;

    /** Паттерн, по которому будет идентифицироваться объект правого плеча связи */
    private SystemObjectPattern rightType;

    /** Код словарной статьи указывающей тип ассоциации (ее кратность) */
    private BigDecimal typeId;


    @Override
    public SystemObjectPattern getLeftType() {
        return this.leftType;
    }

    @Override
    public void setLeftType(SystemObjectPattern leftType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(leftType, Link.LEFT_TYPE);
        this.leftType = leftType;
    }

    @Override
    public SystemObjectPattern getRightType() {
        return this.rightType;
    }

    @Override
    public void setRightType(SystemObjectPattern rightType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(rightType, Link.RIGHT_TYPE);
        this.rightType = rightType;
    }

    @Override
    public BigDecimal getTypeId() {
        return this.typeId;
    }

    @Override
    public void setTypeId(BigDecimal typeId) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(typeId, Link.TYPE);
        this.typeId = typeId;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + Link.LEFT_TYPE + '[' + this.getLeftType() + ']' + ", "
                + Link.RIGHT_TYPE + '[' + this.getRightType() + ']' + ", "
                + Link.TYPE + '[' + this.getTypeId() + ']';
    }

}
