/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractIdentifiable.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class AbstractIdentifiable implements Identifiable {

    /** Идентификатор объекта */
    private BigDecimal coreId;

    /** Описание объекта, в соответствии с требованием */
    private String coreDsc;

    /** Постоянное хранилище хэшкода, для снижения времени вычисления в хранилищах */
    private int hCode = 1;


    /**
     * Метод проверяет значение переданного идентификатора на соответствие общесистемным требованиям
     *
     * @param id        проверяемый идентификатор
     * @param fieldName имя проверяемого поля, используется при сообщении об ошибке
     *
     * @throws IllegalIdException если проверяемое поле null или меньше
     *                            {@link #MIN_ALLOWABLE_VAL минимально допустимого значения}
     */
    public static void checkSystemId(BigDecimal id, String fieldName) throws IllegalIdException {
        if (id == null) {
            throw new IllegalIdException(fieldName + " can't be null.");
        }

        if (MIN_ALLOWABLE_VAL.compareTo(id) > 0) {
            throw new IllegalIdException(fieldName + " can't be less than 1, but has [" + id.toString() + "]");
        }
    }

    /**
     * Метод проверяет значение переданного идентификатора на соответствие общесистемным требованиям.<br> Имя
     * идентификатора - <b>{@link Identifiable#CORE_ID}</b>
     *
     * @param id проверяемый идентификатор
     */
    public static void checkSystemId(BigDecimal id) {
        AbstractIdentifiable.checkSystemId(id, Identifiable.CORE_ID);
    }

    /**
     * Метод проверяет значение переданного объекта на null-значение
     *
     * @param obj       проверяемый объект
     * @param fieldName имя проверяемого поля, используется при сообщении об ошибке
     *
     * @throws IneIllegalArgumentException если передаваемое значение null
     */
    public static void checkNull(Object obj, String fieldName) throws IneIllegalArgumentException {

        if (obj != null) {
            return;
        }

        throw new IneIllegalArgumentException(fieldName + " can't be null.", fieldName);
    }

    /**
     * Метод проверяет значение переданного строки на непустое значение и удаляет пробелы в начале строки и в конце
     *
     * @param string    проверяемая строка
     * @param fieldName имя проверяемого поля, используется при сообщении об ошибке
     *
     * @return строка, очищенная от начальных и конечных пробелов
     * @throws IneIllegalArgumentException если передаваемое значение null или пусто
     */
    public static String checkEmpty(String string, String fieldName) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkNull(string, fieldName);
        string = string.trim();

        if (string.isEmpty()) {
            throw new IneIllegalArgumentException(fieldName + " can't be empty.", fieldName);
        }

        return string;
    }

    /**
     * Формирует коллекцию идентификаторов из коллекции {@link Identifiable}
     *
     * @param source коллекция {@link Identifiable}
     *
     * @return коллекция идентификаторов
     */
    public static Collection<BigDecimal> extractIds(Collection<? extends Identifiable> source) {

        List<BigDecimal> result = Collections.emptyList();

        if (source != null && !source.isEmpty()) {
            result = new ArrayList<BigDecimal>(source.size());
            for (Identifiable identifiable : source) {
                if (identifiable != null) {
                    result.add(identifiable.getCoreId());
                }
            }
        }

        return result;
    }

    @Override
    public void setCoreId(BigDecimal id) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(id);
        this.coreId = id;
        this.setHashCode();
    }

    protected void setHashCode() {
        this.hCode = this.buildHashCode();
    }

    protected int buildHashCode() {

        int result = 1;

        if (this.getCoreId() != null) {
            result = this.getCoreId().hashCode();
        }

        return result;
    }

    @Override
    public BigDecimal getCoreId() {
        return this.coreId;
    }

    @Override
    public String getCoreDsc() {
        return this.coreDsc;
    }

    @Override
    public void setCoreDsc(String dsc) {
        this.coreDsc = dsc;
    }

    /** Высчитывает по идентификатору */
    @Override
    public int hashCode() {
        return this.hCode;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /** Два объекта считаются идентичными если их идентификаторы совпадают */
    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj == this) {
            result = true;
        } else if ((obj != null) && this.hashCode() == obj.hashCode() &&
                (this.getCoreId() != null) && (obj instanceof Identifiable)) {
            Identifiable identifiable = (Identifiable) obj;
            result = this.getCoreId().equals(identifiable.getCoreId());
        }

        return result;
    }

    /** Выводит класс объекта и его идентификатор */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " + Identifiable.CORE_ID + "[" + this.getCoreId() + "]";
    }

    protected String fieldToString(String fieldName, Object fieldValue) {
        return ", " + fieldName + "[" + fieldValue + "]";
    }
}
