/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure.test;

import ru.xr.ine.core.AbstractVersionable;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: TestObjectImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class TestObjectImpl extends AbstractVersionable implements TestObject {
    /** Тестовое поле */
    private String value;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + TestObject.VALUE + '[' + this.getValue() + ']';
    }

}
