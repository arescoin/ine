/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.funcswitch;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.funcswitch.FuncSwitch;
import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.dbe.da.core.AccessFactory;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchUtils.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class FuncSwitchUtils {

    // Названия системных переключателей

    /** Признак сохранения истории модификации объектов */
    public static final String SAVE_HISTORY = "SAVE_HISTORY";

    /** Признак регистрации события модификации объекта */
    public static final String REGISTER_ACTIONS = "REGISTER_ACTIONS";

    /** Признак сохранения дополнительных атрибутов выставляемых на событии */
    public static final String SAVE_ACTION_ATTRIBUTES = "SAVE_ACTION_ATTRIBUTES";

    // локальные переменные
    private static FuncSwitchUtils instance;

    private final Map<String, FuncSwitch> switchesByName = new HashMap<String, FuncSwitch>(100);
    private final Map<BigDecimal, String> namesById = new HashMap<BigDecimal, String>(100);

    @SuppressWarnings({"unchecked"})
    private FuncSwitchUtils() throws CoreException {

        FuncSwitchAccess<FuncSwitch> switchAccess =
                (FuncSwitchAccess<FuncSwitch>) AccessFactory.getImplementation(FuncSwitch.class);

        FuncSwitchNameAccess<FuncSwitchName> switchNameAccess =
                (FuncSwitchNameAccess<FuncSwitchName>) AccessFactory.getImplementation(FuncSwitchName.class);

        Collection<FuncSwitchName> funcSwitchNames = switchNameAccess.getAllObjects();
        Collection<FuncSwitch> switches = switchAccess.getAllObjects();

        for (FuncSwitchName funcSwitchName : funcSwitchNames) {
            namesById.put(funcSwitchName.getCoreId(), funcSwitchName.getName());
        }

        for (FuncSwitch aSwitch : switches) {
            switchesByName.put(namesById.get(aSwitch.getCoreId()), aSwitch);
        }

    }

    private static FuncSwitchUtils getInstance() throws GenericSystemException {

        if (instance == null) {
            synchronized (SAVE_HISTORY) {
                if (instance == null) {
                    try {
                        instance = new FuncSwitchUtils();
                    } catch (CoreException e) {
                        throw GenericSystemException.toGeneric(e);
                    }
                }
            }
        }

        return instance;
    }

    /**
     * Возвращает переключатель по переданному имени
     *
     * @param switchName уникальное имя переключателя
     * @return переключатель
     * @throws ru.xr.ine.core.GenericSystemException при попытке передать <b>null</b>,
     * пустую строку, или несуществующее имя
     */
    public static FuncSwitch getFuncSwitch(String switchName) throws GenericSystemException {

        if (switchName == null || switchName.isEmpty()) {
            throw new GenericSystemException("Illegal FuncSwitch name [" + switchName + "]");
        }

        FuncSwitch funcSwitch = getInstance().switchesByName.get(switchName);
        if (funcSwitch == null) {
            throw new GenericSystemException("FuncSwitchs with name '" + switchName + "' not found");
        }

        return getCopyOf(funcSwitch);
    }

    /**
     * Возвращает переключатель по переданному имени
     *
     * @param id идентификатор переключателя
     * @return переключатель
     * @throws ru.xr.ine.core.GenericSystemException при попытке передать <b>null</b>,
     * пустую строку, или несуществующий идентификатор
     */
    public static FuncSwitch getFuncSwitch(BigDecimal id) throws GenericSystemException {

        if (id == null) {
            throw GenericSystemException.toGeneric(new NullPointerException("Illegal ID exception"));
        }

        String funcSwName = getInstance().namesById.get(id);
        if (funcSwName == null) {
            throw new GenericSystemException("FuncSwitchs with coreId '" + id + "' not found");
        }

        return getFuncSwitch(funcSwName);
    }

    private static FuncSwitch getCopyOf(FuncSwitch funcSwitch) throws GenericSystemException {
        FuncSwitch copy = IdentifiableFactory.getImplementation(FuncSwitch.class);
        copy.setCoreId(funcSwitch.getCoreId());
        copy.setCoreFd(funcSwitch.getCoreFd());
        copy.setCoreTd(funcSwitch.getCoreTd());
        copy.setCoreDsc(funcSwitch.getCoreDsc());
        copy.setEnabled(funcSwitch.isEnabled());
        copy.setChangeDate(funcSwitch.getChangeDate());
        copy.setStateCode(funcSwitch.getStateCode());
        copy.setMark(funcSwitch.getMark());
        return copy;
    }

    static synchronized void saveSearchedSwitch(String switchName, FuncSwitch funcSwitch)
            throws GenericSystemException {
        getInstance().switchesByName.put(switchName, funcSwitch);
        getInstance().namesById.put(funcSwitch.getCoreId(), switchName);
    }

}
