/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.entity;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.dbe.da.core.VersionableAccess;
import ru.xr.ine.oss.entity.Company;

import java.util.Collection;

/**
 * Интерфейс для доступа к данным сущности "Организация".
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CompanyAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CompanyAccess<T extends Company> extends VersionableAccess<T> {

    /**
     * Метод возвращает все актуальные на текущий момент организации
     *
     * @return коллекция организаций
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
