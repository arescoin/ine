/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.userpermits.RolePermit;
import ru.xr.ine.dbe.da.core.CRUDHelper;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Описывает интерфейс доступа к доступам, содержащимся в системных ролях
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface RolePermitAccess<T extends RolePermit> extends VersionableAccess<T> {

    /**
     * Метод возвращает список доступов, содержащихся в системных ролях
     *
     * @return коллекция системных пользователей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    void addHelper(CRUDHelper<T> helper);

    public Map<BigDecimal, T> getPremitsByRole(BigDecimal roleId) throws CoreException;
}
