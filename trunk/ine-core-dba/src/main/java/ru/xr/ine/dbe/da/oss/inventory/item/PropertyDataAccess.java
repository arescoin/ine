/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.dbe.da.core.VersionableAccess;
import ru.xr.ine.oss.inventory.item.PropertyData;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface PropertyDataAccess<T extends PropertyData> extends VersionableAccess<T> {

    /**
     * Метод возвращает все актуальные на текущий момент значения свойств
     *
     * @return коллекция значений свойств
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
