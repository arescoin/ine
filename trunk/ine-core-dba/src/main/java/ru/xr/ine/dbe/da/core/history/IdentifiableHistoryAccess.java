/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к данным по истории изменений идентифицируемого объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IdentifiableHistoryAccess<T extends IdentifiableHistory> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все зарегистрированные действия в системе
     *
     * @return коллекция действий над объектами
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает все записи об изменениях в идентифицируемых объектах
     *
     * @param sysObjId идентификатор системного объекта
     * @param entityId идентификатор сущности
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некоррекного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректых значениях в датах системного объекта
     */
    Collection<T> getActionsHistoryByIds(BigDecimal sysObjId, SyntheticId entityId) throws CoreException;
}
