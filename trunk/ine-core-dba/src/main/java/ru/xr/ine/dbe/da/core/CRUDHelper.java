/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;

/**
 * Интерфейс для классов, проверяющих данные при CRUD-операциях.
 * 
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CRUDHelper.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CRUDHelper<T extends Identifiable> {

    /**
     * Метод выполняет действия необходимые перед созданием объекта
     *
     * @param identifiable создаваемый объект
     * @throws ru.xr.ine.core.GenericSystemException при возникновении ошибок
     */
    void beforeCreate(T identifiable, IdentifiableAccess<T> access) throws GenericSystemException;

    /**
     * Метод выполняет действия необходимые для дополнения объекта данными после его создания
     *
     * @param identifiable созданный и дополненный данными объект
     * @throws ru.xr.ine.core.GenericSystemException при возникновении ошибок
     */
    void afterCreate(T identifiable) throws GenericSystemException;

    /**
     * Метод выполняет действия необходимые перед обновлением объекта
     *
     * @param identifiable    обновляемый объект
     * @param newIdentifiable обновленный объект
     * @throws ru.xr.ine.core.GenericSystemException при возникновении ошибок
     */
    void beforeUpdate(T identifiable, T newIdentifiable) throws GenericSystemException;

    /**
     * Метод выполняет действия необходимые после обновления объекта
     *
     * @param identifiable    обновляемый объект
     * @param newIdentifiable обновленный объект
     * @throws ru.xr.ine.core.GenericSystemException при возникновении ошибок
     */
    void afterUpdate(T identifiable, T newIdentifiable) throws GenericSystemException;

    /**
     * Метод выполняет действия необходимые перед удалением объекта
     *
     * @param identifiable удаляемый объект
     * @throws ru.xr.ine.core.GenericSystemException при возникновении ошибок
     */
    void beforeDelete(T identifiable) throws GenericSystemException;

    /**
     * Метод выполняет действие необходимые после удаления объекта
     *
     * @param identifiable удаляемый объект
     * @throws ru.xr.ine.core.GenericSystemException при возникновении ошибок
     */
    void afterDelete(T identifiable) throws GenericSystemException;
}
