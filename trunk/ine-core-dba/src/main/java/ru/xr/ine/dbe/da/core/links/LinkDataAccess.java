/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.links;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.links.LinkData;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к связям.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface LinkDataAccess<T extends LinkData> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент связи
     *
     * @return коллекция связей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Получает связные объекты для переданных {@link ru.xr.ine.core.Identifiable}-объекта и типа связи.
     *
     * @param objectToLink один из участников связи, для которого получаются привязанные объекты
     * @param linkId       идентификатор описателя связи {@link ru.xr.ine.core.links.Link}
     * @return коллекция идентификаторов объектов, привязанных к переданному
     *         {@link ru.xr.ine.core.Identifiable}-объекту
     * @throws ru.xr.ine.core.GenericSystemException в случае ошибки при работе с кешем
     */
    <E extends Identifiable> Collection<E> getLinkedObjects(Identifiable objectToLink, BigDecimal linkId)
            throws GenericSystemException;

    /**
     * Получает все связи в которых участвует переданный {@link ru.xr.ine.core.Identifiable}-объект.
     *
     * @param linkedObject один из участников связи
     * @return коллекция связей переданного {@link ru.xr.ine.core.Identifiable}-объекта
     * @throws ru.xr.ine.core.GenericSystemException в случае ошибки при работе с кешем
     */
    Collection<T> getLinkDataByObject(Identifiable linkedObject) throws GenericSystemException;

    /**
     * Получает все связи с указанным идентификатором описателя связи, в которых участвует переданный
     * {@link ru.xr.ine.core.Identifiable}-объект.
     *
     * @param linkedObject один из участников связи
     * @param linkId       идентификатор описателя связи
     * @return коллекция связей переданного {@link ru.xr.ine.core.Identifiable}-объекта
     * @throws ru.xr.ine.core.GenericSystemException в случае ошибки при работе с кешем
     */
    Collection<T> getLinkDataByObject(Identifiable linkedObject, BigDecimal linkId) throws GenericSystemException;

}
