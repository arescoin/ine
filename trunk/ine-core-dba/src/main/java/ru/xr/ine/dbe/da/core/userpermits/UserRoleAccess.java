/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.userpermits.UserRole;
import ru.xr.ine.dbe.da.core.CRUDHelper;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к ролям пользователей
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface UserRoleAccess<T extends UserRole> extends VersionableAccess<T> {

    /**
     * Метод возвращает список всех ролей пользователей
     *
     * @return коллекция системных пользователей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает список всех ролей пользователя
     *
     * @param userId идентификатор пользователя
     * @return коллекция системных пользователей
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    Collection<T> getRolesByUserId(BigDecimal userId) throws CoreException;

    void addHelper(CRUDHelper<T> helper);
}
