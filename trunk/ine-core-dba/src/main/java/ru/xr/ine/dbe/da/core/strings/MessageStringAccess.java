/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.strings;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.strings.MessageString;
import ru.xr.ine.dbe.da.core.ObjectAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Описывает интерфейс доступа к строкам локализации
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface MessageStringAccess<T extends MessageString> extends ObjectAccess {

    /**
     * Метод возвращает все строки локализации
     *
     * @return коллекция строк локализации
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Возвращает содержимое локализованного сообщения по указанным ключу и коду языка
     *
     * @param stringKey ключ сообщения
     * @param langCode  код языка
     * @return локализованное сообщение
     * @throws ru.xr.ine.core.CoreException при возникновении ошибок в процессе получения данных
     */
    T getStringByKeyAndLang(String stringKey, BigDecimal langCode) throws CoreException;

    /**
     * Возвращает карту локализованных сообщений по указанным ключу в присутствующих языках
     *
     * @param stringKey ключ сообщения
     * @return карта локализованных сообщений
     * @throws ru.xr.ine.core.CoreException при возникновении ошибок в процессе получения данных
     */
    Map<BigDecimal, T> getStringsByKey(String stringKey) throws CoreException;

    /**
     * Создает в системе новый объект
     *
     * @param identifiable новый объект
     * @return созданный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Изменяет свойства существующего в системе объекта
     *
     * @param identifiable    старый объект
     * @param newIdentifiable новый объект
     * @return модифицированный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Удаляет существующий объект
     *
     * @param identifiable удаляемый объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException;
}
