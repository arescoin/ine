/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.core.structure.SystemObjectType;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Утилитный класс для работы с системными паттернами.
 * <p/>
 * Можно проверить валидность паттерна, получить {@link ru.xr.ine.core.Identifiable}-объект и его составной
 * идентификатор. И обратная операция - по объекту получить его паттерн.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PatternUtils.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class PatternUtils {

    public static final PatternToBigDecimalComparator PATTERN_TO_ID_COMPARATOR = new PatternToBigDecimalComparator();

    private static Logger logger = Logger.getLogger(PatternUtils.class.getName());

    private static SystemObjectAccess<SystemObject> access;

    private static Map<BigDecimal, IdentifiableAccess> ACCESSES_MAP = new HashMap<BigDecimal, IdentifiableAccess>(25);

    private PatternUtils() {
    }

    @SuppressWarnings({"unchecked"})
    public static SystemObjectAccess<SystemObject> getAccess() {
        if (access == null) {
            try {
                access = (SystemObjectAccess<SystemObject>) AccessFactory.getImplementation(SystemObject.class);
            } catch (CoreException e) {
                logger.log(Level.WARNING, "Can't create FieldAccess", e);
            }
        }

        return access;
    }

    /**
     * Проверяет корретность паттерна.
     * В проверку входит следующее:
     * <ul>
     * <li>Все идентификаторы системных объектов в паттерне должны определять столбцы таблицы
     * (как {@link SystemObjectPattern#getFinalId()}, так и {@link SystemObjectPattern.Element#getSysObjId()})</li>
     * <li>Все столбцы должны принадлежать одной таблице</li>
     * </ul>
     * После проверки паттерна, выставляется {@link SystemObjectPattern#getTableId() идентификатор таблицы}.
     *
     * @param pattern проверяемый паттерн
     *
     * @return проверенный паттерн
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          в случае неудовлетворительных результатов проверки паттерна
     * @throws ru.xr.ine.core.CoreException
     *          в случае ошибок при работе с базой и мета-информацией
     */
    public static SystemObjectPattern checkPattern(SystemObjectPattern pattern)
            throws IneIllegalArgumentException, CoreException {

        if (pattern == null) {
            throw new IneIllegalArgumentException("pattern param is null");
        }
        Map<BigDecimal, SystemObject> objects = new HashMap<BigDecimal, SystemObject>();
        objects.put(pattern.getFinalId(), getAccess().getObjectById(pattern.getFinalId()));
        if (pattern.hasElements()) {
            for (SystemObjectPattern.Element element : pattern.getElements()) {
                objects.put(element.getSysObjId(), getAccess().getObjectById(element.getSysObjId()));
            }
        }

        return checkPattern(objects, pattern);
    }

    static <T extends SystemObject> SystemObjectPattern checkPattern(
            Map<BigDecimal, T> systemObjects, SystemObjectPattern pattern)
            throws IneIllegalArgumentException, CoreException {

        T so = systemObjects.get(pattern.getFinalId());
        checkIsColumn(so);
        BigDecimal tableId = so.getUp();
        if (pattern.hasElements()) {
            for (SystemObjectPattern.Element element : pattern.getElements()) {
                so = systemObjects.get(element.getSysObjId());
                checkIsColumn(so);
                checkSameTable(so.getUp(), tableId);
            }
        }
        pattern.setTableId(tableId);

        return pattern;
    }

    private static void checkIsColumn(SystemObject column) throws IneIllegalArgumentException {
        if (!column.getType().equals(SystemObjectType.column.getTypeCode())) {
            throw new IneIllegalArgumentException("Expected type of systemObject is not column. Actual is " +
                    column.getType() + " for object with id = " + column.getCoreId());
        }
    }

    private static void checkSameTable(BigDecimal current, BigDecimal expected) throws IneIllegalArgumentException {
        if (!current.equals(expected)) {
            throw new IneIllegalArgumentException(
                    "expected tableId [" + expected + "] is not equals to current one [" + current + "].");
        }
    }

    /**
     * Получает {@link ru.xr.ine.core.Identifiable объект} по паттерну и значению, которым параметризуется паттерн.
     *
     * @param pattern "простой" системный паттерн
     * @param value   значение-идентификатор
     *
     * @return объект, определяемый паттерном и значением
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          если паттерн "сложный"
     * @throws ru.xr.ine.core.CoreException
     *          в случае ошибок при работе с базой и мета-информацией
     */
    public static Identifiable getObjectByPattern(SystemObjectPattern pattern, BigDecimal value)
            throws IneIllegalArgumentException, CoreException {

        PatternUtils.checkPattern(pattern);
        IdentifiableAccess access = getIdentifiableAccess(pattern.getTableId());
        return access.getObjectById(getIdByPattern(access, pattern, value).getIdValues());
    }

    /**
     * Получает коллекцию {@link ru.xr.ine.core.Identifiable объектов} по паттерну и значению,
     * которым параметризуется паттерн.
     *
     * @param pattern системный паттерн
     * @param value   значение-идентификатор
     *
     * @return объект, определяемый паттерном и значением
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          в случае неудовлетворительных результатов проверки паттерна
     * @throws ru.xr.ine.core.CoreException
     *          в случае ошибок при работе с базой и мета-информацией
     */
    public static Collection<Identifiable> getObjectsByPattern(SystemObjectPattern pattern, BigDecimal value)
            throws IneIllegalArgumentException, CoreException {

        PatternUtils.checkPattern(pattern);

        IdentifiableAccess<Identifiable> identifiableAccess = getIdentifiableAccess(pattern.getTableId());

        Map<String, String> fieldsMap = getInvertedFieldsMap(identifiableAccess);
        SearchCriteriaProfile profile = identifiableAccess.getSearchCriteriaProfile();
        Set<SearchCriterion> searchCriteria = getSearchCriteria(pattern, profile, fieldsMap);

        String fieldName = fieldsMap.get(getAccess().getObjectById(pattern.getFinalId()).getName());
        searchCriteria.add(profile.getSearchCriterion(fieldName, CriteriaRule.equals, value));

        return identifiableAccess.searchObjects(searchCriteria);
    }

    /**
     * Получает {@link Collection коллекцию} {@link Identifiable объектов}, которые могут быть использованы
     * в качестве параметризующего значения, соответствующего переданному {@link SystemObjectPattern паттерну}.
     *
     * @param pattern параметризующий паттерн
     *
     * @return коллекция допустимых значений
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          в случае неудовлетворительных результатов проверки паттерна
     * @throws ru.xr.ine.core.CoreException
     *          в случае ошибок при работе с базой и мета-информацией
     */
    public static Collection<Identifiable> getAvailableObjects(SystemObjectPattern pattern)
            throws IneIllegalArgumentException, CoreException {

        PatternUtils.checkPattern(pattern);

        IdentifiableAccess<Identifiable> identifiableAccess = getIdentifiableAccess(pattern.getTableId());

        Map<String, String> fieldsMap = getInvertedFieldsMap(identifiableAccess);
        SearchCriteriaProfile profile = identifiableAccess.getSearchCriteriaProfile();

        return identifiableAccess.searchObjects(getSearchCriteria(pattern, profile, fieldsMap));
    }

    /**
     * Получает {@link Set набор} {@link SearchCriterion поисковых критериев} из элементов паттерна.
     *
     * @param pattern   системный паттерн
     * @param profile   поисковый профиль
     * @param fieldsMap список полей объекта
     *
     * @return набор поисковых критериев
     * @throws CoreException в случае ошибок при работе с кешем
     */
    public static Set<SearchCriterion> getSearchCriteria(SystemObjectPattern pattern,
                                                         SearchCriteriaProfile profile,
                                                         Map<String, String> fieldsMap) throws CoreException {

        PatternUtils.checkPattern(pattern);

        SystemObjectAccess<SystemObject> objectAccess = getAccess();
        Set<SearchCriterion> searchCriteria = new HashSet<SearchCriterion>();

        List<SystemObjectPattern.Element> list = pattern.getElements();

        if (!list.isEmpty()) {
            for (SystemObjectPattern.Element element : list) {
                /**
                 * По номеру {@link SystemObject} из поля {@link Element#getSysObjId()} получаем название столбца,
                 * а уже по нему получаем название поля, используемое в поиске.
                 */
                String fieldName = fieldsMap.get(
                        objectAccess.getObjectById(element.getSysObjId()).getName());
                /**
                 * Если элемент комплексный (есть перечисление и/или интервал объектов),
                 * то используется критерий {@link CriteriaRule.in}, иначе {@link CriteriaRule.equals}/
                 */
                CriteriaRule rule = element.isComplexElement() ? CriteriaRule.in : CriteriaRule.equals;
                searchCriteria.add(profile.getSearchCriterion(fieldName, rule, element.getEntityIds().toArray()));
            }
        }

        return searchCriteria;
    }

    /**
     * Получает инвертированную мапу полей объекта, для получения поля по столбцу таблицы
     *
     * @param access дата-аксесс для получения списка полей объекта
     *
     * @return мапа вида "столбец таблицы" -> "название поля"
     */
    public static Map<String, String> getInvertedFieldsMap(IdentifiableAccess access) {
        Map<String, String> fieldsMap = new HashMap<String, String>();
        for (Map.Entry<String, String> entry : access.getInstanceFields().entrySet()) {
            fieldsMap.put(entry.getValue(), entry.getKey());
        }

        return fieldsMap;
    }

    /**
     * Внутренний метод для получения составного идентификатора.
     * <p/>
     * Перед возвращением сконструированного идентификатора проводит его валидацию, используя метод
     * {@link ru.xr.ine.dbe.da.core.IdentifiableAccess#checkSyntheticId(ru.xr.ine.core.SyntheticId)}.
     * Так же проверяется условие того, что паттерн не должен быть сложным, т.е. в сочетании со значением должен
     * представлять ровно один объект. Все элементы простого паттерна должны содержать ровно по одному элементу
     * BigDecimal-типа.
     *
     * @param access  дата-аксесс объекта, который будет получаться по составному идентификатору
     * @param pattern "простой" системный паттерн
     * @param value   значение-идентификатор
     *
     * @return составной идентификатор объекта, определяемого паттерном и значением
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          если паттерн "сложный"
     * @throws ru.xr.ine.core.CoreException
     *          в случае ошибок при работе с базой и мета-информацией
     */
    private static SyntheticId getIdByPattern(IdentifiableAccess access, SystemObjectPattern pattern, BigDecimal value)
            throws IneIllegalArgumentException, CoreException {

        if (pattern.isComplexPattern()) {
            throw new IneIllegalArgumentException("Can't get single object by complex pattern: " + pattern.toString());
        }

        SyntheticId id = new SyntheticId();
        if (pattern.hasElements()) {
            for (SystemObjectPattern.Element element : pattern.getElements()) {
                id.addIdEntry(element.getSysObjId(), element.getEntityIds().iterator().next());
            }
        }
        id.addIdEntry(pattern.getFinalId(), value);

        return access.checkSyntheticId(id);
    }

    @SuppressWarnings({"unchecked"})
    private static IdentifiableAccess<Identifiable> getIdentifiableAccess(BigDecimal tableId)
            throws GenericSystemException {

        if (!ACCESSES_MAP.containsKey(tableId)) {
            String ifClassName = getAccess().getInterfaces().get(tableId);
            Class<? extends Identifiable> ifClass;
            try {
                ifClass = (Class<? extends Identifiable>) Class.forName(ifClassName);
            } catch (ClassNotFoundException e) {
                throw new GenericSystemException(e.getMessage(), e);
            }
            ACCESSES_MAP.put(tableId, (IdentifiableAccess) AccessFactory.getImplementation(ifClass));
        }

        return ACCESSES_MAP.get(tableId);
    }

    /**
     * Получает из переданного {@link ru.xr.ine.core.Identifiable InE-объекта} системный паттерн и значение,
     * которым он должен параметризироваться.
     * <br>
     * Результат возвращается в виде мапы из одной строчки, ключом в которой является паттерн, а значением - его
     * параметризующее значение (которое сохраняется отдельно).
     *
     * @param object InE-объект
     *
     * @return однострочная карта вида "SystemObjectPattern (key) -> BigDecimal (value)"
     * @throws ru.xr.ine.core.CoreException
     *          в случае ошибок при работе с базой и мета-информацией
     */
    @SuppressWarnings({"unchecked"})
    public static Map<SystemObjectPattern, BigDecimal> getPatternByObject(Identifiable object) throws CoreException {

        IdentifiableAccess access = (IdentifiableAccess) AccessFactory.getImplementation(object.getClass());

        return getPatternById(access.getSyntheticId(object));
    }

    /**
     * Получает из переданного {@link ru.xr.ine.core.SyntheticId составного идентификатора} InE-объекта системный
     * паттерн и значение, которым он должен параметризироваться.
     * <br>
     * Результат возвращается в виде мапы из одной строчки, ключом в которой является паттерн, а значением - его
     * параметризующее значение (которое сохраняется отдельно).
     *
     * @param id составной идентификатор InE-объекта
     *
     * @return однострочная карта вида "SystemObjectPattern (key) -> BigDecimal (value)"
     */
    public static Map<SystemObjectPattern, BigDecimal> getPatternById(SyntheticId id) {

        List<BigDecimal> keys = id.getIdKeys();
        SystemObjectPattern.Element[] elements = new SystemObjectPattern.Element[keys.size() - 1];
        BigDecimal finalId = null;
        BigDecimal value = null;

        for (int i = 0; i < keys.size(); i++) {
            finalId = keys.get(i);
            value = id.getIdValue(finalId);
            if (i < keys.size() - 1) {
                elements[i] = new SystemObjectPattern.Element(finalId, value.toString());
            }
        }

        SystemObjectPattern pattern = new SystemObjectPattern(finalId);
        for (SystemObjectPattern.Element element : elements) {
            pattern.addElement(element);
        }

        try {
            pattern = PatternUtils.checkPattern(pattern);
        } catch (CoreException e) {
            logger.log(Level.INFO, e.getMessage(), e);
        }

        Map<SystemObjectPattern, BigDecimal> result = new HashMap<SystemObjectPattern, BigDecimal>();
        result.put(pattern, value);

        return result;
    }


    /**
     * Класс-компаратор для поиска объектов, содержаших паттерн, описывающий заданный тип объектов.<br>
     * Сравнение производится по полю {@link SystemObjectPattern#tableId}.<br>
     * Первый объект в сравнение - {@link SystemObjectPattern pattern}, второй - {@link BigDecimal tableId}.
     */
    private static class PatternToBigDecimalComparator implements Comparator, Serializable {

        @Override
        public int compare(Object o1, Object o2) {
            SystemObjectPattern pattern = (SystemObjectPattern) o1;
            BigDecimal tableId = (BigDecimal) o2;
            int result;
            if (pattern == null) {
                result = tableId == null ? 0 : -1;
            } else {
                result = tableId == null ? 1 : pattern.getTableId().compareTo(tableId);
            }

            return result;
        }
    }

}
