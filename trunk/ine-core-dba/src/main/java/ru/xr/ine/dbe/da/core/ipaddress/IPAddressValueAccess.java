/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.ipaddress;

import ru.xr.ine.core.ipaddress.IPAddressValue;
import ru.xr.ine.dbe.da.core.VersionableAccess;

/**
 * Базовый интерфейс для доступа к данным {@link IPAddressValue}-объектов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IPAddressValueAccess<T extends IPAddressValue> extends VersionableAccess<T> {
}
