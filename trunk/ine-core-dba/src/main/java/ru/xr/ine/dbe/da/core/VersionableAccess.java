/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Versionable;

import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface VersionableAccess<T extends Versionable> extends IdentifiableAccess<T> {

    /**
     * Получает предыдущие версии переданного объекта, оторбражающие историю его изменения.
     *
     * @param versionable объект, для которого получается история
     *
     * @return предыдущие версии объекта
     * @throws GenericSystemException в случае ошибок при работе с БД
     */
    Collection<T> getOldVersions(T versionable) throws GenericSystemException;

    /**
     * Получает предыдущие версии переданного объекта, оторбражающие историю его изменения.
     *
     * @param versionable объект, для которого получается история
     *
     * @return предыдущие версии объекта
     * @throws GenericSystemException в случае ошибок при работе с БД
     */
    Collection<T> getOldVersions(T versionable, Date from, Date to) throws GenericSystemException;
}
