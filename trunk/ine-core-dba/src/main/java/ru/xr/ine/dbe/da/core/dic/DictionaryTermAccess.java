/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.dic;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.dic.DictionaryTerm;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к терминам словарных статей
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface DictionaryTermAccess<T extends DictionaryTerm> extends DictionaryEntryAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент термины словарных статей
     *
     * @return коллекция всех терминов словарных статей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает все действительные словарные термины указанного словаря на указанном языке
     *
     * @param dictionaryId идентификатор словаря
     * @param languageId   идентификатор  языка
     * @return коллекция словарных статей {@link ru.xr.ine.core.dic.DictionaryEntry}
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения данных
     */
    public Collection<T> getAllTermsForDictionaryAndLanguage(BigDecimal dictionaryId, BigDecimal languageId)
            throws GenericSystemException;

    /**
     * Метод возвращает все действительные словарные термины c указанным идентификтором словаря на всех языках
     *
     * @param dictionaryId идентификатор словаря
     * @param entryId      идентификатор словарной статьи
     * @return коллекция словарных статей {@link ru.xr.ine.core.dic.DictionaryEntry}
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения данных
     */
    public Collection<T> getAllTermsForDictionaryAndEntry(BigDecimal dictionaryId, BigDecimal entryId)
            throws GenericSystemException;

}
