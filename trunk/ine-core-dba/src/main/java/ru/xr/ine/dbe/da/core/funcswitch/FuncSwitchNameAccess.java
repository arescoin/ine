/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.funcswitch;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к названиям переключателей функциональности
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface FuncSwitchNameAccess<T extends FuncSwitchName> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все названия переключателей функциональности
     *
     * @return коллекция названий переключателей функциональности
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
