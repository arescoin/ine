/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da;

import ru.xr.ine.utils.config.ConfigurationManager;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс предоставляет статический метод для получения JDBC-подключения.
 * <p/>
 * Имеется возможность подключения с использованием двух типов пула соединений:
 * <ul>
 * <li>с использованием пула AS</li>
 * <li>с использованием стороннего пула</li>
 * </ul>
 * <p/>
 * Регулируется состоянием константы {@link #USE_AS_POOL},
 * через установку значения {@link #AS_POOL}в конфигурации системы
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PoolManager.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class PoolManager {

    /** Имя системного свойства устанавливающего источник подключения к БД */
    protected static final String AS_POOL = "AS_POOL";
    /** Имя системного свойства устанавливающего JNDI-имя пула */
    protected static final String AS_POOL_NAME_PROP = "AS_POOL_NAME";

    protected static final String NATIVE_CLASS = "NATIVE_CLASS";

    /** Имя системного свойства устанавливающего имя пользователя БД для подключения без AS */
    protected static final String MANUAL_CONNECTION_NAME_PROP = "MANUAL_CONNECTION_NAME";
    /** Имя системного свойства устанавливающего пароль пользователя БД для подключения без AS */
    protected static final String MANUAL_CONNECTION_PWD_PROP = "MANUAL_CONNECTION_PWD";
    /** Имя системного свойства устанавливающего хост БД для подключения без AS */
    protected static final String MANUAL_CONNECTION_HOST_PROP = "MANUAL_CONNECTION_HOST";
    /** Имя системного свойства устанавливающего порт БД для подключения без AS */
    protected static final String MANUAL_CONNECTION_PORT_PROP = "MANUAL_CONNECTION_PORT";
    /** Имя системного свойства устанавливающего SID БД для подключения без AS */
    protected static final String MANUAL_CONNECTION_SID_PROP = "MANUAL_CONNECTION_SID";

    /** Значение по умолчанию для пула на AS */
    protected static final String DEF_AS_POOL_NAME = "jdbc/ora_suris";

    /** Значение по умолчанию для имени пользователя, при подключении без AS */
    protected static final String DEF_MANUAL_CONNECTION_NAME = "SURIS";
    /** Значение по умолчанию для пароля, при подключении без AS */
    protected static final String DEF_MANUAL_CONNECTION_PWD = "SURIS";
    /** Значение по умолчанию для порта, при подключении без AS */
    protected static final String DEF_MANUAL_CONNECTION_PORT = "1521";
    /** Значение по умолчанию для SID, при подключении без AS */
    protected static final String DEF_MANUAL_CONNECTION_SID = "SURIS";

    /** Системное свойство устанавливающее источник подключения к БД */
    protected static final boolean USE_AS_POOL = Boolean.parseBoolean(ConfigurationManager.getManager().
            getRootConfig().getValueByKey(PoolManager.class.getName() + "." + AS_POOL, Boolean.TRUE.toString()));

    /** Системное свойство, устанавливающее имя пула БД на сервере */
    protected static final String AS_POOL_NAME = ConfigurationManager.getManager().getRootConfig()
            .getValueByKey(PoolManager.class.getName() + "." + AS_POOL_NAME_PROP, DEF_AS_POOL_NAME);


    private static DataSource dataSource;
    private static PoolDelegate POOL_DELEGATE;

    private static Logger logger = Logger.getLogger(PoolManager.class.getName());


    static {
        init();
    }


    private static void init() {

        logger.info("Init connection pool, using AS [" + USE_AS_POOL + "]");

        if (USE_AS_POOL) {
            try {
                logger.info("Connecting with AS pool");
                Context context = new InitialContext();
                logger.info("Looking up for " + AS_POOL_NAME);
                dataSource = (DataSource) context.lookup(AS_POOL_NAME);
                POOL_DELEGATE = new PoolDelegate() {
                    @Override
                    public Connection getConnection() throws SQLException {
                        return dataSource.getConnection();
                    }
                };
            } catch (NamingException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
                e.printStackTrace();
            }
        } else {

            logger.info("Connecting with local Pool Manager");
            String name = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    PoolManager.class.getName() + "." + MANUAL_CONNECTION_NAME_PROP, DEF_MANUAL_CONNECTION_NAME);

            String pwd = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    PoolManager.class.getName() + "." + MANUAL_CONNECTION_PWD_PROP, DEF_MANUAL_CONNECTION_PWD);

            String host = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    PoolManager.class.getName() + "." + MANUAL_CONNECTION_HOST_PROP);

            String port = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    PoolManager.class.getName() + "." + MANUAL_CONNECTION_PORT_PROP, DEF_MANUAL_CONNECTION_PORT);

            String sid = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    PoolManager.class.getName() + "." + MANUAL_CONNECTION_SID_PROP, DEF_MANUAL_CONNECTION_SID);


            String nativeClass = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    PoolManager.class.getName() + "." + NATIVE_CLASS);

            try {
                final DSDelegate dsDelegate = (DSDelegate) Class.forName(nativeClass).newInstance();
                dsDelegate.init(host, port, sid, name, pwd);

                POOL_DELEGATE = new PoolDelegate() {
                    @Override
                    public Connection getConnection() throws SQLException {
                        return dsDelegate.getConnection();
                    }
                };
            } catch (Exception e) {
                logger.log(Level.WARNING, e.getMessage(), e);
                e.printStackTrace();
            }
        }

    }

    /**
     * Возвращает подключение к БД
     *
     * @return подключение
     * @throws java.sql.SQLException если возникает ошибка доступа к БД
     */
    public static Connection getConnection() throws SQLException {
        return POOL_DELEGATE.getConnection();
    }

    /** Интерфейс для предоставления соединений из сконфигурированных источников */
    private static interface PoolDelegate {

        /**
         * Возвращает подключение к БД
         *
         * @return подключение
         * @throws java.sql.SQLException если возникает ошибка доступа к БД
         */
        Connection getConnection() throws SQLException;
    }

    private PoolManager() {
    }

}
