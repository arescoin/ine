/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.structure.CustomAttributeDefVal;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Интерфейс для доступа к значениям по-умолчанию для произвольных атрибутов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomAttributeDefValAccess<T extends CustomAttributeDefVal> extends VersionableAccess<T> {

    /**
     * Метод возвращает все актуальные на текущий момент значения по-умолчанию для произвольных атрибутов
     *
     * @return коллекция значений по-умолчанию для произвольных атрибутов
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в данных
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
