/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.entity;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.dbe.da.core.VersionableAccess;
import ru.xr.ine.oss.entity.Person;

import java.util.Collection;

/**
 * Интерфейс для доступа к данным сущности "Персона".
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PersonAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface PersonAccess<T extends Person> extends VersionableAccess<T> {

    /**
     * Метод возвращает все актуальные на текущий момент персоны
     *
     * @return коллекция персон
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
