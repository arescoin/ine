/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.history.ActionAttribute;
import ru.xr.ine.core.history.IdentifiableHistory;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к пользовательскими атрибутам истории изменений идентифицируемых объектов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ActionAttributeAccess<T extends ActionAttribute> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все пользовательские атрибуты для зарегистрированных действий в системе
     *
     * @return коллекция пользовательских атрибутов
     * @throws ru.xr.ine.core.CoreException в случае ошибок при работе с БД, кешем и метаинформацией
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает пользовательские атрибуты для переданной записи об изменениях в идентифицируемых объектах
     *
     * @param history запись об изменениях в идентифицируемом объекте
     * @return коллекция пользовательских атрибутов
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некоррекного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException в случае ошибок при работе с БД, кешем и метаинформацией
     */
    Collection<T> getActionAttributes(IdentifiableHistory history) throws CoreException;
}
