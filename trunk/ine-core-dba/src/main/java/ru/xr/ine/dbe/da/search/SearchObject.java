/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.search;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Элемент поискового условия.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SearchObject.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SearchObject implements Serializable {

    /** Id столбца, по которому осуществляется поиск */
    private BigDecimal searchObject;

    /** Режим поиска. Ищем или на точное совпадение, или по интервалу значений (в том числе и по перечислению) */
    private SearchMode searchMode;

    /** Инвертирование поискового условия */
    private boolean inverseCondition;

    /** Поисковые значение */
    private Collection searchValues;

    public SearchObject(BigDecimal searchObject, SearchMode searchMode,
            boolean inverseCondition, Collection searchValues) {

        this.searchObject = searchObject;
        this.searchMode = searchMode;
        this.inverseCondition = inverseCondition;
        this.searchValues = searchValues;
    }

    public BigDecimal getSearchObject() {
        return this.searchObject;
    }

    public void setSearchObject(BigDecimal searchObject) {
        this.searchObject = searchObject;
    }

    public SearchMode getSearchMode() {
        return this.searchMode;
    }

    public void setSearchMode(SearchMode searchMode) {
        this.searchMode = searchMode;
    }

    public boolean isInverseCondition() {
        return this.inverseCondition;
    }

    public void setInverseCondition(boolean inverseCondition) {
        this.inverseCondition = inverseCondition;
    }

    public Collection getSearchValues() {
        return this.searchValues;
    }

    public void setSearchValues(Collection searchValues) {
        this.searchValues = searchValues;
    }

    public static enum SearchMode {
    }
}
