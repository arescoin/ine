/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.constants;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.constants.ParametrizedConstant;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к параметризованным константам
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ParametrizedConstantAccess<T extends ParametrizedConstant> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент параметризованные константы
     *
     * @return коллекция констант
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
