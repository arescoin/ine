/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.userpermits.NoSuchUserException;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к системным пользователям
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SystemUserAccess<T extends SystemUser> extends VersionableAccess<T> {

    /**
     * Метод возвращает всех системных пользователей
     *
     * @return коллекция системных пользователей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Возвращает пользователя системы по его системному имени и хешу пароля
     *
     * @param login системное имя пользователя
     * @param md5   хеш пароля пользователя
     * @return пользователь
     * @throws NoSuchUserException если пользователь с указанными данными не найден
     * @throws GenericSystemException при ошщибках доступа к данным
     */
    T login(String login, String md5) throws GenericSystemException;
}
