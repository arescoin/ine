/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.*;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;
import ru.xr.rims.services.StorageFilter;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Set;

/**
 * Описывает интерфейс доступа к идентифицируемым системным данным в БД
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IdentifiableAccess<T extends Identifiable> extends ObjectAccess {

    /**
     * Возвращает все актуальные объекты
     *
     * @return коллекция с {@link ru.xr.ine.core.Identifiable}, может быть пустой
     * @throws ru.xr.ine.core.CoreException при возникновении ошибке в процессе получения и обработки данных
     */
    Collection<? extends T> getAllObjects() throws CoreException;

    /**
     * Возвращает составной идентификатор объекта.
     * <p/>
     * По-умолчанию в составной идентификатор попадает 1 идентификатор, содержащийся в столбце N таблицы объекта. Если
     * идентификатор объекта содержится в другом столбце или в нескольких столбцах - необходимо переопределить метод в
     * соответствующем дата-акцессе.
     *
     * @param identifiable объект с идентификаторами
     * @return объект составного идентификатора.
     * @throws ru.xr.ine.core.GenericSystemException при ошибках в работе с мета-информацией.
     */
    SyntheticId getSyntheticId(T identifiable) throws GenericSystemException;

    /**
     * Проверяет составной идентификатор.
     * <p/>
     * При проверке, идентификаторы выставляются в порядке, в котором будет формироваться поисковый запрос.
     *
     * @param id проверяемый составной идентификатор
     * @return составной идентификатор с отсортированными в поисковом порядке значениями
     * @throws ru.xr.ine.core.GenericSystemException при ошибках в работе с мета-информацией.
     */
    SyntheticId checkSyntheticId(SyntheticId id) throws GenericSystemException;

    /**
     * Возвращает актуальный объект по набору идентификаторов
     * <p/>
     * В большинстве случаев идентификатор должен быть один, однако при получении объектов с составными
     * идентификаторами, их будет больше.
     * <p/>
     * Если идентификаторов несколько, то они должны идти в том порядке, в каком они указаны в
     * {@link ru.xr.ine.core.SyntheticId} соответствующего объекта. Для таких случаев <strong>НАСТОЯТЕЛЬНО</strong>
     * рекомендуется использовать метод {@link #checkSyntheticId(ru.xr.ine.core.SyntheticId)}, перед вызовом данного
     * метода.
     *
     * @param id перечисление идентификаторов
     * @return актуальный {@link ru.xr.ine.core.Identifiable объект}
     * @throws ru.xr.ine.core.CoreException при возникновении ошибке в процессе получения и обработки данных
     * @see #checkSyntheticId(ru.xr.ine.core.SyntheticId)
     */
    T getObjectById(BigDecimal... id) throws CoreException;

    /**
     * Создает в системе новый объект
     *
     * @param identifiable новый объект
     * @return созданный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Изменяет свойства существующего в системе объекта
     *
     * @param identifiable    старый объект
     * @param newIdentifiable новый объект
     * @return модифицированный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Удаляет существующий объект
     *
     * @param identifiable удаляемый объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Подготавливает и возвращает SearchCriteriaProfile для типа
     * обрабатываемого конкретной реализацией IdentifiableAccess
     *
     * @return профиль для формирования критериев поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе получения и обработки
     */
    SearchCriteriaProfile getSearchCriteriaProfile() throws GenericSystemException;

    /**
     * Подготавливает и возвращает StorageFilter для типа
     * обрабатываемого конкретной реализацией IdentifiableAccess
     *
     * @param criteria  перечисление статических критериев поиска (могут отсутствовать)
     * @param criterion критерий поиска используемый для учета результатов предыдущего поиска
     * @param fieldName название поля используемого для формирования очередного запроса,
     *                  используется в значениях SearchCriterion
     * @return фильтр объектов в кеше
     */
    StorageFilter createStorageFilter(Set<SearchCriterion> criteria, SearchCriterion criterion, String fieldName);

    /**
     * Подготавливает и возвращает StorageFilter для типа
     * обрабатываемого конкретной реализацией IdentifiableAccess
     *
     * @param criteria  перечисление статических критериев поиска
     * @param fieldName название поля используемого для формирования очередного запроса,
     *                  используется в значениях SearchCriterion
     * @return фильтр объектов в кеше
     */
    StorageFilter createStorageFilter(Set<SearchCriterion> criteria, String fieldName);

    /**
     * Производит поиск объектов по переданным критериям
     *
     * @param searchCriteria критерии поиска
     * @return результат поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе выпонения поискового запроса
     */
    Collection<T> searchObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException;

    /**
     * Производит поиск объектов по переданным критериям
     *
     * @param searchCriteria критерии поиска
     * @param fieldName      название поля используемого для извлечения данных из найденных объектов
     * @return результат поиска - либо объекты поиска, либо извлечённые данные из объектов поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе выпонения поискового запроса
     */
    Collection searchObjects(Set<SearchCriterion> searchCriteria, String fieldName) throws GenericSystemException;
    Collection searchObjects2(Set<SearchCriterion> searchCriteria, String fieldName) throws GenericSystemException;

    /**
     * Производит поиск объектов по переданным критериям
     *
     * @param storageFilters критерии поиска
     * @return результат поиска - либо объекты поиска, либо извлечённые данные из объектов поиска
     * @throws ru.xr.ine.core.GenericSystemException при ошибке в процессе выпонения поискового запроса
     */
    Collection searchObjects(StorageFilter[] storageFilters) throws GenericSystemException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим переданным критериям поиска
     *
     * @param searchCriteria критерии поиска
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    boolean containsObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    boolean containsObjects(StorageFilter[] filters) throws GenericSystemException;

    /**
     * Возвращает количество объектов в хранилище соответствующим переданным критериям поиска
     *
     * @param searchCriteria критерии поиска
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    int getObjectCount(Set<SearchCriterion> searchCriteria) throws GenericSystemException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    int getObjectCount(StorageFilter[] filters) throws GenericSystemException;
}

