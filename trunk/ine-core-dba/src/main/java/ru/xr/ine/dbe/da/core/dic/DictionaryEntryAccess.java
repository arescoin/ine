/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.dic;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к словарным статьям.
 * <br>
 * Доступны только операции чтения, модификация данных запрещена. Для этого необходимо использовать
 * {@link ru.xr.ine.dbe.da.core.dic.DictionaryTermAccess}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface DictionaryEntryAccess<T extends DictionaryEntry> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент словарные статьи
     *
     * @return коллекция всех словарных статей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает все действительные указанные словарные статьи для указанного словаря
     *
     * @param dictionaryId идентификатор словаря
     * @return коллекция словарных статей {@link ru.xr.ine.core.dic.DictionaryEntry}
     * @throws ru.xr.ine.core.GenericSystemException при ошибках получения данных
     */
    public Collection<T> getAllEntriesForDictionary(BigDecimal dictionaryId) throws GenericSystemException;

}
