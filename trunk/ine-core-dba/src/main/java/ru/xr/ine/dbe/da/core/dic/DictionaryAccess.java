/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.dic;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к описаниям словарей
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface DictionaryAccess<T extends Dictionary> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент описания словарей
     *
     * @return коллекция описаний словарей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
