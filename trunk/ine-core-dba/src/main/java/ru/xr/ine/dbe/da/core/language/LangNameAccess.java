/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.language;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.language.LangName;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к внутрисистемным названиям языков
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangNameAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface LangNameAccess<T extends LangName> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все внутрисистемные названия языков
     *
     * @return коллекция внутрисистемных названий языков
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
