/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.links;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.links.Link;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к описаниям связей.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface LinkAccess<T extends Link> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент описания связей
     *
     * @return коллекция описаний словарей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает все описания связей, в которых участвует объект указанного типа.
     *
     * @param clazz класс объекта, для которого получаются описания связей
     * @return коллекция описаний связей
     * @throws GenericSystemException при ошибках в работе кеша или при некорректных данных
     */
    Collection<T> getLinksByObjType(Class clazz) throws GenericSystemException;
}
