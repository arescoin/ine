/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.history.HistoryEvent;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к данным по событиям в системных объектах.
 * <p/>
 * Расширене простого {@link VersionableHistoryAccess}, с добавлением признака обработки события.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface HistoryEventAccess<T extends HistoryEvent> extends VersionableHistoryAccess<T> {

    /**
     * Возвращает колекцию событий требующих обработки
     *
     * @return колекция событий
     * @throws ru.xr.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    Collection<T> getEventsForProcess() throws CoreException;

    /**
     * Возвращает колекцию событий подверженных обработке
     *
     * @return колекция событий
     * @throws ru.xr.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    Collection<T> getProcessedEvents() throws CoreException;

    /**
     * Отмечает событие как обработанное
     *
     * @param historyEvent событие
     * @throws ru.xr.ine.core.GenericSystemException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    void markAsProcessed(T historyEvent) throws GenericSystemException;

}
