/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к дополнительным атрибутам
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomAttributeAccess<T extends CustomAttribute> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент описания дополнительных атрибутов
     *
     * @return коллекция описаний атрибутов
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в данных
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Получает список атрибутов конкретного объекта.
     *
     * @param identifiableClass класс интерфейса объекта, для которого получаются атрибуты.
     * @return коллекция описаний атрибутов
     * @throws ru.xr.ine.core.GenericSystemException при некорректных значениях в данных
     */
    Collection<T> getAttributesByObject(Class<? extends Identifiable> identifiableClass) throws GenericSystemException;
}
