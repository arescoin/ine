/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.language;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.language.SysLanguage;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к представлениям системных языков
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SysLanguageAccess<T extends SysLanguage> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент представления системных языков
     *
     * @return коллекция описаний словарей
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
