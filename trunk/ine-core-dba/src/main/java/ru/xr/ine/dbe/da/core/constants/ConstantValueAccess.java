/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.constants;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.constants.ConstantValue;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к значениям констант
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ConstantValueAccess<T extends ConstantValue> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент значения констант
     *
     * @return коллекция значение констант
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
