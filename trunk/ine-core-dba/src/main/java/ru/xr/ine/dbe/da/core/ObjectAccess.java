/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.CoreException;

import java.util.Collection;
import java.util.Map;

/**
 * Описывает интерфейс доступа к системным данным в БД
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ObjectAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ObjectAccess {

    /**
     * Возвращает все актуальные объекты
     *
     * @return коллекция с {@link ru.xr.ine.core.Identifiable}, может быть пустой
     * @throws ru.xr.ine.core.CoreException при возникновении ошибке в процессе получения и обработки данных
     */
    Collection<?> getAllObjects() throws CoreException;

    /**
     * Возвращает название таблицы
     *
     * @return название таблицы
     */
    String getTableName();

    /**
     * Возвращает мапинг полей интерфейсов на имена столбцов
     *
     * @return ключ - имя поля класса, значение - имя столбца таблицы
     */
    Map<String, String> getInstanceFields();

    /**
     * Возвращает идентификатор основного региона размещения в кеше
     *
     * @return идентификатор региона
     */
    String getMainRegionKey();
}
