/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.userpermits.Permit;
import ru.xr.ine.dbe.da.core.CRUDHelper;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к описаниям доступов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PermitAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface PermitAccess<T extends Permit> extends VersionableAccess<T> {

    /**
     * Метод возвращает список описаний доступов
     *
     * @return коллекция системных пользователей
     * @throws ru.xr.ine.core.CorruptedIdException
     *          при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException
     *          при некорректных значениях в датах системного объекта
     */
    @Override Collection<T> getAllObjects() throws CoreException;

    void addHelper(CRUDHelper<T> helper);
}
