/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.constants;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Утилитный интерфейс для объявления идентификаторов констант, используемых системой.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ConstantsList.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ConstantsList extends Serializable {

    /** Язык системы по-умолчанию */
    BigDecimal DEF_SYS_LANG = new BigDecimal(3);
}
