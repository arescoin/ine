/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomAttributeValue;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к значениям дополнительных атрибутов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomAttributeValueAccess<T extends CustomAttributeValue> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент значения дополнительных атрибутов.
     * <p/>
     * <strong>НАСТОЯТЕЛЬНО РЕКОМЕНДУЕТСЯ ПОДУМАТЬ, ПРЕЖДЕ ЧЕМ ВЫЗЫВАТЬ ДАННЫЙ МЕТОД!!!</strong>
     * <p/>
     * Первый вызов метода до наполнения кеша будет выполняться <strong>ОЧЕНЬ ДОЛГО</strong>, т.к. потребуется выбрать
     * практически всю базу данных в память.
     *
     * @return коллекция значений произвольных атрибутов
     * @throws ru.xr.ine.core.CoreException при некорректных значениях в данных
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Возвращает актуальный объект по набору идентификаторов
     * <p/>
     * Для получения значения custom-атрибута необходимо передать 2 индентификатора:
     * <ul>
     * <li>1-й id - идентификатор экземпляра объекта, для которого получается значение атрибута
     * <li>2-й id - идентификатор описателя custom-атрибута, по которому получается значение
     * </ul>
     * <b>Вместо прямого вызова этого метода настоятельно рекомендуется использовать
     * {@link #getValue(java.math.BigDecimal, ru.xr.ine.core.structure.CustomAttribute)}!</b>
     *
     * @param id перечисление идентификаторов
     * @return актуальный {@link T объект}
     * @throws ru.xr.ine.core.CoreException при возникновении ошибке в процессе получения и обработки данных
     * @see #getValue(java.math.BigDecimal, ru.xr.ine.core.structure.CustomAttribute)
     */
    @Override
    T getObjectById(BigDecimal... id) throws CoreException;

    /**
     * Получает значение конкретного атрибута
     *
     * @param identifiableId идентификатор экземпляра объекта, для которого получается значение атрибута
     * @param attribute      описатель дополнительного атрибута
     * @return значение атрибута
     * @throws CoreException при некорректных значениях в данных
     */
    T getValue(BigDecimal identifiableId, CustomAttribute attribute) throws CoreException;
}
