/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.history.VersionableHistory;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к данным по истории изменений версионного объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface VersionableHistoryAccess<T extends VersionableHistory> extends IdentifiableHistoryAccess<T> {

    /**
     * Метод возвращает все записи об изменениях в идентифицируемых объектах
     *
     * @param sysObjId идентификатор системного объекта
     * @param entityId идентификатор сущности
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.CorruptedIdException при обнаружении некоррекного идентификатора в системном объекте
     * @throws ru.xr.ine.core.CoreException при некорректых значениях в датах системного объекта
     */
    @Override
    Collection<T> getActionsHistoryByIds(BigDecimal sysObjId, SyntheticId entityId) throws CoreException;
}
