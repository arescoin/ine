/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Исключение выбрасывается при попытке установить невалидное значение имени пользователя системы.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: InvalidUserNameException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class InvalidUserNameException extends IneIllegalArgumentException {


    /**
     * Конструирует новое исключение с указанием детального сообщения о причине
     *
     * @param message детализация ошибки
     */
    public InvalidUserNameException(String message) {
        super(message);
    }

    /**
     * Конструирует новое исключение с указанием детального сообщения о причине и с указанием инициирующего исключения
     *
     * @param message детализация ошибки
     * @param cause   инициирующее исключение
     */
    public InvalidUserNameException(String message, Throwable cause) {
        super(message, cause);
    }
}
