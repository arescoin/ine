/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Внутрисистемные названия языков.
 * <p>
 * Предназначен для визуализации объектов {@link ru.xr.ine.core.language.SysLanguage}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangName.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface LangName extends Identifiable {

    String NAME = "name";

    /**
     * Возвращает системное название языка
     *
     * @return название языка
     */
    String getName();

    /**
     * Задает системное название языка
     *
     * @param name название языка
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setName(String name) throws IneIllegalArgumentException;
}
