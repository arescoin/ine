/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria;

/**
 * Болванка для генерации классов извлекающих значения миную рефлекшн.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Extractor.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class Extractor implements Cloneable {

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * Метод извлекает значение из переданного объекта, должен быть реализован в генерированных классах
     *
     * @param o объект содержащий значение
     * @return извлеченое значение
     * @throws Exception при возникновении ошибки в процессе работы сгенерированного кода
     */
    public abstract Object extractValue(Object o) throws Exception;

}
