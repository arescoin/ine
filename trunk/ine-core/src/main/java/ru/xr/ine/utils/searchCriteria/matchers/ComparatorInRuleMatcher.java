/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import java.util.Comparator;

/**
 * Матчер для поиска во множестве по полному совподению.
 * Количество паттернов: один и более
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ComparatorInRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ComparatorInRuleMatcher extends AbstractComparatorRuleMatcher {

    public ComparatorInRuleMatcher(Comparator comparator) {
        super(comparator);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        try {
            for (Object o : pattern) {
                if (comparator.compare(value, o) == 0) {
                    return true;
                }
            }
        } catch (ClassCastException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }
}
