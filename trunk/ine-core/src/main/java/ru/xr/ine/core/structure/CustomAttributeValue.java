/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.Versionable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Значение атрибута.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValue.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomAttributeValue extends Versionable {

    ValueComparator VALUE_COMPARATOR = new ValueComparator();

    String ENT_N = "entityId";
    String VALUE = "value";

    /**
     * Получает идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     *
     * @return идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     */
    BigDecimal getEntityId();

    /**
     * Устанавливает идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     *
     * @param entityId идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     * @throws ru.xr.ine.core.IllegalIdException если значение null или меньше
     * {@link #MIN_ALLOWABLE_VAL минимально допустимого значения}
     */
    void setEntityId(BigDecimal entityId) throws IllegalIdException;

    /**
     * Получает значение атрибута.
     *
     * @return значение атрибута.
     */
    Object getValue();

    /**
     * Устанавливает значение атрибута.
     *
     * @param value значение атрибута.
     */
    void setValue(Object value);

    class ValueComparator implements Comparator, Serializable {
        @SuppressWarnings({"unchecked"})
        @Override
        public int compare(Object o1, Object o2) {
            return ((Comparable) o1).compareTo(o2);
        }
    }
}
