/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Оболочка для конфигурирования параметров паттерна.
 * <p/>
 * При параметризации простым значением достаточно использовать результат вызова метода {@link #getFinalId()}.
 * <p/>
 * При использовании более сложных параметров необходимо проверять наличие {@link Element элементов}<br>
 * методом {@link #hasElements()}. Немодифицируемый список элементов возвращает метод {@link #getElements()}.<br>
 * Элементы в списке расположены в порядке их указания в паттерне.
 * <br>
 * Паттерн может быть либо "простым", либо "сложным" - см. метод {@link #isComplexPattern()}.
 * <br>
 * Примеры "простых" паттернов:
 * <ul>
 * <li><b>5</b> - означает, что параметризованное данным паттерном свойство может принимать значение
 * из множества хранимых в {@link ru.xr.ine.core.structure.SystemObject системном объекте} с номером <b>5</b></li>
 * <li><b>[1:2][3:4]5</b> - означает, что параметризованное данным паттерном свойство может принимать значение
 * из множества хранимых в системном объекте с номером <b>5</b> и имеющих в значениях связанной сущности
 * в системных объектах: <b>1</b> - <i>значение 2</i>, <b>3</b> - <i>значение 4</i></li>
 * </ul>
 * <br>
 * Примеры "сложных" паттернов:
 * <ul>
 * <li><b>[1:2-4]5</b> - означает, что параметризованное данным паттерном свойство может принимать значения
 * из множества хранимых в системном объекте с номером <b>5</b>, дополнительно ограниченных значениями в связной
 * сущности с номером <b>1</b> лежащими в диапозоне от <i>значения 2</i> до <i>значения 4</i> включительно.</li>
 * <li><b>[1:2,4]5</b> - означает, что параметризованное данным паттерном свойство может принимать значения
 * из множества хранимых в системном объекте с номером <b>5</b>, дополнительно ограниченных конкретными значениями
 * в связной сущности с номером <b>1</b>, а именно - <i>значение 2</i> или <i>значение 4</i>.</li>
 * <li><b>[1:2-4,6][7:8,10,15-17]20</b> - пример смешанного ограничения. Из системного объекта с номером 20
 * выбираются значения, ограниченные значениями в связной сущности в системных объектах: <b>1</b> - <i>значения в
 * интервале от 2 до 4 или значение 6</i>, <b>7</b> - <i>значения 8, или 10 или в интервале от 15 до 17</i></li>
 * </ul>
 * <br>
 * Ещё один пример ограничения доступных значений в паттерне:<p/>
 * <b>[1:2-4]21</b> - свойство, параметризованное данным паттерном, может принимать значения из системного объекта
 * с номером <b>21</b> и лежащих в <i>диапозоне значений от 2 до 4</i>.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectPattern.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemObjectPattern implements Serializable {

    /** Начало элемента */
    public final static String LEFT_BRACE = "[";
    /** Завершение элемента */
    public final static String RIGHT_BRACE = "]";
    /** Разделитель значений в элементе */
    public final static String EL_SEPERATOR = ":";
    /** Разделитель элементов сложного элемента паттерна */
    public static final String COMMA = ",";
    /** Разделитель элементов в интервале */
    public static final String HYPHEN = "-";
    /** Регулярное выражение для валидации строкового представления паттерна - ^(\[\d+:(\d+[,-])*\d+\])*\d+$ */
    public static final String RX_PATTERN = "^(\\" + LEFT_BRACE + "\\d+" + EL_SEPERATOR + "(\\d+" +
            LEFT_BRACE + COMMA + HYPHEN + RIGHT_BRACE + ")*\\d+\\" + RIGHT_BRACE + ")*\\d+$";

    /** Перечисление элементов паттерна */
    private ArrayList<Element> elements = new ArrayList<Element>();

    /** Конечный идентификатор системного объектна, пареметризующего значение */
    private BigDecimal finalId;

    /** Идентификатор таблицы, которой принадлежит {@link #finalId} */
    private BigDecimal tableId;

    private static Logger logger = Logger.getLogger(SystemObjectPattern.class.getName());

    /**
     * Конструирует паттерн с указанием значения конечного идентификатора системного объекта
     *
     * @param finalId идентификатор системного объекта, параметризующего значение
     * @throws ru.xr.ine.core.IllegalIdException при попытке передать некорректный идентификатор
     */
    public SystemObjectPattern(BigDecimal finalId) throws IllegalIdException {
        validateId(finalId, "finalId");

        this.finalId = finalId;
    }

    /**
     * Конструирует полный паттерн по строковому представлению
     *
     * @param pattern строковое представление паттерна
     */
    public SystemObjectPattern(String pattern) {

        if (validateStringPattern(pattern)) {
            String stringPattern = pattern.trim();

            int lastBracePosition = stringPattern.lastIndexOf(RIGHT_BRACE);
            String finalIdString = stringPattern.substring(lastBracePosition + 1);

            finalId = new BigDecimal(finalIdString);

            if (lastBracePosition > -1) {
                parsePattern(stringPattern.substring(0, lastBracePosition + 1));
            }
        } else {
            throw new IneIllegalArgumentException("Invalid pattern '" + pattern + "'");
        }
    }

    /**
     * Преобразует строковое представление элементов паттерна в
     * {@link ru.xr.ine.core.structure.SystemObjectPattern.Element java-объекты}
     * и размещает их в {@link #elements список}
     *
     * @param pattern строковое представление элементов паттерна
     */
    private void parsePattern(String pattern) {

        int rightBracePosition = pattern.indexOf(RIGHT_BRACE);
        String string = pattern.substring(1, rightBracePosition);

        String[] ids = string.split(EL_SEPERATOR);

        elements.add(new Element(new BigDecimal(ids[0]), ids[1]));

        if (pattern.length() != rightBracePosition + 1) {
            parsePattern(pattern.substring(rightBracePosition + 1));
        }
    }

    /**
     * Проверяет корректность строкового представления паттерна.<br/>
     * Для проверки используется {@link #RX_PATTERN регулярное выражение}.
     *
     * @param pattern строка с паттерном
     * @return <code>true</code> - корректно, иначе - <code>false</code>
     */
    public static boolean validateStringPattern(String pattern) {
        return validateStringPattern(pattern, RX_PATTERN);
    }

    /**
     * Проверяет корректность строкового представления паттерна в соответствии с переданным регулярным выражением
     *
     * @param pattern    строка с паттерном
     * @param expression регулярное выражение для проверки паттерна
     * @return <code>true</code> - корректно, иначе - <code>false</code>
     */
    public static boolean validateStringPattern(String pattern, String expression) {
        boolean result = false;

        if (pattern == null) {
            logger.warning("Validated sting is null");
        } else if (pattern.trim().length() < 1) {
            logger.warning("Validated sting to short: '" + pattern + "'");
        } else {
            pattern = pattern.trim();
            Pattern rxPattern = Pattern.compile(expression);
            Matcher matcher = rxPattern.matcher(pattern);
            result = matcher.find();

            if (!result) {
                logger.warning("Validated sting is not valid: '" + pattern + "'");
            }
        }

        return result;
    }

    /**
     * Возвращает конечный идентификатор системного объектна, пареметризующего значение
     *
     * @return конечный идентификатор
     */
    public BigDecimal getFinalId() {
        return this.finalId;
    }

    /**
     * Возвращает идентификатор таблицы, которой принадлежит {@link #getFinalId() finalId}
     *
     * @return идентификатор таблицы
     */
    public BigDecimal getTableId() {
        return tableId;
    }

    /**
     * Устанавливает идентификатор таблицы, которой принадлежит {@link #getFinalId() finalId}
     *
     * @param tableId идентификатор таблицы
     */
    public void setTableId(BigDecimal tableId) {
        this.tableId = tableId;
    }

    /**
     * Возвращает немодифицируемую  коллекцию, содержащую перечисление элементов сложного паттерна
     *
     * @return элементы паттерна
     */
    public List<Element> getElements() {
        return Collections.unmodifiableList(elements);
    }

    /**
     * Возвращает признак наличия элементов сложного паттерна
     *
     * @return <code>true</code>элементы имеются, иначе - <code>false</code>
     */
    public boolean hasElements() {
        return !this.elements.isEmpty();
    }

    /**
     * Добавляет элемент в паттерн
     *
     * @param element элемент паттерна
     * @return признак модификации паттерна
     */
    public boolean addElement(Element element) {
        return this.elements.add(element);
    }

    /**
     * Определяет тип паттерна: <code>true</code> - паттерн "сложный", <code>false</code> - паттерн "простой".
     * "Простой" паттерн содержит <strong>только</strong> {@link Element#isComplexElement() простые элементы}.
     *
     * @return тип паттерна: <code>true</code> - сложный; <code>false</code> - простой.
     */
    public boolean isComplexPattern() {
        boolean result = false;
        for (Element element : elements) {
            if (element.isComplexElement()) {
                result = true;
                break;
            }
        }
        return result;
    }

    private static void validateId(BigDecimal bigDecimal, String paramName) throws IllegalIdException {
        if (bigDecimal == null) {
            throw new IllegalIdException(paramName + " can't be null.");
        }
        if (BigDecimal.ONE.compareTo(bigDecimal) > 0) {
            throw new IllegalIdException(paramName + " can't be less than 1, but has [" + bigDecimal.toString() + "]");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof SystemObjectPattern)) {
            return false;
        }
        SystemObjectPattern pattern = (SystemObjectPattern) o;

        return this.finalId.equals(pattern.finalId) && this.elements.equals(pattern.elements);
    }

    @Override
    public int hashCode() {
        int result = elements != null ? elements.hashCode() : 0;
        result = 31 * result + (finalId != null ? finalId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();

        for (Element element : elements) {
            builder.append(element);
        }
        builder.append(finalId.longValue());

        return builder.toString();
    }

    /**
     * Элемент паттерна, состоит из двух атрибутов:
     * <ul>
     * <li>идентификатор системного объекта</li>
     * <li>идентификаторы сущностей (содержимое системного объекта)</li>
     * </ul>
     */
    public static class Element implements Serializable {

        /** Идентификатор системного объекта */
        private final BigDecimal sysObjId;
        /** Идентификаторы сущностей (содержимое системного объекта) */
        private final Collection<BigDecimal> entityIds;
        /** Признак комплексного элемента, содержащего несколько идентификаторов сущностей */
        private final boolean complexElement;
        /** Строковое представление элемента */
        private final String stringView;

        /**
         * Конструирует новый элемент, на основе переданных идентификаторов
         *
         * @param sysObjId  идентификатор системного объекта
         * @param entityIds идентификаторы сущностей (содержимое системного объекта)
         * @throws ru.xr.ine.core.IllegalIdException при попытке передать некорректные идентификаторы
         */
        @SuppressWarnings({"unchecked"})
        public Element(BigDecimal sysObjId, String entityIds) throws IllegalIdException {

            validateId(sysObjId, "sysObjId");
            this.sysObjId = sysObjId;

            this.entityIds = new HashSet<BigDecimal>();
            /**
             * Если строка с сущностями содержит символы {@link COMMA} и/или {@link HYPHEN}, то это сложный элемент,
             * и при разборе проверяется корректность задания интервала объектов.
             */
            if (entityIds.contains(COMMA) || entityIds.contains(HYPHEN)) {
                /** Сначала разбираем строку на элементы, разделённые символом {@link COMMA}, это перечисление. */
                for (String part : entityIds.split(COMMA)) {
                    /** Далее элемнты разделяем по символу {@link HYPHEN}, это интервал. */
                    if (part.contains(HYPHEN)) {
                        /** Интервал задаётся только двумя значениями */
                        if (part.indexOf(HYPHEN) != part.lastIndexOf(HYPHEN)) {
                            throw new IneIllegalArgumentException("Invalid pattern part: " + part);
                        }
                        /** Получаем границы интервала */
                        String[] parts = part.split(HYPHEN);
                        long[] ids = new long[parts.length];
                        for (int i = 0; i < ids.length; i++) {
                            ids[i] = getId(parts[i], "element[" + i + "] of " + part).longValue();
                        }
                        /** Заполняем значения идентификаторов сущностей */
                        for (long id = ids[0]; id <= ids[1]; id++) {
                            this.entityIds.add(new BigDecimal(id));
                        }
                    } else {
                        this.entityIds.add(getId(part, "element"));
                    }
                }
            } else {
                this.entityIds.add(getId(entityIds, "entityIds"));
            }
            this.complexElement = this.entityIds.size() > 1;

            this.stringView = "[" + this.sysObjId + ":" + entityIds + "]";
        }

        private static BigDecimal getId(String bigDecimal, String paramName) throws IllegalIdException {
            BigDecimal id = new BigDecimal(bigDecimal);
            validateId(id, paramName);
            return id;
        }

        /**
         * Возвращает идентификатор системного объекта
         *
         * @return идентификатор системного объекта
         */
        public BigDecimal getSysObjId() {
            return this.sysObjId;
        }

        /**
         * Возвращает идентификаторы сущностей (содержимое системного объекта).<br>
         *
         * @return идентификаторы сущностей
         */
        public Collection<BigDecimal> getEntityIds() {
            return this.entityIds;
        }

        /**
         * Определяет тип элемента: <code>true</code> - элемент "сложный", иначе - "простой".<br>
         * "Простой" элемент содержит ровно один индентификатор сущности.
         *
         * @return тип элемента: <code>true</code> - простой; <code>false</code> - простой.
         */
        public boolean isComplexElement() {
            return complexElement;
        }

        @Override
        public int hashCode() {
            return this.sysObjId.hashCode() + (31 * this.entityIds.hashCode());
        }

        @SuppressWarnings({"unchecked"})
        @Override
        public boolean equals(Object obj) {

            if (obj == null || !(obj instanceof Element)) {
                return false;
            }
            if (this == obj) {
                return true;
            }

            Element other = (Element) obj;
            return this.entityIds.equals(other.getEntityIds()) && this.getSysObjId().equals(other.sysObjId);
        }

        @Override
        public String toString() {
            return stringView;
        }
    }

}
