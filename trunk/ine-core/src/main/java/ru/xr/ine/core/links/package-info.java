/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

/**
 *  Пакет предоставляет набор интерфейсов, позволяющих описывать ассоциации объектов
 *
 * @since 1.0
 * @SVNVersion "$Id: package-info.java 29 2017-04-04 15:32:19Z xerror $"
 */

package ru.xr.ine.core.links;
