/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import java.util.Date;

/**
 * Интерфейс предназначен для реализации поддержки версионных объектов.
 * <p/>
 * Все объекты поддерживающие версионность обязаны реализовывать данный интерфейс.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Versionable.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface Versionable extends Identifiable {

    String CORE_FD = "coreFd";
    String CORE_TD = "coreTd";

    /**
     * Устанавливает дату начала "жизни" версии объекта
     *
     * @param fd начало"жизни" версии объекта
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setCoreFd(Date fd) throws IneIllegalArgumentException;

    /**
     * Возвращает дату начала "жизни" версии объекта
     *
     * @return начало "жизни" версии объекта
     */
    Date getCoreFd();

    /**
     * Устанавливает дату завершения "жизни" версии объекта
     *
     * @param td окончание "жизни" версии объекта
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setCoreTd(Date td) throws IneIllegalArgumentException;

    /**
     * Возвращает дату завершения "жизни" версии объекта
     *
     * @return окончание "жизни" версии объекта
     */
    Date getCoreTd();

}
