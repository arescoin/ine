/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.config;

import ru.xr.ine.core.IneIllegalArgumentException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractPropertiesBasedConfig.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class AbstractPropertiesBasedConfig extends AbstractConfig {

    protected Properties properties = new Properties();


    @Override
    public void init(String initialisationParam, String... additionalParams) throws ConfigurationException {

        super.init(initialisationParam, additionalParams);

        File configFile = new File(configParam);

        if (!configFile.isFile()) {
            throw new ConfigurationException("Configuration file doesn't exists [" + configParam + "], ["
                    + configFile.getAbsolutePath() + "]");
        }

        if (!configFile.canRead()) {
            throw new ConfigurationException("Configuration file does not readable [" + configParam + "], ["
                    + configFile.getAbsolutePath() + "]");
        }

        try {
            properties.load(new FileInputStream(configFile));
        } catch (IOException e) {
            throw new ConfigurationException("Can't read config", e);
        }

    }

    public String getValueByKey(String key) {
        return properties.getProperty(key);
    }

    public String getValueByKey(String key, String defVal) {
        return properties.getProperty(key, defVal);
    }

    /**
     * Возвращает набор пропертей соответствующих переданому перфиксу
     *
     * @param preffix начало ключа
     * @return набор пропертей
     * @throws ru.xr.ine.core.IneIllegalArgumentException если префикс - null или пустой
     */
    public Properties getPropertiesByPref(String preffix) throws IneIllegalArgumentException {

        if (preffix == null || preffix.trim().isEmpty()) {
            throw new IneIllegalArgumentException("Preffix value can't be null or zero length");
        }

        final Properties result = new Properties();

        for (Map.Entry o : properties.entrySet()) {

            final String key = (String) o.getKey();
            if (key.startsWith(preffix)) {
                result.put(key, o.getValue());
            }
        }

        return result;
    }
}
