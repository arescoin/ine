/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

/**
 * Запись истории изменений с признаком ее обработки системой нотификации
 * <p/>
 * Расширене простого {@link VersionableHistory}, с добавлением признака обработки события.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEvent.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface HistoryEvent extends VersionableHistory {

    /** Перечисление полей */
    String PROCESSED = "processed";

    /**
     * Возвращает признак обработки события
     *
     * @return обработано - true, иначе - false
     */
    boolean isProcessed();

    /**
     * Устанавливает признак обработки события
     *
     * @param processed обработано - true, иначе - false
     */
    void setProcessed(boolean processed);

}
