/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.config;

import ru.xr.ine.core.GenericSystemException;

/**
 * Данное исключение выбрасывается подсистемой конфигурации
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConfigurationException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConfigurationException extends GenericSystemException{

    public ConfigurationException(String message) {
        super(message);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
