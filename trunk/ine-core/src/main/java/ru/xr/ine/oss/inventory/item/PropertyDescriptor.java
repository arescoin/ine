/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.Versionable;
import ru.xr.ine.core.structure.DataType;

/**
 * Описывает свойство элемента системы учета.
 * <p>
 * Значение сойства может изменяться или быть константным,
 * но не следует путать свойство с рабочим значением {@link ValueDescriptor}.
 * <br>
 * Описание не привязано ни к какому конкретному элементу
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDescriptor.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface PropertyDescriptor extends Versionable {

    String NAME = "name";
    String TYPE = "type";

    String getName();

    void setName(String name);

    DataType getType();

     void setType(DataType type);

}
