/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.Versionable;

/**
 * Интерфейс описывает набор методов для работы с описанием роли
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Role.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface Role extends Versionable {

    String NAME = "name";

    /**
     * Возвращает название роли.
     * <p/>
     * Во избежание недоразумений следует соблюдать уникальность имен, однако идентификатором роли имя не является
     *
     * @return название роли
     */
    String getName();

    /**
     * Устанавливает название роли.
     * <p/>
     * Во избежание недоразумений следует соблюдать уникальность имен, однако идентификатором роли имя не является
     *
     * @param name название роли
     * @throws ru.xr.ine.core.IneIllegalArgumentException если name - NULL или пустая строка
     */
    void setName(String name) throws IneIllegalArgumentException;

}
