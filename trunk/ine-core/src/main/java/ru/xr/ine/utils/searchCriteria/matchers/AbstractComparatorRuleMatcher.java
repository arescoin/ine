/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import ru.xr.ine.utils.searchCriteria.RuleMatcher;

import java.util.Comparator;

/**
 * Базовый класс для матчеров, использующих компаратор для гибкого сравнения объектов.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: AbstractComparatorRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class AbstractComparatorRuleMatcher implements RuleMatcher {

    /** Компаратор, выполняющий сравнение объектов */
    protected Comparator comparator;

    protected AbstractComparatorRuleMatcher(Comparator comparator) {
        this.comparator = comparator;
    }
}
