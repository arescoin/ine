/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.Versionable;

/**
 * Интерфейс описывает методы доступные для объекта-хранилища значения константы
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantValue.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ConstantValue extends Versionable {

    String VALUE = "value";

    /**
     * Возвращает значение константы
     *
     * @return значение константы
     */
    String getValue();

    /**
     * Устанавливает значение константы
     *
     * @param value значение константы
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setValue(String value) throws IneIllegalArgumentException;

}
