/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import ru.xr.ine.utils.searchCriteria.RuleMatcher;

/**
 * Матчер для строки на предмет вхождения значения в значение шаблонной строки.
 * <p/>
 * Количество патернов: строго один
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: StringContainsRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class StringContainsRuleMatcher implements RuleMatcher {

    @Override
    public boolean match(Object value, Object... pattern) {
        String val = (String) value;
        String pat = (String) pattern[0];
        return val.contains(pat);
    }

}
