/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import ru.xr.ine.utils.searchCriteria.RuleMatcher;

/**
 * Простой матчер для проверки объекта на null
 * <p/>
 * Количество патернов: ни одного
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CommonIsNullMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CommonIsNullMatcher implements RuleMatcher {

    @Override
    public boolean match(Object value, Object... pattern) {
        return value == null;
    }
}
