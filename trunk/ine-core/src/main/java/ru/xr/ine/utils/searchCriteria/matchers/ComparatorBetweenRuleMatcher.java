/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import java.util.Comparator;

/**
 * Простой матчер для поиска в диапозоне.
 * Количество паттернов: строго два
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ComparatorBetweenRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ComparatorBetweenRuleMatcher extends AbstractComparatorRuleMatcher{

    public ComparatorBetweenRuleMatcher(Comparator comparator) {
        super(comparator);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        try {
            return comparator.compare(value, pattern[0]) >= 0 && comparator.compare(value, pattern[1]) <= 0;
        } catch (ClassCastException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
