/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

/**
 * Пакет предоставляет набор интерфейсов для работы с системными языками.
 * <p>
 * Система локализации настраивается с использование основных представлений языков и стран,
 * в соответствии со стандартами iso639 и iso3166.
 *
 * @since 1.0
 * @SVNVersion "$Id: package-info.java 29 2017-04-04 15:32:19Z xerror $"
 * */
package ru.xr.ine.core.language;