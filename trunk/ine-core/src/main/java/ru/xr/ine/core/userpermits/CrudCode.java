/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.utils.BinaryMaskUtil;

import java.util.Set;

/**
 * Перечиыление возможных действий над объектом
 * <ul>
 * <li>1 - none;</li>
 * <li>2 - update;</li>
 * <li>4 - create;</li>
 * <li>8 - delete.</li>
 * </ul>
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CrudCode.java 29 2017-04-04 15:32:19Z xerror $"
 */
public enum CrudCode {

    none(1), update(2), create(4), delete(8);

    /** Код конкретного представителя */
    private final int code;

    private CrudCode(int code) {
        this.code = code;
    }

    /**
     * Возвращает числовой код
     *
     * @return числовое представление
     */
    public int getCode() {
        return this.code;
    }

    /**
     * Возвращает тип CRUD по переданному коду типа
     *
     * @param code код типа
     * @return тип
     * @throws ru.xr.ine.core.IneIllegalArgumentException при передаче неподдерживаемого кода
     */
    public static CrudCode getCrudCodeByIntVal(int code) throws IneIllegalArgumentException {

        CrudCode result;

        switch (code) {
        case 1:
            result = none;
            break;
        case 2:
            result = update;
            break;
        case 4:
            result = create;
            break;
        case 8:
            result = delete;
            break;
        default:
            throw new IneIllegalArgumentException("Unsuported Code: " + code);
        }

        return result;
    }

    /**
     * Разбирает полученную маску на конкретные числовые представления кодов
     *
     * @param mask маска для разбора
     * @return массив из конкретных числовых кодов
     */
    public synchronized static int[] getByMask(int mask) {

        Set<Long> codes = BinaryMaskUtil.maskToValues(mask);
        int[] result = new int[codes.size()];
        int position = 0;

        for (Long aLong : codes) {
            result[position++] = aLong.intValue();
        }

        return result;
    }

    /**
     * Преобразует массив числовых представлений кодов в массив кодов
     *
     * @param ints числовые коды
     * @return резулбтирующий массив CrudCode
     * @throws ru.xr.ine.core.IneIllegalArgumentException при передаче в метод null
     * и при обнаружении некорректного числового представления в массиве
     */
    public synchronized static CrudCode[] toCodes(int[] ints) throws IneIllegalArgumentException {

        if (ints == null) {
            throw new IneIllegalArgumentException("Argumen can't be null");
        }

        CrudCode[] result = new CrudCode[ints.length];

        for (int positon = 0; positon < ints.length; positon++) {
            result[positon] = getCrudCodeByIntVal(ints[positon]);
        }

        return result;
    }

}
