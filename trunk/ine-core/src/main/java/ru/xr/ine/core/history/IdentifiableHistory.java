/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.userpermits.CrudCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Запись истории изменений идентифицируемого объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistory.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IdentifiableHistory extends Identifiable {

    String ENTITY_ID = "entityId";
    String USER_ID = "userId";
    String ACTION_DATE = "actionDate";
    String ACTION_TYPE = "type";
    String SYSOBJ_ID = "sysObjId";
    String VERSION_DSC = "versionDsc";

    /**
     * Задает идентификатор системного объекта
     * <p/>
     * Идентификатор, участвующий в построении модели системы (мета)
     *
     * @param id идентификатор системного объекта
     * @throws ru.xr.ine.core.IllegalIdException при попытке передачи в качестве параметра пустой ссылки, пустого
     * значения, или значения не удовлетворяющего требованиям к идентификатору
     */
    void setCoreId(BigDecimal id) throws IllegalIdException;

    /**
     * Возвращает идентификатор системного объекта
     * <p/>
     * Идентификатор, участвующий в построении модели системы (мета)
     *
     * @return идентификатор системного объекта
     */
    BigDecimal getCoreId();

    /**
     * Возвращает обоснование к внесенным изменениям
     *
     * @return обоснование
     */
    String getCoreDsc();

    /**
     * Задает обоснование к внесенным изменениям
     *
     * @param dsc обоснование
     */
    void setCoreDsc(String dsc);

    /**
     * Возвращает уникальный идентификатор объекта
     * <p/>
     * В конкретном сдучае это идентификатор именно сущности подвергшейся изменению
     *
     * @return уникальный идентификатор объекта
     */
    SyntheticId getEntityId();

    /**
     * Устанавливает уникальный идентификатор для объекта
     * <p/>
     * В конкретном сдучае это идентификатор именно сущности подвергшейся изменению.
     *
     * @param entityId уникальный идентификатор
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setEntityId(SyntheticId entityId) throws IneIllegalArgumentException;

    /**
     * Возвращает идентификатор пользователя изменившего версию
     *
     * @return идентификатор пользователя изменившего версию
     */
    BigDecimal getUserId();

    /**
     * Задает идентификатор пользователя изменившего версию
     *
     * @param userId идентификатор пользователя изменившего версию
     * @throws ru.xr.ine.core.IllegalIdException при попытке передачи в качестве параметра пустой ссылки, пустого
     * значения, или значения не удовлетворяющего требованиям к идентификатору
     */
    void setUserId(BigDecimal userId) throws IllegalIdException;

    /**
     * Возвращает дату действия
     *
     * @return дата действия
     */
    Date getActionDate();

    /**
     * Задает дату действия
     *
     * @param actionDate дата действия
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setActionDate(Date actionDate) throws IneIllegalArgumentException;

    /**
     * Возвращает тип действия
     *
     * @return тип
     */
    CrudCode getType();

    /**
     * Устанавливает тип действия
     *
     * @param type тип
     */
    void setType(CrudCode type);

}
