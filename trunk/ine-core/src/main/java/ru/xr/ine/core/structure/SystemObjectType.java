/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import java.math.BigDecimal;


/**
 * Является репликой констант типов установленных в пакете INE_CORE_SYS_OBJ.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectType.java 29 2017-04-04 15:32:19Z xerror $"
 */
public enum SystemObjectType {

    /** реплика для: INE_CORE_SYS_OBJ.TABLE_TYP */
    table(BigDecimal.ONE),

    /** реплика для: INE_CORE_SYS_OBJ.COLUMN_TYP */
    column(new BigDecimal(2)),

    /** реплика для: INE_CORE_SYS_OBJ.SEQUENCE_TYP */
    sequence(new BigDecimal(3)),

    /** реплика для: INE_CORE_SYS_OBJ.INTERFACE_TYP */
    interace(new BigDecimal(4));

    private final BigDecimal typeCode;

    private SystemObjectType(BigDecimal typeCode) {
        this.typeCode = typeCode;
    }

    public BigDecimal getTypeCode() {
        return this.typeCode;
    }
}
