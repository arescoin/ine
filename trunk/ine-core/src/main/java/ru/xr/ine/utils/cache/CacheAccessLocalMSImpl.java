/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.cache;

import ru.xr.cache.Cache;
import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.rims.services.StorageFilter;

import java.io.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Локальный вариант использования кеша, не использует сервер, развернут в одной jvm с приложением
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CacheAccessLocalMSImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CacheAccessLocalMSImpl implements CacheAccess {

    private static final Object LOCK = new Object();

    @Override
    public boolean containsRegion(String region) throws GenericSystemException {
        return Cache.hasRegion(region);
    }

    @Override
    public Object get(String region, Object key, Producer producer) throws CoreException {
        try {

            Object result = Cache.get(region, key);
            if (result == null) {
                result = producer.get(key);
                if (result != null) {
                    Cache.put(region, key, forkObject(result));
                }
            }

            return forkObject(result);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }

    }

    @Override
    public Object get(String region, Object key, Producer producer, boolean block) throws CoreException {
        return this.get(region, key, producer);
    }

    @Override
    public Map getRegionContent(String region, Producer producer, boolean block) throws CoreException {

        Map result = Cache.get(region);

        try {
            if (result.isEmpty()) {

                synchronized (LOCK) {
                    result = Cache.get(region);

                    if (result.isEmpty()) {
                        result = (Map) producer.get(region);
                        if (result != null) {
                            Cache.put(region, result);
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        return (Map) forkObject(result);
    }

    @Override
    public Map getRegionContent(String region, Producer producer) throws CoreException {
        return getRegionContent(region, producer, false);
    }

    @Override
    public Set<Object> getKeys(String region) throws CoreException {
        return new HashSet<Object>(Cache.getKeys(region));
    }

    @Override
    public Object put(String region, Object key, Object value) throws GenericSystemException {

        Cache.put(region, key, forkObject(value));

        return null;
    }

    @Override
    public Object put(String region, Object key, Object value, boolean block) throws GenericSystemException {

        Cache.put(region, key, forkObject(value));

        return null;
    }

    @Override
    public void put(String region, Map values) throws GenericSystemException {

        Map map = (Map) forkObject(values);
        for (Object o : values.entrySet()) {
            Map.Entry entry = (Map.Entry) o;
            map.put(entry.getKey(), forkObject(entry.getValue()));
        }

        Cache.put(region, map);
    }


    @Override
    public void put(String region, Map values, boolean block) throws GenericSystemException {
        put(region, values);
    }

    private Object forkObject(Object object) throws GenericSystemException {

        try {

            PipedInputStream convertPipe = new PipedInputStream(999999);
            PipedOutputStream dataPipe = new PipedOutputStream(convertPipe);

            ObjectOutputStream objectOutputStream = new ObjectOutputStream(dataPipe);
            objectOutputStream.writeObject(object);

            ObjectInputStream objectInputStream = new ObjectInputStream(convertPipe);

            return objectInputStream.readObject();

        } catch (IOException | ClassNotFoundException e) {
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    @Override
    public void remove(String region, Object key) throws GenericSystemException {
        Cache.remove(region, key);
    }

    @Override
    public void remove(String region, Object key, boolean block) throws GenericSystemException {
        Cache.remove(region, key);
    }

    @Override
    public void invalidateAll() throws GenericSystemException {
        for (Object o : Cache.getRegions()) {
            Cache.removeRegion(o);
        }
    }

    @Override
    public void invalidate(String region) throws GenericSystemException {
        Cache.removeRegion(region);
    }

    @Override
    public void invalidate(String region, Object key) throws GenericSystemException {
        Cache.put(region, key, null);
    }

    @Override
    public Collection search(String region, StorageFilter storageFilter) throws GenericSystemException {

        Collection filterResult = storageFilter.filter(Cache.getRegionContent(region));

        if (storageFilter.isExtractionRequiered()) {
            filterResult = storageFilter.extractValues(filterResult);
        }

        return filterResult;
    }

    @Override
    public Collection search(Collection<StorageFilter> storageFilters) throws GenericSystemException {
        Collection extractedValues = null;
        Collection filterResult = null;

        for (StorageFilter filter : storageFilters) {

            if (extractedValues != null) {
                filter.prepareCriterionValues(extractedValues);
            }

            filterResult = filter.filter(Cache.getRegionContent(filter.getRegionKey()));
            extractedValues = filter.extractValues(filterResult);
        }

        if (extractedValues != null && !extractedValues.isEmpty()) {
            filterResult = extractedValues;
        }

        return filterResult;
    }

    @Override
    public boolean containsObjects(Object region, StorageFilter filter) throws GenericSystemException {
//        filter.setFirstOnly(true);
        return !search((String) region, filter).isEmpty();
    }

    @Override
    public boolean containsObjects(Collection<StorageFilter> filters) throws GenericSystemException {
        StorageFilter last = null;
        for (StorageFilter filter : filters) {
            last = filter;
        }

        if (last != null) {
            last.setFirstOnly(true);
        }

        return !search(filters).isEmpty();
    }

    @Override
    public int getObjectCount(Object region, StorageFilter filter) throws GenericSystemException {
        return search((String) region, filter).size();
    }

    @Override
    public int getObjectCount(Collection<StorageFilter> filters) throws GenericSystemException {
        return search(filters).size();
    }
}
