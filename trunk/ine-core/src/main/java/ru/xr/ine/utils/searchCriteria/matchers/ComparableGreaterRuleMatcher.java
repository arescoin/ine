/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import ru.xr.ine.utils.searchCriteria.RuleMatcher;

/**
 * Простой мачер для сравнения, условие "больше". Применим исключитьельно для реализаций {@link Comparable}
 * Количество патернов: строго один
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ComparableGreaterRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ComparableGreaterRuleMatcher implements RuleMatcher {

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        Comparable val = (Comparable) value;
        return val.compareTo(pattern[0]) > 0;
    }

}
