/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.ipaddress;

import ru.xr.ine.core.Versionable;

/**
 * Описывает IP-адрес для работы в системе
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValue.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface IPAddressValue extends Versionable {

    /** Имя поля хранящего адрес */
    String IP_ADDRESS = "ipAddress";

    /**
     * Получает объект с адресом
     *
     * @return адрес
     */
    IPAddress getIpAddress();

    /**
     * Устанавливает адрес
     *
     * @param ipAddress адрес
     */
    void setIpAddress(IPAddress ipAddress);
}
