/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import java.io.Serializable;

/**
 * Простй маркерный интерфейс, предназначен для идентификации объекта не расширяющего Identifiable, но поддерживающих
 * соглашение об именовании для работы с фабрикой.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemMarker.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SystemMarker extends Serializable {
}
