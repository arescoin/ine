/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import ru.xr.ine.utils.searchCriteria.RuleMatcher;

/**
 * Простой мачер для поиска в диапазоне. Применим исключитьельно для реализаций {@link Comparable}
 * <p/>
 * Количество патернов: строго два
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ComparableBetweenRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ComparableBetweenRuleMatcher implements RuleMatcher {

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) throws ClassCastException {
        Comparable val = (Comparable) value;
        return val.compareTo(pattern[1]) <= 0 && val.compareTo(pattern[0]) >= 0;
    }

}
