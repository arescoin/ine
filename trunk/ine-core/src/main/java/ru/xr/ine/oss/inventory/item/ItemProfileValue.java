/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.Versionable;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Связка {@link ItemProfile} и {@link ValueDescriptor}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileValue.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ItemProfileValue extends Versionable {

    String ITEM_PROFILE_ID = "itemProfileId";
    String VALUE_DESCRIPTOR_ID = "valueDescriptorId";
    String PATTERN = "pattern";
    String REQUIRED = "required";


    BigDecimal getItemProfileId();

    void setItemProfileId(BigDecimal itemProfileId);

    BigDecimal getValueDescriptorId();

    void setValueDescriptorId(BigDecimal valueDescriptorId);

    SystemObjectPattern getPattern();

    void setPattern(SystemObjectPattern pattern);

    boolean isRequired();

    void setRequired(boolean pattern);

}
