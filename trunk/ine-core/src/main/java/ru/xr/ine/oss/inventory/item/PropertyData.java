/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyData.java 36 2017-04-06 00:05:15Z xerror $"
 */
public interface PropertyData<T> extends Versionable {

    String ITEM_ID = "itemId";
    String DESCRIPTOR_ID = "descriptorId";
    String DATA = "data";

    BigDecimal getItemId();

    void setItemId(BigDecimal itemId);

    BigDecimal getDescriptorId();

    void setDescriptorId(BigDecimal descriptorId);

    T getData();

    void setData(T data);

}
