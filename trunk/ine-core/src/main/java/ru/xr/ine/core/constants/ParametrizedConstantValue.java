/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Интерфейс описывает методы доступные для объекта-хранилища значения параметризованной константы
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValue.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ParametrizedConstantValue extends ConstantValue {

    String PARAM = "param";

    /**
     * Возвращает идентификатор параметра константы
     *
     * @return иденитфикатор параметра константы
     */
    BigDecimal getParam();

    /**
     * Возвращает идентификатор параметра константы
     *
     * @param param идентификатор параметра константы
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setParam(BigDecimal param) throws IneIllegalArgumentException;

}
