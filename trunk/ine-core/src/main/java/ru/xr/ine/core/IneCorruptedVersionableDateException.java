/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

/**
 * Исключение выбрасывается при обнаружении некорректных значений в датах версионного объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneCorruptedVersionableDateException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IneCorruptedVersionableDateException extends IneCorruptedStateException {

    public IneCorruptedVersionableDateException(String message) {
        super(message);
    }

}
