/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.Versionable;

/**
 * Интерфейс описывает системного пользователя
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemUser.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SystemUser extends Versionable {

    /** признак активности учетной записи пользователя */
    String ACTIVE = "active";
    /** имя пользователя под которым он входит в систему(системный логин) */
    String SYS_LOGIN = "login";
    /** хеш код от пароля пользователя */
    String MD5_HASH = "md5";

    /**
     * Возвращает признак активности учетной записи пользователя
     *
     * @return <b>true</b> - учетка активна, иначе - <b>false</b>
     */
    boolean isActive();

    /**
     * Устанавливает признак активности учетной записи пользователя
     *
     * @param active <b>true</b> - учетка активна, иначе - <b>false</b>
     */
    void setActive(boolean active);

    /**
     * Возвращает системное имя пользователя
     *
     * @return системное имя
     */
    String getLogin();

    /**
     * Устанавливает системное имя пользователя
     *
     * @param login системное имя
     */
    void setLogin(String login);

    /**
     * Возвращает хэш код пароля пользователя
     *
     * @return md5 hash code
     */
    String getMd5();

    /**
     * Устанавливает хэш код пароля пользователя
     *
     * @param md5 устнавливаемый хеш код
     */
    void setMd5(String md5);

}
