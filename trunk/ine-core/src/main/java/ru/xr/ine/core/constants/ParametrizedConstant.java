/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.constants;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Параметризованный конфигурационный параметр системы.
 * <p/>
 * Предоставляет возможности гибкой настройки значений по вариантам (паттерн системного объекта) применения.<br>
 * Применяется для настройки функциональности, изменения параметров работы программных модулей.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstant.java 29 2017-04-04 15:32:19Z xerror $"
 */

public interface ParametrizedConstant extends Constant {

    BigDecimal TYPE = new BigDecimal(2);

    String MASK = "mask";

    /**
     * Возвращает паттерн системного объекта для параметризации константы
     *
     * @return паттерн системного объекта для параметризации константы
     */
    SystemObjectPattern getMask();

    /**
     * Возвращает паттерн системного объекта для параметризации константы
     *
     * @param mask паттерн системного объекта для параметризации константы
     *
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setMask(SystemObjectPattern mask) throws IneIllegalArgumentException;

}
