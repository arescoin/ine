/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

/**
 * Выбрасывается при некорректном состоянии пользователя в системе.
 * <p/>
 * Основное предназначение - сигнализировать об отсутствие пользователя, либо о недостаточном наборе привилегий
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneIllegalAccessException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IneIllegalAccessException extends RuntimeCoreException {

    /**
     * Конструирует новое исключение с указанным сообщением
     *
     * @param message сообщение об ошибке
     */
    public IneIllegalAccessException(String message) {
        super(message);
    }

}
