/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

/**
 * Пакет содержит набор интерфейсов и абстрактных классов предоставляющих механизм конфигурирования системы.
 *
 * Механизм позволяет расширять основной набор и вводить дополнительные конфигурации и механизмы.
 *
 * Основой для пакета служит интрефейс {@link ru.xr.ine.utils.config.SystemConfig}.
 * Доступ к конфигурациям осуществляется через статический метод {@link ConfigurationManager#getManager()}.
 * Основная конфигурация инкапсулируется классом {@link ru.xr.ine.utils.config.RootConfig} доступна через метод
 * {@link ConfigurationManager#getRootConfig()}.
 *
 * Дополнительные конфигурации доступны через метод {@link ConfigurationManager#getConfigFor(String)}.
 *
 * @since 1.0
 * @SVNVersion "$Id: package-info.java 29 2017-04-04 15:32:19Z xerror $"
 */
package ru.xr.ine.utils.config;