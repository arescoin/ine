/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Описывает профиль элемента системы учета
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfile.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ItemProfile extends Versionable {

    String PROFILE_NAME = "name";
    String ITEM_TYPE = "itemType";

    String getName();

    void setName(String name);

    BigDecimal getItemType();

    void setItemType(BigDecimal itemType);
}
