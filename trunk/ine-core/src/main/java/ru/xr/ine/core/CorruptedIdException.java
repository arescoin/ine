/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

/**
 * Исключение выбрасывается при обнаружении некорректного идентификатора в процессе конструирования инстанса {@link
 * Identifiable}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CorruptedIdException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CorruptedIdException extends IneCorruptedStateException {

    /**
     * Конструирует инстанс исключения с указанием детального сообщения о причине
     *
     * @param message детализированное сообщение об ошибке
     */
    public CorruptedIdException(String message) {
        super(message);
    }
}
