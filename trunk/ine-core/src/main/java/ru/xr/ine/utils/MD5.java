/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Утилитный класс для получения md5 хэш кода
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: MD5.java 29 2017-04-04 15:32:19Z xerror $"
 */
public final class MD5 {

    private MD5() {
    }

    public static void main(String[] args) {
        if(args.length > 0 && args[0] != null){
            try {
                System.out.println(calculate(args[0]));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Вычисляет MD5-хэш для переданной строки
     *
     * @param source исходные данные для получения md5 hash code
     * @return md5 hash code
     * @throws NoSuchAlgorithmException если md5 не поддерживается в JVM
     * @throws NullPointerException при попытке получить MD5-хэш от <code>null</code
     */
    public static synchronized String calculate(String source) throws NoSuchAlgorithmException, NullPointerException {

        if (source == null) {
            throw new NullPointerException("Source string cant't be null");
        }

        MessageDigest algorithm = MessageDigest.getInstance("MD5");
        algorithm.reset();
        algorithm.update(source.getBytes());

        return new BigInteger(1, algorithm.digest()).toString(16);
    }
}
