/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.GenericSystemException;

/**
 * Выбрасывается если запрашиваемый пользователь неактивный
 *
 * @author Nikolay Ivshin
 * @version 1.0
 * @SVNVersion "$Id: UserInactiveException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserInactiveException extends GenericSystemException {

    public UserInactiveException(String message) {
        super(message);
    }

    public UserInactiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
