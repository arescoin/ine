/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria;

/**
 * Перечисление всех доступных правил
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CriteriaRule.java 29 2017-04-04 15:32:19Z xerror $"
 */
public enum CriteriaRule {
    /** Меньше */
    less,
    /** Больше */
    greater,
    /** Равно */
    equals,
    /** Диапазон */
    between,
    /** В числе */
    in,
    /** Начинасется с */
    startWith,
    /** Содержит */
    contains,
    /** знчение null */
    isNull
}
