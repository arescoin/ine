/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

/**
 * Сигнализирует о попытке модифицировать неактуальную версию объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneNotActualVersionModificationException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IneNotActualVersionModificationException extends GenericSystemException {

    private final Versionable currentVersion;

    /**
     * Конструирует инстанс, содержащий актуальную версию объекта
     *
     * @param versionable актуальная версия объекта, или его последняя реализация.
     * @param message     сообщение об ошибке
     */
    public IneNotActualVersionModificationException(Versionable versionable, String message) {
        super(message);
        this.currentVersion = versionable;
    }

    /**
     * Возвращает текущую версию объекта, послужившую причиной исключению
     *
     * @return текущая реализация
     */
    public Versionable getCurrentVersion() {
        return currentVersion;
    }

}
