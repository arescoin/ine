/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import java.util.Date;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataDate.java 41 2017-04-10 08:25:52Z xerror $"
 */
public interface PropertyDataDate extends PropertyData<Date> {

    @Override
    Date getData();

    @Override
    void setData(Date data);
}
