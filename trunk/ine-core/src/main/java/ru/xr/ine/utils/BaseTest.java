/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.userpermits.SystemUser;

import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: BaseTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class BaseTest {

    protected Logger logger = Logger.getLogger(getClass().getName());
    protected UserProfile user;

    static {

        System.setProperty("ru.xr.ine.utils.cache.CacheAccessRiMSImpl.STANDALONE", "true");

//        try {
//            Cache.invalidateAll();
//        } catch (GenericSystemException e) {
//            fail(e);
//        }

        /** Заглушка пользователя для прохождения автотестов */
        SystemUser systemUser = new SystemUser() {
            @Override
            public Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            @Override
            public boolean isActive() {
                return true;
            }

            @Override
            public void setActive(boolean active) {
            }

            @Override
            public void setCoreFd(Date fd) throws IneIllegalArgumentException {
            }

            @Override
            public Date getCoreFd() {
                return new Date();
            }

            @Override
            public void setCoreTd(Date td) throws IneIllegalArgumentException {
            }

            @Override
            public Date getCoreTd() {
                return new Date();
            }

            @Override
            public void setCoreId(BigDecimal id) throws IllegalIdException {
            }

            @Override
            public BigDecimal getCoreId() {
                return BigDecimal.ONE;
            }

            @Override
            public String getCoreDsc() {
                return "TEST User";
            }

            @Override
            public void setCoreDsc(String dsc) {
            }

            @Override
            public String getLogin() {
                return "root";
            }

            @Override
            public void setLogin(String login) {
            }

            @Override
            public String getMd5() {
                return null;
            }

            @Override
            public void setMd5(String md5) {
            }
        };

        UserHolder.setUser(systemUser);
        UserHolder.getUser().setCurrentRole(BigDecimal.ONE);

//        try {
//            Class accessFactory = Class.forName("ru.xr.ine.dbe.da.core.AccessFactory");
//            Method getImplentation = accessFactory.getMethod("getImplementation", Class.class);
//
//            Object access = getImplentation.invoke(accessFactory, SystemObject.class);
//            for (String intf : ((Map<BigDecimal, String>)
//                    access.getClass().getMethod("getInterfaces").invoke(access)).values()) {
//                try {
//                    Class clazz = Class.forName(intf);
//                    access = getImplentation.invoke(accessFactory, clazz);
//                    access.getClass().getMethod("getAllObjects").invoke(access);
//                } catch (Exception e) {
//                    System.out.println(e.getMessage());
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    protected BaseTest() {
        user = UserHolder.getUser();
        user.setLastAction(new UserProfile.Action(null, "for testing purposes"));
    }

    protected static void fail(Exception e) throws AssertionError {
        fail(e, true);
    }

    protected static void fail(Exception e, boolean logError) throws AssertionError {
        AssertionError ae = new AssertionError(e.getMessage());
        ae.initCause(e);
        if (logError) {
            e.printStackTrace();
        }
        throw ae;
    }
}
