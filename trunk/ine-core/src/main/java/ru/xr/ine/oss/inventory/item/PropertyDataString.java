/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataString.java 41 2017-04-10 08:25:52Z xerror $"
 */
public interface PropertyDataString extends PropertyData<String> {

    @Override
    String getData();

    @Override
    void setData(String data);
}
