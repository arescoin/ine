/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.links;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Значение конкретного линка. Для идентификации, помимо стандартных правил, необходимо учитывать идентификаторы
 * участников ассоциации
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: LinkData.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface LinkData extends Versionable {

    String LINK_ID = "linkId";
    String LEFT_ID = "leftId";
    String RIGHT_ID = "rightId";

    /**
     * Возвращает идентификатор описателя линка {@link ru.xr.ine.core.links.Link}
     *
     * @return идентификатор описателя линка
     */
    BigDecimal getLinkId();

    /**
     * Устанавливает идентификатор описателя линка {@link ru.xr.ine.core.links.Link}
     *
     * @param linkId идентификатор описателя линка
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setLinkId(BigDecimal linkId) throws IllegalIdException;

    /**
     * Возвращает идентификатор левого участника ассоциации
     *
     * @return идентификатор участника
     */
    BigDecimal getLeftId();

    /**
     * Устанавливает идентификатор левого участника ассоциации
     *
     * @param leftId идентификатор участника
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setLeftId(BigDecimal leftId) throws IllegalIdException;

    /**
     * Возвращает идентификатор правого участника ассоциации
     *
     * @return идентификатор участника
     */
    BigDecimal getRightId();

    /**
     * Устанавливает идентификатор правого участника ассоциации
     *
     * @param rightId идентификатор участника
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setRightId(BigDecimal rightId) throws IllegalIdException;
}
