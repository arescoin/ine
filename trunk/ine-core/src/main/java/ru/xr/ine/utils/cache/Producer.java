/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.cache;

import ru.xr.ine.core.CoreException;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Используется для извлечения значения подсистемой кэширования если этого значения не оказалось в кэше.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: Producer.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface Producer {
    static final Producer NULL_PRODUCER = new NullProducer();
    static final Producer HASH_SET_PRODUCER = new HashSetProducer();
    static final Producer HASH_MAP_PRODUCER = new HashMapProducer();

    /**
     * Получение объекта по ключу. Формат ключа и его использование возлагается на конкретные реализации интерфейса.
     *
     * @param key объект-ключ, по которому будет получен конкретный объект.
     * @return искомый объект
     * @throws CoreException в случае возникновения ошибок при получении объекта.
     */
    Object get(Object key) throws CoreException;

    static class NullProducer implements Producer {
        @Override
        public Object get(Object key) throws CoreException {
            return null;
        }
    }

    static class HashSetProducer implements Producer {
        @Override
        public Object get(Object key) throws CoreException {
            return new HashSet();
        }
    }

    static class HashMapProducer implements Producer {
        @Override
        public Object get(Object key) throws CoreException {
            return new HashMap();
        }
    }
}
