/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

/**
 * Пакет предоставляет набор интерфейсов для работы с подсистемой локализации.
 *
 * Данная подсистема предназначена для адаптации произвольных хранилищ ресурсов локализации
 * к стандартным Java-механизмам.
 *
 * @SVNVersion "$Id: package-info.java 29 2017-04-04 15:32:19Z xerror $"
 */
package ru.xr.ine.core.strings;
