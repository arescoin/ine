/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Описывает непосредственно элемент системы учета
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Item.java 41 2017-04-10 08:25:52Z xerror $"
 */
public interface Item extends Versionable {

    String ITEM_TYPE = "type";
    String SELF_NAME = "name";
    String PARENT_ID = "parentId";
    String ITEM_PROFILE = "itemProfile";
    String STATUS = "status";


    BigDecimal getType();

    void setType(BigDecimal type);

    BigDecimal getParentId();

    void setParentId(BigDecimal parentId);

    BigDecimal getItemProfile();

    void setItemProfile(BigDecimal itemProfile);

    BigDecimal getStatus();

    void setStatus(BigDecimal status);

    String getName();

    void setName(String name);
}
