/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Перечисление типов данных, используемых системой
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DataType.java 29 2017-04-04 15:32:19Z xerror $"
 */
public enum DataType {

    stringType(String.class),
    longType(Long.class),
    bigDecimalType(BigDecimal.class),
    dateType(Date.class),
    booleanType(Boolean.class);

    protected static Map<Integer, Class> map = new HashMap<Integer, Class>();

    private Class type;

    static {
        for (DataType dataType : values()) {
            map.put(dataType.ordinal(), dataType.getType());
        }
    }

    private DataType(Class type) {
        this.type = type;
    }

    /**
     * Возвращает класс представляемый данным типом
     *
     * @return java-класс
     */
    public Class getType() {
        return type;
    }

    /**
     * Возвращает тип описывающий java-класс, если данный класс не предусмотрен, вернется null
     *
     * @param aClass требуемый класс
     * @return системный тип
     */
    public static DataType getTypeByClass(Class aClass) {

        DataType result = null;
        for (Map.Entry<Integer, Class> integerClassEntry : map.entrySet()) {
            if (integerClassEntry.getValue().equals(aClass)) {
                result = DataType.getType(integerClassEntry.getKey());
                break;
            }
        }

        if (result == null) {
            if (long.class.isAssignableFrom(aClass)) {
                result = DataType.longType;
            } else if (boolean.class.isAssignableFrom(aClass)) {
                result = DataType.booleanType;
            }
        }

        return result;
    }

    /**
     * Возвращает java-класс по "коду" типа
     *
     * @param ordinal код типа (порядок в енумераторе)
     * @return java-класс
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public static Class getTypeClass(int ordinal) {
        return map.get(ordinal);
    }


    /**
     * Метод "мягко" проверяет и возвращает системный тип по его номеру в енаме,
     * если такого не существует, вернется null
     *
     * @param ordinal код типа (порядок в енумераторе)
     * @return системный тип
     */
    public static DataType getType(int ordinal) {
        DataType result = null;
        for (DataType dataType : values()) {
            if (dataType.ordinal() == ordinal) {
                result = dataType;
                break;
            }
        }

        return result;
    }

}
