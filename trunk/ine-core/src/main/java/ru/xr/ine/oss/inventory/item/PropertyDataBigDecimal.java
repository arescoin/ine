/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataBigDecimal.java 41 2017-04-10 08:25:52Z xerror $"
 */
public interface PropertyDataBigDecimal extends PropertyData<BigDecimal> {

    @Override
    BigDecimal getData();

    @Override
    void setData(BigDecimal data);
}
