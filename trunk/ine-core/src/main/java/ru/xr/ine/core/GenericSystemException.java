/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

/**
 * Обозначает общее системное исключеие.
 * <p/>
 * Предоставляет возможность дополнительного указания причины исключения не содержащейся в основной трассе.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: GenericSystemException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class GenericSystemException extends CoreException {

    private Throwable additionalException;

    public GenericSystemException(String message) {
        super(message);
    }

    public GenericSystemException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Конвертирует переданное исключение в формат системного
     *
     * @param message сообщение присваиваемое результату
     * @param source  источник
     * @return инстанс GenericSystemException
     */
    public static GenericSystemException toGeneric(String message, Throwable source) {

        if (source instanceof GenericSystemException) {
            return (GenericSystemException) source;
        }

        return new GenericSystemException(message, source);
    }

    /**
     * Конвертирует переданное исключение в формат системного, в качестве сообщения используется сообщение источника
     *
     * @param source источник
     * @return инстанс GenericSystemException
     */
    public static GenericSystemException toGeneric(Throwable source) {
        return toGeneric(source.getMessage(), source);
    }

    /**
     * Возвращает дополнительное исключение, если установлено, не содержащееся в основной трассе.
     * Как правило  - реальная причина исключения, используется при обработке при сетевом взаимодействие клиент-сервер
     *
     * @return исключение - причина
     */
    public Throwable getAdditionalException() {
        return additionalException;
    }

    /**
     * Устанавливает дополнительное исключение, не содержащееся в основной трассе.
     * Как правило  - реальная причина исключения, используется при обработке при сетевом взаимодействие клиент-сервер
     *
     * @param additionalException исключение - причина
     */
    public void setAdditionalException(Throwable additionalException) {
        this.additionalException = additionalException;
    }

}
