/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria;

import java.io.Serializable;

/**
 * Определяет методы для выполнения сопоставления значений с "эталонами", сравнения
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface RuleMatcher extends Serializable {

    /**
     * Выполняет проверку на соответствие патерну ("эталону")
     * в некоторых случаях, количество патернов может быть более одного
     *
     * @param value   сравниваемое значение
     * @param pattern эталон или их набор
     * @return результат проверки  true - положительный, иначе - fale
     */
    public boolean match(Object value, Object... pattern);
}
