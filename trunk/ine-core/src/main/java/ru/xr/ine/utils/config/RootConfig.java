/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.config;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Класс подготавливает систему конфигурирования.
 * В метод {@link ru.xr.ine.utils.config.RootConfig#init(String, String...)}
 * необходимо передать полный путь к файлу конфигурации.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RootConfig.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class RootConfig extends AbstractPropertiesBasedConfig {

    /** Ключ для имени приложения в файле конфигурации */
    public static final String APPLICATION_NAME_PROPERTY = "ru.xr.ine.utils.config.RootConfig.APPLICATION_NAME";
    /** Имя приложения по-умолчанию */
    public static final String DEFAULT_APPLICATION_NAME = "DEFAULT_APPLICATION_NAME";

    /** Глобальное имя приложения */
    private String applicationName;
    /** Карта подсистем */
    private final Map<String, Properties> sysMap = new HashMap<String, Properties>();

    @Override
    public void read() throws ConfigurationException {
        applicationName = getValueByKey(APPLICATION_NAME_PROPERTY, DEFAULT_APPLICATION_NAME);
        fillMap();
    }

    /**
     * Возвращает имя приложения для основной системы
     *
     * @return имя приложения
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Возвращает карту содержащую параметры инициализации подсистем
     *
     * @return карта, где ключ - всевдоним подсистемы, значение - параметры её инициализации
     */
    public Map<String, Properties> getSubSistemsProperties() {
        return Collections.unmodifiableMap(sysMap);
    }

    /** Заполняем карту конфигурации подсистем */
    private void fillMap() {
        String[] subSysNames;

        String subSystemsConfigs = getValueByKey(ru.xr.ine.utils.config.SystemConfig.class.getName());
        if (subSystemsConfigs != null && subSystemsConfigs.trim().length() > 0) {
            subSysNames = subSystemsConfigs.trim().split(",");
        } else {
            subSysNames = new String[0];
        }

        for (String subSysName : subSysNames) {
            subSysName = subSysName.trim();

            synchronized (sysMap) {
                if (!sysMap.containsKey(subSysName)) {
                    sysMap.put(subSysName, new Properties());
                }
            }

            String prefix = subSysName + ".";
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                String key = (String) entry.getKey();
                String val = (String) entry.getValue();

                if (key.startsWith(prefix)) {
                    sysMap.get(subSysName).setProperty(key.substring(prefix.length()), val);
                }
            }
        }
    }

}
