/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * интерфейс для описания модифицированного типа
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomType.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CustomType extends Versionable {

    String PARENT_TYPE = "parentTypeId";
    String TYPE_NAME = "typeName";
    String BASE_OBJECT = "baseObject";

    /**
     * Возвращает базовый интерфейс являющийся основый для модифицированного типа
     *
     * @return базовый интерфейс
     */
    BigDecimal getBaseObject();

    /**
     * Устанавливает базовый интерфейс являющийся основый для модифицированного типа
     *
     * @param baseObject базовый интерфейс
     * @throws ru.xr.ine.core.IneIllegalArgumentException если базовый интерфейс уже установлен, если значение null
     * или меньше {@link #MIN_ALLOWABLE_VAL минимально допустимого значения}
     */
    void setBaseObject(BigDecimal baseObject) throws IneIllegalArgumentException;

    /**
     * Возвращает Id родительского модифицированный типа
     * <p/>
     * метод возвращает null если тип не яляется потомком другого модифицированного типа
     *
     * @return Id
     */
    BigDecimal getParentTypeId();

    /**
     * Устанавливает Id родительского модифицированный типа
     *
     * @param parentId модифицированный тип
     * @throws IneIllegalArgumentException если базовый интерфейс не совпадает с базовым интерфейсом
     * родительского модифицированного типа
     */
    void setParentTypeId(BigDecimal parentId) throws IneIllegalArgumentException;

    /**
     * Возвращает имя для модифицированного типа, должно быть уникально среди модифицированных типов
     *
     * @return имя типа
     */
    String getTypeName();

    /**
     * Устанавливает имя для модифицированного типа, должно быть уникально среди модифициррованных типов
     *
     * @param typeName имя типа
     * @throws ru.xr.ine.core.IneIllegalArgumentException если значение null
     */
    void setTypeName(String typeName);

    /**
     * Возвращает коллекцию настраиваемых аттрибутов модфицированного типа
     *
     * @return коллекция настраиваемых аттрибутов
     */
    Collection<CustomAttribute> getCustomAttributes();

    /**
     * Устанавливает коллекцию настраиваемых аттрибутов модфицированного типа
     *
     * @param attributes коллекция настраиваемых аттрибутов
     * @throws ru.xr.ine.core.IneIllegalArgumentException если значение null
     */
    void setCustomAttributes(Collection<CustomAttribute> attributes) throws IneIllegalArgumentException;

}
