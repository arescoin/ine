/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.Versionable;

/**
 * Интерфейс содержит основной набор методов описывающих доступ
 * <p/>
 * Доступ содержит информацию о предоставленном праве на работу с той или иной функциональностью системы, может
 * содержать дополнительное ограничивающее (конкретизирующее) значение.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Permit.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface Permit extends Versionable {

    String NAME = "name";
    String MASK_VALUE = "maskValue";
    String VALUE_REQUIRED = "valueRequired";

    /**
     * Возвращает название доступа
     * <p/>
     * Используется для представления в GUI и тп.
     *
     * @return название
     */
    String getName();

    /**
     * Устанавливает название доступа
     * <p/>
     * Используется для представления в GUI и тп.
     *
     * @param name название
     * @throws ru.xr.ine.core.IneIllegalArgumentException если name - NULL или пустая строка
     */
    void setName(String name) throws IneIllegalArgumentException;

    /**
     * Возвращает значение маски применимости доступа
     *
     * @return значение маски применимости доступа
     */
    String getMaskValue();

    /**
     * Устанавливает значение маски применимости доступа
     *
     * @param maskValue значение маски применимости доступа
     */
    void setMaskValue(String maskValue);

    /**
     * Возвращает признак обязательности значения для данного доступа
     *
     * @return true - значение обязательно, иначе - false
     */
    boolean isValueRequired();

    /**
     * Устанавливает признак обязательности значения для данного доступа
     *
     * @param required true - значение обязательно, иначе - false
     */
    void setValueRequired(boolean required);
}
