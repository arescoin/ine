/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Представляет код страны в соответствии с iso3166
 * <p/>
 * Содержит: <ul> <li> код языка в стандарте <li> название языка <li> строковый код - 2 символа <li> строковый код - 3
 * символа </ul>
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CountryDetails.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CountryDetails extends Identifiable {

    String NAME = "name";
    String CODE_A2 = "codeA2";
    String CODE_A3 = "codeA3";

    /**
     * Возвращает название страны (iso3166)
     *
     * @return название страны (iso3166)
     */
    String getName();

    /**
     * Устанавливает название страны (iso3166)
     *
     * @param name название страны (iso3166)
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setName(String name) throws IneIllegalArgumentException;

    /**
     * Возвращает код страны (два символа iso3166)
     *
     * @return код страны (два символа iso3166)
     */
    String getCodeA2();

    /**
     * Устанавливает код страны (два символа iso3166)
     *
     * @param a2 код страны (два символа iso3166)
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setCodeA2(String a2) throws IneIllegalArgumentException;

    /**
     * Возвращает код страны (три символа iso3166)
     *
     * @return код страны (три символа iso3166)
     */
    String getCodeA3();

    /**
     * Устанавливает код страны (три символа iso3166)
     *
     * @param a3 код страны (три символа iso3166)
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setCodeA3(String a3) throws IneIllegalArgumentException;
}
