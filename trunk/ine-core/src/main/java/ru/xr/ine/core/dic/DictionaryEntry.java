/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.dic;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Предоставляет методы для описания словарной статьи
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntry.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface DictionaryEntry extends Versionable {

    String DIC_ID = "dictionaryId";
    String UP_TERM = "parentTermId";

    /**
     * Возвращает идентификатор словаря владеющего данной словарной статьей
     * <p/>
     * Метод {@link #getCoreId()} возвращает идентификатор (код) словарной статьи
     *
     * @return идентификатор словаря
     */
    BigDecimal getDictionaryId();

    /**
     * Устанавливает идентификатор словаря владеющего данной словарной статьей
     *
     * @param dictionaryId идентификатор словаря
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setDictionaryId(BigDecimal dictionaryId) throws IneIllegalArgumentException;

    /**
     * Возвращает код верхней словарной статьи
     *
     * @return код верхней словарной статьи
     */
    BigDecimal getParentTermId();

    /**
     * Возвращает код верхней словарной статьи
     *
     * @param upTerminCode код верхней словарной статьи
     */
    void setParentTermId(BigDecimal upTerminCode);

}
