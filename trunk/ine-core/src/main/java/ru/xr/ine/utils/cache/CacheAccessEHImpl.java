/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.cache;

/**
 * Нипалучилось работать с терракотой.
 * 
 * Реализация доступа к кешу с использованием <a href="http://ehcache.org/">Terracotta EhCache</a>
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CacheAccessEHImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CacheAccessEHImpl {/*implements CacheAccess {

    public static final String CACHE_CONF_URL = Cache.class.getPackage().getName() + ".ehCacheConfigURL";

    *//** Содержит имя региона хранящего регионы внешних клиентов кеша *//*
    public static final String CACHE_REGION = CacheAccessEHImpl.class.getName() + "_CACHE_REGION";

    private static Logger logger = Logger.getLogger(CacheAccessEHImpl.class.getName());

    private final CacheManager cacheManager;

    *//**
 * Создание экземпляра менеджера доступа к кешу без использования конфигурационного файла.
 * Конфигурация клиентской части получается от сервера, при подключении по адресам указанным в
 * параметре <b>ru.xr.ine.utils.cache.ehCacheConfigURL</b> конфигурации системы.
 *//*
    public CacheAccessEHImpl() {

        TerracottaConfigConfiguration tcc = new TerracottaConfigConfiguration();
        TerracottaConfiguration tc = new TerracottaConfiguration();
        CacheConfiguration cc = new CacheConfiguration();
        Configuration conf = new Configuration();

        tcc.setUrl(ConfigurationManager.getManager().getRootConfig().getValueByKey(CacheAccessEHImpl.CACHE_CONF_URL));
        cc.setEternal(false);
        cc.setMaxElementsInMemory(1);
//        cc.setOverflowToDisk(true);
        cc.addTerracotta(tc);
        conf.addTerracottaConfig(tcc);
        conf.addDefaultCache(cc);

        cacheManager = new CacheManager(conf);

        // регистрируем ркгион для учета остальных регионов
        if (!cacheManager.cacheExists(CACHE_REGION)) {
            try {
                cacheManager.addCache(CACHE_REGION);
            } catch (CacheException e) {
                if (logger.isLoggable(Level.WARNING)) {
                    logger.log(Level.WARNING, e.getMessage(), e);
                }
            }
        }

    }

    *//**
 * Проверка существования указанного региона. Если регион не задан в конфигурационном файле кеша (ehcache.xml), то
 * создаётся регион с характеристиками указанными в таге "<b>defaultCache</b>" (ehcache.xml):
 *
 * @param region имя региона
 *//*
    private void checkCache(String region) {
        synchronized (cacheManager) {
            if (!cacheManager.cacheExists(region)) {
                try {
                    cacheManager.addCache(region);
                    registerRegion(region);
                } catch (CacheException e) {
                    if (logger.isLoggable(Level.WARNING)) {
                        logger.log(Level.WARNING, e.getMessage(), e);
                    }
                }
            }
        }
    }

    *//**
 * Регистрирует имя "региона" в кеше
 *
 * @param region имя добавляемого региона
 *//*
    private void registerRegion(String region) {
        try {
            put(CACHE_REGION, region, "");
        } catch (GenericSystemException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public boolean containsRegion(String region) throws GenericSystemException {
        try {
            return cacheManager.cacheExists(region);
        } catch (IllegalStateException e) {
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    @Override
    public Object get(String region, Object key, Producer producer) throws CoreException {
        return get(region, key.toString(), producer, true);
    }

    @Override
    public Object get(String region, Object key, Producer producer, boolean block) throws CoreException {

        Object result = null;
        checkCache(region);
        ExplicitLockingCache cache;

        try {
            cache = new ExplicitLockingCache(cacheManager.getEhcache(region));
        } catch (Exception e) {
            throw new GenericSystemException(e.getMessage(), e);
        }

        String keyString = key.toString();
        try {
            if (block) {
                cache.acquireWriteLockOnKey(keyString);
            }

            if (!cache.isKeyInCache(keyString)) {
                result = producer.get(keyString);

                if (result != null) {
                    Element element = new Element(keyString, result);
                    try {
                        cache.put(element);
                    } catch (Exception e) {
                        if (logger.isLoggable(Level.WARNING)) {
                            logger.log(Level.WARNING, e.getMessage(), e);
                        }
                        throw new GenericSystemException(e.getMessage(), e);
                    }
                } //result != null
            } else {
                result = cache.get(keyString).getValue();
            }// !cache.isKeyInCache(keyString)

        } catch (IllegalStateException ise) {
            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, ise.getMessage(), ise);
            }
        } finally {
            if (block) {
                cache.releaseWriteLockOnKey(keyString);
            }
        }

        return result;
    }

    @Override
    public Object put(String region, Object key, Object value) throws GenericSystemException {
        return put(region, key.toString(), value, true);
    }

    @Override
    public Object put(String region, Object key, Object value, boolean block) throws GenericSystemException {
        checkCache(region);
        Element element;

        ExplicitLockingCache cache;
        try {
            cache = new ExplicitLockingCache(cacheManager.getEhcache(region));
        } catch (Exception e) {
            throw new GenericSystemException(e.getMessage(), e);
        }

        String keyString = key.toString();
        try {
            if (block) {
                cache.acquireWriteLockOnKey(keyString);
            }
            element = cache.get(keyString);
            cache.put(new Element(keyString, value));
        } catch (Exception e) {
            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
            throw new GenericSystemException(e.getMessage(), e);
        } finally {
            if (block) {
                cache.releaseWriteLockOnKey(keyString);
            }
        }

        return element != null ? element.getValue() : null;
    }

    @Override
    public void remove(String region, Object key) {
        remove(region, key.toString(), true);
    }

    @Override
    public void remove(String region, Object key, boolean block) {
        checkCache(region);
        ExplicitLockingCache cache;
        try {
            cache = new ExplicitLockingCache(cacheManager.getEhcache(region));
        } catch (Exception e) {
            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
            return;
        }

        String keyString = key.toString();
        try {
            if (block) {
                cache.acquireWriteLockOnKey(keyString);
            }
            cache.remove(keyString);
        } catch (Exception e) {
            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        } finally {
            if (block) {
                cache.releaseWriteLockOnKey(keyString);
            }
        }
    }

    @Override
    public void invalidateAll() {

        // В виду того, что на стороне клиента, при подключении его к серверу, не создаются автоматически регионы,
        // существует необходимость прописывать их локально ручками

        logger.log(Level.WARNING, "INVALIDATION OF ALL CACHES");
        try {
            net.sf.ehcache.Cache cache = cacheManager.getCache(CACHE_REGION);

            if (cache != null) {
                for (Object o : cache.getKeys()) {
                    String cacheName = (String) o;

                    if (!cacheManager.cacheExists(cacheName)) {
                        cacheManager.addCache(cacheName);
                    }
                    logger.log(Level.INFO, "INVALIDATION OF " + cacheName + " CACHE");

                    cacheManager.getCache((String) o).removeAll();
                }
            }

            logger.log(Level.WARNING, "INVALIDATION DONE");
        } catch (CacheException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
    }

    @Override
    public void invalidate(String region) {
        checkCache(region);
        synchronized (cacheManager.getCache(region)) {
            try {
                cacheManager.getCache(region).removeAll();
            } catch (Exception e) {
                if (logger.isLoggable(Level.WARNING)) {
                    logger.log(Level.WARNING, e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public void invalidate(String region, Object key) {
        remove(region, key.toString());
    }

    public void lockWrite(String region, Object key) throws GenericSystemException {
        checkCache(region);
        ExplicitLockingCache cache;
        try {
            cache = new ExplicitLockingCache(cacheManager.getEhcache(region));
            cache.acquireWriteLockOnKey(key);
        } catch (Exception e) {
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    public void unLockWrite(String region, Object key) throws GenericSystemException {
        checkCache(region);
        ExplicitLockingCache cache;
        try {
            cache = new ExplicitLockingCache(cacheManager.getEhcache(region));
            cache.releaseWriteLockOnKey(key);
        } catch (Exception e) {
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    public void lockRead(String region, Object key) throws GenericSystemException {
        checkCache(region);
        ExplicitLockingCache cache;
        try {
            cache = new ExplicitLockingCache(cacheManager.getEhcache(region));
            cache.acquireWriteLockOnKey(key);
        } catch (Exception e) {
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    public void unLockRead(String region, Object key) throws GenericSystemException {
        checkCache(region);
        ExplicitLockingCache cache;
        try {
            cache = new ExplicitLockingCache(cacheManager.getEhcache(region));
            cache.releaseWriteLockOnKey(key);
        } catch (Exception e) {
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    @Override
    public Collection search(String region, StorageFilter storageFilter) {
        return null;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Set<Object> getKeys(String region) throws CoreException {
        return Collections.EMPTY_SET;
    }

    @Override
    public Collection search(Collection<StorageFilter> storageFilters) throws GenericSystemException {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean containsObjects(Object region, StorageFilter filter) throws GenericSystemException {
        return false;
    }

    @Override
    public boolean containsObjects(Collection<StorageFilter> filters) throws GenericSystemException {
        return false;
    }

    @Override
    public int getObjectCount(Object region, StorageFilter filter) throws GenericSystemException {
        return 0;
    }

    @Override
    public int getObjectCount(Collection<StorageFilter> filters) throws GenericSystemException {
        return 0;
    }
*/
}
