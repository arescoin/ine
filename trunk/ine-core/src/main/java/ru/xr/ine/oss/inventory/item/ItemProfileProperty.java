/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

import ru.xr.ine.core.Versionable;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Связка {@link ItemProfile} и {@link PropertyDescriptor}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileProperty.java 59 2017-05-04 16:58:36Z xerror $"
 */
public interface ItemProfileProperty extends Versionable {

    String ITEM_PROFILE_ID = "itemProfileId";
    String PROPERTY_DESCRIPTOR_ID = "propertyDescriptorId";
    String DEFAULT_VALUE = "defaultValue";
    String PATTERN = "pattern";
    String REQUIRED = "required";
    String EDITABLE = "editable";


    BigDecimal getItemProfileId();

    void setItemProfileId(BigDecimal itemProfileId);

    BigDecimal getPropertyDescriptorId();

    void setPropertyDescriptorId(BigDecimal propertyDescriptorId);

    String getDefaultValue();

    void setDefaultValue(String value);

    SystemObjectPattern getPattern();

    void setPattern(SystemObjectPattern pattern);

    boolean isRequired();

    void setRequired(boolean pattern);

    boolean isEditable();

    void setEditable(boolean editable);

}
