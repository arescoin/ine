/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.language;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Представление системного языка.
 * <p/>
 * Является комбинацией "языка" (iso639) {@link ru.xr.ine.core.language.LangDetails} и "страны" (iso3166) {@link
 * ru.xr.ine.core.language.CountryDetails}.<br> Используется при локализации системных сообщений и словарей.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SysLanguage.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface SysLanguage extends Versionable {

    String LANG_DETAILS = "langDetails";
    String COUNTRY_DETAILS = "countryDetails";

    /**
     * Возвращает описание языка (iso639)
     *
     * @return описание языка (iso639)
     */
    BigDecimal getLangDetails();

    /**
     * Устанавливает описание языка (iso639)
     *
     * @param langDetails описание языка (iso639)
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setLangDetails(BigDecimal langDetails) throws IllegalIdException;

    /**
     * Возвращает описание страны (iso3166)
     *
     * @return описание языка (iso3166)
     */
    BigDecimal getCountryDetails();

    /**
     * Устанавливает описание страны (iso3166)
     *
     * @param countryDetails описание языка (iso3166)
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setCountryDetails(BigDecimal countryDetails) throws IllegalIdException;
}
