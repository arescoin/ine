/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.cache;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.rims.services.StorageFilter;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Интерфейс доступа к кешу. Предоставляет унифицированные методы для работы с кешом.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CacheAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface CacheAccess {

    /**
     * Проверяет наличие региона
     *
     * @param region регион кеша
     * @return если регион присутствуе - <b>true</b>, иначе - <b>false</b>
     * @throws GenericSystemException ошибка при проверке наличия региона
     */
    boolean containsRegion(String region) throws GenericSystemException;

    /**
     * Получает по ключу объект из указанного региона кеша. Выставляет блокировку
     *
     * @param region   регион кеша
     * @param key      ключ объекта
     * @param producer экземпляр класс {@link ru.xr.ine.utils.cache.Producer продюссера}, который используется для
     *                 получения объекта, если его не обнаружилось в кеше.
     * @return объект, содержимое кеша
     * @throws CoreException ошибка при извлечении объекта из кеша
     */
    Object get(String region, Object key, Producer producer) throws CoreException;

    /**
     * Получает по ключу объект из указанного региона кеша.
     *
     * @param region   регион кеша
     * @param key      ключ объекта
     * @param producer экземпляр класс {@link ru.xr.ine.utils.cache.Producer продюссера}, который используется для
     *                 получения объекта, если его не обнаружилось в кеше.
     * @param block    признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @return объект, содержимое кеша
     * @throws CoreException ошибка при извлечении объекта из кеша
     */
    Object get(String region, Object key, Producer producer, boolean block) throws CoreException;

    /**
     * Получает карту указанного региона
     *
     * @param region   регион кеша
     * @param producer экземпляр класс {@link ru.xr.ine.utils.cache.Producer продюссера}, который используется для
     *                 получения объекта, если его не обнаружилось в кеше.
     * @param block    признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @return содержимое региона
     * @throws CoreException ошибка при извлечении региона
     */
    public Map getRegionContent(String region, Producer producer, boolean block) throws CoreException;

    /**
     * Получает карту указанного региона
     *
     * @param region   регион кеша
     * @param producer экземпляр класс {@link ru.xr.ine.utils.cache.Producer продюссера}, который используется для
     *                 получения объекта, если его не обнаружилось в кеше.
     * @return содержимое региона
     * @throws CoreException ошибка при извлечении региона
     */
    public Map getRegionContent(String region, Producer producer) throws CoreException;


    /**
     * Получает по идентификатору региона кеша набор ключей в регионе.
     *
     * @param region идентификатор региона
     * @return набор ключей в регионе
     * @throws ru.xr.ine.core.CoreException ошибка при получении набора ключей
     */
    Set<Object> getKeys(String region) throws CoreException;

    /**
     * Помещает объект в указанный регион под заданным ключом.
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @param value  кешируемый объект
     * @return предыдущий закешированный объект
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    Object put(String region, Object key, Object value) throws GenericSystemException;

    /**
     * Помещает объекты в указанный регион.
     *
     * @param region регион кеша
     * @param values кешируемые объекты
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    void put(String region, Map values) throws GenericSystemException;

    /**
     * Помещает объект в указанный регион под заданным ключом.
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @param block  признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @param value  кешируемый объект
     * @return предыдущий закешированный объект
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    Object put(String region, Object key, Object value, boolean block) throws GenericSystemException;

    /**
     * Помещает объекты в указанный регион.
     *
     * @param region регион кеша
     * @param block  признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @param values кешируемые объекты
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    void put(String region, Map values, boolean block) throws GenericSystemException;

    /**
     * Удаляет из указанного региона кеша объект по его ключу
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @throws ru.xr.ine.core.GenericSystemException ошибка при удалении объекта из кеша
     */
    void remove(String region, Object key) throws GenericSystemException;

    /**
     * Удаляет из указанного региона кеша объект по его ключу
     *
     * @param region регион кеша
     * @param block  признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @param key    ключ объекта
     * @throws ru.xr.ine.core.GenericSystemException ошибка при удалении объекта из кеша
     */
    void remove(String region, Object key, boolean block) throws GenericSystemException;

    /**
     * Сбрасывает актуальность всего кеша.
     * <br>
     * <b>Метод не предназначен для прикладной разработки!</b>
     *
     * @throws ru.xr.ine.core.GenericSystemException при ошибке обращения к кешу
     */
    void invalidateAll() throws GenericSystemException;

    /**
     * Сбрасывает актуальность региона кеша. При последующем обращении к региону данные будут актуализированы.
     *
     * @param region регион кеша
     * @throws ru.xr.ine.core.GenericSystemException при ошибке обращения к кешу
     */
    void invalidate(String region) throws GenericSystemException;

    /**
     * Сбрасывает актуальность объекта в регионе кеша.
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @throws ru.xr.ine.core.GenericSystemException при ошибке обращения к кешу
     */
    void invalidate(String region, Object key) throws GenericSystemException;

    /**
     * Производит поиск объектов в кеше с применением переданного фильтра
     *
     * @param region        регион поиска
     * @param storageFilter фильтр, устанавливающий условия соответствия критериям поиска
     * @return результат поиска
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    Collection search(String region, StorageFilter storageFilter) throws GenericSystemException;

    /**
     * Производит комплексный поиск объектов в кеше с применением переданной коллекции фильтров
     *
     * @param storageFilters фильтры, устанавливающие условия соответствия критериям поиска
     * @return результат поиска
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    Collection search(Collection<StorageFilter> storageFilters) throws GenericSystemException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param region регион для поиска
     * @param filter фильтр объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    boolean containsObjects(Object region, StorageFilter filter) throws GenericSystemException;

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    boolean containsObjects(Collection<StorageFilter> filters) throws GenericSystemException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param region регион для поиска
     * @param filter фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    int getObjectCount(Object region, StorageFilter filter) throws GenericSystemException;

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.xr.ine.core.GenericSystemException ошибка в процессе поиска
     */
    int getObjectCount(Collection<StorageFilter> filters) throws GenericSystemException;
}
