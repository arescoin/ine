/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import ru.xr.ine.core.IneIllegalArgumentException;

import java.util.Date;

/**
 * Запись истории изменений версионного объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistory.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface VersionableHistory extends IdentifiableHistory {

    String FD = "fd";
    String TD = "td";

    /**
     * Устанавливает дату начала "жизни" версии объекта
     *
     * @param fd начало"жизни" версии объекта
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setFd(Date fd) throws IneIllegalArgumentException;

    /**
     * Возвращает дату начала "жизни" версии объекта
     *
     * @return начало "жизни" версии объекта
     */
    Date getFd();

    /**
     * Устанавливает дату завершения "жизни" версии объекта
     *
     * @param td окончание "жизни" версии объекта
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setTd(Date td) throws IneIllegalArgumentException;

    /**
     * Возвращает дату завершения "жизни" версии объекта
     *
     * @return окончание "жизни" версии объекта
     */
    Date getTd();

}
