/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

/**
 * Исключение выбрасывается при попытке выполнения операций с параметром, неудовлетворяющим предъявляемым к параметру
 * требованиям.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneIllegalArgumentException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IneIllegalArgumentException extends RuntimeCoreException {

    private String problemField;

    /**
     * Конструирует новое исключение с указанием детального сообщения о причине
     *
     * @param message детализация ошибки
     */
    public IneIllegalArgumentException(String message) {
        super(message);
    }

    /**
     * Конструирует новое исключение с указанием детального сообщения о причине и с указанием инициирующего исключения
     *
     * @param message детализация ошибки
     * @param cause   инициирующее исключение
     */
    public IneIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Конструирует новое исключение с указанием детального сообщения о причине и с
     * указанием поля объекта, вызвавшего проблеиу
     * 
     * @param message детализация ошибки
     * @param problemField - поле, вызвавшее проблему валидации
     */
    public IneIllegalArgumentException(String message, String problemField) {
        super(message);
        this.problemField = problemField;
    }

    public String getProblemField() {
        return problemField;
    }

}
