/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.links;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.Versionable;
import ru.xr.ine.core.structure.SystemObjectPattern;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Интерфейс содержит методы описания типа ассоциации.
 * <p/>
 * В ассоциации всегда принимает участие два объекта. Участие в ассоциации может быть как равноправное так и зависимое.
 * Сама ассоциация не определяет роль участников, тем не менее, при создании зависимой ассоциации небоходимо учитывать
 * что зависимый объект размещается правее.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Link.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface Link extends Versionable {

    PatternComparator PATTERN_COMPARATOR = new PatternComparator();

    String LEFT_TYPE = "leftType";
    String RIGHT_TYPE = "rightType";
    String TYPE = "typeId";

    /**
     * Возвращает паттерн, по которому будет идентифицироваться объект левого плеча связи.
     * <p>
     * В вырожденном случае, представляет собой идентификатор столбца таблицы, значение которого будет в связи.
     *
     * @return паттерн, по которому будет идентифицироваться объект левого плеча связи
     */
    SystemObjectPattern getLeftType();

    /**
     * Устанавливает паттерн, по которому будет идентифицироваться объект левого плеча связи
     *
     * @param leftType паттерн, по которому будет идентифицироваться объект левого плеча связи
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setLeftType(SystemObjectPattern leftType) throws IneIllegalArgumentException;

    /**
     * Возвращает паттерн, по которому будет идентифицироваться объект правого плеча связи
     * <p>
     * В вырожденном случае, представляет собой идентификатор столбца таблицы, значение которого будет в связи.
     *
     * @return паттерн, по которому будет идентифицироваться объект правого плеча связи
     */
    SystemObjectPattern getRightType();

    /**
     * Устанавливает паттерн, по которому будет идентифицироваться объект правого плеча связи
     *
     * @param rightType паттерн, по которому будет идентифицироваться объект правого плеча связи
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setRightType(SystemObjectPattern rightType) throws IneIllegalArgumentException;

    /**
     * Возвращает код словарной статьи указывающей тип ассоциации (ее кратность) {@link
     * ru.xr.ine.core.dic.DictionaryEntry}
     *
     * @return тип ассоциации
     */
    BigDecimal getTypeId();

    /**
     * Задает код словарной статьи указывающей тип ассоциации (ее кратность) {@link
     * ru.xr.ine.core.dic.DictionaryEntry}
     *
     * @param typeId тип ассоциации
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setTypeId(BigDecimal typeId) throws IneIllegalArgumentException;

    /**
     * Класс-компаратор для поиска по полям {@link ru.xr.ine.core.links.Link#LEFT_TYPE} и
     * {@link ru.xr.ine.core.links.Link#RIGHT_TYPE}
     */
    class PatternComparator implements Comparator, Serializable {
        @Override
        public int compare(Object o1, Object o2) {
            return ((SystemObjectPattern) o1).getTableId().compareTo((BigDecimal) o2);
        }
    }
}
