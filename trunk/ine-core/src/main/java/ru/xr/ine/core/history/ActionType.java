/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import ru.xr.ine.core.IneIllegalArgumentException;

import java.io.Serializable;

/**
 * Перечисления типов действий
 *
 * @SVNVersion "$Id: ActionType.java 29 2017-04-04 15:32:19Z xerror $"
 */
public enum ActionType implements Serializable {

    /* Создание*/
    create,
    /** Модификация */
    update,
    /** Удаление */
    delete;

    /**
     * Утилитный метод, по переданному коду возвращает значение enum
     *
     * @param code числовой код типа
     * @return enum-значение
     * @throws IneIllegalArgumentException при передаче в метод числового значения несуществующего кода
     */
    public static ActionType getType(int code) {

        switch (code) {
        case 0:
            return create;
        case 1:
            return update;
        case 2:
            return delete;
        default:
            throw new IneIllegalArgumentException("Incorrect type code [" + code + "]");
        }
    }

}
