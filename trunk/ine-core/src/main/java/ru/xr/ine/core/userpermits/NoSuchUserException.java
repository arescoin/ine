/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.GenericSystemException;

/**
 * Выбрасывается если запрашиваемый пользователь не найден
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: NoSuchUserException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class NoSuchUserException extends GenericSystemException {

    public NoSuchUserException(String message) {
        super(message);
    }

    public NoSuchUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
