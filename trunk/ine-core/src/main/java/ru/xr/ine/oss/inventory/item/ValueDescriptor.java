/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.oss.inventory.item;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ValueDescriptor.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface ValueDescriptor extends PropertyDescriptor {

    String NAME = "name";
    String TYPE = "type";
}
