/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.dic;

import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Предоставляет методы для описания термина словарной статьи
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTerm.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface DictionaryTerm extends DictionaryEntry {

    String LANG = "langCode";
    String TERM = "term";

    /**
     * Возвращает системный номер языка словарной статьи
     *
     * @return системный номер языка
     */
    BigDecimal getLangCode();

    /**
     * Устанавливает системный номер языка словарной статьи
     *
     * @param langCode системный номер языка
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setLangCode(BigDecimal langCode) throws IneIllegalArgumentException;

    /**
     * Возвращает значение словарной статьи (термин)
     *
     * @return термин
     */
    String getTerm();

    /**
     * Устанавливает значение словарной статьи (термин)
     *
     * @param term термин
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setTerm(String term) throws IneIllegalArgumentException;

}
