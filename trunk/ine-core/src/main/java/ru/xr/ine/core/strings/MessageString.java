/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.strings;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.SystemMarker;

import java.math.BigDecimal;

/**
 * Интерфейс описывает локализационный ресурс.
 * <p/>
 * В данном случае это строка имеющая уникальный ключ и код локали.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: MessageString.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface MessageString extends SystemMarker {

    String KEY = "key";
    String LANG = "languageCode";
    String MSG = "message";

    /**
     * Возвращает уникальный строковый ключ (идентификатор) для данного локализационного ресурса
     *
     * @return ключ ресурса
     */
    String getKey();

    /**
     * Задает уникальный строковый ключ (идентификатор) для данного локализационного ресурса
     *
     * @param key ключ ресурса
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setKey(String key) throws IneIllegalArgumentException;

    /**
     * Возвращает системный идентификатор языка
     *
     * @return код системного языка
     */
    BigDecimal getLanguageCode();

    /**
     * Задает системный идентификатор языка
     *
     * @param languageCode код системного языка
     * @throws ru.xr.ine.core.IllegalIdException при попытке установить null или некорректное значение
     */
    void setLanguageCode(BigDecimal languageCode) throws IllegalIdException;

    /**
     * Возвращает локализованный строковый ресурс
     *
     * @return локализованная строка
     */
    String getMessage();

    /**
     * Задает локализованный строковый ресурс
     *
     * @param message локализованная строка
     */
    void setMessage(String message);
}
