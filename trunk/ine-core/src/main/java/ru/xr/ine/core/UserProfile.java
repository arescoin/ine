/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import ru.xr.ine.core.userpermits.CrudCode;
import ru.xr.ine.core.userpermits.SystemUser;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Профайл текущего пользователя. Хранит в себе системного пользователя и набор его атрибутов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: UserProfile.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserProfile implements Serializable {
    /**
     * Системный номер таблицы, для которой регистрируется действие. Будет использоваться в том случае, если не
     * получится получить номер штатными средствами. Нужно в основном для значений произвольных атрибутов.
     */
    public static final String TABLE_ID = "TABLE_ID";

    /** ключик для хранения действия */
    private static final String LAST_USR_ACTION_KEY = "LAST_USR_ACTION_KEY";

    /** ключик для хранения обоснования действия */
    private static final String LAST_USR_REASON_KEY = "LAST_USR_REASON_KEY";

    /** ключик для хранения идентификатора текущей роли */
    private static final String CURRENT_ROLE_KEY = "CURRENT_ROLE_KEY";

    private final SystemUser systemUser;

    /** Системные атрибуты. Обрабатываются конкретными подсистемами. */
    private final HashMap<Object, Object> attributes = new HashMap<Object, Object>();

    /** Пользовательские атрибуты. Обрабатываются пользователем. */
    private final HashMap<String, String> userAttributes = new HashMap<String, String>();

    /**
     * Конструирует профайл для пользователя
     *
     * @param systemUser системный пользователь
     */
    public UserProfile(SystemUser systemUser) {
        if (systemUser == null) {
            throw new IneIllegalArgumentException("SystemUser can't be null");
        }

        this.systemUser = systemUser;
    }

    /**
     * Возвращает немодифицируемый объект системного пользователя.
     * <p/>
     * При любой попытке внесения изменений в свойства системного пользователя будет выброшено исключение
     * {@link UnsupportedOperationException}
     *
     * @return системный пользователь
     */
    public SystemUser getSystemUser() {
        return new SysUserWrapper(systemUser);
    }

    /**
     * Добавляет атрибут и его значение к свойствам профайла.
     * <p/>
     * Если такой атрибут уже имеется, то свойство добавлено не будет, о чем просигнализирует возвращаемый параметр.
     * Метод синхронизирован.
     *
     * @param key   ключ для размещения
     * @param value значение свойства
     * @return признак добавления true - атрибут добавлен, иначе - false
     */
    public boolean addAttribute(Object key, Object value) {
        boolean result = false;

        synchronized (attributes) {
            if (!attributes.containsKey(key)) {
                attributes.put(key, value);
                result = true;
            }
        }

        return result;
    }

    /**
     * Устанавливает значение атрибута профайла, при наличии ранее установленного атрибута метод вернет его значение.
     * <p/>
     * Метод синхронизирован.
     *
     * @param key   ключ для размещения
     * @param value значение атрибута
     * @return старое значение атрибута
     */
    public Object setAttribute(Object key, Object value) {
        synchronized (attributes) {
            return attributes.put(key, value);
        }
    }

    /**
     * Удалает атрибут из профайла пользователя
     * <p/>
     * Метод синхронизирован.
     *
     * @param key ключ для поиска атрибута
     * @return прежнее значение, при его налии, иначе - null
     */
    public Object removeAttribute(Object key) {
        synchronized (attributes) {
            return attributes.remove(key);
        }
    }

    /**
     * Получает значение атрибута профайла пользователя
     *
     * @param key ключ для поиска атрибута
     * @return значение атрибута
     */
    public Object getAttribute(Object key) {
        return attributes.get(key);
    }

    /**
     * Получает немодифицируемую карту, содержащую все атрибуты профайла
     *
     * @return карта атрибутов
     */
    public Map<Object, Object> getAttributesMap() {
        return Collections.unmodifiableMap(attributes);
    }

    public BigDecimal getCurrentRole() {
        return (BigDecimal) attributes.get(CURRENT_ROLE_KEY);
    }

    public void setCurrentRole(BigDecimal currentRole) {
        attributes.put(CURRENT_ROLE_KEY, currentRole);
    }

    /**
     * Возвращает обоснование последнего действия пользователя
     *
     * @return строка с обоснованием
     */
    public String getReason() {
        return (String) attributes.get(LAST_USR_REASON_KEY);
    }

    /**
     * Устанавливает обоснование последнего действия пользователя
     *
     * @param reason строка с обоснованием
     */
    public void setReason(String reason) {
        attributes.put(LAST_USR_REASON_KEY, reason);
    }

    /**
     * Возвращает объект, содержащий краткие характеристики активного действия пользователя
     *
     * @return действие пользователя
     */
    public Action getLastAction() {
        return (Action) attributes.get(LAST_USR_ACTION_KEY);
    }

    /**
     * Устанавливает последнее активное действие пользователя
     *
     * @param action действие
     */
    public void setLastAction(Action action) {
        attributes.put(LAST_USR_ACTION_KEY, action);
    }

    /**
     * Добавляет пользовательский атрибут и его значение в профайл.
     * <p/>
     * Если такой атрибут уже имеется, то свойство добавлено не будет, о чем просигнализирует возвращаемый параметр.
     * Метод синхронизирован.
     *
     * @param key   ключ для размещения
     * @param value значение свойства
     * @return признак добавления true - атрибут добавлен, иначе - false
     */
    public boolean addUserAttribute(String key, String value) {
        boolean result = false;

        synchronized (userAttributes) {
            if (!userAttributes.containsKey(key)) {
                userAttributes.put(key, value);
                result = true;
            }
        }

        return result;
    }

    /**
     * Устанавливает значение пользовательского атрибута в профайл,
     * при наличии ранее установленного атрибута метод вернет его значение.
     * <p/>
     * Метод синхронизирован.
     *
     * @param key   ключ для размещения
     * @param value значение атрибута
     * @return старое значение атрибута
     */
    public String setUserAttribute(String key, String value) {
        synchronized (userAttributes) {
            return userAttributes.put(key, value);
        }
    }

    /**
     * Удалает пользовательский атрибут из профайла
     * <p/>
     * Метод синхронизирован.
     *
     * @param key ключ для поиска атрибута
     * @return прежнее значение, при его налии, иначе - null
     */
    public String removeUserAttribute(String key) {
        synchronized (userAttributes) {
            return userAttributes.remove(key);
        }
    }

    /**
     * Удаляет все пользовательские атрибуты из профайла
     * <p/>
     * Метод синхронизирован.
     */
    public void clearUserAttributes() {
        synchronized (userAttributes) {
            userAttributes.clear();
        }
    }

    /**
     * Получает значение пользовательского атрибута из профайла
     *
     * @param key ключ для поиска атрибута
     * @return значение атрибута
     */
    public String getUserAttribute(String key) {
        return userAttributes.get(key);
    }

    /**
     * Получает немодифицируемую карту, содержащую все пользовательские атрибуты профайла
     *
     * @return карта пользовательских атрибутов
     */
    public Map<String, String> getUserAttributesMap() {
        return Collections.unmodifiableMap(userAttributes);
    }

    @Override
    public String toString() {

        if (systemUser != null) {
            return this.getClass().getSimpleName() + ": coreId[" + systemUser.getCoreId()
                    + "]; login[" + systemUser.getLogin() + "]";
        }

        return this.getClass().getSimpleName();
    }

    /** Врапер только на чтение */
    private static final class SysUserWrapper implements SystemUser {

        private final static UnsupportedOperationException EXCEPTION =
                new UnsupportedOperationException("Modification for current user not allowed");

        private final SystemUser systemUser;


        private SysUserWrapper(SystemUser systemUser) {
            if (systemUser == null) {
                throw new IneIllegalArgumentException("SystemUser can't be null");
            }
            this.systemUser = systemUser;
        }

        @Override
        public boolean isActive() {
            return this.systemUser.isActive();
        }

        @Override
        public void setActive(boolean active) {
            throw EXCEPTION;
        }

        @Override
        public Date getCoreFd() {
            return this.systemUser.getCoreFd();
        }

        @Override
        public void setCoreFd(Date fd) throws IneIllegalArgumentException {
            throw EXCEPTION;
        }

        @Override
        public Date getCoreTd() {
            return this.systemUser.getCoreTd();
        }

        @Override
        public void setCoreTd(Date td) throws IneIllegalArgumentException {
            throw EXCEPTION;
        }

        @Override
        public BigDecimal getCoreId() {
            return this.systemUser.getCoreId();
        }

        @Override
        public void setCoreId(BigDecimal id) throws IllegalIdException {
            throw EXCEPTION;
        }

        @Override
        public String getCoreDsc() {
            return this.systemUser.getCoreDsc();
        }

        @Override
        public void setCoreDsc(String dsc) {
            throw EXCEPTION;
        }

        @Override
        public int hashCode() {
            return this.systemUser.hashCode();
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof SystemUser && this.systemUser.equals(obj);
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        @Override
        public String toString() {
            return this.systemUser.toString();
        }

        @Override
        public String getLogin() {
            return this.systemUser.getLogin();
        }

        @Override
        public void setLogin(String login) {
            throw EXCEPTION;
        }

        @Override
        public String getMd5() {
            return this.systemUser.getMd5();
        }

        @Override
        public void setMd5(String md5) {
            throw EXCEPTION;
        }
    }


    /** Контейнер для хранения краткого описания действия пользователя */
    public static final class Action implements Serializable {

        private CrudCode type;
        private final String reason;
        private final Date date;

        /**
         * Создается новый описатель действия. Дата выставляется автоматом, в момент создания
         *
         * @param type   тип ативного действия
         * @param reason обоснование действия пользователя
         */
        public Action(CrudCode type, String reason) {
            this.type = type;
            this.reason = reason;
            this.date = new Date();
        }

        /**
         * Возвращает тип действия
         *
         * @return тип
         */
        public CrudCode getType() {
            return type;
        }

        /**
         * Возвращает обоснование пользователя
         *
         * @return обоснование
         */
        public String getReason() {
            return reason;
        }

        /**
         * Возвращает дату действия
         *
         * @return дата
         */
        public Date getDate() {
            return date;
        }

        /**
         * Устанавливает тип действия
         *
         * @param type тип действий
         */
        public void setType(CrudCode type) {
            this.type = type;
        }

    }

}
