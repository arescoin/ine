/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.funcswitch;

import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IneIllegalArgumentException;

/**
 * Название переключателя состояния функциональности
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchName.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface FuncSwitchName extends Identifiable {

    String NAME = "name";

    /**
     * Возвращет название переключателя функциональности.
     * <p/>
     * Имя переключателя должно быть уникальным, в случае невозможности соблюдения данного требования, используя только
     * имя пакета, можно дополнить имя переключателя именем домена.
     *
     * @return название переключателя функциональности
     */
    String getName();

    /**
     * Устанавливает название переключателя функциональности.
     * <p/>
     * Имя переключателя должно быть уникальным, в случае невозможности соблюдения данного требования, используя только
     * имя пакета, можно дополнить имя переключателя именем домена.
     *
     * @param name название переключателя функциональности
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setName(String name) throws IneIllegalArgumentException;

}
