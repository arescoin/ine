/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.history;

import ru.xr.ine.core.SyntheticId;

import java.math.BigDecimal;

/**
 * Класс является врапером синтетика для выполнения поисковых запросов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SearchId.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SearchId extends SyntheticId {

    /**
     * Устанавливает элемент композитного идентификатора в поисковое значение
     *
     * @param searchId идентификатор
     */
    public void setSearchEntry(BigDecimal searchId) {
        this.setValue(searchId, BigDecimal.ZERO);
    }

    @Override
    public ru.xr.ine.core.history.SearchId getSearchId() {
        throw new IllegalStateException("Not allowed here");
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        for (BigDecimal key : this.getIdKeys()) {
            BigDecimal val = this.getIdValue(key);
            String value;
            if (BigDecimal.ZERO.equals(val)) {
                value = "%";
            } else {
                value = val.toString();
            }
            stringBuffer.append("[").append(key).append(":").append(value).append("]");
        }

        return stringBuffer.toString();
    }
}
