/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.cache;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.rims.services.StorageFilter;

import java.util.*;

/**
 * Реализация кеша для целей разработки на основе обычных HashMap'ов.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CacheAccessDevImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class CacheAccessDevImpl implements CacheAccess {
    private static final Map<String, Map<Object, Object>> cacheMap =
            Collections.synchronizedMap(new HashMap<String, Map<Object, Object>>(100));

    private Map<Object, Object> getRegion(String region) {
        Map<Object, Object> result = cacheMap.get(region);
        if (result == null) {
            result = Collections.synchronizedMap(new HashMap<Object, Object>(1000));
            cacheMap.put(region, result);
        }

        return result;
    }

    @Override
    public boolean containsRegion(String region) throws GenericSystemException {
        return cacheMap.containsKey(region);
    }

    @Override
    public Object get(String region, Object key, Producer producer) throws CoreException {
        Map<Object, Object> regionMap;
        Object result;
        synchronized (cacheMap) {
            regionMap = getRegion(region);
            result = regionMap.get(key);
            if (result == null) {
                result = producer.get(key);
                regionMap.put(key, result);
            }
        }
        return result;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Map getRegionContent(String region, Producer producer) throws CoreException {
        return getRegionContent(region, producer, true);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Map getRegionContent(String region, Producer producer, boolean block) throws CoreException {

        Map<Object, Object> regionMap;

        synchronized (cacheMap) {
            regionMap = getRegion(region);
            if (regionMap == null) {
                regionMap = (Map<Object, Object>) producer.get(region);
            }
        }

        return regionMap;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void put(String region, Map values) throws GenericSystemException {
        getRegion(region).putAll(values);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void put(String region, Map values, boolean block) throws GenericSystemException {
        getRegion(region).putAll(values);

    }

    @Override
    public Object get(String region, Object key, Producer producer, boolean block) throws CoreException {
        return get(region, key, producer);
    }

    @Override
    public Object put(String region, Object key, Object value) throws GenericSystemException {
        Object result;
        synchronized (cacheMap) {
            result = getRegion(region).put(key, value);
        }
        return result;
    }

    @Override
    public void remove(String region, Object key) {
        Map<Object, Object> regionMap;
        synchronized (cacheMap) {
            regionMap = cacheMap.get(region);
            if (regionMap == null) {
                getRegion(region);
            } else {
                regionMap.remove(key);
            }
        }
    }

    @Override
    public void invalidateAll() {
        synchronized (cacheMap) {
            cacheMap.clear();
        }
    }

    @Override
    public void invalidate(String region) {
        Map<Object, Object> regionMap;
        synchronized (cacheMap) {
            regionMap = cacheMap.get(region);
            if (regionMap != null) {
                cacheMap.put(region, Collections.synchronizedMap(new HashMap<Object, Object>(1000)));
            }
        }
    }

    @Override
    public Object put(String region, Object key, Object value, boolean block) throws GenericSystemException {
        return put(region, key, value);
    }

    @Override
    public void remove(String region, Object key, boolean block) {
        remove(region, key);
    }

    @Override
    public void invalidate(String region, Object key) {
        remove(region, key);
    }

    @Override
    public Collection search(String region, StorageFilter storageFilter) {
        return null;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Set<Object> getKeys(String region) throws CoreException {
        return Collections.EMPTY_SET;
    }

    @Override
    public Collection search(Collection<StorageFilter> storageFilters) throws GenericSystemException {
        return Collections.EMPTY_SET;
    }

    @Override
    public boolean containsObjects(Object region, StorageFilter filter) throws GenericSystemException {
        return false;
    }

    @Override
    public boolean containsObjects(Collection<StorageFilter> filters) throws GenericSystemException {
        return false;
    }

    @Override
    public int getObjectCount(Object region, StorageFilter filter) throws GenericSystemException {
        return 0;
    }

    @Override
    public int getObjectCount(Collection<StorageFilter> filters) throws GenericSystemException {
        return 0;
    }
}
