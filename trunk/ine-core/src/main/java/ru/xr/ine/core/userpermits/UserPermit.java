/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Описатель доступа для конкретного пользователя
 * <p/>
 * Метод {@link #getCoreId()} возвращает идентификатор доступа
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: UserPermit.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface UserPermit extends Versionable {

    String USER_ID = "userId";
    String ROLE_ID = "roleId";
    String VALUES = "values";
    String ACTIVE = "active";
    String CRUD_MASK = "crudMask";

    /**
     * Возвращает идентификатор системного пользователя, обладателя доступа
     *
     * @return идентификатор пользователя
     */
    BigDecimal getUserId();

    /**
     * Устанавливает идентификатор системного пользователя, обладателя доступа
     *
     * @param userId идентификатор пользователя
     * @throws ru.xr.ine.core.IllegalIdException при попытке передачи в качестве параметра пустой ссылки, пустого
     * значения, или значения не удовлетворяющего требованиям к идентификатору
     */
    void setUserId(BigDecimal userId) throws IllegalIdException;

    /**
     * Возвращает идентификатор роли настраеваемой для пользователя
     *
     * @return идентификатор роли
     */
    BigDecimal getRoleId();

    /**
     * Устанавливает идентификатор роли настраеваемой для пользователя
     *
     * @param roleId идентификатор роли
     * @throws ru.xr.ine.core.IllegalIdException при попытке передачи в качестве параметра пустой ссылки, пустого
     * значения, или значения не удовлетворяющего требованиям к идентификатору
     */
    void setRoleId(BigDecimal roleId) throws IllegalIdException;

    /**
     * Возвращает список значений для доступа
     *
     * @return список значений доступа
     */
    BigDecimal[] getValues();

    /**
     * Устанавливает список значений для доступа
     *
     * @param value список значений доступа
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передачи в качестве параметра пустой ссылки,
     * пустого значения.
     */
    void setValues(BigDecimal[] value) throws IneIllegalArgumentException;

    /**
     * Возвращает признак активности данного доступа
     * <p/>
     * Для упращения работы с функционалом, доступ можно временно деактивиовать
     *
     * @return признак активности данного доступа
     */
    boolean isActive();

    /**
     * Устанавливает признак активности доступа
     * <p/>
     * Для упращения работы с функционалом, доступ можно временно деактивиовать
     *
     * @param active признак активности доступа
     */
    void setActive(boolean active);

    /**
     * Возвращает маску CRUD
     *
     * @return код(ы) "бинарного словаря"
     */
    int getCrudMask();

    /**
     * Устанавливает маску CRUD
     *
     * @param crudMask код(ы) "бинарного словаря"
     */
    void setCrudMask(int crudMask);

}
