/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.userpermits;

import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Интерфейс описывает набор методов, описывающих конфигурацию роли
 * <p/>
 * Метод {@link #getCoreId()} возвращает идентификатор роли
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RolePermit.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface RolePermit extends Versionable {

    String PERMIT_ID = "permitId";
    String PERMIT_VALUES = "permitValues";
    String CRUD_MASK = "crudMask";

    /**
     * Возвращает идентификатор доступа включенного в данную роль
     *
     * @return идентификатор доступа
     */
    BigDecimal getPermitId();

    /**
     * Устанавливает идентификатор доступа включенного в данную роль
     *
     * @param permitId идентификатор доступа
     * @throws ru.xr.ine.core.IllegalIdException при попытке передачи в качестве параметра пустой ссылки, пустого
     * значения, или значения не удовлетворяющего требованиям к идентификатору
     */
    void setPermitId(BigDecimal permitId) throws IllegalIdException;

    /**
     * Возвращает список значений доступа включенного в данную роль
     *
     * @return список значений доступа
     */
    BigDecimal[] getPermitValues();

    /**
     * Устанавливает список значений доступа включенного в данную роль
     *
     * @param permitValue список значений доступа
     */
    void setPermitValues(BigDecimal[] permitValue);


    /**
     * Возвращает код CRUD
     *
     * @return код(ы) "бинарного словаря"
     */
    int getCrudMask();

    /**
     * Устанавливает код CRUD
     *
     * @param crudMask код(ы) "бинарного словаря"
     */
    void setCrudMask(int crudMask);
}
