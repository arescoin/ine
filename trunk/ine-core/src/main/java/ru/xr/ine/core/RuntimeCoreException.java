/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

/**
 * Корневой класс исключения для всех <i>Runtime</i> исключений в системе.
 * <p/>
 * Все <i>Runtime</i> исключения системы должны расширять этот класс исключения или его наследников.<br> Спецификация
 * языка позволяет не объявлять <i>Runtime</i> исключения в сигнатуре метода, однако, в системе <b>ЛЮБОЙ</b> наследник
 * данного класса, выбрасываемый в методе, <b> должен быть явно оглашен</b> в сигнатуре этого метода!
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RuntimeCoreException.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class RuntimeCoreException extends RuntimeException {

    /**
     * Конструирует новый инстанс {@link ru.xr.ine.core.RuntimeCoreException}, с указанным сообщением
     *
     * @param message детализированное сообщение об ошибке
     */
    public RuntimeCoreException(String message) {
        super(message);
    }

    /**
     * Конструирует новый инстанс {@link ru.xr.ine.core.RuntimeCoreException}, с указанным сообщением
     *
     * @param message детализированное сообщение об ошибке
     * @param cause   причина (более глубокая) возникновения исключения. Пустая ссылка допускается, однако, не
     *                целесообразно пользоваться данным конструктором в ситуации когда известно что ссылка всегда будет
     *                пустой
     */
    public RuntimeCoreException(String message, Throwable cause) {
        super(message, cause);
    }
}