/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import ru.xr.ine.core.userpermits.SystemUser;

/**
 * Утилитный класс, предоставляющий возможность работы с профайлом пользователя
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: UserHolder.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class UserHolder {

    /** Хранилище профайла */
    private static ThreadLocal<UserProfile> threadLocal = new ThreadLocal<>();

    private UserHolder() {
    }

    /**
     * Устанавливает профайл пользователя
     *
     * @param user текущий пользователь
     */
    public static void setUser(SystemUser user) {
        setUserProfile(new UserProfile(user));
    }

    /**
     * Устанавливает профайл пользователя
     *
     * @param userProfile профайл текущего пользователя
     */
    public static void setUserProfile(UserProfile userProfile) {
        threadLocal.set(userProfile);
    }

    /**
     * Возвращает профайл текущего пользователя
     *
     * @return профайл текущего пользователя
     */
    public static UserProfile getUser() {
        return threadLocal.get();
    }

}
