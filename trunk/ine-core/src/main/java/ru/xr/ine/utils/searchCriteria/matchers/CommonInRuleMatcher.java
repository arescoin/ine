/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import ru.xr.ine.utils.searchCriteria.RuleMatcher;

/**
 * Обобщенный матчер для поиска во множестве по полному совподению
 * Количество патернов: один и более
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CommonInRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class CommonInRuleMatcher implements RuleMatcher {

    @Override
    public boolean match(Object value, Object... pattern) {
        boolean result = false;
        for (Object o : pattern) {
            if (value.equals(o)) {
                result = true;
                break;
            }
        }

        return result;
    }

}
