/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

/**
 * todo: Важно! Предоставить возможность создания параметризорованных доступов
 *
 *  Пакет содержит интерфейсы, предназначенные для описания системных пользователей, наборов их доступов и ролей.
 *
 * @since 1.0
 * @SVNVersion "$Id: package-info.java 29 2017-04-04 15:32:19Z xerror $"
 */

package ru.xr.ine.core.userpermits;
