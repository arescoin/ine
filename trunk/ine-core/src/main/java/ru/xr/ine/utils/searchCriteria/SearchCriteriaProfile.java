/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria;

import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.structure.DataType;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.*;

/**
 * Описание поисковых правил для конкретного типа объекта
 * <p/>
 * Пример:
 * <pre>
 * SearchCriteriaProfile searchCriteriaProfile = access.getSearchCriteriaProfile();
 * HashSet<SearchCriterion> searchCriteria = new HashSet<SearchCriterion>();
 * //
 * SearchCriterion&lt;String&gt; searchCriteriaCode =
 * (SearchCriterion&lt;String&gt;) searchCriteriaProfile.getSearchCriterion("code");
 * searchCriteriaCode.setCriterionRule(CriteriaRule.contains);
 * searchCriteriaCode.addCriteriaVal("a");
 * searchCriteria.add(searchCriteriaCode);
 * //
 * Collection collection = access.searchObjects(searchCriteria);
 * </pre>
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SearchCriteriaProfile.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SearchCriteriaProfile implements Serializable {

    private static final long serialVersionUID = 1526472875622776142L;

    /** Класс для которого сформирован профиль */
    private final Class searchClass;

    /** Мапинг: поле - набор правил */
    Map<String, Profile> profileMap = new HashMap<String, Profile>();

    /** Мапинг: поле - имя метода (для полей с не примитивными данными) */
    Map<String, String> methodMap = new HashMap<String, String>();

    /**
     * Конструирует новый объект содержащий правила для типов переданного класса
     *
     * @param searchClass класс для формирования профиля
     */
    public SearchCriteriaProfile(Class searchClass) {
        this.searchClass = searchClass;
    }

    /**
     * Возвращает класс для формирования профиля
     *
     * @return класс используемый для формирования профиля
     */
    public Class getSearchClass() {
        return searchClass;
    }

    /**
     * Возвращает немодифицируемую карту профилей (правил) формитрования поисковых запросов
     * <p/>
     * Мапинг: поле - набор правил
     *
     * @return карта правил
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public Map<String, Profile> getProfileMap() {
        return Collections.unmodifiableMap(profileMap);
    }

    /**
     * Добавляет новое правило
     * <p/>
     * Не предназначен для прикладной разработки
     *
     * @param fieldName      название поля в поисковом объекте
     * @param allowableRules перечисление допустимых правил сопоставления
     * @param method         метод доступа к значению поля
     */
    public void addRule(String fieldName, AllowableRules allowableRules, Method method) {
        profileMap.put(fieldName, new Profile(allowableRules, method));
    }

    /**
     * Добавляет метод
     * <p/>
     * Не предназначен для прикладной разработки
     *
     * @param fieldName название поля в поисковом объекте
     * @param method    метод доступа к значению поля
     */
    public void addMethod(String fieldName, Method method) {
        methodMap.put(fieldName, method.getName());
    }

    /**
     * Конструирует и предоставляет заготовку (контейнер) для формирования поискового запроса
     *
     * @param fieldName название поля в поисковом объекте
     * @return контейнер поискового запроса
     * @throws IneIllegalArgumentException при передаче null в fieldName
     */
    public SearchCriterion getSearchCriterion(String fieldName) throws IneIllegalArgumentException {

        checkSimpleFieldName(fieldName);

        return getSearchCriterion(fieldName, profileMap.get(fieldName).getAllowableRules().getDataType());
    }

    /**
     * Конструирует и предоставляет заготовку (контейнер) для формирования поискового запроса
     *
     * @param fieldName название поля в поисковом объекте
     * @param dataType  тип данных
     * @return контейнер поискового запроса
     * @throws IneIllegalArgumentException при передаче null в fieldName
     */
    public SearchCriterion getSearchCriterion(String fieldName, DataType dataType) throws IneIllegalArgumentException {

        checkSimpleFieldName(fieldName);

        return constructSearchCriterion(fieldName, dataType, profileMap.get(fieldName).getMethod());
    }

    /**
     * Конструирует и возвращает подготовленный элемент поискового запроса.<br>
     * Все параметры должны отличаться от null, vals должен содержать не менее одного значения
     *
     * @param fieldName    название поля в поисковом объекте
     * @param criteriaRule правило сравнения
     * @param vals         значения для сравнения
     * @return контейнер поискового запроса
     * @throws IneIllegalArgumentException при передаче null в fieldName
     */
    @SuppressWarnings({"unchecked"})
    public <T> SearchCriterion<T> getSearchCriterion(String fieldName, CriteriaRule criteriaRule, T... vals)
            throws IneIllegalArgumentException {

        checkSimpleFieldName(fieldName);
        checkNull(criteriaRule, "CriteriaRule");
        checkNull(vals, "Criteria Values");

        if (vals.length < 1) {
            throw new IneIllegalArgumentException("The size of Criteria Values must be greater than zero");
        }

        SearchCriterion<T> searchCriterion =
                getSearchCriterion(fieldName, profileMap.get(fieldName).getAllowableRules().getDataType());
        searchCriterion.setCriterionRule(criteriaRule);

        for (T val : vals) {
            searchCriterion.addCriteriaVal(val);
        }

        return searchCriterion;
    }

    /**
     * Конструирует и возвращает элемент-заготовку для формирования поискового запроса.
     *
     * @param fieldName    название поля в поисковом объекте
     * @param comparator   компаратор для гибкого сравнения объектов
     * @return контейнер поискового запроса
     * @throws IneIllegalArgumentException при передаче null в fieldName
     */
    @SuppressWarnings({"unchecked"})
    public <T> SearchCriterion<T> getSearchCriterion(String fieldName, Comparator comparator)
            throws IneIllegalArgumentException {

        checkObjectFieldName(fieldName);

        return (SearchCriterion<T>) constructSearchCriterion(fieldName, comparator, getMethod(fieldName));
    }

    /**
     * Конструирует и возвращает подготовленный элемент поискового запроса.<br>
     * Все параметры должны отличаться от null, vals должен содержать не менее одного значения
     *
     * @param fieldName    название поля в поисковом объекте
     * @param comparator   компаратор для гибкого сравнения объектов
     * @param criteriaRule правило сравнения
     * @param vals         значения для сравнения
     * @return контейнер поискового запроса
     * @throws IneIllegalArgumentException при передаче null в fieldName
     */
    @SuppressWarnings({"unchecked"})
    public <T> SearchCriterion<T> getSearchCriterion(String fieldName, Comparator comparator,
            CriteriaRule criteriaRule, T... vals) throws IneIllegalArgumentException {

        checkObjectFieldName(fieldName);
        checkNull(vals, "Criteria Values");

        if (vals.length < 1) {
            throw new IneIllegalArgumentException("The size of Criteria Values must be greater than zero");
        }

        SearchCriterion<T> searchCriterion = constructSearchCriterion(fieldName, comparator, getMethod(fieldName));
        searchCriterion.setCriterionRule(criteriaRule);

        for (T val : vals) {
            searchCriterion.addCriteriaVal(val);
        }

        return searchCriterion;
    }

    /**
     * Конструирует и предоставляет заготовку (контейнер) для формирования поискового запроса
     *
     * @param fieldName название поля в поисковом объекте
     * @param dataType  тип данных
     * @param method    метод доступа для получения значения требуемого атрибута
     * @return контейнер поискового запроса
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передачи null в fieldName, dataType или method
     */
    protected SearchCriterion constructSearchCriterion(String fieldName, DataType dataType, Method method)
            throws IneIllegalArgumentException {

        checkNull(dataType, "DataType");
        checkNull(method, "Method");

        SearchCriterion searchCriterion;
        switch (dataType) {
        case stringType:
            searchCriterion = new SearchCriterion<String>(fieldName, searchClass, method);
            break;
        case booleanType:
            searchCriterion = new SearchCriterion<Boolean>(fieldName, searchClass, method);
            break;
        case dateType:
            searchCriterion = new SearchCriterion<Date>(fieldName, searchClass, method);
            break;
        case longType:
            searchCriterion = new SearchCriterion<Long>(fieldName, searchClass, method);
            break;
        case bigDecimalType:
            searchCriterion = new SearchCriterion<BigDecimal>(fieldName, searchClass, method);
            break;
        default:
            throw new IllegalArgumentException("Unsuported data type [" + dataType + "]");
        }

        searchCriterion.setCriteriaRuleSet(AllowableRules.getCriteriaRules(dataType));

        return searchCriterion;
    }

    /**
     * Конструирует и предоставляет заготовку (контейнер) для формирования поискового запроса
     *
     * @param fieldName  название поля в поисковом объекте
     * @param comparator компаратор для гибкого сравнения объектов
     * @param method     метод доступа для получения значения требуемого атрибута
     * @return контейнер поискового запроса
     * @throws ru.xr.ine.core.IneIllegalArgumentException при попытке передачи null в fieldName, comparator или method
     */
    protected SearchCriterion constructSearchCriterion(String fieldName, Comparator comparator, Method method)
            throws IneIllegalArgumentException {

        checkNull(comparator, "Comparator");
        checkNull(method, "Method");

        SearchCriterion searchCriterion = new SearchCriterion(fieldName, searchClass, method);
        searchCriterion.setCriteriaRuleSet(AllowableRules.getRulesForComparator(comparator));

        return searchCriterion;
    }

    private void checkNull(Object value, String valueName) throws IneIllegalArgumentException {
        if (value == null) {
            throw new IneIllegalArgumentException(valueName + " can't be null");
        }
    }

    private void checkSimpleFieldName(String fieldName) throws IneIllegalArgumentException {
        checkNull(fieldName, "Field name");
        if (!profileMap.containsKey(fieldName)) {
            throw new IneIllegalArgumentException("Field [" + fieldName + "] not found");
        }
    }

    private void checkObjectFieldName(String fieldName) throws IneIllegalArgumentException {
        checkNull(fieldName, "Field name");
        if (!methodMap.containsKey(fieldName)) {
            throw new IneIllegalArgumentException("Field [" + fieldName + "] not found");
        }
    }

    @SuppressWarnings({"unchecked"})
    private Method getMethod(String fieldName) throws IneIllegalArgumentException {
        try {
            return searchClass.getMethod(methodMap.get(fieldName));
        } catch (NoSuchMethodException e) {
            throw new IneIllegalArgumentException("Failed to get method for field " + fieldName);
        }
    }

    /**
     * Профиль поиска, содержит в себе:
     * <ul>
     * <li>Перечисление допустимых правил сопоставления</li>
     * <li>метод доступа к значению поля</li>
     * </ul>
     * <p/>
     * При десериализации возможно возникновение исключения NoSuchMethodException оборачиваемого в
     * IllegalStateException, однако эта ситуация не должна возникнуть в штатных ситуациях.
     */
    public class Profile implements Serializable {

        private final AllowableRules allowableRules;
        private Class searchClass;
        private transient Method method;

        /** строковое имя метода доступа, используется для востановления объекта после десериализации */
        private String methodName;

        /**
         * Конструирует профиль поиска для содержащий нобор допустимых правил сопоставления и
         * метод доступа к значению поля
         *
         * @param allowableRules содержащий нобор допустимых правил сопоставления
         * @param method         метод доступа к значению поля
         */
        public Profile(AllowableRules allowableRules, Method method) {

            this.searchClass = SearchCriteriaProfile.this.getSearchClass();
            this.allowableRules = allowableRules;
            this.method = method;
            this.methodName = this.method.getName();
        }

        /**
         * Возарщает набор допустимых правил сопоставления
         *
         * @return правила
         */
        public AllowableRules getAllowableRules() {
            return allowableRules;
        }

        /**
         * Возвращает метод доступа к значению поискового поля
         *
         * @return акцессор
         */
        public Method getMethod() {
            return method;
        }

        @SuppressWarnings({"unchecked"})
        private Object readResolve() {
            //для разрещения проблемы несериализуемости "метода"
            try {
                this.method = this.searchClass.getMethod(methodName);
                return this;
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException(e);
            }
        }
    }

}
