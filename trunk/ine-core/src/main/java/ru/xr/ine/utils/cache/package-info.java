/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

/**
 * Пакет предназначен для работы с кешем данных. Для получения данных из кеша необходимо использовать класс
 * {@link Cache}. Также этот класс используется для управления данными в кеше - удаление и добавление новых данных.
 * <br>
 * При получения необходимо реализовать интерфейс {@link Producer}, который будет получать объект, если его нет в кеше.
 * <br>
 * Пример использования кеша:
 * <code><pre>
 * BigDecimal key = new BigDecimal(1);
 * String someObj = Cache.get("someRegion", key, new Producer() {
 *     Object get(Object key) throws GenericSystemException {
 *         return "Some cached string with id = " + ((BigDecimal) key).toString();
 *     }
 * });
 * </pre></code>
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: package-info.java 29 2017-04-04 15:32:19Z xerror $"
 */
package ru.xr.ine.utils.cache;
