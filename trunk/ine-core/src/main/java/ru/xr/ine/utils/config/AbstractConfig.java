/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.config;

import java.io.File;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractConfig.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class AbstractConfig implements SystemConfig {

    /** Признак необходимости проверки параметра инициализации */
    protected boolean checkInitParam = true;
    /** Признак необходимости проверки дополнительных параметров инициализации */
    protected boolean checkAdditionals = true;

    protected String configParam;
    protected String[] additionalParams;

    @Override
    public void init(String initialisationParam, String... additionalParams) throws ConfigurationException {

        if (checkInitParam && initialisationParam == null) {
            throw new ConfigurationException("Configuration path can't be null");
        }

        configParam = initialisationParam.trim();

        if (checkInitParam && configParam.length() < 1) {
            throw new ConfigurationException("Configuration path can't have zero length");
        }

        String path = System.getProperty(ConfigurationManager.SYSTEM_CONFIC_PATH_PROPERY);

        if(path != null) {
            if(!path.endsWith(Character.toString(File.separatorChar))){
                path = path + Character.toString(File.separatorChar);
            }
            configParam = path + configParam;
        }
    }

    @Override
    public boolean isWritable() {
        return false;
    }

    @Override
    public void write() throws UnsupportedOperationException, ConfigurationException {
        throw new UnsupportedOperationException();
    }
}
