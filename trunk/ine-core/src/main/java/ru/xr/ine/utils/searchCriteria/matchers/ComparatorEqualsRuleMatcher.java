/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.searchCriteria.matchers;

import java.util.Comparator;

/**
 * Простой матчер для сравнения на равенство.
 * Количество паттернов: строго один
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ComparatorEqualsRuleMatcher.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ComparatorEqualsRuleMatcher extends AbstractComparatorRuleMatcher {

    public ComparatorEqualsRuleMatcher(Comparator comparator) {
        super(comparator);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        if (pattern.length == 0) {
            return false;
        }
        try {
            return comparator.compare(value, pattern[0]) == 0;
        } catch (ClassCastException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
