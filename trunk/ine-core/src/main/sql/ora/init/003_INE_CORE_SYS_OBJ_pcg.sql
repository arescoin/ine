/*
"$Id: 003_INE_CORE_SYS_OBJ_pcg.sql 29 2017-04-04 15:32:19Z xerror $"
*/

-- создаем пакет INE_CORE_SYS_OBJ
create or replace PACKAGE INE_CORE_SYS_OBJ AS

  -- Объявляем константы...
  -- минимальное значение для идентификатора в системе
  MIN_ALLOWABLE_VAL    CONSTANT NUMBER :=  1;

  -- тип системного объекта Таблица
  TABLE_TYP      CONSTANT NUMBER :=  1;
  -- тип системного объекта Столбец
  COLUMN_TYP     CONSTANT NUMBER :=  2;
  -- тип системного объекта Последовательность
  SEQUENCE_TYP   CONSTANT NUMBER :=  3;
  -- тип системного объекта Интерфейс
  INTERFACE_TYP  CONSTANT NUMBER :=  4;

  -- Системная дата начала жизни
  sysFD CONSTANT DATE := INE_CORE_SYS.GET_SYSTEM_FROMDATE;
  -- Системная дата закрытия версии
  sysTD CONSTANT DATE := INE_CORE_SYS.GET_SYSTEM_TODATE;

  -- FD, TD значение названия
  FD_VAL CONSTANT VARCHAR2(255) := 'FD';
  TD_VAL CONSTANT VARCHAR2(255) := 'TD';

  -- FD, TD описание
  fd_Dsc CONSTANT VARCHAR2(255) := 'Дата начала жизни';
  td_Dsc CONSTANT VARCHAR2(255) := 'Дата закрытия версии';

  -- суффикс исторических таблиц
  HIST_SFX    CONSTANT    VARCHAR2(255) := '_HSTR';

  -- типы кастомных таблиц
  CUSTOM_TYP_STRING       CONSTANT  VARCHAR2(255) := 'String';
  CUSTOM_TYP_LONG         CONSTANT  VARCHAR2(255) := 'Long';
  CUSTOM_TYP_BIG_DECIMAL  CONSTANT  VARCHAR2(255) := 'BigDecimal';
  CUSTOM_TYP_BOOLEAN      CONSTANT  VARCHAR2(255) := 'Boolean';
  CUSTOM_TYP_DATE         CONSTANT  VARCHAR2(255) := 'Date';


  FUNCTION add_Sys_Obj (
    in_typ       NUMBER,
    in_objName   VARCHAR2,
    in_up        NUMBER,
    in_dsc       VARCHAR2
  ) RETURN NUMBER;

  FUNCTION add_Sys_Obj (
    in_typ       NUMBER,
    in_objName   VARCHAR2,
    in_up        NUMBER,
    in_dsc       VARCHAR2,
    in_pattern   VARCHAR2
  ) RETURN NUMBER;

  PROCEDURE add_Pattern (
    in_id        NUMBER,
    in_pattern   VARCHAR2
  );

  FUNCTION add_Sys_Obj_FD (
    in_up       NUMBER
  ) RETURN NUMBER;

  FUNCTION add_Sys_Obj_TD (
    in_up       NUMBER
  ) RETURN NUMBER;

  FUNCTION add_Sys_Obj_FDTD (
    in_up       NUMBER
  ) RETURN NUMBER;

  FUNCTION add_Sys_Obj_HSTR (
    in_table_name VARCHAR2
  ) RETURN NUMBER;

  FUNCTION get_SYSOBJ_N_BY_NAME_TYP(
     in_sysobj_name VARCHAR2,
     in_sys_typ NUMBER,
     in_parent_n NUMBER := null
  ) RETURN NUMBER;

  FUNCTION get_Table_N(
    in_table_name VARCHAR2
  ) RETURN NUMBER;

  FUNCTION get_Column_N(
    in_table_name VARCHAR2,
    in_column_name VARCHAR2
  ) RETURN NUMBER;

  FUNCTION create_Custom_Table(
     in_parent_name VARCHAR2, in_val_typ VARCHAR2
  ) RETURN NUMBER;

  PROCEDURE create_CustomAttrs_Tables (
    in_parent_name VARCHAR2
  );

END INE_CORE_SYS_OBJ;
/


-- Создаем само тело пакета
------------------------------------------------------------------------------------------------------------------------
create or replace PACKAGE BODY INE_CORE_SYS_OBJ AS

FUNCTION add_Sys_Obj (
    in_typ      NUMBER,
    in_objName  VARCHAR2,
    in_up       NUMBER,
    in_dsc      VARCHAR2
  )
  RETURN NUMBER IS new_id NUMBER;

  paramNumber NUMBER:=0;

BEGIN
  new_id := 0;

  -- проверка пришедших параметров
  -- проверяем тип
  IF (in_typ is null) then
    RAISE_APPLICATION_ERROR(-20001, 'Type parameter must not be null.');
  ELSIF (in_typ < 1) THEN
    RAISE_APPLICATION_ERROR(-20002, 'Type parameter length must be greater than zero.' || ' in_typ: ' || in_typ);
  END IF;

  -- проверяем in_objName на нал
  IF (in_objName IS NULL) THEN
    RAISE_APPLICATION_ERROR(-20001, 'in_objName parameter must not be null.');
  END IF;


  -- проверяем in_objName на повторениия
  SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO WHERE SO.OBJ_NAME = in_objName AND SO.up = in_up;
  IF (paramNumber > 0) THEN
    RAISE_APPLICATION_ERROR(-20003, 'ObjName parameter must be unique.'
        || ' in_objName: ' || in_objName || ', up: ' || in_up);
  END IF;

  IF (in_up IS NULL) THEN
    paramNumber := 0;
    SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO WHERE SO.OBJ_NAME = in_objName AND SO.up IS NULL;
    IF (paramNumber > 0) THEN
      RAISE_APPLICATION_ERROR(-20003, 'ObjName parameter must be unique.' || ' in_objName: ' || in_objName);
    END IF;
  END IF;

  IF (in_up IS NULL AND in_typ = COLUMN_TYP) THEN
    RAISE_APPLICATION_ERROR(-20003, 'Incompatible combination: in_typ[' || in_typ || '] and in_up[' || in_up
      || '], in_up must not be null');
  END IF;


  paramNumber := 0;

  -- проверяем in_up
  IF (in_up IS NOT NULL) THEN
--    dbms_output.put_line('Recive: ' || in_up);

    SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO WHERE SO.N = in_up;
--    dbms_output.put_line('Count: ' || paramNumber);

    IF (paramNumber < 1) THEN
      RAISE_APPLICATION_ERROR(-20004, 'in_up parameter must be from real objects.');
    END IF;
  END IF;

   -- проверяем in_dsc на нал
  IF (in_dsc IS NULL) THEN
    RAISE_APPLICATION_ERROR(-20001, 'in_dsc parameter must not be null.');
  ELSIF (LENGTH(in_dsc) < 10) THEN
    RAISE_APPLICATION_ERROR(-20002, 'in_dsc parameter length must be greater than 9.');
  END IF;

  SELECT SEQ_SYS_OBJ_N.NEXTVAL INTO paramNumber FROM dual;

  INSERT INTO
    SYS_OBJ SO(SO.N, SO.TYP, SO.OBJ_NAME, SO.UP, SO.DSC, SO.FD, SO.TD)
  VALUES
    (paramNumber, in_typ, in_objName, in_up, in_dsc, sysFD, sysTD);

  new_id := paramnumber;

  RETURN(new_id);
END;
-- FUNCTION add_Sys_Obj

FUNCTION add_Sys_Obj (
    in_typ      NUMBER,
    in_objName  VARCHAR2,
    in_up       NUMBER,
    in_dsc      VARCHAR2,
    in_pattern  VARCHAR2
  ) RETURN NUMBER IS new_id NUMBER;
BEGIN
  new_id := add_Sys_Obj(in_typ, in_objName, in_up, in_dsc);
  add_Pattern(new_id, in_pattern);
  RETURN(new_id);
END;
-- FUNCTION add_Sys_Obj


PROCEDURE add_Pattern (
  in_id        NUMBER,
  in_pattern   VARCHAR2
) IS
  typ NUMBER;
BEGIN

  -- проверяем in_id на нал
  IF (in_id IS NULL) THEN
    RAISE_APPLICATION_ERROR(-20001, 'in_id parameter must not be null.');
  END IF;

  SELECT TYP INTO typ FROM SYS_OBJ WHERE N = in_id;

  -- проверяем, что in_pattern задан только для типа "столбец"
  IF (typ <> COLUMN_TYP) THEN
    RAISE_APPLICATION_ERROR(-20003, 'Incompatible combination: in_typ[' || typ || '] and in_pattern[' || in_pattern
      || '], in_typ must be equals to ' || COLUMN_TYP);
  END IF;

  UPDATE SYS_OBJ SET PATTERN = in_pattern WHERE N = in_id;

END;
-- PROCEDURE add_Pattern


------------------------------------------------------------------------------------------------------------------------
FUNCTION add_Sys_Obj_FD (in_up NUMBER) RETURN NUMBER IS new_id NUMBER;
  retValue NUMBER:= 0;
BEGIN
  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP,
    INE_CORE_SYS_OBJ.FD_VAL,
    in_up,
    INE_CORE_SYS_OBJ.FD_DSC
  );

  return(retValue);
END;
-- FUNCTION add_Sys_Obj_FD

FUNCTION add_Sys_Obj_TD (in_up NUMBER) RETURN NUMBER IS new_id NUMBER;
  retValue NUMBER:= 0;
BEGIN
  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP,
    INE_CORE_SYS_OBJ.TD_VAL,
    in_up,
    INE_CORE_SYS_OBJ.TD_DSC
  );

  return(retValue);
END;
-- FUNCTION add_Sys_Obj_TD


------------------------------------------------------------------------------------------------------------------------
FUNCTION add_Sys_Obj_FDTD (in_up NUMBER) RETURN NUMBER IS new_id NUMBER;
  retValue NUMBER:= 0;
BEGIN
  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP,
    INE_CORE_SYS_OBJ.FD_VAL,
    in_up,
    INE_CORE_SYS_OBJ.FD_DSC
  );

  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP,
    INE_CORE_SYS_OBJ.TD_VAL,
    in_up,
    INE_CORE_SYS_OBJ.TD_DSC
  );

  return(retValue);
END;
--FUNCTION add_Sys_Obj_FDTD


------------------------------------------------------------------------------------------------------------------------
FUNCTION add_Sys_Obj_HSTR (in_table_name VARCHAR2) RETURN NUMBER IS new_id NUMBER;
  retValue             NUMBER:= 0;
  paramNumber          NUMBER := 0;
  history_Table_Name   VARCHAR2(2000);
  cursNum              INTEGER;
  result               INTEGER;
  sysObjDsc            VARCHAR2(2000);
  system_ID            NUMBER;
  fkSystemId           NUMBER;

BEGIN

  history_Table_Name := in_table_name || HIST_SFX;

  dbms_output.put_line(history_Table_Name);

  -- Проверяем на отсутствие таблицы с историей в реестре
  SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO
    WHERE SO.OBJ_NAME = history_Table_Name AND SO.TYP = INE_CORE_SYS_OBJ.TABLE_TYP
    AND systimestamp BETWEEN SO.FD AND SO.TD;
  IF (paramNumber <> 0) THEN
    RAISE_APPLICATION_ERROR(-20005, 'Target TableName parameter is exists.' || ' table_Name: ' || history_Table_Name);
  END IF;

  -- Проверяем на наличие таблицы-оригинала в реестре
  paramNumber := get_Table_N(in_table_name);

  -- Теперь формируем DDL
  cursNum := dbms_sql.open_cursor;
  dbms_sql.parse(cursNum, 'CREATE TABLE ' || history_Table_Name ||
    ' AS (SELECT * FROM ' || in_table_name || ' WHERE 1=2)', dbms_sql.native);
  result := dbms_sql.execute(cursNum);
  dbms_sql.close_cursor(cursNum);

  SELECT SO.DSC INTO sysObjDsc FROM SYS_OBJ SO
    WHERE SO.OBJ_NAME = in_table_name AND SO.TYP = INE_CORE_SYS_OBJ.TABLE_TYP
    AND systimestamp BETWEEN SO.FD AND SO.TD;

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(
    INE_CORE_SYS_OBJ.TABLE_TYP, history_Table_Name, paramNumber, 'History: '|| sysObjDsc
  );

  FOR v_TABLE_DSCS IN
    (SELECT SO.DSC, SO.OBJ_NAME FROM SYS_OBJ SO
       WHERE systimestamp BETWEEN SO.FD AND SO.TD AND SO.TYP = INE_CORE_SYS_OBJ.COLUMN_TYP AND SO.UP IN
         (
          SELECT SO2.N FROM SYS_OBJ SO2 WHERE SO2.OBJ_NAME = in_table_name
            AND SO2.TYP = INE_CORE_SYS_OBJ.TABLE_TYP AND systimestamp BETWEEN SO2.FD AND SO2.TD
         )
    ) LOOP

    fkSystemId := INE_CORE_SYS_OBJ.add_Sys_Obj(
      INE_CORE_SYS_OBJ.COLUMN_TYP, v_TABLE_DSCS.OBJ_NAME, system_ID, v_TABLE_DSCS.DSC
    );

  END LOOP;

  return(retValue);
END;
-- FUNCTION add_Sys_Obj_HSTR


------------------------------------------------------------------------------------------------------------------------
PROCEDURE create_CustomAttrs_Tables(in_parent_name VARCHAR2) IS
  paramNumber      NUMBER;

BEGIN
  paramNumber := get_Table_N(in_parent_name);

  paramNumber := create_Custom_Table(in_parent_name, CUSTOM_TYP_STRING);
  paramNumber := create_Custom_Table(in_parent_name, CUSTOM_TYP_LONG);
  paramNumber := create_Custom_Table(in_parent_name, CUSTOM_TYP_BIG_DECIMAL);
  paramNumber := create_Custom_Table(in_parent_name, CUSTOM_TYP_BOOLEAN);
  paramNumber := create_Custom_Table(in_parent_name, CUSTOM_TYP_DATE);

END;
-- PROCEDURE create_CustomAttrs_Tables


------------------------------------------------------------------------------------------------------------------------
FUNCTION obtainSQLTyp(in_java_typ VARCHAR2) RETURN VARCHAR2 IS sqlTyp VARCHAR2(50);
BEGIN
  IF(in_java_typ = CUSTOM_TYP_STRING)      THEN return('VARCHAR2(4000)'); END IF;
  IF(in_java_typ = CUSTOM_TYP_LONG)        THEN return('NUMBER');         END IF;
  IF(in_java_typ = CUSTOM_TYP_BIG_DECIMAL) THEN return('NUMBER');         END IF;
  IF(in_java_typ = CUSTOM_TYP_BOOLEAN)     THEN return('NUMBER(1)');      END IF;
  IF(in_java_typ = CUSTOM_TYP_DATE)        THEN return('DATE');           END IF;

  -- а вот если до этого момента не вышли, то однозначно - хрень и падать надо!!!
  RAISE_APPLICATION_ERROR(-20007, 'Incorrect given type');
END;
--FUNCTION obtainSQLTyp


------------------------------------------------------------------------------------------------------------------------
FUNCTION create_Custom_Table(in_parent_name VARCHAR2, in_val_typ VARCHAR2) RETURN NUMBER IS new_id NUMBER;
  sqlFirstPart    varchar2(2000);
  sqlLastPart     varchar2(2000);
  tableAttrName   varchar2(255);
  index1          varchar2(100);
  index2          varchar2(100);
  columnType      varchar2(50);

  cursNum         NUMBER;
  paramNumber     NUMBER;
  fakeId          NUMBER;
BEGIN
  new_id := 0;

  -- проверяем тип таблицы
  IF(length(in_val_typ) < 1) THEN
    RAISE_APPLICATION_ERROR(-20005, 'TypeName parameter is invalid.');
  END IF;

  -- проверяем наличие родительской таблицы
  paramNumber := get_Table_N(in_parent_name);

  tableAttrName := in_parent_name || '_' || in_val_typ;

  -- Проверяем на наличие таблицы атрибутов в реестре
  SELECT COUNT(*) INTO new_id FROM SYS_OBJ SO
    WHERE SO.OBJ_NAME = tableAttrName AND SO.TYP = INE_CORE_SYS_OBJ.TABLE_TYP AND systimestamp BETWEEN SO.FD AND SO.TD;
  IF (new_id <> 0) THEN
    RAISE_APPLICATION_ERROR(-20006, 'CustomAttrs TableName exists.');
  END IF;

  -- подбиваем названия индексов
  index1 := 'UK_'|| tableAttrName || '_ID';
  index2 := 'IND_'|| tableAttrName || '_A';

  IF(length(index1) > 29) THEN
    index1 := in_parent_name || in_val_typ || '1';
  END IF;

  IF(length(index2) > 29) THEN
    index2 := in_parent_name || in_val_typ || '2';
  END IF;
  -- eсли не хватит, разруливать руками...

  -- получаем тип для SQL
  columnType := obtainSQLTyp(in_val_typ);

  sqlFirstPart := 'CREATE TABLE ' || tableAttrName || '('
    || 'ENT_N NUMBER NOT NULL,'
    || ' ATTR_N NUMBER NOT NULL,'
    || ' ATTR_VAL ' || columnType || ' NOT NULL,'
    || ' DSC VARCHAR2(1),'
    || ' FD TIMESTAMP NOT NULL,'
    || ' TD TIMESTAMP NOT NULL,'
    || ' CONSTRAINT ' || index1 || ' UNIQUE (ENT_N, ATTR_N) ENABLE)';

  dbms_output.put_line(sqlFirstPart);

  -- выполняем DDL
  cursNum := dbms_sql.open_cursor;

  dbms_sql.parse(cursNum, sqlFirstPart, dbms_sql.native);
  new_id := dbms_sql.execute(cursNum);

  sqlFirstPart := 'CREATE INDEX '|| index2 || ' ON ' || tableAttrName || '(ATTR_N)';
  dbms_sql.parse(cursNum, sqlFirstPart, dbms_sql.native);
  new_id := dbms_sql.execute(cursNum);

  dbms_sql.close_cursor(cursNum);

  -- Заносим все в реестр
  new_id := add_Sys_Obj(TABLE_TYP, tableAttrName, paramNumber,
    'Таблица произволных атрибутов для: ' || in_parent_name || ', тип: ' || in_val_typ);
  fakeId := add_Sys_Obj(INTERFACE_TYP, 'ru.xr.ine.core.structure.CustomAttributeValue', new_id,
    'Интерфейс значения произвольного атрибута');
  fakeId := add_Sys_Obj(COLUMN_TYP, 'ENT_N', new_id, 'Ссылка на N в таблице ' || in_parent_name);
  fakeId := add_Sys_Obj(COLUMN_TYP, 'ATTR_N', new_id, 'Ссылка на ATTR_N в таблице описания произвольных атрибутов');
  fakeId := add_Sys_Obj(COLUMN_TYP, 'ATTR_VAL', new_id, 'Значение атрибута');
  fakeId := add_Sys_Obj(COLUMN_TYP, 'DSC', new_id, 'Коментарий. Поле не используется!');
  fakeId := add_Sys_Obj_FDTD(new_id);

  -- Теперь создаем историю
  fakeId := add_Sys_Obj_HSTR(tableAttrName);

  return(new_id);
END;
-- FUNCTION create_Custom_Table


------------------------------------------------------------------------------------------------------------------------
-- проверка на наличие системного объекта с именем и типом, если нет то кидаем ошибку...
FUNCTION get_SYSOBJ_N_BY_NAME_TYP(
  in_sysobj_name VARCHAR2,
  in_sys_typ NUMBER,
  in_parent_n NUMBER := null) RETURN NUMBER IS new_id NUMBER;
BEGIN
  -- Проверяем на наличие таблицы-оригинала в реестре
  IF (in_parent_n is null) THEN
    SELECT COUNT(*) INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ
      AND systimestamp BETWEEN SO.FD AND SO.TD;
  ELSE
    SELECT COUNT(*) INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ AND SO.UP = in_parent_n
      AND systimestamp BETWEEN SO.FD AND SO.TD;
  END IF;

  IF (new_id < 1) THEN
    RAISE_APPLICATION_ERROR(-20008,
    'Object not found. in_sysobj_name ['||in_sysobj_name||']; '||
    'in_sys_typ ['||in_sys_typ||']; '||
    'in_parent_n ['||in_parent_n||']');
  ELSIF (new_id > 1) THEN
    RAISE_APPLICATION_ERROR(-20008, 'Too many objects found ['||new_id||']. '||
    'in_sysobj_name ['||in_sysobj_name||']; '||
    'in_sys_typ ['||in_sys_typ||']; '||
    'in_parent_n ['||in_parent_n||']');
  END IF;


  IF (in_parent_n is null) THEN
    SELECT N INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ
      AND systimestamp BETWEEN SO.FD AND SO.TD;
  ELSE
    SELECT N INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ AND SO.UP = in_parent_n
      AND systimestamp BETWEEN SO.FD AND SO.TD;
  END IF;

  return(new_id);
END;
-- FUNCTION get_SYSOBJ_N_BY_NAME_TYP

------------------------------------------------------------------------------------------------------------------------
FUNCTION get_Table_N(in_table_name VARCHAR2) RETURN NUMBER IS table_id NUMBER;
BEGIN
  table_id := get_SYSOBJ_N_BY_NAME_TYP(in_table_name, TABLE_TYP);
  return(table_id);
END;
-- FUNCTION get_Table_N

------------------------------------------------------------------------------------------------------------------------
FUNCTION get_Column_N(in_table_name VARCHAR2, in_column_name VARCHAR2) RETURN NUMBER IS column_id NUMBER;
BEGIN
  column_id := get_SYSOBJ_N_BY_NAME_TYP(in_column_name, COLUMN_TYP, get_Table_N(in_table_name));
  return(column_id);
END;
-- FUNCTION get_Column_N

------------------------------------------------------------------------------------------------------------------------


END INE_CORE_SYS_OBJ;


