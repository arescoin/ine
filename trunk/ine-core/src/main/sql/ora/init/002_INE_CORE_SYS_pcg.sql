/*
"$Id: 002_INE_CORE_SYS_pcg.sql 29 2017-04-04 15:32:19Z xerror $"
*/


-- создаем пакет INE_CORE_SYS
create or replace PACKAGE INE_CORE_SYS AS

  -- Возвращает системное FromDate
  FUNCTION get_System_FromDate RETURN DATE;

  -- Возвращает системное ToDate
  FUNCTION get_System_ToDate RETURN DATE;

  -- Возвращает текущую дату, получаемую через INE_CORE_SYS.getCurrentDate и пропущенную через функцию trunc этого пакета
  FUNCTION getCurrentDate RETURN TIMESTAMP;

  -- Обрезает переданный timestamp - убирает timezone
  FUNCTION trunc_timestamp(in_date TIMESTAMP) RETURN TIMESTAMP;

END INE_CORE_SYS;
/

-- создаем тело пакета INE_CORE_SYS
create or replace PACKAGE BODY INE_CORE_SYS AS
  fd DATE := to_date('01-01-2010 00:00:01', 'DD-MM-YYYY HH24:MI:SS');
  td DATE := to_date('01-01-3010 00:00:01', 'DD-MM-YYYY HH24:MI:SS');

  -- Возвращает системное FromDate
  FUNCTION get_System_FromDate RETURN DATE IS sysfd DATE;
  BEGIN
    sysfd := fd;
    RETURN(sysfd);
  END;

  FUNCTION get_System_ToDate RETURN DATE IS systd DATE;
  BEGIN
    systd := td;
    RETURN(systd);
  END;

  FUNCTION getCurrentDate RETURN TIMESTAMP IS l_date TIMESTAMP;
  BEGIN
    l_date := trunc_timestamp(systimestamp);
    RETURN(l_date);
  END;

  FUNCTION trunc_timestamp(in_date TIMESTAMP) RETURN TIMESTAMP IS l_date TIMESTAMP;
  BEGIN
    l_date := to_timestamp(to_char(in_date, 'DD.MM.YYYY HH24:MI:SS.FF3'), 'DD.MM.YYYY HH24:MI:SS.FF3');
    RETURN(l_date);
  END;

END INE_CORE_SYS;

