/*
"$Id: 011_INE_CORE_CUSTOM_TYPES_DSC.sql 29 2017-04-04 15:32:19Z xerror $"
*/
-- таблица для модифицированных типов
CREATE TABLE CUSTOM_TYPES_DSC
(
  N             NUMBER                 NOT NULL
, BASE          NUMBER                 NOT NULL -- есть паттерн
, PARENT        NUMBER                          -- есть паттерн
, TYPE_NAME     VARCHAR2(255)          NOT NULL
, DSC           VARCHAR2(2047)
, FD            TIMESTAMP              NOT NULL
, TD            TIMESTAMP              NOT NULL
, CONSTRAINT    UK_CUSTOM_TYPES_DSC_ID UNIQUE (N) ENABLE
);

CREATE INDEX IND_CUSTOM_TYPES_DSC_FDTD       ON  CUSTOM_TYPES_DSC(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_CUSTOM_TYPES_DSC INCREMENT BY 1 START WITH 100 ORDER;

-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'CUSTOM_TYPES_DSC', null,
     'Таблица для описания модифицированных типов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
     'ru.xr.ine.core.structure.CustomType', system_ID, 'Интерфейс описания модифицированного типа');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'N', system_ID, 'Уникальный номер типа');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'BASE', system_ID, 'Номер системного объекта, SYS_OBJ', INE_CORE_SYS_OBJ.get_Column_N('SYS_OBJ', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'PARENT', system_ID, 'Номер типа, который расширен', INE_CORE_SYS_OBJ.get_Column_N('CUSTOM_TYPES_DSC', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'TYPE_NAME', system_ID, 'Название типа');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'DSC', system_ID, 'Описание-комментарий к типу');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP,
     'SEQ_CUSTOM_TYPES_DSC', system_ID, 'Последовательность для идентификатора');

  COMMIT;

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CUSTOM_TYPES_DSC');

  COMMIT;

END;

/