/*
"$Id: 006_INE_CORE_CONSTANTS.sql 29 2017-04-04 15:32:19Z xerror $"
*/

-- Перечисление констант
CREATE TABLE CONSTANTS
(
  N               NUMBER           NOT NULL
, CONST_NAME      VARCHAR2(2000)   NOT NULL
, TYP             NUMBER           NOT NULL -- есть паттерн
, PARAM_MASK      VARCHAR2(2000)
, DEF_VAL         VARCHAR2(2000)   NOT NULL
, IS_NULLABLE     NUMBER           NOT NULL
, IS_MODYFABLE    NUMBER           NOT NULL
, DSC             VARCHAR2(2000)
, FD              TIMESTAMP        NOT NULL
, TD              TIMESTAMP        NOT NULL
, CONSTRAINT      UK_CONSTANTS_ID  UNIQUE (N) ENABLE
);
-- Индексы на таблы
CREATE INDEX IND_CONSTANTS_FDTD   ON CONSTANTS(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_CONSTANTS INCREMENT BY 1 START WITH 1000 ORDER;

-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER := 0;
  DD_N       NUMBER;
  DD_CODE    NUMBER;
  DIC_N      NUMBER;
BEGIN

  DD_N    := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'CONSTANTS', null,
     'Содержит описание существующих в системе констант');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
     'ru.xr.ine.core.constants.Constant', system_ID, 'Интерфейс константы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'N', system_ID, 'Cистемный номер константы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'CONST_NAME', system_ID, 'Наименование константы');

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'CONSTANT_TYPES';
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'TYP', system_ID, 'Тип описываемого элемента (простая, параметризированная). Словарь №'||DIC_N||')',
     '['||DD_N||':'||DIC_N||']'||DD_CODE);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'PARAM_MASK', system_ID, 'Маска набора параметров (по идентификаторам SYS_OBJ)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'DEF_VAL', system_ID, 'Значение по умолчанию');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'IS_NULLABLE', system_ID, 'Признак допустимости значения null');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'IS_MODYFABLE', system_ID, 'Признак допустимости изменения значений константы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'DSC', system_ID, 'Описание константы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP,
     'SEQ_CONSTANTS', system_ID, 'Последовательность для идентификатора');

  COMMIT;

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CONSTANTS');

  COMMIT;

END;

/

CREATE TABLE CONSTANT_LIST
(
  CONST_N    NUMBER          NOT NULL -- есть паттерн
, PARAM      NUMBER
, CONST_VAL  VARCHAR2(2000)  NOT NULL
, DSC        VARCHAR2(2000)
, FD         TIMESTAMP       NOT NULL
, TD         TIMESTAMP       NOT NULL
, CONSTRAINT UK_CONSTANT_LIST_ID  UNIQUE (CONST_N, PARAM) ENABLE
);


-- Индексы на таблы
CREATE INDEX IND_CONSTANT_LIST_N       ON  CONSTANT_LIST(CONST_N);
CREATE INDEX IND_CONSTANT_LIST_FDTD    ON  CONSTANT_LIST(FD, TD);
CREATE INDEX IND_CONSTANT_LIST_PARAM   ON  CONSTANT_LIST(PARAM);

-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'CONSTANT_LIST', null,
     'Содержит значения существующих в системе констант');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
     'ru.xr.ine.core.constants.ConstantValue', system_ID, 'Интерфейс значения константы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'CONST_N', system_ID, 'Cистемный номер константы', INE_CORE_SYS_OBJ.get_Column_N('CONSTANTS', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'PARAM', system_ID, 'Параметр константы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'CONST_VAL', system_ID, 'Значение константы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'DSC', system_ID, 'Описание к конкретному значению');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);
  COMMIT;

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CONSTANT_LIST');

  COMMIT;

END;

/

-- Вспомогательный пакет для работы с константами
CREATE OR REPLACE PACKAGE CONSTANTS_HELPER AS

  -- Возвращает значение константы из таблицы CONSTANT_LIST.
  -- Если там значение не оказалось, то берётся значение по-умолчанию из таблицы CONSTANTS
  -- Если такой константы не существует - возвращается NULL
  FUNCTION safe_Get_Constant_Value(
    in_const_n VARCHAR2,
    in_param NUMBER := -1
  ) RETURN VARCHAR2;

END CONSTANTS_HELPER;
/

-- Тело пакета
CREATE OR REPLACE PACKAGE BODY CONSTANTS_HELPER AS

  FUNCTION safe_Get_Constant_Value(
    in_const_n VARCHAR2,
    in_param NUMBER := -1
  ) RETURN VARCHAR2 IS
    ConstVal VARCHAR2(2000);
  BEGIN
    BEGIN
      -- Получаем значение константы из таблицы CONSTANT_LIST
      SELECT CONST_VAL INTO ConstVal FROM CONSTANT_LIST WHERE CONST_N = in_const_n AND NVL(PARAM, -1) = in_param;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        -- Если ничего там не нашли, берём значение по-умолчанию из таблицы CONSTANTS
        SELECT DEF_VAL INTO ConstVal FROM CONSTANTS WHERE N = in_const_n;
    END;
    RETURN(ConstVal);
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      -- Если в таблице CONSTANTS такой константы нет, возвращаем NULL
      RETURN(NULL);
  END safe_Get_Constant_Value;

END CONSTANTS_HELPER;

/
