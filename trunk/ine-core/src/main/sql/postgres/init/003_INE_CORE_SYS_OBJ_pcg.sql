/*
"$Id: 003_INE_CORE_SYS_OBJ_pcg.sql 29 2017-04-04 15:32:19Z xerror $"
*/


CREATE SCHEMA ine_core_sys_obj AUTHORIZATION :INE_USR;
GRANT ALL ON SCHEMA ine_core_sys_obj TO :INE_USR;

------------------------------------------------------------------------------------------------------------------------
  -- тип системного объекта Таблица
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.TABLE_TYP() RETURNS NUMERIC AS $func$
  BEGIN
    RETURN 1;
  END;
$func$ LANGUAGE plpgsql;

  -- тип системного объекта Столбец
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.COLUMN_TYP() RETURNS NUMERIC AS $func$
  BEGIN
    RETURN 2;
  END;
$func$ LANGUAGE plpgsql;

  -- тип системного объекта Последовательность
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.SEQUENCE_TYP() RETURNS NUMERIC AS $func$
  BEGIN
    RETURN 3;
  END;
$func$ LANGUAGE plpgsql;

  -- тип системного объекта INTERFACE_TYP
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.INTERFACE_TYP() RETURNS NUMERIC AS $func$
  BEGIN
    RETURN 4;
  END;
$func$ LANGUAGE plpgsql;

  -- FD значение названия
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.FD_VAL() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'FD';
  END;
$func$ LANGUAGE plpgsql;

  -- TD значение названия
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.TD_VAL() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'TD';
  END;
$func$ LANGUAGE plpgsql;

  -- FD описание
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.fd_Dsc() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'Дата начала жизни';
  END;
$func$ LANGUAGE plpgsql;

  -- TD зописание
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.td_Dsc() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'Дата закрытия версии';
  END;
$func$ LANGUAGE plpgsql;

  -- суффикс исторических таблиц
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.HIST_SFX() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN '_HSTR';
  END;
$func$ LANGUAGE plpgsql;

  -- типы кастомных таблиц  --------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.CUSTOM_TYP_STRING() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'String';
  END;
$func$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.CUSTOM_TYP_LONG() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'Long';
  END;
$func$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.CUSTOM_TYP_BIG_DECIMAL() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'BigDecimal';
  END;
$func$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.CUSTOM_TYP_BOOLEAN() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'Boolean';
  END;
$func$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.CUSTOM_TYP_DATE() RETURNS character varying(255) AS $func$
  BEGIN
    RETURN 'Date';
  END;
$func$ LANGUAGE plpgsql;

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.get_Table_N(in_table_name character varying(2000))
 RETURNS NUMERIC AS $func$
BEGIN
  return(INE_CORE_SYS_OBJ.get_SYSOBJ_N_BY_NAME_TYP(in_table_name, INE_CORE_SYS_OBJ.TABLE_TYP()));
END;
$func$ LANGUAGE plpgsql;
-- FUNCTION get_Table_N

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.get_Column_N(
  in_table_name character varying(2000),
  in_column_name character varying(2000)
  ) RETURNS NUMERIC AS $func$
BEGIN
  return(INE_CORE_SYS_OBJ.get_SYSOBJ_N_BY_NAME_TYP(in_column_name, INE_CORE_SYS_OBJ.COLUMN_TYP(), INE_CORE_SYS_OBJ.get_Table_N(in_table_name)));
END;
$func$ LANGUAGE plpgsql;
-- FUNCTION get_Column_N


------------------------------------------------------------------------------------------------------------------------


--======================================================================================================================

------------------------------------------------------------------------------------------------------------------------
-- проверка на наличие системного объекта с именем и типом, если нет то кидаем ошибку...
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.get_SYSOBJ_N_BY_NAME_TYP(
  in_sysobj_name character varying(2000),
  in_sys_typ NUMERIC,
  in_parent_n NUMERIC DEFAULT null)  RETURNS NUMERIC AS $func$
DECLARE
  new_id NUMERIC;
BEGIN
  -- Проверяем на наличие таблицы-оригинала в реестре
  IF (in_parent_n is null) THEN
    SELECT COUNT(*) INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ
      AND now() BETWEEN SO.FD AND SO.TD;
  ELSE
    SELECT COUNT(*) INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ AND SO.UP = in_parent_n
      AND now() BETWEEN SO.FD AND SO.TD;
  END IF;

  IF (new_id < 1) THEN
    RAISE EXCEPTION
    'Object not found. in_sysobj_name [%]; in_sys_typ [%]; in_parent_n [%]', in_sysobj_name, in_sys_typ, in_parent_n;
  ELSIF (new_id > 1) THEN
    RAISE EXCEPTION 'Too many objects found [%]. in_sysobj_name [%]; in_sys_typ [%]; in_parent_n [%]',
    new_id, in_sysobj_name, in_sys_typ,in_parent_n;
  END IF;


  IF (in_parent_n is null) THEN
    SELECT N INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ
      AND now() BETWEEN SO.FD AND SO.TD;
  ELSE
    SELECT N INTO new_id FROM SYS_OBJ SO
      WHERE SO.OBJ_NAME = in_sysobj_name AND SO.TYP = in_sys_typ AND SO.UP = in_parent_n
      AND now() BETWEEN SO.FD AND SO.TD;
  END IF;

  return(new_id);
END;
$func$ LANGUAGE plpgsql;
-- FUNCTION get_SYSOBJ_N_BY_NAME_TYP





--======================================================================================================================
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.add_Sys_Obj (
    in_typ      NUMERIC,
    in_objName  character varying(2000),
    in_up       NUMERIC,
    in_dsc      character varying(2000)
  ) RETURNS NUMERIC AS $func$

DECLARE
  new_id      NUMERIC:=0;
  paramNumber NUMERIC:=0;

BEGIN

  -- проверка пришедших параметров
  -- проверяем тип
  IF (in_typ is null) then
    RAISE EXCEPTION 'Type parameter must not be null.';
  ELSIF (in_typ < 1) THEN
    RAISE EXCEPTION 'Type parameter length must be greater than zero. in_typ: %', in_typ;
  END IF;

  -- проверяем in_objName на нал
  IF (in_objName IS NULL) THEN
    RAISE EXCEPTION 'in_objName parameter must not be null.';
  END IF;

  -- проверяем in_objName на повторениия
  SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO WHERE SO.OBJ_NAME = in_objName AND SO.up = in_up;
  IF (paramNumber > 0) THEN
    RAISE EXCEPTION 'Objadd_Sys_Obj_FDTD: %, up: %', in_objName , in_up;
  END IF;

  -- проверяем in_up на нал
  IF (in_up IS NULL) THEN
    paramNumber := 0;
    SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO WHERE SO.OBJ_NAME = in_objName AND SO.up IS NULL;
    IF (paramNumber > 0) THEN
      RAISE EXCEPTION 'ObjName parameter must be unique. in_objName: %', in_objName;
    END IF;
  END IF;

  -- проверяем in_up
  IF (in_up IS NULL AND in_typ = INE_CORE_SYS_OBJ.COLUMN_TYP()) THEN
    RAISE EXCEPTION 'Incompatible combination: in_typ[%] and in_up[%], in_up must not be null', in_typ ,in_up;
  END IF;


  paramNumber := 0;

  -- проверяем in_up
  IF (in_up IS NOT NULL) THEN
--    dbms_output.put_line('Recive: ' || in_up);

    SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO WHERE SO.N = in_up;
--    dbms_output.put_line('Count: ' || paramNumber);

    IF (paramNumber < 1) THEN
      RAISE EXCEPTION 'in_up parameter must be from real objects.';
    END IF;
  END IF;

   -- проверяем in_dsc на нал
  IF (in_dsc IS NULL) THEN
    RAISE EXCEPTION 'in_dsc parameter must not be null.';
  ELSIF (LENGTH(in_dsc) < 10) THEN
    RAISE EXCEPTION 'in_dsc parameter length must be greater than 9.';
  END IF;



  paramNumber := nextval('SEQ_SYS_OBJ_N');

  INSERT INTO
    SYS_OBJ (N, TYP, OBJ_NAME, UP, DSC, FD, TD)
  VALUES
    (paramNumber, in_typ, in_objName, in_up, in_dsc, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

  new_id := paramnumber;

  RETURN(new_id);
END;
$func$ LANGUAGE plpgsql;
-- FUNCTION add_Sys_Obj


--======================================================================================================================
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.add_Pattern (
  in_id        NUMERIC,
  in_pattern   character varying(2000)
) RETURNS void AS $func$

DECLARE
  typ NUMERIC;
  typ2 NUMERIC;
BEGIN

  -- проверяем in_id на нал
  IF (in_id IS NULL) THEN
    RAISE EXCEPTION 'in_id parameter must not be null.';
  END IF;

  SELECT SYS_OBJ.TYP INTO typ FROM SYS_OBJ WHERE SYS_OBJ.N = in_id;

  -- проверяем, что in_pattern задан только для типа "столбец"
  IF (typ != INE_CORE_SYS_OBJ.COLUMN_TYP()) THEN
    RAISE EXCEPTION 'Incompatible combination: in_typ[%] and in_pattern[%], in_typ must be equals to %',
     typ, in_pattern, COLUMN_TYP;
  END IF;

  UPDATE SYS_OBJ SET PATTERN = in_pattern WHERE N = in_id;

END;
$func$ LANGUAGE plpgsql;
-- PROCEDURE add_Pattern


--======================================================================================================================
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.add_Sys_Obj (
    in_typ      NUMERIC,
    in_objName  character varying(2000),
    in_up       NUMERIC,
    in_dsc      character varying(2000),
    in_pattern  character varying(2000)
  ) RETURNS NUMERIC AS $func$
DECLARE
  new_id NUMERIC;
BEGIN
  new_id := INE_CORE_SYS_OBJ.add_Sys_Obj(in_typ, in_objName, in_up, in_dsc);
  PERFORM INE_CORE_SYS_OBJ.add_Pattern(new_id, in_pattern);
  RETURN(new_id);
END;
$func$ LANGUAGE plpgsql;
-- FUNCTION add_Sys_Obj

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.add_Sys_Obj_FD (in_up NUMERIC) RETURNS NUMERIC AS $func$
DECLARE
  retValue NUMERIC:= 0;
BEGIN
  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP(),
    INE_CORE_SYS_OBJ.FD_VAL(),
    in_up,
    INE_CORE_SYS_OBJ.FD_DSC()
  );

  RETURN(retValue);
END;
 $func$ LANGUAGE plpgsql;
-- FUNC add_Sys_Obj_FD


------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.add_Sys_Obj_TD (in_up NUMERIC) RETURNS NUMERIC AS $func$
DECLARE
  retValue NUMERIC := 0;
BEGIN
  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP(),
    INE_CORE_SYS_OBJ.TD_VAL(),
    in_up,
    INE_CORE_SYS_OBJ.TD_DSC()
  );

  return(retValue);
END;
 $func$ LANGUAGE plpgsql;
-- FUNCTION add_Sys_Obj_TD


------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD (in_up NUMERIC) RETURNS NUMERIC AS $func$
DECLARE
  retValue NUMERIC:= 0;
BEGIN
  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP(),
    INE_CORE_SYS_OBJ.FD_VAL(),
    in_up,
    INE_CORE_SYS_OBJ.FD_DSC()
  );

  retValue := INE_CORE_SYS_OBJ.ADD_SYS_OBJ(
    INE_CORE_SYS_OBJ.COLUMN_TYP(),
    INE_CORE_SYS_OBJ.TD_VAL(),
    in_up,
    INE_CORE_SYS_OBJ.TD_DSC()
  );

  return(retValue);
END;
 $func$ LANGUAGE plpgsql;
--FUNCTION add_Sys_Obj_FDTD

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR (in_table_name character varying(2000))
 RETURNS NUMERIC AS $func$
DECLARE
  retValue             NUMERIC:= 0;
  paramNumber          NUMERIC := 0;
  history_Table_Name   character varying(2000);
  cursNum              NUMERIC;
  result               NUMERIC;
  sysObjDsc            character varying(2000);
  system_ID            NUMERIC;
  fkSystemId           NUMERIC;
  v_TABLE_DSCS         SYS_OBJ%rowtype;
BEGIN

  history_Table_Name := in_table_name || INE_CORE_SYS_OBJ.HIST_SFX();

  -- Проверяем на отсутствие таблицы с историей в реестре
  SELECT COUNT(*) INTO paramNumber FROM SYS_OBJ SO
    WHERE SO.OBJ_NAME = history_Table_Name AND SO.TYP = INE_CORE_SYS_OBJ.TABLE_TYP()
    AND now() BETWEEN SO.FD AND SO.TD;
  IF (paramNumber <> 0) THEN
    RAISE EXCEPTION 'Target TableName parameter is exists. table_Name: %', history_Table_Name;
  END IF;

  -- Проверяем на наличие таблицы-оригинала в реестре
  paramNumber := INE_CORE_SYS_OBJ.get_Table_N(in_table_name);

  -- Теперь формируем DDL
  EXECUTE 'CREATE TABLE ' || history_Table_Name ||' AS (SELECT * FROM ' || in_table_name || ' WHERE 1=2)';


  SELECT SO.DSC INTO sysObjDsc FROM SYS_OBJ SO
    WHERE SO.OBJ_NAME = in_table_name AND SO.TYP = INE_CORE_SYS_OBJ.TABLE_TYP() AND now() BETWEEN SO.FD AND SO.TD;

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), history_Table_Name, paramNumber, 'History: '|| sysObjDsc);

  FOR v_TABLE_DSCS IN SELECT * FROM SYS_OBJ SO
    WHERE now() BETWEEN SO.FD AND SO.TD AND SO.TYP = INE_CORE_SYS_OBJ.COLUMN_TYP() AND SO.UP IN
         (
          SELECT SO2.N FROM SYS_OBJ SO2 WHERE SO2.OBJ_NAME = in_table_name AND SO2.TYP = INE_CORE_SYS_OBJ.TABLE_TYP() AND now() BETWEEN SO2.FD AND SO2.TD
         )
     LOOP

    fkSystemId := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), v_TABLE_DSCS.OBJ_NAME, system_ID, v_TABLE_DSCS.DSC);

  END LOOP;

  return(retValue);
END;
 $func$ LANGUAGE plpgsql;
-- FUNCTION add_Sys_Obj_HSTR




------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.obtainSQLTyp(in_java_typ character varying(2000))
  RETURNS character varying(2000) AS $func$
BEGIN
  IF(in_java_typ = INE_CORE_SYS_OBJ.CUSTOM_TYP_STRING())      THEN return('character varying(2000)');  END IF;
  IF(in_java_typ = INE_CORE_SYS_OBJ.CUSTOM_TYP_LONG())        THEN return('NUMERIC');                  END IF;
  IF(in_java_typ = INE_CORE_SYS_OBJ.CUSTOM_TYP_BIG_DECIMAL()) THEN return('NUMERIC');                  END IF;
  IF(in_java_typ = INE_CORE_SYS_OBJ.CUSTOM_TYP_BOOLEAN())     THEN return('NUMERIC(1)');               END IF;
  IF(in_java_typ = INE_CORE_SYS_OBJ.CUSTOM_TYP_DATE())        THEN return('TIMESTAMP WITH TIME ZONE'); END IF;

  -- а вот если до этого момента не вышли, то однозначно - хрень и падать надо!!!
  RAISE EXCEPTION 'Incorrect given type [%]', in_java_typ;
END;
$func$ LANGUAGE plpgsql;
--FUNCTION obtainSQLTyp

------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.create_CustomAttrs_Tables(in_parent_name character varying(2000))
  RETURNS void AS $func$
DECLARE
  paramNumber      NUMERIC;
BEGIN
  paramNumber := INE_CORE_SYS_OBJ.get_Table_N(in_parent_name);

  paramNumber := INE_CORE_SYS_OBJ.create_Custom_Table(in_parent_name, INE_CORE_SYS_OBJ.CUSTOM_TYP_STRING());
  paramNumber := INE_CORE_SYS_OBJ.create_Custom_Table(in_parent_name, INE_CORE_SYS_OBJ.CUSTOM_TYP_LONG());
  paramNumber := INE_CORE_SYS_OBJ.create_Custom_Table(in_parent_name, INE_CORE_SYS_OBJ.CUSTOM_TYP_BIG_DECIMAL());
  paramNumber := INE_CORE_SYS_OBJ.create_Custom_Table(in_parent_name, INE_CORE_SYS_OBJ.CUSTOM_TYP_BOOLEAN());
  paramNumber := INE_CORE_SYS_OBJ.create_Custom_Table(in_parent_name, INE_CORE_SYS_OBJ.CUSTOM_TYP_DATE());

END;
$func$ LANGUAGE plpgsql;
-- PROCEDURE create_CustomAttrs_Tables


------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION INE_CORE_SYS_OBJ.create_Custom_Table(
  in_parent_name character varying(2000),
  in_val_typ character varying(2000)
)
  RETURNS NUMERIC AS $func$
DECLARE
  sqlFirstPart    character varying(2000);
  sqlLastPart     character varying(2000);
  tableAttrName   character varying(255);
  index1          character varying(100);
  index2          character varying(100);
  columnType      character varying(50);

  cursNum         NUMERIC;
  paramNumber     NUMERIC;
  fakeId          NUMERIC;
  new_id          NUMERIC;
BEGIN
  new_id := 0;

  -- проверяем тип таблицы
  IF(length(in_val_typ) < 1) THEN
    RAISE EXCEPTION 'TypeName parameter is invalid.';
  END IF;

  -- проверяем наличие родительской таблицы
  paramNumber := INE_CORE_SYS_OBJ.get_Table_N(in_parent_name);

  tableAttrName := in_parent_name || '_' || in_val_typ;

  -- Проверяем на наличие таблицы атрибутов в реестре
  SELECT COUNT(*) INTO new_id FROM SYS_OBJ SO
    WHERE SO.OBJ_NAME = tableAttrName AND SO.TYP = INE_CORE_SYS_OBJ.TABLE_TYP() AND now() BETWEEN SO.FD AND SO.TD;
  IF (new_id <> 0) THEN
    RAISE EXCEPTION 'CustomAttrs TableName exists.';
  END IF;

  -- подбиваем названия индексов
  index1 := 'UK_'|| tableAttrName || '_ID';
  index2 := 'IND_'|| tableAttrName || '_A';

  IF(length(index1) > 29) THEN
    index1 := in_parent_name || in_val_typ || '1';
  END IF;

  IF(length(index2) > 29) THEN
    index2 := in_parent_name || in_val_typ || '2';
  END IF;
  -- eсли не хватит, разруливать руками...

  -- получаем тип для SQL
  columnType := INE_CORE_SYS_OBJ.obtainSQLTyp(in_val_typ);

  sqlFirstPart := 'CREATE TABLE ' || tableAttrName || '('
    || 'ENT_N NUMERIC NOT NULL,'
    || ' ATTR_N NUMERIC NOT NULL,'
    || ' ATTR_VAL ' || columnType || ' NOT NULL,'
    || ' DSC character varying(50),'
    || ' FD TIMESTAMP WITH TIME ZONE NOT NULL,'
    || ' TD TIMESTAMP WITH TIME ZONE NOT NULL,'
    || ' CONSTRAINT ' || index1 || ' UNIQUE (ENT_N, ATTR_N))';

  -- выполняем DDL
  EXECUTE sqlFirstPart;

  sqlFirstPart := 'CREATE INDEX '|| index2 || ' ON ' || tableAttrName || '(ATTR_N)';
  EXECUTE sqlFirstPart;

  -- Заносим все в реестр
  new_id := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), tableAttrName, paramNumber, 'Таблица произволных атрибутов для: ' || in_parent_name || ', тип: ' || in_val_typ);
  fakeId := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.structure.CustomAttributeValue', new_id, 'Интерфейс значения произвольного атрибута');
  fakeId := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ENT_N', new_id, 'Ссылка на N в таблице ' || in_parent_name);
  fakeId := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ATTR_N', new_id, 'Ссылка на ATTR_N в таблице описания произвольных атрибутов');
  fakeId := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ATTR_VAL', new_id, 'Значение атрибута');
  fakeId := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', new_id, 'Коментарий. Поле не используется!');
  fakeId := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(new_id);

  -- Теперь создаем историю
  fakeId := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR(tableAttrName);

  return(new_id);
END;
$func$ LANGUAGE plpgsql;
-- FUNCTION create_Custom_Table




-- END INE_CORE_SYS_OBJ;


/*
-- создаем пакет INE_CORE_SYS_OBJ
create or replace PACKAGE INE_CORE_SYS_OBJ AS

  -- Объявляем константы...
  -- минимальное значение для идентификатора в системе
  --MIN_ALLOWABLE_VAL    CONSTANT NUMBER :=  1;

  -- тип системного объекта Таблица
  TABLE_TYP      CONSTANT NUMBER :=  1;
  -- тип системного объекта Столбец
  COLUMN_TYP     CONSTANT NUMBER :=  2;
  -- тип системного объекта Последовательность
  SEQUENCE_TYP   CONSTANT NUMBER :=  3;
  -- тип системного объекта Интерфейс
  INTERFACE_TYP  CONSTANT NUMBER :=  4;




  -- Системная дата начала жизни
  sysFD CONSTANT DATE := INE_CORE_SYS.GET_SYSTEM_FROMDATE;
  -- Системная дата закрытия версии
  sysTD CONSTANT DATE := INE_CORE_SYS.GET_SYSTEM_TODATE;

  -- FD, TD значение названия
  FD_VAL CONSTANT VARCHAR2(255) := 'FD';
  TD_VAL CONSTANT VARCHAR2(255) := 'TD';




  -- FD, TD описание
  fd_Dsc CONSTANT VARCHAR2(255) := 'Дата начала жизни';
  td_Dsc CONSTANT VARCHAR2(255) := 'Дата закрытия версии';




  -- суффикс исторических таблиц
  HIST_SFX    CONSTANT    VARCHAR2(255) := '_HSTR';

  -- типы кастомных таблиц
  CUSTOM_TYP_STRING       CONSTANT  VARCHAR2(255) := 'String';
  CUSTOM_TYP_LONG         CONSTANT  VARCHAR2(255) := 'Long';
  CUSTOM_TYP_BIG_DECIMAL  CONSTANT  VARCHAR2(255) := 'BigDecimal';
  CUSTOM_TYP_BOOLEAN      CONSTANT  VARCHAR2(255) := 'Boolean';
  CUSTOM_TYP_DATE         CONSTANT  VARCHAR2(255) := 'Date';

*/
