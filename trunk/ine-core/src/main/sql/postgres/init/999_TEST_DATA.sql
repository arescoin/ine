/*
 * $Id: 999_TEST_DATA.sql 29 2017-04-04 15:32:19Z xerror $
 */
/*
 * Создание различных тестовых данных
 * НЕ ДЛЯ ВЫКЛАДЫВАНИЯ НА PRODUCTION!
*/

/* ================================================================================================================== */

-- 013_INE_CORE_ENTITIES.sql
DO $$

DECLARE
  VFD  TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD  TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (1, 'DSC1',  VFD, VTD, 'Test Custom Company', 1, 1);

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (2, 'DSC2',  VFD, VTD, 'Test State Company', 2, 2);

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (3, 'DSC2',  VFD, VTD, 'Customer Company', 2, 2);


  INSERT INTO PERSON (N, DSC, FD, TD, FAMILY_NAME, NAME, MIDDLE_NAME, ALIAS,
                      FAMILY_NAME_PREFIX, FAMILY_GENERATION, ADDRESS_FORM)
  VALUES (1, 'DSC1',  VFD, VTD, 'Family1', 'Name1', 'Middle1', 'Alias1', 1, 'Generation1', 'AddressForm1');

  INSERT INTO PERSON (N, DSC, FD, TD, FAMILY_NAME, NAME, MIDDLE_NAME, ALIAS,
                      FAMILY_NAME_PREFIX, FAMILY_GENERATION, ADDRESS_FORM)
  VALUES (2, 'DSC2',  VFD, VTD, 'Family2', 'Name2', 'Middle2', 'Alias2', 1, 'Generation2', 'AddressForm2');

END$$;


/* ================================================================================================================== */

-- 104_INE_CORE_DATA_DIC.sql
DO $$
DECLARE
  LINK_ID NUMERIC;

  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  -- [11] населенные пункты OSS/J
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(11, 1, 1, 'Москва', 77, VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(11, 2, 1, 'Санкт-Петербург', 78, VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(11, 3, 1, 'Выборг', 47, VFD, VTD);

  -- [13] улицы OSS/J
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(13, 1, 1, 'Новокосинская', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(13, 2, 1, '1905 года', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(13, 3, 1, '40 лет Комсомола', 3,VFD, VTD);

  -- [14] почтовые индексы OSS/J
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(14, 1, 1, '111672', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(14, 2, 1, '123100', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(14, 3, 1, '188800', 3,VFD, VTD);


  --связь названий и типов населенных пунктов
  SELECT N INTO LINK_ID FROM LINKS WHERE TYP = 1
  AND LEFT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':10]'
                       ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE')
  AND RIGHT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':11]'
                        ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');

  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(1,LINK_ID,1,1,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(2,LINK_ID,1,2,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(3,LINK_ID,1,3,VFD,VTD);

  --связь названий и типов улиц
  SELECT N INTO LINK_ID FROM LINKS WHERE TYP = 2
  AND LEFT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':12]'
                       ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE')
  AND RIGHT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':13]'
                        ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');

  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(4,LINK_ID,1,1,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(5,LINK_ID,1,2,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(6,LINK_ID,7,3,VFD,VTD);


END$$;


/* ================================================================================================================== */

-- 105_INE_CORE_DATA_USPTS.sql
DO $$
DECLARE

  ROLE_SYS_ADMIN NUMERIC ;
  ROLE_BRANCH_OFFICER NUMERIC ;

  NUM NUMERIC ;

  VFD TIMESTAMP WITH TIME ZONE  := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE  := INE_CORE_SYS.GET_SYSTEM_TODATE();

BEGIN

  SELECT N INTO ROLE_SYS_ADMIN FROM ROLES WHERE ROLE_NAME='SYS_ADMIN';
  SELECT N INTO ROLE_BRANCH_OFFICER FROM ROLES WHERE ROLE_NAME='BRANCH_OFFICER';

  -- Пользователи
  -- Зверек root1, пароль 'root'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'root1', '63a9f0ea7bb98050796b649e85481845', 1, 'Root User 1', VFD, VTD);
  -- Роль: Админ
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  -- Зверек никто 1, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody2', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 2', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 2, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody3', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 3', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 3, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody4', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 4', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 4, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody5', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 5', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 5 неактивный, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody6', '6e854442cd2a940c9e95941dce4ad598', 0, 'Test User 6', VFD, VTD);
  -- Роль: Работник (блокированый)
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

END$$;


/* ================================================================================================================== */

-- 106_INE_CORE_DATA_CONSTANTS.sql
DO $$
DECLARE
  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  -- константы для тестов
  INSERT INTO CONSTANTS (N, CONST_NAME, DEF_VAL, IS_NULLABLE, IS_MODYFABLE, FD, TD)
  VALUES(1, 'TST_CONSTANT', '1', 0, 1, VFD, VTD);

  INSERT INTO CONSTANTS (N, CONST_NAME, DEF_VAL, IS_NULLABLE, IS_MODYFABLE, FD, TD)
  VALUES(2, 'TST_PARAMS_CONSTANT', '1', 0, 1, VFD, VTD);


END$$;


/* ================================================================================================================== */

-- 107_INE_CORE_DATA_FUNCSW.sql
INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES(1, 'TEST_SWITCH', 'Тестовй переключатель. Не менять!');


/* ================================================================================================================== */

-- 111_INE_CORE_DATA_CUSTOM_ATTRS_DSC.sql
DO $$
DECLARE
  SYS_VAR NUMERIC;
  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  SYS_VAR := INE_CORE_SYS_OBJ.get_Table_N('TEST_TABLE');

  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    1, 'Test value #1', 'Test data #1. Do not edit or remove !!!', VFD, VTD);
  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    2, 'Test value #2', 'Test data #2. Do not edit or remove !!!', VFD, VTD);
  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    3, 'Test value #3', 'Test data #3. Do not edit or remove !!!', VFD, VTD);
  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    4, 'Test value #4', 'Test data #4. Do not edit or remove !!!', VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    1, SYS_VAR, 1, 'testStringAttribute', 1, 'String attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_STRING (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 1, 'Test attribute value #1', VFD, VTD);
  insert into TEST_TABLE_STRING (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 1, 'Test attribute value #2', VFD, VTD);
  insert into TEST_TABLE_STRING (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 1, 'Test attribute value #3', VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    2, SYS_VAR, 2, 'testLongAttribute', 1, 'Long attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_LONG (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 2, 1, VFD, VTD);
  insert into TEST_TABLE_LONG (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 2, 2, VFD, VTD);
  insert into TEST_TABLE_LONG (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 2, 3, VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    3, SYS_VAR, 3, 'testBigDecimalAttribute', 1, 'BigDecimal attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_BIGDECIMAL (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 3, 1.1, VFD, VTD);
  insert into TEST_TABLE_BIGDECIMAL (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 3, 2.2, VFD, VTD);
  insert into TEST_TABLE_BIGDECIMAL (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 3, 3.3, VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    4, SYS_VAR, 4, 'testDateAttribute', 1, 'Date attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_DATE (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 4, CURRENT_TIMESTAMP + time '20:00', VFD, VTD);
  insert into TEST_TABLE_DATE (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 4, CURRENT_TIMESTAMP + time '20:00', VFD, VTD);
  insert into TEST_TABLE_DATE (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 4, CURRENT_TIMESTAMP + time '20:00', VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    5, SYS_VAR, 5, 'testBooleanAttribute', 0, 'Boolean attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_BOOLEAN (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 5, 1, VFD, VTD);
  insert into TEST_TABLE_BOOLEAN (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 5, 0, VFD, VTD);


END$$;
