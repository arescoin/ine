/*
"$Id: 005_INE_CORE_USPTS.sql 29 2017-04-04 15:32:19Z xerror $"
*/

CREATE TABLE ROLES
(
  N            NUMERIC                       NOT NULL
, ROLE_NAME    CHARACTER VARYING(2000)       NOT NULL
, DSC          CHARACTER VARYING(2000)       NOT NULL
, FD           TIMESTAMP WITH TIME ZONE      NOT NULL
, TD           TIMESTAMP WITH TIME ZONE      NOT NULL
, CONSTRAINT   UK_ROLES_ID   UNIQUE (N)
);

--  Создаем индексы
CREATE INDEX IND_ROLES_FDTD ON ROLES(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_ROLES INCREMENT BY 1 START WITH 1;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'ROLES', null, 'Содержит описание ситемных ролей');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.userpermits.Role', system_ID, 'Интерфейс описания роли');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Системный номер роли');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ROLE_NAME', system_ID, 'Системное имя роли (уникальное)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание роли');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_ROLES', system_ID, 'Последовательность для идентификатора');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('ROLES');

END$$;


CREATE TABLE PERMITS
(
  N             NUMERIC                     NOT NULL
, PERMIT_NAME   CHARACTER VARYING(2000)     NOT NULL
, VAL_MASK      CHARACTER VARYING(2000)
, IS_VAL_REQ    NUMERIC                     NOT NULL
, DSC           CHARACTER VARYING(2000)     NOT NULL
, FD            TIMESTAMP WITH  TIME ZONE   NOT NULL
, TD            TIMESTAMP WITH  TIME ZONE   NOT NULL
, CONSTRAINT    UK_PERMITS_ID  UNIQUE (N)
);

--  Создаем индексы
CREATE INDEX IND_PERMITS_NAME    ON PERMITS(PERMIT_NAME);
CREATE INDEX IND_PERMITS_FDTD    ON PERMITS(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_PERMITS INCREMENT BY 1 START WITH 1;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'PERMITS', null, 'Содержит описание системных доступов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.userpermits.Permit', system_ID, 'Интерфейс описания доступа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Системный номер доступа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PERMIT_NAME', system_ID, 'Системное имя доступа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VAL_MASK', system_ID, 'Маска для проверки значения доступа');
     -- TODO я так понимаю, здесь у нас должен быть системный паттерн и нужна проверка его при сохранении?

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_VAL_REQ', system_ID, 'Признак необходимости значения (1 - требуется, иначе - 0)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание доступа');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_PERMITS', system_ID, 'Последовательность для идентификатора');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('PERMITS');

END$$;


CREATE TABLE ROLE_PERMITS
(
  ROLE_N       NUMERIC                   NOT NULL -- есть паттерн
, PERMIT_N     NUMERIC                   NOT NULL -- есть паттерн
, CRUD_MASK    NUMERIC                   NOT NULL
, PERMIT_VAL   CHARACTER VARYING(2000)
, DSC          CHARACTER VARYING(2000)   NOT NULL
, FD           TIMESTAMP WITH TIME ZONE  NOT NULL
, TD           TIMESTAMP WITH TIME ZONE  NOT NULL
, CONSTRAINT UK_ROLE_PERMITS_ID UNIQUE (ROLE_N, PERMIT_N)
);

--  Создаем индексы
CREATE INDEX IND_ROLE_PERMITS_RN     ON ROLE_PERMITS(ROLE_N);
CREATE INDEX IND_ROLE_PERMITS_PN     ON ROLE_PERMITS(PERMIT_N);
CREATE INDEX IND_ROLE_PERMITS_FDTD   ON ROLE_PERMITS(FD, TD);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC ;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'ROLE_PERMITS', null, 'Содержит конфигурацию ролей, здесь хранятся наборы доступов для каждой роли');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.userpermits.RolePermit', system_ID, 'Интерфейс ролей доступа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ROLE_N', system_ID, 'Ссылка на системный номер роли', '' || INE_CORE_SYS_OBJ.get_Column_N('ROLES', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PERMIT_N', system_ID, 'Ссылка на системный номер доступа', '' || INE_CORE_SYS_OBJ.get_Column_N('PERMITS', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CRUD_MASK', system_ID, 'Бинарная маска, определяет набор доступных действий пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PERMIT_VAL', system_ID, 'Содержит значение доступа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание связки');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('ROLE_PERMITS');

END$$;



CREATE TABLE SYS_USERS
(
  N           NUMERIC                    NOT NULL
, SYS_LOGIN   CHARACTER VARYING(255)     NOT NULL
, MD5_HASH    CHARACTER VARYING(32)      NOT NULL
, IS_ACTIVE   NUMERIC                    NOT NULL
, DSC         CHARACTER VARYING(2000)
, FD          TIMESTAMP WITH TIME ZONE   NOT NULL
, TD          TIMESTAMP WITH TIME ZONE   NOT NULL
, CONSTRAINT  UK_SYS_USERS_ID  UNIQUE (N)
, CONSTRAINT  UK_SYS_USERS     UNIQUE (SYS_LOGIN)
);

--  Создаем индексы
CREATE INDEX IND_SYS_USERS_ACT      ON SYS_USERS(IS_ACTIVE);
CREATE INDEX IND_SYS_USERS_FDTD     ON SYS_USERS(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_SYS_USERS INCREMENT BY 1 START WITH 1;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC ;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'SYS_USERS', null, 'Содержит системных пользователей');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.userpermits.SystemUser', system_ID, 'Интерфейс системного пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Системный номер пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SYS_LOGIN', system_ID, 'Системное имя пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'MD5_HASH', system_ID, 'MD5 хэш код пароля пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_ACTIVE', system_ID, 'Признак активности');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_SYS_USERS', system_ID, 'Последовательность для идентификатора');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('SYS_USERS');

END$$;


CREATE TABLE USER_ROLES
(
  USER_N       NUMERIC                   NOT NULL -- есть паттерн
, ROLE_N       NUMERIC                   NOT NULL -- есть паттерн
, IS_ACTIVE    NUMERIC                   NOT NULL
, DSC          CHARACTER VARYING(2000)   NOT NULL
, FD           TIMESTAMP WITH TIME ZONE  NOT NULL
, TD           TIMESTAMP WITH TIME ZONE  NOT NULL
, CONSTRAINT   UK_USER_ROLES UNIQUE (USER_N, ROLE_N)
);

--  Создаем индексы
CREATE INDEX IND_USER_ROLES_UN     ON  USER_ROLES(USER_N);
CREATE INDEX IND_USER_ROLES_RN     ON  USER_ROLES(ROLE_N);
CREATE INDEX IND_USER_ROLES_ISA    ON  USER_ROLES(IS_ACTIVE);
CREATE INDEX IND_USER_ROLES_UA     ON  USER_ROLES(USER_N, IS_ACTIVE);
CREATE INDEX IND_USER_ROLES_FDTD   ON  USER_ROLES(FD, TD);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC ;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'USER_ROLES', null, 'Содержит список ролей для пользователя');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.userpermits.UserRole', system_ID, 'Интерфейс ролей пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'USER_N', system_ID, 'Ссылка на системный номер пользователя, SYS_USERS.N', '' || INE_CORE_SYS_OBJ.get_Column_N('SYS_USERS', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ROLE_N', system_ID, 'Ссылка на системный номер роли', '' || INE_CORE_SYS_OBJ.get_Column_N('ROLES', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_ACTIVE', system_ID, 'Признак активности (1 - активна, иначе - 0)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание связки');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('USER_ROLES');

END$$;

CREATE TABLE USER_PERMITS
(
  USER_N       NUMERIC                    NOT NULL -- есть паттерн
, ROLE_N       NUMERIC                    NOT NULL -- есть паттерн
, PERMIT_N     NUMERIC                    NOT NULL -- есть паттерн
, CRUD_MASK    NUMERIC                    NOT NULL
, PERMIT_VAL   CHARACTER VARYING(2000)
, IS_ACTIVE    NUMERIC                    NOT NULL
, DSC          CHARACTER VARYING(2000)    NOT NULL
, FD           TIMESTAMP WITH TIME ZONE   NOT NULL
, TD           TIMESTAMP WITH TIME ZONE   NOT NULL
, CONSTRAINT   UK_USER_PERMITS_ID    UNIQUE (USER_N, ROLE_N, PERMIT_N)
);

--  Создаем индексы
CREATE INDEX  IND_USER_PERMITS_UN      ON  USER_PERMITS(USER_N);
CREATE INDEX  IND_USER_PERMITS_RN      ON  USER_PERMITS(ROLE_N);
CREATE INDEX  IND_USER_PERMITS_PN      ON  USER_PERMITS(PERMIT_N);
CREATE INDEX  IND_USER_PERMITS_ISA     ON  USER_PERMITS(IS_ACTIVE);
CREATE INDEX  IND_USER_PERMITS_FDTD    ON  USER_PERMITS(FD, TD);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC ;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'USER_PERMITS', null, 'Содержит список доступов для пользователя');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.userpermits.UserPermit', system_ID, 'Интерфейс доступов пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'USER_N', system_ID, 'Ссылка на системный номер пользователя, SYS_USERS.N', '' || INE_CORE_SYS_OBJ.get_Column_N('SYS_USERS', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ROLE_N', system_ID, 'Ссылка на системный номер роли, подвергшейся модерированию, ROLES.N', '' || INE_CORE_SYS_OBJ.get_Column_N('ROLES', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PERMIT_N', system_ID, 'Ссылка на системный номер доступа, PERMITS.N', '' || INE_CORE_SYS_OBJ.get_Column_N('PERMITS', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CRUD_MASK', system_ID, 'Бинарная маска, определяет набор доступных действий пользователя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PERMIT_VAL', system_ID, 'Содержит значение доступа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_ACTIVE', system_ID, 'Признак активности (1 - активна, иначе - 0)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание связки');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('USER_PERMITS');


-- Обновляем информацию в SYS_OBJ о столбцах с паттернами в табличке OBJ_HISTORY
  PERFORM INE_CORE_SYS_OBJ.add_Pattern(INE_CORE_SYS_OBJ.get_Column_N('OBJ_HISTORY', 'USER_ID'), '' || INE_CORE_SYS_OBJ.get_Column_N('SYS_USERS', 'N'));
END$$;