/*
"$Id: 001_sys_obj.sql 36 2017-04-06 00:05:15Z xerror $"
*/

-- Создаем корневую таблицу системы
CREATE TABLE SYS_OBJ
(
  N             NUMERIC          NOT NULL
, TYP           NUMERIC          NOT NULL -- есть паттерн
, OBJ_NAME      character varying(2000)  NOT NULL
, UP            NUMERIC                   -- есть паттерн
, PATTERN       character varying(200)
, DSC           character varying(2000)
, FD            TIMESTAMP WITH TIME ZONE      NOT NULL
, TD            TIMESTAMP WITH TIME ZONE      NOT NULL
, CONSTRAINT PK_SYS_OBJ     PRIMARY KEY (N)
, CONSTRAINT FK_SYS_OBJ_UPN FOREIGN KEY (UP) REFERENCES SYS_OBJ(N)
);

  -- Накатываем индексы для SYS_OBJ
  CREATE INDEX IND_SYS_OBJ_TYP      ON   SYS_OBJ (TYP);
  CREATE INDEX IND_SYS_OBJ_NAME_UP  ON   SYS_OBJ (OBJ_NAME, UP);
  CREATE INDEX IND_SYS_OBJ_FDTD     ON   SYS_OBJ (FD, TD);

-- Теперь создаем сиквенс для идентификаторов в SYS_OBJ
CREATE SEQUENCE SEQ_SYS_OBJ_N INCREMENT 1 START 1;


\i 002_INE_CORE_SYS_pcg.sql
\i 003_INE_CORE_SYS_OBJ_pcg.sql
--==================================================

DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'SYS_OBJ', null, 'Содержит описание существующих в системе таблиц, и прочих объектов БД');

  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.structure.SystemObject', system_ID, 'Интерфейс системного объекта');
  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер элемента');
  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID, 'Тип элемента. Словарь №4');

  -- Паттерн будет добавлен позже, в скрипте 004_INE_CORE_DIC.sql
  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'OBJ_NAME', system_ID, 'Системное название объекта');
  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'UP', system_ID, 'Ссылка на системный номер родительского элемента');
  -- Паттерн будет добавлен позже, в скрипте 004_INE_CORE_DIC.sql

  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PATTERN', system_ID, 'Паттерн для описания ограничений на данные в столбце');
  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание элемента');
  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);
  FAKE_VAR  := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_SYS_OBJ_N', system_ID, 'Последовательность для идентификатора');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('SYS_OBJ');
END$$;


-- создаем таблицу с историей пользовательской активности
CREATE TABLE OBJ_HISTORY
 (
   N            NUMERIC                   NOT NULL,
   ENT_ID       CHARACTER VARYING(500)    NOT NULL,
   FD           TIMESTAMP WITH TIME ZONE  NOT NULL,
   TD           TIMESTAMP WITH TIME ZONE  NOT NULL,
   USER_ID      NUMERIC                   NOT NULL, -- есть паттерн
   ACTION_DATE  TIMESTAMP WITH TIME ZONE  NOT NULL,
   TYP          NUMERIC                   NOT NULL,
   REASON       CHARACTER VARYING(2000),
   PROCESSED    NUMERIC(1) DEFAULT 0     NOT NULL
 );

  -- Накатываем индексы для SYS_OBJ
  CREATE INDEX  IND_OBJ_HISTORY_N         ON   OBJ_HISTORY (N);
  CREATE INDEX  IND_OBJ_HISTORY_ENT       ON   OBJ_HISTORY (ENT_ID);
  CREATE INDEX  IND_OBJ_HISTORY_ACT_DATE  ON   OBJ_HISTORY (ACTION_DATE);
  CREATE INDEX  IND_OBJ_HISTORY_ACT_TYP   ON   OBJ_HISTORY (TYP);
  CREATE INDEX  IND_OBJ_HISTORY_FDTD      ON   OBJ_HISTORY (FD, TD);

DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'OBJ_HISTORY', null, 'Содержит историю изменений системных объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.history.VersionableHistory', system_ID, 'Интерфейс истории изменения версионного объекта');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер элемента (системного объекта)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ENT_ID', system_ID, 'Идентификатор сущности (либо системный номер, либо составной номер)');

  -- эт мы ставим от и до
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),'USER_ID', system_ID, 'Cистемный номер пользователя внесшего изменения');
  -- Паттерн будет добавлен позже, в скрипте 005_INE_CORE_USPTS.sql

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ACTION_DATE', system_ID, 'Дата активного действия');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID, 'Тип действия');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'REASON', system_ID, 'Обоснование действия');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PROCESSED', system_ID, 'Признак выполнения асинхронной обработки записи');
END$$;


-- создаем таблицу с пользовательскими атрибутами объектов истории пользовательской активности
CREATE TABLE OBJ_HISTORY_ATTRS
 (
   N            NUMERIC                   NOT NULL,
   ENT_ID       CHARACTER VARYING(500)    NOT NULL,
   ACTION_DATE  TIMESTAMP WITH TIME ZONE  NOT NULL,
   ATTR_NAME    CHARACTER VARYING(250)    NOT NULL,
   ATTR_VALUE   CHARACTER VARYING(2000)   NOT NULL
 );


DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'OBJ_HISTORY_ATTRS', null, 'Содержит пользовательские атрибуты истории изменений системных объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.history.ActionAttribute', system_ID, 'Интерфейс пользовательского атрибута истории изменений объектов системы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер элемента (системного объекта)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ENT_ID', system_ID, 'Идентификатор сущности (либо системный номер, либо составной номер)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ACTION_DATE', system_ID, 'Дата активного действия');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ATTR_NAME', system_ID, 'Имя пользовательского атрибута');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),'ATTR_VALUE', system_ID, 'Значение пользовательского атрибута');
END$$;
