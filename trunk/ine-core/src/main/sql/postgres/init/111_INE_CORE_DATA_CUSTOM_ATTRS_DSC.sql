/*
"$Id: 111_INE_CORE_DATA_CUSTOM_ATTRS_DSC.sql 29 2017-04-04 15:32:19Z xerror $"
*/


-- Создаем тестовые данные для дополнительных атрибутов
CREATE TABLE TEST_TABLE(
  N            NUMERIC                    NOT NULL
, VALUE        CHARACTER VARYING(2000)
, DSC          CHARACTER VARYING(2000)
, FD           TIMESTAMP WITH TIME ZONE   NOT NULL
, TD           TIMESTAMP WITH TIME ZONE   NOT NULL
);


-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_TEST_TABLE INCREMENT BY 1 START WITH 10;

DO $$

DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC;

BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'TEST_TABLE', null,
     'Тестовая таблица. На прод не выкатывать!!!');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(),
     'ru.xr.ine.core.structure.test.TestObject', system_ID, 'Интерфейс тестового объекта');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VALUE', system_ID, 'Тестовое значение');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

    -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),
     'SEQ_TEST_TABLE', system_ID, 'Последовательность для идентификатора');

 PERFORM INE_CORE_SYS_OBJ.create_CustomAttrs_Tables('TEST_TABLE');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('TEST_TABLE');


END$$;
