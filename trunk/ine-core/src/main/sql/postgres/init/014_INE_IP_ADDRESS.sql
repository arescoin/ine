/*
* $Id: 014_INE_IP_ADDRESS.sql 29 2017-04-04 15:32:19Z xerror $
*/


-- Логический Ресурс IP_ADDRESS
CREATE TABLE IP_ADDRESS
(
  N              NUMERIC                 NOT NULL
, IP_ADDR        BYTEA                   NOT NULL
, DSC            CHARACTER VARYING(2000)
, FD             TIMESTAMP WITH TIME ZONE               NOT NULL
, TD             TIMESTAMP WITH TIME ZONE               NOT NULL
, CONSTRAINT     UK_IP_ADDRESS   UNIQUE (N)
);

-- Последовательность id-шников
CREATE SEQUENCE SEQ_IP_ADDRESS_N INCREMENT BY 1 START WITH 10;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  VAR        NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'IP_ADDRESS', null, 'IP адрес в битовом представление');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.ipaddress.IPAddressValue', system_ID, 'Интерфейс IP адреса');
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Идентификатор');
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IP_ADDR', system_ID, 'значение IP-адреса');
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание ресурса');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_IP_ADDRESS_N', system_ID, 'Последовательность для идентификаторов');

  -- Добавляем историю
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('IP_ADDRESS');

  -- Структура для кастом атрибутов
  EXECUTE  INE_CORE_SYS_OBJ.create_CustomAttrs_Tables('IP_ADDRESS');

END$$;