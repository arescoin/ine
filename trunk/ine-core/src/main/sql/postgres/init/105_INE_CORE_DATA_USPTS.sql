/*
"$Id: 105_INE_CORE_DATA_USPTS.sql 29 2017-04-04 15:32:19Z xerror $"
*/

--  Создаем рутовую роль, пермиты и рутового пользователя + бесправного пользователя

/*
  SYS_ADMIN
  USER_ADMIN
  OBJECT_ADMIN
  EQUIPMENT_ADMIN
  BRANCH_OFFICER
  TECH_SPECIALIST
  TECH_SUPPORT_COORDINATOR
*/

DO $$
DECLARE

  -- Роли
  ROLE_SYS_ADMIN                NUMERIC := nextval('SEQ_ROLES');
  ROLE_USER_ADMIN               NUMERIC := nextval('SEQ_ROLES');
  ROLE_OBJECT_ADMIN             NUMERIC := nextval('SEQ_ROLES');
  ROLE_EQUIPMENT_ADMIN          NUMERIC := nextval('SEQ_ROLES');
  ROLE_BRANCH_OFFICER           NUMERIC := nextval('SEQ_ROLES');
  ROLE_TECH_SPECIALIST          NUMERIC := nextval('SEQ_ROLES');
  ROLE_TECH_SUPPORT_COORDINATOR NUMERIC := nextval('SEQ_ROLES');

  -- Доступы
  PERMIT_SYSTEM_ACCESS NUMERIC := nextval('SEQ_PERMITS');
  PERMIT_SYSOBJ_ACCESS NUMERIC := nextval('SEQ_PERMITS');
  PERMIT_PARTY_ACCESS  NUMERIC := nextval('SEQ_PERMITS');
  PERMIT_PARTY_MEMBER  NUMERIC := nextval('SEQ_PERMITS');

  -- Системные пользователи
  USER_ROOT    NUMERIC := nextval('SEQ_SYS_USERS');
  USER_ADAPTER NUMERIC := nextval('SEQ_SYS_USERS');
  USER_IMPORT  NUMERIC := nextval('SEQ_SYS_USERS');

  -- Web-пользователи
  USER_USER_ADMIN               NUMERIC := nextval('SEQ_SYS_USERS');
  USER_OBJECT_ADMIN             NUMERIC := nextval('SEQ_SYS_USERS');
  USER_EQUIPMENT_ADMIN          NUMERIC := nextval('SEQ_SYS_USERS');
  USER_BRANCH_OFFICER           NUMERIC := nextval('SEQ_SYS_USERS');
  USER_TECH_SPECIALIST          NUMERIC := nextval('SEQ_SYS_USERS');
  USER_TECH_SUPPORT_COORDINATOR NUMERIC := nextval('SEQ_SYS_USERS');

  -- Системные даты
  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();

BEGIN

  -----------------------------------------------------------------------------------------------------
  ------------------------------------------ Роли ----------------------------------------------------
  -----------------------------------------------------------------------------------------------------

  -- Роль SYS_ADMIN
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_SYS_ADMIN, 'SYS_ADMIN', 'Системный администратор', VFD, VTD);

  -- Роль USER_ADMIN
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_USER_ADMIN, 'USER_ADMIN', 'Администратор пользователей', VFD, VTD);

  -- Роль OBJECT_ADMIN
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_OBJECT_ADMIN, 'OBJECT_ADMIN', 'Администратор объектов учета', VFD, VTD);

  -- Роль EQUIPMENT_ADMIN
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_EQUIPMENT_ADMIN, 'EQUIPMENT_ADMIN', 'Администратор оборудования', VFD, VTD);

  -- Роль BRANCH_OFFICER
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_BRANCH_OFFICER, 'BRANCH_OFFICER', 'Сотрудник филиала', VFD, VTD);

  -- Роль TECH_SPECIALIST
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_TECH_SPECIALIST, 'TECH_SPECIALIST', 'Технический специалист', VFD, VTD);

  -- Роль TECH_SUPPORT_COORDINATOR
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_TECH_SUPPORT_COORDINATOR, 'TECH_SUPPORT_COORDINATOR', 'Координатор техподдержки', VFD, VTD);

  -----------------------------------------------------------------------------------------------------
  --------------------------------------------- Доступы -----------------------------------------------
  -----------------------------------------------------------------------------------------------------

  -- Доступ к системе
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_SYSTEM_ACCESS, 'System Access', null, 0, 'Разрешение на доступ к системе.', VFD, VTD);

  -- Доступ к системным объектам
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_SYSOBJ_ACCESS, 'System''s Objects Access', null, 0, 'Разрешение на доступ к системным объектам.', VFD, VTD);

  -- Доступ к объектам филиала
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_PARTY_ACCESS, 'Object User Access by Party', null, 0, 'Разрешение на доступ к объектам филиала.', VFD, VTD);

  -- Доступ определяющий принадлежность пользователя филиалу
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_PARTY_MEMBER, 'User to Party Relationship', null, 0, 'Принадлежность пользователя к филиалу.', VFD, VTD);

  -----------------------------------------------------------------------------------------------------
  ----------------------------------------- Настройка ролей -------------------------------------------
  -----------------------------------------------------------------------------------------------------

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_SYS_ADMIN, PERMIT_SYSTEM_ACCESS, 14, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_USER_ADMIN, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_OBJECT_ADMIN, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_EQUIPMENT_ADMIN, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_BRANCH_OFFICER, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_TECH_SPECIALIST, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_TECH_SUPPORT_COORDINATOR, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  -----------------------------------------------------------------------------------------------------
  ------------------------------------------ Пользователи --------------------------------------------
  -----------------------------------------------------------------------------------------------------

  -- Пользователь root, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, 'root', '63a9f0ea7bb98050796b649e85481845', 1, 'Root User', VFD, VTD);

  -- Пользователь apapter, пароль 'adapter'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ADAPTER, 'adapter', '8a7a38cfa57d4ac98cced700b804556a', 1, 'Adapter User', VFD, VTD);

  -- Пользователь import, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_IMPORT, 'import', '63a9f0ea7bb98050796b649e85481845', 1, 'Data Importer', VFD, VTD);

  ----------------------------------------------- web --------------------------------------------------
  -- Пользователь user admin, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, 'user_admin', '63a9f0ea7bb98050796b649e85481845', 1, 'User Admin', VFD, VTD);

  -- Пользователь object admin, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, 'obj_admin', '63a9f0ea7bb98050796b649e85481845', 1, 'Object Admin', VFD, VTD);

  -- Пользователь equipment admin, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, 'eq_admin', '63a9f0ea7bb98050796b649e85481845', 1, 'Equipment Admin', VFD, VTD);

  -- Пользователь branch officer, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, 'officer', '63a9f0ea7bb98050796b649e85481845', 1, 'Branch (Party) Officer', VFD, VTD);

  -- Пользователь tech specialist, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, 'techspec', '63a9f0ea7bb98050796b649e85481845', 1, 'Tech Specialist', VFD, VTD);

  -- Пользователь tech support coordinator, пароль 'root'
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, 'techsupport',
          '63a9f0ea7bb98050796b649e85481845', 1, 'Techsupport Coordinator', VFD, VTD);

  -----------------------------------------------------------------------------------------------------
  ------------------------------------- Настройка пользователей ---------------------------------------
  -----------------------------------------------------------------------------------------------------

  ----------------------------------------- Роли пользователей ----------------------------------------

  -- все роли для пользователя root
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_USER_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_OBJECT_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_EQUIPMENT_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_TECH_SPECIALIST, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_TECH_SUPPORT_COORDINATOR, 1, 'DSC', VFD, VTD);

  -- 'всемогущая' роль пользователю адаптер
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ADAPTER, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  -- 'всемогущая' роль пользователю import
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_IMPORT, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю user_admin
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, ROLE_USER_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю obj_admin
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, ROLE_OBJECT_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю eq_admin
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, ROLE_EQUIPMENT_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю officer
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю techspec
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, ROLE_TECH_SPECIALIST, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю techsupport
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, ROLE_TECH_SUPPORT_COORDINATOR, 1, 'DSC', VFD, VTD);

  --------------------------------------- Доступы пользователей ---------------------------------------

  -- Пользовательский доступ по филиалу
  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_SYS_ADMIN, PERMIT_PARTY_ACCESS, 14, '1,2,3,4,5,6,7,8,9,10,11,12', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, ROLE_USER_ADMIN, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, ROLE_OBJECT_ADMIN, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, ROLE_EQUIPMENT_ADMIN, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, ROLE_BRANCH_OFFICER, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, ROLE_TECH_SPECIALIST, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, ROLE_TECH_SUPPORT_COORDINATOR,
          PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  -- Принадлежность пользователя филиалу
  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_SYS_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, ROLE_USER_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, ROLE_OBJECT_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, ROLE_EQUIPMENT_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, ROLE_BRANCH_OFFICER, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, ROLE_TECH_SPECIALIST, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, ROLE_TECH_SUPPORT_COORDINATOR,
          PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  -----------------------------------------------------------------------------------------------------

END$$;