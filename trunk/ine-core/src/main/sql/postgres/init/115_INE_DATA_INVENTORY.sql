
/*
"$Id: 115_INE_DATA_INVENTORY.sql 67 2017-05-19 09:55:27Z xerror $"
*/

-- Профайлы для оборудования
-- 1
INSERT INTO INV_ITM_PRFL(n, name, typ, fd, td, dsc)
VALUES (nextval('SEQ_INV_ITM_PRFL'), 'QSH-ECP100',                        3, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Контроллер QTech QSH-ECP-100');
-- 2
INSERT INTO INV_ITM_PRFL(n, name, typ, fd, td, dsc)
VALUES (nextval('SEQ_INV_ITM_PRFL'), 'Пульсар Счетчик воды Одноструйный', 5, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Пульсар Счетчик воды Одноструйный RS485 9600');
-- 3
INSERT INTO INV_ITM_PRFL(n, name, typ, fd, td, dsc)
VALUES (nextval('SEQ_INV_ITM_PRFL'), 'Пульсар Теплосчетчик',              4, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Пульсар Теплосчетчик');
------------------------------------------------------------------------------------------------------------------------

-- Описатели свойств
-- 1
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC') , 'EXT_PORT_DRV_TYPE', 2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Тип драйвера внешнего порта');
-- 2
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'T00',                2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Периодичность хардбитов');
-- 3
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'T80',                2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Периодичность отправки состояния');
-- 4
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'PASSWORD',           1,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Ключ для расчета ЭЦП');
-- 5
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'ENCRYPTION_EN',      5,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Включение шифрования');
-- 6
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'PROTOCOL_VERSION',   1,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Версия протокола');
-- 7
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'FIRMWARE_VERSION',   1,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Версия прошивки');
-- 8
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'BUS_ADDRESS',        2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Адрес на шине');
-- 9
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'BAUD_RATE',          2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Скорость передачи');
-- 10
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'PARITY',             2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Serial Parity');
-- 11
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'BITS',               2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Размер пакета');
-- 12
INSERT INTO INV_PROPERTY_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_PROPERTY_DSC'), 'STOP_BITS',          2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Стоповые биты');
------------------------------------------------------------------------------------------------------------------------

-- Описатели значений
-- 1
INSERT INTO INV_VALUE_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_VALUE_DSC'), 'ENERGY_VOLUME',   3,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Количество энергии');
-- 2
INSERT INTO INV_VALUE_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_VALUE_DSC'), 'COOLANT_VOLUME',  3,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Количество теплонсителя');
-- 3
INSERT INTO INV_VALUE_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_VALUE_DSC'), 'TEMPERATURE_OUT', 3,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Температура на входе');
-- 4
INSERT INTO INV_VALUE_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_VALUE_DSC'), 'TEMPERATURE_IN',  3,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Температура на выходе');
-- 5
INSERT INTO INV_VALUE_DSC(n, name, typ, fd, td, dsc)
VALUES ( nextval('SEQ_INV_VALUE_DSC'), 'WATER_VOLUME',    2,  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Объём воды');
------------------------------------------------------------------------------------------------------------------------


-- Тестовые контроллеры кутек
-- 1
INSERT INTO INVENTORY_ITEM(n, typ, self_name, parent_n, profile_n, status_n, fd, td, dsc)
VALUES (nextval('SEQ_INVENTORY_ITEM'), 3, 'CEFBDA3C', null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'test item');
-- 2
INSERT INTO INVENTORY_ITEM(n, typ, self_name, parent_n, profile_n, status_n, fd, td, dsc)
VALUES (nextval('SEQ_INVENTORY_ITEM'), 3, 'CEFBDA6C', null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'test item');

-- INSERT INTO INVENTORY_ITEM(n, typ, self_name, parent_n, profile_n, status_n, fd, td, dsc)
-- VALUES (nextval('SEQ_INVENTORY_ITEM'), 1, 'CEFBDA11', null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'test item');
------------------------------------------------------------------------------------------------------------------------

-- Описатели для профиля QSH кутек
-- 1
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, def_val, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 1, 1, '1', null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 2
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, def_val, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 1, 2, '1', null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 3
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, def_val, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 1, 3, '1', null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 4
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, def_val, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 1, 4, '0000000000000000',
                                                  null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 5
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 1, 5,      null, 1, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 6
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 1, 6,      null, 1, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 7
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 1, 7,      null, 1, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
------------------------------------------------------------------------------------------------------------------------

-- Описатели для профиля пульсар водосчетчик
-- 8 INV_PROPERTY_DSC  BUS_ADDRESS
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 2, 8,  null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 9 INV_PROPERTY_DSC  BAUD_RATE
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 2, 9,  null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 10 INV_PROPERTY_DSC  PARITY
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 2, 10, null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 11 INV_PROPERTY_DSC  BITS
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 2, 11, null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 12 INV_PROPERTY_DSC  STOP_BITS
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 2, 12, null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');

-- Описатели для профиля пульсар тепломер
-- 8 INV_PROPERTY_DSC  BUS_ADDRESS
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 3, 8,  null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 9 INV_PROPERTY_DSC  BAUD_RATE
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 3, 9,  null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 10 INV_PROPERTY_DSC  PARITY
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 3, 10, null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 11 INV_PROPERTY_DSC  BITS
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 3, 11, null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 12 INV_PROPERTY_DSC  STOP_BITS
INSERT INTO INV_PRFL_PROPS(n, profile_n, prprt_dsc_n, pattern, is_rqrd, is_edit, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_PROPS'), 3, 12, null, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
------------------------------------------------------------------------------------------------------------------------

-- Значения для профиля пульсара
-- 1
INSERT INTO INV_PRFL_VALS(n, profile_n, val_dsc_n, pattern, is_rqrd, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_VALS'), 3, 1, null, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 2
INSERT INTO INV_PRFL_VALS(n, profile_n, val_dsc_n, pattern, is_rqrd, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_VALS'), 3, 2, null, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 3
INSERT INTO INV_PRFL_VALS(n, profile_n, val_dsc_n, pattern, is_rqrd, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_VALS'), 3, 3, null, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 4
INSERT INTO INV_PRFL_VALS(n, profile_n, val_dsc_n, pattern, is_rqrd, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_VALS'), 3, 4, null, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
-- 5
INSERT INTO INV_PRFL_VALS(n, profile_n, val_dsc_n, pattern, is_rqrd, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRFL_VALS'), 2, 5, null, 0, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'initial config');
------------------------------------------------------------------------------------------------------------------------

-- Данные для тестового инвентаря
INSERT INTO INV_PRPT_INT_DATA(n, item_n, descriptor_n, val, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRPT_INT_DATA'), 1, 1, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), null);

INSERT INTO INV_PRPT_INT_DATA(n, item_n, descriptor_n, val, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRPT_INT_DATA'), 1, 2, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), null);

INSERT INTO INV_PRPT_INT_DATA(n, item_n, descriptor_n, val, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRPT_INT_DATA'), 1, 3, 1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), null);

INSERT INTO INV_PRPT_STR_DATA(n, item_n, descriptor_n, val, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRPT_STR_DATA'), 1, 4, '0000000000000000', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), null);

INSERT INTO INV_PRPT_BOOL_DATA(n, item_n, descriptor_n, val, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRPT_STR_DATA'), 1, 5,     1, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), null);

INSERT INTO INV_PRPT_STR_DATA(n, item_n, descriptor_n, val, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRPT_STR_DATA'), 1, 6, '0.1', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), null);

INSERT INTO INV_PRPT_STR_DATA(n, item_n, descriptor_n, val, fd, td, dsc)
VALUES (nextval('SEQ_INV_PRPT_STR_DATA'), 1, 7, '1.0', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), null);
