/*
"$Id: 008_INE_CORE_LANGUAGE.sql 29 2017-04-04 15:32:19Z xerror $"
*/

CREATE TABLE LANGS
(
  N           NUMERIC                 NOT NULL
, LNG_NAME    CHARACTER VARYING(2000) NOT NULL
, DSC         CHARACTER VARYING(2000) NOT NULL
, CONSTRAINT  PK_LANGS                PRIMARY KEY (N)
, CONSTRAINT  UK_LANGS_LNG_NAME       UNIQUE(LNG_NAME)
);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'LANGS', null, 'Содержит список языков системы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.language.LangName', system_ID, 'Интерфейс названия языка');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Идентификатор системного языка (используется для представления в системе)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LNG_NAME', system_ID, 'Наименование системного языка');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарии');

END$$;

CREATE TABLE LANG_CODES
(
  N          NUMERIC                    NOT NULL
, DSC        CHARACTER VARYING(1)
, LNG_CODE   CHARACTER VARYING(4)       NOT NULL
, LNG_NAME   CHARACTER VARYING(2000)    NOT NULL
, CONSTRAINT PK_LANG_CODES     PRIMARY KEY (N)
, CONSTRAINT UK_LNG_CODES_LC   UNIQUE (LNG_CODE)
, CONSTRAINT UK_LNG_CODES_LN   UNIQUE (LNG_NAME)
);


CREATE OR REPLACE FUNCTION FUNC_LANG_CODES_CHANGE() RETURNS trigger AS
  $BODY$
  BEGIN
    IF CONSTANTS_HELPER.safe_Get_Constant_Value(4) != '1' THEN
      RAISE EXCEPTION 'Modification of table LANG_CODES is forbidden. Set appropriate value to constant ALLOW_MODIFY_ISO_LANGS';
    ELSEIF  (TG_OP = 'DELETE') THEN
      RETURN OLD;
    END IF;

    RETURN NEW;
  END;
  $BODY$
    LANGUAGE plpgsql VOLATILE
    COST 100;

CREATE TRIGGER LANG_CODES_CHANGE BEFORE INSERT OR UPDATE OR DELETE ON LANG_CODES FOR EACH ROW
  EXECUTE PROCEDURE FUNC_LANG_CODES_CHANGE();


-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'LANG_CODES', null, 'Содержит данные по кодам языков (стандарт iso639)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.language.LangDetails', system_ID, 'Интерфейс кодов языков');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Ввнутренний порядковый (уникальный) номер языка');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарии. Не используется!');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LNG_CODE', system_ID, 'Буквенный код языка в соответствии со стандартом');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LNG_NAME', system_ID, 'Название языка');

END$$;


CREATE TABLE COUNTRY_CODES
(
  CODE       NUMERIC                 NOT NULL
, COUNTRY    CHARACTER VARYING(2000) NOT NULL
, A2         CHARACTER VARYING(2000) NOT NULL
, A3         CHARACTER VARYING(2000) NOT NULL
, DSC        CHARACTER VARYING(1)
, CONSTRAINT PK_COUNTRY_CODES      PRIMARY KEY (CODE)
, CONSTRAINT UK_COUNTRY_CODES_A2   UNIQUE (A2)
, CONSTRAINT UK_COUNTRY_CODES_A3   UNIQUE (A3)
);



-- Добавляем триггер на модификацию данных в таблице
CREATE OR REPLACE FUNCTION FUNC_COUNTRY_CODES() RETURNS trigger AS
  $BODY$
  BEGIN
    IF CONSTANTS_HELPER.safe_Get_Constant_Value(4) != '1' THEN
      RAISE EXCEPTION 'Modification of table COUNTRY_CODES is forbidden. Set appropriate value to constant ALLOW_MODIFY_ISO_LANGS';
    ELSEIF  (TG_OP = 'DELETE') THEN
      RETURN OLD;
    END IF;

    RETURN NEW;
  END;
  $BODY$
    LANGUAGE plpgsql VOLATILE COST 100;

CREATE TRIGGER COUNTRY_CODES_CHANGE BEFORE INSERT OR UPDATE OR DELETE ON COUNTRY_CODES FOR EACH ROW
  EXECUTE PROCEDURE FUNC_COUNTRY_CODES();


-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'COUNTRY_CODES', null, 'Содержит данные по регионам (стандарт iso3166).');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.language.CountryDetails', system_ID, 'Интерфейс кодов стран');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CODE', system_ID, 'Числовой код страны (стандарт iso3166)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'COUNTRY', system_ID, 'Название страны');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'A2', system_ID, 'Код, 2-а символа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'A3', system_ID, 'Код, 3-и символа');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарии. Не используется!');

END$$;


CREATE TABLE LANG_MAPPING
(
  LANGS_N                       NUMERIC       NOT NULL -- есть паттерн
, LANG_CODES_N                  NUMERIC       NOT NULL -- есть паттерн
, COUNTRY_CODES_CODE            NUMERIC       NOT NULL -- есть паттерн
, FD                            TIMESTAMP WITH TIME ZONE     NOT NULL
, TD                            TIMESTAMP WITH TIME ZONE     NOT NULL
, DSC                           CHARACTER VARYING(1)
, CONSTRAINT FK_LANG_MAPPING_LN FOREIGN KEY(LANGS_N)            REFERENCES LANGS(N)
, CONSTRAINT FK_LANG_MAPPING_LC FOREIGN KEY(LANG_CODES_N)       REFERENCES LANG_CODES(N)
, CONSTRAINT FK_LANG_MAPPING_CN FOREIGN KEY(COUNTRY_CODES_CODE) REFERENCES COUNTRY_CODES(CODE)
, CONSTRAINT UK_LANG_MAPPING_ID UNIQUE (LANGS_N, LANG_CODES_N, COUNTRY_CODES_CODE)
);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'LANG_MAPPING', null, 'Содержит сопоставления комбинации "язык-регион" к системному языку');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.language.SysLanguage', system_ID, 'Интерфейс системного языка');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LANGS_N', system_ID, 'Ссылка на системный номер языка (таблица LANGS.N)', ''||INE_CORE_SYS_OBJ.get_Column_N('LANGS', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LANG_CODES_N', system_ID, 'Ссылка на внутренний порядковый (уникальный) номер языка (таблица LANG_CODES.N)', ''||INE_CORE_SYS_OBJ.get_Column_N('LANG_CODES', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'COUNTRY_CODES_CODE', system_ID, 'Ссылка на числовой код страны (таблица COUNTRY_CODES.CODE)', ''||INE_CORE_SYS_OBJ.get_Column_N('COUNTRY_CODES', 'CODE'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарии. Не используется!');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('LANG_MAPPING');


-- Обновляем информацию в SYS_OBJ о столбцах с паттернами в табличке DIC_DATA
  EXECUTE INE_CORE_SYS_OBJ.add_Pattern(INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'LANG'), ''||INE_CORE_SYS_OBJ.get_Column_N('LANGS', 'N'));

END$$;