/*
"$Id: 007_INE_CORE_FUNCSW.sql 29 2017-04-04 15:32:19Z xerror $"
*/

CREATE TABLE FUNC_SWITCH
(
  N           NUMERIC                  NOT NULL
, FUNC_NAME   CHARACTER VARYING(2000)  NOT NULL
, DSC         CHARACTER VARYING(2000)  NOT NULL
, CONSTRAINT  UK_FUNC_SWITCH_ID  UNIQUE (N)
);

-- Последовательность номеров для имён переключателей, создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_FUNC_SWITCH INCREMENT BY 1 START WITH 10;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC ;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'FUNC_SWITCH', null, 'Содержит описания переключателей функциональности');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.funcswitch.FuncSwitchName', system_ID, 'Интерфейс названия переключателя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Системный номер переключателя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'FUNC_NAME', system_ID, 'Название переключателя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание переключателя');
  -- Последовательность номеров для имён
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_FUNC_SWITCH', system_ID,
     'Последовательность номеров для имён переключателей, создаваемых в процессе эксплуатации');

END$$;


CREATE TABLE FUNC_STATE
(
  FUNC_N       NUMERIC           NOT NULL -- есть паттерн
, DSC          CHARACTER VARYING(1)
, STATE        NUMERIC           NOT NULL
, C_DATE       TIMESTAMP WITH TIME ZONE        NOT NULL
, CM           NUMERIC           NOT NULL
, STATE_CODE   CHARACTER VARYING(2000)   NOT NULL
, FD           TIMESTAMP WITH TIME ZONE        NOT NULL
, TD           TIMESTAMP WITH TIME ZONE        NOT NULL
, CONSTRAINT   UK_FUNC_STATE_ID UNIQUE (FUNC_N)
);

-- Индексы на таблы
CREATE INDEX IND_FUNC_STATE_STATE    ON   FUNC_STATE(STATE);
CREATE INDEX IND_FUNC_STATE_FDTD     ON   FUNC_STATE(FD, TD);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'FUNC_STATE', null, 'Содержит состояния переключателей функциональности');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.funcswitch.FuncSwitch', system_ID, 'Интерфейс значения переключателя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'FUNC_N', system_ID, 'Идентификатор переключателя', ''||INE_CORE_SYS_OBJ.get_Column_N('FUNC_SWITCH', 'N'));
  FAKE_VAR := ine_core_sys_obj.add_sys_obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_id, 'Комментарий. Не используется!');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'STATE', system_ID, 'Состояние переключателя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'C_DATE', system_ID, 'Дата смены состояния переключателя');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CM', system_ID, 'Конторльная метка переключателя (безопасность)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'STATE_CODE', system_ID, 'Код состояние переключателя (безопасность)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('FUNC_STATE');

END$$;