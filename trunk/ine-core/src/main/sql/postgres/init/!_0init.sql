/*

Выполнить из под postgres

Скрипт создает базу и заполняет ее.

"$Id: !_0init.sql 43 2017-04-10 17:02:25Z xerror $"
*/

\c postgres

\i !_1createUserAndDB.sql

-- Вызов скриптов в нужной последовательности
\i 001_sys_obj.sql;
\i 004_INE_CORE_DIC.sql;
\i 104_INE_CORE_DATA_DIC.sql;
\i 005_INE_CORE_USPTS.sql;
\i 006_INE_CORE_CONSTANTS.sql;
\i 007_INE_CORE_FUNCSW.sql;
\i 008_INE_CORE_LANGUAGE.sql;
\i 009_INE_CORE_LINKS.sql;
\i 010_INE_CORE_STRINGS.sql;
\i 011_INE_CORE_CUSTOM_TYPES_DSC.sql;
\i 012_INE_CORE_CUSTOM_ATTRS_DSC.sql;
\i 013_INE_CORE_ENTITIES.sql;
\i 014_INE_IP_ADDRESS.sql;
\i 015_INE_INVENTORY.sql;
--
-- Набиваем данными
\i 105_INE_CORE_DATA_USPTS.sql;
\i 106_INE_CORE_DATA_CONSTANTS.sql;
\i 107_INE_CORE_DATA_FUNCSW.sql;
\i 108_INE_CORE_DATA_LANGUAGE.sql;
\i 111_INE_CORE_DATA_CUSTOM_ATTRS_DSC.sql;
\i 115_INE_DATA_INVENTORY.sql

\i 999_LAST.sql
\i 999_TEST_DATA.sql


DO $$

DECLARE
  rowIn SYS_OBJ%rowtype;
BEGIN

  FOR rowIn IN
    SELECT * FROM SYS_OBJ WHERE TYP = INE_CORE_SYS_OBJ.COLUMN_TYP()
     LOOP
        UPDATE SYS_OBJ SET obj_name = lower(rowIn.obj_name) where n = rowIn.n;
     END LOOP;

END$$;
