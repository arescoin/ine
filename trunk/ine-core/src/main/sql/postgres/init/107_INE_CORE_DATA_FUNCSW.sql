/*
"$Id: 107_INE_CORE_DATA_FUNCSW.sql 29 2017-04-04 15:32:19Z xerror $"
*/
DO $$

DECLARE
    -- Системные даты
    VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
    VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();

    NUM NUMERIC := nextval('SEQ_FUNC_SWITCH');
BEGIN

  -- Переключатель регулирует возможность создания будущих версий сущностей
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES
  (NUM, 'FUTURE_HISTORY', 'Переключатель регулирует возможность создания будущих версий сущностей');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 0, VFD, 0, 0, VFD, VTD);


  -- Переключатель регулирует запись изменяемого объекта в таблицу с историей изменения объекта
  NUM := nextval('SEQ_FUNC_SWITCH');
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES
  (NUM, 'SAVE_HISTORY', 'Переключатель регулирует запись изменяемого объекта в таблицу с историей изменения объекта');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 1, VFD, 0, 0, VFD, VTD);


  -- Переключатель регулирует запись действий над изменяемым объектом
  NUM := nextval('SEQ_FUNC_SWITCH');
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES
  (NUM, 'REGISTER_ACTIONS', 'Переключатель регулирует запись действий над изменяемым объектом');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 1, VFD, 0, 0, VFD, VTD);



  -- Переключатель регулирует запись пользовательских атрибутов действия над изменяемым объектом
  NUM := nextval('SEQ_FUNC_SWITCH');
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES (NUM, 'SAVE_ACTION_ATTRIBUTES',
    'Переключатель регулирует запись пользовательских атрибутов действия над изменяемым объектом.'||
    'Работает только при включённом переключателе REGISTER_ACTIONS');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 1, VFD, 0, 0, VFD, VTD);


END$$;

