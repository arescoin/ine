/*
* $Id: 999_LAST.sql 29 2017-04-04 15:32:19Z xerror $
*/


DO $$

DECLARE
  permit_sfx CHARACTER VARYING(10) := '_PERMIT';
  rowIn SYS_OBJ%rowtype;
BEGIN

  FOR rowIn IN
    SELECT * FROM SYS_OBJ WHERE TYP = INE_CORE_SYS_OBJ.TABLE_TYP()
      AND UP is null AND N not in (SELECT N FROM PERMITS) order by n
      LOOP
        INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD)
          VALUES (rowIn.n, rowIn.OBJ_NAME || PERMIT_SFX, null, 0,
                  'Разрешение на доступ к ' || rowIn.OBJ_NAME,
                  INE_CORE_SYS.GET_SYSTEM_FROMDATE(),
                  INE_CORE_SYS.GET_SYSTEM_TODATE()
                  );

        INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
          VALUES (1, rowIn.n, 14, null, 'Автоприсвоение при инициализации',
                  INE_CORE_SYS.GET_SYSTEM_FROMDATE(),
                  INE_CORE_SYS.GET_SYSTEM_TODATE()
                  );

  END LOOP;

END$$;


DO $$

DECLARE
  fake         NUMERIC;
  cursNum      NUMERIC;
  new_id       NUMERIC;
  sqlFirstPart CHARACTER VARYING(2000);
BEGIN

  select max(N) into new_id from PERMITS;
  new_id := new_id + 1000;

  RAISE NOTICE ' - %', new_id;

  EXECUTE 'DROP SEQUENCE SEQ_PERMITS';

  EXECUTE 'CREATE SEQUENCE SEQ_PERMITS MINVALUE 1 INCREMENT BY 1 START WITH ' || new_id;

END$$;
