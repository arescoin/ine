
/*
"$Id: 015_INE_INVENTORY.sql 59 2017-05-04 16:58:36Z xerror $"
*/
-- ru.xr.ine.oss.inventory.item.ValueDescriptor
CREATE TABLE INV_VALUE_DSC
(
    N           NUMERIC                    NOT NULL
  , NAME        CHARACTER VARYING(2000)    NOT NULL
  , TYP         NUMERIC                    NOT NULL -- есть паттерн словарь 'DATA_TYPES'
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_VALUE_DSC       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_VALUE_DSC     ON  INV_VALUE_DSC(N);
-- Последовательность номеров для объектов
CREATE SEQUENCE SEQ_INV_VALUE_DSC INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DIC_N      NUMERIC;
  DD_N       NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
BEGIN

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'DATA_TYPES';

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_VALUE_DSC', null,'Содержит перечень описаний значений объектов учёта');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.ValueDescriptor', system_ID, 'Интерфейс описывает значения объекта учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N',    system_ID, 'Cистемный номер описаний значений объекта учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'NAME', system_ID, 'Имя свойства');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP',  system_ID, 'Тип описываемого значения (словарная статья Словарь №'|| DIC_N || ')',
                                           '[' || DD_N || ':' || DIC_N || ']' || DD_CODE);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_VALUE_DSC', system_ID, 'Последовательность номеров для описаний значений объекта учета');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_VALUE_DSC');

END$$;

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- ru.xr.ine.oss.inventory.item.PropertyDescriptor
CREATE TABLE INV_PROPERTY_DSC
(
    N           NUMERIC                    NOT NULL
  , NAME        CHARACTER VARYING(2000)    NOT NULL
  , TYP         NUMERIC                    NOT NULL -- есть паттерн словарь 'DATA_TYPES'
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INV_PROPERTY_DSC       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PROPERTY_DSC     ON  INV_PROPERTY_DSC(N);
-- Последовательность номеров для объектов
CREATE SEQUENCE SEQ_INV_PROPERTY_DSC INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DIC_N      NUMERIC;
  DD_N       NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
BEGIN

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'DATA_TYPES';

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PROPERTY_DSC', null,'Содержит перечень свойств объектов учёта');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.PropertyDescriptor', system_ID, 'Интерфейс описывает свойство объекта учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер свойства объекта учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'NAME', system_ID, 'Имя свойства');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID, 'Тип описываемого значения (словарная статья Словарь №'|| DIC_N || ')',
                                           '[' || DD_N || ':' || DIC_N || ']' || DD_CODE);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_PROPERTY_DSC', system_ID, 'Последовательность номеров для свойства объекта учета');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PROPERTY_DSC');

END$$;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-- ru.xr.ine.oss.inventory.item.ItemProfile
CREATE TABLE INV_ITM_PRFL
(
    N           NUMERIC                    NOT NULL
  , NAME        CHARACTER VARYING(2000)    NOT NULL
  , TYP         NUMERIC                    NOT NULL -- есть паттерн словарь INV_ITEM_SUBTYPE
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INV_ITM_PRFL       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_ITM_PRFL     ON  INV_ITM_PRFL(N);
-- Последовательность номеров для объектов
CREATE SEQUENCE SEQ_INV_ITM_PRFL INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DIC_N      NUMERIC;
  DD_N       NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
BEGIN
  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'INV_ITEM_SUBTYPE';

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_ITM_PRFL', null,'Содержит перечень профилей объектов учёта');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.ItemProfile', system_ID, 'Интерфейс описывает профиль объекта учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер профиля объекта учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'NAME', system_ID, 'Имя свойства');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID, 'Тип (словарная статья )',
                                           '[' || DD_N || ':' || DIC_N || ']' || DD_CODE);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_ITM_PRFL', system_ID, 'Последовательность номеров для свойства объекта учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_ITM_PRFL');

END$$;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE INVENTORY_ITEM
(
    N           NUMERIC                    NOT NULL
  , TYP         NUMERIC                    NOT NULL -- есть паттерн словарь INV_ITEM_SUBTYPE
  , SELF_NAME   CHARACTER VARYING(2000)
  , PARENT_N    NUMERIC                             -- есть паттерн
  , PROFILE_N   NUMERIC                    NOT NULL -- есть паттерн
  , STATUS_N    NUMERIC                    NOT NULL -- есть паттерн
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INVTR_ITM_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INVENTORY_ITEM     ON  INVENTORY_ITEM(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INVENTORY_ITEM INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
  DIC_N      NUMERIC;
  DD_N       NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
BEGIN
  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'DATA_TYPES';

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INVENTORY_ITEM', null,'Содержит перечень существующих в системе объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.Item', system_ID, 'Интерфейс объекта учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N',         system_ID,  'Cистемный номер объекта учета');
  FAKE_VAR2:= FAKE_VAR;
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP',       system_ID,  'Тип (словарная статья)', '[' || DD_N || ':' || DIC_N || ']' || DD_CODE);
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PARENT_N',  system_ID,  'Неотъемлемый родитель, при его наличии. Идентификатор объекта учета.', ''||FAKE_VAR2);
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SELF_NAME', system_ID,  'Собственный идентификатор устройства');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PROFILE_N', system_ID,  'Идентификатор описателя объекта учета', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_ITM_PRFL', 'N'));

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'INV_ITEM_STATUS';
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'STATUS_N',  system_ID, 'Идентификатор состояния объекта учета (словарная статья)',
                                                                                                  '[' || DD_N || ':' || DIC_N || ']' || DD_CODE); -- INV_ITEM_STATUS

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INVENTORY_ITEM', system_ID, 'Последовательность номеров для объекта учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INVENTORY_ITEM');

END$$;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-- ru.xr.ine.oss.inventory.item.ItemProfileProperty
CREATE TABLE INV_PRFL_PROPS
(
    N           NUMERIC                    NOT NULL
  , PROFILE_N   NUMERIC                    NOT NULL -- есть паттерн
  , PRPRT_DSC_N NUMERIC                    NOT NULL -- есть паттерн
  , DEF_VAL     CHARACTER VARYING(2000)
  , PATTERN     CHARACTER VARYING(2000)
  , IS_RQRD     NUMERIC                    NOT NULL
  , IS_EDIT     NUMERIC                    NOT NULL
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INV_PRFL_PROPS       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PRFL_PROPS     ON  INV_PRFL_PROPS(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INV_PRFL_PROPS INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PRFL_PROPS', null,'Содержит перечень свойств в профиле объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.ItemProfileProperty', system_ID, 'Интерфейс свойств в профиле объектов учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N',           system_ID, 'Cистемный номер');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PROFILE_N',   system_ID, 'Номер профиля', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_ITM_PRFL', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PRPRT_DSC_N', system_ID, 'Номер описателя проперти', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_PROPERTY_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PATTERN',     system_ID, 'Индивидуальный патерн');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_RQRD',     system_ID, 'Признак обязательности');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_EDIT',     system_ID, 'Возможность ручной модификации');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_PRFL_PROPS', system_ID, 'Последовательность номеров для объекта учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PRFL_PROPS');

END$$;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-- ru.xr.ine.oss.inventory.item.ItemProfileValue
CREATE TABLE INV_PRFL_VALS
(
    N           NUMERIC                    NOT NULL
  , PROFILE_N   NUMERIC                    NOT NULL -- есть паттерн
  , VAL_DSC_N   NUMERIC                    NOT NULL -- есть паттерн
  , PATTERN     CHARACTER VARYING(2000)
  , IS_RQRD     NUMERIC                    NOT NULL
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INV_PRFL_VALS       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PRFL_VALS     ON  INV_PRFL_VALS(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INV_PRFL_VALS INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PRFL_VALS', null,'Содержит перечень значчений в профиле объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.ItemProfileProperty', system_ID, 'Интерфейс значчений в профиле объектов учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'N',         system_ID, 'Cистемный номер');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'PROFILE_N', system_ID, 'Номер профиля',             '' || INE_CORE_SYS_OBJ.get_Column_N('INV_ITM_PRFL', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'VAL_DSC_N', system_ID, 'Номер описателя значчений', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_VALUE_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'PATTERN',   system_ID, 'Индивидуальный патерн');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'IS_RQRD',   system_ID, 'Признак обязательности');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_PRFL_VALS', system_ID, 'Последовательность номеров для объекта учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PRFL_VALS');

END$$;

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
CREATE TABLE INV_PRPT_DEC_DATA
(
   N            NUMERIC                    NOT NULL
  ,ITEM_N       NUMERIC                    NOT NULL  -- есть паттерн
  ,DESCRIPTOR_N NUMERIC                    NOT NULL  -- есть паттерн
  ,VAL          NUMERIC(16,6)              NOT NULL
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INV_PRPT_DEC_DATA       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PRPT_DEC_DATA     ON  INV_PRPT_DEC_DATA(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INV_PRPT_DEC_DATA INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PRPT_DEC_DATA', null,'Содержит конкретные значчения свойств объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.PropertyDataBigDecimal', system_ID, 'Интерфейс значчений свойств объектов учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'N',            system_ID, 'Cистемный номер',     '' || INE_CORE_SYS_OBJ.get_Column_N('INVENTORY_ITEM'  , 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'ITEM_N',       system_ID, 'Номер объекта учета', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_PROPERTY_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'DESCRIPTOR_N', system_ID, 'Номер описателя значчения');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'VAL',          system_ID, 'Значение свойства');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_PRPT_DEC_DATA', system_ID, 'Последовательность номеров для значчений свойств объектов учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PRPT_DEC_DATA');

END$$;
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE INV_PRPT_INT_DATA
(
    N            NUMERIC                    NOT NULL
  , ITEM_N       NUMERIC                    NOT NULL
  , DESCRIPTOR_N NUMERIC                    NOT NULL
  , VAL          BIGINT                     NOT NULL
  , FD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC          CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INV_PRPT_INT_DATA        UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PRPT_INT_DATA     ON  INV_PRPT_INT_DATA(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INV_PRPT_INT_DATA INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PRPT_INT_DATA', null,'Содержит конкретные значчения свойств объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.PropertyDataLong', system_ID, 'Интерфейс значчений свойств объектов учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'N',            system_ID, 'Cистемный номер');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'ITEM_N',       system_ID, 'Номер объекта учета'      , '' || INE_CORE_SYS_OBJ.get_Column_N('INVENTORY_ITEM'  , 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'DESCRIPTOR_N', system_ID, 'Номер описателя значчения', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_PROPERTY_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'VAL',          system_ID, 'Значение свойства');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_PRPT_INT_DATA', system_ID, 'Последовательность номеров для значчений свойств объектов учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PRPT_INT_DATA');

END$$;
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE INV_PRPT_STR_DATA
(
    N            NUMERIC                    NOT NULL
  , ITEM_N       NUMERIC                    NOT NULL
  , DESCRIPTOR_N NUMERIC                    NOT NULL
  , VAL          CHARACTER VARYING(2000)      NOT NULL
  , FD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC          CHARACTER VARYING(2000)
  , CONSTRAINT  UK_INV_PRPT_STR_DATA        UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PRPT_STR_DATA     ON  INV_PRPT_STR_DATA(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INV_PRPT_STR_DATA INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PRPT_STR_DATA', null,'Содержит конкретные значчения свойств объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.PropertyDataString', system_ID, 'Интерфейс значчений свойств объектов учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'N',            system_ID, 'Cистемный номер');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'ITEM_N',       system_ID, 'Номер объекта учета'      , '' || INE_CORE_SYS_OBJ.get_Column_N('INVENTORY_ITEM'  , 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'DESCRIPTOR_N', system_ID, 'Номер описателя значчения', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_PROPERTY_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'VAL',          system_ID, 'Значение свойства');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_PRPT_STR_DATA', system_ID, 'Последовательность номеров для значчений свойств объектов учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PRPT_STR_DATA');

END$$;
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE INV_PRPT_DATE_DATA
(
    N            NUMERIC                    NOT NULL
  , ITEM_N       NUMERIC                    NOT NULL
  , DESCRIPTOR_N NUMERIC                    NOT NULL
  , VAL          TIMESTAMP WITH TIME ZONE   NOT NULL
  , FD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC          CHARACTER VARYING(2000)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PRPT_DATE_DATA     ON  INV_PRPT_DATE_DATA(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INV_PRPT_DATE_DATA INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PRPT_DATE_DATA', null,'Содержит конкретные значчения свойств объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.PropertyDataString', system_ID, 'Интерфейс значчений свойств объектов учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'N',            system_ID, 'Cистемный номер');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'ITEM_N',       system_ID, 'Номер объекта учета'      , '' || INE_CORE_SYS_OBJ.get_Column_N('INVENTORY_ITEM'  , 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'DESCRIPTOR_N', system_ID, 'Номер описателя значчения', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_PROPERTY_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'VAL',          system_ID, 'Значение свойства');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'IND_INV_PRPT_DATE_DATA', system_ID, 'Последовательность номеров для значчений свойств объектов учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PRPT_DATE_DATA');

END$$;
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE INV_PRPT_BOOL_DATA
(
    N            NUMERIC                    NOT NULL
  , ITEM_N       NUMERIC                    NOT NULL
  , DESCRIPTOR_N NUMERIC                    NOT NULL
  , VAL          NUMERIC(1)                 NOT NULL
  , FD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD           TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC          CHARACTER VARYING(2000)
);

-- Индексы на таблы
CREATE INDEX   IND_INV_PRPT_BOOL_DATA     ON  INV_PRPT_BOOL_DATA(N);

-- Последовательность номеров для объектов учета создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_INV_PRPT_BOOL_DATA INCREMENT BY 1 START WITH 1;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'INV_PRPT_BOOL_DATA', null,'Содержит конкретные значчения свойств объектов учета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.inventory.item.PropertyDataBoolean', system_ID, 'Интерфейс значчений свойств объектов учета');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'N',            system_ID, 'Cистемный номер');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'ITEM_N',       system_ID, 'Номер объекта учета'      , '' || INE_CORE_SYS_OBJ.get_Column_N('INVENTORY_ITEM'  , 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'DESCRIPTOR_N', system_ID, 'Номер описателя значчения', '' || INE_CORE_SYS_OBJ.get_Column_N('INV_PROPERTY_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),    'VAL',          system_ID, 'Значение свойства');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  -- накатываем FD-TD
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_INV_PRPT_BOOL_DATA', system_ID, 'Последовательность номеров для значчений свойств объектов учета');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('INV_PRPT_BOOL_DATA');

END$$;
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--------------------------------
-- Это пока не используется!!!
--------------------------------

CREATE TABLE DT_PNT_PRFL
(
    N           NUMERIC                    NOT NULL -- есть паттерн
  , PHYSICAL    NUMERIC                    NOT NULL
  , INTR_TYPE   NUMERIC                    NOT NULL
  , DATA_TYPE   NUMERIC                    NOT NULL
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT   UK_DT_PNT_PRFL_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX   IND_DT_PNT_PRFL     ON  DT_PNT_PRFL(N);

-- Последовательность номеров для профайлов создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_DT_PNT_PRFL INCREMENT BY 1 START WITH 3000;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'DT_PNT_PRFL', null,'Содержит описания существующих в системе профилей источников данных');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.entity.DataPointProfile', system_ID, 'Интерфейс профиля источника');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер профиля');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PHYSICAL', system_ID, 'Тип (физический/логический)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'INTR_TYPE', system_ID, 'Тип взаимодействия: пассив, актив, комб.');
  -- Паттерн будет добавлен ниже

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),'DATA_TYPE', system_ID, 'Правило формирования кода словарной статьи. Словарь №2');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_DT_PNT_PRFL', system_ID, 'Последовательность номеров для профиля источника');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('DT_PNT_PRFL');

END$$;

CREATE TABLE DT_POINT
(
    N           NUMERIC                    NOT NULL -- есть паттерн
  , CROSS_ID    CHARACTER VARYING(2000)    NOT NULL
  , PROFILE_N   NUMERIC                    NOT NULL
  , FD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , TD          TIMESTAMP WITH TIME ZONE   NOT NULL
  , DSC         CHARACTER VARYING(2000)
  , CONSTRAINT  UK_DT_POINT_ID  UNIQUE (N, CROSS_ID)
);

-- Индексы на таблы
CREATE INDEX   IND_DT_POINT_DN     ON  DT_POINT(N);
CREATE INDEX   IND_DT_POINT_CL     ON  DT_POINT(CROSS_ID, PROFILE_N);
CREATE INDEX   IND_DT_POINT_FDTD   ON  DT_POINT(FD, TD);

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'DT_POINT', null, 'Содержит значения существующих в системе источников данных');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.oss.entity.DataPoint', system_ID, 'Интерфейс источника');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CROSS_ID', system_ID, 'Идентификатор содержащий в себе внешние атрибуты');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PROFILE_N', system_ID, 'Номер профиля источника');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('DT_POINT');

END$$;