/*
"$Id: 012_INE_CORE_CUSTOM_ATTRS_DSC.sql 29 2017-04-04 15:32:19Z xerror $"
*/

-- Таблица описателей произвольных атрибутов.
CREATE TABLE CUSTOM_ATTRS_DSC
(
  N             NUMERIC                   NOT NULL
, SYS_OBJ_N     NUMERIC                   NOT NULL -- есть паттерн
, CUSTOM_TYPE   NUMERIC                            -- есть паттерн
, ATTR_TYP      NUMERIC                   NOT NULL -- есть паттерн
, ATTR_NAME     CHARACTER VARYING(2000)   NOT NULL
, IS_REQUIRED   NUMERIC                   NOT NULL
, PATTERN       CHARACTER VARYING(2000)
, LIMITS        CHARACTER VARYING(2000)
, DSC           CHARACTER VARYING(2000)
, FD            TIMESTAMP WITH TIME ZONE  NOT NULL
, TD            TIMESTAMP WITH TIME ZONE  NOT NULL
, CONSTRAINT    UK_CUSTOM_ATTRS_DSC_ID UNIQUE (N)
);

CREATE INDEX IND_CUSTOM_ATTRS_DSC_SYSOBJ_N   ON  CUSTOM_ATTRS_DSC(SYS_OBJ_N);
CREATE INDEX IND_CUSTOM_ATTRS_DSC_FDTD       ON  CUSTOM_ATTRS_DSC(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_CUSTOM_ATTRS_DSC INCREMENT BY 1 START WITH 100;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC;
  DIC_N      NUMERIC;
  DD_N       NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMERIC := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
  --DATA_TYPES
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'CUSTOM_ATTRS_DSC', null, 'Таблица для описания характеристик произвольных атрибутов расширяемых объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.structure.CustomAttribute', system_ID, 'Интерфейс описания произвольного атрибута');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Уникальный номер атрибута');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SYS_OBJ_N', system_ID, 'Номер системного объекта, SYS_OBJ', ''||INE_CORE_SYS_OBJ.get_Column_N('SYS_OBJ', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CUSTOM_TYPE', system_ID,
    'Номер агрегирующего custom-типа, расширяющего базовый системный объект из SYS_OBJ_N. Может быть null', ''||INE_CORE_SYS_OBJ.get_Column_N('CUSTOM_TYPES_DSC', 'N'));

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'DATA_TYPES';

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ATTR_TYP', system_ID, 'Тип атрибута. Словарь №' || DIC_N, '[' ||DD_N || ':' || DIC_N || ']' || DD_CODE);
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ATTR_NAME', system_ID, 'Имя атрибута');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_REQUIRED', system_ID, 'Признак обязательности атрибута');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PATTERN', system_ID, 'Паттерн системного объекта для возможности работы с другими объектами InE');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LIMITS', system_ID, 'Ограничения на значения атрибута');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_CUSTOM_ATTRS_DSC', system_ID, 'Последовательность для идентификатора');

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CUSTOM_ATTRS_DSC');

END$$;


-- Таблица значений по-умолчанию для произвольных атрибутов.
CREATE TABLE CUSTOM_ATTRS_DEF_VALS
(
  ATTR_N          NUMERIC                    NOT NULL -- есть паттерн
, VAL_STRING      CHARACTER VARYING(2000)
, VAL_LONG        NUMERIC
, VAL_BIGDECIMAL  NUMERIC
, VAL_BOOLEAN     NUMERIC(1)
, VAL_DATE        TIMESTAMP WITH TIME ZONE
, DSC             CHARACTER VARYING(1)
, FD              TIMESTAMP WITH TIME ZONE   NOT NULL
, TD              TIMESTAMP WITH TIME ZONE   NOT NULL
, CONSTRAINT      UK_CUSTOM_ATTRS_DEF_VALS_ID UNIQUE (ATTR_N)
);

CREATE INDEX IND_CUSTOM_ATTRS_DEF_VALS_FDTD     ON  CUSTOM_ATTRS_DEF_VALS(FD, TD);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'CUSTOM_ATTRS_DEF_VALS', null, 'Таблица для хранения значений по-умолчанию для описателей произвольных атрибутов расширяемых объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.structure.CustomAttributeDefVal', system_ID, 'Интерфейс значения по-умолчанию произвольного атрибута');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ATTR_N', system_ID, 'Номер описателя атрибута', ''||INE_CORE_SYS_OBJ.get_Column_N('CUSTOM_ATTRS_DSC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VAL_STRING', system_ID, 'Строковое значение');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VAL_LONG', system_ID, 'Числовое значение (целое)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VAL_BIGDECIMAL', system_ID, 'Числовое значение (дробное)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VAL_BOOLEAN', system_ID, 'Булевое значение');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VAL_DATE', system_ID, 'Значение дата');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание-комментарий. Не используется');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CUSTOM_ATTRS_DEF_VALS');

END$$;