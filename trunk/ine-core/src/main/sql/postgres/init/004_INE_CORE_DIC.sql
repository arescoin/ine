/*
"$Id: 004_INE_CORE_DIC.sql 29 2017-04-04 15:32:19Z xerror $"
*/

CREATE TABLE DIC
(
  N            NUMERIC                  NOT NULL
, DIC_NAME     CHARACTER VARYING(2000)  NOT NULL
, DIC_TYPE     NUMERIC                  NOT NULL -- есть паттерн
, CODE_POLICY  NUMERIC                  NOT NULL -- есть паттерн
, FULLY_FILL   NUMERIC                  NOT NULL -- есть паттерн
, UP           NUMERIC                   -- есть паттерн
, DSC          CHARACTER VARYING(2000)
, FD           TIMESTAMP WITH TIME ZONE NOT NULL
, TD           TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_DIC_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_DIC_UP     ON DIC(UP);
CREATE INDEX IND_DIC_FDTD   ON DIC(FD, TD);

-- Последовательность номеров для словарей создаваемых в процессе эксплуатации
CREATE SEQUENCE SEQ_DIC INCREMENT BY 1 START WITH 3000;

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  FAKE_VAR2  NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'DIC', null,'Содержит описания существующих в системе словарей');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.dic.Dictionary', system_ID, 'Интерфейс словаря');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер словаря');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DIC_NAME', system_ID, 'Наименование словаря');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DIC_TYPE', system_ID, 'Тип словаря. Словарь №1');
  -- Паттерн будет добавлен ниже

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(),'CODE_POLICY', system_ID, 'Правило формирования кода словарной статьи. Словарь №2');
  -- Паттерн будет добавлен ниже

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'FULLY_FILL', system_ID, 'Признак необходимости локализации. Словарь №3');
  -- Паттерн будет добавлен ниже

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'UP', system_ID, 'Идентификатор верхнего словаря', '' || INE_CORE_SYS_OBJ.get_Column_N('DIC', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Последовательность номеров для словарей
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(), 'SEQ_DIC', system_ID, 'Последовательность номеров для словарей создаваемых в процессе эксплуатации');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('DIC');

END$$;

CREATE TABLE DIC_DATA
(
  DIC_N       NUMERIC                   NOT NULL -- есть паттерн
, CODE        NUMERIC                   NOT NULL
, LANG        NUMERIC                   NOT NULL -- есть паттерн
, TERM        CHARACTER VARYING(2000)   NOT NULL
, UP          NUMERIC                   -- есть паттерн
, FD          TIMESTAMP WITH TIME ZONE   NOT NULL
, TD          TIMESTAMP WITH TIME ZONE   NOT NULL
, DSC         CHARACTER VARYING(1)
, CONSTRAINT  UK_DIC_DATA_ID  UNIQUE (DIC_N, CODE, LANG)
);

-- Индексы на таблы
CREATE INDEX   IND_DIC_DATA_DN     ON  DIC_DATA(DIC_N);
CREATE INDEX   IND_DIC_DATA_CL     ON  DIC_DATA(CODE, LANG);
CREATE INDEX   IND_DIC_DATA_UP     ON  DIC_DATA(UP);
CREATE INDEX   IND_DIC_DATA_FDTD   ON  DIC_DATA(FD, TD);

DO $$
-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'DIC_DATA', null, 'Содержит значения существующих в системе словарей');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.dic.DictionaryEntry', system_ID, 'Интерфейс словарной статьи');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DIC_N', system_ID, 'Cистемный номер словаря', '' || INE_CORE_SYS_OBJ.get_Column_N('DIC', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CODE', system_ID, 'Код словарной статьи');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LANG', system_ID, 'Язык словаря');
  -- Паттерн будет добавлен позже, в скрипте 008_INE_CORE_LANGUAGE.sql

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TERM', system_ID, 'Термин словарной статьи в указанном языке');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'UP', system_ID, 'Идентификатор кода словарной статьи верхнего словаря');
  -- Как сюда добавить паттерн - яхз. поэтому сделана программная обработка в соответствующем CRUDHelper'е

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание, коментарий. Не используется!');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('DIC_DATA');

-- Обновляем информацию в SYS_OBJ о столбцах с паттернами в табличках SYS_OBJ и DIC

  PERFORM INE_CORE_SYS_OBJ.add_Pattern(INE_CORE_SYS_OBJ.get_Column_N('DIC', 'DIC_TYPE'),
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':1]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE'));

  PERFORM INE_CORE_SYS_OBJ.add_Pattern(INE_CORE_SYS_OBJ.get_Column_N('DIC', 'CODE_POLICY'),
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':2]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE'));

  PERFORM INE_CORE_SYS_OBJ.add_Pattern(INE_CORE_SYS_OBJ.get_Column_N('DIC', 'FULLY_FILL'),
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':3]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE'));

  PERFORM INE_CORE_SYS_OBJ.add_Pattern(INE_CORE_SYS_OBJ.get_Column_N('SYS_OBJ', 'TYP'),
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N')||':4]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE'));
END$$;