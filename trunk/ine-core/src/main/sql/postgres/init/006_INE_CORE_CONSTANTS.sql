/*
"$Id: 006_INE_CORE_CONSTANTS.sql 29 2017-04-04 15:32:19Z xerror $"
*/

-- Перечисление констант
CREATE TABLE CONSTANTS
(
  N               NUMERIC                     NOT NULL
, CONST_NAME      CHARACTER VARYING(2000)     NOT NULL
--, TYP             NUMERIC                     NOT NULL -- есть паттерн
-- , PARAM_MASK      CHARACTER VARYING(2000)
, DEF_VAL         CHARACTER VARYING(2000)     NOT NULL
, IS_NULLABLE     NUMERIC                     NOT NULL
, IS_MODYFABLE    NUMERIC                     NOT NULL
, DSC             CHARACTER VARYING(2000)
, FD              TIMESTAMP WITH TIME ZONE    NOT NULL
, TD              TIMESTAMP WITH TIME ZONE    NOT NULL
, CONSTRAINT      UK_CONSTANTS_ID  UNIQUE (N)
);
-- Индексы на таблы
CREATE INDEX IND_CONSTANTS_FDTD   ON CONSTANTS(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_CONSTANTS INCREMENT BY 1 START WITH 1000;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  DD_N    := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'CONSTANTS', null, 'Содержит описание существующих в системе констант');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.constants.Constant', system_ID, 'Интерфейс константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CONST_NAME', system_ID, 'Наименование константы');

--   SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'CONSTANT_TYPES';
--   FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID,
--     'Тип описываемого элемента (простая, параметризированная). Словарь №'||DIC_N||')', '['||DD_N||':'||DIC_N||']'||DD_CODE);

--   FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PARAM_MASK', system_ID, 'Маска набора параметров (по идентификаторам SYS_OBJ)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DEF_VAL', system_ID, 'Значение по умолчанию');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_NULLABLE', system_ID, 'Признак допустимости значения null');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_MODYFABLE', system_ID, 'Признак допустимости изменения значений константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_CONSTANTS', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CONSTANTS');

END$$;

CREATE TABLE CONSTANT_LIST
(
  CONST_N    NUMERIC                   NOT NULL -- есть паттерн
-- , PARAM      NUMERIC
, CONST_VAL  CHARACTER VARYING(2000)   NOT NULL
, DSC        CHARACTER VARYING(2000)
, FD         TIMESTAMP WITH TIME ZONE  NOT NULL
, TD         TIMESTAMP WITH TIME ZONE  NOT NULL
, CONSTRAINT UK_CONSTANT_LIST_ID  UNIQUE (CONST_N)
);


-- Индексы на таблы
CREATE INDEX IND_CONSTANT_LIST_N       ON  CONSTANT_LIST(CONST_N);
CREATE INDEX IND_CONSTANT_LIST_FDTD    ON  CONSTANT_LIST(FD, TD);
-- CREATE INDEX IND_CONSTANT_LIST_PARAM   ON  CONSTANT_LIST(PARAM);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC ;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'CONSTANT_LIST', null, 'Содержит значения существующих в системе констант');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.constants.ConstantValue', system_ID, 'Интерфейс значения константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CONST_N', system_ID, 'Cистемный номер константы', ''||INE_CORE_SYS_OBJ.get_Column_N('CONSTANTS', 'N'));
--   FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PARAM', system_ID, 'Параметр константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CONST_VAL', system_ID, 'Значение константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание к конкретному значению');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CONSTANT_LIST');

END$$;


------------------------------------------------------------------------------------------------------------------------

-- Перечисление констант
CREATE TABLE CONSTANTS_P
(
  N               NUMERIC                     NOT NULL
, CONST_NAME      CHARACTER VARYING(2000)     NOT NULL
, PARAM_MASK      CHARACTER VARYING(2000)
, DEF_VAL         CHARACTER VARYING(2000)     NOT NULL
, IS_NULLABLE     NUMERIC                     NOT NULL
, IS_MODYFABLE    NUMERIC                     NOT NULL
, DSC             CHARACTER VARYING(2000)
, FD              TIMESTAMP WITH TIME ZONE    NOT NULL
, TD              TIMESTAMP WITH TIME ZONE    NOT NULL
, CONSTRAINT      UK_CONSTANTS_P_ID  UNIQUE (N)
);
-- Индексы на таблы
CREATE INDEX IND_CONSTANTS_P_FDTD   ON CONSTANTS_P(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_CONSTANTS_P INCREMENT BY 1 START WITH 1000;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  DD_N    := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'CONSTANTS_P', null, 'Содержит описание существующих в системе параметризованных констант');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.constants.ParametrizedConstant', system_ID, 'Интерфейс параметризованной константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CONST_NAME', system_ID, 'Наименование константы');

--   SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'CONSTANT_TYPES';
--   FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID,
--     'Тип описываемого элемента (простая, параметризированная). Словарь №'||DIC_N||')', '['||DD_N||':'||DIC_N||']'||DD_CODE);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PARAM_MASK', system_ID, 'Маска набора параметров (по идентификаторам SYS_OBJ)');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DEF_VAL', system_ID, 'Значение по умолчанию');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_NULLABLE', system_ID, 'Признак допустимости значения null');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'IS_MODYFABLE', system_ID, 'Признак допустимости изменения значений константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_CONSTANTS_P', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CONSTANTS_P');

END$$;

CREATE TABLE CONSTANT_P_LIST
(
  CONST_N    NUMERIC                   NOT NULL -- есть паттерн
, PARAM      NUMERIC
, CONST_VAL  CHARACTER VARYING(2000)   NOT NULL
, DSC        CHARACTER VARYING(2000)
, FD         TIMESTAMP WITH TIME ZONE  NOT NULL
, TD         TIMESTAMP WITH TIME ZONE  NOT NULL
, CONSTRAINT UK_CONSTANT_P_LIST_ID  UNIQUE (CONST_N, PARAM)
);


-- Индексы на таблы
CREATE INDEX IND_CONSTANT_P_LIST_N       ON  CONSTANT_P_LIST(CONST_N);
CREATE INDEX IND_CONSTANT_P_LIST_FDTD    ON  CONSTANT_P_LIST(FD, TD);
CREATE INDEX IND_CONSTANT_P_LIST_PARAM   ON  CONSTANT_P_LIST(PARAM);

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC ;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'CONSTANT_P_LIST', null, 'Содержит значения существующих в системе констант');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.constants.ParametrizedConstantValue', system_ID, 'Интерфейс значения параметризованной константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CONST_N', system_ID, 'Cистемный номер константы', ''||INE_CORE_SYS_OBJ.get_Column_N('CONSTANTS_P', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PARAM', system_ID, 'Параметр константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CONST_VAL', system_ID, 'Значение константы');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Описание к конкретному значению');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CONSTANT_P_LIST');

END$$;

------------------------------------------------------------------------------------------------------------------------


-- Вспомогательный "пакет" - схема  для работы с константами
CREATE SCHEMA constants_helper AUTHORIZATION :INE_USR;
GRANT ALL ON SCHEMA constants_helper TO :INE_USR;

-- Возвращает системное значение простой константы
CREATE OR REPLACE FUNCTION constants_helper.safe_Get_Constant_Value(
  in_const_n NUMERIC
) RETURNS CHARACTER VARYING(2000) AS $func$
DECLARE
  ConstVal CHARACTER VARYING(2000);
BEGIN
  BEGIN
     -- Получаем значение константы из таблицы CONSTANT_LIST
     SELECT CONST_VAL INTO ConstVal FROM CONSTANT_LIST WHERE CONST_N = in_const_n;
   EXCEPTION
     WHEN NO_DATA_FOUND THEN
       -- Если ничего там не нашли, берём значение по-умолчанию из таблицы CONSTANTS
       SELECT DEF_VAL INTO ConstVal FROM CONSTANTS WHERE N = in_const_n;
  END;
   RETURN(ConstVal);
   EXCEPTION WHEN NO_DATA_FOUND THEN
     -- Если в таблице CONSTANTS такой константы нет, возвращаем NULL
     RETURN(NULL);
  END;
$func$ LANGUAGE plpgsql;
