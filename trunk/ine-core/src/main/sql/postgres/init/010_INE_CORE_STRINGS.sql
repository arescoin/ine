/*
"$Id: 010_INE_CORE_STRINGS.sql 29 2017-04-04 15:32:19Z xerror $"
*/

CREATE TABLE MESSAGE_STRINGS
(
  MSG_KEY     CHARACTER VARYING(4000)    NOT NULL
, LANG_N      NUMERIC                    NOT NULL -- есть паттерн
, VAL         CHARACTER VARYING(4000)    NOT NULL
, CONSTRAINT  UK_MESSAGE_STRINGS_ID UNIQUE (MSG_KEY, LANG_N)
, CONSTRAINT  FK_MESSAGE_STRINGS_LN FOREIGN KEY (LANG_N) REFERENCES LANGS(N)
);


-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'MESSAGE_STRINGS', null, 'Содержит описание существующих в системе строковых ресурсов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.xr.ine.core.strings.MessageString', system_ID, 'Интерфейс строкового ресурса');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'MSG_KEY', system_ID, 'Уникальный ключ сообщения');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LANG_N', system_ID, 'Системный номер языка, таблица LANGS.N', ''||INE_CORE_SYS_OBJ.get_Column_N('LANGS', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'VAL', system_ID, 'Сообщение на соответствующем языке');

END$$;