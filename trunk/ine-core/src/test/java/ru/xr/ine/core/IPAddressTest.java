/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.ipaddress.IPAddress;

import java.net.UnknownHostException;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IPAddressTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IPAddressTest {

    public static final String IP_ADDRESSES_V4 = "192.168.0.2";
    public static final String IP_ADDRESSES_V6 = "fe80::216:ebff:fe0e:da5c";

    @Test
    public void testV4() {
        test(IP_ADDRESSES_V4);
    }

    @Test
    public void testV6() {
        test(IP_ADDRESSES_V6);
    }


    private void test(String address) {

        try {
            IPAddress ipAddress = IPAddress.parse(address);

            IPAddress ipAddress2 = new IPAddress(ipAddress.asBytes());
            Assert.assertEquals(ipAddress, ipAddress2);

        } catch (UnknownHostException e) {
            fail(e);
        }

    }

    protected static void fail(Exception e) throws AssertionError {
        AssertionError ae = new AssertionError(e.getMessage());
        ae.initCause(e);
        e.printStackTrace();
        throw ae;
    }

}
