/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.core.structure;

import org.junit.Assert;
import org.junit.Test;
import ru.xr.ine.core.IllegalIdException;
import ru.xr.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectPatternTest.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemObjectPatternTest {

    private BigDecimal finalId = new BigDecimal(3);
    private String part_1 = "[1:2]";
    private String part_2 = "[3:4]";
    private String part_3 = "[5:6]";


    @Test
    public void testPatternString() {
        Assert.assertTrue(SystemObjectPattern.validateStringPattern("2"));
        Assert.assertTrue(SystemObjectPattern.validateStringPattern("[3:4]2"));
        Assert.assertTrue(SystemObjectPattern.validateStringPattern("[3:4][3:4]3"));

        Assert.assertTrue(SystemObjectPattern.validateStringPattern("[1:2-3]4"));
        Assert.assertTrue(SystemObjectPattern.validateStringPattern("[1:2,3,4-5]6"));
        Assert.assertTrue(SystemObjectPattern.validateStringPattern("[1:2-3][4:5-6]7"));
        Assert.assertTrue(SystemObjectPattern.validateStringPattern("[1:2-3,4-5,6,7][8:9-10,11,12-13]14"));
    }

    @Test
    public void testPatternStringBad() {
        Assert.assertTrue(!SystemObjectPattern.validateStringPattern("[k:4]"));
        Assert.assertTrue(!SystemObjectPattern.validateStringPattern("[4:k]"));
        Assert.assertTrue(!SystemObjectPattern.validateStringPattern("[4!5]"));
        Assert.assertTrue(!SystemObjectPattern.validateStringPattern("]4:4]"));
        Assert.assertTrue(!SystemObjectPattern.validateStringPattern("[4:4 "));
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testInvalidConstructorStringParam() {
        createStringPattern("");
    }

    @Test(expected = IllegalIdException.class)
    public void testInvalidConstructorNullIdParam() {
        createIDPattern(null);
    }

    @Test(expected = IllegalIdException.class)
    public void testInvalidConstructorNegtIdParam() {
        createIDPattern(new BigDecimal(0));
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testInvalitConstructorString() {
        new SystemObjectPattern("[1:2-3,4,5-6,7-8-9]5");
    }

    @Test
    public void testElementEquals() {
        SystemObjectPattern pattern1 = new SystemObjectPattern("[1:2,3-4]5");
        SystemObjectPattern pattern2 = new SystemObjectPattern("[1:3-4,2]5");
        Assert.assertTrue(pattern1.getElements().iterator().next().equals(pattern2.getElements().iterator().next()));
    }

    @Test
    public void testStringConstructor() {
        SystemObjectPattern pattern = createStringPattern(part_1 + part_2 + part_3 + finalId.intValue());
        Assert.assertTrue(pattern.toString().equals(part_1 + part_2 + part_3 + finalId.intValue()));
        testPattern(pattern);
    }

    @Test
    public void testIdConstructor() {
        testPattern(createIDPattern(finalId));
    }

    private void testPattern(SystemObjectPattern pattern) {
        Assert.assertTrue(pattern.hasElements());
        Assert.assertEquals(3, pattern.getElements().size());
        Assert.assertEquals(pattern.getFinalId(), finalId);

        List<SystemObjectPattern.Element> elements = pattern.getElements();
        Assert.assertTrue(elements.get(0).toString().equals(part_1));
        Assert.assertTrue(elements.get(1).toString().equals(part_2));
        Assert.assertTrue(elements.get(2).toString().equals(part_3));
    }

    private SystemObjectPattern createStringPattern(String s) {
        return new SystemObjectPattern(s);
    }

    private SystemObjectPattern createIDPattern(BigDecimal id) {

        SystemObjectPattern pattern = new SystemObjectPattern(id);
        SystemObjectPattern.Element element1 = new SystemObjectPattern.Element(new BigDecimal(1), "2");
        SystemObjectPattern.Element element2 = new SystemObjectPattern.Element(new BigDecimal(3), "4");
        SystemObjectPattern.Element element3 = new SystemObjectPattern.Element(new BigDecimal(5), "6");

        pattern.addElement(element1);
        pattern.addElement(element2);
        pattern.addElement(element3);

        return pattern;
    }

    @Test
    public void testConstructorStringSinglePattern() {
        String string = "[1:2]3";
        SystemObjectPattern pattern = new SystemObjectPattern(string);
        Assert.assertEquals(string, pattern.toString());

        Collection entityIds = pattern.getElements().iterator().next().getEntityIds();
        Assert.assertEquals(1, entityIds.size());
        Assert.assertTrue(entityIds.iterator().next() instanceof BigDecimal);
    }

    @Test
    public void testConstructorStringIntervalPattern() {
        String string = "[1:2-3]4";
        SystemObjectPattern pattern = new SystemObjectPattern(string);
        Assert.assertEquals(string, pattern.toString());

        Collection entityIds = pattern.getElements().iterator().next().getEntityIds();
        Assert.assertEquals(2, entityIds.size());
    }

    @Test
    public void testConstructorStringComplexPattern() {
        String string = "[1:2-3,4]5";
        SystemObjectPattern pattern = new SystemObjectPattern(string);
        Assert.assertEquals(string, pattern.toString());

        Collection entityIds = pattern.getElements().iterator().next().getEntityIds();
        Assert.assertEquals(3, entityIds.size());
    }
}
