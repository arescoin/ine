/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.utils.config;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestConfigurationManager.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class TestConfigurationManager {

    @Test
    public void testApplicationName() {
        String appName = ConfigurationManager.getManager().getRootConfig().getApplicationName();
        Assert.assertNotNull("Application name is null", appName);
    }

    @Test
    public void testGetConfigFor() {
        SystemConfig systemConfig = ConfigurationManager.getManager().getConfigFor("logging");
        Assert.assertNotNull("Logging configuration not found", systemConfig);

    }

}
