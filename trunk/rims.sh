#!/bin/bash

#  SVNVersion "$Id: rims.sh 29 2017-04-04 15:32:19Z xerror $"
JAVA_HOME=/home/xr/bin/java/jdk1.8.0_112/; export JAVA_HOME;

START_ARG='start'
STOP_ARG='stop'
JAVA_EXEC=$JAVA_HOME/bin/java
TIME_OUT=1

args=("$@")

ARG="$1"

CACHE_PATH="$2"
CACHE_IFS_PATH="$3"
RIMS_API_PATH="$4"
RIMS_COMMON_PATH="$5"
SERVER_PATH="$6"
COMPILE_CPATH="$7"
SECURITY="$8"
INDEX="$9"
PROPS_FILE=${args[9]}
echo ${args[9]}

XDEBUG='-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5006'

CLASS_PATH=$CACHE_PATH:$CACHE_IFS_PATH:$RIMS_API_PATH:$RIMS_COMMON_PATH:$SERVER_PATH:$COMPILE_CPATH

MAIN_CLASS=ru.xr.rims.server.RMSServer

# Стартует сервер кеша
do_Start(){
  echo 'Do start';
  echo 'Params: ' $CLASS_PATH;
  echo 'Arguments: ' $PROPS_FILE;

echo "java -verbose:gc $XDEBUG -Xmx1g -Xms512m $INDEX $SECURITY -classpath $CLASS_PATH $MAIN_CLASS $PROPS_FILE"

  $JAVA_HOME/bin/java -verbose:gc $XDEBUG -Xmx1g -Xms512m $INDEX $SECURITY -classpath $CLASS_PATH $MAIN_CLASS $PROPS_FILE > server.log 2>&1 &
  pid=$!

  echo 'PROCESS PID' $pid
}

# Убивает процесс кеша
do_Stop(){
  echo 'Do stop';
  echo 'Params: ' $CLASS_PATH;
  echo 'Arguments: ' $PROPS_FILE;
  
  $JAVA_HOME/bin/java -classpath $CLASS_PATH ru.xr.rims.server.admin.Killer $PROPS_FILE
}

do_Help(){
  echo 'Use [start|stop] arguments';
}

if [ -z $ARG ]
then
  echo "Argumen not specified"
else
  if [ $ARG = $START_ARG ]
  then
    do_Start;
    exit 0;
  else
    if [ $ARG = $STOP_ARG ]
    then
      do_Stop;
      exit 0;
    else
      echo 'Unknown argument' $ARG;
      do_Help;
      exit 100;
    fi
  fi
fi
