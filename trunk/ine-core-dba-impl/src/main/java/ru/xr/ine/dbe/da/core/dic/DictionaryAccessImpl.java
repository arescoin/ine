/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.dic;

import ru.xr.ine.core.*;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class DictionaryAccessImpl<T extends Dictionary>
        extends AbstractVersionableAccess<T> implements DictionaryAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "DIC";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_DIC_NAME = "dic_name";
    public static final String COLUMN_DIC_TYPE = "dic_type";
    public static final String COLUMN_ENTRY_CODE = "code_policy";
    public static final String COLUMN_FULLY_FILL = "fully_fill";
    public static final String COLUMN_UP = "up";

    /** Запрос на выборку всех словарей */
    public static final String ALL_DICTIONARIES_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DIC_NAME +
            ", " + TABLE_PREF + COLUMN_DIC_TYPE +
            ", " + TABLE_PREF + COLUMN_ENTRY_CODE +
            ", " + TABLE_PREF + COLUMN_FULLY_FILL +
            ", " + TABLE_PREF + COLUMN_UP +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.dic.Dictionary} */
    public static final String CACHE_REGION_DICTIONARIES = Dictionary.class.getName();

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(Dictionary.NAME, COLUMN_DIC_NAME);
        map.put(Dictionary.DIC_TYPE, COLUMN_DIC_TYPE);
        map.put(Dictionary.CODE_POLICY, COLUMN_ENTRY_CODE);
        map.put(Dictionary.L10N_POLICY, COLUMN_FULLY_FILL);
        map.put(Dictionary.PARENT, COLUMN_UP);
        setInstanceFieldsMapping(DictionaryAccessImpl.class, map);

        registerVersionableHelpers(DictionaryAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_DICTIONARIES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(Dictionary.class);

        this.setVersionableFields(resultSet, result);

        result.setName(this.getString(resultSet, COLUMN_DIC_NAME));
        result.setDicType(this.getId(resultSet, COLUMN_DIC_TYPE));
        result.setEntryCodePolicy(this.getId(resultSet, COLUMN_ENTRY_CODE));
        result.setLocalizationPolicy(this.getId(resultSet, COLUMN_FULLY_FILL));
        result.setParentId(this.getBigDecimal(resultSet, COLUMN_UP));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_DICTIONARIES, ALL_DICTIONARIES_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_DICTIONARIES, ALL_DICTIONARIES_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_DICTIONARIES, ALL_DICTIONARIES_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_DICTIONARIES);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        //проверяем в удаляемом словаре нет статей
        DictionaryEntryAccess<DictionaryEntry> access = (DictionaryEntryAccess<DictionaryEntry>)
                AccessFactory.getImplementation(DictionaryEntry.class);
        if (access.getAllEntriesForDictionary(identifiable.getCoreId()).size() != 0) {
            throw new IllegalArgumentException("Dictionary is not empty so it can not be deleted.");
        }
        this.deleteObject(identifiable, CACHE_REGION_DICTIONARIES, ALL_DICTIONARIES_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Dictionary.class;
    }

}
