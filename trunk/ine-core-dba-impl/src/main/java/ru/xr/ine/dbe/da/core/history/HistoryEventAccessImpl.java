/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.RuntimeCoreException;
import ru.xr.ine.core.history.HistoryEvent;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Предоставляет методы для работы с историей изменений с признаком ее обработки системой нотификации
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class HistoryEventAccessImpl<T extends HistoryEvent> extends VersionableHistoryAccessImpl<T>
        implements HistoryEventAccess<T> {

    /** Наименование колонки с признаком обработки */
    public static final String COLUMN_PROCESSED = "processed";

    public static final String SELECT_HISTORY_BY_PROCESSED = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ENT_ID +
            ", " + TABLE_PREF + COLUMN_USER_ID +
            ", " + TABLE_PREF + COLUMN_ACTION_DATE +
            ", " + TABLE_PREF + COLUMN_ACTION_TYP +
            ", " + TABLE_PREF + COLUMN_REASON +
            ", " + TABLE_PREF + AbstractVersionableAccess.COLUMN_FD +
            ", " + TABLE_PREF + AbstractVersionableAccess.COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_PROCESSED +
            " FROM " + TABLE +
            " WHERE " + TABLE_PREF + COLUMN_PROCESSED + " = ?";

    public static final String UPDATE_STRING = "UPDATE " + TABLE + " SET " + COLUMN_PROCESSED + " = 1 WHERE "
            + COLUMN_ID + " = ? AND "
            + COLUMN_ENT_ID + " = ?  and "
            + AbstractVersionableAccess.COLUMN_FD + " = ? and "
            + AbstractVersionableAccess.COLUMN_TD + " = ? and "
            + COLUMN_USER_ID + " = ? and "
            + COLUMN_ACTION_TYP + "= ? and "
            + COLUMN_ACTION_DATE + " = ?";


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(HistoryEvent.PROCESSED, HistoryEventAccessImpl.COLUMN_PROCESSED);
        setInstanceFieldsMapping(VersionableHistoryAccessImpl.class, map);

        registerIdentifiableHistoryHelpers(HistoryEventAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    protected void setBaseFields(ResultSet resultSet, T obj) throws SQLException, CoreException, RuntimeCoreException {
        super.setBaseFields(resultSet, obj);
        obj.setProcessed(getBoolean(resultSet, COLUMN_PROCESSED));
    }

    @Override
    public T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"})
        T historyEvent = (T) IdentifiableFactory.getImplementation(HistoryEvent.class);
        setBaseFields(resultSet, historyEvent);

        return historyEvent;
    }

    @Override
    public Collection<T> getEventsForProcess() throws CoreException {
        return getObjects(false, SELECT_HISTORY_BY_PROCESSED + " limit 300", 0);
//        return getObjects(false, SELECT_HISTORY_BY_PROCESSED + " and rownum <= " + 300, 0);
    }

    @Override
    public Collection<T> getProcessedEvents() throws CoreException {
        return getObjects(false, SELECT_HISTORY_BY_PROCESSED, 1);
    }

    @Override
    public void markAsProcessed(T historyEvent) throws GenericSystemException {
        historyEvent.setProcessed(true);
        this.updateObjectInDB(UPDATE_STRING, new Object[]{historyEvent.getCoreId(),
                historyEvent.getEntityId().toString(), historyEvent.getFd(),
                historyEvent.getTd(), historyEvent.getUserId(),
                historyEvent.getType().getCode(), historyEvent.getActionDate()});
    }

}
