/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.funcswitch;

import ru.xr.ine.core.*;
import ru.xr.ine.core.funcswitch.FuncSwitchName;
import ru.xr.ine.dbe.da.core.AbstractIdentifiableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class FuncSwitchNameAccessImpl<T extends FuncSwitchName>
        extends AbstractIdentifiableAccess<T> implements FuncSwitchNameAccess<T> {

    /** Таблица описаний переключателей */
    public static final String TABLE = "FUNC_SWITCH";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_FUNC_NAME = "func_name";

    /** Запрос на выборку всех значений */
    public static final String ALL_DATA_SELECT = "SELECT "
            + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FUNC_NAME +
            " FROM " + TABLE;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.funcswitch.FuncSwitchName} */
    public static final String CACHE_REGION_FUNC_SWITCH_NAMES = FuncSwitchName.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(FuncSwitchName.NAME, COLUMN_FUNC_NAME);
        setInstanceFieldsMapping(FuncSwitchNameAccessImpl.class, map);

        registerIdentifiableHelpers(FuncSwitchNameAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_FUNC_SWITCH_NAMES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(FuncSwitchName.class);

        this.setIdentifiableFields(resultSet, value);
        value.setName(this.getString(resultSet, COLUMN_FUNC_NAME));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_FUNC_SWITCH_NAMES, ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_FUNC_SWITCH_NAMES, ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_FUNC_SWITCH_NAMES, ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_FUNC_SWITCH_NAMES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_FUNC_SWITCH_NAMES, ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return FuncSwitchName.class;
    }

}
