/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.*;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.oss.inventory.item.PropertyData;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractPropertyDataAccess.java 67 2017-05-19 09:55:27Z xerror $"
 */
public abstract class AbstractPropertyDataAccess<T extends PropertyData>
        extends AbstractVersionableAccess<T> implements PropertyDataAccess<T> {

    /**
     * Перечисление колонок таблицы
     */
    public static final String COLUMN_ITEM_N = "item_n";
    public static final String COLUMN_DSC_ID = "descriptor_n";
    public static final String COLUMN_VAL = "val";


    protected abstract void setData(ResultSet resultSet, String columnName, T object) throws SQLException;

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(getSubjectClass());

        this.setVersionableFields(resultSet, value);

        value.setItemId(this.getId(resultSet, COLUMN_ITEM_N));
        value.setDescriptorId(this.getId(resultSet, COLUMN_DSC_ID));
        setData(resultSet, COLUMN_VAL, value);

        return value;
    }

//    @Override
//    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
//        return this.createObject(identifiable, getMainRegionKey(), getCommonSelect(), true);
//    }
//
//    @Override
//    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
//        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
//    }
//
//    @Override
//    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
//        this.deleteObject(identifiable, getMainRegionKey(), getCommonSelect());
//    }

//    @Override
//    protected Class getSubjectClass() throws GenericSystemException {
//        return Item.class;
//    }

}
