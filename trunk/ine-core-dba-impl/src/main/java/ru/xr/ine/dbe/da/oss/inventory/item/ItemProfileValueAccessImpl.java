/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.structure.PatternUtils;
import ru.xr.ine.oss.inventory.item.ItemProfileValue;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileValueAccessImpl.java 37 2017-04-06 20:27:32Z xerror $"
 */
public class ItemProfileValueAccessImpl<T extends ItemProfileValue>
        extends AbstractVersionableAccess<T> implements ItemProfileValueAccess<T> {


    /**
     * Таблица и префикс для обращения к ее столбцам
     */
    public static final String TABLE = "INV_PRFL_VALS";
    public static final String TABLE_PREF = TABLE + DOT;

    /**
     * Перечисление колонок таблицы
     */
    public static final String COLUMN_PROFILE_N = "PROFILE_N";
    public static final String COLUMN_VAL_DSC_N = "VAL_DSC_N";
    public static final String COLUMN_PATTERN = "PATTERN";
    public static final String COLUMN_REQ = "IS_RQRD";


    /**
     * Запрос на выборку всех значений
     */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_PROFILE_N +
            ", " + TABLE_PREF + COLUMN_VAL_DSC_N +
            ", " + TABLE_PREF + COLUMN_PATTERN +
            ", " + TABLE_PREF + COLUMN_REQ +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /**
     * Регион кэша для хранения данных типа {@link ItemProfileValue}
     */
    public static final String CACHE_REGION_ITEM_PROFILE_VALS = ItemProfileValue.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(ItemProfileValue.ITEM_PROFILE_ID, COLUMN_PROFILE_N);
        map.put(ItemProfileValue.VALUE_DESCRIPTOR_ID, COLUMN_VAL_DSC_N);
        map.put(ItemProfileValue.PATTERN, COLUMN_PATTERN);
        map.put(ItemProfileValue.REQUIRED, COLUMN_REQ);
        setInstanceFieldsMapping(ItemProfileValueAccessImpl.class, map);

        registerVersionableHelpers(ItemProfileValueAccessImpl.class, new LinkedHashSet<>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(getSubjectClass());

        this.setVersionableFields(resultSet, value);

        value.setItemProfileId(this.getId(resultSet, COLUMN_PROFILE_N));
        value.setValueDescriptorId(this.getId(resultSet, COLUMN_VAL_DSC_N));

        String pattern = this.getString(resultSet, COLUMN_PATTERN);
        if (pattern != null) {
            value.setPattern(PatternUtils.checkPattern(new SystemObjectPattern(pattern)));
        }

        value.setRequired(this.getBooleanNullable(resultSet, COLUMN_REQ));

        return value;
    }


    @Override
    protected String getCommonSelect() {
        return ALL_DATA_SELECT;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_ITEM_PROFILE_VALS;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(getMainRegionKey(), getCommonSelect());
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(getMainRegionKey(), getCommonSelect(), id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, getMainRegionKey(), getCommonSelect(), true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, getMainRegionKey(), getCommonSelect());
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ItemProfileValue.class;
    }
}
