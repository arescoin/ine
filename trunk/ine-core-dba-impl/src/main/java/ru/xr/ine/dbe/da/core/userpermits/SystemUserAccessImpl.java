/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import ru.xr.ine.core.*;
import ru.xr.ine.core.userpermits.NoSuchUserException;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class SystemUserAccessImpl<T extends SystemUser>
        extends AbstractVersionableAccess<T> implements SystemUserAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "SYS_USERS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ACTIVE = "is_active";
    public static final String COLUMN_SYS_LOGIN = "sys_login";
    public static final String COLUMN_MD5_HASH = "md5_hash";

    /** Запрос на выборку всех системных пользователей */
    public static final String ALL_SYS_USERS_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_SYS_LOGIN +
            ", " + TABLE_PREF + COLUMN_MD5_HASH +
            ", " + TABLE_PREF + COLUMN_ACTIVE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.userpermits.SystemUser} */
    public static final String CACHE_REGION_SYSTEM_USERS = SystemUser.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(SystemUser.SYS_LOGIN, COLUMN_SYS_LOGIN);
        map.put(SystemUser.MD5_HASH, COLUMN_MD5_HASH);
        map.put(SystemUser.ACTIVE, COLUMN_ACTIVE);
        setInstanceFieldsMapping(SystemUserAccessImpl.class, map);

        registerVersionableHelpers(SystemUserAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SYSTEM_USERS;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(SystemUser.class);

        this.setVersionableFields(resultSet, result);
        result.setActive(this.getBoolean(resultSet, COLUMN_ACTIVE));
        result.setMd5(this.getString(resultSet, COLUMN_MD5_HASH));
        result.setLogin(this.getString(resultSet, COLUMN_SYS_LOGIN));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_SYSTEM_USERS, ALL_SYS_USERS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_SYSTEM_USERS, ALL_SYS_USERS_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_SYSTEM_USERS, ALL_SYS_USERS_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_SYSTEM_USERS);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_SYSTEM_USERS, ALL_SYS_USERS_SELECT);
    }

    @Override
    public T login(String login, String md5) throws GenericSystemException {

        if ((login == null) || (md5 == null)) {
            throw new GenericSystemException("Both login and password must be set");
        }

        Collection<T> users;
        try {
            users = getAllObjects();
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        for (T user : users) {
            if ((login.equals(user.getLogin())) && (md5.equals(user.getMd5()))) {
                return user;
            }
        }

        throw new NoSuchUserException("User not found");
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return SystemUser.class;
    }

}
