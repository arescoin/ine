/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.links;

import ru.xr.ine.core.*;
import ru.xr.ine.core.links.Link;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.dbe.da.core.*;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.core.structure.PatternUtils;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class LinkAccessImpl<T extends Link> extends AbstractVersionableAccess<T> implements LinkAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "LINKS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_LEFT_OBJ = "left_obj";
    public static final String COLUMN_RIGHT_OBJ = "right_obj";
    public static final String COLUMN_TYP = "typ";

    /** Запрос на выборку всех описаний связей */
    public static final String ALL_LINKS_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_LEFT_OBJ +
            ", " + TABLE_PREF + COLUMN_RIGHT_OBJ +
            ", " + TABLE_PREF + COLUMN_TYP +
            ", NULL AS " + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.links.Link} */
    public static final String CACHE_REGION_LINKS = Link.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(Link.CORE_DSC, null);
        map.put(Link.LEFT_TYPE, COLUMN_LEFT_OBJ);
        map.put(Link.RIGHT_TYPE, COLUMN_RIGHT_OBJ);
        map.put(Link.TYPE, COLUMN_TYP);
        setInstanceFieldsMapping(LinkAccessImpl.class, map);

        Set<CRUDHelper> linkHelpers = new LinkedHashSet<CRUDHelper>(1);
        linkHelpers.add(new LinkHelper());
        registerVersionableHelpers(LinkAccessImpl.class, linkHelpers);
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_LINKS;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(Link.class);

        this.setVersionableFields(resultSet, result);

        result.setLeftType(PatternUtils.checkPattern(
                new SystemObjectPattern(this.getString(resultSet, COLUMN_LEFT_OBJ))));
        result.setRightType(PatternUtils.checkPattern(
                new SystemObjectPattern(this.getString(resultSet, COLUMN_RIGHT_OBJ))));
        result.setTypeId(this.getId(resultSet, COLUMN_TYP));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_LINKS, ALL_LINKS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_LINKS, ALL_LINKS_SELECT, id);
    }

    @Override
    public Collection<T> getLinksByObjType(Class clazz) throws GenericSystemException {

        // Метод ищет все линки, в которых участвует переданный класс.
        // Для этого надо выполнить 2 поиска по обоим участникам связи.
        if (clazz == null) {
            throw new IneIllegalArgumentException("clazz parameter can't be null.");
        }
        BigDecimal objId = FieldAccess.getTableId(clazz);

        Set<T> result = new HashSet<T>();
        SearchCriteriaProfile profile = getSearchCriteriaProfile();

        Set<SearchCriterion> criteria = new HashSet<SearchCriterion>();
        // Для поиска по участникам связи используется поиск с компаратором, который создаётся следующим образом:
        // первым объектом в компаратор передаётся значение, извлечённое из поля сохранённого в кеше искомого объекта;
        // а вторым объектом передаётся значение, выставленное в поисковом условии.
        criteria.add(profile.getSearchCriterion(T.LEFT_TYPE, T.PATTERN_COMPARATOR, CriteriaRule.equals, objId));
        result.addAll(searchObjects(criteria));

        criteria.clear();
        criteria.add(profile.getSearchCriterion(T.RIGHT_TYPE, T.PATTERN_COMPARATOR, CriteriaRule.equals, objId));
        result.addAll(searchObjects(criteria));

        return result;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_LINKS, ALL_LINKS_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_LINKS);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_LINKS, ALL_LINKS_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Link.class;
    }


    public static final class LinkHelper<E extends Link> extends AbstractCRUDHelper<E> {

        @SuppressWarnings({"unchecked"})
        public LinkAccess<Link> getAccess() throws GenericSystemException {
            return (LinkAccess<Link>) AccessFactory.getImplementation(Link.class);
        }

        @Override
        public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {
            this.checkDuplicates(identifiable, false);
        }

        @Override
        public void beforeUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {
            this.checkDuplicates(newIdentifiable, true);
        }

        private void checkDuplicates(E identifiable, boolean update) throws GenericSystemException {

            SearchCriteriaProfile profile = this.getAccess().getSearchCriteriaProfile();
            Set<SearchCriterion> criteria = new LinkedHashSet<SearchCriterion>(4);
            criteria.add(profile.getSearchCriterion(
                    E.LEFT_TYPE, E.PATTERN_COMPARATOR, CriteriaRule.equals, identifiable.getLeftType().getTableId()));
            criteria.add(profile.getSearchCriterion(
                    E.RIGHT_TYPE, E.PATTERN_COMPARATOR, CriteriaRule.equals, identifiable.getRightType().getTableId()));
            criteria.add(profile.getSearchCriterion(E.TYPE, CriteriaRule.equals, identifiable.getTypeId()));

            if (update) {
                SearchCriterion coreId =
                        profile.getSearchCriterion(E.CORE_ID, CriteriaRule.equals, identifiable.getCoreId());
                coreId.setInvert(true);
                criteria.add(coreId);
            }

            if (this.getAccess().containsObjects(criteria)) {
                throw new IneIllegalArgumentException("There are exists link with same attributes: " +
                        "leftType.tableId[" + identifiable.getLeftType().getTableId() + "], " +
                        "rightType.tableId[" + identifiable.getRightType().getTableId() + "], " +
                        "typeId[" + identifiable.getTypeId() + "]");
            }
        }
    }

}
