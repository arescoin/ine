/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.*;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.oss.inventory.item.ItemProfile;
import ru.xr.ine.oss.inventory.item.ItemProfileImpl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfileAccessImpl.java 35 2017-04-05 13:55:01Z xerror $"
 */
public class ItemProfileAccessImpl<T extends ItemProfile>
        extends AbstractVersionableAccess<T> implements ItemProfileAccess<T> {

    /**
     * Таблица и префикс для обращения к ее столбцам
     */
    public static final String TABLE = "INV_ITM_PRFL";
    public static final String TABLE_PREF = TABLE + DOT;

    /**
     * Перечисление колонок таблицы
     */
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TYPE = "typ";

    /**
     * Запрос на выборку всех значений
     */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_NAME +
            ", " + TABLE_PREF + COLUMN_TYPE +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /**
     * Регион кэша для хранения данных типа {@link ItemProfile}
     */
    public static final String CACHE_REGION_ITEM_PROFILE = ItemProfile.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(ItemProfile.PROFILE_NAME, COLUMN_NAME);
        map.put(ItemProfile.ITEM_TYPE, COLUMN_TYPE);
        setInstanceFieldsMapping(ItemProfileAccessImpl.class, map);

        registerVersionableHelpers(ItemProfileAccessImpl.class, new LinkedHashSet<>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_ITEM_PROFILE;
    }


    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(ItemProfile.class);

        this.setVersionableFields(resultSet, value);

        value.setItemType(this.getId(resultSet, COLUMN_TYPE));
        value.setName(this.getString(resultSet, COLUMN_NAME));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(getMainRegionKey(), ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(getMainRegionKey(), ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, getMainRegionKey(), ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, getMainRegionKey(), ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ItemProfile.class;
    }
}
