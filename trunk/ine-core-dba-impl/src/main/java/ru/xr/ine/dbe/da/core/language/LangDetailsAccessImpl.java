/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.language;

import ru.xr.ine.core.*;
import ru.xr.ine.core.language.LangDetails;
import ru.xr.ine.dbe.da.core.AbstractIdentifiableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class LangDetailsAccessImpl<T extends LangDetails>
        extends AbstractIdentifiableAccess<T> implements LangDetailsAccess<T> {

    /** Таблица состояний переключателя и префикс для обращения к ее столбцам */
    public static final String TABLE = "LANG_CODES";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_LNG_CODE = "lng_code";
    public static final String COLUMN_LNG_NAME = "lng_name";

    /** Запрос на выборку всех описаний языков */
    public static final String ALL_LANGUAGES_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_LNG_CODE +
            ", " + TABLE_PREF + COLUMN_LNG_NAME +
            ", NULL AS " + COLUMN_DSC +
            " FROM " + TABLE;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.language.LangDetails} */
    public static final String CACHE_REGION_LANGUAGES = LangDetails.class.getName();

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(Identifiable.CORE_DSC, null);
        map.put(LangDetails.CODE, COLUMN_LNG_CODE);
        map.put(LangDetails.NAME, COLUMN_LNG_NAME);
        setInstanceFieldsMapping(LangDetailsAccessImpl.class, map);

        registerIdentifiableHelpers(LangDetailsAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_LANGUAGES;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_LANGUAGES, ALL_LANGUAGES_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_LANGUAGES, ALL_LANGUAGES_SELECT, id);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(LangDetails.class);

        this.setIdentifiableFields(resultSet, result);

        result.setCode(this.getString(resultSet, COLUMN_LNG_CODE));
        result.setName(this.getString(resultSet, COLUMN_LNG_NAME));

        return result;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_LANGUAGES, ALL_LANGUAGES_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_LANGUAGES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_LANGUAGES, ALL_LANGUAGES_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return LangDetails.class;
    }

}
