/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.language;

import ru.xr.ine.core.*;
import ru.xr.ine.core.language.CountryDetails;
import ru.xr.ine.dbe.da.core.AbstractIdentifiableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class CountryDetailsAccessImpl<T extends CountryDetails>
        extends AbstractIdentifiableAccess<T> implements CountryDetailsAccess<T> {

    /** Таблица состояний переключателя и префикс для обращения к ее столбцам */
    public static final String TABLE = "COUNTRY_CODES";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_COUNTRY = "country";
    public static final String COLUMN_A2 = "a2";
    public static final String COLUMN_A3 = "a3";

    /** Запрос на выборку всех кодов стран */
    public static final String ALL_COUNTRIES_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_CODE +
            ", " + TABLE_PREF + COLUMN_COUNTRY +
            ", " + TABLE_PREF + COLUMN_A2 +
            ", " + TABLE_PREF + COLUMN_A3 +
            ", NULL AS " + COLUMN_DSC +
            " FROM " + TABLE;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.language.CountryDetails} */
    public static final String CACHE_REGION_COUNTRIES = CountryDetails.class.getName();

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(CountryDetails.CORE_DSC, null);
        map.put(CountryDetails.CORE_ID, COLUMN_CODE);
        map.put(CountryDetails.NAME, COLUMN_COUNTRY);
        map.put(CountryDetails.CODE_A2, COLUMN_A2);
        map.put(CountryDetails.CODE_A3, COLUMN_A3);
        setInstanceFieldsMapping(CountryDetailsAccessImpl.class, map);

        setIdColumns(CountryDetailsAccessImpl.class, new String[]{COLUMN_CODE});

        registerIdentifiableHelpers(CountryDetailsAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_COUNTRIES;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_COUNTRIES, ALL_COUNTRIES_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_COUNTRIES, ALL_COUNTRIES_SELECT, id);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(CountryDetails.class);

        this.setIdentifiableFields(resultSet, result, COLUMN_CODE);

        result.setName(this.getString(resultSet, COLUMN_COUNTRY));
        result.setCodeA2(this.getString(resultSet, COLUMN_A2));
        result.setCodeA3(this.getString(resultSet, COLUMN_A3));

        return result;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_COUNTRIES, ALL_COUNTRIES_SELECT, false);
    }

    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_COUNTRIES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_COUNTRIES, ALL_COUNTRIES_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return CountryDetails.class;
    }

}
