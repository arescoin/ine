/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure.test;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.structure.test.TestObject;
import ru.xr.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Доступ к тестовым объектам из таблицы TEST_TABLE.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: TestObjectAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public interface TestObjectAccess<T extends TestObject> extends VersionableAccess<T> {

    /**
     * Возвращает все актуальные объекты
     *
     * @return коллекция с {@link ru.xr.ine.core.structure.test.TestObject}, может быть пустой
     * @throws ru.xr.ine.core.CoreException
     *          при возникновении ошибке в процессе получения и обработки данных
     */
    Collection<T> getAllObjects() throws CoreException;
}
