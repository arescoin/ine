/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.*;
import ru.xr.ine.core.constants.ParametrizedConstant;
import ru.xr.ine.core.constants.ParametrizedConstantValue;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.core.links.Link;
import ru.xr.ine.core.links.LinkData;
import ru.xr.ine.core.strings.MessageString;
import ru.xr.ine.core.structure.*;
import ru.xr.ine.dbe.da.core.constants.ParametrizedConstantAccess;
import ru.xr.ine.dbe.da.core.constants.ParametrizedConstantValueAccess;
import ru.xr.ine.dbe.da.core.links.LinkAccess;
import ru.xr.ine.dbe.da.core.links.LinkDataAccess;
import ru.xr.ine.dbe.da.core.structure.CustomAttributeAccess;
import ru.xr.ine.dbe.da.core.structure.CustomAttributeValueAccess;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.dbe.da.core.structure.PatternUtils;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * CRUD-helper для поиска удаляемого объекта в связанных через паттерн объектах.
 * Т.е. ищет использование данного объекта в других объектах, а связь осуществляется через паттерн.
 */
@SuppressWarnings({"unchecked"})
final class PatternHelper<E extends Identifiable> extends AbstractCRUDHelper<E> {

    private static final Logger logger = Logger.getLogger(PatternHelper.class.getName());

    @Override
    public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {

        // по номеру таблицы получим все системные объекты ее составляющие
        BigDecimal tableId = FieldAccess.getTableId(identifiable.getClass());
        Collection<SystemObject> systemObjects = FieldAccess.getPatternsForObject(tableId);

        try {
//            IdentifiableAccess access = (IdentifiableAccess)
//                    AccessFactory.getImplementation(Class.forName(FieldAccess.getInterface(tableId)));

            if (!systemObjects.isEmpty()) {
                for (SystemObject systemObject : systemObjects) {

                    String fieldName =
                            PatternUtils.getInvertedFieldsMap(access).get(systemObject.getName());

                    SystemObjectPattern pattern = systemObject.getPattern();

                    SearchCriteriaProfile searchCriteriaProfile = access.getSearchCriteriaProfile();

                    Object fieldValue;

                    // По имени атрибута получаем его значение
                    if (searchCriteriaProfile.getProfileMap().containsKey(fieldName)) {
                        // если это простой атрибут
                        fieldValue = searchCriteriaProfile.getSearchCriterion(fieldName).extractValue(identifiable);
                    } else {
                        // если это непримитив
                        fieldValue = searchCriteriaProfile.getSearchCriterion(
                                fieldName,
                                PatternUtils.PATTERN_TO_ID_COMPARATOR,
                                CriteriaRule.equals, BigDecimal.ZERO
                        ).extractValue(identifiable);

                        if (fieldValue instanceof DataType) {
                            fieldValue = new BigDecimal(((DataType) fieldValue).ordinal() + 1);
                        }
                    }
                    if (null == fieldValue && !systemObject.isRequired()) {
                        return;
                    }

                    Collection<Identifiable> identifiableCollection = new ArrayList<>();
                    if (fieldValue != null && fieldValue instanceof BigDecimal) {
                        identifiableCollection = PatternUtils.getObjectsByPattern(pattern, (BigDecimal) fieldValue);
                    }
                    if (identifiableCollection.isEmpty()) {
                        throw new IneIllegalArgumentException(
                                "Can't create object " + identifiable
                                        + ", cause it have unresolved dependency for: " + fieldName);
                    }
                }
            }
        } catch (Exception e) {
            throw GenericSystemException.toGeneric(e);
        }

    }

    @Override
    public void beforeDelete(E identifiable) throws GenericSystemException {

        String message;

        // Поиск через SystemObject
        message = this.searchInSystemObjects(identifiable);
        if (message != null) {
            throw new IneIllegalArgumentException(
                    "Can't delete object " + identifiable + ", cause it is used in " + message);
        }

        // Поиск в параметризованных константах
        message = this.searchInParametrizedConstants(identifiable);
        if (message != null) {
            throw new IneIllegalArgumentException("Can't delete object " + identifiable +
                    ", cause it is used in " + ParametrizedConstant.class.getSimpleName() + "[" + message + "]");
        }

        // Поиск в кастомных атрибутах
        message = this.searchInCustomAttributes(identifiable);
        if (message != null) {
            throw new IneIllegalArgumentException("Can't delete object " + identifiable +
                    ", cause it is used in " + CustomAttribute.class.getSimpleName() + "[" + message + "]");
        }

        // Поиск по линкам. Сначала слева, потом справа.
        message = this.searchInLinks(identifiable, true);
        if (message != null) {
            throw new IneIllegalArgumentException("Can't delete object " + identifiable + ", cause it is used in " +
                    Link.class.getSimpleName() + "[" + message + "] on left side");
        }
        message = this.searchInLinks(identifiable, false);
        if (message != null) {
            throw new IneIllegalArgumentException("Can't delete object " + identifiable + ", cause it is used in " +
                    Link.class.getSimpleName() + "[" + message + "] on right side");
        }

        /*
         * TODO: Проверить, как будет всё это работать на большой портянке данных.
         */
    }

    /**
     * Ищем по системным объектам, в паттернах которых описан переданный объект.<br>
     * Поиск осуществляется последовательно по всем паттернам. Если найдено использование, то поиск прерывается
     * и возвращается класс интерфейса, в котором найдено использование переданного объекта
     *
     * @param identifiable проверяемый объект
     *
     * @return Если переданный объект используется, то вернётся имя интерфейса, использующего переданный объект.
     *         иначе вернётся <code>null</code>
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private String searchInSystemObjects(E identifiable) throws GenericSystemException {

        BigDecimal tableId = FieldAccess.getTableId(identifiable.getClass());
        for (SystemObject systemObject : FieldAccess.getPatternObjects(tableId)) {
            String iFace = null;
            try {
                iFace = this.searchBySystemObject(identifiable, systemObject);
            } catch (ClassNotFoundException e) {
                logger.log(Level.INFO, e.getMessage(), e);
            }
            if (iFace != null) {
                return iFace;
            }
        }

        return null;
    }

    /**
     * Ищем по объекту, описанному в системном объекте с паттерном.<br>
     * <ul>
     * <li>По системному объекту определяем акцесс объекта, с которому связан переданный объект</li>
     * <li>По паттерну в системном объекте определяем поле, по которому нужно искать переданный объект</li>
     * <li>Из переданного объекта посредством SynteticId определяем поисковое значение</li>
     * <li>Ищем использование переданного объекта</li>
     * </ul>
     *
     * @param identifiable проверяемый объект
     * @param systemObject системный объект, в котором содержится связующий паттерн
     *
     * @return Если переданный объект используется, то вернётся имя интерфейса, использующего переданный объект.
     *         иначе вернётся <code>null</code>
     * @throws ClassNotFoundException если класс не найден (такой ошибки быть не должно при нормальной работе)
     * @throws ru.xr.ine.core.GenericSystemException
     *                                в случае ошибок при работе с кешом и мета-информацией
     */
    private String searchBySystemObject(E identifiable, SystemObject systemObject)
            throws ClassNotFoundException, GenericSystemException {

        try {
            // Получаем значение для поиска
            BigDecimal id = this.getSearchValueByPattern(identifiable, systemObject.getPattern());
            if (id == null) {
                return null;
            }

            // Получаем родительский системный объект, в котором содержится имя таблицы.
            SystemObject parent = FieldAccess.getObjectById(systemObject.getUp());
            String iFaceName = FieldAccess.getInterface(parent.getCoreId());
            if (MessageString.class.getName().equals(iFaceName)) {
                logger.log(Level.WARNING, "Can not search usages of object in {0}. Object: {1}",
                        new Object[]{MessageString.class.getName(), identifiable});
                return null;//TODO подумать над этим костылём! у стрингов нету поиска.
            }

            // А по таблице получаем дата-акцесс, который и будет искать использование переданного объекта.
            IdentifiableAccess access = this.getAccess(iFaceName);

            // Получаем инвертированную мапу с полями таблицы и методами. Нужно для определения поискового поля
            Map<String, String> fieldsMap = PatternUtils.getInvertedFieldsMap(access);

            if (fieldsMap.get(systemObject.getName()) == null) {

                /**
                 * TODO пофиксить когда-нибудь...
                 * Такой костыль пришлось сделать из-за особенностей работы с DictionaryEntry/DictionaryTerm.
                 * Оба интерфейса используют одну и ту же таблицу (DIC_DATA), но в самой таблице прописан только
                 * один интерфейс - DictionaryEntry. Поэтому получается фигня, при удалении LangName.
                 * В табличке DIC_DATA для поля LANG прописан паттерн на табличку интерфейса LangName.
                 * При получении родительского объекта для этого поля, получаем с него интерфейс DictionaryEntry,
                 * а на нём нет такого поля, как LANG (оно есть в интерфейсе DictionaryTerm).
                 * Поэтому убеждаемся, что работаем с DictionaryEntry, и для поиска используем DictionaryTerm.
                 */
                if (DictionaryEntry.class.getName().equals(iFaceName)) {
                    access = this.getAccess(DictionaryTerm.class.getName());
                    fieldsMap = PatternUtils.getInvertedFieldsMap(access);
                } else {
                    logger.log(Level.WARNING, "Can not get fieldName for column[{0}] on {1} interface",
                            new String[]{systemObject.getName(), iFaceName});
                    return null;
                }
            }

            return this.searchByAccess(access, fieldsMap.get(systemObject.getName()), id) ? iFaceName : null;
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Выполняет поиск переданного значения поля на переданном дата-акцессе
     *
     * @param access    дата-акцесс, по которому выполняет поиск
     * @param fieldName имя поискового поля
     * @param id        искомое значение
     *
     * @return <code>true</code> если найден хотя бы один объект, <code>false</code> иначе
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private boolean searchByAccess(IdentifiableAccess access, String fieldName, BigDecimal id)
            throws GenericSystemException {

        // Формируем поисковый критерий
        SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(2);
        search.add(profile.getSearchCriterion(fieldName, CriteriaRule.equals, id));

        return access.containsObjects(search);
    }

    /**
     * Проверяем, используется ли переданный объект в качестве параметра для
     * {@link ru.xr.ine.core.constants.ParametrizedConstant параметризованной константы}.
     *
     * @param identifiable проверяемый объект
     *
     * @return если объект используется хотя бы в одной константе, то возвращается её
     *         {@link ru.xr.ine.core.constants.ParametrizedConstant#getName() название},
     *         иначе вернётся <code>null</code>
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private String searchInParametrizedConstants(E identifiable) throws GenericSystemException {

        BigDecimal tableId = FieldAccess.getTableId(identifiable.getClass());

        ParametrizedConstantAccess<ParametrizedConstant> access =
                (ParametrizedConstantAccess) AccessFactory.getImplementation(ParametrizedConstant.class);

        SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(2);
        search.add(profile.getSearchCriterion(
                ParametrizedConstant.MASK, PatternUtils.PATTERN_TO_ID_COMPARATOR, CriteriaRule.equals, tableId));

        /** Мапы для группировки идентификаторов констант для одинаковых finalId в паттернах (для оптимизации поиска) */
        Map<BigDecimal, Collection<BigDecimal>> constantIds = new HashMap<>();
        Map<BigDecimal, BigDecimal> values = new HashMap<>();

        for (ParametrizedConstant constant : access.searchObjects(search)) {
            try {
                BigDecimal id = this.getSearchValueByPattern(identifiable, constant.getMask());
                if (id != null) {
                    /** Группируем константы по finalId*/
                    BigDecimal finalId = constant.getMask().getFinalId();
                    if (!constantIds.containsKey(finalId)) {
                        constantIds.put(finalId, new ArrayList<BigDecimal>());
                    }
                    constantIds.get(finalId).add(constant.getCoreId());
                    values.put(finalId, id);
                }
            } catch (ClassNotFoundException e) {
                logger.log(Level.INFO, e.getMessage(), e);
            }
        }

        /** Выполняем поиск по группам констант */
        for (BigDecimal columnId : constantIds.keySet()) {
            if (this.searchInConstantValue(constantIds.get(columnId), values.get(columnId))) {
                return constantIds.get(columnId).toString();
            }
        }

        return null;
    }

    /**
     * Поиск значения константы по значению параметра.
     *
     * @param constIds идентификатор константы
     * @param param    значение параметра
     *
     * @return <code>true</code> если такое значение найдено, <code>false</code> иначе
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private boolean searchInConstantValue(Collection<BigDecimal> constIds, BigDecimal param)
            throws GenericSystemException {

        ParametrizedConstantValueAccess access = (ParametrizedConstantValueAccess)
                AccessFactory.getImplementation(ParametrizedConstantValue.class);

        SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(2);
        search.add(profile.getSearchCriterion(ParametrizedConstantValue.CORE_ID, CriteriaRule.in, constIds.toArray()));
        search.add(profile.getSearchCriterion(ParametrizedConstantValue.PARAM, CriteriaRule.equals, param));

        return access.containsObjects(search);
    }

    /**
     * Проверяем, используется ли объект, в качестве значения
     * {@link ru.xr.ine.core.structure.CustomAttribute произвольного атрибута}
     *
     * @param identifiable проверяемый объект
     *
     * @return если объект используется хотя бы в одном атрибуте, то вернётся название этого атрибута,
     *         иначе вернётся <code>null</code>
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private String searchInCustomAttributes(E identifiable) throws GenericSystemException {

        BigDecimal tableId = FieldAccess.getTableId(identifiable.getClass());

        CustomAttributeAccess<CustomAttribute> access =
                (CustomAttributeAccess) AccessFactory.getImplementation(CustomAttribute.class);

        SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(2);
        search.add(profile.getSearchCriterion(
                CustomAttribute.PATTERN, PatternUtils.PATTERN_TO_ID_COMPARATOR, CriteriaRule.equals, tableId));

        /**
         * Мапы для группировки идентификаторов кастомных атрибутов для одинаковых finalId в паттернах
         * (для оптимизации поиска). Группируем так же по принадлежности атрибутов объектам (по systemObjectId)
         */
        Map<BigDecimal, Map<BigDecimal, Collection<BigDecimal>>> attributeIdsByColumnId = new HashMap<>();
        Map<BigDecimal, BigDecimal> values = new HashMap<>();

        for (CustomAttribute attribute : access.searchObjects(search)) {
            try {
                BigDecimal id = this.getSearchValueByPattern(identifiable, attribute.getPattern());
                if (id != null) {
                    BigDecimal finalId = attribute.getPattern().getFinalId();

                    /** Выполняем группировку по finalId */
                    if (!attributeIdsByColumnId.containsKey(finalId)) {
                        attributeIdsByColumnId.put(finalId, new HashMap<BigDecimal, Collection<BigDecimal>>());
                    }
                    Map<BigDecimal, Collection<BigDecimal>> attrIds = attributeIdsByColumnId.get(finalId);

                    /** Выполняем группировку по systemObjectId */
                    BigDecimal systemObjectId = attribute.getSystemObjectId();
                    if (!attrIds.containsKey(systemObjectId)) {
                        attrIds.put(systemObjectId, new ArrayList<BigDecimal>());
                    }
                    attrIds.get(systemObjectId).add(attribute.getCoreId());

                    values.put(finalId, id);
                }
            } catch (ClassNotFoundException e) {
                logger.log(Level.INFO, e.getMessage(), e);
            }
        }

        /** Выполняем поиски по группам идентификаторов атрибутов и их объектам */
        for (BigDecimal columnId : attributeIdsByColumnId.keySet()) {
            Map<BigDecimal, Collection<BigDecimal>> attrIds = attributeIdsByColumnId.get(columnId);
            for (BigDecimal systemObjectId : attrIds.keySet()) {
                if (this.searchInAttributeValue(attrIds.get(systemObjectId), systemObjectId, values.get(columnId))) {
                    return attrIds.get(systemObjectId).toString();
                }
            }
        }

        return null;
    }

    /**
     * Поиск по значению произвольного атрибута
     *
     * @param attributeIds   идентификаторы произвольных атрибутов
     * @param systemObjectId идентификатор системного объекта, которому принадлежат переданные атрибуты
     * @param value          значение произвольного атрибута
     *
     * @return <code>true</code> если значение найдено, <code>false</code> иначе
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private boolean searchInAttributeValue(Collection<BigDecimal> attributeIds,
                                           BigDecimal systemObjectId, BigDecimal value) throws GenericSystemException {

        CustomAttributeValueAccess access = (CustomAttributeValueAccess)
                AccessFactory.getImplementation(CustomAttributeValue.class);

        SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(2);
        SearchCriterion valueCriterion = profile.getSearchCriterion(
                CustomAttributeValue.VALUE, CustomAttributeValue.VALUE_COMPARATOR, CriteriaRule.equals, value);
        valueCriterion.addAdditionalParam(FieldAccess.getInterface(systemObjectId));
        search.add(valueCriterion);
        search.add(profile.getSearchCriterion(CustomAttributeValue.CORE_ID, CriteriaRule.in, attributeIds.toArray()));

        /*
        SearchCriterion idCriterion = profile.getSearchCriterion(
                CustomAttributeValue.CORE_ID, CriteriaRule.in, attributeIds.toArray());
        idCriterion.addAdditionalParam(FieldAccess.getInterface(systemObjectId));

        search.add(idCriterion);
        search.add(profile.getSearchCriterion(
                CustomAttributeValue.VALUE, CustomAttributeValue.VALUE_COMPARATOR, CriteriaRule.equals, value));
        */

        return access.containsObjects(search);
    }

    /**
     * Проверяем, связан ли переданный через {@link ru.xr.ine.core.links.LinkData линки} с другими объектами.
     *
     * @param identifiable проверяемый объект
     * @param left         признак, определяющий, с какой стороны линка искать
     *
     * @return если объект связан хотя бы с одним другим объектом, то вернётся
     *         {@link ru.xr.ine.core.links.Link#getCoreId() идентификатор линка},
     *         иначе вернётся <code>null</code>
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private String searchInLinks(E identifiable, boolean left) throws GenericSystemException {

        BigDecimal tableId = FieldAccess.getTableId(identifiable.getClass());

        LinkAccess<Link> access = (LinkAccess) AccessFactory.getImplementation(Link.class);

        SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(2);
        search.add(profile.getSearchCriterion(left ? Link.LEFT_TYPE : Link.RIGHT_TYPE,
                PatternUtils.PATTERN_TO_ID_COMPARATOR, CriteriaRule.equals, tableId));

        /** Мапы для группировки идентификаторов линков для одинаковых finalId в паттернах (для оптимизации поиска) */
        Map<BigDecimal, Collection<BigDecimal>> linkIds = new HashMap<>();
        Map<BigDecimal, BigDecimal> values = new HashMap<>();

        BigDecimal id;
        for (Link link : access.searchObjects(search)) {
            try {
                id = this.getSearchValueByPattern(identifiable, left ? link.getLeftType() : link.getRightType());
                if (id != null) {
                    /**
                     * Выполняем группировку идентификаторов по finalId. После группировки выполним поиск
                     * с использованием условия {@link CriteriaRule#in} на linkId. Иначе может получится значительная
                     * просадка по производительности в случае множества типов связей объекта с другими объектами.
                     * Например, {@link ru.xr.ine.core.structure.CustomType} требует выполнить 28 поисков
                     * (на момент написания коментария), когда нужно выполнить всего 1 поиск.
                     */
                    BigDecimal finalId = left ? link.getLeftType().getFinalId() : link.getRightType().getFinalId();
                    if (!linkIds.containsKey(finalId)) {
                        linkIds.put(finalId, new ArrayList<BigDecimal>());
                    }
                    linkIds.get(finalId).add(link.getCoreId());
                    values.put(finalId, id);
                }
            } catch (ClassNotFoundException e) {
                logger.log(Level.INFO, e.getMessage(), e);
            }
        }

        /** Выполняем поиски по группам линков */
        for (BigDecimal columnId : linkIds.keySet()) {
            if (this.searchInLinkData(linkIds.get(columnId), values.get(columnId), left)) {
                return linkIds.get(columnId).toString();
            }
        }

        return null;
    }


    /**
     * Поиск линка по значению связываемого объекта.
     *
     * @param linkIds коллекция идентификаторов описателей линка
     * @param value   значение объекта
     * @param left    признак, определяющий, с какой стороны линка искать
     *
     * @return <code>true</code> если такое значение найдено, <code>false</code> иначе
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешом и мета-информацией
     */
    private boolean searchInLinkData(Collection<BigDecimal> linkIds, BigDecimal value, boolean left)
            throws GenericSystemException {

        LinkDataAccess access = (LinkDataAccess) AccessFactory.getImplementation(LinkData.class);

        SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(2);
        search.add(profile.getSearchCriterion(left ? LinkData.LEFT_ID : LinkData.RIGHT_ID, CriteriaRule.equals, value));
        search.add(profile.getSearchCriterion(LinkData.LINK_ID, CriteriaRule.in, linkIds.toArray()));

        return access.containsObjects(search);
    }

    /**
     * Проверяем вхождение переданного объекта в паттерн и возвращаем значение из поля,
     * соответствующего {@link ru.xr.ine.core.structure.SystemObjectPattern#finalId}.<br>
     * Проверка вхождения осуществляется итерированием
     * {@link ru.xr.ine.core.structure.SystemObjectPattern#elements элементов паттерна}.
     * Для каждого элемента получается значение из переданного объекта из поля, соответствующего
     * {@link ru.xr.ine.core.structure.SystemObjectPattern.Element#sysObjId идентификатору поля}.
     * Полученное значение должно попадать в
     * {@link ru.xr.ine.core.structure.SystemObjectPattern.Element#entityIds коллекцию допустимых значений}.<br>
     * Если какое-либо значение не попадает в элемент, то возвращается <code>null</code>, что означает -
     * объект не попадает в паттерн. Иначе, возвращается значение из поля, соответствующего
     * {@link ru.xr.ine.core.structure.SystemObjectPattern#finalId}.
     *
     * @param identifiable проверяемый объект
     * @param pattern      паттерн, по которому проверяется объект
     *
     * @return либо поисковое значение, либо <code>null</code>, если объект не попадает в паттерн
     * @throws ClassNotFoundException если класс не найден (такой ошибки быть не должно при нормальной работе)
     * @throws ru.xr.ine.core.GenericSystemException
     *                                в случае ошибок при работе с кешом и мета-информацией
     */
    @SuppressWarnings({"SuspiciousMethodCalls"})
    private BigDecimal getSearchValueByPattern(E identifiable, SystemObjectPattern pattern)
            throws ClassNotFoundException, GenericSystemException {

        // Получаем дата-акцесс переданного объекта (хотя, подойдёт любой акцесс)
        AbstractIdentifiableAccess idAccess = (AbstractIdentifiableAccess)
                this.getAccess(IdentifiableFactory.getInterface(identifiable).getName());

        // Из паттерна, нужно получить имена столбцов, а по столбцам - получить значения из переданного объекта.
        Map<String, SystemObjectPattern.Element> elements = new HashMap<>();
        for (SystemObjectPattern.Element element : pattern.getElements()) {
            elements.put(FieldAccess.getObjectById(element.getSysObjId()).getName(), element);
        }

        Collection<String> columnNames = new ArrayList<>(elements.keySet());
        // Добавляем столбец finalId, его значение будет использоваться в качестве поискового условия
        columnNames.add(FieldAccess.getObjectById(pattern.getFinalId()).getName());
        Object[] values = idAccess.getFieldValuesByColumns(identifiable, columnNames);

        // Полученные значения проверяем на вхождение в элементы паттерна.
        int i = 0;
        for (String column : columnNames) {
            if (i == columnNames.size() - 1) {
                break;
            }
            // Если значение не входит в элемент паттерна, то и объект не попадает под условие паттерна
            if (!elements.get(column).getEntityIds().contains(values[i])) {
                return null;
            }
            i++;
        }

        // Получаем значение для поиска
        return (BigDecimal) values[i];
    }

    private IdentifiableAccess getAccess(String name) throws ClassNotFoundException, GenericSystemException {
        Class iFace = Class.forName(name);
        return (IdentifiableAccess) AccessFactory.getImplementation(iFace);
    }

}
