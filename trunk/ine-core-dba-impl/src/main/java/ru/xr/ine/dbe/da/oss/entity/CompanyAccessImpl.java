/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.entity;

import ru.xr.ine.core.*;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;
import ru.xr.ine.oss.entity.Company;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CompanyAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class CompanyAccessImpl<T extends Company> extends AbstractVersionableAccess<T> implements CompanyAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "COMPANY";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_PROP_TYPE = "property_type";

    /** Запрос на выборку всех значений */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_NAME +
            ", " + TABLE_PREF + COLUMN_TYPE +
            ", " + TABLE_PREF + COLUMN_PROP_TYPE +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.oss.entity.Company} */
    public static final String CACHE_REGION_COMPANIES = Company.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(Company.NAME, COLUMN_NAME);
        map.put(Company.TYPE, COLUMN_TYPE);
        map.put(Company.PROPERTY_TYPE, COLUMN_PROP_TYPE);
        setInstanceFieldsMapping(CompanyAccessImpl.class, map);

        registerVersionableHelpers(CompanyAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_COMPANIES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(Company.class);

        this.setVersionableFields(resultSet, value);

        value.setName(this.getString(resultSet, COLUMN_NAME));
        value.setType(this.getId(resultSet, COLUMN_TYPE));
        value.setPropertyType(this.getId(resultSet, COLUMN_PROP_TYPE));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_COMPANIES, ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_COMPANIES, ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_COMPANIES, ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_COMPANIES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_COMPANIES, ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Company.class;
    }

}
