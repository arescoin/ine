/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.structure.PatternUtils;
import ru.xr.ine.oss.inventory.item.ItemProfileProperty;
import ru.xr.ine.oss.inventory.item.ItemProfilePropertyImpl;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfilePropertyAccessImpl.java 59 2017-05-04 16:58:36Z xerror $"
 */
public class ItemProfilePropertyAccessImpl<T extends ItemProfileProperty>
        extends AbstractVersionableAccess<T> implements ItemProfilePropertyAccess<T> {


    /**
     * Таблица и префикс для обращения к ее столбцам
     */
    public static final String TABLE = "INV_PRFL_PROPS";
    public static final String TABLE_PREF = TABLE + DOT;

    /**
     * Перечисление колонок таблицы
     */
    public static final String COLUMN_PROFILE_N = "PROFILE_N";
    public static final String COLUMN_PROPERTY_DSC_N = "PRPRT_DSC_N";
    public static final String COLUMN_DEF_VAL = "DEF_VAL";
    public static final String COLUMN_PATTERN = "PATTERN";
    public static final String COLUMN_REQ = "IS_RQRD";
    public static final String COLUMN_EDT = "IS_EDIT";

    /**
     * Запрос на выборку всех значений
     */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_PROFILE_N +
            ", " + TABLE_PREF + COLUMN_PROPERTY_DSC_N +
            ", " + TABLE_PREF + COLUMN_DEF_VAL +
            ", " + TABLE_PREF + COLUMN_PATTERN +
            ", " + TABLE_PREF + COLUMN_REQ +
            ", " + TABLE_PREF + COLUMN_EDT +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /**
     * Регион кэша для хранения данных типа {@link ItemProfileProperty}
     */
    public static final String CACHE_REGION_ITEM_PROFILE_PROPS = ItemProfileProperty.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(ItemProfileProperty.ITEM_PROFILE_ID, COLUMN_PROFILE_N);
        map.put(ItemProfileProperty.PROPERTY_DESCRIPTOR_ID, COLUMN_PROPERTY_DSC_N);
        map.put(ItemProfileProperty.DEFAULT_VALUE, COLUMN_DEF_VAL);
        map.put(ItemProfileProperty.PATTERN, COLUMN_PATTERN);
        map.put(ItemProfileProperty.REQUIRED, COLUMN_REQ);
        map.put(ItemProfileProperty.EDITABLE, COLUMN_EDT);
        setInstanceFieldsMapping(ItemProfilePropertyAccessImpl.class, map);

        registerVersionableHelpers(ItemProfilePropertyAccessImpl.class, new LinkedHashSet<>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_ITEM_PROFILE_PROPS;
    }


    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(getSubjectClass());

        this.setVersionableFields(resultSet, value);

        value.setItemProfileId(this.getId(resultSet, COLUMN_PROFILE_N));
        value.setPropertyDescriptorId(this.getId(resultSet, COLUMN_PROPERTY_DSC_N));
        value.setDefaultValue(this.getString(resultSet, COLUMN_DEF_VAL));

        String pattern = this.getString(resultSet, COLUMN_PATTERN);
        if (pattern != null) {
            value.setPattern(PatternUtils.checkPattern(new SystemObjectPattern(pattern)));
        }

        value.setRequired(this.getBooleanNullable(resultSet, COLUMN_REQ));
        value.setEditable(this.getBooleanNullable(resultSet, COLUMN_EDT));

        return value;
    }

    @Override
    protected String getCommonSelect() {
        return ALL_DATA_SELECT;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(getMainRegionKey(), getCommonSelect());
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(getMainRegionKey(), getCommonSelect(), id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, getMainRegionKey(), getCommonSelect(), true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, getMainRegionKey(), getCommonSelect());
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ItemProfileProperty.class;
    }
}
