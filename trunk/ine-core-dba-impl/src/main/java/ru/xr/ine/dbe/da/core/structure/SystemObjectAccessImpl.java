/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.RuntimeCoreException;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.core.structure.SystemObjectType;
import ru.xr.ine.dbe.da.PoolManager;
import ru.xr.ine.dbe.da.config.PropertiesBasedDAConfig;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.CRUDHelper;
import ru.xr.ine.utils.cache.Cache;
import ru.xr.ine.utils.cache.Producer;
import ru.xr.ine.utils.config.ConfigurationManager;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Предоставляет методы для работы с системными объектами на стороне БД
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SystemObjectAccessImpl<T extends SystemObject>
        extends AbstractVersionableAccess<T> implements SystemObjectAccess<T> {

    /** Имя таблицы и префикс... */
    public static final String TABLE = "SYS_OBJ";
    public static final String TABLE_PREF = TABLE + ".";

    /** Перечисление колонок талтцы */
    public static final String COLUMN_TYP = "typ";
    public static final String COLUMN_OBJ_NAME = "obj_name";
    public static final String COLUMN_UP = "up";
    public static final String COLUMN_PATTERN = "pattern";

    /** Строка для полной выборки */
    public static final String SYS_OBJ_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_TYP +
            ", " + TABLE_PREF + COLUMN_OBJ_NAME +
            ", " + TABLE_PREF + COLUMN_UP +
            ", " + TABLE_PREF + COLUMN_PATTERN +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD +
            " AND " + TABLE_PREF + COLUMN_TD;

    private static final TableNameCaseConverter CONVERTER;

    static {
        if (((PropertiesBasedDAConfig)
                ConfigurationManager.getManager().getConfigFor(DA_CONFIG_NAME)).getUpperLower().equals("UPPER")) {
            CONVERTER = new UpperCaseConverte();
        } else {
            CONVERTER = new LowerCaseConverte();
        }

    }

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.structure.SystemObject} */
    public static final String CACHE_REGION_SYSTEM_OBJECTS = SystemObject.class.getName();

    private static volatile SysObjCacheContainer CONTAINER;


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(SystemObject.NAME, COLUMN_OBJ_NAME);
        map.put(SystemObject.TYPE, COLUMN_TYP);
        map.put(SystemObject.UP, COLUMN_UP);
        map.put(SystemObject.PATTERN, COLUMN_PATTERN);
        setInstanceFieldsMapping(SystemObjectAccessImpl.class, map);

        registerVersionableHelpers(SystemObjectAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getContainer().getObjects().values();
    }

    /**
     * Возвращает системный номер таблицы для хранения данных определенного интерфейса
     *
     * @param identifiableClass системный интерфейс
     *
     * @return идентификатор таблицы
     * @throws ru.xr.ine.core.GenericSystemException
     *          при возникновении ошибок в механизме доступа к мета
     */
    public BigDecimal getTableId(Class identifiableClass) throws GenericSystemException {
        return this.getContainer().getTableIdsByTableName().get(
                AccessFactory.getImplementation(identifiableClass).getTableName());
    }

    /**
     * Возвращает маппинг "идентификатор таблицы - имя класса"
     *
     * @return маппинг "идентификатор таблицы - имя класса"
     * @throws ru.xr.ine.core.GenericSystemException
     *          при возникновении ошибок в механизме доступа к мета
     */
    @Override
    public Map<BigDecimal, String> getInterfaces() throws GenericSystemException {
        return this.getContainer().getInterfacesByTable();
    }

    /**
     * Возвращает идентификатор системного объекта, описывающего сиквенс в БД,
     * используемого для генерации иденитфикатора
     *
     * @param identifiableClass класс для которого ищется сиквенс
     *
     * @return имя сиквенса
     * @throws ru.xr.ine.core.CoreException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public BigDecimal getSequenceIdByClass(Class identifiableClass) throws CoreException {

        BigDecimal tableId = this.getTableId(identifiableClass);
        for (BigDecimal seqId : this.getContainer().getIdsByType().get(SystemObjectType.sequence.getTypeCode())) {
            if (tableId.equals(this.getObjectById(seqId).getUp())) {
                return seqId;
            }
        }

        return null;
    }

    /**
     * Возвращает системный номер столбца таблицы
     *
     * @param tableName  название таблицы
     * @param columnName название поля
     *
     * @return идентификатор столбца
     * @throws ru.xr.ine.core.GenericSystemException
     *          при возникновении ошибок в механизме доступа к мета
     */
    public BigDecimal getColumnId(String tableName, String columnName) throws GenericSystemException {

        SysObjCacheContainer<T> container = this.getContainer();

        return container.getColumnsByTable().get(container.getTableIdsByTableName().get(tableName)).get(columnName);
    }

    /**
     * Возвращает все дочерние объекты для переданного идентификатора системного объекта.
     *
     * @param parentId идентификатор системного объекта
     *
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.CoreException
     *          при возникновении ошибок в механизме доступа к мета
     */
    public Collection<T> getChildren(BigDecimal parentId) throws CoreException {

        Collection<T> result = new HashSet<T>();
        SysObjCacheContainer<T> container = this.getContainer();
        for (BigDecimal id : container.getChildrenByParent().get(parentId)) {
            result.add(container.getObjects().get(id));
        }

        return result;
    }

    /**
     * Возвращает все объекты, в которых содержатся паттерны переданного системного типа
     *
     * @param tableId системный тип объектов
     *
     * @return коллекция объектов с паттернами
     * @throws GenericSystemException при работе с кешом
     */
    public Collection<T> getPatternObjects(BigDecimal tableId) throws GenericSystemException {
        return this.getContainer().getPatternsByType().get(tableId);
    }

    /**
     * Возвращает все объекты, в которых содержатся паттерны переданного системного типа
     *
     * @param tableId системный тип объектов
     *
     * @return коллекция объектов с паттернами
     * @throws GenericSystemException при работе с кешом
     */
    public Collection<T> getPatternsForType(BigDecimal tableId) throws GenericSystemException {
        return this.getContainer().getPatternsForType().get(tableId);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {

        this.checkIdParam(1, null, id);

        return this.getContainer().getObjects().get(id[0]);
    }

    @Override
    public String getTableName() {
        return SystemObjectAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SYSTEM_OBJECTS;
    }

    @Override
    public T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T systemObject = (T) IdentifiableFactory.getImplementation(SystemObject.class);

        this.setVersionableFields(resultSet, systemObject);

        systemObject.setType(this.getId(resultSet, COLUMN_TYP));
        systemObject.setName(resultSet.getString(COLUMN_OBJ_NAME));
        systemObject.setUp(this.getBigDecimal(resultSet, COLUMN_UP));

        String pattern = this.getString(resultSet, COLUMN_PATTERN);
        if (pattern != null) {
            systemObject.setPattern(new SystemObjectPattern(pattern));
        }

        return systemObject;
    }

    @SuppressWarnings({"unchecked"})
    private SysObjCacheContainer<T> getContainer() throws GenericSystemException {

        if (CONTAINER == null) {
            synchronized (CACHE_REGION_SYSTEM_OBJECTS) {
                if (CONTAINER == null) {
                    try {
                        CONTAINER = (SysObjCacheContainer) Cache.get(
                                CACHE_REGION_SYSTEM_OBJECTS, CACHE_REGION_SYSTEM_OBJECTS, new SystemObjectsProducer());
                    } catch (CoreException e) {
                        throw GenericSystemException.toGeneric(e);
                    }
                }
            }
        }

        return CONTAINER;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return SystemObject.class;
    }


    public class SystemObjectsProducer implements Producer {

        private Logger logger = Logger.getLogger(SystemObjectsProducer.class.getName());

        @Override
        public Object get(Object key) throws CoreException {

            // Создаем контейнеры для складирования структуры
            SysObjCacheContainer<T> container = new SysObjCacheContainer<T>();

            /*
             * Простая мапа со всеми системными объектами, вида 'Идентификатор объекта' -> 'Объект'.
             * Нужна для быстрого доступа к конкретному объекту по его идентификатору.
             */
            Map<BigDecimal, T> objects = container.getObjects();
            /*
             * Агрегация идентификаторов системных объектов по типу системных объектов. Мапа вида:
             * 'Идентификатор типа из поля T.getType()' -> 'Коллеция идентификаторов объектов данного типа'.
             * Нужна для получения объектов одного типа.
             */
            Map<BigDecimal, Collection<BigDecimal>> idsByType = container.getIdsByType();

            // Тут выполняем запрос к хранилищу и разбор результатов
            for (T systemObject : getObjects(false, SYS_OBJ_SELECT)) {

                // кладём идентификатор и объект в общую мапу объектов
                BigDecimal id = systemObject.getCoreId();
                objects.put(id, systemObject);

                // кладём идентификатор объекта в мапу, соответствующую его типу
                this.addIdToType(idsByType, systemObject);

                // выполняем различные группировки объекта в зависимости от его типа
                this.groupSingleObject(container, systemObject);
            }

            // Проверяем паттерны в системных объектах и группируем их по типам системных объектов
            this.groupPatterns(container);

            // Выставляем признак обязательности заполнения данными для всех столбцов
            this.setRequired(container);

            return container;
        }

        /* Добавляем идентификатор системного объекта в список объектов его типа */
        private void addIdToType(Map<BigDecimal, Collection<BigDecimal>> idsByType, T systemObject) {

            BigDecimal type = systemObject.getType();
            if (!idsByType.containsKey(type)) {
                idsByType.put(type, new HashSet<BigDecimal>());
            }
            idsByType.get(type).add(systemObject.getCoreId());
        }

        /* Помещаем объект в различные мапы в зависимости от его типа */
        private void groupSingleObject(SysObjCacheContainer<T> container, T systemObject) {

            /*
             * Маппинг дочерних объектов на родителя. По идентификатору родительского системного объекта (таблице)
             * можно получить все его подчинённые объекты (столбцы, сиквенсы, интерфейс java-объекта).
             */
            Map<BigDecimal, Collection<BigDecimal>> childrenByParent = container.getChildrenByParent();
            /*
             * Агрегация маппингов "название столбца - идентификатор столбца" по идентификатору таблицы. Нужно, чтобы
             * по идентификатору таблицы получить список её столбцов, а потом по имени столбца его идентификатор.
             */
            Map<BigDecimal, Map<String, BigDecimal>> columnsByTableName = container.getColumnsByTable();

            BigDecimal id = systemObject.getCoreId();
            BigDecimal parent = systemObject.getUp();

            /* связываем парента с его дочерними элементами */
            if (parent != null) {
                if (!childrenByParent.containsKey(parent)) {
                    childrenByParent.put(parent, new HashSet<BigDecimal>(10));
                }
                childrenByParent.get(parent).add(id);
            }

            /* Выполняем остальные группировки объекта в зависимости от его типа */
            BigDecimal type = systemObject.getType();
            if (type.equals(SystemObjectType.table.getTypeCode())) {
                /* связываем имя таблицы и её системный номер */
                container.getTableIdsByTableName().put(systemObject.getName(), id);
            } else if (type.equals(SystemObjectType.column.getTypeCode())) {
                /* кладём идентификатор столбца в его таблицу */
                if (!columnsByTableName.containsKey(parent)) {
                    columnsByTableName.put(parent, new HashMap<String, BigDecimal>(10));
                }
                columnsByTableName.get(parent).put(systemObject.getName(), id);
            } else if (type.equals(SystemObjectType.interace.getTypeCode())) {
                /* связываем имя класса интерфейса с идентификатором таблицы */
                container.getInterfacesByTable().put(parent, systemObject.getName());
            }
        }

        /* Группируем объекты, содержащие паттерн, по идентификторам таблиц, описываемых паттернами */
        private void groupPatterns(SysObjCacheContainer<T> container) throws CoreException {

            Map<BigDecimal, T> objects = container.getObjects();
            /*
             * Маппинг объектов с паттернам по типу системных объектов, вида
             * 'Идентификатор таблицы' -> 'Системные объекты, содержащие паттерны, использующие данную таблицу'.
             */
            Map<BigDecimal, Collection<T>> patternsByType = container.getPatternsByType();
            Map<BigDecimal, Collection<T>> patternsForType = container.getPatternsForType();

            /* Итерируем все системные объекты и смотрим, у кого есть паттерн */
            for (T object : objects.values()) {
                if (object.getPattern() != null) {
                    PatternUtils.checkPattern(objects, object.getPattern());

                    /* Если паттерн проверен, то берём таблицу, которую он описывает, и добавляем объект в её список */
                    BigDecimal tableId = object.getPattern().getTableId();

                    if (!patternsByType.containsKey(tableId)) {
                        patternsByType.put(tableId, new ArrayList<T>(10));
                    }
                    patternsByType.get(tableId).add(object);

                    /* Потом вычленяем только патерны на конкретном типе */
                    if (!patternsForType.containsKey(object.getUp())) {
                        patternsForType.put(object.getUp(), new ArrayList<T>(10));
                    }
                    patternsForType.get(object.getUp()).add(object);
                }
            }
        }

        /* Выставляем признак обязательности заполения данными для всех столбцов */
        private void setRequired(SysObjCacheContainer<T> container) throws GenericSystemException {

            Map<BigDecimal, T> objects = container.getObjects();
            /*
             * Маппинг дочерних объектов на родителя. По идентификатору родительского системного объекта (таблице)
             * можно получить все его подчинённые объекты (столбцы, сиквенсы, интерфейс java-объекта).
             */
            Map<BigDecimal, Collection<BigDecimal>> childrenByParent = container.getChildrenByParent();

            T object;
            BigDecimal column = SystemObjectType.column.getTypeCode();
            Map<String, Boolean> columnsNullability;
            /* Итерируем все таблицы */
            for (BigDecimal table : container.getIdsByType().get(SystemObjectType.table.getTypeCode())) {
                /* Получаем список столбцов таблицы с признаком допустимости null-значений */
                columnsNullability = this.getColumnsNullability(objects.get(table).getName());
                for (BigDecimal child : childrenByParent.get(table)) {
                    object = objects.get(child);
                    /* Если null-значения недопустимы, то выставляем обязательность заполения данными в true */
                    if (object.getType().equals(column)) {
                        object.setRequired(!columnsNullability.get(object.getName()));
                    }
                }
            }
        }

        /* Возвращаем список столбцов таблицы с признаком возможности заносить в столбец null-значения */
        private Map<String, Boolean> getColumnsNullability(String tableName) throws GenericSystemException {
            Map<String, Boolean> columnsNullability = new HashMap<String, Boolean>();

            try {
                Connection connection = PoolManager.getConnection();
                try {
                    DatabaseMetaData dbMetaData = connection.getMetaData();

                    ResultSet resultSet = dbMetaData.getColumns(null, null, CONVERTER.convert(tableName), "%");
                    while (resultSet.next()) {
                        columnsNullability.put(resultSet.getString("COLUMN_NAME"),
                                resultSet.getLong("NULLABLE") == 1);
                    }

                    resultSet.close();
                } catch (SQLException e) {
                    if (logger.isLoggable(Level.SEVERE)) {
                        logger.log(Level.SEVERE, "SQLException: ", e);
                    }
                    toRollback(connection);
                    throw new GenericSystemException(e.getMessage(), e);
                } finally {
                    connection.close();
                }
            } catch (SQLException e) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.log(Level.SEVERE, "SQLException: ", e);
                }
                throw new GenericSystemException(e.getMessage(), e);
            }

            return columnsNullability;
        }
    }

    public interface TableNameCaseConverter {
        String convert(String tablename);
    }

    public static final class LowerCaseConverte implements TableNameCaseConverter {
        @Override
        public String convert(String tablename) {
            return tablename.toLowerCase();
        }
    }

    public static final class UpperCaseConverte implements TableNameCaseConverter {
        @Override
        public String convert(String tablename) {
            return tablename;
        }
    }

    @Override
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return super.searchObjects2(searchCriteria, null);
    }
}
