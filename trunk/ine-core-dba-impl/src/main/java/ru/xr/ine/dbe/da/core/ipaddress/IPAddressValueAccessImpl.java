/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.ipaddress;

import ru.xr.ine.core.*;
import ru.xr.ine.core.ipaddress.IPAddress;
import ru.xr.ine.core.ipaddress.IPAddressValue;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация интерфейса для доступа к данным {@link ru.xr.ine.core.ipaddress.IPAddressValue}-объектов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class IPAddressValueAccessImpl<T extends IPAddressValue>
        extends AbstractVersionableAccess<T> implements IPAddressValueAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "IP_ADDRESS";
    public static final String TABLE_PREF = TABLE + DOT;

    public static final String COLUMN_IP_ADDRESS = "ip_addr";

    /** Запрос на выборку всех значений */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_IP_ADDRESS +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;


    /** Регион кэша для хранения данных типа {@link IPAddressValue} */
    public static final String CACHE_REGION_IP_ADDRESS_VALUES = IPAddressValue.class.getName();

    static {
        setInstanceFieldsMapping(IPAddressValueAccessImpl.class, new HashMap<String, String>());

        registerVersionableHelpers(IPAddressValueAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {

        final Map<String, String> map = new HashMap<String, String>();
        map.put(IPAddressValue.IP_ADDRESS, COLUMN_IP_ADDRESS);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    @Override
    public String getTableName() {
        return IPAddressValueAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return IPAddressValueAccessImpl.CACHE_REGION_IP_ADDRESS_VALUES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(IPAddressValue.class);

        this.setVersionableFields(resultSet, value);
        value.setIpAddress(new IPAddress(resultSet.getBytes(COLUMN_IP_ADDRESS)));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(this.getMainRegionKey(), ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(this.getMainRegionKey(), ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, this.getMainRegionKey(), ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, this.getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, this.getMainRegionKey(), ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return IPAddressValue.class;
    }

}
