/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.history;

import ru.xr.ine.core.*;
import ru.xr.ine.core.history.VersionableHistory;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Предоставляет методы для работы с историей изменений версионных объектов на стороне БД
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class VersionableHistoryAccessImpl<T extends VersionableHistory>
        extends IdentifiableHistoryAccessImpl<T> implements VersionableHistoryAccess<T> {

    public static final String COLUMN_REASON = "reason";

    public static final String SELECT_ALL_HISTORY = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ENT_ID +
            ", " + TABLE_PREF + COLUMN_USER_ID +
            ", " + TABLE_PREF + COLUMN_ACTION_DATE +
            ", " + TABLE_PREF + COLUMN_ACTION_TYP +
            ", " + TABLE_PREF + COLUMN_REASON +
            ", " + TABLE_PREF + AbstractVersionableAccess.COLUMN_FD +
            ", " + TABLE_PREF + AbstractVersionableAccess.COLUMN_TD +
            " FROM " + TABLE;

    public static final String SELECT_HISTORY_BY_IDS = SELECT_ALL_HISTORY +
            " WHERE " + TABLE_PREF + COLUMN_ID + " = ? AND " + TABLE_PREF + COLUMN_ENT_ID + " = ?";

    public static final String SELECT_HISTORY_LIKE = SELECT_ALL_HISTORY +
            " WHERE " + TABLE_PREF + COLUMN_ID + " = ? AND " + TABLE_PREF + COLUMN_ENT_ID + " like ?";

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(VersionableHistory.FD, AbstractVersionableAccess.COLUMN_FD);
        map.put(VersionableHistory.TD, AbstractVersionableAccess.COLUMN_TD);
        setInstanceFieldsMapping(VersionableHistoryAccessImpl.class, map);

        registerVersionableHelpers(VersionableHistoryAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    protected static void registerVersionableHelpers(
            Class<? extends IdentifiableHistoryAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<CRUDHelper>();
        helpersAll.addAll(helpers);

        IdentifiableHistoryAccessImpl.registerIdentifiableHistoryHelpers(access, helpersAll);
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getObjects(false, SELECT_ALL_HISTORY);
    }

    public Collection<T> getActionsHistoryByIds(BigDecimal sysObjId, SyntheticId entityId)
            throws CoreException, RuntimeCoreException {

        String selectedSQL;
        if (entityId.isSearchId()) {
            selectedSQL = SELECT_HISTORY_LIKE;
        } else {
            selectedSQL = SELECT_HISTORY_BY_IDS;
        }

        return this.getObjects(false, selectedSQL, sysObjId, entityId.toString());
    }

    @Override
    protected void setBaseFields(ResultSet resultSet, T obj) throws SQLException, CoreException, RuntimeCoreException {
        super.setBaseFields(resultSet, obj);

        Date fd = this.getDate(resultSet, AbstractVersionableAccess.COLUMN_FD);
        Date td = this.getDate(resultSet, AbstractVersionableAccess.COLUMN_TD);

        try {
            obj.setFd(fd);
            obj.setTd(td);
        } catch (IneIllegalArgumentException e) {
            throw new IneCorruptedVersionableDateException("Corrupted Date: fd ["
                    + fd + "], td[" + td + "], rowId [" + resultSet.getString(1) + "].");
        }
    }

    @Override
    public T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"})
        T result = (T) IdentifiableFactory.getImplementation(VersionableHistory.class);
        this.setBaseFields(resultSet, result);

        return result;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return VersionableHistory.class;
    }

}
