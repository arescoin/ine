/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.userpermits;

import ru.xr.ine.core.*;
import ru.xr.ine.core.userpermits.RolePermit;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class RolePermitAccessImpl<T extends RolePermit>
        extends AbstractVersionableAccess<T> implements RolePermitAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "ROLE_PERMITS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ROLE_N = "role_n";
    public static final String COLUMN_PERMIT_N = "permit_n";
    public static final String COLUMN_CRUD_MASK = "crud_mask";
    public static final String COLUMN_PERMIT_VAL = "permit_val";

    /** Запрос на выборку всех доступов, содержащихся в системных ролях */
    public static final String ALL_ROLE_PERMITS_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + " , " + TABLE_PREF + COLUMN_ROLE_N +
            ", " + TABLE_PREF + COLUMN_PERMIT_N +
            ", " + TABLE_PREF + COLUMN_CRUD_MASK +
            ", " + TABLE_PREF + COLUMN_PERMIT_VAL +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.userpermits.RolePermit} */
    public static final String CACHE_REGION_ROLE_PERMITS = RolePermit.class.getName();

    private static final Map<String, BigDecimal> ID_COLUMNS_MAP = new HashMap<String, BigDecimal>(3);

    /** Key - roleId, Key - permitId */
    private static final Map<BigDecimal, HashMap<BigDecimal, RolePermit>> ROLE_PERMIT_MAP =
            new HashMap<BigDecimal, HashMap<BigDecimal, RolePermit>>();

    private static final String DELIMETER = ",";


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(RolePermit.CORE_ID, COLUMN_ROLE_N);
        map.put(RolePermit.CRUD_MASK, COLUMN_CRUD_MASK);
        map.put(RolePermit.PERMIT_ID, COLUMN_PERMIT_N);
        map.put(RolePermit.PERMIT_VALUES, COLUMN_PERMIT_VAL);
        setInstanceFieldsMapping(RolePermitAccessImpl.class, map);

        setIdColumns(RolePermitAccessImpl.class, new String[]{COLUMN_ROLE_N, COLUMN_PERMIT_N});

        registerVersionableHelpers(RolePermitAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_ROLE_PERMITS;
    }

    @Override
    protected void checkIdParam(BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(2, "Requiered: id[0] - rolePermit.id, id[1] - rolePermit.permitId", id);
    }

    @Override
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(id);
        identifiable.setCoreId(id[0]);
        identifiable.setPermitId(id[1]);
    }

    @Override
    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {
        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(getColumnId(COLUMN_ROLE_N), identifiable.getCoreId());
        syntheticId.addIdEntry(getColumnId(COLUMN_PERMIT_N), identifiable.getPermitId());

        return syntheticId;
    }

    private static BigDecimal getColumnId(String columnName) throws GenericSystemException {
        if (!ID_COLUMNS_MAP.containsKey(columnName)) {
            ID_COLUMNS_MAP.put(columnName, FieldAccess.getColumnId(TABLE, columnName));
        }

        return ID_COLUMNS_MAP.get(columnName);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(RolePermit.class);

        this.setVersionableFields(resultSet, result, COLUMN_ROLE_N);
        result.setPermitId(this.getId(resultSet, COLUMN_PERMIT_N));
        result.setPermitValues(this.parseArrayString(this.getString(resultSet, COLUMN_PERMIT_VAL)));
        result.setCrudMask(this.getBigDecimal(resultSet, COLUMN_CRUD_MASK).intValue());

        return result;
    }

    private BigDecimal[] parseArrayString(String arrayString) {

        if (arrayString == null || arrayString.trim().isEmpty()) {
            return null;
        }

        String[] strings = arrayString.split(DELIMETER);
        BigDecimal[] result = new BigDecimal[strings.length];

        for (int i = 0; i < strings.length; i++) {
            try {
                result[i] = new BigDecimal(strings[i]);
            } catch (Exception e) {
                throw new IneIllegalArgumentException("Failed to parse part of input string: '" + arrayString + "'", e);
            }
        }

        return result;
    }

    @Override
    protected void setNonPrimitveType(PreparedStatement preparedStatement, int i, Object param) throws SQLException {

        if (param instanceof BigDecimal[]) {
            BigDecimal[] decimals = (BigDecimal[]) param;
            int length = decimals.length;
            String result = length > 0 ? "" : null;
            for (int j = 0; j < length; j++) {
                result += decimals[j];
                if (j < length - 1) {
                    result += DELIMETER;
                }
            }
            preparedStatement.setString(i, result);
        } else {
            super.setNonPrimitveType(preparedStatement, i, param);
        }
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {

        Collection<T> rolePermits = getAllObjects(CACHE_REGION_ROLE_PERMITS, ALL_ROLE_PERMITS_SELECT);
        for (T rolePermit : rolePermits) {
            processRolePermit(rolePermit);
        }

        return rolePermits;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {

        T rolePermit = null;
        Map<BigDecimal, RolePermit> map = ROLE_PERMIT_MAP.get(id[0]);
        if (map != null) {
            rolePermit = (T) map.get(id[1]);
        }

        if (rolePermit == null) {
            rolePermit = getObjectById(CACHE_REGION_ROLE_PERMITS, ALL_ROLE_PERMITS_SELECT, id);
            if (rolePermit != null) {
                processRolePermit(rolePermit);
            }
        }

        return rolePermit;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        T rolePermit = this.createObject(identifiable, CACHE_REGION_ROLE_PERMITS, ALL_ROLE_PERMITS_SELECT, false);
        this.processRolePermit(rolePermit);

        return rolePermit;
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        T reolePermit = this.updateObject(identifiable, newIdentifiable, CACHE_REGION_ROLE_PERMITS);
        this.processRolePermit(reolePermit);

        return reolePermit;
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        this.deleteObject(identifiable, CACHE_REGION_ROLE_PERMITS, ALL_ROLE_PERMITS_SELECT);

        BigDecimal roleId = identifiable.getCoreId();
        if (ROLE_PERMIT_MAP.containsKey(roleId)) {
            ROLE_PERMIT_MAP.get(roleId).put(identifiable.getPermitId(), null);
        }
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return RolePermit.class;
    }

    private void processRolePermit(T rolePermit) {

        BigDecimal roleId = rolePermit.getCoreId();
        if (!ROLE_PERMIT_MAP.containsKey(roleId)) {
            ROLE_PERMIT_MAP.put(roleId, new HashMap<BigDecimal, RolePermit>());
        }

        ROLE_PERMIT_MAP.get(roleId).put(rolePermit.getPermitId(), rolePermit);
    }

    @SuppressWarnings({"unchecked", "RedundantCast"})
    public Map<BigDecimal, T> getPremitsByRole(BigDecimal roleId) throws CoreException {

        if (ROLE_PERMIT_MAP.isEmpty()) {
            getAllObjects();
        }

        return ROLE_PERMIT_MAP.containsKey(roleId)
                ? (Map<BigDecimal, T>) ROLE_PERMIT_MAP.get(roleId)
                : new HashMap<BigDecimal, T>();
    }

    public void addHelper(CRUDHelper<T> helper) {

        LinkedHashSet<CRUDHelper> helpers = new LinkedHashSet<CRUDHelper>();
        helpers.add(helper);

        registerVersionableHelpers(RolePermitAccessImpl.class, helpers);
    }

}
