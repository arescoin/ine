/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.dic;

import ru.xr.ine.core.*;
import ru.xr.ine.core.dic.Dictionary;
import ru.xr.ine.core.dic.DictionaryEntry;
import ru.xr.ine.core.dic.DictionaryTerm;
import ru.xr.ine.dbe.da.core.*;
import ru.xr.ine.dbe.da.core.structure.FieldAccess;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class DictionaryEntryAccessImpl<T extends DictionaryEntry>
        extends AbstractVersionableAccess<T> implements DictionaryEntryAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "DIC_DATA";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_DIC_N = "dic_n";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_UP = "up";

    /** Запрос на выборку всех словарных статей */
    public static final String ALL_DICTIONARY_ENTRIES_SELECT = "SELECT DISTINCT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_DIC_N +
            ", " + TABLE_PREF + COLUMN_CODE +
            ", " + TABLE_PREF + COLUMN_UP +
            ", NULL AS " + COLUMN_DSC +
            ", INE_CORE_SYS.GET_SYSTEM_FROMDATE" + PRC_END + " AS " + COLUMN_FD +
            ", INE_CORE_SYS.GET_SYSTEM_TODATE" + PRC_END + " AS " + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.dic.DictionaryEntry} */
    public static final String CACHE_REGION_DICTIONARY_ENTRIES = DictionaryEntry.class.getName();

    /** Кешируем локально идентификаторы столбцов, чтоб не лазать лишний раз по сети в кеш */
    private static final Map<String, BigDecimal> ID_COLUMNS_MAP = new HashMap<String, BigDecimal>(2);

    static {
        final Map<String, String> map = new HashMap<String, String>();
        setInstanceFieldsMapping(DictionaryEntryAccessImpl.class, map);

        setIdColumns(DictionaryEntryAccessImpl.class, new String[]{COLUMN_DIC_N, COLUMN_CODE});

        Set<CRUDHelper> helpers = new LinkedHashSet<CRUDHelper>();
        helpers.add(new DictionaryEntryHelper());
        registerDictionaryEntryHelpers(DictionaryEntryAccessImpl.class, helpers);
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {

        Map<String, String> map = new HashMap<String, String>();
        map.put(DictionaryEntry.CORE_DSC, null);
        map.put(DictionaryEntry.CORE_ID, COLUMN_CODE);
        map.put(DictionaryEntry.DIC_ID, COLUMN_DIC_N);
        map.put(DictionaryEntry.UP_TERM, COLUMN_UP);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerDictionaryEntryHelpers(Class<? extends DictionaryEntryAccessImpl> access,
                                                         Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<CRUDHelper>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {
        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(getColumnId(COLUMN_DIC_N), identifiable.getDictionaryId());
        syntheticId.addIdEntry(getColumnId(COLUMN_CODE), identifiable.getCoreId());

        return syntheticId;
    }

    private static BigDecimal getColumnId(String columnName) throws GenericSystemException {
        if (!ID_COLUMNS_MAP.containsKey(columnName)) {
            ID_COLUMNS_MAP.put(columnName, FieldAccess.getColumnId(TABLE, columnName));
        }

        return ID_COLUMNS_MAP.get(columnName);
    }

    @Override
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(id);
        identifiable.setDictionaryId(id[0]);
        identifiable.setCoreId(id[1]);
    }

    @Override
    protected void checkIdParam(BigDecimal... id) throws IneIllegalArgumentException {
        super.checkIdParam(2, "id[0] - entry.dictionaryId, id[1] - entry.id", id);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        return fillObject(resultSet, (T) IdentifiableFactory.getImplementation(DictionaryEntry.class));
    }

    protected T fillObject(ResultSet resultSet, T object) throws SQLException, IneCorruptedStateException {

        this.setVersionableFields(resultSet, object, COLUMN_CODE);

        object.setDictionaryId(this.getId(resultSet, COLUMN_DIC_N));
        object.setParentTermId(this.getBigDecimal(resultSet, COLUMN_UP));

        return object;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_DICTIONARY_ENTRIES, ALL_DICTIONARY_ENTRIES_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_DICTIONARY_ENTRIES, ALL_DICTIONARY_ENTRIES_SELECT, id);
    }

    @Override
    @CRUDIgnore
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        throw new UnsupportedOperationException("Operation is prohibited for DictionaryEntry object. " +
                "Use appropriate operation for DictionaryTerm object.");
    }

    @Override
    @CRUDIgnore
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        throw new UnsupportedOperationException("Operation is prohibited for DictionaryEntry object. " +
                "Use appropriate operation for DictionaryTerm object.");
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        @SuppressWarnings({"unchecked"}) DictionaryTermAccess<DictionaryTerm> access =
                (DictionaryTermAccess<DictionaryTerm>) AccessFactory.getImplementation(DictionaryTerm.class);
        for (DictionaryTerm term :
                access.getAllTermsForDictionaryAndEntry(identifiable.getDictionaryId(), identifiable.getCoreId())) {
            access.deleteObject(term);
        }
    }

    public Collection<T> getAllEntriesForDictionary(BigDecimal dictionaryId) throws GenericSystemException {
        SearchCriteriaProfile profile = getSearchCriteriaProfile();
        Set<SearchCriterion> searchCriteria = new HashSet<SearchCriterion>();
        searchCriteria.add(profile.getSearchCriterion(DictionaryEntry.DIC_ID, CriteriaRule.equals, dictionaryId));

        return searchObjects(searchCriteria);
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_DICTIONARY_ENTRIES;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return DictionaryEntry.class;
    }

    public static final class DictionaryEntryHelper<E extends DictionaryEntry> extends AbstractCRUDHelper<E> {

        @SuppressWarnings({"unchecked"})
        @Override
        public void beforeDelete(E identifiable) throws GenericSystemException {

            DictionaryAccess<Dictionary> dictionaryAccess =
                    (DictionaryAccess<Dictionary>) AccessFactory.getImplementation(Dictionary.class);

            Set<SearchCriterion> search = new LinkedHashSet<SearchCriterion>(2);
            search.add(dictionaryAccess.getSearchCriteriaProfile().getSearchCriterion(
                    Dictionary.PARENT, CriteriaRule.equals, identifiable.getDictionaryId()));
            Collection<Dictionary> dictionaries = dictionaryAccess.searchObjects(search);

            if (!dictionaries.isEmpty()) {

                Collection<BigDecimal> ids = new ArrayList<BigDecimal>(dictionaries.size());
                for (Dictionary dictionary : dictionaries) {
                    ids.add(dictionary.getCoreId());
                }

                DictionaryEntryAccess<E> entryAccess =
                        (DictionaryEntryAccess<E>) AccessFactory.getImplementation(DictionaryEntry.class);

                search = new LinkedHashSet<SearchCriterion>(3);
                SearchCriteriaProfile profile = entryAccess.getSearchCriteriaProfile();
                search.add(profile.getSearchCriterion(E.UP_TERM, CriteriaRule.equals, identifiable.getParentTermId()));
                search.add(profile.getSearchCriterion(E.DIC_ID, CriteriaRule.in, ids.toArray()));

                if (entryAccess.containsObjects(search)) {
                    throw new IneIllegalArgumentException("Can't delete object [" + identifiable +
                            "], cause it is used in dictionaries [" + ids + "]");
                }
            } // if
        } // beforeDelete

    } // DictionaryEntryHelper

}
