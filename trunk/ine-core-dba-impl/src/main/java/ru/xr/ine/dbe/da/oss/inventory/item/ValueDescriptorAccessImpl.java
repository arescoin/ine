/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.oss.inventory.item.ValueDescriptor;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ValueDescriptorAccessImpl.java 57 2017-05-03 23:25:32Z xerror $"
 */
public class ValueDescriptorAccessImpl <T extends ValueDescriptor>
        extends AbstractVersionableAccess<T> implements ValueDescriptorAccess<T> {


    /**
     * Таблица и префикс для обращения к ее столбцам
     */
    public static final String TABLE = "INV_VALUE_DSC";
    public static final String TABLE_PREF = TABLE + DOT;

    /**
     * Перечисление колонок таблицы
     */
    public static final String COLUMN_NAME = "NAME";
    public static final String COLUMN_TYPE = "TYP";

    /**
     * Запрос на выборку всех значений
     */
    public static final String ALL_DATA = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_NAME +
            ", " + TABLE_PREF + COLUMN_TYPE +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /**
     * Регион кэша для хранения данных типа {@link ValueDescriptor}
     */
    public static final String CACHE_REGION = ValueDescriptor.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(ValueDescriptor.NAME, COLUMN_NAME);
        map.put(ValueDescriptor.TYPE, COLUMN_TYPE);
        setInstanceFieldsMapping(ValueDescriptorAccessImpl.class, map);

        registerVersionableHelpers(ValueDescriptorAccessImpl.class, new LinkedHashSet<>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(getSubjectClass());

        this.setVersionableFields(resultSet, value);

        value.setName(this.getString(resultSet, COLUMN_NAME));
        value.setType(DataType.getType(this.getId(resultSet, COLUMN_TYPE).intValue() - 1));

        return value;
    }


    @Override
    protected String getCommonSelect() {
        return ALL_DATA;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(getMainRegionKey(), getCommonSelect());
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(getMainRegionKey(), getCommonSelect(), id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, getMainRegionKey(), getCommonSelect(), true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, getMainRegionKey(), getCommonSelect());
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ValueDescriptor.class;
    }
}