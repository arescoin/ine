/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.*;
import ru.xr.ine.dbe.da.core.*;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked", "UnusedDeclaration"})
public class CustomAttributeAccessImpl<T extends CustomAttribute>
        extends AbstractVersionableAccess<T> implements CustomAttributeAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "CUSTOM_ATTRS_DSC";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_SYS_OBJ_N = "sys_obj_n";
    public static final String COLUMN_CUSTOM_TYPE = "custom_type";
    public static final String COLUMN_ATTR_TYP = "attr_typ";
    public static final String COLUMN_ATTR_NAME = "attr_name";
    public static final String COLUMN_IS_REQUIRED = "is_required";
    public static final String COLUMN_PATTERN = "pattern";
    public static final String COLUMN_LIMITS = "limits";

    /** Запрос на выборку всех атрибутов */
    public static final String ALL_CUSTOM_ATTRIBUTE_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_SYS_OBJ_N +
            ", " + TABLE_PREF + COLUMN_CUSTOM_TYPE +
            ", " + TABLE_PREF + COLUMN_ATTR_TYP +
            ", " + TABLE_PREF + COLUMN_ATTR_NAME +
            ", " + TABLE_PREF + COLUMN_IS_REQUIRED +
            ", " + TABLE_PREF + COLUMN_PATTERN +
            ", " + TABLE_PREF + COLUMN_LIMITS +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.structure.CustomAttribute} */
    public static final String CACHE_REGION_CUSTOM_ATTRIBUTES = CustomAttribute.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(CustomAttribute.SYSTEM_OBJECT_ID, COLUMN_SYS_OBJ_N);
        map.put(CustomAttribute.CUSTOM_TYPE_ID, COLUMN_CUSTOM_TYPE);
        map.put(CustomAttribute.DATA_TYPE, COLUMN_ATTR_TYP);
        map.put(CustomAttribute.ATTRIBITE_NAME, COLUMN_ATTR_NAME);
        map.put(CustomAttribute.REQUIRED, COLUMN_IS_REQUIRED);
        map.put(CustomAttribute.PATTERN, COLUMN_PATTERN);
        map.put(CustomAttribute.LIMITATIONS, COLUMN_LIMITS);
        setInstanceFieldsMapping(CustomAttributeAccessImpl.class, map);

        Set<CRUDHelper> customAttributeHelpers = new LinkedHashSet<CRUDHelper>();
        customAttributeHelpers.add(new CustomAttributeHelper());
        registerVersionableHelpers(CustomAttributeAccessImpl.class, customAttributeHelpers);
    }


    @Override
    public String getTableName() {
        return CustomAttributeAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_CUSTOM_ATTRIBUTES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        T result = (T) IdentifiableFactory.getImplementation(CustomAttribute.class);

        this.setVersionableFields(resultSet, result);

        result.setAttributeName(this.getString(resultSet, COLUMN_ATTR_NAME));
        result.setSystemObjectId(this.getBigDecimal(resultSet, COLUMN_SYS_OBJ_N));
        result.setCustomTypeId(this.getBigDecimal(resultSet, COLUMN_CUSTOM_TYPE));
        result.setRequired(this.getBoolean(resultSet, COLUMN_IS_REQUIRED));
        result.setLimitations(this.getString(resultSet, COLUMN_LIMITS));

        BigDecimal typeCode = this.getId(resultSet, COLUMN_ATTR_TYP);
        DataType dataType = DataType.getType(typeCode.intValue() - 1);
        if (dataType == null) {
            throw new GenericSystemException("Wrong Type code [" + typeCode + "]");
        }
        result.setDataType(dataType);

        String pattern = this.getString(resultSet, COLUMN_PATTERN);
        if (pattern != null) {
            result.setPattern(PatternUtils.checkPattern(new SystemObjectPattern(pattern)));
        }

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_CUSTOM_ATTRIBUTES, ALL_CUSTOM_ATTRIBUTE_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_CUSTOM_ATTRIBUTES, ALL_CUSTOM_ATTRIBUTE_SELECT, id);
    }

    @Override
    public Collection<T> getAttributesByObject(Class<? extends Identifiable> identifiableClass)
            throws GenericSystemException {

        return this.getAttributesByObject(identifiableClass, null);
    }

    Collection<T> getAttributesByObject(Class<? extends Identifiable> identifiableClass, BigDecimal customTypeId)
            throws GenericSystemException {

        Set<SearchCriterion> criteria = getAttributesByObjectSearchCriteria(identifiableClass);
        SearchCriterion customTypeCriterion = getSearchCriteriaProfile().getSearchCriterion(T.CUSTOM_TYPE_ID);
        customTypeCriterion.setCriterionRule(customTypeId == null ? CriteriaRule.isNull : CriteriaRule.equals);
        customTypeCriterion.addCriteriaVal(customTypeId);
        criteria.add(customTypeCriterion);

        return this.searchObjects(criteria);
    }

    /**
     * Получаем все атрибуты, привязанные к заданному базовому типу, без группировки по кастомным типам.<br>
     * Метод предназначен для внутреннего использованения в {@link CustomAttributeValueAccessImpl.ValuesByTypeProducer}
     *
     * @param identifiableClass класс, для которого получается коллекция
     *
     * @return коллекция всех атрибутов
     * @throws GenericSystemException в случае ошибок при работе с мета-информацией, кешем или БД
     */
    Collection<T> getAllAttributesByObject(Class identifiableClass) throws GenericSystemException {
        return this.searchObjects(getAttributesByObjectSearchCriteria(identifiableClass));
    }

    private Set<SearchCriterion> getAttributesByObjectSearchCriteria(Class identifiableClass)
            throws GenericSystemException {

        Set<SearchCriterion> criteria = new LinkedHashSet<SearchCriterion>();
        BigDecimal tableId = FieldAccess.getTableId(identifiableClass);
        criteria.add(getSearchCriteriaProfile().getSearchCriterion(T.SYSTEM_OBJECT_ID, CriteriaRule.equals, tableId));

        return criteria;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_CUSTOM_ATTRIBUTES, ALL_CUSTOM_ATTRIBUTE_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_CUSTOM_ATTRIBUTES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_CUSTOM_ATTRIBUTES, ALL_CUSTOM_ATTRIBUTE_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return CustomAttribute.class;
    }


    public static final class CustomAttributeHelper<E extends CustomAttribute> extends AbstractCRUDHelper<E> {

        private static final String MSG_START = "Attribute description can not be deleted. ";
        private static final String VALUES_EXISTS = "while exists values of this attribute";

        private CustomAttributeAccess<CustomAttribute> getAttributeAccess() throws GenericSystemException {
            return (CustomAttributeAccess) AccessFactory.getImplementation(CustomAttribute.class);
        }

        private CustomAttributeValueAccess<CustomAttributeValue> getValueAccess() throws GenericSystemException {
            return (CustomAttributeValueAccess) AccessFactory.getImplementation(CustomAttributeValue.class);
        }

        @Override
        public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {
            this.checkPattern(identifiable);
            this.checkDuplicates(identifiable);
        }

        @Override
        public void beforeUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {

            // проверяем изменение атрибутов, которое нельзя изменять
            if (!newIdentifiable.getAttributeName().equals(identifiable.getAttributeName())) {
                throw new IneIllegalArgumentException("Change of attributeName is prohibited");
            }
            if (!newIdentifiable.getSystemObjectId().equals(identifiable.getSystemObjectId())) {
                throw new IneIllegalArgumentException("Change of systemObjectId is prohibited");
            }
            if (!newIdentifiable.getDataType().equals(identifiable.getDataType())) {
                throw new IneIllegalArgumentException("Change of dataType is prohibited");
            }
            if (newIdentifiable.getCustomTypeId() == null ? identifiable.getCustomTypeId() != null
                    : !newIdentifiable.getCustomTypeId().equals(identifiable.getCustomTypeId())) {
                throw new IneIllegalArgumentException("Change of customTypeId is prohibited");
            }

            // проверяем изменение атрибутов, которые нельзя изменять если есть значения данного кастмного атрибута
            if (this.hasValues(newIdentifiable)) {
                if (!identifiable.isRequired() && newIdentifiable.isRequired()) {
                    throw new IneIllegalArgumentException(
                            "Can't change required attibute from FALSE to TRUE, " + VALUES_EXISTS);
                }

                if (newIdentifiable.getPattern() != null && identifiable.getPattern() == null) {
                    throw new IneIllegalArgumentException("Can't change pattern, " + VALUES_EXISTS);
                }

                if (newIdentifiable.getPattern() == null && identifiable.getPattern() != null) {
                    throw new IneIllegalArgumentException("Can't change pattern, " + VALUES_EXISTS);
                }

                if (newIdentifiable.getPattern() != null
                        && !newIdentifiable.getPattern().equals(identifiable.getPattern())) {
                    throw new IneIllegalArgumentException("Can't change pattern, " + VALUES_EXISTS);
                }


                if (newIdentifiable.getLimitations() != null && identifiable.getLimitations() == null) {
                    throw new IneIllegalArgumentException("Can't change pattern, " + VALUES_EXISTS);
                }

                if (newIdentifiable.getLimitations() == null && identifiable.getLimitations() != null) {
                    throw new IneIllegalArgumentException("Can't change pattern, " + VALUES_EXISTS);
                }

                if (newIdentifiable.getLimitations() != null && (
                        !newIdentifiable.getLimitations().equals(identifiable.getLimitations()))) {
                    throw new IneIllegalArgumentException("Can't change limitations, " + VALUES_EXISTS);
                }
            }

            this.checkPattern(newIdentifiable);
        }

        @Override
        public void beforeDelete(E identifiable) throws GenericSystemException {

            //проверка что удаляемое описание атрибута не используется в CustomType
            if (identifiable.getCustomTypeId() != null) {
                throw new IneIllegalArgumentException(
                        MSG_START + "It is used in CustomType[" + identifiable.getCustomTypeId() + "]");
            }

            //проверка что нет CustomAttributeValue объектов с удаляемым описанием CustomAttribute
            if (this.hasValues(identifiable)) {
                throw new IneIllegalArgumentException(MSG_START +
                        "There are some CustomAttributeValue objects of this CustomAttribute type");
            }

            // удаляем дефолтное значение атрибута, если есть
            this.deleteDefValue(identifiable);
        }

        private void checkPattern(E identifiable) {
            if (identifiable.getPattern() != null && !identifiable.getDataType().equals(DataType.bigDecimalType)) {
                throw new IneIllegalArgumentException(
                        "Incorrect DataType found for CustomAttribute with pattern passed. Required: " +
                                DataType.bigDecimalType + ", but current are: " + identifiable.getDataType());
            }
        }

        private void checkDuplicates(E identifiable) throws GenericSystemException {

            boolean sameNameExists;
            if (identifiable.getCustomTypeId() == null) {
                /** Если атрибут добавляется на корневой объект (без кастом-типа), то проверяем все атрибуты */
                sameNameExists = this.checkDuplicatiesForBaseObject(identifiable);
            } else {
                /** Иначе, надо проверить атрибуты, принадлежащие кастом-типам определённой ветки */
                sameNameExists = this.checkDuplicatesForCustomType(identifiable);
            }

            /** Теперь проверяем атрибут на совпадение с именами атрибутов самого класса */
            if (!sameNameExists) {
                sameNameExists = this.checkDuplicatesForBaseClass(identifiable);
            }

            if (sameNameExists) {
                throw new IneIllegalArgumentException("Attribute name [" + identifiable.getAttributeName() +
                        "] is already used in this object");
            }
        }

        private boolean checkDuplicatiesForBaseObject(E identifiable) throws GenericSystemException {
            /**
             * Для проверки атрибута, добавляемого в корневой объект, достаточно выполнить обычный поиск
             * по всем атрибутам, привязанных к базовому типу.
             */
            SearchCriteriaProfile profile = this.getAttributeAccess().getSearchCriteriaProfile();
            Set<SearchCriterion> criteria = new LinkedHashSet<SearchCriterion>(3);
            criteria.add(profile.getSearchCriterion(
                    E.SYSTEM_OBJECT_ID, CriteriaRule.equals, identifiable.getSystemObjectId()));
            criteria.add(profile.getSearchCriterion(
                    E.ATTRIBITE_NAME, CriteriaRule.equals, identifiable.getAttributeName()));

            return this.getAttributeAccess().containsObjects(criteria);
        }

        private boolean checkDuplicatesForCustomType(E identifiable) throws GenericSystemException {
            /**
             * А вот при добавлении атрибута в некоторый кастом-тип, надо проверять уникальность имени
             * только в рамках иерархии типов, которой принадлежит кастом-тип.
             */
            Map<BigDecimal, CustomType> allTypes = this.getTypesForBaseObject(identifiable.getSystemObjectId());
            CustomType customType = allTypes.get(identifiable.getCustomTypeId());
            /** Сначала получаем все кастомные типы, являющиеся предками интересующего нас типа (включая его самого) */
            Collection<CustomType> types = this.getParents(allTypes, customType);
            /** Потом добавляем всех потомков */
            types.addAll(this.getChildren(allTypes, customType));
            /** А теперь итерируем полученную коллекцию и проверяем имена атрибутов в каждом типе */
            for (CustomType type : types) {
                for (CustomAttribute attribute : type.getCustomAttributes()) {
                    if (attribute.getAttributeName().equals(identifiable.getAttributeName())) {
                        return true;
                    }
                }
            }

            return false;
        }

        private Map<BigDecimal, CustomType> getTypesForBaseObject(BigDecimal objectId) throws GenericSystemException {
            /**
             * Получаем список всех кастомных типов объекта в виде мапы (для удобства дальнейшего использования)
             */
            CustomTypeAccess<CustomType> access = (CustomTypeAccess) AccessFactory.getImplementation(CustomType.class);
            Collection<CustomType> types = access.getTypesForBaseObject(objectId);
            Map<BigDecimal, CustomType> result = new HashMap<BigDecimal, CustomType>(types.size(), 1.1f);
            for (CustomType type : types) {
                result.put(type.getCoreId(), type);
            }

            return result;
        }

        private Collection<CustomType> getParents(Map<BigDecimal, CustomType> types, CustomType type) {
            /**
             * Рекурсивная функция.
             * Если у переданного типа есть предок, то вызываем эту же функцию с передачей в неё предка.
             * Иначе получается завершение рекурсии - создаём результирующую коллекцию, в которую будут помещаться
             * все предки.
             * В любом случае, в результирующую коллекцию добавляется переданный тип. Поэтому получение предков
             * происходит с добавлением в результат типа, для которого и получаются предки.
             */
            Collection<CustomType> result;
            if (type.getParentTypeId() != null) {
                CustomType parent = types.get(type.getParentTypeId());
                result = this.getParents(types, parent);
            } else {
                result = new ArrayList<CustomType>();
            }
            result.add(type);

            return result;
        }

        private Collection<CustomType> getChildren(Map<BigDecimal, CustomType> types, CustomType type) {
            /**
             * Рекурсивная функция.
             * Сначала получаем всех непосредственных потомков переданного типа.
             * Потом полученная коллекция итерируется, и для каждого типа в ней
             * получаются его непосредственные потомки.
             */
            Collection<CustomType> children = new ArrayList<CustomType>();
            for (Map.Entry<BigDecimal, CustomType> entry : types.entrySet()) {
                if (type.getCoreId().equals(entry.getValue().getParentTypeId())) {
                    children.add(entry.getValue());
                }
            }
            Collection<CustomType> result = new ArrayList<CustomType>(children);
            for (CustomType child : children) {
                result.addAll(this.getChildren(types, child));
            }

            return result;
        }

        private boolean checkDuplicatesForBaseClass(E identifiable) throws GenericSystemException {
            /**
             * Получаем список доступных полей класса (для которых есть геттеры/сеттеры).
             * Имя создаваемого атрибута не должно совпадать с назвниями полей класса.
             */
            String intf = FieldAccess.getInterface(identifiable.getSystemObjectId());

            try {
                return checkDuplicatesForBaseClass(Class.forName(intf), identifiable.getAttributeName());
            } catch (Exception e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

        private boolean checkDuplicatesForBaseClass(Class clazz, String attrName) throws GenericSystemException {
            /**
             * Получаем список доступных полей класса (для которых есть геттеры/сеттеры).
             * Имя создаваемого атрибута не должно совпадать с назвниями полей класса.
             */

            try {
                BeanInfo beanInfo = Introspector.getBeanInfo(clazz);
                PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
//                if (descriptors == null) {
//                    return false;
//                }

                for (PropertyDescriptor descriptor : descriptors) {
                    if (attrName.equals(descriptor.getName())) {
                        return true;
                    }
                }
            } catch (Exception e) {
                throw GenericSystemException.toGeneric(e);
            }


            for (Class aClass1 : clazz.getInterfaces()) {
                if (checkDuplicatesForBaseClass(aClass1, attrName)) {
                    return true;
                }
            }

            return false;
        }

        private boolean hasValues(E identifiable) throws GenericSystemException {

            SearchCriterion idSearch = this.getValueAccess().getSearchCriteriaProfile().getSearchCriterion(
                    CustomAttributeValue.CORE_ID, CriteriaRule.equals, identifiable.getCoreId());
            idSearch.addAdditionalParam(FieldAccess.getInterface(identifiable.getSystemObjectId()));

            Set<SearchCriterion> searchCriterias = new HashSet<SearchCriterion>();
            searchCriterias.add(idSearch);

            return this.getValueAccess().containsObjects(searchCriterias);
        }

        private void deleteDefValue(E identifiable) throws GenericSystemException {

            CustomAttributeDefValAccess<CustomAttributeDefVal> defValAccess =
                    (CustomAttributeDefValAccess) AccessFactory.getImplementation(CustomAttributeDefVal.class);
            try {
                CustomAttributeDefVal defVal = defValAccess.getObjectById(identifiable.getCoreId());
                if (defVal != null) {
                    defValAccess.deleteObject(defVal);
                }
            } catch (CoreException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

    }

}
