/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: AbstractCRUDHelper.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class AbstractCRUDHelper<T extends Identifiable> implements CRUDHelper<T> {

    @Override
    public void beforeCreate(T identifiable, IdentifiableAccess<T> access) throws GenericSystemException {
    }

    @Override
    public void afterCreate(T identifiable) throws GenericSystemException {
    }

    @Override
    public void beforeUpdate(T identifiable, T newIdentifiable) throws GenericSystemException {
    }

    @Override
    public void afterUpdate(T identifiable, T newIdentifiable) throws GenericSystemException {
    }

    @Override
    public void beforeDelete(T identifiable) throws GenericSystemException {
    }

    @Override
    public void afterDelete(T identifiable) throws GenericSystemException {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (!(o instanceof AbstractCRUDHelper)) {
            return false;
        }
        return this.getClass().getName().equals(o.getClass().getName());
    }

    @Override
    public int hashCode() {
        return this.getClass().getName().hashCode();
    }

}
