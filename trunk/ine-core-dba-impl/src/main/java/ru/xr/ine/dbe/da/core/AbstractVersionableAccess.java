/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.SystemObjectPattern;
import ru.xr.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.xr.ine.dbe.da.core.structure.PatternUtils;
import ru.xr.ine.utils.cache.Cache;
import ru.xr.ine.utils.cache.Producer;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Заготовка для общих операций выборки версионных объектов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractVersionableAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public abstract class AbstractVersionableAccess<T extends Versionable>
        extends AbstractIdentifiableAccess<T> implements VersionableAccess<T> {

    /** Название колонки с датой начала версии */
    public static final String COLUMN_FD = "fd";

    /** Название колонки с датой закрытия версии */
    public static final String COLUMN_TD = "td";

    private static Logger logger = Logger.getLogger(AbstractVersionableAccess.class.getName());

    static {
        final Map<String, String> map = new HashMap<String, String>();
        setInstanceFieldsMapping(AbstractVersionableAccess.class, map);

        registerVersionableHelpers(AbstractVersionableAccess.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {

        Map<String, String> map = new HashMap<String, String>();
        map.put(Versionable.CORE_FD, COLUMN_FD);
        map.put(Versionable.CORE_TD, COLUMN_TD);
        map.putAll(instanceFields);

        AbstractIdentifiableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerVersionableHelpers(Class<? extends AbstractVersionableAccess> access,
                                                     Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<CRUDHelper>();
        helpersAll.add(new CommonPatternHelper());
        helpersAll.addAll(helpers);

        AbstractIdentifiableAccess.registerIdentifiableHelpers(access, helpersAll);
    }

    @Override
    public Collection<T> getOldVersions(T versionable) throws GenericSystemException {

        try {
            return getObjects(false, "SELECT TBL_HSTR." + COLUMN_ROWID + ", TBL_HSTR.* FROM " +
                    getTableName() + HISTORY + " TBL_HSTR WHERE " + getIdConditionSql("TBL_HSTR"),
                    (Object[]) getSyntheticId(versionable).getIdValues());
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    @Override
    public Collection<T> getOldVersions(T versionable, Date from, Date to) throws GenericSystemException {

        try {
            return getObjects(false, "SELECT TBL_HSTR." + COLUMN_ROWID + ", TBL_HSTR.* FROM " +
                    getTableName() + HISTORY + " TBL_HSTR WHERE " + getIdConditionSql("TBL_HSTR") +
                    " AND TBL_HSTR." + COLUMN_FD + " between ? and ?",
                    getSyntheticId(versionable).getIdValues(), from, to);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Проверяет применимость поступившего объекта
     *
     * @param identifiable проверяемый объект
     */
    @Override
    protected void checkApplicable(T identifiable) {

        if (identifiable == null) {
            throw new IneIllegalArgumentException("The source object can't be null");
        }

        if (identifiable.getCoreTd().before(new Date())) {
            throw new IneIllegalArgumentException("The source object has been expired");
        }
    }

    /**
     * Метод должен быть переопределен, должен конструировать инстанс объекта по результату выборки
     *
     * @param resultSet "строка" выборки
     *
     * @return сконструированный объект
     * @throws java.sql.SQLException при некорректной обработке данных из выбоки
     * @throws ru.xr.ine.core.CoreException
     *                               при обнаружении некорректных значений в полях
     * @throws ru.xr.ine.core.RuntimeCoreException
     *                               при возникновении системных ошибок
     */
    @Override
    protected abstract T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException;

    /* *********************
     * Обновление объекта
     * *********************/

    /**
     * Оболочка для вызова при изменении объекта.
     *
     * @param versionable    старый объект
     * @param newVersionable новый объект
     * @param region         имя региона кеша, хранящего данный объект
     *
     * @return модифицированный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке передать null
     * @throws ru.xr.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    @Override
    protected T updateObject(T versionable, T newVersionable, String region)
            throws IneIllegalArgumentException, GenericSystemException {

        //проверки объекта и его модификации
        this.checkApplicable(versionable);
        this.checkApplicable(newVersionable);

        try {
            this.beforeUpdate(versionable, newVersionable);
            this.updateObjectInDB(versionable, newVersionable);
            Cache.put(region, getSyntheticId(newVersionable), newVersionable, false);
            this.afterUpdate(versionable, newVersionable);
        } catch (CoreException e) {
            logger.log(Level.WARNING, "Can't update an object!", e);
            throw GenericSystemException.toGeneric(e);
        }

        return newVersionable;
    }

    /**
     * Изменяет свойства существующего в системе объекта
     *
     * @param identifiable    старый объект
     * @param newIdentifiable новый объект
     *
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.xr.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    public void updateObjectInDB(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        try {
            T oldIdentifiable = this.getObjectById(this.getSyntheticId(identifiable).getIdValues());
            if (!identifiable.equals(oldIdentifiable)) {
                throw new IneCorruptedStateException("identifiable object is not actual: expected [" +
                        oldIdentifiable + "] but was received [" + identifiable + "]");
            }
            this.updateObjectInDB(newIdentifiable);

            if (FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_HISTORY).isEnabled()) {
                oldIdentifiable.setCoreTd(newIdentifiable.getCoreFd());
                this.createObject(oldIdentifiable, false, true);
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /* *********************
     * Удаление объекта
     * *********************/

    @Override
    protected void deleteObject(T identifiable, Producer allProducer, String region)
            throws IneIllegalArgumentException, GenericSystemException {

        try {
            if (FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_HISTORY).isEnabled()) {
                this.createObject(identifiable, false, true);
            }
            super.deleteObject(identifiable, allProducer, region);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /* ***********************
     * Получение данных из БД
     * ***********************/

    /**
     * Получает значение даты начала версии из колонки по умолчанию {@link #COLUMN_FD}
     *
     * @param resultSet результат выборки
     *
     * @return идентификатор
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    @SuppressWarnings({"UnusedDeclaration"})
    protected Date getTd(ResultSet resultSet) throws SQLException {
        return getDate(resultSet, COLUMN_TD);
    }

    /**
     * Получает значение даты начала версии из указанной колонки
     *
     * @param resultSet результат выборки
     * @param toDate    наименование колонки
     *
     * @return идентификатор
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    protected Date getTd(ResultSet resultSet, String toDate) throws SQLException {
        return getDate(resultSet, toDate);
    }

    /**
     * Получает значение даты закрытия версии из колонки по умолчанию {@link #COLUMN_TD}
     *
     * @param resultSet результат выборки
     *
     * @return идентификатор
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    @SuppressWarnings({"UnusedDeclaration"})
    protected Date getFd(ResultSet resultSet) throws SQLException {
        return getDate(resultSet, COLUMN_FD);
    }

    /**
     * Получает значение даты закрытия версии из указанной колонки
     *
     * @param resultSet результат выборки
     * @param fromDate  наименование колонки
     *
     * @return идентификатор
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    protected Date getFd(ResultSet resultSet, String fromDate) throws SQLException {
        return getDate(resultSet, fromDate);
    }

    protected void setVersionableFields(ResultSet resultSet, Versionable versionable)
            throws SQLException, CorruptedIdException, IneCorruptedVersionableDateException {
        this.setVersionableFields(resultSet, versionable, AbstractIdentifiableAccess.COLUMN_ID);
    }

    protected void setVersionableFields(ResultSet resultSet, Versionable versionable, String idColumn)
            throws SQLException, CorruptedIdException, IneCorruptedVersionableDateException {
        this.setVersionableFields(resultSet, versionable, idColumn, AbstractIdentifiableAccess.COLUMN_DSC);
    }

    protected void setVersionableFields(ResultSet resultSet, Versionable versionable, String idColumn, String dscColumn)
            throws SQLException, CorruptedIdException, IneCorruptedVersionableDateException {
        this.setVersionableFields(resultSet, versionable, idColumn, dscColumn,
                AbstractVersionableAccess.COLUMN_FD, AbstractVersionableAccess.COLUMN_TD);
    }

    protected void setVersionableFields(ResultSet resultSet, Versionable versionable,
                                        String idColumn, String dscColumn, String fdColumn, String tdColumn)
            throws SQLException, CorruptedIdException, IneCorruptedVersionableDateException {

        this.setIdentifiableFields(resultSet, versionable, idColumn, dscColumn);

        Date fd = this.getFd(resultSet, fdColumn);
        Date td = this.getTd(resultSet, tdColumn);

        try {
            versionable.setCoreFd(fd);
            versionable.setCoreTd(td);
        } catch (IneIllegalArgumentException e) {
            throw new IneCorruptedVersionableDateException("Corrupted Date: fd ["
                    + fd + "], td[" + td + "], rowId [" + resultSet.getString(1) + "].");
        }
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Versionable.class;
    }

    /* ***********************
     * Утилитные CRUD-хелперы
     * ***********************/

    protected static class CommonPatternHelper<E extends Versionable> extends AbstractCRUDHelper<E> {

        private Map<Class<E>, Collection<Method>> patternFieldsByClass = new HashMap<Class<E>, Collection<Method>>();

        @Override
        public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {
            for (SystemObjectPattern pattern : getPatterns(identifiable)) {
                if (pattern != null) {
                    this.checkPattern(pattern);
                }
            }
        }

        @Override
        public void beforeUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {
            for (SystemObjectPattern pattern : getPatterns(newIdentifiable)) {
                if (pattern != null) {
                    this.checkPattern(pattern);
                }
            }
        }

        protected void checkPattern(SystemObjectPattern pattern) throws GenericSystemException {
            try {
                PatternUtils.checkPattern(pattern);
            } catch (CoreException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

        @SuppressWarnings({"unchecked"})
        private Collection<SystemObjectPattern> getPatterns(E identifiable) throws GenericSystemException {

            Class<E> clazz = (Class<E>) identifiable.getClass();
            if (!patternFieldsByClass.containsKey(clazz)) {
                AbstractVersionableAccess<E> access = (AbstractVersionableAccess)
                        AccessFactory.getImplementation(identifiable.getClass());
                Map<String, Method> fieldMethods = access.obtainFieldMethodMapping(identifiable.getClass());

                Collection<Method> methods = new HashSet<Method>();
                for (Method method : fieldMethods.values()) {
                    if (SystemObjectPattern.class.isAssignableFrom(method.getReturnType())) {
                        methods.add(method);
                    }
                }
                patternFieldsByClass.put(clazz, methods);
            }

            Collection<SystemObjectPattern> result = new ArrayList<SystemObjectPattern>();
            for (Method method : patternFieldsByClass.get(clazz)) {
                try {
                    result.add((SystemObjectPattern) method.invoke(identifiable));
                } catch (Exception e) {
                    throw GenericSystemException.toGeneric(e);
                }
            }

            return result;
        }
    }

}
