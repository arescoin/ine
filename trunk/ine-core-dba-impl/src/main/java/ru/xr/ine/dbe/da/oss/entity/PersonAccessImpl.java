/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.entity;

import ru.xr.ine.core.*;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;
import ru.xr.ine.oss.entity.Person;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PersonAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class PersonAccessImpl<T extends Person> extends AbstractVersionableAccess<T> implements PersonAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "PERSON";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_FAMILY_NAME = "family_name";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_MIDDLE_NAME = "middle_name";
    public static final String COLUMN_ALIAS = "alias";
    public static final String COLUMN_FAMILY_NAME_PREFIX = "family_name_prefix";
    public static final String COLUMN_FAMILY_GENERATION = "family_generation";
    public static final String COLUMN_ADDRESS_FORM = "address_form";

    /** Запрос на выборку всех значений */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_FAMILY_NAME +
            ", " + TABLE_PREF + COLUMN_NAME +
            ", " + TABLE_PREF + COLUMN_MIDDLE_NAME +
            ", " + TABLE_PREF + COLUMN_ALIAS +
            ", " + TABLE_PREF + COLUMN_FAMILY_NAME_PREFIX +
            ", " + TABLE_PREF + COLUMN_FAMILY_GENERATION +
            ", " + TABLE_PREF + COLUMN_ADDRESS_FORM +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.oss.entity.Person} */
    public static final String CACHE_REGION_PERSONS = Person.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(Person.FAMILY_NAME, COLUMN_FAMILY_NAME);
        map.put(Person.NAME, COLUMN_NAME);
        map.put(Person.MIDDLE_NAME, COLUMN_MIDDLE_NAME);
        map.put(Person.ALIAS, COLUMN_ALIAS);
        map.put(Person.FAMILY_NAME_PREFIX, COLUMN_FAMILY_NAME_PREFIX);
        map.put(Person.FAMILY_GENERATION, COLUMN_FAMILY_GENERATION);
        map.put(Person.FORM_OF_ADDRESS, COLUMN_ADDRESS_FORM);
        setInstanceFieldsMapping(PersonAccessImpl.class, map);

        registerVersionableHelpers(PersonAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_PERSONS;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(Person.class);

        this.setVersionableFields(resultSet, value);

        value.setFamilyName(this.getString(resultSet, COLUMN_FAMILY_NAME));
        value.setName(this.getString(resultSet, COLUMN_NAME));
        value.setMiddleName(this.getString(resultSet, COLUMN_MIDDLE_NAME));
        value.setAlias(this.getString(resultSet, COLUMN_ALIAS));
        value.setFamilyNamePrefixCode(this.getId(resultSet, COLUMN_FAMILY_NAME_PREFIX));
        value.setFamilyGeneration(this.getString(resultSet, COLUMN_FAMILY_GENERATION));
        value.setFormOfAddress(this.getString(resultSet, COLUMN_ADDRESS_FORM));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_PERSONS, ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_PERSONS, ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_PERSONS, ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_PERSONS);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_PERSONS, ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Person.class;
    }

}
