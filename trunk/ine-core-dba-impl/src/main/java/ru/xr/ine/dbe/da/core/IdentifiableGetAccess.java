/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.*;
import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.utils.cache.Cache;
import ru.xr.ine.utils.cache.Producer;
import ru.xr.ine.utils.searchCriteria.AllowableRules;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * Утилитный класс, обеспечивает разгрузку основного {@link ru.xr.ine.dbe.da.core.AbstractIdentifiableAccess}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableGetAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
class IdentifiableGetAccess<T extends Identifiable> {

    private AbstractIdentifiableAccess<T> access;

    IdentifiableGetAccess(AbstractIdentifiableAccess<T> identifiableAccess) {
        this.access = identifiableAccess;
    }

    @SuppressWarnings({"unchecked"})
    public T getObjectById(String region, Producer producer, SyntheticId id) throws CoreException {
        return (T) Cache.get(region, id, new DefaultSystemProducer(region, producer));
    }

    public SearchCriteriaProfile getSearchCriteriaProfile() throws GenericSystemException {

        SearchCriteriaProfile result;
        @SuppressWarnings({"unchecked"}) Class<T> subjectClass = this.access.getSubjectClass();

        Class impl = IdentifiableFactory.getImplementation(subjectClass).getClass();
        Map<String, Method> methodMap = this.access.obtainFieldMethodMapping(impl);
        result = new SearchCriteriaProfile(impl);

        for (String fieldName : methodMap.keySet()) {
            Method method = methodMap.get(fieldName);
            Class aClass = method.getReturnType();
            DataType dataType = DataType.getTypeByClass(aClass);
            if (dataType != null) {
                result.addRule(fieldName, AllowableRules.getCriteriaRules(dataType), method);
            } else {
                result.addMethod(fieldName, method);
            }
        }

        return result;
    }

    class DefaultSystemProducer implements Producer {

        private final String region;
        private final Producer producer;

        public DefaultSystemProducer(String region, Producer producer) {
            this.region = region;
            this.producer = producer;
        }

        @Override
        public Object get(Object key) throws CoreException {

            Object result = null;
            if (key != null && Cache.getKeys(this.region).isEmpty()) {
                if (Cache.getRegionContent(this.region, this.producer).keySet().contains(key)) {
                    result = Cache.get(this.region, key, NULL_PRODUCER);
                }
            }

            return result;
        }
    }

}
