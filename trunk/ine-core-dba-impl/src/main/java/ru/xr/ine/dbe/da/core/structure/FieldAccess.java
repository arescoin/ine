/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.SyntheticId;
import ru.xr.ine.core.structure.SystemObject;
import ru.xr.ine.core.structure.SystemObjectType;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.dbe.da.core.IdentifiableAccess;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Утилитный класс обеспечивающий доступ к мета-информации для полей объектов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: FieldAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
public final class FieldAccess {

    private static Logger logger = Logger.getLogger(FieldAccess.class.getName());

    private static final FieldAccess fieldAccess = new FieldAccess();

    private SystemObjectAccess<SystemObject> ACCESS;


    @SuppressWarnings({"unchecked"})
    private FieldAccess() {
        try {
            ACCESS = (SystemObjectAccess<SystemObject>) AccessFactory.getImplementation(SystemObject.class);
        } catch (CoreException e) {
            logger.log(Level.WARNING, "Can't create FieldAccess", e);
        }
    }

    /**
     * Возвращает системный объект по идентификатору
     *
     * @param id идентификатор
     *
     * @return системный объект
     * @throws GenericSystemException при ошибках в работе с кешом
     */
    public static SystemObject getObjectById(BigDecimal id) throws GenericSystemException {
        try {
            return fieldAccess.ACCESS.getObjectById(id);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Возвращает идентификатор системного объекта, описывающего столбец таблицы.
     *
     * @param tableName  идентификатор таблицы
     * @param columnName название столбца
     *
     * @return идентификатор поля (системного объекта)
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static BigDecimal getColumnId(String tableName, String columnName) throws GenericSystemException {
        return fieldAccess.ACCESS.getColumnId(tableName, columnName);
    }

    /**
     * Возвращает идентификатор системного объекта, описывающего таблицу Identifiable-объекта. Класс интерфейса -
     * наследника {@link ru.xr.ine.core.Identifiable}, должен иметь имплементацию и соответствующую реализацию
     * {@link ru.xr.ine.dbe.da.core.ObjectAccess}'а.
     *
     * @param identifiableClass класс интерфейса
     *
     * @return идентификатор таблицы (системного объекта)
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static BigDecimal getTableId(Class identifiableClass) throws GenericSystemException {
        return fieldAccess.ACCESS.getTableId(identifiableClass);
    }

    /**
     * Возвращает имя класса, соответствующего переданному идентификатору таблицы
     *
     * @param tableId идентификатор таблицы
     *
     * @return имя класса
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static String getInterface(BigDecimal tableId) throws GenericSystemException {
        return fieldAccess.ACCESS.getInterfaces().get(tableId);
    }

    /**
     * Возвращает список InE-интерфейсов, для которых есть таблицы.
     *
     * @return список InE-интерфейсов
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static Collection<String> getSupportedIntefaces() throws GenericSystemException {
        return new HashSet<>(fieldAccess.ACCESS.getInterfaces().values());
    }

    /**
     * Возвращает имя сиквенса в БД, используемого для генерации иденитфикатора
     *
     * @param identifiableClass класс для которого ищется сиквенс
     *
     * @return имя сиквенса
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static String getSequenceForIdentifiable(Class identifiableClass) throws GenericSystemException {

        SystemObject systemObject;
        try {
            systemObject = fieldAccess.ACCESS.getObjectById(fieldAccess.ACCESS.getSequenceIdByClass(identifiableClass));
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
        if (systemObject != null) {
            return systemObject.getName();
        }

        return null;
    }

    /**
     * Возвращает все дочерние объекты для переданного идентификатора системного объекта.
     *
     * @param parentId идентификатор системного объекта
     *
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.GenericSystemException
     *          при возникновении ошибок в механизме доступа к мета
     */
    public static Collection<SystemObject> getChildren(BigDecimal parentId) throws GenericSystemException {
        try {
            return fieldAccess.ACCESS.getChildren(parentId);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Возвращает все дочерние объекты определённого типа для переданного идентификатора системного объекта.
     *
     * @param parentId идентификатор системного объекта
     * @param type     тип дочерних объектов
     *
     * @return коллекция системных объектов
     * @throws ru.xr.ine.core.GenericSystemException
     *          при возникновении ошибок в механизме доступа к мета
     */
    public static Collection<SystemObject> getChildren(BigDecimal parentId, SystemObjectType type)
            throws GenericSystemException {

        Collection<SystemObject> children = getChildren(parentId);
        for (Iterator<SystemObject> it = children.iterator(); it.hasNext(); ) {
            if (!it.next().getType().equals(type.getTypeCode())) {
                it.remove();
            }
        }

        return children;
    }

    /**
     * Возвращает все объекты, в которых содержатся паттерны переданного системного типа
     *
     * @param tableId системный тип объектов
     *
     * @return коллекция объектов с паттернами
     * @throws GenericSystemException при работе с кешом
     */
    public static Collection<SystemObject> getPatternObjects(BigDecimal tableId) throws GenericSystemException {

        Collection<SystemObject> result = fieldAccess.ACCESS.getPatternObjects(tableId);

        return result == null ? new ArrayList<SystemObject>() : result;
    }

    /**
     * Возвращает все паттерны переданного системного типа
     *
     * @param tableId системный тип объектов
     *
     * @return коллекция объектов с паттернами
     * @throws GenericSystemException при работе с кешом
     */
    public static Collection<SystemObject> getPatternsForObject(BigDecimal tableId) throws GenericSystemException {

        Collection<SystemObject> result = fieldAccess.ACCESS.getPatternsForType(tableId);

        return result == null ? new ArrayList<SystemObject>() : result;
    }

    @SuppressWarnings({"unchecked"})
    public static SyntheticId getSyntheticId(Identifiable identifiable) throws GenericSystemException {

        IdentifiableAccess access = (IdentifiableAccess) AccessFactory.getImplementation(identifiable.getClass());
        try {
            return access.getSyntheticId(identifiable);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Возвращает системную дату начала жизни объекта
     *
     * @return системная дата начала жизни объектов
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static Date getSystemFd() throws GenericSystemException {
        return fieldAccess.ACCESS.getSystemFd();
    }

    /**
     * Возвращает системную дату закрытия объекта
     *
     * @return системная дата начала закрытия объекта
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static Date getSystemTd() throws GenericSystemException {
        return fieldAccess.ACCESS.getSystemTd();
    }

    /**
     * Возвращает текущее  время в базе данных
     *
     * @return текущее время в базе данных
     * @throws GenericSystemException при ошибке получении INE_CORE_SYS.getCurrentDate
     */
    public static Date getCurrentDate() throws GenericSystemException {
        return fieldAccess.ACCESS.getCurrentDate();
    }

}
