/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.oss.inventory.item.PropertyDataLong;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertyDataLongAccessImpl.java 67 2017-05-19 09:55:27Z xerror $"
 */
public class PropertyDataLongAccessImpl<T extends PropertyDataLong> extends AbstractPropertyDataAccess<T> {


    /**
     * Таблица и префикс для обращения к ее столбцам
     */
    public static final String TABLE = "INV_PRPT_INT_DATA";
    public static final String TABLE_PREF = TABLE + DOT;

    /**
     * Запрос на выборку всех значений
     */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_ITEM_N +
            ", " + TABLE_PREF + COLUMN_DSC_ID +
            ", " + TABLE_PREF + COLUMN_VAL +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /**
     * Регион кэша для хранения данных типа {@link PropertyDataLong}
     */
    public static final String CACHE_REGION = PropertyDataLong.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(PropertyDataLong.ITEM_ID, COLUMN_ITEM_N);
        map.put(PropertyDataLong.DESCRIPTOR_ID, COLUMN_DSC_ID);
        map.put(PropertyDataLong.DATA, COLUMN_VAL);
        setInstanceFieldsMapping(PropertyDataLongAccessImpl.class, map);

        registerVersionableHelpers(PropertyDataLongAccessImpl.class, new LinkedHashSet<>());
    }


    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(getMainRegionKey(), getCommonSelect());
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(getMainRegionKey(), getCommonSelect(), id);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    protected void setData(ResultSet resultSet, String columnName, T object) throws SQLException {
        object.setData(resultSet.getLong(columnName));
    }

    @Override
    protected String getCommonSelect() {
        return ALL_DATA_SELECT;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return PropertyDataLong.class;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, getMainRegionKey(), ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, getMainRegionKey(), ALL_DATA_SELECT);
    }

}
