/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.oss.inventory.item;

import ru.xr.ine.core.*;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.oss.inventory.item.Item;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemAccessImpl.java 44 2017-04-11 08:00:31Z xerror $"
 */
public class ItemAccessImpl<T extends Item> extends AbstractVersionableAccess<T> implements ItemAccess<T> {

    /**
     * Таблица и префикс для обращения к ее столбцам
     */
    public static final String TABLE = "INVENTORY_ITEM";
    public static final String TABLE_PREF = TABLE + DOT;

    /**
     * Перечисление колонок таблицы
     */
    public static final String COLUMN_SELF_NAME = "SELF_NAME";
    public static final String COLUMN_TYPE = "typ";
    public static final String COLUMN_PARENT_ID = "parent_n";
    public static final String COLUMN_PROFILE_ID = "profile_n";
    public static final String COLUMN_STATUS = "status_n";

    /**
     * Запрос на выборку всех значений
     */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", " + TABLE_PREF + COLUMN_SELF_NAME +
            ", " + TABLE_PREF + COLUMN_TYPE +
            ", " + TABLE_PREF + COLUMN_PARENT_ID +
            ", " + TABLE_PREF + COLUMN_PROFILE_ID +
            ", " + TABLE_PREF + COLUMN_STATUS +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /**
     * Регион кэша для хранения данных типа {@link Item}
     */
    public static final String CACHE_REGION_ITEM = Item.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(Item.SELF_NAME, COLUMN_SELF_NAME);
        map.put(Item.ITEM_TYPE, COLUMN_TYPE);
        map.put(Item.PARENT_ID, COLUMN_PARENT_ID);
        map.put(Item.ITEM_PROFILE, COLUMN_PROFILE_ID);
        map.put(Item.STATUS, COLUMN_STATUS);
        setInstanceFieldsMapping(ItemAccessImpl.class, map);

        registerVersionableHelpers(ItemAccessImpl.class, new LinkedHashSet<>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_ITEM;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(Item.class);

        this.setVersionableFields(resultSet, value);

        value.setName(this.getString(resultSet, COLUMN_SELF_NAME));
        value.setType(this.getId(resultSet, COLUMN_TYPE));
        value.setParentId(this.getBigDecimal(resultSet, COLUMN_PARENT_ID));
        value.setItemProfile(this.getId(resultSet, COLUMN_PROFILE_ID));
        value.setStatus(this.getId(resultSet, COLUMN_STATUS));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(CACHE_REGION_ITEM, ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(CACHE_REGION_ITEM, ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, getMainRegionKey(), ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, getMainRegionKey(), ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Item.class;
    }
}
