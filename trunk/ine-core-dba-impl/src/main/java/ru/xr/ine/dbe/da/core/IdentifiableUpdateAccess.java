/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core;

import ru.xr.ine.core.CoreException;
import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.utils.cache.Cache;

import java.util.*;

/**
 * Утилитный класс, обеспечивает разгрузку основного {@link ru.xr.ine.dbe.da.core.AbstractIdentifiableAccess}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableUpdateAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
class IdentifiableUpdateAccess<T extends Identifiable> {

    /** Запросы на изменение и колонки по классам */
    private final static Map<String, OperationContainer> UPDATE_DATA_MAP = new HashMap<String, OperationContainer>();

    private AbstractIdentifiableAccess<T> access;

    //private static Logger logger = Logger.getLogger(IdentifiableUpdateAccess.class.getName());

    public IdentifiableUpdateAccess(AbstractIdentifiableAccess<T> access) {
        this.access = access;
    }

    /**
     * Оболочка для вызова при изменении объекта.
     *
     * @param identifiable    исходный объект
     * @param newIdentifiable модифицированный объект
     * @param region          имя региона кеша, хранящего данный объект
     * @return модифицированный объект
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке передать null
     * @throws ru.xr.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    protected T updateObject(T identifiable, T newIdentifiable, String region)
            throws IneIllegalArgumentException, GenericSystemException {

        try {
            this.access.beforeUpdate(identifiable, newIdentifiable);
            this.updateObjectInDB(newIdentifiable);
            this.access.afterUpdate(identifiable, newIdentifiable);
            Cache.put(region, this.access.getSyntheticId(newIdentifiable), newIdentifiable, false);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return newIdentifiable;
    }

    protected void updateObjectInDB(T t) throws IneIllegalArgumentException, GenericSystemException {
        OperationContainer container = obtainUpdateOperationContainer();
        this.access.updateObjectInDB(container.getSqlString(),
                this.access.getFieldValuesByColumns(t, container.getColumns()));
    }

    /**
     * Создает запрос к БД на модификацию
     *
     * @return строка создания
     */
    protected OperationContainer obtainUpdateOperationContainer() {

        synchronized (UPDATE_DATA_MAP) {
            Collection<String> columnNames = new ArrayList<String>(this.access.getColumnNames(true));

            // такая конструкция клюа - access.getClass().getName() + access.getTableName() - обусловлена тем,
            // что у CustomAttributeValueAccessImpl'а getTableName зависит от данных, изменяемых в данный момент.
            if (!UPDATE_DATA_MAP.containsKey(this.access.getClass().getName() + this.access.getTableName())) {
                StringBuilder stringBuilder = new StringBuilder(250);
                stringBuilder.append("UPDATE ").append(this.access.getTableName()).append(" SET ");

                for (String columnName : columnNames) {
                    stringBuilder.append(columnName).append(" = ?, ");
                }
                stringBuilder.delete(stringBuilder.length() - 2, stringBuilder.length());
                stringBuilder.append(" WHERE ").append(this.access.getIdConditionSql(""));

                columnNames.addAll(Arrays.asList(this.access.getIdColumns()));

                UPDATE_DATA_MAP.put(this.access.getClass().getName() + this.access.getTableName(),
                        new OperationContainer(stringBuilder.toString(), columnNames));
            }
        }

        return UPDATE_DATA_MAP.get(this.access.getClass().getName() + this.access.getTableName());
    }

}
