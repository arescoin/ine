/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.structure;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.IdentifiableFactory;
import ru.xr.ine.core.IneIllegalArgumentException;
import ru.xr.ine.core.structure.CustomAttribute;
import ru.xr.ine.core.structure.CustomAttributeValue;
import ru.xr.ine.core.structure.CustomType;
import ru.xr.ine.dbe.da.core.AccessFactory;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.*;

/**
 * Утилитный класс обеспечивающий доступ к мета-информации для настраиваемых полей объектов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: AttributesAccess.java 29 2017-04-04 15:32:19Z xerror $"
 */
@SuppressWarnings({"unchecked"})
public class AttributesAccess {


    private static CustomAttributeAccess<CustomAttribute> getCustomAttributeAccess() throws GenericSystemException {
        return (CustomAttributeAccess) AccessFactory.getImplementation(CustomAttribute.class);
    }

    private static CustomAttributeValueAccess getCustomAttrValAccess() throws GenericSystemException {
        return (CustomAttributeValueAccess) AccessFactory.getImplementation(CustomAttributeValue.class);
    }

    private static CustomTypeAccess getCustomTypeAccess() throws GenericSystemException {
        return (CustomTypeAccess) AccessFactory.getImplementation(CustomType.class);
    }

    /**
     * Возвращает коллекцию {@link ru.xr.ine.core.structure.CustomAttribute настраиваемых атрибутов}
     * для переданного класса Identifiable-объекта, дополненную атрибутами переданного
     * {@link ru.xr.ine.core.structure.CustomType модифицированного типа}.
     *
     * @param identifiableClass класс реализации Identifiable-объекта.
     * @param customType        модифицированный тип. Может быть <code>null</code>.
     *                          Тогда вернутся атрибуты только базового объекта
     * @return коллекция {@link ru.xr.ine.core.structure.CustomAttribute настраиваемых атрибутов}
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static Collection<CustomAttribute> getAttributes(Class identifiableClass, CustomType customType)
            throws GenericSystemException {

        AttributesAccess.checkCustomType(identifiableClass, customType);
        Collection<CustomAttribute> attributes =
                AttributesAccess.getCustomAttributeAccess().getAttributesByObject(identifiableClass);
        if (customType != null) {
            attributes.addAll(AttributesAccess.getCustomTypeAccess().getAllAttributes(customType));
        }

        return attributes;
    }

    /**
     * Получает значения {@link ru.xr.ine.core.structure.CustomAttribute произвольных атрибутов} переданного
     * объекта, расширенного заданным {@link ru.xr.ine.core.structure.CustomType модифицированным типом}.
     *
     * @param obj        переданный {@link ru.xr.ine.core.Identifiable объект}
     * @param customType модифицированный тип. Может быть <code>null</code>.
     *                   Тогда вернутся значения атрибутов только с базового объекта.
     * @return карта значений произвольных атрибутов объекта. ключом являются сами произвольные атрибуты.
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    public static Map<CustomAttribute, CustomAttributeValue> getValues(Identifiable obj, CustomType customType)
            throws GenericSystemException {

        AttributesAccess.checkCustomType(obj, customType);

        return AttributesAccess.getValues(obj, AttributesAccess.getAttributes(obj.getClass(), customType));
    }

    private static Map<CustomAttribute, CustomAttributeValue> getValues(Identifiable obj,
                                                                        Collection<CustomAttribute> attributes) throws GenericSystemException {

        Map<CustomAttribute, CustomAttributeValue> result =
                new HashMap<CustomAttribute, CustomAttributeValue>(attributes.size(), 1.1f);

        // Если коллекция атрибутов пустая, то и возвращаем пустую мапу
        if (attributes.isEmpty()) {
            return result;
        }

        // сначала сделаем мапу с описателями атрибутов для последующего формирования результирующей коллекции.
        Map<BigDecimal, CustomAttribute> attrsById = new HashMap<BigDecimal, CustomAttribute>(attributes.size(), 1.1f);
        for (CustomAttribute attribute : attributes) {
            attrsById.put(attribute.getCoreId(), attribute);
        }

        // формируем поисковый запрос на значения атрибутов
        SearchCriteriaProfile profile = AttributesAccess.getCustomAttrValAccess().getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<SearchCriterion>();
        SearchCriterion entId =
                profile.getSearchCriterion(CustomAttributeValue.ENT_N, CriteriaRule.equals, obj.getCoreId());
        entId.addAdditionalParam(IdentifiableFactory.getInterface(obj).getName());
        search.add(entId);
        search.add(profile.getSearchCriterion(
                CustomAttributeValue.CORE_ID, CriteriaRule.in, attrsById.keySet().toArray()));

        // ищем их
        Collection<CustomAttributeValue> values = AttributesAccess.getCustomAttrValAccess().searchObjects(search);

        // формируем выходную мапу
        for (CustomAttributeValue value : values) {
            result.put(attrsById.get(value.getCoreId()), value);
            attrsById.remove(value.getCoreId());
        }

        return result;
    }

    /**
     * Создает значения {@link ru.xr.ine.core.structure.CustomAttribute произвольных атрибутов} для переданного
     * объекта.
     *
     * @param obj        объект, для которого создаются значения произвольных атрибутов
     * @param customType кастом-тип, который расширяет переданный объект
     * @param values     создаваемые значения
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          в случае некорректных значений атрибутов. Значения должны
     *          принадлежать переданному объекту.
     */
    public static void createValues(Identifiable obj, CustomType customType, Collection<CustomAttributeValue> values)
            throws IneIllegalArgumentException, GenericSystemException {

        AttributesAccess.createValues(obj, customType, values, false);
    }

    /**
     * Создает значения {@link ru.xr.ine.core.structure.CustomAttribute произвольных атрибутов} для переданного
     * объекта при апдейте.
     *
     * @param obj        объект, для которого создаются значения произвольных атрибутов
     * @param customType кастом-тип, который расширяет переданный объект
     * @param values     создаваемые значения
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          в случае некорректных значений атрибутов. Значения должны
     *          принадлежать переданному объекту.
     */
    public static void createValuesOnUpdate(Identifiable obj, CustomType customType,
                                            Collection<CustomAttributeValue> values) throws IneIllegalArgumentException, GenericSystemException {

        AttributesAccess.createValues(obj, customType, values, true);
    }

    /**
     * Создает значения {@link ru.xr.ine.core.structure.CustomAttribute произвольных атрибутов} для переданного
     * объекта.
     *
     * @param obj               объект, для которого создаются значения произвольных атрибутов
     * @param customType        кастом-тип, который расширяет переданный объект
     * @param values            создаваемые значения
     * @param skipRequiredCheck пропустить проверку наличия значений обязательных атрибутов
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          в случае некорректных значений атрибутов. Значения должны
     *          принадлежать переданному объекту.
     */
    static void createValues(Identifiable obj, CustomType customType, Collection<CustomAttributeValue> values,
                             boolean skipRequiredCheck) throws IneIllegalArgumentException, GenericSystemException {

        AttributesAccess.checkCustomType(obj, customType);
        // сначала проверяем всех атрибуты: что они свои, что все обязательные заполнены
        Collection<CustomAttribute> attributes = AttributesAccess.checkValues(obj, customType, values, true);

        if (!skipRequiredCheck) {
            // проверяем, что не осталось обязательных атрибутов, которые не заполнены
            for (CustomAttribute attr : attributes) {
                if (attr.isRequired()) {
                    throw new IneIllegalArgumentException("Failed to create attributes values, cause attribute ["
                            + attr.getAttributeName() + "] is required.");
                }
            }
        }

        // все проверки прошли успешно, создаём атрибуты
        for (CustomAttributeValue value : values) {
            AttributesAccess.getCustomAttrValAccess().createObject(value);
        }
    }

    /**
     * Изменяет значения {@link ru.xr.ine.core.structure.CustomAttribute произвольных атрибутов} для переданного
     * объекта.
     *
     * @param obj        объект, для которого изменяются значения произвольных атрибутов
     * @param customType кастом-тип, который расширяет переданный объект
     * @param values     изменяемые значения
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          в случае некорректных значений атрибутов. Значения должны
     *          принадлежать переданному объекту.
     */
    public static void updateValues(Identifiable obj, CustomType customType, Collection<CustomAttributeValue> values)
            throws IneIllegalArgumentException, GenericSystemException {

        AttributesAccess.checkCustomType(obj, customType);
        // сначала проверяем всех атрибуты: что они свои
        AttributesAccess.checkValues(obj, customType, values, false);

        // проверяем, что значения существует.
        Map<CustomAttribute, CustomAttributeValue> savedValues = AttributesAccess.getValues(obj, customType);
        Map<BigDecimal, CustomAttribute> attributes = AttributesAccess.getAttributesByValues(values);

        CustomAttribute attribute;
        for (CustomAttributeValue value : values) {
            attribute = attributes.get(value.getCoreId());
            if (savedValues.get(attribute) == null) {
                throw new IneIllegalArgumentException("Failed to update attributes values, cause attribute ["
                        + attribute.getAttributeName() + "] doesn't have any value for " + obj.getClass().getName()
                        + " object with id[" + obj.getCoreId() + "]");
            }
        }

        // все проверки прошли успешно, изменяем атрибуты
        for (CustomAttributeValue value : values) {
            AttributesAccess.getCustomAttrValAccess().updateObject(
                    savedValues.get(attributes.get(value.getCoreId())), value);
        }
    }

    /**
     * Удаляет переданные значения {@link ru.xr.ine.core.structure.CustomAttribute произвольных атрибутов} для
     * указанного объекта.
     *
     * @param obj        объект, для которого удаляются значения произвольных атрибутов
     * @param customType кастом-тип, который расширяет переданный объект
     * @param values     удаляемые значения
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке удаления обязательного атрибута или в случае
     *          некорректных значений атрибутов. Значения должны принадлежать переданному объекту.
     */
    public static void deleteValues(Identifiable obj, CustomType customType, Collection<CustomAttributeValue> values)
            throws IneIllegalArgumentException, GenericSystemException {

        AttributesAccess.checkCustomType(obj, customType);
        AttributesAccess.checkValues(obj, customType, values, false);
        AttributesAccess.deleteValues(values, false);
    }

    /**
     * Удаляет переданные значения {@link ru.xr.ine.core.structure.CustomAttribute произвольных атрибутов} для
     * указанного объекта.
     *
     * @param obj               объект, для которого удаляются значения произвольных атрибутов
     * @param customType        кастом-тип, который расширяет переданный объект
     * @param values            удаляемые значения
     * @param skipRequiredCheck признак необходимости проверки удаляемого значения на обязательность.
     *                          <code>true</code> - проверка не осуществляется, иначе при попытке удаления
     *                          обязательного атрибута выкинется ошибка
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке удаления обязательного атрибута или в случае
     *          некорректных значений атрибутов. Значения должны принадлежать переданному объекту.
     */
    public static void deleteValues(Identifiable obj, CustomType customType, Collection<CustomAttributeValue> values,
                                    boolean skipRequiredCheck) throws IneIllegalArgumentException, GenericSystemException {

        AttributesAccess.checkCustomType(obj, customType);
        AttributesAccess.checkValues(obj, customType, values, false);
        AttributesAccess.deleteValues(values, skipRequiredCheck);
    }

    /**
     * Утилитный метод, для удаления переданных значений {@link ru.xr.ine.core.structure.CustomAttribute произвольных
     * атрибутов}. Проверка обязательности атрибута регулируется параметром skipRequiredCheck.
     *
     * @param values            удаляемые значения
     * @param skipRequiredCheck признак необходимости проверки удаляемого значения на обязательность.
     *                          <code>true</code> - проверка не осуществляется, иначе при попытке удаления
     *                          обязательного атрибута выкинется ошибка
     * @throws ru.xr.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информации
     * @throws ru.xr.ine.core.IneIllegalArgumentException
     *          при попытке удаления обязательного атрибута без выставления
     *          соответствующего флага
     */
    private static void deleteValues(Collection<CustomAttributeValue> values, boolean skipRequiredCheck)
            throws IneIllegalArgumentException, GenericSystemException {

        Map<BigDecimal, CustomAttribute> attributes = new HashMap<BigDecimal, CustomAttribute>();
        if (!skipRequiredCheck) {
            attributes = AttributesAccess.getAttributesByValues(values);
        }

        CustomAttribute attribute;
        for (CustomAttributeValue value : values) {
            if (!skipRequiredCheck) {
                attribute = attributes.get(value.getCoreId());
                if (attribute.isRequired()) {
                    throw new IneIllegalArgumentException(
                            "Can't delete required attribute [" + attribute.getAttributeName() + "].");
                }
            }

            AttributesAccess.getCustomAttrValAccess().deleteObject(value);
        }
    }

    /* Получаем все описатели атрибутов по их значениям за одно обращение к кешу */
    private static Map<BigDecimal, CustomAttribute> getAttributesByValues(Collection<CustomAttributeValue> values)
            throws GenericSystemException {

        // формируем поисковый запрос
        SearchCriteriaProfile profile = AttributesAccess.getCustomAttributeAccess().getSearchCriteriaProfile();
        Set<SearchCriterion> criteria = new LinkedHashSet<SearchCriterion>();
        SearchCriterion<BigDecimal> coreId = profile.getSearchCriterion(CustomAttribute.CORE_ID);
        coreId.setCriterionRule(CriteriaRule.in);

        for (CustomAttributeValue value : values) {
            coreId.addCriteriaVal(value.getCoreId());
        }
        criteria.add(coreId);

        // ищем и заполняем выходную мапу
        Map<BigDecimal, CustomAttribute> result = new HashMap<BigDecimal, CustomAttribute>();
        for (CustomAttribute attribute : AttributesAccess.getCustomAttributeAccess().searchObjects(criteria)) {
            result.put(attribute.getCoreId(), attribute);
        }

        return result;
    }

    private static void checkCustomType(Identifiable obj, CustomType customType) throws GenericSystemException {
        if (customType != null) {
            AttributesAccess.checkCustomType(IdentifiableFactory.getInterface(obj), customType);
        }
    }

    private static void checkCustomType(Class<? extends Identifiable> identifiableClass, CustomType customType)
            throws GenericSystemException {

        if (customType == null) {
            return;
        }

        BigDecimal tableId = FieldAccess.getTableId(identifiableClass);
        if (!customType.getBaseObject().equals(tableId)) {
            throw new IneIllegalArgumentException("customType[" + customType.getCoreId() +
                    "] does not extends object " + identifiableClass.getSimpleName() + ", tableId[" + tableId + "]");
        }
    }

    private static Collection<CustomAttribute> checkValues(Identifiable obj,
                                                           CustomType customType, Collection<CustomAttributeValue> values, boolean removeChecked)
            throws IneIllegalArgumentException, GenericSystemException {

        // Сначала проверяем коллекцию на null-значения.
        for (CustomAttributeValue value : values) {
            // null-значения вообще не поддаются обработке, такое недопустимо
            if (value == null) {
                throw new IneIllegalArgumentException("null attribute value is not expected");
            }
        }

        // Нужно получить коллекцию всех описателей атрибутов объекта, с учётом его кастом-типа
        Collection<CustomAttribute> allAttributes = AttributesAccess.getAttributes(obj.getClass(), customType);
        // Получаем мапу описателей для переданных значений
        Map<BigDecimal, CustomAttribute> attributes = AttributesAccess.getAttributesByValues(values);

        // проверяем атрибуты на принадлежность переданному объекту
        CustomAttribute attribute;
        for (CustomAttributeValue value : values) {
            attribute = attributes.get(value.getCoreId());
            if (!allAttributes.contains(attribute) || !value.getEntityId().equals(obj.getCoreId())) {
                throw new IneIllegalArgumentException(
                        "Unexpected attribute found, that does not belong to specified object");
            }
            //удаляем проверенный атрибут из коллекции, если требуется
            if (removeChecked) {
                allAttributes.remove(attribute);
            }
        }

        return allAttributes;
    }

}
