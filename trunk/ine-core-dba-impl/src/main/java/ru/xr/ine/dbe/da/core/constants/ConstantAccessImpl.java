/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.dbe.da.core.constants;

import ru.xr.ine.core.*;
import ru.xr.ine.core.constants.Constant;
import ru.xr.ine.dbe.da.core.AbstractVersionableAccess;
import ru.xr.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantAccessImpl.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class ConstantAccessImpl<T extends Constant> extends AbstractVersionableAccess<T> implements ConstantAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "CONSTANTS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_CONST_NAME = "const_name";
    //    public static final String COLUMN_TYP = "typ";
    public static final String COLUMN_DEF_VAL = "def_val";
    public static final String COLUMN_IS_NULLABLE = "is_nullable";
    public static final String COLUMN_IS_MODYFABLE = "is_modyfable";

    /** Запрос на выборку */
    public static final String CONSTANTS_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_CONST_NAME +
//            ", " + TABLE_PREF + COLUMN_TYP +
            ", " + TABLE_PREF + COLUMN_DEF_VAL +
            ", " + TABLE_PREF + COLUMN_IS_NULLABLE +
            ", " + TABLE_PREF + COLUMN_IS_MODYFABLE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.xr.ine.core.constants.Constant} */
    public static final String CACHE_REGION_CONSTANTS = Constant.class.getName();

    static {
        setInstanceFieldsMapping(ConstantAccessImpl.class, new HashMap<String, String>());
        registerConstantHelpers(ConstantAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(Constant.NAME, COLUMN_CONST_NAME);
//        map.put(Constant.TYPE, COLUMN_TYP);
        map.put(Constant.DEFAULT_VALUE, COLUMN_DEF_VAL);
        map.put(Constant.NULLABLE, COLUMN_IS_NULLABLE);
        map.put(Constant.MODIFABLE, COLUMN_IS_MODYFABLE);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerConstantHelpers(Class<? extends ConstantAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<CRUDHelper>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    @Override
    public String getTableName() {
        return ConstantAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_CONSTANTS;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(Constant.class);

        this.setVersionableFields(resultSet, result);

        result.setName(this.getString(resultSet, COLUMN_CONST_NAME));
//        result.setType(this.getId(resultSet, COLUMN_TYP));
        result.setDefaultValue(this.getString(resultSet, COLUMN_DEF_VAL));
        result.setNullable(this.getBoolean(resultSet, COLUMN_IS_NULLABLE));
        result.setModifiable(this.getBoolean(resultSet, COLUMN_IS_MODYFABLE));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_CONSTANTS, CONSTANTS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_CONSTANTS, CONSTANTS_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_CONSTANTS, CONSTANTS_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_CONSTANTS);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_CONSTANTS, CONSTANTS_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Constant.class;
    }

}
