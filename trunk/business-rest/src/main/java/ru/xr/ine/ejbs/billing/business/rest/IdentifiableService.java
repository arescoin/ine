/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.billing.business.rest;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.Identifiable;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.ejbs.core.IdentifiableBean;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
public abstract class IdentifiableService<T extends Identifiable> {

    private static final Logger logger = Logger.getLogger(IdentifiableService.class.getName());


    protected abstract IdentifiableBean<T> getBean();


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getById")
    @GET
    public Response getById(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    getBean().getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    @Path("/all")
    public Response getAll() {

        try {
            Collection<T> objects = getBean().getAllObjects(UserHolder.getUser());
            Map<BigDecimal, T> itemHashMap = new HashMap<>();

            for (T identifiable : objects) {
                itemHashMap.put(identifiable.getCoreId(), identifiable);
            }

            return SrvUtils.createMapResponse(itemHashMap);
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    protected Response getByNameFieldEq(String fieldName, CriteriaRule rule, Object value) {
        try {
            SearchCriteriaProfile criteriaProfile = getBean().getSearchCriteriaProfile(UserHolder.getUser());
            Set<SearchCriterion> criteria = new LinkedHashSet<>();
            criteria.add(criteriaProfile.getSearchCriterion(fieldName, rule, value));

            return SrvUtils.createObjectResponse(getBean().searchObjects(UserHolder.getUser(), criteria));
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
