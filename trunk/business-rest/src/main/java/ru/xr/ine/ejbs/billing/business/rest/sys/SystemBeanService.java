/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.billing.business.rest.sys;

import ru.xr.ine.ejbs.billing.business.rest.SrvUtils;
import ru.xr.ine.ejbs.billing.business.rest.oss.DataPointProfileService;
import ru.xr.ine.ejbs.core.SystemBean;
import ru.xr.ine.ejbs.core.userpermits.RoleBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemBeanService.java 67 2017-05-19 09:55:27Z xerror $"
 */
@Path("/sysSrv")
@Stateless
public class SystemBeanService {

    private static final Logger logger = Logger.getLogger(DataPointProfileService.class.getName());


    @EJB(beanName = "SystemBean", mappedName = "stateless.SystemBean", name = "SystemBean")
    SystemBean systemBean;

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getState")
    @GET
    public Response getState() {
        try {
            return SrvUtils.createObjectResponse(systemBean.getState().toString());
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/initSystem")
    @GET
    public Response start() {
        try {
            systemBean.startSystem();
            return SrvUtils.createObjectResponse("fire");
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getEvents")
    @GET
    public Response getEvents() {
        try {
            systemBean.startSystem();
            return SrvUtils.createObjectResponse("fire");
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return javax.ws.rs.core.Response.status(javax.ws.rs.core.Response.Status.NOT_FOUND).build();
        }
    }






}
