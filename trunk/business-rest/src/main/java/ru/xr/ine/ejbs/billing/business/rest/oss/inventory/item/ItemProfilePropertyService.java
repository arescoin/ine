/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.billing.business.rest.oss.inventory.item;

import ru.xr.ine.core.UserHolder;
import ru.xr.ine.ejbs.billing.business.rest.SrvUtils;
import ru.xr.ine.ejbs.oss.inventory.item.ItemBean;
import ru.xr.ine.ejbs.oss.inventory.item.ItemProfilePropertyBean;
import ru.xr.ine.oss.inventory.item.Item;
import ru.xr.ine.oss.inventory.item.ItemProfileProperty;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemProfilePropertyService.java 45 2017-04-11 16:52:40Z xerror $"
 */
@Path("/inventory/ItemProfileProperty")
@Stateless
public class ItemProfilePropertyService {

    private static final Logger logger = Logger.getLogger(ItemProfilePropertyService.class.getName());

    @EJB(mappedName = "stateless.ItemProfilePropertyBean", name = "ItemProfilePropertyBean")
    ItemProfilePropertyBean<ItemProfileProperty> itemProfilePropertyBean;

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getById")
    @GET
    public Response getById(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    itemProfilePropertyBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
