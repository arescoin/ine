/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.billing.business.rest.oss;

import ru.xr.ine.core.UserHolder;
import ru.xr.ine.ejbs.billing.business.rest.SrvUtils;
import ru.xr.ine.ejbs.oss.entity.DataPointProfileBean;
import ru.xr.ine.oss.entity.DataPointProfile;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DataPointProfileService.java 29 2017-04-04 15:32:19Z xerror $"
 */
@Path("/dataProfile")
@Stateless
public class DataPointProfileService {

    private static final Logger logger = Logger.getLogger(DataPointProfileService.class.getName());


    @EJB(mappedName = "stateless.DataPointProfileBean", name = "DataPointProfileBean")
    DataPointProfileBean<DataPointProfile> pointProfileBean;

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getAll")
    @GET
    public Response getById() {

        try {
            return SrvUtils.createObjectResponse(pointProfileBean.getAllObjects(UserHolder.getUser()));
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
