package ru.xr.ine.ejbs.billing.business.rest.auth;

import ru.xr.ine.core.GenericSystemException;
import ru.xr.ine.core.UserHolder;
import ru.xr.ine.core.UserProfile;
import ru.xr.ine.core.userpermits.SystemUser;
import ru.xr.ine.ejbs.core.userpermits.SystemUserBean;
import ru.xr.ine.utils.MD5;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: APIFilter.java 46 2017-04-13 16:05:04Z xerror $"
 */
@Stateless
public class APIFilter implements Filter {

    private static final Logger logger = Logger.getLogger(APIFilter.class.getName());

    public static final String COOKIE_KEY = "REST_SRV_AUTH_KEY";

    public static final Map<String, SessionHolder> sessionMap = new HashMap<>();

    public static final long LIVE_TIME = 600000L;

    private static long lastUpdateTime = 1;

    @EJB
    SystemUserBean<SystemUser> systemUserBean;


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        long reqTime = System.currentTimeMillis();
        long updateTime = reqTime - lastUpdateTime;
        String rHost = request.getRemoteHost();

        updateMap(reqTime, updateTime);

        String cookieVal = getCookie((HttpServletRequest) request);

        if (cookieVal != null) {
            SessionHolder sessionHolder = sessionMap.get(cookieVal);

            if (sessionHolder != null) {

                if (sessionHolder.getIpAddress().equals(rHost)) {
                    long time = reqTime - sessionHolder.getLastTime();
                    if (time < LIVE_TIME) {
                        sessionHolder.setLastTime(reqTime);
                        UserHolder.setUserProfile(sessionHolder.getUserProfile());
                        chain.doFilter(request, response);
                        return;
                    }
                }
            }
        }

        String method = ((HttpServletRequest) request).getMethod();
        String path = ((HttpServletRequest) request).getPathInfo();

        if ("POST".equals(method) && ("/authService".equals(path))) {
            String auth = ((HttpServletRequest) request).getHeader("authorization");
            if (null != auth && !auth.isEmpty()) {

                String[] lap = decode(auth);
                if (null == lap || null == lap[0] || null == lap[1] || lap[0].isEmpty() || lap[1].isEmpty()) {
                    ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                } else {
                    String login = lap[0];
                    String pwd = lap[1];
                    try {
                        UserHolder.setUser(systemUserBean.login(login, MD5.calculate(pwd)));
                        String key = reqTime + ":" + UserHolder.getUser().hashCode();
                        try {
                            key = MD5.calculate(key);
                            SessionHolder sessionHolder = new SessionHolder();
                            sessionHolder.setLastTime(reqTime);
                            sessionHolder.setUserProfile(UserHolder.getUser());
                            sessionHolder.setIpAddress(rHost);
                            synchronized (sessionMap) {
                                sessionMap.put(key, sessionHolder);
                            }

                            ((HttpServletResponse) response).addCookie(new Cookie(COOKIE_KEY, key));
                            chain.doFilter(request, response);

                        } catch (NoSuchAlgorithmException e) {
                            logger.warning(e.getMessage());
                            ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                        }
                    } catch (GenericSystemException | NoSuchAlgorithmException e) {
                        ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    }
                }
            } else {
                ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } else {
            ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private String getCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String cookieVal = null;
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals(COOKIE_KEY)) {
                    cookieVal = cookie.getValue();
                    break;
                }
            }
        }

        return cookieVal;
    }

    private void updateMap(long reqTime, long updateTime) {
        if (updateTime > LIVE_TIME) {
            lastUpdateTime = reqTime;
            updateMap(reqTime);
        }
    }

    private void updateMap(long reqTime) {

        Collection<String> toRemove = new ArrayList<>();
        for (String s : sessionMap.keySet()) {
            SessionHolder sessionHolder = sessionMap.get(s);
            if (reqTime - sessionHolder.getLastTime() > LIVE_TIME) {
                toRemove.add(s);
            }
        }

        synchronized (sessionMap) {
            for (String s : toRemove) {
                sessionMap.remove(s);
            }
        }
    }

    @Override
    public void destroy() {
    }

    private String[] decode(String auth) {
        //Replacing "Basic THE_BASE_64" to "THE_BASE_64" directly
        auth = auth.replaceFirst("[B|b]asic ", "");

        //Decode the Base64 into byte[]
        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(auth);

        //If the decode fails in any case
        if (decodedBytes == null || decodedBytes.length == 0) {
            return null;
        }

        //Now we can convert the byte[] into a splitted array :
        //  - the first one is login,
        //  - the second one password
        return new String(decodedBytes).split(":", 2);
    }

    private static class SessionHolder {
        private UserProfile userProfile;
        private String ipAddress;
        private long lastTime;

        private UserProfile getUserProfile() {
            return userProfile;
        }

        private void setUserProfile(UserProfile userProfile) {
            this.userProfile = userProfile;
        }

        private String getIpAddress() {
            return ipAddress;
        }

        private void setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
        }

        private long getLastTime() {
            return lastTime;
        }

        private void setLastTime(long lastTime) {
            this.lastTime = lastTime;
        }
    }

}
