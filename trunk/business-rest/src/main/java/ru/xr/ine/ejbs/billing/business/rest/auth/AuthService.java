package ru.xr.ine.ejbs.billing.business.rest.auth;

import ru.xr.ine.core.UserHolder;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AuthService.java 46 2017-04-13 16:05:04Z xerror $"
 */
@Path("/authService")
@Stateless
public class AuthService {

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response getById() {
        return Response.status(Response.Status.OK).
                entity("Logged in as [" + UserHolder.getUser().getSystemUser().getLogin() + "]").build();
    }
}
