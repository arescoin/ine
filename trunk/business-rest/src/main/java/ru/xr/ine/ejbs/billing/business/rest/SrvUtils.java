package ru.xr.ine.ejbs.billing.business.rest;

import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: SrvUtils.java 29 2017-04-04 15:32:19Z xerror $"
 */
public class SrvUtils {

    public static Response createObjectResponse(Object o) {

        if (null != o) {
            return Response.status(Response.Status.OK).entity(o).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    public static Response createCollectionResponse(Collection collection) {

        if (null != collection && !collection.isEmpty()) {
            return Response.status(Response.Status.OK).entity(collection).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    public static Response createMapResponse(Map map) {

        if (null != map && !map.isEmpty()) {
            return Response.status(Response.Status.OK).entity(map).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
