/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.billing.business.rest.oss.inventory.item;

import ru.xr.ine.core.UserHolder;
import ru.xr.ine.ejbs.billing.business.rest.IdentifiableService;
import ru.xr.ine.ejbs.billing.business.rest.SrvUtils;
import ru.xr.ine.ejbs.core.IdentifiableBean;
import ru.xr.ine.ejbs.oss.inventory.item.ItemBean;
import ru.xr.ine.oss.inventory.item.Item;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;
import ru.xr.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.xr.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ItemService.java 46 2017-04-13 16:05:04Z xerror $"
 */
@Path("/inventory/item")
@Stateless
public class ItemService extends IdentifiableService<Item> {

    private static final Logger logger = Logger.getLogger(ItemService.class.getName());

    @EJB(mappedName = "stateless.ItemBean", name = "ItemBean")
    ItemBean<Item> itemBean;


//    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
//    @GET
//    @Path("/all")
//    public Response getAll() {
//
//        try {
//            Collection<Item> dictionaries = itemBean.getAllObjects(UserHolder.getUser());
//            Map<BigDecimal, Item> itemHashMap = new HashMap<>();
//
//            for (Item item : dictionaries) {
//                itemHashMap.put(item.getCoreId(), item);
//            }
//
//            return SrvUtils.createMapResponse(itemHashMap);
//        } catch (GenericSystemException e) {
//            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
//        }
//    }

    @Override
    protected IdentifiableBean<Item> getBean() {
        return itemBean;
    }

//    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
//    @Path("/getById")
//    @GET
//    public Response getById(@QueryParam("id") Long id) {
//
//        try {
//            return SrvUtils.createObjectResponse(
//                    itemBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
//        } catch (Exception e) {
//            logger.log(Level.WARNING, e.getMessage(), e);
//            return Response.status(Response.Status.NOT_FOUND).build();
//        }
//    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getByName")
    @GET
    public Response getByName(@QueryParam("name") String name) {

        try {
//            SearchCriteriaProfile criteriaProfile = itemBean.getSearchCriteriaProfile(UserHolder.getUser());
//            Set<SearchCriterion> criteria = new LinkedHashSet<>();
//            criteria.add(criteriaProfile.getSearchCriterion(Item.SELF_NAME, CriteriaRule.equals, name));

            return this.getByNameFieldEq(Item.SELF_NAME, CriteriaRule.equals, name);

//            return SrvUtils.createObjectResponse(itemBean.searchObjects(UserHolder.getUser(), criteria));
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
