/*
 * Copyright (c) 2017.  Denis Gomon, xerrorster@gmail.com. Free for ALL. No warranty
 */

package ru.xr.ine.ejbs.billing.business.rest.oss.inventory.item;

import ru.xr.ine.core.structure.DataType;
import ru.xr.ine.ejbs.billing.business.rest.IdentifiableService;
import ru.xr.ine.ejbs.core.IdentifiableBean;
import ru.xr.ine.ejbs.oss.inventory.item.PropertyDescriptorBean;
import ru.xr.ine.oss.inventory.item.PropertyDescriptor;
import ru.xr.ine.utils.searchCriteria.CriteriaRule;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id$"
 */
@Path("/inventory/PropertyDescriptor")
@Stateless
public class PropertyDescriptorService extends IdentifiableService<PropertyDescriptor> {

    private static final Logger logger = Logger.getLogger(PropertyDescriptor.class.getName());

    @EJB(mappedName = "stateless.PropertyDescriptorBean", name = "PropertyDescriptorBean")
    PropertyDescriptorBean<PropertyDescriptor> propertyDescriptorBean;

    @Override
    protected IdentifiableBean<PropertyDescriptor> getBean() {
        return propertyDescriptorBean;
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getByName")
    @GET
    public Response getByName(@QueryParam("name") String name) {
        try {
            return this.getByNameFieldEq(PropertyDescriptor.NAME, CriteriaRule.equals, name);
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getByType")
    @GET
    public Response getByType(@QueryParam("type") String typeName) {
        try {
            DataType dataType = DataType.valueOf(typeName);
            return this.getByNameFieldEq(PropertyDescriptor.TYPE, CriteriaRule.equals, dataType);
        } catch (Exception e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


}
