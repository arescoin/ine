@echo off

SETLOCAL

chcp 65001

set START_ARG=start
set STOP_ARG=stop
set TIME_OUT=1
set ARG=%1%
set CACHE_PATH=%2%
set RIMS_API_PATH=%3%
set RIMS_COMMON_PATH=%4%
set SERVER_PATH=%5%
set _COMPILE_CPATH=%6%
set COMPILE_CPATH=%_COMPILE_CPATH:"=%
set _SECURITY=%7%
set SECURITY=%_SECURITY:"=%
set _INDEX=%8%
set INDEX=%_INDEX:"=%
set _PROPS_FILE=%9%
set PROPS_FILE=%_PROPS_FILE:"=%

set CLASS_PATH=%CACHE_PATH%;%RIMS_API_PATH%;%RIMS_COMMON_PATH%;%SERVER_PATH%;%COMPILE_CPATH%

set DEBUG_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005

if "%ARG%"=="" (
  echo "Argument is not specified"
  goto :do_Help
) else  (
  if "%ARG%"=="%START_ARG%" ( 
    goto :do_Start
  ) else (
    if "%ARG%"=="%STOP_ARG%" (
      goto :do_Stop
    ) else (
      echo "Unknown argument" %ARG%
      goto :do_Help
    )
  )
)

:do_Start
  echo "do start"
  echo "Params:"  %CLASS_PATH%
  echo "Arguments:"  %PROPS_FILE%
  START /B java -Xms512m -Xmx1G %DEBUG_OPTS% %SECURITY% %INDEX% -classpath %CLASS_PATH% ru.xr.rims.server.RMSServer %PROPS_FILE% 1>System.out.log 2>&1
  goto :end

:do_Stop
  echo "Do stop"
  echo "Params:"  %CLASS_PATH%
  echo "Arguments:"  %PROPS_FILE%

  START /B java -classpath %CLASS_PATH% ru.xr.rims.server.admin.Killer %PROPS_FILE%
  goto :end

:do_Help
  echo "Use [start|stop] arguments"
  goto :end

:end
ENDLOCAL
