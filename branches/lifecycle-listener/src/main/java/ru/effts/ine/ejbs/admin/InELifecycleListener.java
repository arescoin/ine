/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.admin;

import com.sun.appserv.server.LifecycleEvent;
import com.sun.appserv.server.LifecycleListener;
import com.sun.appserv.server.ServerLifecycleException;

import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: InELifecycleListener.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class InELifecycleListener implements LifecycleListener {

    private int startupTimer = 10;
    private boolean runOnStartup = true;

    private static final Logger logger = Logger.getLogger(InELifecycleListener.class.getName());

    @Override
    public void handleEvent(LifecycleEvent event) throws ServerLifecycleException {

        setProperties(event);

        if (runOnStartup) {


            if (event.getEventType() == LifecycleEvent.READY_EVENT) {

                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "Perform scheduling preparation");
                }

                final InitialContext ctx = event.getLifecycleEventContext().getInitialContext();

                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        InELifecycleListener.scheduleMessage(ctx);
                    }
                }, startupTimer * 1000);

                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "Preparation task scheduled [{0}] sek", startupTimer);
                }
            }
        } else {
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "Startup processing is switched off");
            }
        }
    }

    private void setProperties(LifecycleEvent event) {
        Object eventData = event.getData();

        if (eventData instanceof Properties) {
            Properties properties = (Properties) eventData;
            try {
                startupTimer = Integer.parseInt(properties.getProperty("startupTimer", startupTimer + ""));
            } catch (NumberFormatException e) {
                if (logger.isLoggable(Level.WARNING)) {
                    logger.log(Level.WARNING, "Error while apply startupTimer", e);
                }
            }

            runOnStartup = Boolean.parseBoolean(properties.getProperty("runOnStartup", runOnStartup + ""));
        }
    }

    private static void scheduleMessage(InitialContext ctx) {
        try {

            final Queue queue = (Queue) ctx.lookup("IneStartupQueue");
            final QueueConnectionFactory queueCF = (QueueConnectionFactory) ctx.lookup("jms/InEQueueCF");

            QueueConnection queueConnection = queueCF.createQueueConnection();
            queueConnection.start();
            QueueSession queueSession = queueConnection.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);

            QueueSender queueSender = queueSession.createSender(queue);
            queueSender.send(queueSession.createTextMessage("System.Start.LCL"));

            queueSender.close();
            queueSession.close();
            queueConnection.close();

        } catch (NamingException e) {
            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, "Error while context obtain", e);
            }
        } catch (JMSException e) {
            if (logger.isLoggable(Level.WARNING)) {
                logger.log(Level.WARNING, "Error while JMS processing", e);
            }
        }
    }

    public int getStartupTimer() {
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Ask startupTimer [{0}]", this.startupTimer);
        }

        return this.startupTimer;
    }

    public void setStartupTimer(int startupTimer) {
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Set startupTimer [{0}]", startupTimer);
        }

        this.startupTimer = startupTimer;
    }

    public boolean isRunOnStartup() {
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Ask runOnStartup [{0}]", this.runOnStartup);
        }

        return this.runOnStartup;
    }

    public void setRunOnStartup(boolean runOnStartup) {
        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "Set runOnStartup [{0}]", runOnStartup);
        }

        this.runOnStartup = runOnStartup;
    }
}
