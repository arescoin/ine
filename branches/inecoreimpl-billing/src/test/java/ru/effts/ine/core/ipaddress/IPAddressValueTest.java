/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.ipaddress;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.net.UnknownHostException;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueTest.java 3219 2011-11-21 13:48:26Z ikulkov $"
 */
public class IPAddressValueTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return IPAddressValue.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            IdentifiableFactory.getImplementation(IPAddressValue.class).setIpAddress(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        try {
            IPAddress address = IPAddress.parse("127.0.0.1");

            IPAddressValue value = (IPAddressValue) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            value.setIpAddress(address);
            Assert.assertSame("Set operation failed.", address, value.getIpAddress());
        } catch (UnknownHostException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
