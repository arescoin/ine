package ru.effts.ine.core.structure;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
@Ignore
public class CustomAttributeTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return CustomAttribute.class;
    }

    @Test(expected = IllegalIdException.class)
    public void testNullSystemObjectId() {
        try {
            IdentifiableFactory.getImplementation(CustomAttribute.class).setSystemObjectId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testWrongSystemObjectId() {
        try {
            IdentifiableFactory.getImplementation(CustomAttribute.class).setSystemObjectId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullDataType() {
        try {
            IdentifiableFactory.getImplementation(CustomAttribute.class).setDataType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullAttributeName() {
        try {
            IdentifiableFactory.getImplementation(CustomAttribute.class).setAttributeName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyAttributeName() {
        try {
            IdentifiableFactory.getImplementation(CustomAttribute.class).setAttributeName("  ");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testWrongPattern() {
        try {
            IdentifiableFactory.getImplementation(CustomAttribute.class).setPattern(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String attributeName = "Test name";
        String attributeNameWS = "  Test name  ";
        BigDecimal systemObjectId = Identifiable.MIN_ALLOWABLE_VAL;
        DataType dataType = DataType.bigDecimalType;
        boolean required = false;
        SystemObjectPattern pattern = new SystemObjectPattern(BigDecimal.ONE);
        String limitations = null;

        try {
            CustomAttribute value = (CustomAttribute) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            value.setAttributeName(attributeName);
            Assert.assertSame("Set operation failed.", attributeName, value.getAttributeName());

            value.setAttributeName(attributeNameWS);
            Assert.assertEquals("Set operation failed.", attributeName, value.getAttributeName());

            value.setSystemObjectId(systemObjectId);
            Assert.assertSame("Set operation failed.", systemObjectId, value.getSystemObjectId());

            value.setDataType(dataType);
            Assert.assertSame("Set operation failed.", dataType, value.getDataType());

            value.setRequired(required);
            Assert.assertSame("Set operation failed.", required, value.isRequired());

            value.setPattern(pattern);
            Assert.assertSame("Set operation failed.", pattern, value.getPattern());

            value.setLimitations(limitations);
            Assert.assertSame("Set operation failed.", limitations, value.getLimitations());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
