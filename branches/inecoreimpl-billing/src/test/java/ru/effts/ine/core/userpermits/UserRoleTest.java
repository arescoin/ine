package ru.effts.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleTest.java 932 2010-11-10 15:55:40Z ikulkov $"
 */
public class UserRoleTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return UserRole.class;
    }

    @Test(expected = IllegalIdException.class)
    public void testNullUserId() {
        try {
            IdentifiableFactory.getImplementation(UserRole.class).setUserId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroUserId() {
        try {
            IdentifiableFactory.getImplementation(UserRole.class).setUserId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeUserId() {
        try {
            IdentifiableFactory.getImplementation(UserRole.class).setUserId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        UserRole value1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        UserRole value2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() == value2.hashCode());
        Assert.assertTrue("Equals operation failed.", value1.equals(value2));

        UserRole value3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        value3.setCoreId(value1.getCoreId().add(value2.getCoreId()));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() != value3.hashCode());
        Assert.assertTrue("Equals operation failed.", !value1.equals(value3));
    }

    protected UserRole fillBasicFields(Identifiable identifiable) {
        UserRole value = (UserRole) super.fillBasicFields(identifiable);
        value.setUserId(Identifiable.MIN_ALLOWABLE_VAL);
        return value;
    }

    @Test
    public void testSetFields() {
        BigDecimal userId = Identifiable.MIN_ALLOWABLE_VAL;
        boolean actrive = true;
        try {
            UserRole userRole = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            userRole.setUserId(userId);
            Assert.assertSame("Set operation failed.", userId, userRole.getUserId());

            userRole.setActive(actrive);
            Assert.assertSame("Set operation failed.", actrive, userRole.isActive());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
