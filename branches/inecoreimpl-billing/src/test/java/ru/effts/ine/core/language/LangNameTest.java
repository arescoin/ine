package ru.effts.ine.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangNameTest.java 622 2010-09-07 14:49:47Z ikulkov $"
 */
public class LangNameTest extends IdentifiableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return LangName.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            IdentifiableFactory.getImplementation(LangName.class).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            IdentifiableFactory.getImplementation(LangName.class).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "Test name";
        String nameWS = "  Test name  ";
        try {
            LangName langName = (LangName) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            langName.setName(name);
            Assert.assertSame("Set operation failed.", name, langName.getName());

            langName.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, langName.getName());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
