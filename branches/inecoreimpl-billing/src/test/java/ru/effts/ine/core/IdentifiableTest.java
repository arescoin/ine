/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * Заготовка для тестов идентифицируемых объектов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableTest.java 3835 2014-06-04 15:12:43Z DGomon $"
 */
public abstract class IdentifiableTest extends BaseTest {

    protected Class<? extends Identifiable> idClass;

    /**
     * Тестовые классы должны реализовывать данный метод, возвращая класс тестируемого интерфейса
     *
     * @return класс тестируемого интерфейса
     */
    protected abstract Class<? extends Identifiable> getClassType();

    @Before
    public void setClass() {
        idClass = getClassType();
    }

    protected Identifiable getInstance() throws GenericSystemException {
        return IdentifiableFactory.getImplementation(idClass);
    }

    /**
     * Метод заполняет основные атрибуты тестируемого объекта
     *
     * @param identifiable инстанс тестиреумого класса
     *
     * @return ссылка на инстанс тестируемого класса
     */
    protected Identifiable fillBasicFields(Identifiable identifiable) {
        identifiable.setCoreId(Identifiable.MIN_ALLOWABLE_VAL);
        identifiable.setCoreDsc("Test description");
        return identifiable;
    }

    /** Общая проверка на корректность обработки пустого значения в идентификаторе */
    @Test(expected = IllegalIdException.class)
    public void testNullId() {
        try {
            IdentifiableFactory.getImplementation(idClass).setCoreId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    /** Общая проверка на корректность обработки 0 */
    @Test(expected = IllegalIdException.class)
    public void testZeroId() {
        try {
            IdentifiableFactory.getImplementation(idClass).setCoreId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    /** Общая проверка на корректность обработки идентификатора < 0 */
    @Test(expected = IllegalIdException.class)
    public void testNegativeId() {
        try {
            IdentifiableFactory.getImplementation(idClass).setCoreId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetId() throws GenericSystemException {
        long id = 1;

        Identifiable identifiable = IdentifiableFactory.getImplementation(idClass);
        identifiable.setCoreId(new BigDecimal(id));

        Assert.assertTrue("Id operation failed.", identifiable.getCoreId().longValue() == id);
    }

    @Test
    public void testDsc() throws GenericSystemException {
        String testDsc = "Test Description";

        Identifiable identifiable = IdentifiableFactory.getImplementation(idClass);
        identifiable.setCoreDsc(testDsc);

        Assert.assertEquals("Description operation failed.", identifiable.getCoreDsc(), testDsc);
    }

    @Test
    public void testHashAndEquals() throws GenericSystemException {
        Identifiable identifiable1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        Identifiable identifiable2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

        Identifiable identifiable3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        identifiable3.setCoreId(identifiable1.getCoreId().add(identifiable2.getCoreId()));

        Assert.assertTrue("HashCode operation failed. Class: " + idClass.getName(),
                identifiable1.hashCode() == identifiable2.hashCode());
        Assert.assertTrue("Equals operation failed. Class: " + idClass.getName(),
                identifiable1.equals(identifiable2));
        Assert.assertTrue("Equals operation failed.  Class: " + idClass.getName(),
                !identifiable1.equals(identifiable3));
    }
}
