package ru.effts.ine.oss.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PersonTest.java 481 2010-08-02 12:21:14Z sfilatov $"
 */
public class PersonTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Person.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullFamilyName() {
        try {
            ((Person) IdentifiableFactory.getImplementation(idClass)).setFamilyName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyFamilyName() {
        try {
            ((Person) IdentifiableFactory.getImplementation(idClass)).setFamilyName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((Person) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((Person) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullMiddleName() {
        try {
            ((Person) IdentifiableFactory.getImplementation(idClass)).setMiddleName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyMiddleName() {
        try {
            ((Person) IdentifiableFactory.getImplementation(idClass)).setMiddleName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullFamilyNamePrefix() {
        try {
            ((Person) IdentifiableFactory.getImplementation(idClass)).setFamilyNamePrefixCode(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        try {
            String familyName = "Test Family Name";
            String name = "Test Name";
            String middleName = "Test Middle Name";
            String alias = "Test Alias";
            BigDecimal familyNamePrefix = BigDecimal.ONE;
            String familyGeneration = "Test Family Generation";
            String formOfAddress = "Test Form of Address";

            Person person = (Person) IdentifiableFactory.getImplementation(idClass);

            person.setFamilyName(familyName);
            Assert.assertSame("Set operation failed.", familyName, person.getFamilyName());

            person.setName(name);
            Assert.assertSame("Set operation failed.", name, person.getName());

            person.setMiddleName(middleName);
            Assert.assertSame("Set operation failed.", middleName, person.getMiddleName());

            person.setAlias(alias);
            Assert.assertSame("Set operation failed.", alias, person.getAlias());

            person.setFamilyNamePrefixCode(familyNamePrefix);
            Assert.assertSame("Set operation failed.", familyNamePrefix, person.getFamilyNamePrefixCode());

            person.setFamilyGeneration(familyGeneration);
            Assert.assertSame("Set operation failed.", familyGeneration, person.getFamilyGeneration());

            person.setFormOfAddress(formOfAddress);
            Assert.assertSame("Set operation failed.", formOfAddress, person.getFormOfAddress());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
