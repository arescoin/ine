package ru.effts.ine.core;

import org.junit.Before;
import ru.effts.ine.utils.BaseTest;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemMarkerTest.java 455 2010-07-27 16:16:50Z DGomon $"
 */
public abstract class SystemMarkerTest extends BaseTest{

    protected Class<? extends SystemMarker> idClass;

    protected abstract Class<? extends SystemMarker> getClassType();

    @Before
    public void setClass() {
        idClass = getClassType();
    }

}
