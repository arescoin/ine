package ru.effts.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RoleTest.java 422 2010-07-19 07:46:10Z sfilatov $"
 */
public class RoleTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Role.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((Role) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((Role) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "Test name";
        String nameWS = "  Test name  ";

        try {
            Role role = (Role) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            role.setName(name);
            Assert.assertSame("Set operation failed.", name, role.getName());

            role.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, role.getName());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
