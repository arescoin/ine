package ru.effts.ine.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.VersionableTest;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserTest.java 422 2010-07-19 07:46:10Z sfilatov $"
 */
public class SystemUserTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return SystemUser.class;
    }

    @Test
    public void testSetFields() {
        boolean active = true;

        try {
            SystemUser systemUser = (SystemUser) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            systemUser.setActive(active);
            Assert.assertSame("Set operation failed.", active, systemUser.isActive());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
