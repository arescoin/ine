package ru.effts.ine.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsTest.java 631 2010-09-08 12:49:30Z ikulkov $"
 */
public class LangDetailsTest extends IdentifiableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return LangDetails.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable details = IdentifiableFactory.getImplementation(idClass);
        details.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", details.getCoreDsc(), null);
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            IdentifiableFactory.getImplementation(LangDetails.class).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            IdentifiableFactory.getImplementation(LangDetails.class).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullCode() {
        try {
            IdentifiableFactory.getImplementation(LangDetails.class).setCode(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyCode() {
        try {
            IdentifiableFactory.getImplementation(LangDetails.class).setCode("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testLength1Code() {
        try {
            IdentifiableFactory.getImplementation(LangDetails.class).setCode("r");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testLength3Code() {
        try {
            IdentifiableFactory.getImplementation(LangDetails.class).setCode("rus");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "Test name";
        String nameWS = "  Test name  ";
        String code = "ru";
        String codeWS = "  ru  ";
        try {
            LangDetails details = (LangDetails) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            details.setName(name);
            Assert.assertSame("Set operation failed.", name, details.getName());

            details.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, details.getName());

            details.setCode(code);
            Assert.assertSame("Set operation failed.", code, details.getCode());

            details.setCode(codeWS);
            Assert.assertEquals("Set operation failed.", code, details.getCode());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
