package ru.effts.ine.core.structure;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
@Ignore
public class CustomAttributeDefValTest extends VersionableTest{
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return CustomAttributeDefVal.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable identifiable = IdentifiableFactory.getImplementation(idClass);
        identifiable.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", identifiable.getCoreDsc(), null);
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testGetValueByWrongType() {
        try {
            IdentifiableFactory.getImplementation(CustomAttributeDefVal.class).getValueByType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String valueString = "TestValue";
        Long valueLong = (long) 1;
        BigDecimal valueBigDecimal = new BigDecimal("2.2");
        Boolean valueBoolean = Boolean.TRUE;
        Date valueDate = new Date();
        DataType type;

        try {
            CustomAttributeDefVal value = (CustomAttributeDefVal)
                    fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            value.setString(valueString);
            Assert.assertSame("Set operation failed.", valueString, value.getString());
            Assert.assertNull("Set operation failed.", value.getLong());
            Assert.assertNull("Set operation failed.", value.getBigDecimal());
            Assert.assertNull("Set operation failed.", value.getBoolean());
            Assert.assertNull("Set operation failed.", value.getDate());

            value.setLong(valueLong);
            Assert.assertSame("Set operation failed.", valueLong, value.getLong());
            Assert.assertNull("Set operation failed.", value.getString());
            Assert.assertNull("Set operation failed.", value.getBigDecimal());
            Assert.assertNull("Set operation failed.", value.getBoolean());
            Assert.assertNull("Set operation failed.", value.getDate());

            value.setBigDecimal(valueBigDecimal);
            Assert.assertSame("Set operation failed.", valueBigDecimal, value.getBigDecimal());
            Assert.assertNull("Set operation failed.", value.getString());
            Assert.assertNull("Set operation failed.", value.getLong());
            Assert.assertNull("Set operation failed.", value.getBoolean());
            Assert.assertNull("Set operation failed.", value.getDate());

            value.setBoolean(valueBoolean);
            Assert.assertSame("Set operation failed.", valueBoolean, value.getBoolean());
            Assert.assertNull("Set operation failed.", value.getString());
            Assert.assertNull("Set operation failed.", value.getLong());
            Assert.assertNull("Set operation failed.", value.getBigDecimal());
            Assert.assertNull("Set operation failed.", value.getDate());

            value.setDate(valueDate);
            Assert.assertSame("Set operation failed.", valueDate, value.getDate());
            Assert.assertNull("Set operation failed.", value.getString());
            Assert.assertNull("Set operation failed.", value.getLong());
            Assert.assertNull("Set operation failed.", value.getBigDecimal());
            Assert.assertNull("Set operation failed.", value.getBoolean());

            type = DataType.stringType;
            value.setValueByType(valueString, type);
            Assert.assertSame("Set operation failed.", valueString, value.getString());
            Assert.assertSame("Get operation failed.", valueString, value.getValueByType(type));

            type = DataType.longType;
            value.setValueByType(valueLong, type);
            Assert.assertSame("Set operation failed.", valueLong, value.getLong());
            Assert.assertSame("Get operation failed.", valueLong, value.getValueByType(type));

            type = DataType.bigDecimalType;
            value.setValueByType(valueBigDecimal, type);
            Assert.assertSame("Set operation failed.", valueBigDecimal, value.getBigDecimal());
            Assert.assertSame("Get operation failed.", valueBigDecimal, value.getValueByType(type));

            type = DataType.booleanType;
            value.setValueByType(valueBoolean, type);
            Assert.assertEquals("Set operation failed.", valueBoolean, value.getBoolean());
            Assert.assertEquals("Get operation failed.", valueBoolean, value.getValueByType(type));

            type = DataType.dateType;
            value.setValueByType(valueDate, type);
            Assert.assertSame("Set operation failed.", valueDate, value.getDate());
            Assert.assertSame("Get operation failed.", valueDate, value.getValueByType(type));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
