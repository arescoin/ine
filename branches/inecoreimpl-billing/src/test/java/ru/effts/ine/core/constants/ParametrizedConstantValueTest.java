package ru.effts.ine.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueTest.java 932 2010-11-10 15:55:40Z ikulkov $"
 */
public class ParametrizedConstantValueTest extends ConstantValueTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ParametrizedConstantValue.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullParam() {
        try {
            ((ParametrizedConstantValue) IdentifiableFactory.getImplementation(idClass)).
                    setParam(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        super.testSetFields();
        try {
            BigDecimal param = new BigDecimal(1);

            ParametrizedConstantValue constantValue = (ParametrizedConstantValue)
                    IdentifiableFactory.getImplementation(idClass);

            constantValue.setParam(param);
            Assert.assertSame("Set operation failed.", param, constantValue.getParam());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        ParametrizedConstantValue value1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        ParametrizedConstantValue value2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() == value2.hashCode());
        Assert.assertTrue("Equals operation failed.", value1.equals(value2));

        ParametrizedConstantValue value3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        value3.setCoreId(value1.getCoreId().add(value2.getCoreId()));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() != value3.hashCode());
        Assert.assertTrue("Equals operation failed.", !value1.equals(value3));
    }

    protected ParametrizedConstantValue fillBasicFields(Identifiable identifiable) {
        ParametrizedConstantValue value = (ParametrizedConstantValue) super.fillBasicFields(identifiable);
        value.setParam(new BigDecimal(1));
        return value;
    }
}
