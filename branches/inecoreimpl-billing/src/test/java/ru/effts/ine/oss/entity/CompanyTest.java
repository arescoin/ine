package ru.effts.ine.oss.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CompanyTest.java 475 2010-07-30 13:30:08Z ikulkov $"
 */
public class CompanyTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Company.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((Company) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((Company) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullType() {
        try {
            ((Company) IdentifiableFactory.getImplementation(idClass)).setType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullPropertyType() {
        try {
            ((Company) IdentifiableFactory.getImplementation(idClass)).setPropertyType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        try {
            String name = "Test Company";
            BigDecimal type = BigDecimal.ONE;
            BigDecimal propertyType = BigDecimal.ONE;

            Company company = (Company) IdentifiableFactory.getImplementation(idClass);

            company.setName(name);
            Assert.assertSame("Set operation failed.", name, company.getName());

            company.setType(type);
            Assert.assertSame("Set operation failed.", type, company.getType());

            company.setPropertyType(propertyType);
            Assert.assertSame("Set operation failed.", propertyType, company.getPropertyType());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
