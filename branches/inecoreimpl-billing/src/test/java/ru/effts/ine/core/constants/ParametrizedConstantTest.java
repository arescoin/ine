package ru.effts.ine.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantTest.java 1056 2010-11-29 15:32:40Z ikulkov $"
 */
public class ParametrizedConstantTest extends ConstantTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ParametrizedConstant.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullMask() {
        try {
            ((ParametrizedConstant) IdentifiableFactory.getImplementation(idClass)).setMask(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        super.testSetFields();
        try {
            SystemObjectPattern mask = new SystemObjectPattern(BigDecimal.ONE);

            ParametrizedConstant constant = (ParametrizedConstant) IdentifiableFactory.getImplementation(idClass);

            constant.setMask(mask);
            Assert.assertSame("Set operation failed.", mask, constant.getMask());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
