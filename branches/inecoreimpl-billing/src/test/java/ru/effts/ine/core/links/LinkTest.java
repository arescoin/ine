package ru.effts.ine.core.links;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkTest.java 828 2010-10-20 14:45:09Z ikulkov $"
 */
public class LinkTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Link.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable details = IdentifiableFactory.getImplementation(idClass);
        details.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", details.getCoreDsc(), null);
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullLeftType() {
        try {
            IdentifiableFactory.getImplementation(Link.class).setLeftType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullRightType() {
        try {
            IdentifiableFactory.getImplementation(Link.class).setRightType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullTypeId() {
        try {
            IdentifiableFactory.getImplementation(Link.class).setTypeId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        SystemObjectPattern leftType = new SystemObjectPattern(BigDecimal.ONE);
        SystemObjectPattern rightType = new SystemObjectPattern(BigDecimal.ONE);
        BigDecimal typeId = new BigDecimal(1);
        try {
            Link link = (Link) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            link.setLeftType(leftType);
            Assert.assertSame("Set operation failed.", leftType, link.getLeftType());

            link.setRightType(rightType);
            Assert.assertSame("Set operation failed.", rightType, link.getRightType());

            link.setTypeId(typeId);
            Assert.assertSame("Set operation failed.", typeId, link.getTypeId());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
