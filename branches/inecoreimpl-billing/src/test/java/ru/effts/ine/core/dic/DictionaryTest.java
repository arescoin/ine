package ru.effts.ine.core.dic;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTest.java 227 2010-05-17 11:10:05Z ikulkov $"
 */
public class DictionaryTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Dictionary.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ((Dictionary) IdentifiableFactory.getImplementation(idClass)).setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ((Dictionary) IdentifiableFactory.getImplementation(idClass)).setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullDicType() {
        try {
            ((Dictionary) IdentifiableFactory.getImplementation(idClass)).setDicType(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullEntryCodePolicy() {
        try {
            ((Dictionary) IdentifiableFactory.getImplementation(idClass)).setEntryCodePolicy(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullLocalizationPolicy() {
        try {
            ((Dictionary) IdentifiableFactory.getImplementation(idClass)).setLocalizationPolicy(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        String name = "TestDictionary";
        String nameWS = "  TestDictionary  ";
        BigDecimal dicType = new BigDecimal(1);
        BigDecimal entryCodePolicy = new BigDecimal(1);
        BigDecimal localizationPolicy = new BigDecimal(1);
        BigDecimal parentId = new BigDecimal(1);

        try {
            Dictionary dictionary = (Dictionary) IdentifiableFactory.getImplementation(idClass);

            dictionary.setName(name);
            Assert.assertSame("Set operation failed.", name, dictionary.getName());

            dictionary.setName(nameWS);
            Assert.assertEquals("Set operation failed.", name, dictionary.getName());

            dictionary.setDicType(dicType);
            Assert.assertSame("Set operation failed.", dicType, dictionary.getDicType());

            dictionary.setEntryCodePolicy(entryCodePolicy);
            Assert.assertSame("Set operation failed.", entryCodePolicy, dictionary.getEntryCodePolicy());

            dictionary.setLocalizationPolicy(localizationPolicy);
            Assert.assertSame("Set operation failed.", localizationPolicy, dictionary.getLocalizationPolicy());

            dictionary.setParentId(parentId);
            Assert.assertSame("Set operation failed.", parentId, dictionary.getParentId());

            dictionary.setParentId(null);
            Assert.assertSame("Set operation failed.", null, dictionary.getParentId());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
