package ru.effts.ine.core.structure;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */

@Ignore
public class CustomAttributeValueTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return CustomAttributeValue.class;
    }

    @Test(expected = IllegalIdException.class)
    public void testNullEntityId() {
        try {
            IdentifiableFactory.getImplementation(CustomAttributeValue.class).setEntityId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testWrongEntityId() {
        try {
            IdentifiableFactory.getImplementation(CustomAttributeValue.class).setEntityId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    public void testHashAndEquals() throws GenericSystemException {
        CustomAttributeValue value1 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        CustomAttributeValue value2 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() == value2.hashCode());
        Assert.assertTrue("Equals operation failed.", value1.equals(value2));

        CustomAttributeValue value3 = fillBasicFields(IdentifiableFactory.getImplementation(idClass));
        value3.setCoreId(value1.getCoreId().add(value2.getCoreId()));

        Assert.assertTrue("HashCode operation failed.", value1.hashCode() != value3.hashCode());
        Assert.assertTrue("Equals operation failed.", !value1.equals(value3));
    }

    protected CustomAttributeValue fillBasicFields(Identifiable identifiable) {
        CustomAttributeValue value = (CustomAttributeValue) super.fillBasicFields(identifiable);
        value.setEntityId(Identifiable.MIN_ALLOWABLE_VAL);
        return value;
    }

    @Test
    public void testSetFields() {
        BigDecimal entityId = Identifiable.MIN_ALLOWABLE_VAL;
        String value = "Test value";

        try {
            CustomAttributeValue entry = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            entry.setEntityId(entityId);
            Assert.assertSame("Set operation failed.", entityId, entry.getEntityId());

            entry.setValue(value);
            Assert.assertSame("Set operation failed.", value, entry.getValue());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
