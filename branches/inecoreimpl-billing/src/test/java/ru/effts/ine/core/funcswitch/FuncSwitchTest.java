package ru.effts.ine.core.funcswitch;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchTest.java 631 2010-09-08 12:49:30Z ikulkov $"
 */
public class FuncSwitchTest extends VersionableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return FuncSwitch.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable details = IdentifiableFactory.getImplementation(idClass);
        details.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", details.getCoreDsc(), null);
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullChangeDate() {
        try {
            IdentifiableFactory.getImplementation(FuncSwitch.class).setChangeDate(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        boolean enabled = true;
        Date changeDate = new Date();
        BigDecimal mark = new BigDecimal(1);
        String stateCode = "Test code";

        try {
            FuncSwitch funcSwitch = (FuncSwitch) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            funcSwitch.setEnabled(enabled);
            Assert.assertSame("Set operation failed.", enabled, funcSwitch.isEnabled());

            funcSwitch.setChangeDate(changeDate);
            Assert.assertSame("Set operation failed.", changeDate, funcSwitch.getChangeDate());

            funcSwitch.setMark(mark);
            Assert.assertSame("Set operation failed.", mark, funcSwitch.getMark());

            funcSwitch.setStateCode(stateCode);
            Assert.assertSame("Set operation failed.", stateCode, funcSwitch.getStateCode());

            funcSwitch.setMark(null);
            Assert.assertSame("Set operation failed.", null, funcSwitch.getMark());

            funcSwitch.setStateCode(null);
            Assert.assertSame("Set operation failed.", null, funcSwitch.getStateCode());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
