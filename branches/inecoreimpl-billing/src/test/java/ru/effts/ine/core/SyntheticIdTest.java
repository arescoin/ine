/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SyntheticIdTest.java 720 2010-09-24 14:51:00Z ikulkov $"
 */
public class SyntheticIdTest {

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullKey() {
        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(null, new BigDecimal(1));
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullValue() {
        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(new BigDecimal(1), null);
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testSameKey() {
        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(new BigDecimal(1), new BigDecimal(1));
        syntheticId.addIdEntry(new BigDecimal(1), new BigDecimal(2));
    }

    @Test
    public void testEqualsAndHashCode() {

        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(new BigDecimal(1), new BigDecimal(1));
        syntheticId.addIdEntry(new BigDecimal(2), new BigDecimal(3));
        syntheticId.addIdEntry(new BigDecimal(3), new BigDecimal(4));


        SyntheticId syntheticId2 = new SyntheticId();
        syntheticId2.addIdEntry(new BigDecimal(1), new BigDecimal(1));
        syntheticId2.addIdEntry(new BigDecimal(3), new BigDecimal(4));
        syntheticId2.addIdEntry(new BigDecimal(2), new BigDecimal(3));

        Assert.assertTrue(syntheticId.equals(syntheticId2));
        Assert.assertEquals(syntheticId.hashCode(), syntheticId2.hashCode());
    }

    @Test
    public void testToStringAndParse() {
        SyntheticId syntheticId1 = new SyntheticId();
        syntheticId1.addIdEntry(new BigDecimal(1), new BigDecimal(1));
        syntheticId1.addIdEntry(new BigDecimal(2), new BigDecimal(2));
        syntheticId1.addIdEntry(new BigDecimal(3), new BigDecimal(3));

        Assert.assertEquals("[1:1][2:2][3:3]", syntheticId1.toString());

        SyntheticId syntheticId2 = SyntheticId.parseString(syntheticId1.toString());
        Assert.assertEquals(syntheticId1, syntheticId2);
    }
}
