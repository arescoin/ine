package ru.effts.ine.core.dic;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermTest.java 1092 2010-12-09 10:55:20Z ikulkov $"
 */
public class DictionaryTermTest extends DictionaryEntryTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return DictionaryTerm.class;
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullLangCode() {
        try {
            IdentifiableFactory.getImplementation(DictionaryTerm.class).setLangCode(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullTerm() {
        try {
            IdentifiableFactory.getImplementation(DictionaryTerm.class).setTerm(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyTerm() {
        try {
            IdentifiableFactory.getImplementation(DictionaryTerm.class).setTerm("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    protected DictionaryTerm fillBasicFields(Identifiable identifiable) {
        DictionaryTerm value = (DictionaryTerm) super.fillBasicFields(identifiable);
        value.setLangCode(Identifiable.MIN_ALLOWABLE_VAL);
        return value;
    }

    @Test
    public void testSetFields() {
        BigDecimal langCode = Identifiable.MIN_ALLOWABLE_VAL;
        String term = "Test dictionary term";
        String termWS = "  Test dictionary term  ";

        try {
            DictionaryTerm entry = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            entry.setLangCode(langCode);
            Assert.assertSame("Set operation failed.", langCode, entry.getLangCode());

            entry.setTerm(term);
            Assert.assertSame("Set operation failed.", term, entry.getTerm());

            entry.setTerm(termWS);
            Assert.assertEquals("Set operation failed.", term, entry.getTerm());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
