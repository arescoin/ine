/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventTest.java 1045 2010-11-26 09:46:10Z dgomon $"
 */
public class HistoryEventTest extends VersionableHistoryTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return HistoryEvent.class;
    }

    @Test
    public void testProcessed() {
        try {
            HistoryEvent historyEvent = (HistoryEvent) IdentifiableFactory.getImplementation(idClass);

            historyEvent.setProcessed(true);
            Assert.assertTrue(historyEvent.isProcessed());

            historyEvent.setProcessed(false);
            Assert.assertFalse(historyEvent.isProcessed());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
