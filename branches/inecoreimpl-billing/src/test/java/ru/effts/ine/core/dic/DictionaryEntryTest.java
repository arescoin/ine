package ru.effts.ine.core.dic;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryTest.java 1092 2010-12-09 10:55:20Z ikulkov $"
 */
public class DictionaryEntryTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return DictionaryEntry.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable details = IdentifiableFactory.getImplementation(idClass);
        details.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", details.getCoreDsc(), null);
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullDictionaryId() {
        try {
            IdentifiableFactory.getImplementation(DictionaryEntry.class).setDictionaryId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Override
    protected DictionaryEntry fillBasicFields(Identifiable identifiable) {
        DictionaryEntry value = (DictionaryEntry) super.fillBasicFields(identifiable);
        value.setDictionaryId(Identifiable.MIN_ALLOWABLE_VAL);
        return value;
    }

    @Test
    public void testSetFields() {
        BigDecimal dictionaryId = Identifiable.MIN_ALLOWABLE_VAL;
        BigDecimal parentTermId = Identifiable.MIN_ALLOWABLE_VAL;

        try {
            DictionaryEntry entry = fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            entry.setDictionaryId(dictionaryId);
            Assert.assertSame("Set operation failed.", dictionaryId, entry.getDictionaryId());

            entry.setParentTermId(parentTermId);
            Assert.assertSame("Set operation failed.", parentTermId, entry.getParentTermId());

            entry.setParentTermId(null);
            Assert.assertSame("Set operation failed.", null, entry.getParentTermId());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
