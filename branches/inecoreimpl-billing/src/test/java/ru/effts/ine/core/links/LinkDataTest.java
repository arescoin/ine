package ru.effts.ine.core.links;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataTest.java 631 2010-09-08 12:49:30Z ikulkov $"
 */
public class LinkDataTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return LinkData.class;
    }

    @Test
    public void testDsc() throws GenericSystemException {
        Identifiable details = IdentifiableFactory.getImplementation(idClass);
        details.setCoreDsc("Test Description");
        Assert.assertEquals("Description operation failed.", details.getCoreDsc(), null);
    }

    @Test(expected = IllegalIdException.class)
    public void testNullLinkId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setLinkId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroLinkId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setLinkId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeLinkId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setLinkId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullLeftId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setLeftId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroLeftId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setLeftId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeLeftId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setLeftId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullRightId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setRightId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroRightId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setRightId(new BigDecimal(0));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegativeRightId() {
        try {
            IdentifiableFactory.getImplementation(LinkData.class).setRightId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {
        BigDecimal linkId = new BigDecimal(1);
        BigDecimal leftId = new BigDecimal(1);
        BigDecimal rightId = new BigDecimal(1);
        try {
            LinkData linkData = (LinkData) fillBasicFields(IdentifiableFactory.getImplementation(idClass));

            linkData.setLinkId(linkId);
            Assert.assertSame("Set operation failed.", linkId, linkData.getLinkId());

            linkData.setLeftId(leftId);
            Assert.assertSame("Set operation failed.", leftId, linkData.getLeftId());

            linkData.setRightId(rightId);
            Assert.assertSame("Set operation failed.", rightId, linkData.getRightId());
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }
}
