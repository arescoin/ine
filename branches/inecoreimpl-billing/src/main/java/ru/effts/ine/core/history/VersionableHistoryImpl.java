/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.util.Date;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryImpl.java 3392 2011-12-01 14:27:21Z dgomon $"
 */
public class VersionableHistoryImpl extends IdentifiableHistoryImpl implements VersionableHistory {

    /** Дата открытия версии */
    private Date td;

    /** Дата закрыти версии */
    private Date fd;


    @Override
    public void setFd(Date fd) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkNull(fd, VersionableHistory.FD);
        this.fd = fd;
        this.check();
    }

    @Override
    public Date getFd() {
        return this.fd;
    }

    @Override
    public void setTd(Date td) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkNull(td, VersionableHistory.TD);
        this.td = td;
        check();
    }

    @Override
    public Date getTd() {
        return this.td;
    }

    private void check() {
        if ((this.getFd() != null) && (this.getTd() != null) && this.getFd().after(this.getTd())) {
            throw new IneIllegalArgumentException("From date can't be after ToDate", VersionableHistory.TD);
        }
    }

    @Override
    public boolean equals(Object o) {

        boolean result = false;

        if (this == o) {
            result = true;
        } else if ((this.getFd() != null) && (this.getTd() != null)
                && (o instanceof VersionableHistory) && super.equals(o)) {

            VersionableHistoryImpl that = (VersionableHistoryImpl) o;
            if (this.getFd().equals(that.getFd()) && this.getTd().equals(that.getTd())) {
                result = true;
            }
        }

        return result;
    }

    @Override
    public int hashCode() {

        int result = super.hashCode();
        result = 31 * result + (this.getFd() != null ? this.getFd().hashCode() : 0);
        result = 31 * result + (this.getTd() != null ? this.getTd().hashCode() : 0);

        return result;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + VersionableHistory.FD + "[" + this.getFd() + "], "
                + VersionableHistory.TD + "[" + this.getTd() + "]";
    }

}
