/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueImpl.java 3400 2011-12-02 10:28:35Z dgomon $"
 */
public class CustomAttributeValueImpl extends AbstractVersionable implements CustomAttributeValue {

    /** Идентификатор экземпляра {@link ru.effts.ine.core.Versionable объекта} к которому привязан данный атрибут. */
    private BigDecimal entityId;

    /** Значение атрибута */
    private Object value;


    @Override
    public BigDecimal getEntityId() {
        return entityId;
    }

    @Override
    public void setEntityId(BigDecimal entityId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(entityId, CustomAttributeValue.ENT_N);

        this.entityId = entityId;
        this.setHashCode();
    }

    @Override
    public Object getValue() {
        return this.value;
    }

    @Override
    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof CustomAttributeValueImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        CustomAttributeValueImpl that = (CustomAttributeValueImpl) o;

        //noinspection RedundantIfStatement
        if (entityId != null ? !entityId.equals(that.entityId) : that.entityId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.getEntityId() != null ? this.getEntityId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + CustomAttributeValue.ENT_N + '[' + this.getEntityId() + "], "
                + CustomAttributeValue.VALUE + '[' + this.getValue() + ']';
    }

}
