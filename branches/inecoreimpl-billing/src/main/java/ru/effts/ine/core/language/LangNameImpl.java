/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.language;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangNameImpl.java 3395 2011-12-02 08:27:06Z dgomon $"
 */
public class LangNameImpl extends AbstractIdentifiable implements LangName {

    /** Системное название языка */
    private String name;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, LangName.NAME);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + LangName.NAME + "[" + this.getName() + "]";
    }

}
