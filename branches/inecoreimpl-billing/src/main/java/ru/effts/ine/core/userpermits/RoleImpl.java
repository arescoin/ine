/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RoleImpl.java 3403 2011-12-02 12:24:56Z dgomon $"
 */
public class RoleImpl extends AbstractVersionable implements Role {

    /** Название роли */
    private String name;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Role.NAME);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + Role.NAME + '[' + this.getName() + ']';
    }

}
