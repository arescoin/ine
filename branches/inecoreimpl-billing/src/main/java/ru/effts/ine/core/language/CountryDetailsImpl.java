/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.language;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsImpl.java 3395 2011-12-02 08:27:06Z dgomon $"
 */
public class CountryDetailsImpl extends AbstractIdentifiable implements CountryDetails {

    /** Название страны (iso3166) */
    private String name;

    /** Код страны (два символа iso3166) */
    private String codeA2;

    /** Код страны (три символа iso3166) */
    private String codeA3;


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, CountryDetails.NAME);
    }

    @Override
    public String getCodeA2() {
        return this.codeA2;
    }

    @Override
    public void setCodeA2(String a2) throws IneIllegalArgumentException {
        a2 = AbstractIdentifiable.checkEmpty(a2, CountryDetails.CODE_A2);

        if (a2.length() != 2) {
            throw new IneIllegalArgumentException("Length of " + CountryDetails.CODE_A2
                    + " must be only 2 symbols.", CountryDetails.CODE_A2);
        }

        this.codeA2 = a2;
    }

    @Override
    public String getCodeA3() {
        return this.codeA3;
    }

    @Override
    public void setCodeA3(String a3) throws IneIllegalArgumentException {
        a3 = AbstractIdentifiable.checkEmpty(a3, CountryDetails.CODE_A3);

        if (a3.length() != 3) {
            throw new IneIllegalArgumentException("Length of " + CountryDetails.CODE_A3
                    + " must be only 3 symbols.", CountryDetails.CODE_A3);
        }

        this.codeA3 = a3;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + CountryDetails.NAME + '[' + name + ']' + ", "
                + CountryDetails.CODE_A2 + '[' + codeA2 + ']' + ", "
                + CountryDetails.CODE_A3 + '[' + codeA3 + ']';
    }

}
