/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure.test;

import ru.effts.ine.core.Versionable;

/**
 * Интерфейс для тестирования функциональности дополнительных атрибутов. Работает с таблицей TEST_TABLE.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: TestObject.java 3398 2011-12-02 08:58:53Z dgomon $"
 */
public interface TestObject extends Versionable {

    String VALUE = "value";

    /**
     * Получает тестовое поле.
     *
     * @return значение тестового поля.
     */
    String getValue();

    /**
     * Устанавливает тестовое поле
     *
     * @param value значение тестового поля
     */
    void setValue(String value);
}
