/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.funcswitch;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameImpl.java 3390 2011-12-01 13:04:55Z dgomon $"
 */
public class FuncSwitchNameImpl extends AbstractIdentifiable implements FuncSwitchName {

    /** Наименование переключателя */
    private String name;


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, FuncSwitchName.NAME);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + FuncSwitchName.NAME + "['" + name + "'] ";
    }

}
