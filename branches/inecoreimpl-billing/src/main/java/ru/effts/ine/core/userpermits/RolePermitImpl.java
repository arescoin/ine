/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitImpl.java 3402 2011-12-02 12:09:51Z dgomon $"
 */
public class RolePermitImpl extends AbstractVersionable implements RolePermit {

    /** Идентификатор доступа включенного в данную роль */
    private BigDecimal permitId;

    /** Значение доступа включенного в данную роль */
    private BigDecimal[] permitValues;

    private int crudMask;


    @Override
    public BigDecimal getPermitId() {
        return this.permitId;
    }

    @Override
    public void setPermitId(BigDecimal permitId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(permitId, RolePermit.PERMIT_ID);
        this.permitId = permitId;
    }

    @Override
    public BigDecimal[] getPermitValues() {
        return this.permitValues;
    }

    @Override
    public void setPermitValues(BigDecimal[] permitValues) {
        this.permitValues = permitValues;
    }

    @Override
    public int getCrudMask() {
        return this.crudMask;
    }

    @Override
    public void setCrudMask(int crudMask) {

        int[] ints = CrudCode.getByMask(crudMask);
        CrudCode.toCodes(ints);

        this.crudMask = crudMask;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof RolePermitImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        RolePermitImpl that = (RolePermitImpl) o;

        //noinspection RedundantIfStatement
        if (permitId != null ? !permitId.equals(that.permitId) : that.permitId != null) {
            return false;
        }

        return true;
    }

    @Override
    public int buildHashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.getPermitId() != null ? this.getPermitId().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + RolePermit.PERMIT_ID + '[' + this.getPermitId() + "], "
                + RolePermit.PERMIT_VALUES + '[' + Arrays.toString(this.getPermitValues()) + ']';
    }

}
