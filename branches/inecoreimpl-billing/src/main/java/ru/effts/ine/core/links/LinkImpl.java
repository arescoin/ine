/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.links;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkImpl.java 3396 2011-12-02 08:27:24Z dgomon $"
 */
public class LinkImpl extends AbstractVersionable implements Link {

    /** Паттерн, по которому будет идентифицироваться объект левого плеча связи */
    private SystemObjectPattern leftType;

    /** Паттерн, по которому будет идентифицироваться объект правого плеча связи */
    private SystemObjectPattern rightType;

    /** Код словарной статьи указывающей тип ассоциации (ее кратность) */
    private BigDecimal typeId;


    @Override
    public SystemObjectPattern getLeftType() {
        return this.leftType;
    }

    @Override
    public void setLeftType(SystemObjectPattern leftType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(leftType, Link.LEFT_TYPE);
        this.leftType = leftType;
    }

    @Override
    public SystemObjectPattern getRightType() {
        return this.rightType;
    }

    @Override
    public void setRightType(SystemObjectPattern rightType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(rightType, Link.RIGHT_TYPE);
        this.rightType = rightType;
    }

    @Override
    public BigDecimal getTypeId() {
        return this.typeId;
    }

    @Override
    public void setTypeId(BigDecimal typeId) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(typeId, Link.TYPE);
        this.typeId = typeId;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + Link.LEFT_TYPE + '[' + this.getLeftType() + ']' + ", "
                + Link.RIGHT_TYPE + '[' + this.getRightType() + ']' + ", "
                + Link.TYPE + '[' + this.getTypeId() + ']';
    }

}
