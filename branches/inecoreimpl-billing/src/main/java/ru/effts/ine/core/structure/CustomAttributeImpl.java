/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeImpl.java 3400 2011-12-02 10:28:35Z dgomon $"
 */
public class CustomAttributeImpl extends AbstractVersionable implements CustomAttribute {

    /** идентификатор системного объекта владеющего данным атрибутом */
    private BigDecimal systemObjectId;

    /** идентификатор системного объекта владеющего данным атрибутом */
    private BigDecimal customTypeId;

    /** тип данных значения атрибута */
    private DataType dataType;

    /** имя атрибута */
    private String attributeName;

    /** признак обязательности заполнения атрибута значением отличным от <b>null</b> */
    private boolean required;

    /** паттерн системного объекта */
    private SystemObjectPattern pattern;

    /** ограничения на значение атрибута */
    private String limitations;


    @Override
    public BigDecimal getSystemObjectId() {
        return this.systemObjectId;
    }

    @Override
    public void setSystemObjectId(BigDecimal systemObjectId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(systemObjectId, CustomAttribute.SYSTEM_OBJECT_ID);
        this.systemObjectId = systemObjectId;
    }

    @Override
    public BigDecimal getCustomTypeId() {
        return this.customTypeId;
    }

    @Override
    public void setCustomTypeId(BigDecimal customTypeId) throws IllegalIdException {

        if (customTypeId != null && MIN_ALLOWABLE_VAL.compareTo(customTypeId) > 0) {
            throw new IllegalIdException("customTypeId can't be less than 1, but has [" + customTypeId + "]");
        }

        this.customTypeId = customTypeId;
    }

    @Override
    public DataType getDataType() {
        return this.dataType;
    }

    @Override
    public void setDataType(DataType dataType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(dataType, CustomAttribute.DATA_TYPE);
        this.dataType = dataType;
    }

    @Override
    public String getAttributeName() {
        return this.attributeName;
    }

    @Override
    public void setAttributeName(String attributeName) throws IneIllegalArgumentException {
        this.attributeName = AbstractIdentifiable.checkEmpty(attributeName, CustomAttribute.ATTRIBITE_NAME);
    }

    @Override
    public boolean isRequired() {
        return this.required;
    }

    @Override
    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public SystemObjectPattern getPattern() {
        return this.pattern;
    }

    @Override
    public void setPattern(SystemObjectPattern pattern) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(pattern, CustomAttribute.PATTERN);
        this.pattern = pattern;
    }

    @Override
    public String getLimitations() {
        return this.limitations;
    }

    @Override
    public void setLimitations(String limitations) {
        this.limitations = limitations;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + CustomAttribute.SYSTEM_OBJECT_ID + '[' + this.getSystemObjectId() + "], "
                + CustomAttribute.DATA_TYPE + '[' + this.getDataType() + "], "
                + CustomAttribute.ATTRIBITE_NAME + '[' + this.getAttributeName() + "], "
                + CustomAttribute.REQUIRED + '[' + this.isRequired() + "], "
                + CustomAttribute.PATTERN + '[' + this.getPattern() + "], "
                + CustomAttribute.LIMITATIONS + '[' + this.getLimitations() + ']';
    }

}
