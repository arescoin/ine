/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.dic;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryImpl.java 3388 2011-12-01 12:34:26Z dgomon $"
 */
public class DictionaryEntryImpl extends AbstractVersionable implements DictionaryEntry {

    /** Идентификатор верхней словарной статьи */
    private BigDecimal parentTermId;

    /** Идентификатор словаря */
    private BigDecimal dictionaryId;


    @Override
    public BigDecimal getDictionaryId() {
        return this.dictionaryId;
    }

    @Override
    public void setDictionaryId(BigDecimal dictionaryId) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(dictionaryId, DictionaryEntry.DIC_ID);

        this.dictionaryId = dictionaryId;
    }

    @Override
    public BigDecimal getParentTermId() {
        return this.parentTermId;
    }

    @Override
    public void setParentTermId(BigDecimal upTerminCode) {
        this.parentTermId = upTerminCode;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof DictionaryEntryImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        DictionaryEntryImpl that = (DictionaryEntryImpl) o;

        return !(this.getDictionaryId() != null
                ? !this.getDictionaryId().equals(that.getDictionaryId())
                : that.getDictionaryId() != null);
    }

    @Override
    public int buildHashCode() {

        int result = super.hashCode();
        result = 31 * result + (this.getDictionaryId() != null ? this.getDictionaryId().hashCode() : 0);

        return result;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", " + DictionaryEntry.DIC_ID + "[" + this.getDictionaryId() + "]";
    }
}
