/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectImpl.java 3579 2012-01-25 10:21:19Z ikulkov $"
 */
public class SystemObjectImpl extends AbstractVersionable implements SystemObject {

    /** Уникальный идентификатор типа системного объекта */
    private BigDecimal type;

    /** Имя системного объекта */
    private String name;

    /** Уникальный идентификатор родительского системного объекта */
    private BigDecimal up;

    /** Паттерн системного объекта, для параметризации столбца таблицы */
    private SystemObjectPattern pattern;

    /** Признак обязательности заполнения столбца таблицы данными */
    private boolean required;

    @Override
    public BigDecimal getType() {
        return this.type;
    }

    @Override
    public void setType(BigDecimal type) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(type, SystemObject.TYPE);

        if (this.getPattern() != null && !type.equals(SystemObjectType.column.getTypeCode())) {
            throw new IneIllegalArgumentException(
                    "Not column SystemObjectType can't be specified for not null SystemObjectPattern");
        }

        this.type = type;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, SystemObject.NAME);
    }

    @Override
    public BigDecimal getUp() {
        return this.up;
    }

    @Override
    public void setUp(BigDecimal upId) {
        this.up = upId;
    }

    @Override
    public SystemObjectPattern getPattern() {
        return this.pattern;
    }

    @Override
    public void setPattern(SystemObjectPattern pattern) throws IneIllegalArgumentException {

        if (pattern != null
                && (this.getType() != null && !this.getType().equals(SystemObjectType.column.getTypeCode()))) {
            throw new IneIllegalArgumentException(
                    "Not null SystemObjectPattern can be specified only for column SystemObjectType");
        }

        this.pattern = pattern;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + SystemObject.NAME + '[' + this.getName() + "], "
                + SystemObject.TYPE + '[' + this.getType() + "], "
                + SystemObject.UP + '[' + this.getUp() + "], "
                + SystemObject.PATTERN + '[' + this.getPattern() + ']';
    }

}
