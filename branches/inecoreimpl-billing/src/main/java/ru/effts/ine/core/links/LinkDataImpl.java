/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.links;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataImpl.java 3396 2011-12-02 08:27:24Z dgomon $"
 */
public class LinkDataImpl extends AbstractVersionable implements LinkData {

    /** Идентификатор описателя линка {@link ru.effts.ine.core.links.Link} */
    private BigDecimal linkId;

    /** Идентификатор левого участника ассоциации */
    private BigDecimal leftId;

    /** Bдентификатор правого участника ассоциации */
    private BigDecimal rightId;


    @Override
    public BigDecimal getLinkId() {
        return this.linkId;
    }

    @Override
    public void setLinkId(BigDecimal linkId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(linkId, LinkData.LINK_ID);
        this.linkId = linkId;
    }

    @Override
    public BigDecimal getLeftId() {
        return this.leftId;
    }

    @Override
    public void setLeftId(BigDecimal leftId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(leftId, LinkData.LEFT_ID);
        this.leftId = leftId;
    }

    @Override
    public BigDecimal getRightId() {
        return this.rightId;
    }

    @Override
    public void setRightId(BigDecimal rightId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(rightId, LinkData.RIGHT_ID);
        this.rightId = rightId;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() +
                ", " + LinkData.LINK_ID + "[" + this.getLinkId() + ']' + ", "
                + LinkData.LEFT_ID + "[" + this.getLeftId() + ']' + ", "
                + LinkData.RIGHT_ID + "[" + this.getRightId() + ']';
    }

}
