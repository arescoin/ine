/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.AbstractVersionable;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserImpl.java 3403 2011-12-02 12:24:56Z dgomon $"
 */
public class SystemUserImpl extends AbstractVersionable implements SystemUser {

    /** Признак активности учетной записи пользователя */
    private boolean active;

    private String login;

    private String md5;

    private String fio;


    @Override
    public boolean isActive() {
        return this.active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String getLogin() {
        return this.login;
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public String getMd5() {
        return this.md5;
    }

    @Override
    public void setMd5(String md5) {
        this.md5 = md5;
    }

    @Override
    public String toString() {
        return getClass().getName() + ": " +
                SystemUser.ACTIVE + '[' + this.isActive() + "], " +
                SystemUser.SYS_LOGIN + '[' + this.getLogin() + ']';
    }

}
