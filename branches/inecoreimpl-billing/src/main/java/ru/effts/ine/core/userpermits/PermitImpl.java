/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PermitImpl.java 3403 2011-12-02 12:24:56Z dgomon $"
 */
public class PermitImpl extends AbstractVersionable implements Permit {

    /** Название доступа */
    private String name;

    /** Значение маски применимости доступа */
    private String maskValue;

    /** Признак обязательности значения для данного доступа */
    private boolean valueRequired;


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Permit.NAME);
    }

    @Override
    public String getMaskValue() {
        return this.maskValue;
    }

    @Override
    public void setMaskValue(String maskValue) {
        this.maskValue = maskValue;
    }

    @Override
    public boolean isValueRequired() {
        return this.valueRequired;
    }

    @Override
    public void setValueRequired(boolean required) {
        this.valueRequired = required;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + Permit.NAME + '[' + this.getName() + "], "
                + Permit.MASK_VALUE + '[' + this.getMaskValue() + "],"
                + Permit.VALUE_REQUIRED + '[' + this.isValueRequired() + ']';
    }

}
