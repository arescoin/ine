/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import java.util.Date;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractVersionable.java 3880 2014-11-11 14:03:00Z DGomon $"
 */
public abstract class AbstractVersionable extends AbstractIdentifiable implements Versionable {

    private Date coreFd;
    private Date coreTd;

    @Override
    public void setCoreFd(Date fd) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkNull(fd, "coreFd");

        this.coreFd = fd;

        this.checkDate();
        this.setHashCode();
    }

    @Override
    public Date getCoreFd() {
        return this.coreFd;
    }

    @Override
    public void setCoreTd(Date td) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkNull(td, "coreTd");

        this.coreTd = td;

        this.checkDate();
        this.setHashCode();
    }

    @Override
    public Date getCoreTd() {
        return this.coreTd;
    }

    private void checkDate() {
        if (this.getCoreFd() != null && this.getCoreTd() != null && this.getCoreFd().after(this.getCoreTd())) {
            throw new IneIllegalArgumentException("FromDate can't be after ToDate. " +
                    "FD[" + getCoreFd() + " (" + getCoreFd().getTime() + ")], " +
                    "TD[" + getCoreTd() + " (" + getCoreTd().getTime() + ")].");
        }
    }

    /** Высчитывает по идентификатору и датам версии */
    @Override
    protected int buildHashCode() {

        int result = super.hashCode();

        if (this.getCoreFd() != null) {
            result = 31 * result + this.getCoreFd().hashCode();
        }

        if (this.getCoreTd() != null) {
            result = 31 * result + this.getCoreTd().hashCode();
        }

        return result;
    }

    /**
     * Два объекта считаются идентичными если их идентификаторы и даты совпадают, однако, если хотя бы одна из дат не
     * задана, объекты считаются разными.
     */
    @Override
    public boolean equals(Object obj) {

        boolean result = false;

        if (this == obj) {
            result = true;
        } else if (super.equals(obj) && obj instanceof Versionable) {
            Versionable otherVersionable = (Versionable) obj;

            if (this.hashCode() == otherVersionable.hashCode() &&
                    this.getCoreFd() != null && this.getCoreTd() != null
                    && this.getCoreFd().equals(otherVersionable.getCoreFd())
                    && this.getCoreTd().equals(otherVersionable.getCoreTd())) {

                result = true;
            }
        }

        return result;
    }

    /** Выводит класс объекта, его идентификатор и даты */
    @Override
    public String toString() {
        return super.toString() + ", " + Versionable.CORE_FD + "[" + this.getCoreFd()
                + "], " + Versionable.CORE_TD + "[" + this.getCoreTd() + "]";
    }

}
