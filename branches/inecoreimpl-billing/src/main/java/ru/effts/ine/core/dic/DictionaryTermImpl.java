/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.dic;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermImpl.java 3388 2011-12-01 12:34:26Z dgomon $"
 */
public class DictionaryTermImpl extends DictionaryEntryImpl implements DictionaryTerm {

    /** Код языка словарной статьи */
    private BigDecimal langCode;

    /** Термин */
    private String term;


    @Override
    public BigDecimal getLangCode() {
        return this.langCode;
    }

    @Override
    public void setLangCode(BigDecimal langCode) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(langCode, DictionaryTerm.LANG);
        this.langCode = langCode;
    }

    @Override
    public String getTerm() {
        return this.term;
    }

    @Override
    public void setTerm(String term) throws IneIllegalArgumentException {
        this.term = AbstractIdentifiable.checkEmpty(term, DictionaryTerm.TERM);
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof DictionaryTermImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        DictionaryTermImpl that = (DictionaryTermImpl) o;

        return !(this.getLangCode() != null
                ? !this.getLangCode().equals(that.getLangCode())
                : that.getLangCode() != null);
    }

    @Override
    public int buildHashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.getLangCode() != null ? this.getLangCode().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return super.toString() + "], " + DictionaryTerm.LANG + "[" + this.getLangCode() + "]";
    }
}
