/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure.test;

import ru.effts.ine.core.AbstractVersionable;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: TestObjectImpl.java 3399 2011-12-02 08:59:04Z dgomon $"
 */
public class TestObjectImpl extends AbstractVersionable implements TestObject {
    /** Тестовое поле */
    private String value;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + TestObject.VALUE + '[' + this.getValue() + ']';
    }

}
