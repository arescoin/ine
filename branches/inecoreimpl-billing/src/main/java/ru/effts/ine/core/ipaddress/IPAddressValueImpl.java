/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.ipaddress;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class IPAddressValueImpl extends AbstractVersionable implements IPAddressValue {

    IPAddress ipAddress;


    @Override
    public IPAddress getIpAddress() {
        return ipAddress;
    }

    @Override
    public void setIpAddress(IPAddress ipAddress) {
        AbstractIdentifiable.checkNull(ipAddress, IPAddressValue.IP_ADDRESS);
        this.ipAddress = ipAddress;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + IPAddressValue.IP_ADDRESS + "[" + ipAddress + "] ";
    }

}
