/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ParametrizedConstantImpl extends ConstantImpl implements ParametrizedConstant {

    /** Маска параметризации */
    private SystemObjectPattern mask;

    @Override
    public BigDecimal getType() {
        return ParametrizedConstant.TYPE;
    }

    @Override
    public SystemObjectPattern getMask() {
        return this.mask;
    }

    @Override
    public void setMask(SystemObjectPattern mask) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(mask, ParametrizedConstant.MASK);
        this.mask = mask;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + ParametrizedConstant.MASK + "[" + this.getMask() + "]";
    }
}
