/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.funcswitch;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchImpl.java 3390 2011-12-01 13:04:55Z dgomon $"
 */
public class FuncSwitchImpl extends AbstractVersionable implements FuncSwitch {

    /** Состояние переключателя: включено/выключено */
    private boolean enabled;

    /** Дата изменения состояния переключателя */
    private Date changeDate;

    /** Контрольная метка смены состояния переключателя */
    private BigDecimal mark;

    /*Код состояния переключателя*/
    private String stateCode;


    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

    @Override
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public Date getChangeDate() {
        return changeDate;
    }

    @Override
    public void setChangeDate(Date changeDate) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(changeDate, FuncSwitch.C_DATE);
        this.changeDate = changeDate;
    }

    @Override
    public BigDecimal getMark() {
        return this.mark;
    }

    @Override
    public void setMark(BigDecimal mark) {
        this.mark = mark;
    }

    @Override
    public String getStateCode() {
        return this.stateCode;
    }

    @Override
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + FuncSwitch.C_DATE + "[" + this.getChangeDate() + "], "
                + FuncSwitch.STATE + "[" + this.isEnabled() + "], "
                + FuncSwitch.MARK + "[" + mark + "], "
                + FuncSwitch.STATE_CODE + "['" + stateCode + "']";
    }

}
