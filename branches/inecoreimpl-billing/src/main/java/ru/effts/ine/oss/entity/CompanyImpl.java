/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.oss.entity;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CompanyImpl.java 3404 2011-12-02 12:43:15Z dgomon $"
 */
public class CompanyImpl extends AbstractVersionable implements Company {

    /** Название организации */
    private String name;

    /** Тип организации (словарь 16) */
    private BigDecimal type;

    /** Тип собственности (словарь 17) */
    private BigDecimal propertyType;


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Company.NAME);
    }

    @Override
    public BigDecimal getType() {
        return this.type;
    }

    @Override
    public void setType(BigDecimal type) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(type, Company.TYPE);
        this.type = type;
    }

    @Override
    public BigDecimal getPropertyType() {
        return this.propertyType;
    }

    @Override
    public void setPropertyType(BigDecimal propertyType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(propertyType, Company.PROPERTY_TYPE);
        this.propertyType = propertyType;
    }

}
