/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.strings;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringImpl.java 3397 2011-12-02 08:36:34Z dgomon $"
 */
public class MessageStringImpl implements MessageString, Serializable {

    /** Уникальный строковый ключ (идентификатор) для данного локализационного ресурса */
    private String key;

    /** Системный идентификатор языка */
    private BigDecimal languageCode;

    /** Локализованный строковый ресурс */
    private String message;


    @Override
    public String getKey() {
        return this.key;
    }

    @Override
    public void setKey(String key) throws IneIllegalArgumentException {
        this.key = AbstractIdentifiable.checkEmpty(key, MessageString.KEY);
    }

    @Override
    public BigDecimal getLanguageCode() {
        return this.languageCode;
    }

    @Override
    public void setLanguageCode(BigDecimal languageCode) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(languageCode, MessageString.LANG);
        this.languageCode = languageCode;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setMessage(String message) {
        this.message = AbstractIdentifiable.checkEmpty(message, MessageString.MSG);
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + MessageString.KEY + '[' + this.getKey() + "], "
                + MessageString.LANG + '[' + this.getLanguageCode() + "], "
                + MessageString.MSG + '[' + this.getMessage() + ']';
    }

}
