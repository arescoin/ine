/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueImpl.java 3387 2011-12-01 12:24:05Z dgomon $"
 */
public class ConstantValueImpl
        extends AbstractVersionable implements ConstantValue {

    private String value;

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public void setValue(String value) throws IneIllegalArgumentException {
        this.value = AbstractIdentifiable.checkEmpty(value, ConstantValue.VALUE);
    }

    @Override
    public String toString() {
        return super.toString() + ", " + ConstantValue.VALUE + "[" + this.getValue() + "]";
    }
}
