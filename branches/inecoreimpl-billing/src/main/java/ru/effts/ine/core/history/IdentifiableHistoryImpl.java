/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

import ru.effts.ine.core.*;
import ru.effts.ine.core.userpermits.CrudCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryImpl.java 3392 2011-12-01 14:27:21Z dgomon $"
 */
public class IdentifiableHistoryImpl implements IdentifiableHistory {

    /** Идентификатор объекта */
    private SyntheticId entityId;

    /** Описание версии */
    private String versionDsc;

    /** Идентификатор системного объекта */
    private BigDecimal sysObjId;

    /** Идентификатор пользователя - автора изменений */
    private BigDecimal userId;

    /** Дата действия */
    private Date actionDate;

    /** Тип действия */
    private CrudCode type;


    @Override
    public void setCoreId(BigDecimal sysObjId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(sysObjId, IdentifiableHistory.SYSOBJ_ID);
        this.sysObjId = sysObjId;
    }

    @Override
    public BigDecimal getCoreId() {
        return this.sysObjId;
    }

    @Override
    public String getCoreDsc() {
        return this.versionDsc;
    }

    @Override
    public void setCoreDsc(String versionDsc) {
        this.versionDsc = versionDsc;
    }

    @Override
    public SyntheticId getEntityId() {
        return this.entityId;
    }

    @Override
    public void setEntityId(SyntheticId entityId) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(entityId, IdentifiableHistory.ENTITY_ID);
        this.entityId = entityId;
    }

    @Override
    public BigDecimal getUserId() {
        return this.userId;
    }

    @Override
    public void setUserId(BigDecimal userId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(userId, IdentifiableHistory.USER_ID);
        this.userId = userId;
    }

    @Override
    public Date getActionDate() {
        return this.actionDate;
    }

    @Override
    public void setActionDate(Date actionDate) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(actionDate, IdentifiableHistory.ACTION_DATE);
        this.actionDate = actionDate;
    }

    @Override
    public CrudCode getType() {
        return this.type;
    }

    @Override
    public void setType(CrudCode type) {
        this.type = type;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /** Вычисляет по идентификатору типа, идентификатору сущности, идентификатору пользователя, и дате действия */
    @Override
    public int hashCode() {

        int result = this.getEntityId() != null ? this.getEntityId().hashCode() : 0;
        result = 31 * result + (this.getCoreId() != null ? this.getCoreId().hashCode() : 0);
        result = 31 * result + (this.getUserId() != null ? this.getUserId().hashCode() : 0);
        result = 31 * result + (this.getActionDate() != null ? this.getActionDate().hashCode() : 0);
        result = 31 * result + (this.getType() != null ? this.getType().hashCode() : 0);

        return result;
    }

    /** Два объекта считаются идентичными если совпадают их типы, идентификаторы, авторы, даты изменений */
    @Override
    public boolean equals(Object o) {

        boolean result = false;

        if (this != o) {
            if (o instanceof IdentifiableHistory) {
                IdentifiableHistory that = (IdentifiableHistory) o;

                // проверяем на непустые значения
                if (this.getCoreId() != null && that.getCoreId() != null
                        && this.getEntityId() != null && that.getEntityId() != null
                        && this.getUserId() != null && that.getUserId() != null
                        && this.getActionDate() != null && that.getActionDate() != null
                        && this.getType() != null && that.getType() != null) {

                    // теперь проверим на совпадение полей в объектах...
                    if (this.getActionDate().equals(that.getActionDate())
                            && this.getEntityId().equals(that.getEntityId())
                            && this.getUserId().equals(that.getUserId())
                            && this.getCoreId().equals(that.getCoreId())
                            && this.getType().equals(that.getType())) {

                        result = true;
                    }
                }
            }
        }

        return result;
    }

    /** Выводит класс объекта и его идентификатор */
    @Override
    public String toString() {
        return this.getClass().getSimpleName() + ": " +
                Identifiable.CORE_ID + " (" + IdentifiableHistory.SYSOBJ_ID + ")" + "[" + this.getCoreId() + "], " +
                IdentifiableHistory.ENTITY_ID + "[" + this.getEntityId() + "], " +
                IdentifiableHistory.USER_ID + "[" + this.getUserId() + "], " +
                IdentifiableHistory.ACTION_DATE + "[" + this.getActionDate() + "], " +
                IdentifiableHistory.ACTION_TYPE + "[" + this.getType() + "], " +
                IdentifiableHistory.VERSION_DSC + "[" + this.getCoreDsc() + "]";
    }
}
