/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ConstantImpl extends AbstractVersionable implements Constant {

    /** Имя константы */
    private String name;

    /** Тип константы */
//    private BigDecimal type;

    /** Значение константы по умолччанию */
    private String defaultValue;

    /** Признак допустимости пустого значения для константы */
    private boolean nullable;

    /** Признак возможности модификации данной костанты */
    private boolean modifiable;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Constant.NAME);
    }

    @Override
    public BigDecimal getType() {
        return Constant.TYPE;
    }

//    @Override
//    public void setType(BigDecimal type) throws IneIllegalArgumentException {
//        AbstractIdentifiable.checkNull(type, Constant.TYPE);
//
//        this.type = type;
//    }

    @Override
    public String getDefaultValue() {
        return this.defaultValue;
    }

    @Override
    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public boolean isNullable() {
        return this.nullable;
    }

    @Override
    public void setNullable(boolean nullable) {
        this.nullable = nullable;
    }

    @Override
    public boolean isModifiable() {
        return this.modifiable;
    }

    @Override
    public void setModifiable(boolean modifiable) {
        this.modifiable = modifiable;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + Constant.NAME + "[" + this.getName() + "], "
                + Constant.TYPE + "[" + this.getType() + "]";
    }
}
