/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.language;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageImpl.java 3395 2011-12-02 08:27:06Z dgomon $"
 */
public class SysLanguageImpl extends AbstractVersionable implements SysLanguage {

    /** Описание языка (iso639) */
    private BigDecimal langDetails;

    /** Описание страны (iso3166) */
    private BigDecimal countryDetails;


    @Override
    public BigDecimal getLangDetails() {
        return this.langDetails;
    }

    @Override
    public void setLangDetails(BigDecimal langDetails) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(langDetails, SysLanguage.LANG_DETAILS);
        this.langDetails = langDetails;
    }

    @Override
    public BigDecimal getCountryDetails() {
        return this.countryDetails;
    }

    @Override
    public void setCountryDetails(BigDecimal countryDetails) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(countryDetails, SysLanguage.COUNTRY_DETAILS);
        this.countryDetails = countryDetails;
    }

    @Override
    public void setCoreDsc(String dsc) {
        // ничего не выставляем, ибо комментарии для объекта не поддерживаются
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + SysLanguage.LANG_DETAILS + "[" + this.getLangDetails() + ']' + ", "
                + SysLanguage.COUNTRY_DETAILS + "[" + this.getCountryDetails() + ']';
    }

}
