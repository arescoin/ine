/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.SyntheticId;

import java.util.Date;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeImpl.java 3392 2011-12-01 14:27:21Z dgomon $"
 */
public class ActionAttributeImpl extends AbstractIdentifiable implements ActionAttribute {

    /** Идентификатор сущности, над которой было совершено действие */
    private SyntheticId entityId;

    /** Дата совершённого действия */
    private Date actionDate;

    /** Название пользовательского атрибута действия */
    private String attributeName;

    /** Значение пользовательского атрибута действия */
    private String attributeValue;


    public SyntheticId getEntityId() {
        return this.entityId;
    }

    public void setEntityId(SyntheticId entityId) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(entityId, ActionAttribute.ENTITY_ID);
        this.entityId = entityId;
    }

    public Date getActionDate() {
        return this.actionDate;
    }

    public void setActionDate(Date actionDate) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(actionDate, ActionAttribute.ACTION_DATE);
        this.actionDate = actionDate;
    }

    public String getAttributeName() {
        return this.attributeName;
    }

    public void setAttributeName(String attributeName) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkEmpty(attributeName, ActionAttribute.ATTRIBUTE_NAME);
        this.attributeName = attributeName;
    }

    public String getAttributeValue() {
        return this.attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof ActionAttributeImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        ActionAttributeImpl that = (ActionAttributeImpl) o;

        if (this.getEntityId() != null ? !this.getEntityId().equals(that.getEntityId())
                : that.getEntityId() != null) {
            return false;
        }
        if (this.getActionDate() != null ? !this.getActionDate().equals(that.getActionDate())
                : that.getActionDate() != null) {
            return false;
        }
        if (this.getAttributeName() != null ? !this.getAttributeName().equals(that.getAttributeName())
                : that.getAttributeName() != null) {
            return false;
        }
        if (this.getAttributeValue() != null ? !this.getAttributeValue().equals(that.getAttributeValue())
                : that.getAttributeValue() != null) {
            return false;
        }

        //noinspection RedundantIfStatement
        return true;
    }

    @Override
    public int buildHashCode() {

        int result = super.hashCode();
        result = 31 * result + (this.getEntityId() != null ? this.getEntityId().hashCode() : 0);
        result = 31 * result + (this.getActionDate() != null ? this.getActionDate().hashCode() : 0);
        result = 31 * result + (this.getAttributeName() != null ? this.getAttributeName().hashCode() : 0);
        result = 31 * result + (this.getAttributeValue() != null ? this.getAttributeValue().hashCode() : 0);

        return result;
    }

    @Override
    public String toString() {
        return super.toString() + ", " +
                ActionAttribute.ENTITY_ID + "[" + this.getEntityId() + "], " +
                ActionAttribute.ACTION_DATE + "[" + this.getActionDate() + "], " +
                ActionAttribute.ATTRIBUTE_NAME + "[" + this.getAttributeName() + "], " +
                ActionAttribute.ATTRIBUTE_VALUE + "[" + this.getAttributeValue() + "]";
    }

}
