/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueImpl.java 3387 2011-12-01 12:24:05Z dgomon $"
 */
public class ParametrizedConstantValueImpl
        extends ConstantValueImpl implements ParametrizedConstantValue {

    private BigDecimal param;

    @Override
    public BigDecimal getParam() {
        return this.param;
    }

    @Override
    public void setParam(BigDecimal param) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(param, ParametrizedConstantValue.PARAM);

        this.param = param;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + ParametrizedConstantValue.PARAM + "[" + this.getParam() + "]";
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }

        if (!(o instanceof ParametrizedConstantValueImpl)) {
            return false;
        }

        if (!super.equals(o)) {
            return false;
        }

        ParametrizedConstantValueImpl that = (ParametrizedConstantValueImpl) o;

        return !(this.getParam() != null ? !this.getParam().equals(that.getParam()) : that.getParam() != null);
    }

    @Override
    public int buildHashCode() {

        int result = super.hashCode();

        if (this.getParam() != null) {
            result = 31 * result + this.getParam().hashCode();
        }

        return result;
    }

}
