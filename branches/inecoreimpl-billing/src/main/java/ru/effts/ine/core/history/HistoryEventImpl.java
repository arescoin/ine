/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventImpl.java 3392 2011-12-01 14:27:21Z dgomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class HistoryEventImpl extends VersionableHistoryImpl implements HistoryEvent {

    private boolean processed;


    @Override
    public boolean isProcessed() {
        return this.processed;
    }

    @Override
    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + HistoryEvent.PROCESSED + "[" + this.isProcessed() + "]";
    }

}
