/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleImpl.java 3403 2011-12-02 12:24:56Z dgomon $"
 */
public class UserRoleImpl extends AbstractVersionable implements UserRole {

    /** Идентификатор системного пользователя, обладателя роли */
    private BigDecimal userId;

    /** Признак активности роли */
    private boolean active;


    @Override
    public BigDecimal getUserId() {
        return this.userId;
    }

    @Override
    public void setUserId(BigDecimal userId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(userId, UserRole.USER_ID);
        this.userId = userId;
    }

    @Override
    public boolean isActive() {
        return this.active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int buildHashCode() {
        int result = super.hashCode();
        result = 31 * result + (this.getUserId() != null ? this.getUserId().hashCode() : 0);

        return result;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (!(o instanceof UserRoleImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        UserRoleImpl userRole = (UserRoleImpl) o;

        //noinspection RedundantIfStatement
        if (this.getUserId() != null
                ? !this.getUserId().equals(userRole.getUserId())
                : userRole.getUserId() != null) {

            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + UserRole.USER_ID + '[' + this.getUserId() + "], "
                + UserRole.ACTIVE + '[' + this.isActive() + ']';
    }

}
