/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.dic;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DictionaryImpl.java 3388 2011-12-01 12:34:26Z dgomon $"
 */
public class DictionaryImpl extends AbstractVersionable implements Dictionary {

    /** Название словаря */
    private String name;

    /** тип словаря */
    private BigDecimal dicType;

    /** политика заполнения кодов словарных статей */
    private BigDecimal entryCodePolicy;

    /** политика локализации словарных статей */
    private BigDecimal localizationPolicy;

    /** идентификатор верхнего словаря */
    private BigDecimal parentId;


    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {
        this.name = AbstractIdentifiable.checkEmpty(name, Dictionary.NAME);
    }

    @Override
    public BigDecimal getDicType() {
        return this.dicType;
    }

    @Override
    public void setDicType(BigDecimal dicType) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(dicType, Dictionary.DIC_TYPE);
        this.dicType = dicType;
    }

    @Override
    public BigDecimal getEntryCodePolicy() {
        return this.entryCodePolicy;
    }

    @Override
    public void setEntryCodePolicy(BigDecimal entryCodePolicy) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkSystemId(entryCodePolicy, Dictionary.CODE_POLICY);
        this.entryCodePolicy = entryCodePolicy;
    }

    @Override
    public BigDecimal getLocalizationPolicy() {
        return this.localizationPolicy;
    }

    @Override
    public void setLocalizationPolicy(BigDecimal localizationPolicy) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkSystemId(localizationPolicy, Dictionary.L10N_POLICY);
        this.localizationPolicy = localizationPolicy;
    }

    @Override
    public BigDecimal getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(BigDecimal parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + Dictionary.NAME + "[" + this.getName() + "], "
                + Dictionary.DIC_TYPE + "[" + this.getDicType() + "], "
                + Dictionary.PARENT + "[" + this.getParentId() + "], "
                + Dictionary.CODE_POLICY + "[" + this.getEntryCodePolicy() + "], "
                + Dictionary.L10N_POLICY + "[" + this.getLocalizationPolicy() + "]";
    }
}
