/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.structure.CustomAttributeDefVal;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface CustomAttributeDefValBean<T extends CustomAttributeDefVal> extends VersionableBean<T> {
}
