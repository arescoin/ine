/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.Versionable;
import ru.effts.ine.core.history.VersionableHistory;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.history.VersionableHistory} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface VersionableHistoryBean<T extends VersionableHistory> extends IdentifiableHistoryBean<T> {

    /**
     * Метод возвращает все записи об изменениях в идентифицируемом объекте
     *
     * @param user     пользователь, запрашивающий историю объекта
     * @param sysObjId идентификатор системного объекта
     * @param entityId идентификатор сущности
     * @return коллекция системных объектов
     * @throws ru.effts.ine.core.GenericSystemException при возникновении ошибок
     */
    Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException;

    /**
     * Метод возвращает версию объекта из истории
     *
     * @param user               пользователь запрашивающий версию объекта
     * @param versionableHistory момент истории объекта
     * @return объект
     * @throws GenericSystemException при возникновении ошибок
     */
    Versionable getObjectVersion(UserProfile user, T versionableHistory) throws GenericSystemException;

}
