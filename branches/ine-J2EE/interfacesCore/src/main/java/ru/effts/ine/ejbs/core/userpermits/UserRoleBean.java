/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.userpermits.UserRole;
import ru.effts.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.userpermits.UserRole} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleBean.java 2027 2011-04-21 10:40:35Z sfilatov $"
 */
public interface UserRoleBean<T extends UserRole> extends VersionableBean<T> {

    /**
     * Метод возвращает список всех ролей пользователя
     *
     * @param userId идентификатор пользователя
     * @return коллекция объектов UserRole
     * @throws ru.effts.ine.core.CoreException
     *          при некорректных значениях в датах системного объекта
     */
    Collection<T> getRolesByUserId(UserProfile user, BigDecimal userId) throws CoreException;

}
