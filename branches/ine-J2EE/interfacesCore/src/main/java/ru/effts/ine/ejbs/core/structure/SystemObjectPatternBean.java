/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Интерфейс для j2ee доступа к методам утилитного класса PatternUtils, работающего с
 * {@link ru.effts.ine.core.structure.SystemObjectPattern системными паттернами}.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectPatternBean.java 1517 2011-02-15 13:33:57Z ikulkov $"
 */
public interface SystemObjectPatternBean {

    /**
     * Проверяет корретность паттерна.
     * В проверку входит следующее:
     * <ul>
     * <li>Все идентификаторы системных объектов в паттерне должны определять столбцы таблицы
     * (как {@link ru.effts.ine.core.structure.SystemObjectPattern#getFinalId()}, так и
     * {@link ru.effts.ine.core.structure.SystemObjectPattern.Element#getSysObjId()})</li>
     * <li>Все столбцы должны принадлежать одной таблице</li>
     * </ul>
     * После проверки паттерна, выставляется
     * {@link ru.effts.ine.core.structure.SystemObjectPattern#getTableId() идентификатор таблицы}.
     *
     * @param user    профайл c пользователем делающим запрос
     * @param pattern проверяемый паттерн
     * @return проверенный паттерн
     * @throws ru.effts.ine.core.IneCorruptedStateException в случае обнаружения ошибок при проверке паттерна
     * @throws ru.effts.ine.core.CoreException в случае ошибок при работе с базой и мета-информацией
     */
    SystemObjectPattern checkPattern(UserProfile user, SystemObjectPattern pattern) throws CoreException;

    /**
     * Получает {@link ru.effts.ine.core.Identifiable объект} по паттерну и значению, которым параметризуется паттерн.
     *
     * @param user    профайл c пользователем делающим запрос
     * @param pattern системный паттерн
     * @param value   значение-идентификатор
     * @return объект, определяемый паттерном и значением
     * @throws ru.effts.ine.core.CoreException в случае ошибок при работе с базой и мета-информацией
     */
    Identifiable getObjectByPattern(UserProfile user, SystemObjectPattern pattern, BigDecimal value)
            throws CoreException;

    /**
     * Получает {@link java.util.Collection коллекцию} {@link ru.effts.ine.core.Identifiable объектов} по паттерну
     * и значению, которым параметризуется паттерн.
     *
     * @param user    профайл c пользователем делающим запрос
     * @param pattern системный паттерн
     * @param value   значение-идентификатор
     * @return коллекция объектов, определяемых паттерном и значением
     * @throws ru.effts.ine.core.CoreException в случае ошибок при работе с базой и мета-информацией
     */
    Collection<Identifiable> getObjectsByPattern(UserProfile user, SystemObjectPattern pattern, BigDecimal value)
            throws CoreException;

    /**
     * Получает {@link java.util.Collection коллекцию} {@link ru.effts.ine.core.Identifiable объектов}, которые
     * могут быть использованы в качестве параметризующего значения переданного паттерна.
     *
     * @param user    профайл c пользователем делающим запрос
     * @param pattern системный паттерн
     * @return коллекция объектов, доступных для параметризации в соответствии с переданным паттерном
     * @throws ru.effts.ine.core.CoreException в случае ошибок при работе с базой и мета-информацией
     */
    Collection<Identifiable> getAvailableObjects(UserProfile user, SystemObjectPattern pattern) throws CoreException;

    /**
     * Получает из переданного {@link ru.effts.ine.core.Identifiable InE-объекта} системный паттерн и значение,
     * которым он должен параметризироваться.
     * <br>
     * Результат возвращается в виде мапы из одной строчки, ключом в которой является паттерн, а значением - его
     * параметризующее значение (которое сохраняется отдельно).
     *
     * @param user   профайл c пользователем делающим запрос
     * @param object InE-объект
     * @return однострочная карта вида "SystemObjectPattern (key) -> BigDecimal (value)"
     * @throws ru.effts.ine.core.CoreException в случае ошибок при работе с базой и мета-информацией
     */
    Map<SystemObjectPattern, BigDecimal> getPatternByObject(UserProfile user, Identifiable object) throws CoreException;

}
