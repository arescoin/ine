/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.constants.ParametrizedConstant} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface ParametrizedConstantBean<T extends ParametrizedConstant> extends VersionableBean<T> {
}
