/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.userpermits.NoSuchUserException;
import ru.effts.ine.core.userpermits.SystemUser;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.userpermits.SystemUser} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface SystemUserBean<T extends SystemUser> extends VersionableBean<T> {

    /**
     * Возвращает пользователя системы по его системному имени и хешу пароля
     *
     * @param login системное имя пользователя
     * @param md5   хеш пароля пользователя
     * @return пользователь
     * @throws ru.effts.ine.core.userpermits.NoSuchUserException если пользователь с указанными данными не найден
     * @throws ru.effts.ine.core.GenericSystemException при ошщибках доступа к данным
     */
    T login(String login, String md5) throws GenericSystemException, NoSuchUserException;

}
