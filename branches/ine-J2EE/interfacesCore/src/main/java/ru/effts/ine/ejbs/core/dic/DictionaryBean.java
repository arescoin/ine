/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.dic.Dictionary} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface DictionaryBean<T extends Dictionary> extends VersionableBean<T> {
}
