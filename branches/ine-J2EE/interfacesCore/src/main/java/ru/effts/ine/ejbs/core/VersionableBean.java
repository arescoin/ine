/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.Versionable;

import java.util.Collection;
import java.util.Date;

/**
 * Базовый интерфейс для j2ee доступа к Versionable ine-объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableBean.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public interface VersionableBean<T extends Versionable> extends IdentifiableBean<T> {

    /**
     * Получает предыдущие версии переданного объекта, оторбражающие историю его изменения.
     *
     * @param user        пользователь который запрашивает историю
     * @param versionable объект, для которого получается история
     *
     * @return предыдущие версии объекта
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибках получения и обработки данных
     */
    Collection<T> getOldVersions(UserProfile user, T versionable) throws GenericSystemException;

    Collection<T> getOldVersions(UserProfile user, T versionable, Date fd, Date td) throws GenericSystemException;

}
