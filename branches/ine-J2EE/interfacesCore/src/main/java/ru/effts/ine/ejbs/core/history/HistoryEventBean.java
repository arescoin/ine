/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.history.HistoryEvent;
import ru.effts.ine.core.userpermits.SystemUser;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Интерфейс доступа к функциональности работы с союытиями CUD
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventBean.java 3099 2011-10-27 10:41:08Z dgomon $"
 */
public interface HistoryEventBean<T extends HistoryEvent> extends VersionableHistoryBean<T> {

    String TIMER_INFO = HistoryEventBean.class.getName();

    /**
     * Возвращает колекцию событий требующих обработки
     * <p/>
     * Внимание! не предназначен для прикладной разработки!
     *
     * @return колекция событий
     * @throws ru.effts.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    Collection<T> getEventsForProcess() throws CoreException;

    /**
     * Возвращает колекцию событий подверженных обработке
     * <p/>
     * Внимание! не предназначен для прикладной разработки!
     *
     * @return колекция событий
     * @throws ru.effts.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    Collection<T> getProcessedEvents() throws CoreException;

    /**
     * Отмечает событие как обработанное
     * <p/>
     * Внимание! не предназначен для прикладной разработки!
     *
     * @param historyEvent событие
     * @throws ru.effts.ine.core.GenericSystemException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    void markAsProcessed(T historyEvent) throws GenericSystemException;

    /**
     * Возвращает признак работающего таймера
     *
     * @param timerInfo идентификатор таймера
     * @return true - запущен, иначе - false
     * @throws IllegalArgumentException при попытке передать null
     * @throws ru.effts.ine.core.GenericSystemException при возникновении ошибок на серверной стороне
     */
    boolean isTimerStarted(Serializable timerInfo) throws GenericSystemException;

    /**
     * Возвращает признак работающего таймера обработки системных событий
     *
     * @return true - запущен, иначе - false
     * @throws ru.effts.ine.core.GenericSystemException при возникновении ошибок на серверной стороне
     */
    boolean isTimerStarted() throws GenericSystemException;

    /** Запускает таймер обработки системных событий */
    void startTimer();

    /** Останавливает таймер обработки системных событий */
    void stopTimer();

    /** Внутренний системный пользователь, применяется для работы вне контекста сессии */
    SystemUser SYSTEM_USER_SYS = new SystemUser() {
        @Override
        public Object clone() throws CloneNotSupportedException {
            return super.clone();
        }

        @Override
        public boolean isActive() {
            return true;
        }

        @Override
        public void setActive(boolean active) {
        }

        @Override
        public void setCoreFd(Date fd) throws IneIllegalArgumentException {
        }

        @Override
        public Date getCoreFd() {
            return new Date();
        }

        @Override
        public void setCoreTd(Date td) throws IneIllegalArgumentException {
        }

        @Override
        public Date getCoreTd() {
            return new Date();
        }

        @Override
        public void setCoreId(BigDecimal id) throws IllegalIdException {
        }

        @Override
        public BigDecimal getCoreId() {
            return BigDecimal.ONE;
        }

        @Override
        public String getCoreDsc() {
            return "History performer";
        }

        @Override
        public void setCoreDsc(String dsc) {
        }

        @Override
        public String getLogin() {
            return "HistoryPerformer";
        }

        @Override
        public void setLogin(String login) {
        }

        @Override
        public String getMd5() {
            return null;
        }

        @Override
        public void setMd5(String md5) {
        }
    };

}
