/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.strings;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.strings.MessageString;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.strings.MessageString} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface MessageStringBean<T extends MessageString> {

    /**
     * Метод возвращает все строки локализации
     *
     * @param user пользователь, выполняющий запрос
     * @return коллекция строк локализации
     * @throws ru.effts.ine.core.CorruptedIdException
     *          при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException
     *          при некорректных значениях в датах системного объекта
     */
    Collection<T> getAllObjects(UserProfile user) throws CoreException;

    /**
     * Возвращает содержимое локализованного сообщения по указанным ключу и коду языка
     *
     * @param user      пользователь, выполняющий запрос
     * @param stringKey ключ сообщения
     * @param langCode  код языка
     * @return локализованное сообщение
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок в процессе получения данных
     */
    T getStringByKeyAndLang(UserProfile user, String stringKey, BigDecimal langCode) throws CoreException;

    /**
     * Возвращает карту локализованных сообщений по указанным ключу в присутствующих языках
     *
     * @param user      пользователь, выполняющий запрос
     * @param stringKey ключ сообщения
     * @return карта локализованных сообщений
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок в процессе получения данных
     */
    Map<BigDecimal, T> getStringsByKey(UserProfile user, String stringKey) throws CoreException;

    /**
     * Создает в системе новый объект
     *
     * @param user         пользователь, выполняющий запрос
     * @param identifiable новый объект
     * @return созданный объект
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    T createObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Изменяет свойства существующего в системе объекта
     *
     * @param user            пользователь, выполняющий запрос
     * @param identifiable    старый объект
     * @param newIdentifiable новый объект
     * @return модифицированный объект
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    T updateObject(UserProfile user, T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Удаляет существующий объект
     *
     * @param user         пользователь, выполняющий запрос
     * @param identifiable удаляемый объект
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    void deleteObject(UserProfile user, T identifiable) throws IneIllegalArgumentException, GenericSystemException;

}
