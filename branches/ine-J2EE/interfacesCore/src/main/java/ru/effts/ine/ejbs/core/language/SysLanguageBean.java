/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.language.SysLanguage} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface SysLanguageBean<T extends SysLanguage> extends VersionableBean<T> {
}
