/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;

import java.math.BigDecimal;

/**
 * Утилитный интерфейс, не предназначен для прикладной разработки!
 * Предоставляет методы для работы с FieldAccess
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: FieldAccessBean.java 1744 2011-03-30 15:23:38Z ikulkov $"
 */
public interface FieldAccessBean {

    /**
     * Возвращает имя класса, соответствующего переданному идентификатору таблицы
     *
     * @param tableId идентификатор таблицы
     * @return имя класса
     * @throws ru.effts.ine.core.CoreException в случае возникновения ошибок при доступе к мета-информации
     */
    String getInterface(BigDecimal tableId) throws CoreException;

    /**
     * Возвращает идентификатор системного объекта
     *
     * @param identifiableClass один из поддерживаемых интерфейсов
     * @return идентификатор системного объекта
     * @throws ru.effts.ine.core.CoreException в случае возникновения ошибок при доступе к мета-информации
     */
    BigDecimal getTableId(Class identifiableClass) throws CoreException;

    /**
     * Возвращает системный идентификатор для поля объекта
     *
     * @param interfaceName имя интерфейса
     * @param fieldName     название поля
     * @return идентификатор поля (системного объекта)
     * @throws ru.effts.ine.core.GenericSystemException при ощибках доступа к данным
     */
    public BigDecimal getFieldId(Class interfaceName, String fieldName) throws GenericSystemException;

}
