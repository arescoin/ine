/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.ipaddress;

import ru.effts.ine.core.ipaddress.IPAddressValue;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.ipaddress.IPAddressValue} объектам
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueBean.java 3219 2011-11-21 13:48:26Z ikulkov $"
 */
public interface IPAddressValueBean<T extends IPAddressValue> extends VersionableBean<T> {
}
