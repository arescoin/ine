/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import ru.effts.ine.ejbs.core.VersionableBean;
import ru.effts.ine.oss.entity.Company;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.oss.entity.Company} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CompanyBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface CompanyBean<T extends Company> extends VersionableBean<T> {
}
