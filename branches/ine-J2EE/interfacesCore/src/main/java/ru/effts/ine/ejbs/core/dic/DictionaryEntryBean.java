/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.dic.DictionaryEntry} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface DictionaryEntryBean<T extends DictionaryEntry> extends VersionableBean<T> {

    /**
     * Метод возвращает все действительные указанные словарные статьи для указанного словаря
     *
     * @param user         пользователь, запрашивающий словарные статьи
     * @param dictionaryId идентификатор словаря
     * @return коллекция словарных статей {@link ru.effts.ine.core.dic.DictionaryEntry}
     * @throws ru.effts.ine.core.GenericSystemException при ошибках получения и обработки данных
     */
    public Collection<T> getAllEntriesForDictionary(UserProfile user, BigDecimal dictionaryId)
            throws GenericSystemException;
}
