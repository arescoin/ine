/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.userpermits.Role;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.userpermits.Role} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RoleBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface RoleBean<T extends Role> extends VersionableBean<T> {
}
