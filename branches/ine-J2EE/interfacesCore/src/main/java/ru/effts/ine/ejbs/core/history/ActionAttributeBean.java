/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.history.ActionAttribute;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.ejbs.core.IdentifiableBean;

import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.history.ActionAttribute} объектам
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeBean.java 1602 2011-03-11 15:30:21Z ikulkov $"
 */
public interface ActionAttributeBean<T extends ActionAttribute> extends IdentifiableBean<T> {

    /**
     * Метод возвращает пользовательские атрибуты для переданной записи об изменениях в идентифицируемых объектах
     *
     * @param user     пользователь, запрашивающий атрибуты истории объекта
     * @param history запись об изменениях в идентифицируемом объекте
     * @return коллекция пользовательских атрибутов
     * @throws ru.effts.ine.core.GenericSystemException в случае ошибок при работе с БД, кешем и метаинформацией
     */
    Collection<T> getActionAttributes(UserProfile user, IdentifiableHistory history) throws GenericSystemException;
}
