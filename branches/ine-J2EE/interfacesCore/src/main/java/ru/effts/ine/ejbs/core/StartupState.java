/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

/**
 * Список состояний запуска системы.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: StartupState.java 2126 2011-05-13 13:23:30Z ikulkov $"
 */
public enum StartupState {
    /** Система в процессе запуска */
    IN_PROGRESS,
    /** Система успешно запущена */
    STARTED,
    /** Запуск системы завершился неуспешно */
    START_FAILED
}
