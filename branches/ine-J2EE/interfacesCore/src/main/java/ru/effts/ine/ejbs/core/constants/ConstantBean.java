/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import ru.effts.ine.core.constants.Constant;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.constants.Constant} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface ConstantBean<T extends Constant> extends VersionableBean<T> {
}
