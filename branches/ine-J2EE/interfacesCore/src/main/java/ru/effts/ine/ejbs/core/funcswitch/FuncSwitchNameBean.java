/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import ru.effts.ine.core.funcswitch.FuncSwitchName;
import ru.effts.ine.ejbs.core.IdentifiableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.funcswitch.FuncSwitchName} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface FuncSwitchNameBean<T extends FuncSwitchName> extends IdentifiableBean<T> {
}
