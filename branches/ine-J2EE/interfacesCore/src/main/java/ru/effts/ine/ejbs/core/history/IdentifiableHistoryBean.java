/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.ejbs.core.IdentifiableBean;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.history.IdentifiableHistory} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryBean.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public interface IdentifiableHistoryBean<T extends IdentifiableHistory> extends IdentifiableBean<T> {
    /**
     * Метод возвращает все записи об изменениях в идентифицируемого объекта
     *
     * @param user     пользователь, запрашивающий историю объекта
     * @param sysObjId идентификатор системного объекта
     * @param entityId идентификатор сущности
     * @return коллекция системных объектов
     * @throws ru.effts.ine.core.GenericSystemException при некорректых значениях
     */
    Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException;

}
