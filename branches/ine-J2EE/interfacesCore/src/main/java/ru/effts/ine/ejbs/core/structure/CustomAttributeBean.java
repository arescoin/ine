/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.ejbs.core.VersionableBean;

import java.util.Collection;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.structure.CustomAttribute} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeBean.java 1727 2011-03-29 15:10:22Z ikulkov $"
 */
public interface CustomAttributeBean<T extends CustomAttribute> extends VersionableBean<T> {

    /**
     * Возвращает список атрибутов конкретного объекта.
     *
     * @param user              пользователь, выполняющий запрос
     * @param identifiableClass класс интерфейса объекта, для которого получаются атрибуты.
     * @return коллекция описаний атрибутов
     * @throws ru.effts.ine.core.GenericSystemException при некорректных значениях в данных
     */
    Collection<T> getAttributesByObject(UserProfile user, Class<? extends Identifiable> identifiableClass)
            throws GenericSystemException;

    /**
     * Возвращает коллекцию {@link ru.effts.ine.core.structure.CustomAttribute настраиваемых атрибутов}
     * для переданного класса Identifiable-объекта, дополненную атрибутами переданного
     * {@link ru.effts.ine.core.structure.CustomType модифицированного типа}.
     *
     * @param user              пользователь, выполняющий запрос
     * @param identifiableClass класс реализации Identifiable-объекта.
     * @param customType        модифицированный тип. Может быть <code>null</code>.
     *                          Тогда вернутся атрибуты только базового объекта
     * @return коллекция {@link ru.effts.ine.core.structure.CustomAttribute настраиваемых атрибутов}
     * @throws ru.effts.ine.core.GenericSystemException в случае возникновения ошибок при доступе к мета-информации
     */
    Collection<CustomAttribute> getAttributes(UserProfile user, Class identifiableClass, CustomType customType)
            throws GenericSystemException;

    /**
     * Проверяет есть ли CustomAttibuteValue для описания CustomAttribute
     *
     * @param user пользователь, выполняющий запрос
     * @param t    объект на проверку
     * @return true если изпользуется
     * @throws GenericSystemException при ошибке доступа к данным
     */
    boolean isCustomAttributeUsed(UserProfile user, T t) throws GenericSystemException;

}
