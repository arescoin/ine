/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.ejbs.core.VersionableBean;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.core.structure.CustomAttributeValue} объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueBean.java 1727 2011-03-29 15:10:22Z ikulkov $"
 */
public interface CustomAttributeValueBean<T extends CustomAttributeValue> extends VersionableBean<T> {

    /**
     * Возвращает актуальный объект по набору идентификаторов
     * <p/>
     * Для получения значения custom-атрибута необходимо передать 2 индентификатора:
     * <ul>
     * <li>1-й id - идентификатор экземпляра объекта, для которого получается значение атрибута
     * <li>2-й id - идентификатор описателя custom-атрибута, по которому получается значение
     * </ul>
     * <b>Вместо прямого вызова этого метода настоятельно рекомендуется использовать
     * {@link #getValue(UserProfile, java.math.BigDecimal, ru.effts.ine.core.structure.CustomAttribute)}!</b>
     *
     * @param user пользователь выполняющий запрос
     * @param id   перечисление идентификаторов
     * @return актуальный {@link T объект}
     * @throws ru.effts.ine.core.GenericSystemException при возникновении ошибке в процессе получения и обработки данных
     * @see #getValue(UserProfile, java.math.BigDecimal, ru.effts.ine.core.structure.CustomAttribute)
     */
    @Override
    T getObjectById(UserProfile user, BigDecimal... id) throws GenericSystemException;

    /**
     * Получает значение конкретного атрибута
     *
     * @param user           пользователь выполняющий запрос
     * @param identifiableId идентификатор экземпляра объекта, для которого получается значение атрибута
     * @param attribute      описатель дополнительного атрибута
     * @return значение атрибута
     * @throws GenericSystemException при некорректных значениях в данных
     */
    T getValue(UserProfile user, BigDecimal identifiableId, CustomAttribute attribute) throws GenericSystemException;

    /**
     * Получает значения {@link ru.effts.ine.core.structure.CustomAttribute произвольных атрибутов} переданного
     * объекта, расширенного заданным {@link ru.effts.ine.core.structure.CustomType модифицированным типом}.
     *
     * @param user       пользователь выполняющий запрос
     * @param obj        переданный {@link ru.effts.ine.core.Identifiable объект}
     * @param customType модифицированный тип. Может быть <code>null</code>.
     *                   Тогда вернутся значения атрибутов только с базового объекта.
     * @return карта значений произвольных атрибутов объекта. ключом являются сами произвольные атрибуты.
     * @throws ru.effts.ine.core.GenericSystemException в случае возникновения ошибок при доступе к мета-информации
     */
    Map<CustomAttribute, CustomAttributeValue> getValues(UserProfile user, Identifiable obj, CustomType customType)
            throws GenericSystemException;

    /**
     * Создает значения {@link ru.effts.ine.core.structure.CustomAttribute произвольных атрибутов} для переданного
     * объекта.
     *
     * @param user       пользователь выполняющий запрос
     * @param obj        объект, для которого создаются значения произвольных атрибутов
     * @param customType кастом-тип, который расширяет переданный объект
     * @param values     создаваемые значения
     * @throws ru.effts.ine.core.GenericSystemException в него вложено ru.effts.ine.core.CoreException в случае
     * возникновения ошибок при доступе к мета-информации и ru.effts.ine.core.IneIllegalArgumentException  в случае
     * некорректных значений атрибутов. Значения должны принадлежать переданному объекту.
     */
    public void createValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException;

    /**
     * Изменяет значения {@link ru.effts.ine.core.structure.CustomAttribute произвольных атрибутов} для переданного
     * объекта.
     *
     * @param user       пользователь выполняющий запрос
     * @param obj        объект, для которого изменяются значения произвольных атрибутов
     * @param customType кастом-тип, который расширяет переданный объект
     * @param values     изменяемые значения
     * @throws ru.effts.ine.core.GenericSystemException в него вложено ru.effts.ine.core.CoreException в случае
     * возникновения ошибок при доступе к мета-информации и ru.effts.ine.core.IneIllegalArgumentException  в случае
     * некорректных значений атрибутов. Значения должны принадлежать переданному объекту.
     */
    public void updateValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException;

    /**
     * Удаляет переданные значения {@link ru.effts.ine.core.structure.CustomAttribute произвольных атрибутов} для
     * указанного объекта.
     *
     * @param user       пользователь выполняющий запрос
     * @param obj        объект, для которого удаляются значения произвольных атрибутов
     * @param customType кастом-тип, который расширяет переданный объект
     * @param values     удаляемые значения
     * @throws ru.effts.ine.core.GenericSystemException в него вложено ru.effts.ine.core.CoreException в случае
     * возникновения ошибок при доступе к мета-информации и ru.effts.ine.core.IneIllegalArgumentException при попытке
     * удаления обязательного атрибута или в случае некорректных значений атрибутов.
     * Значения должны принадлежать переданному объекту.
     */
    public void deleteValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException;

}
