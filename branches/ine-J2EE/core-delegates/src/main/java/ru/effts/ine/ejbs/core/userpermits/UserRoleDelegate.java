/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.userpermits.UserRole;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link UserRoleBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class UserRoleDelegate<T extends UserRole> extends VersionableDelegate<T> implements UserRoleBean<T> {

    public UserRoleDelegate() {
        init("stateless.UserRoleBean");
    }

    @Override
    @SuppressWarnings({"unchecked"})
    public Collection<T> getRolesByUserId(UserProfile user, BigDecimal userId) throws CoreException {
        try {
            return ((UserRoleBean<T>) lookup()).getRolesByUserId(user, userId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
