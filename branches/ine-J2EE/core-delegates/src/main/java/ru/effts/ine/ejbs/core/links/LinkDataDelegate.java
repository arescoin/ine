/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.links;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.Versionable;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link LinkDataBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class LinkDataDelegate<T extends LinkData> extends VersionableDelegate<T> implements LinkDataBean<T> {

    public LinkDataDelegate() {
        init("stateless.LinkDataBean");
    }

    @Override
    public <E extends Identifiable> Collection<E> getLinkedObjects(UserProfile user, Identifiable objectToLink,
            BigDecimal linkId) throws GenericSystemException {
        try {
            return ((LinkDataBean<T>) lookup()).getLinkedObjects(user, objectToLink, linkId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<T> getLinkDataByObject(UserProfile user, Identifiable linkedObject)
            throws GenericSystemException {
        try {
            return ((LinkDataBean<T>) lookup()).getLinkDataByObject(user, linkedObject);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<T> getLinkDataByObject(UserProfile user, Identifiable linkedObject, BigDecimal linkId)
            throws GenericSystemException {
        try {
            return ((LinkDataBean<T>) lookup()).getLinkDataByObject(user, linkedObject, linkId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public T createLinkData(UserProfile user, Versionable linkedObjectLeft, Versionable linkedObjectRight)
            throws GenericSystemException {
        try {
            return ((LinkDataBean<T>) lookup()).createLinkData(user, linkedObjectLeft, linkedObjectRight);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public T createLinkData(UserProfile user, Versionable linkedObjectLeft, Versionable linkedObjectRight, BigDecimal type)
            throws GenericSystemException {
        try {
            return ((LinkDataBean<T>) lookup()).createLinkData(user, linkedObjectLeft, linkedObjectRight, type);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
