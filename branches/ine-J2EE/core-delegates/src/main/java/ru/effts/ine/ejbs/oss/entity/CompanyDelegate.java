/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import ru.effts.ine.ejbs.core.VersionableDelegate;
import ru.effts.ine.oss.entity.Company;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CompanyBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CompanyDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class CompanyDelegate<T extends Company> extends VersionableDelegate<T> implements CompanyBean<T> {

    public CompanyDelegate() {
        init("stateless.CompanyBean");
    }
}
