/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.language.CountryDetails;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CountryDetailsBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class CountryDetailsDelegate<T extends CountryDetails> extends IdentifiableDelegate<T>
        implements CountryDetailsBean<T> {

    public CountryDetailsDelegate() {
        init("stateless.CountryDetailsBean");
    }
}
