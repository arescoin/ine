/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs;

import javassist.*;
import javassist.bytecode.AccessFlag;
import javassist.bytecode.MethodInfo;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;
import ru.effts.ine.ejbs.core.AbstractDelegate;
import ru.effts.ine.utils.BinaryMaskUtil;

import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Фабрика предоставляет возможность инстанционирования делегатов(предоставляющих доступ к EJB) по интерфейсам.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DelegateFactory.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public final class DelegateFactory {

    private static final Logger logger = Logger.getLogger(DelegateFactory.class.getName());

    public static final String DELEGATE_DEBUG_CALLING = "DELEGATE_DEBUG_CALLING";

    /** название package в котором лежат ossj интерфейсы */
    public static final String PCG_PREF_JAVAX_OSS = "javax.oss.";
    private static final String PCG_PREF_RU_EFFTS_INE = "ru.effts.ine.";
    private static final String EJB_PACKAGE_NAME = "ejbs.";
    private static final String DELEGATE_NAME = "Delegate";

    private static final boolean DEBUG_CALLING;

    private static final Map<String, Class> delegatesMap = new HashMap<String, Class>();

    private static final Map<String, Class> delegatesToDbgMap = new HashMap<String, Class>();

    private static ClassPool classPool;
    private static CtClass abstractDelegateCtClass;

    static {

        DEBUG_CALLING = Boolean.parseBoolean(System.getProperty(DELEGATE_DEBUG_CALLING, Boolean.toString(false)));

        if (DEBUG_CALLING) {
            classPool = ClassPool.getDefault();
            classPool.appendClassPath(new LoaderClassPath(DelegateFactory.class.getClassLoader()));

            try {
                abstractDelegateCtClass = classPool.get("ru.effts.ine.ejbs.core.AbstractDelegate");
                discoverLib();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * Метод возвращает экземпляр делегата для работы с объектами указанного интерфейса
     *
     * @param interfaceName имя интерфейса delegate для работы с объектами которого требуется
     * @return экземпляр конкретного делегата
     * @throws IllegalAccessException при недоступности класса в JVM
     * @throws InstantiationException если тип класса не позволяет создавать новые инстансы
     * @throws ClassNotFoundException если класс для delegate не найден в JVM
     */
    public static <T extends AbstractDelegate> T getDelegateForInterface(String interfaceName)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException {

        if (interfaceName == null) {
            throw new IneIllegalArgumentException("interfaceName value is null");
        }

        Class<? extends AbstractDelegate> clazz;

        if (!delegatesMap.containsKey(interfaceName)) {
            clazz = constructDelegateClass(interfaceName);
            delegatesMap.put(interfaceName, clazz);
        } else {
            clazz = delegatesMap.get(interfaceName);
        }

        return (T) clazz.newInstance();
    }

    /**
     * Метод возвращает экземпляр делегата для работы с объектами указанного интерфейса
     * <p/>
     * Принимает на вход наследников интерфейсов:
     * <ul>
     * <li> Versionable </li>
     * <li> AttributeAccess </li>
     * <li> IneManagedEntityValue </li>
     * <ul>
     *
     * @param clazz сласс интерфейса обекта для получения делегата
     * @return экземпляр конкретного делегата
     * @throws IllegalAccessException при недоступности класса в JVM
     * @throws InstantiationException если тип класса не позволяет создавать новые инстансы
     * @throws ClassNotFoundException если класс для delegate не найден в JVM
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public static <T extends AbstractDelegate> T obtainDelegateByInterface(Class clazz)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException {

        Class<? extends AbstractDelegate> result = null;

        if (!delegatesMap.containsKey(clazz.getName())) {

            if (Versionable.class.isAssignableFrom(clazz) || Identifiable.class.isAssignableFrom(clazz)
             /*&& AttributeAccess.class.isAssignableFrom(clazz)*/) {



               /* Class[] classes = clazz.getInterfaces();
                for (Class aClass : classes) {
                    if (*//*AttributeAccess.class.isAssignableFrom(aClass)
                            && *//*!Versionable.class.isAssignableFrom(aClass)) {

                        result = constructDelegateClass(aClass.getName());
                        break;
                    }
                }

            } else {*/
                result = constructDelegateClass(clazz.getName());
            }

            delegatesMap.put(clazz.getName(), result);

        } else {
            result = delegatesMap.get(clazz.getName());
        }


        if (result == null) {
            throw new IllegalArgumentException("Source interface not allowed [" + clazz.getName() + "]");
        }

        return (T) result.newInstance();
    }

    private static Class<? extends AbstractDelegate> constructDelegateClass(String className)
            throws ClassNotFoundException {

        if (className.startsWith(PCG_PREF_RU_EFFTS_INE)) {

            className = className.replaceFirst(PCG_PREF_RU_EFFTS_INE, PCG_PREF_RU_EFFTS_INE + EJB_PACKAGE_NAME)
                    + DELEGATE_NAME;

        } else if (className.startsWith(PCG_PREF_JAVAX_OSS)) {

            className = className.replaceFirst(PCG_PREF_JAVAX_OSS, PCG_PREF_RU_EFFTS_INE + EJB_PACKAGE_NAME)
                    + DELEGATE_NAME;
            className = className.replaceFirst("dbe.da", "");

        } else {
            throw new IneIllegalArgumentException("The class name must be an interface of OSS/J specification or be " +
                    "one of ine-core interfaces. But currently is " + className);
        }

        if (DEBUG_CALLING) {
            try {
                return obtainDebugClass(className);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        return (Class<? extends AbstractDelegate>) Class.forName(className);
    }

    private static synchronized void discoverLib() throws Exception {

        Class aClass = LoadPoint.class;
        logger.config("Load Class: " + aClass);

        URL url = aClass.getResource("/ru/effts/ine/ejbs/LoadPoint.class");
        logger.config("Get URL: " + url);

        String fullPath = url.getFile();
        logger.config("Work in path: " + fullPath);

        String jarPath = fullPath.substring(0, fullPath.indexOf("!/")).replaceAll("file:/", "/");
        ZipFile zf = new ZipFile(jarPath);
//        ZipFile zf = new ZipFile("/home/xerror/development/projects/suris/data/ine-J2EE/delegates/target/suris-delegates-0.2.7-SNAPSHOT.jar");

        Enumeration entries = zf.entries();

        while (entries.hasMoreElements()) {

            ZipEntry ze = (ZipEntry) entries.nextElement();

            if (!ze.isDirectory()
                    && ze.getName().endsWith(".class")
                    && !ze.getName().contains("LoadPoint.class")
                    && !ze.getName().contains("AbstractDelegate.class")
                    && !ze.getName().contains("DelegateFactory.class")) {
                processClass(zf, ze);
            }

        }
    }

    private static void processClass(ZipFile zf, ZipEntry ze) throws Exception {
        CtClass ctClass = classPool.makeClass(zf.getInputStream(ze), false);
        obtainDebugClass(ctClass);
    }

    private static synchronized Class<? extends AbstractDelegate> obtainDebugClass(CtClass ctClass)
            throws Exception {

        if (delegatesToDbgMap.containsKey(ctClass.getName())) {
            return delegatesToDbgMap.get(ctClass.getName());
        }

        if (ctClass.isFrozen()) {
            throw new RuntimeException(ctClass.getName() + ": frozen class (cannot edit)");
        }

        try {

            if (!abstractDelegateCtClass.getName().equals(ctClass.getName())) {
                obtainDebugClass(ctClass.getSuperclass());
            }

        } catch (Exception e) {
            throw new ClassNotFoundException("Can't find class by name: " + ctClass.getName(), e);
        }

        for (CtMethod ctMethod : ctClass.getDeclaredMethods()) {
            MethodInfo methodInfo = ctMethod.getMethodInfo();

            if (AccessFlag.isPublic(methodInfo.getAccessFlags())
                    && methodInfo.isMethod()
                    && !BinaryMaskUtil.maskToValues(
                    methodInfo.getAccessFlags()).contains(new Long(AccessFlag.STATIC))
                    ) {

                try {
                    insertDebugInfo(ctMethod);
                } catch (CannotCompileException e) {
                    e.printStackTrace();
                } catch (NotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            Class<? extends AbstractDelegate> result = (Class<? extends AbstractDelegate>) ctClass.toClass();
            logger.config("Process [" + ctClass.getName() + "] ok");
            delegatesToDbgMap.put(ctClass.getName(), result);

            return result;

        } catch (CannotCompileException e) {
            throw new RuntimeException(e);
        }
    }

    private static synchronized Class<? extends AbstractDelegate> obtainDebugClass(String className)
            throws Exception {

        if (delegatesToDbgMap.containsKey(className)) {
            return delegatesToDbgMap.get(className);
        }

        return obtainDebugClass(classPool.get(className));
    }

    private static void insertDebugInfo(CtMethod ctMethod) throws CannotCompileException, NotFoundException {

        ctMethod.addLocalVariable("startTime", classPool.get("long"));

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("System.out.println(System.currentTimeMillis() + \" Invoking Method For: " +
                "[\" + Thread.currentThread().getName() + \"] \" + this.getClass().getName() +" + "\" ");
        stringBuilder.append(ctMethod.getName()).append(" \"" + ");");
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append(" System.out.println(\"  With arguments: " +
                "[\" + Thread.currentThread().getName() + \"] \" + java.util.Arrays.deepToString($args));");
        stringBuilder.append(System.getProperty("line.separator"));
        stringBuilder.append("startTime = System.currentTimeMillis();");

        ctMethod.insertBefore(stringBuilder.toString());

        ctMethod.insertAfter("long resultTime = System.currentTimeMillis() - startTime;\n" +
                " System.out.println(System.currentTimeMillis() + \" Invocation Time: " +
                "[\" + Thread.currentThread().getName() + \"] \" + resultTime);");
    }

}
