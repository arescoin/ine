/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.links;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link LinkBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class LinkDelegate<T extends Link> extends VersionableDelegate<T> implements LinkBean<T> {

    public LinkDelegate() {
        init("stateless.LinkBean");
    }

    @Override
    public Collection<T> getLinksByObjType(UserProfile user, Class clazz) throws GenericSystemException {
        try {
            return ((LinkBean<T>) lookup()).getLinksByObjType(user, clazz);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public BigDecimal getLinkId(UserProfile user, Class leftType, Class rightType, long type)
            throws GenericSystemException {
        try {
            return ((LinkBean<T>) lookup()).getLinkId(user, leftType, rightType, type);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
