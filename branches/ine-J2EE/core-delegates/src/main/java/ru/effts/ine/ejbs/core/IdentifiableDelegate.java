/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.userpermits.CrudCode;
import ru.effts.ine.utils.cache.StorageFilterImpl;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public abstract class IdentifiableDelegate<T extends Identifiable>
        extends AbstractDelegate implements IdentifiableBean<T> {

    private static final HashMap<Class, SearchCriteriaProfile> PROFILE_MAPPING =
            new HashMap<Class, SearchCriteriaProfile>(50);

    private static final HashMap<Class, String> KEY_MAPPING = new HashMap<Class, String>(50);

    public Collection<T> getAllObjects(UserProfile user) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).getAllObjects(user);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public T getObjectById(UserProfile user, BigDecimal... id) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).getObjectById(user, id);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public SyntheticId getSyntheticId(UserProfile user, T identifiable) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).getSyntheticId(user, identifiable);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public T createObject(UserProfile user, T identifiable) throws GenericSystemException {
        try {
            user.getLastAction().setType(CrudCode.create);
            return ((IdentifiableBean<T>) lookup()).createObject(user, identifiable);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        } finally {
            user.clearUserAttributes();
        }
    }

    public T updateObject(UserProfile user, T newIdentifiable) throws GenericSystemException {
        try {
            user.getLastAction().setType(CrudCode.update);
            return ((IdentifiableBean<T>) lookup()).updateObject(user, newIdentifiable);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        } finally {
            user.clearUserAttributes();
        }
    }

    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {
        try {
            user.getLastAction().setType(CrudCode.delete);
            ((IdentifiableBean<T>) lookup()).deleteObject(user, identifiable);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        } finally {
            user.clearUserAttributes();
        }
    }

    public Set<String> getInstanceFields(UserProfile user) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).getInstanceFields(user);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public SearchCriteriaProfile getSearchCriteriaProfile(UserProfile user) throws GenericSystemException {

        if (!PROFILE_MAPPING.containsKey(this.getClass())) {
            synchronized (PROFILE_MAPPING) {
                if (!PROFILE_MAPPING.containsKey(this.getClass())) {

                    try {
                        PROFILE_MAPPING.put(
                                this.getClass(), ((IdentifiableBean<T>) lookup()).getSearchCriteriaProfile(user));
                    } catch (EJBException e) {
                        throw this.getCauseException(e);
                    }

                }
            }
        }

        return PROFILE_MAPPING.get(this.getClass());
    }

    public String getMainRegionKey(UserProfile user) throws GenericSystemException {

        if (!KEY_MAPPING.containsKey(this.getClass())) {
            synchronized (KEY_MAPPING) {
                if (!KEY_MAPPING.containsKey(this.getClass())) {

                    try {
                        KEY_MAPPING.put(this.getClass(), ((IdentifiableBean<T>) lookup()).getMainRegionKey(user));
                    } catch (EJBException e) {
                        throw this.getCauseException(e);
                    }
                }
            }
        }

        return KEY_MAPPING.get(this.getClass());
    }

    public StorageFilter createStorageFilter(UserProfile user, Set<SearchCriterion> criteria, String fieldName)
            throws GenericSystemException {
        try {
            return new StorageFilterImpl(criteria, fieldName, this.getMainRegionKey(user));
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public StorageFilter createStorageFilter(UserProfile user,
                                             Set<SearchCriterion> criteria, SearchCriterion criterion, String fieldName) throws GenericSystemException {
        try {
            return new StorageFilterImpl(criteria, criterion, fieldName, this.getMainRegionKey(user));
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public Collection<T> searchObjects(UserProfile user, Set<SearchCriterion> searchCriterias)
            throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).searchObjects(user, searchCriterias);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public Collection searchObjects(UserProfile user, Set<SearchCriterion> searchCriterias, String fieldName)
            throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).searchObjects(user, searchCriterias, fieldName);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public Collection searchObjects(UserProfile user, StorageFilter[] storageFilters) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).searchObjects(user, storageFilters);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public boolean containsObjects(UserProfile user, Set<SearchCriterion> searchCriteria)
            throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).containsObjects(user, searchCriteria);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public boolean containsObjects(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).containsObjects(user, filters);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public int getObjectCount(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).getObjectCount(user, searchCriteria);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public int getObjectCount(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        try {
            return ((IdentifiableBean<T>) lookup()).getObjectCount(user, filters);
        } catch (EJBException e) {
            throw this.getCauseException(e);
        }
    }

    public void startSystem() {
        throw new UnsupportedOperationException("This method is not for public use");
    }

    public StartupState getState() {
        throw new UnsupportedOperationException("This method is not for public use");
    }

}
