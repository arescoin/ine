/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CustomTypeBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class CustomTypeDelegate<T extends CustomType> extends VersionableDelegate<T>
        implements CustomTypeBean<T> {

    public CustomTypeDelegate() {
        init("stateless.CustomTypeBean");
    }

    @Override
    public Collection<String> getBaseObjects(UserProfile user) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getBaseObjects(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<T> getTypesForBaseObject(UserProfile user, BigDecimal baseObject) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getTypesForBaseObject(user, baseObject);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public boolean isCustomTypeUsed(UserProfile user, T customType) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).isCustomTypeUsed(user, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<CustomAttribute> getAllAttributes(UserProfile user, T customType) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getAllAttributes(user, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<CustomAttribute> getParentAttributes(UserProfile user, T customType) throws GenericSystemException {
        try {
            return ((CustomTypeBean<T>) lookup()).getParentAttributes(user, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
