/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link SysLanguageBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class SysLanguageDelegate<T extends SysLanguage> extends VersionableDelegate<T>
        implements SysLanguageBean<T> {

    public SysLanguageDelegate() {
        init("stateless.SysLanguageBean");
    }
}
