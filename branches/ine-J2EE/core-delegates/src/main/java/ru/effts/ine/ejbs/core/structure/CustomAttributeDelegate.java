/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link CustomAttributeBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class CustomAttributeDelegate<T extends CustomAttribute> extends VersionableDelegate<T>
        implements CustomAttributeBean<T> {

    public CustomAttributeDelegate() {
        init("stateless.CustomAttributeBean");
    }

    @Override
    public Collection<T> getAttributesByObject(UserProfile user, Class<? extends Identifiable> identifiableClass)
            throws GenericSystemException {
        try {
            return ((CustomAttributeBean<T>) lookup()).getAttributesByObject(user, identifiableClass);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<CustomAttribute> getAttributes(UserProfile user, Class identifiableClass, CustomType customType)
            throws GenericSystemException {
        try {
            return ((CustomAttributeBean<T>) lookup()).getAttributes(user, identifiableClass, customType);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public boolean isCustomAttributeUsed(UserProfile user, T t) throws GenericSystemException {
        try {
            return ((CustomAttributeBean<T>) lookup()).isCustomAttributeUsed(user, t);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

}
