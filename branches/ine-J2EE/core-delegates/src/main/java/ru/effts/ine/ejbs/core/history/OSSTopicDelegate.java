/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import javax.jms.*;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: OSSTopicDelegate.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public class OSSTopicDelegate extends VersionableHistoryDelegate {

    private static final Logger logger = Logger.getLogger(OSSTopicDelegate.class.getName());

    public OSSTopicDelegate() {
        super.init("jms/InETopicCF");
    }

    public void test() {
        try {
            TopicConnectionFactory connectionFactory = (TopicConnectionFactory) lookup();
            Destination destination = (Destination) lookup("InEOSSJTopicTop");
            TopicConnection topicConnection = connectionFactory.createTopicConnection();
            TopicSession session = topicConnection.createTopicSession(true, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer messageConsumer = session.createConsumer(destination);
            topicConnection.start();

            while (true) {
                Message message = messageConsumer.receive(500);
                if (message instanceof ObjectMessage) {
                    ObjectMessage objectMessage = (ObjectMessage) message;
                    logger.config("Received message: " + objectMessage.getObject());
                    break;
                }
            }

        } catch (JMSException e) {
            e.printStackTrace();
        }


    }
}
