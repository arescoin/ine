/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.SystemObject;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.util.Date;
import java.util.Set;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link SystemObjectBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class SystemObjectDelegate<T extends SystemObject> extends VersionableDelegate<T>
        implements SystemObjectBean<T> {

    public SystemObjectDelegate() {
        init("stateless.SystemObjectBean");
    }

    @Override
    public Set<String> getSupportedIntefaces(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getSupportedIntefaces(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Date getCurrentDate(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getCurrentDate(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Date getSystemFd(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getSystemFd(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Date getSystemTd(UserProfile user) throws GenericSystemException {
        try {
            return ((SystemObjectBean<T>) lookup()).getSystemTd(user);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

}
