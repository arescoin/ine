/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link IdentifiableHistoryBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class IdentifiableHistoryDelegate<T extends IdentifiableHistory> extends IdentifiableDelegate<T>
        implements IdentifiableHistoryBean<T> {

    public IdentifiableHistoryDelegate() {
        init("stateless.IdentifiableHistoryBean");
    }

    @Override
    public Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException {
        try {
            return ((IdentifiableHistoryBean<T>) lookup()).getActionsHistoryByIds(user, sysObjId, entityId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
