/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import ru.effts.ine.ejbs.core.VersionableDelegate;
import ru.effts.ine.oss.entity.Person;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link PersonBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class PersonDelegate<T extends Person> extends VersionableDelegate<T> implements PersonBean<T> {

    public PersonDelegate() {
        init("stateless.PersonBean");
    }
}
