/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.language.LangName;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link LangNameBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangNameDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class LangNameDelegate<T extends LangName> extends IdentifiableDelegate<T>
        implements LangNameBean<T> {

    public LangNameDelegate() {
        init("stateless.LangNameBean");
    }
}
