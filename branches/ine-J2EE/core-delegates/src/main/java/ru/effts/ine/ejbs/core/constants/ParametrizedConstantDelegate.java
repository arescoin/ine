/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link ParametrizedConstantBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class ParametrizedConstantDelegate<T extends ParametrizedConstant> extends VersionableDelegate<T>
        implements ParametrizedConstantBean<T> {

    public ParametrizedConstantDelegate() {
        init("stateless.ParametrizedConstantBean");
    }
}
