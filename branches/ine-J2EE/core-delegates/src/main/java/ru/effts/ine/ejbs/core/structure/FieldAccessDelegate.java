/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.ejbs.core.AbstractDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link FieldAccessBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FieldAccessDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class FieldAccessDelegate extends AbstractDelegate implements FieldAccessBean {

    public FieldAccessDelegate() {
        init("stateless.FieldAccessBean");
    }

    @Override
    public String getInterface(BigDecimal tableId) throws CoreException {
        try {
            return ((FieldAccessBean) lookup()).getInterface(tableId);
        } catch (
                EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public BigDecimal getTableId(Class identifiableClass) throws CoreException {
        try {
            return ((FieldAccessBean) lookup()).getTableId(identifiableClass);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public BigDecimal getFieldId(Class interfaceName, String fieldName) throws GenericSystemException {
        try {
            return ((FieldAccessBean) lookup()).getFieldId(interfaceName, fieldName);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

}
