/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import ru.effts.ine.core.constants.Constant;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link ConstantBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class ConstantDelegate<T extends Constant> extends VersionableDelegate<T>
        implements ConstantBean<T> {

    public ConstantDelegate() {
        init("stateless.ConstantBean");
    }
}
