/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.language.LangDetails;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link LangDetailsBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class LangDetailsDelegate<T extends LangDetails> extends IdentifiableDelegate<T>
        implements LangDetailsBean<T> {

    public LangDetailsDelegate() {
        init("stateless.LangDetailsBean");
    }
}
