/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link DictionaryEntryBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class DictionaryEntryDelegate<T extends DictionaryEntry> extends VersionableDelegate<T>
        implements DictionaryEntryBean<T> {

    public DictionaryEntryDelegate() {
        init("stateless.DictionaryEntryBean");
    }

    @Override
    public Collection<T> getAllEntriesForDictionary(UserProfile user, BigDecimal dictionaryId)
            throws GenericSystemException {
        try {
            return ((DictionaryEntryBean<T>) lookup()).getAllEntriesForDictionary(user, dictionaryId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
