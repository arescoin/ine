/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.ejbs.core.AbstractDelegate;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link SystemObjectPatternBean}.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectPatternDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class SystemObjectPatternDelegate extends AbstractDelegate implements SystemObjectPatternBean {

    public SystemObjectPatternDelegate() {
        init("stateless.SystemObjectPatternBean");
    }

    @Override
    public SystemObjectPattern checkPattern(UserProfile user, SystemObjectPattern pattern) throws CoreException {
        try {
            return ((SystemObjectPatternBean) lookup()).checkPattern(user, pattern);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Identifiable getObjectByPattern(UserProfile user, SystemObjectPattern pattern, BigDecimal value)
            throws CoreException {
        try {
            return ((SystemObjectPatternBean) lookup()).getObjectByPattern(user, pattern, value);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<Identifiable> getObjectsByPattern(
            UserProfile user, SystemObjectPattern pattern, BigDecimal value) throws CoreException {
        try {
            return ((SystemObjectPatternBean) lookup()).getObjectsByPattern(user, pattern, value);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Collection<Identifiable> getAvailableObjects(UserProfile user, SystemObjectPattern pattern)
            throws CoreException {
        try {
            return ((SystemObjectPatternBean) lookup()).getAvailableObjects(user, pattern);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Map<SystemObjectPattern, BigDecimal> getPatternByObject(UserProfile user, Identifiable object)
            throws CoreException {
        try {
            return ((SystemObjectPatternBean) lookup()).getPatternByObject(user, object);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
