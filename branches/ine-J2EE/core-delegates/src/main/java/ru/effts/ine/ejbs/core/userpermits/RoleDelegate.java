/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.userpermits.Role;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link RoleBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RoleDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class RoleDelegate<T extends Role> extends VersionableDelegate<T> implements RoleBean<T> {

    public RoleDelegate() {
        init("stateless.RoleBean");
    }
}
