/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.structure.CustomAttributeDefVal;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class CustomAttributeDefValDelegate<T extends CustomAttributeDefVal>
        extends VersionableDelegate<T> implements CustomAttributeDefValBean<T> {

    public CustomAttributeDefValDelegate() {
        init("stateless.CustomAttributeDefValBean");
    }
}
