/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.userpermits.Permit;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link PermitBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class PermitDelegate<T extends Permit> extends VersionableDelegate<T> implements PermitBean<T> {

    public PermitDelegate() {
        init("stateless.PermitBean");
    }
}
