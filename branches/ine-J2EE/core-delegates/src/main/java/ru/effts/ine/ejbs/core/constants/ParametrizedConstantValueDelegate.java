/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import ru.effts.ine.core.constants.ParametrizedConstantValue;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link ParametrizedConstantValueBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class ParametrizedConstantValueDelegate<T extends ParametrizedConstantValue> extends VersionableDelegate<T>
        implements ConstantValueBean<T> {

    public ParametrizedConstantValueDelegate() {
        init("stateless.ParametrizedConstantValueBean");
    }
}
