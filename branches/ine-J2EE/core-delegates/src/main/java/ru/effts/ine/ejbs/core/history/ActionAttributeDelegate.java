/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.history.ActionAttribute;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;

import javax.ejb.EJBException;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class ActionAttributeDelegate<T extends ActionAttribute> extends IdentifiableDelegate<T>
        implements ActionAttributeBean<T> {

    public ActionAttributeDelegate() {
        init("stateless.ActionAttributeBean");
    }

    @Override
    public Collection<T> getActionAttributes(UserProfile user, IdentifiableHistory history)
            throws GenericSystemException {
        try {
            return ((ActionAttributeBean<T>) lookup()).getActionAttributes(user, history);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
