/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.userpermits.UserPermit;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link UserPermitBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class UserPermitDelegate<T extends UserPermit> extends VersionableDelegate<T> implements UserPermitBean<T> {

    public UserPermitDelegate() {
        init("stateless.UserPermitBean");
    }
}
