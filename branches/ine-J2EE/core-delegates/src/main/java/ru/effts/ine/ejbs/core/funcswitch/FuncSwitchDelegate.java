/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import ru.effts.ine.core.funcswitch.FuncSwitch;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link FuncSwitchBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class FuncSwitchDelegate<T extends FuncSwitch> extends VersionableDelegate<T>
        implements FuncSwitchBean<T> {

    public FuncSwitchDelegate() {
        init("stateless.FuncSwitchBean");
    }
}
