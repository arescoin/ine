/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.userpermits.RolePermit;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link RolePermitBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class RolePermitDelegate<T extends RolePermit> extends VersionableDelegate<T> implements RolePermitBean<T> {

    public RolePermitDelegate() {
        init("stateless.RolePermitBean");
    }
}
