/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.Versionable;

import javax.ejb.EJBException;
import java.util.Collection;
import java.util.Date;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableDelegate.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public abstract class VersionableDelegate<T extends Versionable> extends IdentifiableDelegate<T>
        implements VersionableBean<T> {

    protected void init(String mappedName) {
        super.init(mappedName);
    }

    public Collection<T> getOldVersions(UserProfile user, T versionable)
            throws GenericSystemException {
        try {
            return ((VersionableBean<T>) lookup()).getOldVersions(user, versionable);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    public Collection<T> getOldVersions(UserProfile user, T versionable, Date fd, Date td)
            throws GenericSystemException {

        try {
            return ((VersionableBean<T>) lookup()).getOldVersions(user, versionable, fd, fd);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }
}
