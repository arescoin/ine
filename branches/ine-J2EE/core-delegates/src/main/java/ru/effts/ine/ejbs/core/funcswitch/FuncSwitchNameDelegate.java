/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import ru.effts.ine.core.funcswitch.FuncSwitchName;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link FuncSwitchNameBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class FuncSwitchNameDelegate<T extends FuncSwitchName> extends IdentifiableDelegate<T>
        implements FuncSwitchNameBean<T> {

    public FuncSwitchNameDelegate() {
        init("stateless.FuncSwitchNameBean");
    }
}
