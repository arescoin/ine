/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.Versionable;
import ru.effts.ine.core.history.VersionableHistory;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link VersionableHistoryBean}.
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryDelegate.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class VersionableHistoryDelegate<T extends VersionableHistory> extends IdentifiableHistoryDelegate<T>
        implements VersionableHistoryBean<T> {

    public VersionableHistoryDelegate() {
        init("stateless.VersionableHistoryBean");
    }

    @Override
    public Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException {
        try {
            return ((VersionableHistoryBean<T>) lookup()).getActionsHistoryByIds(user, sysObjId, entityId);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    @Override
    public Versionable getObjectVersion(UserProfile user, T versionableHistory) throws GenericSystemException {
        try {
            return ((VersionableHistoryBean<T>) lookup()).getObjectVersion(user, versionableHistory);
        } catch (EJBException e) {
            if (GenericSystemException.class.isInstance(e.getCause())) {
                throw (GenericSystemException) e.getCause();
            }
            throw GenericSystemException.toGeneric(e.getCause());
        }
    }
}
