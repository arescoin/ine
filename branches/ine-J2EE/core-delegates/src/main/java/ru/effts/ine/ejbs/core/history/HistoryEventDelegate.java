/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.history.HistoryEvent;

import javax.ejb.EJBException;
import java.io.Serializable;
import java.util.Collection;

/**
 * Предоставляет доступ к методам на {@link HistoryEventBean}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class HistoryEventDelegate<T extends HistoryEvent>
        extends VersionableHistoryDelegate<T> implements HistoryEventBean<T> {

    public HistoryEventDelegate() {
        init("stateless.HistoryEventBean");
    }

    /**
     * Возвращает колекцию событий требующих обработки
     * <p/>
     * Внимание! не предназначен для прикладной разработки!
     *
     * @return колекция событий
     * @throws ru.effts.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public Collection<T> getEventsForProcess() throws CoreException {
        try {
            return ((HistoryEventBean<T>) lookup()).getEventsForProcess();
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    /**
     * Возвращает колекцию событий подверженных обработке
     * <p/>
     * Внимание! не предназначен для прикладной разработки!
     *
     * @return колекция событий
     * @throws ru.effts.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public Collection<T> getProcessedEvents() throws CoreException {
        try {
            return ((HistoryEventBean<T>) lookup()).getProcessedEvents();
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    /**
     * Отмечает событие как обработанное
     * <p/>
     * Внимание! не предназначен для прикладной разработки!
     *
     * @param historyEvent событие
     * @throws ru.effts.ine.core.GenericSystemException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public void markAsProcessed(T historyEvent) throws GenericSystemException {
        try {
            ((HistoryEventBean<T>) lookup()).markAsProcessed(historyEvent);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    /**
     * Возвращает признак работающего таймера
     *
     * @param timerInfo идентификатор таймера
     * @return true - запущен, иначе - false
     * @throws IllegalArgumentException при попытке передать null
     * @throws ru.effts.ine.core.GenericSystemException при возникновении ошибок на серверной стороне
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public boolean isTimerStarted(Serializable timerInfo) throws GenericSystemException {
        try {
            return ((HistoryEventBean<T>) lookup()).isTimerStarted(timerInfo);
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    /**
     * Возвращает признак работающего таймера обработки системных событий
     *
     * @return true - запущен, иначе - false
     * @throws ru.effts.ine.core.GenericSystemException при возникновении ошибок на серверной стороне
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public boolean isTimerStarted() throws GenericSystemException {
        try {
            return ((HistoryEventBean<T>) lookup()).isTimerStarted();
        } catch (EJBException e) {
            throw getCauseException(e);
        }
    }

    /** Запускает таймер обработки системных событий */
    @SuppressWarnings({"unchecked"})
    @Override
    public void startTimer() {
        try {
            ((HistoryEventBean<T>) lookup()).startTimer();
        } catch (EJBException e) {
            e.printStackTrace();
        }
    }

    /** Останавливает таймер обработки системных событий */
    @SuppressWarnings({"unchecked"})
    @Override
    public void stopTimer() {
        try {
            ((HistoryEventBean<T>) lookup()).stopTimer();
        } catch (EJBException e) {
            e.printStackTrace();
        }
    }

}
