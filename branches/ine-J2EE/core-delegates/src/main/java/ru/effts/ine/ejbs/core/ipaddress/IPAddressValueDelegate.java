/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.ipaddress;

import ru.effts.ine.core.ipaddress.IPAddressValue;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * Класс скрывающий jndi доступ к ejb, выполняющему методы {@link IPAddressValueBean}.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class IPAddressValueDelegate<T extends IPAddressValue>
        extends VersionableDelegate<T> implements IPAddressValueBean<T> {

    public IPAddressValueDelegate() {
        init("stateless.IPAddressValueBean");
    }
}
