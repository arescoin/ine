/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.ejbs.DelegateFactory;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;
import ru.effts.ine.utils.BaseTest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: InitializationTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class InitializationTest extends BaseTest {

    public static final String TEST_PROPS_PATH =
            System.getProperty("user.dir") + File.separator + "initialcontext.properties";

    @Before
    public void cratePropertiesFile() throws IOException {

        Properties testProps = new Properties();
        testProps.setProperty("java.naming.factory.initial", "com.sun.enterprise.naming.SerialInitContextFactory");
        testProps.setProperty("java.naming.factory.url.pkgs", "com.sun.enterprise.naming");
        testProps.setProperty("java.naming.factory.state", "com.sun.corba.ee.impl.presentation.rmi.JNDIStateFactoryImpl");
        testProps.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
        testProps.setProperty("org.omg.CORBA.ORBInitialPort", "3700");

        File f = new File(TEST_PROPS_PATH);
        if (f.createNewFile()) {
            FileWriter fw = new FileWriter(f);
            testProps.store(fw, "#This file was created during unit testing. It can be deleted freely.");
            fw.close();
            f.deleteOnExit();
        }

        System.setProperty(IdentifiableDelegate.PROPERTY_NAME, TEST_PROPS_PATH);
    }

    @Test
    public void initTest() {
        try {
            IdentifiableDelegate id = DelegateFactory.getDelegateForInterface(ConstantValue.class.getName());
            Assert.assertNotNull(id);
        } catch (Exception e) {
            fail(e);
        }
    }

    @After
    public void deletePropertyFile() {
        System.clearProperty(IdentifiableDelegate.PROPERTY_NAME);
    }
}
