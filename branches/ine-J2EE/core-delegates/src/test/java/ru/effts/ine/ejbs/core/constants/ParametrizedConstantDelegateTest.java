/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;
import ru.effts.ine.ejbs.core.structure.FieldAccessDelegate;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantDelegateTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ParametrizedConstantDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            ParametrizedConstantDelegate<ParametrizedConstant> parametrizedConstantDelegate
                    = DelegateFactory.getDelegateForInterface("ru.effts.ine.core.constants.ParametrizedConstant");

            ParametrizedConstant parametrizedConstant =
                    IdentifiableFactory.getImplementation(ParametrizedConstant.class);
            parametrizedConstant.setCoreDsc("Description");
            parametrizedConstant.setName("TestParametrizedConstant");
//            parametrizedConstant.setType(BigDecimal.ONE);
            parametrizedConstant.setModifiable(true);
            parametrizedConstant.setNullable(true);
            parametrizedConstant.setMask(new SystemObjectPattern(
                    new FieldAccessDelegate().getFieldId(ParametrizedConstant.class, ParametrizedConstant.CORE_ID)));
            parametrizedConstant.setDefaultValue("1");

            parametrizedConstant = parametrizedConstantDelegate.createObject(user, parametrizedConstant);

            ParametrizedConstant returned = parametrizedConstantDelegate.getObjectById(
                    user, parametrizedConstant.getCoreId());
            Assert.assertEquals("TestParametrizedConstant", returned.getName());

            ParametrizedConstant cUpdated = parametrizedConstantDelegate.getObjectById(user, returned.getCoreId());
            cUpdated.setName("New Name");

            parametrizedConstantDelegate.updateObject(user, cUpdated);

            parametrizedConstant = parametrizedConstantDelegate.getObjectById(user, parametrizedConstant.getCoreId());
            Assert.assertEquals("New Name", parametrizedConstant.getName());

            //проверка что нельзя обновить устаревшей версией
            try {
                parametrizedConstant.setCoreDsc("");
                parametrizedConstantDelegate.updateObject(user, returned);
                Assert.fail();
            } catch (Exception ignored) {
            }

            parametrizedConstantDelegate.deleteObject(user, parametrizedConstant);
            if (parametrizedConstantDelegate.getObjectById(user, parametrizedConstant.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove ParametrizedConstant");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
