/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.core.language.CountryDetails;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;
import ru.effts.ine.ejbs.core.constants.ConstantValueDelegate;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class CountryDetailsDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4);

        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");
            CountryDetailsDelegate<CountryDetails> countryDetailsDelegate =
                    DelegateFactory.obtainDelegateByInterface(CountryDetails.class);

            CountryDetails countryDetails = IdentifiableFactory.getImplementation(CountryDetails.class);
            countryDetails.setCoreDsc("Description");
            countryDetails.setCoreId(BigDecimal.valueOf(8888l));
            countryDetails.setCodeA2("YY");
            countryDetails.setCodeA3("YY1");
            countryDetails.setName("TestCountryDetails");

            BigDecimal createdId = countryDetailsDelegate.createObject(user, countryDetails).getCoreId();

            countryDetails = countryDetailsDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestCountryDetails", countryDetails.getName());
            CountryDetails cUpdated = countryDetailsDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            countryDetailsDelegate.updateObject(user, cUpdated);

            countryDetails = countryDetailsDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", countryDetails.getName());

            countryDetailsDelegate.deleteObject(user, countryDetails);
            if (countryDetailsDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove CountryDetails");
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            ConstantValueDelegate<ConstantValue> bean = DelegateFactory.obtainDelegateByInterface(ConstantValue.class);
            ConstantValue constantValue = bean.getObjectById(user, constantId);
            constantValue.setValue(value);
            bean.updateObject(user, constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }
}
