/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import org.junit.Assert;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс для тестирования соответствующего EJB
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class DictionaryEntryDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            DictionaryEntryDelegate<DictionaryEntry> dictionaryEntryDelegate =
                    DelegateFactory.getDelegateForInterface("ru.effts.ine.core.dic.DictionaryEntry");
            Collection<DictionaryEntry> entries =
                    dictionaryEntryDelegate.getAllEntriesForDictionary(user, BigDecimal.ONE);

            DictionaryEntry dicEntry = entries.iterator().next();
            try {
                dictionaryEntryDelegate.createObject(user, dicEntry);
            } catch (Exception e) {
                Assert.assertTrue(e.getCause() instanceof UnsupportedOperationException);
            }
            try {
                dictionaryEntryDelegate.updateObject(user, dicEntry);
            } catch (Exception e) {
                Assert.assertTrue(e.getCause() instanceof UnsupportedOperationException);
            }

            int before = entries.size();
            dicEntry.setCoreId(new BigDecimal(9999));

            dictionaryEntryDelegate.deleteObject(user, dicEntry);
            Assert.assertEquals(before, dictionaryEntryDelegate.getAllEntriesForDictionary(
                    user, BigDecimal.ONE).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
