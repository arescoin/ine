/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.userpermits.Permit;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class PermitDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            PermitDelegate<Permit> testedDelegate = DelegateFactory.obtainDelegateByInterface(Permit.class);

            Permit permit = IdentifiableFactory.getImplementation(Permit.class);
            permit.setCoreDsc("Description");
            permit.setName("Name");
            permit.setMaskValue("Mask");
            permit.setValueRequired(false);

            BigDecimal createdId = testedDelegate.createObject(user, permit).getCoreId();
            permit = testedDelegate.getObjectById(user, createdId);

            Assert.assertEquals("Name", permit.getName());

            Permit cUpdated = testedDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Name", cUpdated.getName());
            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, permit);
                Assert.fail();
            } catch (Exception ignored) {
            }

            testedDelegate.deleteObject(user, permit);
            if (testedDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Permit");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
