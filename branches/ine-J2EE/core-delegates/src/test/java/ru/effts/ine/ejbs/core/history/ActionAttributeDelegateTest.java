/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.core.history.ActionAttribute;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;
import ru.effts.ine.ejbs.core.dic.DictionaryDelegate;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class ActionAttributeDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        String attrName = "TestAttributeName";
        String attrValue = "TestAttributeValue";
        try {
            if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS).isEnabled() ||
                    !FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_ACTION_ATTRIBUTES).isEnabled()) {
                return;
            }

            BigDecimal tableId = FieldAccess.getTableId(Dictionary.class);

            DictionaryDelegate<Dictionary> dictionaryBean = DelegateFactory.obtainDelegateByInterface(Dictionary.class);

            Dictionary dictionary = IdentifiableFactory.getImplementation(Dictionary.class);
            dictionary.setCoreDsc("Description");
            dictionary.setDicType(BigDecimal.ONE);
            dictionary.setEntryCodePolicy(BigDecimal.ONE);
            dictionary.setLocalizationPolicy(BigDecimal.ONE);
            dictionary.setName("ActionAttributeDelegateTestName");

            user.setUserAttribute(attrName, attrValue);

            dictionary = dictionaryBean.createObject(user, dictionary);

            IdentifiableHistoryDelegate<IdentifiableHistory> historyBean =
                    DelegateFactory.obtainDelegateByInterface(IdentifiableHistory.class);

            Collection<IdentifiableHistory> history = historyBean.getActionsHistoryByIds(
                    user, tableId, dictionaryBean.getSyntheticId(user, dictionary));
            Assert.assertEquals(1, history.size());

            ActionAttributeDelegate<ActionAttribute> attributeBean =
                    DelegateFactory.obtainDelegateByInterface(ActionAttribute.class);

            Collection<ActionAttribute> attributes = attributeBean.getActionAttributes(user, history.iterator().next());
            Assert.assertEquals(1, attributes.size());
            ActionAttribute attribute = attributes.iterator().next();
            Assert.assertEquals(attrName, attribute.getAttributeName());
            Assert.assertEquals(attrValue, attribute.getAttributeValue());

            dictionaryBean.deleteObject(user, dictionary);
        } catch (Exception e) {
            fail(e);
        }
    }
}
