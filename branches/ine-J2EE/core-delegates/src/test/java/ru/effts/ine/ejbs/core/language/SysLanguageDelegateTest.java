/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class SysLanguageDelegateTest extends BaseDelegateTest {

    private static final BigDecimal LANG_ID = BigDecimal.valueOf(9999l);

    protected void testDelegate() {

        final BigDecimal countryDetailsAndorra = BigDecimal.valueOf(20l);//андорра

        try {
            LangNameDelegate<LangName> langNameDelegate = DelegateFactory.obtainDelegateByInterface(LangName.class);
            SysLanguageDelegate<SysLanguage> sysLanguageDelegate =
                    DelegateFactory.obtainDelegateByInterface(SysLanguage.class);

            //создание языка удаленным бином
            LangName langName = IdentifiableFactory.getImplementation(LangName.class);
            langName.setCoreDsc("Description");
            langName.setCoreId(LANG_ID);
            langName.setName("TestLangName");

            langNameDelegate.createObject(user, langName);

            SysLanguage sysLanguage = IdentifiableFactory.getImplementation(SysLanguage.class);
            sysLanguage.setLangDetails(new BigDecimal(107));  // sq Албанский
            sysLanguage.setCountryDetails(new BigDecimal(8)); // AL Албания
            sysLanguage.setCoreId(LANG_ID);

            SysLanguage created = sysLanguageDelegate.createObject(user, sysLanguage);

            SyntheticId id = sysLanguageDelegate.getSyntheticId(user, created);
            sysLanguage = sysLanguageDelegate.getObjectById(user, id.getIdValues());
            sysLanguage.setCountryDetails(countryDetailsAndorra);

            sysLanguage = sysLanguageDelegate.updateObject(user, sysLanguage);

            Assert.assertEquals(countryDetailsAndorra, sysLanguage.getCountryDetails());

            //проверка что нельзя обновить устаревшей версией
            try {
                sysLanguageDelegate.updateObject(user, created);
                Assert.fail();
            } catch (Exception ignored) {
            }

            //удаление
            sysLanguageDelegate.deleteObject(user, sysLanguage);
            if (sysLanguageDelegate.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove SysLanguage");
            }
            langNameDelegate.deleteObject(user, langName);
        } catch (Exception e) {
            fail(e);
        }
    }
}
