/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.Constant;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantDelegateTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ConstantDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            ConstantDelegate<Constant> constantDelegate = DelegateFactory.obtainDelegateByInterface(Constant.class);

            Constant constant = IdentifiableFactory.getImplementation(Constant.class);
            constant.setCoreDsc("Description");
            constant.setName("TestConstant");
//            constant.setType(BigDecimal.ONE);
            constant.setModifiable(true);
            constant.setNullable(true);
            constant.setDefaultValue("Test");

            BigDecimal createdId = constantDelegate.createObject(user, constant).getCoreId();

            constant = constantDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestConstant", constant.getName());
            Constant cUpdated = constantDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            constantDelegate.updateObject(user, cUpdated);

            constant = constantDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", constant.getName());

            constantDelegate.deleteObject(user, constant);
            if (constantDelegate.getObjectById(user, createdId) != null) {
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testIllegalUpdate() {
        try {
            ConstantDelegate<Constant> bean = DelegateFactory.obtainDelegateByInterface(Constant.class);
            Constant object1 = bean.getObjectById(user, Identifiable.MIN_ALLOWABLE_VAL);
            Constant object2 = bean.getObjectById(user, Identifiable.MIN_ALLOWABLE_VAL);
            object1.setCoreDsc("");
            bean.updateObject(user, object1);
            //обновление устаревшей версией невозможно
            try {
                bean.updateObject(user, object2);
                Assert.fail();
            } catch (Exception ex) {
            }
        } catch (Exception e) {
            fail(e);
        }
    }

}
