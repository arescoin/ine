/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.UserRole;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleDelegateTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class UserRoleDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            UserRoleDelegate<UserRole> testedDelegate = DelegateFactory.obtainDelegateByInterface(UserRole.class);

            UserRole userRole = IdentifiableFactory.getImplementation(UserRole.class);
            userRole.setCoreId(new BigDecimal(2));
            userRole.setCoreDsc("Description");
            userRole.setActive(true);
            userRole.setUserId(new BigDecimal(2));

            userRole = testedDelegate.createObject(user, userRole);

            SyntheticId createdId = testedDelegate.getSyntheticId(user, userRole);
            userRole = testedDelegate.getObjectById(user, createdId.getIdValues());

            Assert.assertEquals(true, userRole.isActive());

            UserRole cUpdated = testedDelegate.getObjectById(user, createdId.getIdValues());
            cUpdated.setActive(false);

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals(false, cUpdated.isActive());

            try {
                testedDelegate.updateObject(user, userRole);
                Assert.fail();
            } catch (Exception ignored) {
            }

            //проверка что нельзя обновить устаревшей версией
            testedDelegate.deleteObject(user, cUpdated);
            if (testedDelegate.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove UserRole");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
