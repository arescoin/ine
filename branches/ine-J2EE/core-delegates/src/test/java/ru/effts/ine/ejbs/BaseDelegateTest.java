/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.ejbs.core.StartupState;
import ru.effts.ine.ejbs.core.SystemBean;
import ru.effts.ine.utils.BaseTest;
import ru.effts.ine.utils.config.ConfigurationManager;

import javax.naming.InitialContext;
import java.io.File;
import java.io.FileReader;
import java.util.Properties;

/**
 * Базовый класс для тестирования делегатов
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: BaseDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public abstract class BaseDelegateTest extends BaseTest {

    static {
        String propValue = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                SystemBean.class.getName() + "." + SystemBean.CHECK_SYSTEM_START_STATE_PROP_NAME, "true");

        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.getProperties().list(System.out);
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

        if (Boolean.parseBoolean(propValue)) {
            try {
                InitialContext context;

                if (System.getProperty("java.initialcontext.file.path") != null) {
                    //если переменная оружения установлена открываем указанный в ней путь
                    File propFile = new File(System.getProperty("java.initialcontext.file.path"));
                    Properties props = new Properties();
                    props.load(new FileReader(propFile));
                    context = new InitialContext(props);
                } else {
                    context = new InitialContext();
                }

                SystemBean bean = (SystemBean) context.lookup("stateless.SystemBean");
                bean.startSystem();

                while (true) {
                    StartupState state = bean.getState();
                    if (state == StartupState.STARTED) {
                        break;
                    } else if (state == StartupState.START_FAILED) {
                        Assert.fail("Startup failed. See server log-file for details.");
                    } else {
                        Thread.sleep(1000);
                    }
                }
            } catch (Exception e) {
                fail(e);
            }
        }
    }

    protected boolean testingRequired = true;//для отключения в наследниках тестирования

    protected abstract void testDelegate();

    @Test
    public void test() {

        if (!testingRequired) {
            return;
        }

        testDelegate();
    }
}
