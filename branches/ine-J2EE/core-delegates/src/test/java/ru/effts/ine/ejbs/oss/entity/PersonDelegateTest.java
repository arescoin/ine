/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;
import ru.effts.ine.oss.entity.Person;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */

public class PersonDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            Person person = IdentifiableFactory.getImplementation(Person.class);
            person.setCoreDsc("Description");
            person.setName("Name");
            person.setFamilyName("familyName");
            person.setMiddleName("middleName");
            person.setAlias("alias");
            person.setFamilyNamePrefixCode(BigDecimal.ONE);
            person.setFamilyGeneration("familyGeneration");
            person.setFormOfAddress("formOfAddress");

            PersonDelegate<Person> personDelegate = DelegateFactory.getDelegateForInterface(Person.class.getName());

            BigDecimal createdId = personDelegate.createObject(user, person).getCoreId();
            person = personDelegate.getObjectById(user, createdId);
            Assert.assertEquals("Name", person.getName());
            Person cUpdated = personDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            cUpdated = personDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Name", cUpdated.getName());

            //проверка что нельзя обновить устаревшей версией
            try {
                personDelegate.updateObject(user, person);
                Assert.fail();
            } catch (Exception ex) {
            }
            personDelegate.deleteObject(user, person);
            if (personDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Person");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
