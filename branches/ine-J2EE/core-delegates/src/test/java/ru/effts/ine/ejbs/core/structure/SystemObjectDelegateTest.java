/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import org.junit.Assert;
import ru.effts.ine.core.structure.SystemObject;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class SystemObjectDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            SystemObjectDelegate<SystemObject> testedDelegate =
                    DelegateFactory.obtainDelegateByInterface(SystemObject.class);
            Assert.assertTrue(testedDelegate.getSupportedIntefaces(user).contains(TestObject.class.getName()));
        } catch (Exception e) {
            fail(e);
        }
    }
}
