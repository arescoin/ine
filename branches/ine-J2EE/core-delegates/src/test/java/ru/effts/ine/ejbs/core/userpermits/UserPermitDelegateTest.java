/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.UserPermit;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class UserPermitDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            UserPermitDelegate<UserPermit> testedDelegate = DelegateFactory.obtainDelegateByInterface(UserPermit.class);

            UserPermit userPermit = IdentifiableFactory.getImplementation(UserPermit.class);
            userPermit.setCoreId(BigDecimal.ONE);
            userPermit.setCoreDsc("Description");
            userPermit.setActive(true);
            userPermit.setUserId(new BigDecimal(2)); // тестовый юзверь Test User
            userPermit.setRoleId(new BigDecimal(2)); // тестовая роль NOBODY
            userPermit.setValues(new BigDecimal[]{new BigDecimal(1)});

            userPermit = testedDelegate.createObject(user, userPermit);

            SyntheticId createdId = testedDelegate.getSyntheticId(user, userPermit);
            userPermit = testedDelegate.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(true, userPermit.isActive());

            UserPermit cUpdated = testedDelegate.getObjectById(user, createdId.getIdValues());
            cUpdated.setActive(false);

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals(false, cUpdated.isActive());
            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, userPermit);
                Assert.fail();
            } catch (Exception ignored) {
            }

            testedDelegate.deleteObject(user, cUpdated);
            if (testedDelegate.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove UserPermit");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
