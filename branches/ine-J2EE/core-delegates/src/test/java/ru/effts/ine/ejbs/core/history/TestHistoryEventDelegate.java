/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.utils.BaseTest;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestHistoryEventDelegate.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class TestHistoryEventDelegate extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testStartStopTimer() {
        HistoryEventDelegate delegate = new HistoryEventDelegate();
        try {
            if (delegate.isTimerStarted()) {
                delegate.stopTimer();
            }
            Assert.assertFalse("Timer must be stopped", delegate.isTimerStarted());

            delegate.startTimer();
            Assert.assertTrue("Timer must be started", delegate.isTimerStarted());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void startTimer() {
        HistoryEventDelegate delegate = new HistoryEventDelegate();
        try {
            delegate.startTimer();
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void stopTimer() {
        HistoryEventDelegate delegate = new HistoryEventDelegate();
        try {
            delegate.stopTimer();
        } catch (Exception e) {
            fail(e);
        }
    }

}
