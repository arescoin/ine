/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.funcswitch.FuncSwitch;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class FuncSwitchDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            FuncSwitchDelegate<FuncSwitch> funcSwitchDelegate =
                    DelegateFactory.obtainDelegateByInterface(FuncSwitch.class);

            FuncSwitch funcSwitch = IdentifiableFactory.getImplementation(FuncSwitch.class);
            funcSwitch.setCoreDsc("Description");
            funcSwitch.setEnabled(true);
            funcSwitch.setChangeDate(new Date());
            funcSwitch.setMark(BigDecimal.ONE);
            funcSwitch.setStateCode("test");
            funcSwitch.setCoreId(BigDecimal.ONE);

            FuncSwitch created = funcSwitchDelegate.createObject(user, funcSwitch);

            SyntheticId id = funcSwitchDelegate.getSyntheticId(user, created);
            funcSwitch = funcSwitchDelegate.getObjectById(user, id.getIdValues());
            Assert.assertEquals("test", funcSwitch.getStateCode());
            funcSwitch.setStateCode("New test");

            funcSwitch = funcSwitchDelegate.updateObject(user, funcSwitch);

            Assert.assertEquals("New test", funcSwitch.getStateCode());

            //проверка что нельзя обновить устаревшей версией
            try {
                created.setCoreDsc("");
                funcSwitchDelegate.updateObject(user, created);
                Assert.fail();
            } catch (Exception ignored) {
            }

            funcSwitchDelegate.deleteObject(user, funcSwitch);
            if (funcSwitchDelegate.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove FuncSwitch");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
