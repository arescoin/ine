/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.RolePermit;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class RolePermitDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            RolePermitDelegate<RolePermit> testedDelegate = DelegateFactory.obtainDelegateByInterface(RolePermit.class);

            RolePermit rolePermit = IdentifiableFactory.getImplementation(RolePermit.class);
            rolePermit.setCoreDsc("Description");
            rolePermit.setCoreId(new BigDecimal(2)); // тестовая роль NOBODY
            rolePermit.setPermitId(new BigDecimal(2)); // тестовый доступ System's Objects Access
            rolePermit.setPermitValues(new BigDecimal[]{new BigDecimal(1)}); // некое тестовое значение

            rolePermit = testedDelegate.createObject(user, rolePermit);

            SyntheticId createdId = testedDelegate.getSyntheticId(user, rolePermit);
            rolePermit = testedDelegate.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("Description", rolePermit.getCoreDsc());

            RolePermit cUpdated = testedDelegate.getObjectById(user, createdId.getIdValues());
            cUpdated.setCoreDsc("New Description");

            cUpdated = testedDelegate.updateObject(user, cUpdated);

            Assert.assertEquals("New Description", cUpdated.getCoreDsc());

            //проверка что нельзя обновить устаревшей версией
            try {
                testedDelegate.updateObject(user, rolePermit);
                Assert.fail();
            } catch (Exception ignored) {
            }

            testedDelegate.deleteObject(user, rolePermit);
            if (testedDelegate.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove RolePermit");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
