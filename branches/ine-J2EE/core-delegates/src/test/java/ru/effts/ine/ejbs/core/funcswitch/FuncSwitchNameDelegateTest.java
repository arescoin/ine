/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.funcswitch.FuncSwitchName;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class FuncSwitchNameDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            FuncSwitchNameDelegate<FuncSwitchName> funcSwitchNameDelegate =
                    DelegateFactory.obtainDelegateByInterface(FuncSwitchName.class);

            FuncSwitchName funcSwitchName = IdentifiableFactory.getImplementation(FuncSwitchName.class);
            funcSwitchName.setCoreDsc("Description");
            funcSwitchName.setName("TestFuncSwitchName");

            BigDecimal createdId = funcSwitchNameDelegate.createObject(user, funcSwitchName).getCoreId();

            funcSwitchName = funcSwitchNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestFuncSwitchName", funcSwitchName.getName());

            FuncSwitchName cUpdated = funcSwitchNameDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            funcSwitchNameDelegate.updateObject(user, cUpdated);

            funcSwitchName = funcSwitchNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", funcSwitchName.getName());

            funcSwitchNameDelegate.deleteObject(user, funcSwitchName);
            if (funcSwitchNameDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove FuncSwitchName");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
