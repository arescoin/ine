/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangNameDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class LangNameDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            LangNameDelegate<LangName> langNameDelegate = DelegateFactory.obtainDelegateByInterface(LangName.class);

            LangName langName = IdentifiableFactory.getImplementation(LangName.class);
            langName.setCoreDsc("Description");
            langName.setCoreId(BigDecimal.valueOf(8888l));
            langName.setName("TestLangName");
            BigDecimal createdId = langNameDelegate.createObject(user, langName).getCoreId();

            langName = langNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("TestLangName", langName.getName());

            LangName cUpdated = langNameDelegate.getObjectById(user, createdId);
            cUpdated.setName("New Name");

            langNameDelegate.updateObject(user, cUpdated);

            langName = langNameDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Name", langName.getName());

            langNameDelegate.deleteObject(user, langName);
            if (langNameDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove LangName");
            }
        } catch (Exception e) {
            fail(e);
        }
    }

}
