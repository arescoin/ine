/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import org.junit.Assert;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneNotActualVersionModificationException;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class ConstantValueDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {
        try {
            ConstantValueDelegate<ConstantValue> constantValueDelegate =
                    DelegateFactory.obtainDelegateByInterface(ConstantValue.class);

            ConstantValue constantValue = IdentifiableFactory.getImplementation(ConstantValue.class);
            constantValue.setCoreDsc("Description");
            constantValue.setValue("Test");
            constantValue.setCoreId(new BigDecimal(1l));

            BigDecimal createdId = constantValueDelegate.createObject(user, constantValue).getCoreId();

            constantValue = constantValueDelegate.getObjectById(user, createdId);
            Assert.assertEquals("Test", constantValue.getValue());
            ConstantValue cUpdated = constantValueDelegate.getObjectById(user, createdId);
            cUpdated.setValue("New Value");

            constantValueDelegate.updateObject(user, cUpdated);

            cUpdated = constantValueDelegate.getObjectById(user, createdId);
            Assert.assertEquals("New Value", cUpdated.getValue());

            //проверка что нельзя обновить устаревшей версией
            try {
                constantValueDelegate.updateObject(user, constantValue);
                Assert.fail();
            } catch (IneNotActualVersionModificationException ignored) {
            } catch (Exception e) {
                fail(e);
            }

            constantValueDelegate.deleteObject(user, constantValue);
            if (constantValueDelegate.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove ConstantValue");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
