/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneNotActualVersionModificationException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class DictionaryTermDelegateTest extends BaseDelegateTest {

    private static final BigDecimal RU = new BigDecimal(1);
    private static final BigDecimal EN = new BigDecimal(2);

    protected void testDelegate() {

        final BigDecimal dictionaryId = BigDecimal.ONE;

        try {
            DictionaryTermDelegate<DictionaryTerm> bean =
                    DelegateFactory.getDelegateForInterface(DictionaryTerm.class.getName());

            //исходное количество терминов
            int before = bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size();

            //создание нового
            DictionaryTerm dicTerm = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            dicTerm.setDictionaryId(dictionaryId);
            dicTerm.setLangCode(EN);
            dicTerm.setTerm("TestTerm for delegate test 1");
            dicTerm.setCoreDsc("Description");
            DictionaryTerm created = bean.createObject(user, dicTerm);

            //получение созданного и проверка данных
            SyntheticId id = bean.getSyntheticId(user, created);
            Assert.assertEquals(before + 1, bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size());
            dicTerm = bean.getObjectById(user, id.getIdValues());
            Assert.assertEquals("TestTerm for delegate test 1", dicTerm.getTerm());

            //обновление
            DictionaryTerm dicTermUpdated = bean.getObjectById(user, id.getIdValues());
            dicTermUpdated.setTerm("New TestTerm for delegate test 1");

            bean.updateObject(user, dicTermUpdated);

            dicTerm = bean.getObjectById(user, id.getIdValues());
            Assert.assertEquals("New TestTerm for delegate test 1", dicTerm.getTerm());

            //проверка что нельзя обновить устаревшей версией
            try {
                created.setCoreDsc("");
                bean.updateObject(user, created);
                Assert.fail();
            } catch (IneNotActualVersionModificationException ignored) {
            } catch (Exception ex) {
                fail(ex);
            }

            //удаление
            bean.deleteObject(user, dicTerm);
            Assert.assertEquals(before, bean.getAllTermsForDictionaryAndLanguage(
                    user, dictionaryId, EN).size());
            if (bean.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove DictionaryTerm");
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testCreateFullyFillDicTerms() {
        final BigDecimal dictionaryId = new BigDecimal(8);
        try {
            DictionaryTermDelegate<DictionaryTerm> bean =
                    DelegateFactory.getDelegateForInterface(DictionaryTerm.class.getName());

            int termsRU = bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, RU).size();
            int termsEN = bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size();

            Collection<DictionaryTerm> terms = new ArrayList<DictionaryTerm>();

            DictionaryTerm term = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            term.setDictionaryId(dictionaryId);
            term.setLangCode(RU);
            term.setTerm("TestTerm for delegate test 2");
            terms.add(term);

            try {
                bean.createDictionaryTerms(user, terms);
            } catch (Exception e) {
                Assert.assertTrue("Failed to catch expected exception", e instanceof GenericSystemException);
            }

            term = IdentifiableFactory.getImplementation((DictionaryTerm.class));
            term.setDictionaryId(dictionaryId);
            term.setLangCode(EN);
            term.setTerm("TestTerm for delegate test 2");
            terms.add(term);

            BigDecimal newCode = bean.createDictionaryTerms(user, terms);

            Assert.assertEquals("Failed to create dictionary terms", termsRU + 1,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, RU).size());
            Assert.assertEquals("Failed to create dictionary terms", termsEN + 1,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size());

            try {
                bean.deleteObject(user, term);
            } catch (Exception e) {
                Assert.assertTrue("Failed to catch expected exception. Need manual delete of dictionary term[" +
                        newCode + "] in dictionary " + dictionaryId, e instanceof GenericSystemException);
            }
            term = bean.getObjectById(user, dictionaryId, newCode, RU);
            new DictionaryEntryDelegate<DictionaryEntry>().deleteObject(user, term);

            Assert.assertEquals("Failed to delete dictionary terms. Need manual delete of dictionary term[" +
                    newCode + "] in dictionary " + dictionaryId, termsRU,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, RU).size());
            Assert.assertEquals("Failed to delete dictionary terms. Need manual delete of dictionary term[" +
                    newCode + "] in dictionary " + dictionaryId, termsEN,
                    bean.getAllTermsForDictionaryAndLanguage(user, dictionaryId, EN).size());

        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testAddTerm() {
        final BigDecimal dictionaryId = BigDecimal.ONE;
        try {
            DictionaryTermDelegate<DictionaryTerm> bean =
                    DelegateFactory.getDelegateForInterface(DictionaryTerm.class.getName());

            DictionaryTerm termRU = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            termRU.setDictionaryId(dictionaryId);
            termRU.setLangCode(RU);
            termRU.setTerm("TestTerm for delegate test 3");

            termRU = bean.createObject(user, termRU);

            Assert.assertEquals("Failed to create dictionary term", 1,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, termRU.getCoreId()).size());

            DictionaryTerm termEN = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            termEN.setCoreId(termRU.getCoreId());
            termEN.setDictionaryId(dictionaryId);
            termEN.setLangCode(EN);
            termEN.setTerm("TestTerm for delegate test 3");

            termEN = bean.addObject(user, termEN);

            Assert.assertEquals("Failed to add dictionary term", 2,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, termEN.getCoreId()).size());

            DictionaryEntryDelegate<DictionaryTerm> entryBean =
                    DelegateFactory.getDelegateForInterface(DictionaryEntry.class.getName());
            entryBean.deleteObject(user, termRU);
        } catch (Exception e) {
            fail(e);
}
    }

    @Test
    public void testAddTerms() {
        final BigDecimal dictionaryId = BigDecimal.ONE;
        try {
            DictionaryTermDelegate<DictionaryTerm> bean =
                    DelegateFactory.getDelegateForInterface(DictionaryTerm.class.getName());

            Collection<DictionaryTerm> terms = new ArrayList<DictionaryTerm>();

            DictionaryTerm term = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            term.setDictionaryId(dictionaryId);
            term.setLangCode(RU);
            term.setTerm("TestTerm for delegate test 4");
            terms.add(term);

            BigDecimal newCode = bean.createDictionaryTerms(user, terms);

            Assert.assertEquals("Failed to create dictionary terms", 1,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, newCode).size());

            term = IdentifiableFactory.getImplementation(DictionaryTerm.class);
            term.setCoreId(newCode);
            term.setDictionaryId(dictionaryId);
            term.setLangCode(EN);
            term.setTerm("TestTerm for delegate test 4");
            terms.clear();
            terms.add(term);

            bean.addDictionaryTerms(user, terms);

            Assert.assertEquals("Failed to add dictionary term", 2,
                    bean.getAllTermsForDictionaryAndEntry(user, dictionaryId, newCode).size());

            term = bean.getObjectById(user, dictionaryId, newCode, RU);
            DictionaryEntryDelegate<DictionaryTerm> entryBean =
                    DelegateFactory.getDelegateForInterface(DictionaryEntry.class.getName());
            entryBean.deleteObject(user, term);
        } catch (Exception e) {
            fail(e);
        }
    }
}
