/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.constants.Constant;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;
import ru.effts.ine.ejbs.core.constants.ConstantDelegate;
import ru.effts.ine.utils.BaseTest;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DelegateFactoryTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class DelegateFactoryTest extends BaseTest {

    @Test
    public void testFactory() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        IdentifiableDelegate delegate = DelegateFactory.getDelegateForInterface(Constant.class.getName());
        Assert.assertTrue(ConstantDelegate.class.isInstance(delegate));

//        delegate = DelegateFactory.getDelegateForInterface(AssociationRule.class.getName());
//        Assert.assertTrue(AssociationRuleDelegate.class.isInstance(delegate));
    }
}
