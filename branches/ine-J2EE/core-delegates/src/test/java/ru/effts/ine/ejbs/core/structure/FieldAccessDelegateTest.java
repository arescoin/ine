/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import org.junit.Assert;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.ejbs.BaseDelegateTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FieldAccessDelegateTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class FieldAccessDelegateTest extends BaseDelegateTest {

    protected void testDelegate() {

        try {
            FieldAccessDelegate testedDelegate = (FieldAccessDelegate)
                    Class.forName("ru.effts.ine.ejbs.core.structure.FieldAccessDelegate").newInstance();


            BigDecimal id = testedDelegate.getTableId(TestObject.class);
            Assert.assertTrue(id.longValue() > 0);

            Assert.assertEquals(TestObject.class.getName(), testedDelegate.getInterface(id));

        } catch (Exception e) {
            fail(e);
        }

    }
}
