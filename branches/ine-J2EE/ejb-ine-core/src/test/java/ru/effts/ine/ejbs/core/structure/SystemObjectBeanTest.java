/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.structure.SystemObject;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@SuppressWarnings({"unchecked"})
public class SystemObjectBeanTest extends BaseTest {

    @Test
    public void testInterfaces() {
        try {
            //проверяемый bean
            SystemObjectBean<SystemObject> bean = (SystemObjectBean<SystemObject>) new SystemObjectBeanImpl();
            Assert.assertTrue(bean.getSupportedIntefaces(user).contains(TestObject.class.getName()));
        } catch (Exception e) {
            fail(e);
        }
    }
}
