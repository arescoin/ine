/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantBeanTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ParametrizedConstantBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testParametrizedConstantBeanLocal() {
        try {
            ParametrizedConstantBean<ParametrizedConstant> bean =
                    new ParametrizedConstantBeanImpl<ParametrizedConstant>();
            //создание нового описания парамтрезированной константы
            ParametrizedConstant object = IdentifiableFactory.getImplementation(ParametrizedConstant.class);
            object.setCoreDsc("Description");
            object.setName("TestParametrizedConstant");
//            object.setType(BigDecimal.ONE);
            object.setModifiable(true);
            object.setNullable(true);
            object.setDefaultValue("Test");
            BigDecimal createdId = bean.createObject(user, object).getCoreId();

            //получение и проверка
            ParametrizedConstant returned = bean.getObjectById(user, createdId);
            Assert.assertEquals("TestParametrizedConstant", returned.getName());

            //обновление
            ParametrizedConstant cUpdated = bean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            bean.updateObject(user, cUpdated);
            //проверка что обновилось
            object = bean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", object.getName());

            //удаление
            bean.deleteObject(user, object);
            if (bean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove ParametrizedConstant");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
