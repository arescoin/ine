/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.funcswitch.FuncSwitchName;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class FuncSwitchNameBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testFuncSwitchBeanLocal() {
        try {
            FuncSwitchNameBean<FuncSwitchName> bean = new FuncSwitchNameBeanImpl<FuncSwitchName>();
            //создаем новый объект
            FuncSwitchName object = IdentifiableFactory.getImplementation(FuncSwitchName.class);
            object.setCoreDsc("Description");
            object.setName("TestFuncSwitchName");
            BigDecimal createdId = bean.createObject(user, object).getCoreId();
            //получаем созданный объект и провреям данные
            object = bean.getObjectById(user, createdId);
            Assert.assertEquals("TestFuncSwitchName", object.getName());

            //обновление
            FuncSwitchName cUpdated = bean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            bean.updateObject(user, cUpdated);
            //получаем обновленный объект и провреям данные
            object = bean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", object.getName());

            //удаление
            bean.deleteObject(user, object);
            if (bean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove FuncSwitchName");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
