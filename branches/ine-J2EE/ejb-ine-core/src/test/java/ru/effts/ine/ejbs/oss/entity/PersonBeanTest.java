/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.oss.entity.Person;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class PersonBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testPersonBeanLocal() {

        try {
            //проверяемый bean
            PersonBean<Person> checkedBean = new PersonBeanImpl<Person>();
            //получение пустого объекта
            Person person = IdentifiableFactory.getImplementation(Person.class);
            //наполнение тестовыми данными
            person.setCoreDsc("Description");
            person.setName("Name");
            person.setFamilyName("familyName");
            person.setMiddleName("middleName");
            person.setAlias("alias");
            person.setFamilyNamePrefixCode(BigDecimal.ONE);
            person.setFamilyGeneration("familyGeneration");
            person.setFormOfAddress("formOfAddress");

            //создание
            BigDecimal createdId = checkedBean.createObject(user, person).getCoreId();
            //получение и проверка данных
            person = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", person.getName());

            //обновление
            Person cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            person = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", person.getName());

            //удаление
            checkedBean.deleteObject(user, person);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Person object");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
