/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.UserRole;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleBeanTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class UserRoleBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testUserRoleBeanLocal() {
        try {
            //проверяемый bean
            UserRoleBean<UserRole> checkedBean = new UserRoleBeanImpl<UserRole>();
            //получение пустого объекта
            UserRole userRole = IdentifiableFactory.getImplementation(UserRole.class);
            //наполнение тестовыми данными
            userRole.setCoreId(new BigDecimal(2));
            userRole.setCoreDsc("Description");
            userRole.setActive(true);
            userRole.setUserId(new BigDecimal(2));

            //создание
            userRole = checkedBean.createObject(user, userRole);
            //получение и проверка данных
            SyntheticId createdId = checkedBean.getSyntheticId(user, userRole);
            userRole = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(true, userRole.isActive());

            //обновление
            UserRole cUpdated = checkedBean.getObjectById(user, createdId.getIdValues());
            cUpdated.setActive(false);
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            userRole = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(false, userRole.isActive());

            //удаление
            checkedBean.deleteObject(user, userRole);
            if (checkedBean.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove UserRole");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
