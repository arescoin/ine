/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.oss.entity.Company;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CompanyBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class CompanyBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testCompanyBeanLocal() {
        try {
            //проверяемый checkedBean
            CompanyBean<Company> checkedBean = new CompanyBeanImpl<Company>();
            //получение пустого объекта
            Company company = IdentifiableFactory.getImplementation(Company.class);
            //наполнение тестовыми данными
            company.setCoreDsc("Description");
            company.setName("Name");
            company.setType(BigDecimal.ONE);
            company.setPropertyType(BigDecimal.ONE);

            //создание
            BigDecimal createdId = checkedBean.createObject(user, company).getCoreId();
            //получение и проверка данных
            company = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", company.getName());

            //обновление
            Company cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            company = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", company.getName());

            //удаление
            checkedBean.deleteObject(user, company);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Company object");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
