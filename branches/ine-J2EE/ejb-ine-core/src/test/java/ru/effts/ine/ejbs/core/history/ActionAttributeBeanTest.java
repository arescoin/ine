package ru.effts.ine.ejbs.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.core.history.ActionAttribute;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.ejbs.core.dic.DictionaryBean;
import ru.effts.ine.ejbs.core.dic.DictionaryBeanImpl;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeBeanTest.java 1655 2011-03-17 15:33:38Z ikulkov $"
 */
public class ActionAttributeBeanTest extends BaseTest {

    @Test
    public void testGetActionAttributes() {
        String attrName = "TestAttributeName";
        String attrValue = "TestAttributeValue";
        try {
            if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS).isEnabled() ||
                    !FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_ACTION_ATTRIBUTES).isEnabled()) {
                return;
            }

            BigDecimal tableId = FieldAccess.getTableId(Dictionary.class);

            DictionaryBean<Dictionary> dictionaryBean = new DictionaryBeanImpl<Dictionary>();

            Dictionary dictionary = IdentifiableFactory.getImplementation(Dictionary.class);
            dictionary.setCoreDsc("Description");
            dictionary.setDicType(BigDecimal.ONE);
            dictionary.setEntryCodePolicy(BigDecimal.ONE);
            dictionary.setLocalizationPolicy(BigDecimal.ONE);
            dictionary.setName("ActionAttributeBeanTestName");

            user.setUserAttribute(attrName, attrValue);

            dictionary = dictionaryBean.createObject(user, dictionary);

            IdentifiableHistoryBean<IdentifiableHistory> historyBean =
                    new IdentifiableHistoryBeanImpl<IdentifiableHistory>();
            Collection<IdentifiableHistory> history = historyBean.getActionsHistoryByIds(
                    user, tableId, dictionaryBean.getSyntheticId(user, dictionary));
            Assert.assertEquals(1, history.size());

            ActionAttributeBean<ActionAttribute> attributeBean = new ActionAttributeBeanImpl<ActionAttribute>();

            Collection<ActionAttribute> attributes = attributeBean.getActionAttributes(user, history.iterator().next());
            Assert.assertEquals(1, attributes.size());
            ActionAttribute attribute = attributes.iterator().next();
            Assert.assertEquals(attrName, attribute.getAttributeName());
            Assert.assertEquals(attrValue, attribute.getAttributeValue());

            dictionaryBean.deleteObject(user, dictionary);

            user.clearUserAttributes();
        } catch (Exception e) {
            fail(e);
        }
    }
}
