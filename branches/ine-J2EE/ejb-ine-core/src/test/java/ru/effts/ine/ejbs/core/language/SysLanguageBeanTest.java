/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.language.LangNameAccess;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageBeanTest.java 1933 2011-04-14 16:19:05Z dgomon $"
 */
public class SysLanguageBeanTest extends BaseTest {

    private static final BigDecimal LANG_ID = BigDecimal.valueOf(99999l);

    @Test
    @SuppressWarnings({"unchecked"})
    public void testSysLanguageBeanLocal() {
        try {
            LangNameAccess<LangName> access =
                    (LangNameAccess<LangName>) AccessFactory.getImplementation(LangName.class);
            LangName name = IdentifiableFactory.getImplementation(LangName.class);
            name.setCoreId(LANG_ID);
            name.setCoreDsc("TestObject for CRUD-operations testing");
            name.setName("LANG NAME");
            UserHolder.getUser().setReason("Common CRUD testing: create; for class: " + access.getClass().getName());
            access.createObject(name);
            
            //проверяемый bean
            SysLanguageBean<SysLanguage> checkedBean = new SysLanguageBeanImpl<SysLanguage>();
            //создание
            SysLanguage object = IdentifiableFactory.getImplementation(SysLanguage.class);
            object.setLangDetails(new BigDecimal(107));  // sq Албанский
            object.setCountryDetails(new BigDecimal(8)); // AL Албания
            object.setCoreId(LANG_ID);
            SysLanguage created = checkedBean.createObject(user, object);

            //обновление
            SyntheticId id = checkedBean.getSyntheticId(user, created);
            object = checkedBean.getObjectById(user, id.getIdValues());

            //удаление системного языка и имени языка
            checkedBean.deleteObject(user, object);
            if (checkedBean.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove SysLanguage");
            }
            access.deleteObject(access.getObjectById(LANG_ID));
        } catch (Exception e) {
            fail(e);
        }
    }
}
