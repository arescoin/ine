/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.UserPermit;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitBeanTest.java 3240 2011-11-23 12:15:41Z ikulkov $"
 */
public class UserPermitBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testUserPermitBeanLocal() {
        try {
            //проверяемый bean
            UserPermitBean<UserPermit> checkedBean = new UserPermitBeanImpl<UserPermit>();
            //получение пустого объекта
            UserPermit userPermit = IdentifiableFactory.getImplementation(UserPermit.class);
            //наполнение тестовыми данными
            userPermit.setCoreId(BigDecimal.ONE);
            userPermit.setCoreDsc("Description");
            userPermit.setActive(true);
            userPermit.setUserId(new BigDecimal(2)); // тестовый юзверь Test User
            userPermit.setRoleId(new BigDecimal(2)); // тестовая роль NOBODY
            userPermit.setValues(new BigDecimal[]{new BigDecimal(1)});

            //создание
            userPermit = checkedBean.createObject(user, userPermit);
            //получение и проверка данных
            SyntheticId createdId = checkedBean.getSyntheticId(user, userPermit);
            userPermit = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(true, userPermit.isActive());

            //обновление
            UserPermit cUpdated = checkedBean.getObjectById(user, createdId.getIdValues());
            cUpdated.setActive(false);
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            userPermit = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals(false, userPermit.isActive());

            //удаление
            checkedBean.deleteObject(user, userPermit);
            if (checkedBean.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove UserPermit");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
