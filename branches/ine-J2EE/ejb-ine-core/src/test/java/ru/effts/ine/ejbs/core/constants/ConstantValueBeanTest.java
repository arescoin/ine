/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class ConstantValueBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testConstantValueBeanLocal() {
        try {
            ConstantValueBean<ConstantValue> bean = new ConstantValueBeanImpl<ConstantValue>();
            //создание нового значения константы
            ConstantValue object = IdentifiableFactory.getImplementation(ConstantValue.class);
            object.setCoreDsc("Description");
            object.setValue("Test");
            object.setCoreId(new BigDecimal(1l));
            BigDecimal createdId = bean.createObject(user, object).getCoreId();

            //получение и проверка созадданного значения константы 
            object = bean.getObjectById(user, createdId);
            Assert.assertEquals("Test", object.getValue());
            ConstantValue cUpdated = bean.getObjectById(user, createdId);

            //обновление
            cUpdated.setValue("New Value");
            bean.updateObject(user, cUpdated);
            object = bean.getObjectById(user, createdId);
            Assert.assertEquals("New Value", object.getValue());

            //удаление
            bean.deleteObject(user, object);
            if (bean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove ConstantValue");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
