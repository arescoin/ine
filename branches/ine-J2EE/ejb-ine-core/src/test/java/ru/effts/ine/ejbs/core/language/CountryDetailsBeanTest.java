/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.core.language.CountryDetails;
import ru.effts.ine.ejbs.core.constants.ConstantValueBean;
import ru.effts.ine.ejbs.core.constants.ConstantValueBeanImpl;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class CountryDetailsBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testCountryDetailsBeanLocal() {
        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4);
        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");

            //проверяемый bean
            CountryDetailsBean<CountryDetails> checkedBean = new CountryDetailsBeanImpl<CountryDetails>();

            //создание
            CountryDetails countryDetails = IdentifiableFactory.getImplementation(CountryDetails.class);
            countryDetails.setCoreId(BigDecimal.valueOf(8888l));
            countryDetails.setCodeA2("YY");
            countryDetails.setCodeA3("YY1");
            countryDetails.setCoreDsc("Description");
            countryDetails.setName("TestCountryDetails");
            //получение созданного и проверка данных
            BigDecimal createdId = checkedBean.createObject(user, countryDetails).getCoreId();
            countryDetails = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("TestCountryDetails", countryDetails.getName());

            //обновление
            CountryDetails cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение обновленного и проверка данных
            countryDetails = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", countryDetails.getName());

            //удаление
            checkedBean.deleteObject(user, countryDetails);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove CountryDetails");
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            ConstantValueBean<ConstantValue> bean = new ConstantValueBeanImpl<ConstantValue>();
            ConstantValue constantValue = bean.getObjectById(user, constantId);
            constantValue.setValue(value);
            bean.updateObject(user, constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }
}
