/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.userpermits.SystemUser;
import ru.effts.ine.utils.BaseTest;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserBeanTest.java 3486 2011-12-12 15:21:41Z ikulkov $"
 */
public class SystemUserBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testSystemUserBeanLocal() {
        try {
            //проверяемый bean
            SystemUserBean<SystemUser> checkedBean = new SystemUserBeanImpl<SystemUser>();
            //получение пустого объекта
            SystemUser systemUser = IdentifiableFactory.getImplementation(SystemUser.class);
            //наполнение тестовыми данными
            systemUser.setCoreDsc("Description");
            systemUser.setLogin("test");
            systemUser.setMd5("98f6bcd4621d373cade4e832627b4f6");//от пароля 'test'

            //создание
            BigDecimal createdId = checkedBean.createObject(user, systemUser).getCoreId();
            //получение и проверка данных
            systemUser = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("test", systemUser.getLogin());

            //обновление
            SystemUser cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setLogin("New Login");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            systemUser = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Login", systemUser.getLogin());
            //поиск по обновленным данным
            SearchCriteriaProfile searchCriteriaProfile = checkedBean.getSearchCriteriaProfile(user);
            SearchCriterion<String> searchCriterion = (SearchCriterion<String>)
                    searchCriteriaProfile.getSearchCriterion(SystemUser.SYS_LOGIN);
            searchCriterion.setCriterionRule(CriteriaRule.equals);
            searchCriterion.setInvert(false);
            searchCriterion.addCriteriaVal("New Login");
            Set<SearchCriterion> searchCriterias = new HashSet<SearchCriterion>();
            searchCriterias.add(searchCriterion);
            Collection<SystemUser> col = checkedBean.searchObjects(user, searchCriterias);
            //провреяем что нашелся и только один
            Assert.assertEquals(1, col.size());
            Assert.assertEquals(systemUser, col.iterator().next());
            Assert.assertEquals(systemUser, checkedBean.login("New Login", systemUser.getMd5()));

            //удаление
            checkedBean.deleteObject(user, systemUser);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove SystemUser");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
