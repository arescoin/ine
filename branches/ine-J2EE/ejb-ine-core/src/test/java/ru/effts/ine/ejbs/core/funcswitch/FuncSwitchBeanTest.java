/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneCorruptedVersionableDateException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.funcswitch.FuncSwitch;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class FuncSwitchBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testFuncSwitchBeanLocal() {
        try {
            FuncSwitchBean<FuncSwitch> bean = new FuncSwitchBeanImpl<FuncSwitch>();
            //создаем новый объект
            FuncSwitch funcSwitch = IdentifiableFactory.getImplementation(FuncSwitch.class);
            funcSwitch.setCoreDsc("Description");
            funcSwitch.setEnabled(true);
            funcSwitch.setChangeDate(new Date());
            funcSwitch.setMark(BigDecimal.ONE);
            funcSwitch.setStateCode("test");
            funcSwitch.setCoreId(BigDecimal.ONE);
            FuncSwitch created = bean.createObject(user, funcSwitch);

            //получаем созданный объект и провреям данные
            SyntheticId id = bean.getSyntheticId(user, created);
            funcSwitch = bean.getObjectById(user, id.getIdValues());
            Assert.assertEquals("test", funcSwitch.getStateCode());

            //обновляем
            funcSwitch.setStateCode("New test");
            funcSwitch = bean.updateObject(user, funcSwitch);
            //получаем обновленный объект и провреям данные
            Assert.assertEquals("New test", funcSwitch.getStateCode());

            //удаление
            bean.deleteObject(user, funcSwitch);
            if (bean.getObjectById(user, id.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove FuncSwitch");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
