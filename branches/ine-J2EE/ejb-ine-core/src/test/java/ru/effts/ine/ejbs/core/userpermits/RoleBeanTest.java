/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.userpermits.Role;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RoleBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class RoleBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testRoleBeanLocal() {
        try {
            //проверяемый bean
            RoleBean<Role> checkedBean = new RoleBeanImpl<Role>();
            //получение пустого объекта
            Role role = IdentifiableFactory.getImplementation(Role.class);
            //наполнение тестовыми данными
            role.setCoreDsc("Description");
            role.setName("Name");

            //создание
            BigDecimal createdId = checkedBean.createObject(user, role).getCoreId();
            //получение и проверка данных
            role = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", role.getName());

            //обновление
            Role cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            role = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", role.getName());

            //удаление
            checkedBean.deleteObject(user, role);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove RoleBean");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
