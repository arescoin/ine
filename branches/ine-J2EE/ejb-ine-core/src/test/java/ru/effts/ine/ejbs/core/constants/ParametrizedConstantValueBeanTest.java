/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.core.constants.ParametrizedConstantValue;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.utils.BaseTest;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueBeanTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ParametrizedConstantValueBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testParametrizedConstantValueBeanLocal() {
        try {

            ParametrizedConstant obj02 = IdentifiableFactory.getImplementation(ParametrizedConstant.class);

            obj02.setCoreDsc("TestObject for CRUD-operations testing");
            obj02.setCoreFd(FieldAccess.getSystemFd());
            obj02.setCoreTd(FieldAccess.getSystemTd());
            obj02.setName("TEST_PARAMETRIZED_CONSTANT2");
            obj02.setModifiable(true);
            obj02.setNullable(true);
            obj02.setDefaultValue("1");
            obj02.setMask(new SystemObjectPattern(FieldAccess.getColumnId("TEST_TABLE", "n")));

            obj02 = new ParametrizedConstantBeanImpl<>().createObject(user, obj02);

            ParametrizedConstantValueBean<ParametrizedConstantValue> bean =
                    new ParametrizedConstantValueBeanImpl<>();

            //создание нового значения парамтрезированной константы
            ParametrizedConstantValue object = IdentifiableFactory.getImplementation(ParametrizedConstantValue.class);
            object.setCoreDsc("Description");
            object.setValue("Test");
            object.setCoreId(obj02.getCoreId());
            object.setParam(Identifiable.MIN_ALLOWABLE_VAL);
            ParametrizedConstantValue created = bean.createObject(user, object);

            //получение и проврка созданного значения
            SyntheticId createdId = bean.getSyntheticId(user, created);
            object = bean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("Test", object.getValue());

            //обновление
            ParametrizedConstantValue cUpdated = bean.getObjectById(user, createdId.getIdValues());
            cUpdated.setValue("New Value");
            bean.updateObject(user, cUpdated);
            object = bean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("New Value", object.getValue());

            //удаление
            bean.deleteObject(user, object);
            if (bean.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove ParametrizedConstantValue");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
