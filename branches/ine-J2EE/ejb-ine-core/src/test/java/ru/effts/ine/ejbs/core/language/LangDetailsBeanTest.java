/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.core.language.LangDetails;
import ru.effts.ine.ejbs.core.constants.ConstantValueBean;
import ru.effts.ine.ejbs.core.constants.ConstantValueBeanImpl;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsBeanTest.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public class LangDetailsBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testLangDetailsBeanLocal() {
        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4L);
        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");

            //проверяемый bean
            LangDetailsBean<LangDetails> checkedBean = new LangDetailsBeanImpl<LangDetails>();

            //создание
            LangDetails langDetails = IdentifiableFactory.getImplementation(LangDetails.class);
            langDetails.setCoreId(BigDecimal.valueOf(8888l));
            langDetails.setCode("rr");
            langDetails.setCoreDsc("Description");
            langDetails.setName("TestLangDetails");
            BigDecimal createdId = checkedBean.createObject(user, langDetails).getCoreId();

            //получение созданного и проверка данных
            langDetails = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("TestLangDetails", langDetails.getName());

            //обновление
            LangDetails cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение обновленного и проверка данных
            langDetails = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", langDetails.getName());

            //удаление
            checkedBean.deleteObject(user, langDetails);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove LangDetails");
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            ConstantValueBean<ConstantValue> bean = new ConstantValueBeanImpl<ConstantValue>();
            ConstantValue constantValue = bean.getObjectById(user, constantId);
            Thread.currentThread().sleep(1005);
            constantValue.setValue(value);
            bean.updateObject(user, constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }

}
