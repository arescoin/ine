/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.RolePermit;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitBeanTest.java 3240 2011-11-23 12:15:41Z ikulkov $"
 */
public class RolePermitBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testRolePermitBeanLocal() {
        try {
            //проверяемый bean
            RolePermitBean<RolePermit> checkedBean = new RolePermitBeanImpl<RolePermit>();
            //получение пустого объекта
            RolePermit rolePermit = IdentifiableFactory.getImplementation(RolePermit.class);
            //наполнение тестовыми данными
            rolePermit.setCoreDsc("Description");
            rolePermit.setCoreId(new BigDecimal(2)); // тестовая роль NOBODY
            rolePermit.setPermitId(new BigDecimal(2)); // тестовый доступ System's Objects Access
            rolePermit.setPermitValues(new BigDecimal[]{new BigDecimal(1)}); // некое тестовое значение

            //создание
            SyntheticId createdId = checkedBean.getSyntheticId(user, checkedBean.createObject(user, rolePermit));
            //получение и проверка данных
            rolePermit = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("Description", rolePermit.getCoreDsc());

            //обновление
            RolePermit cUpdated = checkedBean.getObjectById(user, createdId.getIdValues());
            cUpdated.setCoreDsc("New Description");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            rolePermit = checkedBean.getObjectById(user, createdId.getIdValues());
            Assert.assertEquals("New Description", rolePermit.getCoreDsc());

            //удаление
            checkedBean.deleteObject(user, rolePermit);
            if (checkedBean.getObjectById(user, createdId.getIdValues()) != null) {
                Assert.fail("method deleteObject() has failed to remove RolePermitBean");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
