/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class DictionaryBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testDictionaryBeanLocal() {
        try {
            DictionaryBean<Dictionary> dictionaryBean = new DictionaryBeanImpl<Dictionary>();
            //создание нового словаря
            Dictionary dictionary = IdentifiableFactory.getImplementation(Dictionary.class);
            dictionary.setCoreDsc("Description");
            dictionary.setDicType(BigDecimal.ONE);
            dictionary.setEntryCodePolicy(BigDecimal.ONE);
            dictionary.setLocalizationPolicy(BigDecimal.ONE);
            dictionary.setName("Name");
            BigDecimal createdId = dictionaryBean.createObject(user, dictionary).getCoreId();

            //получение созданного и проверка данных
            dictionary = dictionaryBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", dictionary.getName());

            //обновление
            Dictionary dicUpdated = dictionaryBean.getObjectById(user, createdId);
            dicUpdated.setName("New Name");
            dictionaryBean.updateObject(user, dicUpdated);
            //получение обновленного и проверка данных
            dictionary = dictionaryBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", dictionary.getName());

            //удаление
            dictionaryBean.deleteObject(user, dictionary);
            if (dictionaryBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove Dictionary");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
