/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.userpermits.Permit;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class PermitBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testPermitBeanLocal() {
        try {
            //проверяемый bean
            PermitBean<Permit> checkedBean = new PermitBeanImpl<Permit>();
            //получение пустого объекта
            Permit permit = IdentifiableFactory.getImplementation(Permit.class);
            //наполнение тестовыми данными
            permit.setCoreDsc("Description");
            permit.setName("Name");
            permit.setMaskValue("Mask");
            permit.setValueRequired(false);

            //создание
            BigDecimal createdId = checkedBean.createObject(user, permit).getCoreId();
            //получение и проверка данных
            permit = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("Name", permit.getName());

            //обновление
            Permit cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            //получение и проверка данных
            permit = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("New Name", permit.getName());

            //удаление
            checkedBean.deleteObject(user, permit);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove PermitBean");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
