/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeBeanTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
@Ignore
public class CustomAttributeBeanTest extends BaseTest {

    private static final String CORE_DSC = "CustomAttribute CRUD-test via bean-local";
    private static final String ATTRIBUTE_NAME = "CustomAttributeBeanTest AttributeName";
    private BigDecimal testObjId;

    @Before
    public void init() throws Exception {
        testObjId = FieldAccess.getTableId(TestObject.class);
    }

    @Test
    @SuppressWarnings({"unchecked"})
    public void testCustomAttributeBeanLocal() {
        try {
            //проверяемый bean
            CustomAttributeBean<CustomAttribute> checkedBean = new CustomAttributeBeanImpl<CustomAttribute>();
            //исходное количество атрибутов тестовых объектов
            int before = checkedBean.getAttributesByObject(user, TestObject.class).size();
            //получение пустого объекта
            CustomAttribute object = IdentifiableFactory.getImplementation(CustomAttribute.class);
            //наполнение данными
            object.setCoreDsc(CORE_DSC);
            object.setSystemObjectId(testObjId);
            object.setAttributeName(ATTRIBUTE_NAME);
            object.setDataType(DataType.stringType);
            object.setRequired(false);
            //создание нового описания
            BigDecimal createdId = checkedBean.createObject(user, object).getCoreId();
            //проверка что объект создан и есть в кеше
            Assert.assertEquals(before + 1, checkedBean.getAttributesByObject(user, TestObject.class).size());
            CustomAttribute returned = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals(ATTRIBUTE_NAME, returned.getAttributeName());

            //обновление
            CustomAttribute cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setLimitations("TEST");
            checkedBean.updateObject(user, cUpdated);
            object = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("TEST", object.getLimitations());

            //удаление
            checkedBean.deleteObject(user, object);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove CustomAttribute");
            }
            Assert.assertEquals(before, checkedBean.getAttributesByObject(user, TestObject.class).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
