/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.utils.BaseTest;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс для тестирования соответствующего EJB
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class DictionaryEntryBeanTest extends BaseTest {

    @Test
    public void testDictionaryEntryBeanLocal() {
        try {
            DictionaryEntryBean<DictionaryEntry> bean = new DictionaryEntryBeanImpl<DictionaryEntry>();
            Collection<DictionaryEntry> entries = bean.getAllEntriesForDictionary(user, BigDecimal.ONE);

            DictionaryEntry dicEntry = entries.iterator().next();
            try {
                bean.createObject(user, dicEntry);
            } catch (EJBException e) {
                Assert.assertTrue(e.getCausedByException() instanceof UnsupportedOperationException);
            }
            try {
                bean.updateObject(user, dicEntry);
            } catch (EJBException e) {
                Assert.assertTrue(e.getCausedByException() instanceof UnsupportedOperationException);
            }
            int before = entries.size();
            dicEntry.setCoreId(new BigDecimal(9999));
            bean.deleteObject(user, dicEntry);
            Assert.assertEquals(before, bean.getAllEntriesForDictionary(user, BigDecimal.ONE).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
