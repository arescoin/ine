/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangNameBeanTest.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
public class LangNameBeanTest extends BaseTest {

    @Test
    @SuppressWarnings({"unchecked"})
    public void testLangNameBeanLocal() {
        try {
            LangNameBean<LangName> checkedBean = new LangNameBeanImpl<LangName>();

            //создание нового
            LangName object = IdentifiableFactory.getImplementation(LangName.class);
            object.setCoreId(BigDecimal.valueOf(8888l));
            object.setCoreDsc("Description");
            object.setName("TestLangName");
            BigDecimal createdId = checkedBean.createObject(user, object).getCoreId();
            //получение и проверка данных
            object = checkedBean.getObjectById(user, createdId);
            Assert.assertEquals("TestLangName", object.getName());

            //обновление
            LangName cUpdated = checkedBean.getObjectById(user, createdId);
            cUpdated.setName("New Name");
            checkedBean.updateObject(user, cUpdated);
            object = checkedBean.getObjectById(user, createdId);
            //получение и проверка данных
            Assert.assertEquals("New Name", object.getName());

            //удаление
            checkedBean.deleteObject(user, object);
            if (checkedBean.getObjectById(user, createdId) != null) {
                Assert.fail("method deleteObject() has failed to remove LangName");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
