/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.dbe.da.core.structure.AttributesAccess;
import ru.effts.ine.dbe.da.core.structure.CustomAttributeValueAccess;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.structure.CustomAttributeValueBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueBeanImpl.java 1727 2011-03-29 15:10:22Z ikulkov $"
 */
@Stateless(name = "CustomAttributeValueBean", mappedName = "stateless.CustomAttributeValueBean")
@Remote(CustomAttributeValueBean.class)
public class CustomAttributeValueBeanImpl<T extends CustomAttributeValue> extends VersionableBeanImpl<T>
        implements CustomAttributeValueBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(CustomAttributeValue.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @Override
    public T getValue(UserProfile user, BigDecimal identifiableId, CustomAttribute attribute)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((CustomAttributeValueAccess<T>) getAccess()).getValue(identifiableId, attribute);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Map<CustomAttribute, CustomAttributeValue> getValues(
            UserProfile user, Identifiable obj, CustomType customType) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return AttributesAccess.getValues(obj, customType);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    protected void updateChecking(T versionable, T newVersionable) throws GenericSystemException {
        super.updateChecking(versionable, newVersionable);
        if (!versionable.getEntityId().equals(newVersionable.getEntityId())) {
            throw new GenericSystemException(
                    "Incorrect object modification. EntityId can not be modified.");
        }
    }

    @Override
    public T getObjectById(UserProfile user, BigDecimal... id) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().getObjectById(id);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void createValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            getAccess();

            AttributesAccess.createValues(obj, customType, values);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void updateValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            getAccess();

            AttributesAccess.updateValues(obj, customType, values);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteValues(UserProfile user, Identifiable obj, CustomType customType,
            Collection<CustomAttributeValue> values) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            getAccess();

            AttributesAccess.deleteValues(obj, customType, values);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
