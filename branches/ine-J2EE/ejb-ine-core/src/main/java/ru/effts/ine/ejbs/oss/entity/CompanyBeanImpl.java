/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;
import ru.effts.ine.oss.entity.Company;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link CompanyBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CompanyBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "CompanyBean", mappedName = "stateless.CompanyBean")
@Remote(CompanyBean.class)
public class CompanyBeanImpl<T extends Company> extends VersionableBeanImpl<T> implements CompanyBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    protected void init() throws GenericSystemException {
        init(Company.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
