/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.ejbs.core.history.HistoryEventBean;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.*;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean слушает командный {@link Topic топик} <code>(mappedName = "InECommandTopic")</code>
 * в ожидании сообщений с именем интерфейса, для которого необходимо загрузить данные.<br>
 * После загрузки данных отсылает в {@link Queue очередь} <code>(mappedName = "IneResponceQueue")</code>
 * ответ об успешном или неуспешном результате операции.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: GetterMDB.java 3567 2012-01-19 09:55:13Z ikulkov $"
 */
@MessageDriven(mappedName = "InECommandTopic")
public class GetterMDB implements MessageListener {

    private static final Logger logger = Logger.getLogger(GetterMDB.class.getName());

    @Resource(mappedName = "jms/InEQueueCF")
    private QueueConnectionFactory queueConnectionFactory;

    @Resource(mappedName = "IneResponceQueue")
    private Queue responceQueue;

    @Override
    public void onMessage(Message message) {

        String iFace = "";
        Boolean result = null;
        String errorMessage = "";
        try {
            if (message instanceof TextMessage) {
                iFace = ((TextMessage) message).getText();
                logger.log(Level.FINE, "Received command message for {0}", iFace);

                UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));

                Class clazz = Class.forName(iFace);
                this.loadObjects(clazz);

                if (DictionaryEntry.class.isAssignableFrom(clazz)) {
                    this.loadObjects(DictionaryTerm.class);
                }

                result = true;

                logger.log(Level.FINE, "All objects for {0} loaded", iFace);
            } else {
                logger.log(Level.WARNING, "Wrong message type [{0}], but expected [{1}]",
                        new Object[]{message.getJMSType(), TextMessage.class.getName()});
            }
        } catch (Throwable e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            result = false;
            errorMessage = e.getMessage();
        }

        if (result != null) {
            this.sendResponce(iFace, result, errorMessage);
        }
    }

    private void loadObjects(Class clazz) throws GenericSystemException {
        try {
            Collection objects = AccessFactory.getImplementation(clazz).getAllObjects();
            logger.log(Level.SEVERE, "Loaded {0} {1} objects", new Object[]{objects.size(), clazz});
        } catch (CoreException e) {
            logger.log(Level.WARNING, "Failed to load object for {0}", new Object[] {clazz});
            throw GenericSystemException.toGeneric(e);
        }
    }

    private void sendResponce(String text, boolean result, String errorMessage) {
        try {
            QueueConnection queueConnection = queueConnectionFactory.createQueueConnection();
            queueConnection.start();
            QueueSession queueSession = queueConnection.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);
            QueueSender queueSender = queueSession.createSender(responceQueue);

            TextMessage message = queueSession.createTextMessage(text);
            message.setBooleanProperty(SystemState.LOAD_SUCCESSFUL, result);
            message.setStringProperty(SystemState.ERROR_MESSAGE, errorMessage);

            queueSender.send(message);

            queueSender.close();
            queueSession.close();
            queueConnection.close();
        } catch (JMSException e) {
            logger.log(Level.SEVERE, "Failed to send responce on " + text + ", cause: ", e);
        }
    }
}
