/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.constants;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.constants.ConstantValueBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: ConstantValueBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "ConstantValueBean", mappedName = "stateless.ConstantValueBean")
@Remote(ConstantValueBean.class)
@SuppressWarnings({"unchecked"})
public class ConstantValueBeanImpl<T extends ConstantValue> extends VersionableBeanImpl<T>
        implements ConstantValueBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ConstantValue.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
