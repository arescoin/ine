/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.dbe.da.core.structure.PatternUtils;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link SystemObjectPatternBean}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectPatternBeanImpl.java 1517 2011-02-15 13:33:57Z ikulkov $"
 */
@Stateless(name = "SystemObjectPatternBean", mappedName = "stateless.SystemObjectPatternBean")
@Remote(SystemObjectPatternBean.class)
public class SystemObjectPatternBeanImpl implements SystemObjectPatternBean {

    @Override
    public SystemObjectPattern checkPattern(UserProfile user, SystemObjectPattern pattern) throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.checkPattern(pattern);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Identifiable getObjectByPattern(UserProfile user, SystemObjectPattern pattern, BigDecimal value)
            throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getObjectByPattern(pattern, value);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<Identifiable> getObjectsByPattern(
            UserProfile user, SystemObjectPattern pattern, BigDecimal value) throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getObjectsByPattern(pattern, value);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<Identifiable> getAvailableObjects(UserProfile user, SystemObjectPattern pattern)
            throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getAvailableObjects(pattern);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Map<SystemObjectPattern, BigDecimal> getPatternByObject(UserProfile user, Identifiable object)
            throws CoreException {
        try {
            UserHolder.setUserProfile(user);
            return PatternUtils.getPatternByObject(object);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
