/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.core.VersionableAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;

import javax.ejb.EJBException;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * Класс для j2ee доступа к Versionable ine-объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: VersionableBeanImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public abstract class VersionableBeanImpl<T extends Versionable> extends IdentifiableBeanImpl<T>
        implements VersionableBean<T> {

    @Override
    public Collection<T> getOldVersions(UserProfile user, T versionable) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().getOldVersions(versionable);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T createObject(UserProfile user, T versionable) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);

            versionable.setCoreTd(FieldAccess.getSystemTd());
            if (null == versionable.getCoreFd()) {
                versionable.setCoreFd(FieldAccess.getCurrentDate());
            }

            return getAccess().createObject(versionable);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @SuppressWarnings({"ThrowableInstanceNeverThrown"})
    @Override
    public T updateObject(UserProfile user, T newVersionable)
            throws IneNotActualVersionModificationException, GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            SyntheticId id = getAccess().getSyntheticId(newVersionable);

            //доступ к карте хранящей объекты синхронизации
            synchronized (getSyncObject(id)) {
                //проверка версии
                T returnedVersionable = getAccess().getObjectById(id.getIdValues());
                updateChecking(returnedVersionable, newVersionable);
                if (returnedVersionable.getCoreFd().after(newVersionable.getCoreFd())) {
                    throw new EJBException(new IneNotActualVersionModificationException(returnedVersionable,
                            "Object version is not up-to-date. Actual: " + returnedVersionable + "; Have: "
                                    + newVersionable + " : ["
                                    + returnedVersionable.getCoreFd().getTime() + " : "
                                    + newVersionable.getCoreFd().getTime() + " : "
                                    + "]"));
                }
                final Date d = FieldAccess.getCurrentDate();
//                if (returnedVersionable.getCoreFd().equals(d)) {
//                    d.setTime(d.getTime() + 1);//если объект обновляется то версия не может сохраниться
//                }
                newVersionable.setCoreFd(d);
                newVersionable.setCoreTd(FieldAccess.getSystemTd());

                return getAccess().updateObject(returnedVersionable, newVersionable);

            }
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    protected abstract Map getSyncMap();

    @SuppressWarnings({"SynchronizationOnLocalVariableOrMethodParameter"})
    private Object getSyncObject(SyntheticId id) {

        Map syncMap = getSyncMap();
        if (!syncMap.containsKey(id)) {
            synchronized (syncMap) {
                if (!syncMap.containsKey(id)) {
                    syncMap.put(id, id);
                }
            }
        }

        return syncMap.get(id);
    }

    @Override
    public Collection<T> getOldVersions(UserProfile user, T versionable, Date fd, Date td)
            throws GenericSystemException {
//        this.getOldVersions(user, versionable);
        return getAccess().getOldVersions(versionable, fd, td);
    }

    @Override
    protected VersionableAccess<T> getAccess() throws GenericSystemException {
        return (VersionableAccess<T>) super.getAccess();
    }
}
