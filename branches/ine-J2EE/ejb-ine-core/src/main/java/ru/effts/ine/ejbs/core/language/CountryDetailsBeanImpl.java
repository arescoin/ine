/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.language.CountryDetails;
import ru.effts.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link CountryDetailsBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "CountryDetailsBean", mappedName = "stateless.CountryDetailsBean")
@Remote(CountryDetailsBean.class)
public class CountryDetailsBeanImpl<T extends CountryDetails> extends IdentifiableBeanImpl<T>
        implements CountryDetailsBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(CountryDetails.class);
    }
}
