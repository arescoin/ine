/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.ipaddress;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.ipaddress.IPAddressValue;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс, представлющий собой stateless ejb. Реализует {@link IPAddressValueBean}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueBeanImpl.java 3219 2011-11-21 13:48:26Z ikulkov $"
 */
@Stateless(name = "IPAddressValueBean", mappedName = "stateless.IPAddressValueBean")
@Remote(IPAddressValueBean.class)
public class IPAddressValueBeanImpl<T extends IPAddressValue>
        extends VersionableBeanImpl<T> implements IPAddressValueBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(IPAddressValue.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
