/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.history.ActionAttribute;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.dbe.da.core.history.ActionAttributeAccess;
import ru.effts.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Collection;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.history.ActionAttributeBean}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeBeanImpl.java 1602 2011-03-11 15:30:21Z ikulkov $"
 */
@Stateless(name = "ActionAttributeBean", mappedName = "stateless.ActionAttributeBean")
@Remote(ActionAttributeBean.class)
public class ActionAttributeBeanImpl<T extends ActionAttribute> extends IdentifiableBeanImpl<T>
        implements ActionAttributeBean<T> {

    @Override
    protected void init() throws GenericSystemException {
        init(ActionAttribute.class);
    }

    @Override
    public Collection<T> getActionAttributes(UserProfile user, IdentifiableHistory history)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((ActionAttributeAccess<T>) getAccess()).getActionAttributes(history);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
