/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.*;
import ru.effts.ine.core.userpermits.UserRole;
import ru.effts.ine.dbe.da.core.userpermits.UserRoleAccess;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link UserRoleBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleBeanImpl.java 2027 2011-04-21 10:40:35Z sfilatov $"
 */
@Stateless(name = "UserRoleBean", mappedName = "stateless.UserRoleBean")
@Remote(UserRoleBean.class)
public class UserRoleBeanImpl<T extends UserRole> extends VersionableBeanImpl<T> implements UserRoleBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(UserRole.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @Override
    public Collection<T> getRolesByUserId(UserProfile user, BigDecimal userId) throws CoreException {

        try {
            UserHolder.setUserProfile(user);
            return new ArrayList<T>(((UserRoleAccess<T>) getAccess()).getRolesByUserId(userId));

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
