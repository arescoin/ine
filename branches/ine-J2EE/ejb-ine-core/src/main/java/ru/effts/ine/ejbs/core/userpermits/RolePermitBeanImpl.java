/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.RolePermit;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link RolePermitBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "RolePermitBean", mappedName = "stateless.RolePermitBean")
@Remote(RolePermitBean.class)
public class RolePermitBeanImpl<T extends RolePermit> extends VersionableBeanImpl<T> implements RolePermitBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(RolePermit.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
