/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.IdentifiableAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.utils.config.ConfigurationManager;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import javax.ejb.EJBException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * Класс для j2ee доступа к Identifiable ine-объектам
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableBeanImpl.java 3520 2011-12-21 15:34:42Z ikulkov $"
 */
@SuppressWarnings({"unchecked"})
public abstract class IdentifiableBeanImpl<T extends Identifiable>
        extends SystemBeanImpl implements IdentifiableBean<T> {

    protected IdentifiableAccess<T> access;

    private Boolean checkSystemState;

    protected abstract void init() throws GenericSystemException;

    protected void init(Class clazz) throws GenericSystemException {
        this.access = (IdentifiableAccess<T>) AccessFactory.getImplementation(clazz);
        IdentifiableFactory.getImplementation((Class<T>) clazz);
    }

    protected IdentifiableAccess<T> getAccess() throws GenericSystemException {
        if (access == null) {
            init();
        }
        if (queueConnectionFactory != null && this.checkSystemStartState()) {
            synchronized (systemState) {
                if (systemState.getState() != StartupState.STARTED) {
                    if (systemState.getState() == StartupState.IN_PROGRESS) {
                        throw new GenericSystemException("System startup is in progress");
                    } else {
                        throw new GenericSystemException("System startup failed.\n" + systemState.getErrorMessage());
                    }
                }
            }
        }
        return access;
    }

    private boolean checkSystemStartState() {
        if (checkSystemState == null) {
            String propValue = ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    SystemBean.class.getName() + "." + CHECK_SYSTEM_START_STATE_PROP_NAME, "true");
            checkSystemState = Boolean.parseBoolean(propValue);
        }
        return checkSystemState;
    }

    @Override
    public Collection<T> getAllObjects(UserProfile user) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return (Collection<T>) new ArrayList(getAccess().getAllObjects());
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T getObjectById(UserProfile user, BigDecimal... id) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().getObjectById(id);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public SyntheticId getSyntheticId(UserProfile user, T identifiable) throws GenericSystemException {

        try {
            UserHolder.setUserProfile(user);
            getAccess();
            return FieldAccess.getSyntheticId(identifiable);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T createObject(UserProfile user, T identifiable) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().createObject(identifiable);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @SuppressWarnings({"DuplicateThrows"})
    @Override
    public T updateObject(UserProfile user, T newIdentifiable)
            throws GenericSystemException, IneNotActualVersionModificationException {

        try {
            UserHolder.setUserProfile(user);

            T returnedIdentifiable = getAccess().getObjectById(newIdentifiable.getCoreId());
            updateChecking(returnedIdentifiable, newIdentifiable);

            return getAccess().updateObject(returnedIdentifiable, newIdentifiable);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            getAccess().deleteObject(identifiable);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Set<String> getInstanceFields(UserProfile user) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().getInstanceFields().keySet();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    /**
     * метод переопределяется в потомках для проверки иных данных и должен выкинуть исключение если
     * модификация объекта некоректна
     *
     * @param t    исходный объект
     * @param tNew модифицированный объект
     * @throws GenericSystemException если объект не может быть модифицирован
     */
    protected void updateChecking(T t, T tNew) throws GenericSystemException {
        if (!t.getCoreId().equals(tNew.getCoreId())) {
            throw new GenericSystemException("Incorrect object modification. CoreId can not be modified.");
        }
    }

    public String getMainRegionKey(UserProfile user) throws GenericSystemException {
        UserHolder.setUserProfile(user);
        return getAccess().getMainRegionKey();
    }

    @Override
    public SearchCriteriaProfile getSearchCriteriaProfile(UserProfile user) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().getSearchCriteriaProfile();
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public StorageFilter createStorageFilter(UserProfile user, Set<SearchCriterion> criteria, String fieldName)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().createStorageFilter(criteria, fieldName);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public StorageFilter createStorageFilter(UserProfile user,
            Set<SearchCriterion> criteria, SearchCriterion criterion, String fieldName) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().createStorageFilter(criteria, criterion, fieldName);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection<T> searchObjects(UserProfile user, Set<SearchCriterion> searchCriteria)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().searchObjects(searchCriteria);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection searchObjects(UserProfile user, Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().searchObjects(searchCriteria, fieldName);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public Collection searchObjects(UserProfile user, StorageFilter[] storageFilters) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().searchObjects(storageFilters);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public boolean containsObjects(UserProfile user, Set<SearchCriterion> searchCriteria)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().containsObjects(searchCriteria);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public boolean containsObjects(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().containsObjects(filters);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public int getObjectCount(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().getObjectCount(searchCriteria);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public int getObjectCount(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return getAccess().getObjectCount(filters);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public void startSystem() {
        throw new UnsupportedOperationException("This method is not for public use");
    }

    @Override
    public StartupState getState() {
        throw new UnsupportedOperationException("This method is not for public use");
    }
}
