/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.language.LangDetails;
import ru.effts.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link LangDetailsBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "LangDetailsBean", mappedName = "stateless.LangDetailsBean")
@Remote(LangDetailsBean.class)
public class LangDetailsBeanImpl<T extends LangDetails> extends IdentifiableBeanImpl<T>
        implements LangDetailsBean<T> {
    
    @Override
    public void init() throws GenericSystemException {
        init(LangDetails.class);
    }
}
