/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.structure.CustomAttributeDefVal;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "CustomAttributeDefValBean", mappedName = "stateless.CustomAttributeDefValBean")
@Remote(CustomAttributeDefValBean.class)
public class CustomAttributeDefValBeanImpl<T extends CustomAttributeDefVal> extends VersionableBeanImpl<T>
        implements CustomAttributeDefValBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(CustomAttributeDefVal.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
