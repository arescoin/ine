/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.oss.entity;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;
import ru.effts.ine.oss.entity.Person;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link PersonBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PersonBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "PersonBean", mappedName = "stateless.PersonBean")
@Remote(PersonBean.class)
public class PersonBeanImpl<T extends Person> extends VersionableBeanImpl<T> implements PersonBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Person.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
