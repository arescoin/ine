/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.links;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IneNotActualVersionModificationException;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.links.LinkAccess;
import ru.effts.ine.dbe.da.core.links.LinkDataAccess;
import ru.effts.ine.dbe.da.core.links.LinkUtils;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link LinkBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkBeanImpl.java 1767 2011-04-01 15:29:57Z ikulkov $"
 */
@Stateless(name = "LinkBean", mappedName = "stateless.LinkBean")
@Remote(LinkBean.class)
@SuppressWarnings({"ThrowableInstanceNeverThrown", "unchecked"})
public class LinkBeanImpl<T extends Link> extends VersionableBeanImpl<T> implements LinkBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(Link.class);
    }

    @Override
    protected Map getSyncMap() {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    public Collection<T> getLinksByObjType(UserProfile user, Class clazz) throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((LinkAccess<T>) getAccess()).getLinksByObjType(clazz);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @SuppressWarnings({"DuplicateThrows"})
    @Override
    public T updateObject(UserProfile user, T newVersionable)
            throws GenericSystemException, IneNotActualVersionModificationException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {

        //проверка что не существует фактических связей удаляемого типа
        UserHolder.setUserProfile(user);
        getAccess();

        try {
            LinkDataAccess<LinkData> linkDataAccess = (LinkDataAccess) AccessFactory.getImplementation(LinkData.class);
            LinkedHashSet<SearchCriterion> criteria = new LinkedHashSet<SearchCriterion>();
            criteria.add(linkDataAccess.getSearchCriteriaProfile().getSearchCriterion(
                    LinkData.LINK_ID, CriteriaRule.equals, identifiable.getCoreId()));
            if (linkDataAccess.containsObjects(criteria)) {
                throw new EJBException(new GenericSystemException(
                        "Impossible to delete Link. There are some LinkData of this Link."));
            }
            super.deleteObject(user, identifiable);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public BigDecimal getLinkId(UserProfile user, Class leftType, Class rightType, long type)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return LinkUtils.getLinkId(leftType, rightType, type);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
