/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.history;

import ru.effts.ine.core.*;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.dbe.da.core.history.IdentifiableHistoryAccess;
import ru.effts.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.history.IdentifiableHistoryBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@SuppressWarnings({"unchecked", "ThrowableInstanceNeverThrown"})
@Stateless(name = "IdentifiableHistoryBean", mappedName = "stateless.IdentifiableHistoryBean")
@Remote(IdentifiableHistoryBean.class)
public class IdentifiableHistoryBeanImpl<T extends IdentifiableHistory> extends IdentifiableBeanImpl<T>
        implements IdentifiableHistoryBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(IdentifiableHistory.class);
    }

    @Override
    public Collection<T> getActionsHistoryByIds(UserProfile user, BigDecimal sysObjId, SyntheticId entityId)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((IdentifiableHistoryAccess<T>) getAccess()).getActionsHistoryByIds(sysObjId, entityId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public T createObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));

    }

    @Override
    public T updateObject(UserProfile user, T newIdentifiable) throws GenericSystemException,
            IneNotActualVersionModificationException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new EJBException(new UnsupportedOperationException("Operation is not supported"));
    }
}
