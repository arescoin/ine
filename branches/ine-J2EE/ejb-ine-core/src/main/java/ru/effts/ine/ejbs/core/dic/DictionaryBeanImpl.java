/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.dic.DictionaryBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@SuppressWarnings({"unchecked"})
@Stateless(name = "DictionaryBean", mappedName = "stateless.DictionaryBean")
@Remote(DictionaryBean.class)
public class DictionaryBeanImpl<T extends Dictionary> extends VersionableBeanImpl<T>
        implements DictionaryBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Dictionary.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
