/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.UserPermit;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link UserPermitBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "UserPermitBean", mappedName = "stateless.UserPermitBean")
@Remote(UserPermitBean.class)
public class UserPermitBeanImpl<T extends UserPermit> extends VersionableBeanImpl<T> implements UserPermitBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(UserPermit.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
