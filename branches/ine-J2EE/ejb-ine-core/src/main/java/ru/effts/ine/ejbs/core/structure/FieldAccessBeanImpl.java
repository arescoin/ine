/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.ObjectAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.ejbs.core.history.HistoryEventBean;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: FieldAccessBeanImpl.java 1744 2011-03-30 15:23:38Z ikulkov $"
 */
@Stateless(name = "FieldAccessBean", mappedName = "stateless.FieldAccessBean")
@Remote(FieldAccessBean.class)
public class FieldAccessBeanImpl implements FieldAccessBean {

    @Override
    public String getInterface(BigDecimal tableId) throws CoreException {
        try {
            UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));
            return FieldAccess.getInterface(tableId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public BigDecimal getTableId(Class identifiableClass) throws CoreException {
        try {
            UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));
            return FieldAccess.getTableId(identifiableClass);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    public BigDecimal getFieldId(Class interfaceName, String fieldName) throws GenericSystemException {
        try {
            if (CustomAttributeValue.class.equals(interfaceName)) {
                throw new GenericSystemException("It's impossible to get id for CustomAttributeValue interface");
            }
            UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));

            ObjectAccess access = AccessFactory.getImplementation(interfaceName);
            return FieldAccess.getColumnId(access.getTableName(), access.getInstanceFields().get(fieldName));
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

}
