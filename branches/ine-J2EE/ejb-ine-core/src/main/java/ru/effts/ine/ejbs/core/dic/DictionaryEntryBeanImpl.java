/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.dic;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.dbe.da.core.dic.DictionaryEntryAccess;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.dic.DictionaryEntryBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "DictionaryEntryBean", mappedName = "stateless.DictionaryEntryBean")
@Remote(DictionaryEntryBean.class)
public class DictionaryEntryBeanImpl<T extends DictionaryEntry> extends VersionableBeanImpl<T>
        implements DictionaryEntryBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(DictionaryEntry.class);
    }

    @Override
    public Collection<T> getAllEntriesForDictionary(UserProfile user, BigDecimal dictionaryId)
            throws GenericSystemException {
        try {
            UserHolder.setUserProfile(user);
            return ((DictionaryEntryAccess<T>) getAccess()).getAllEntriesForDictionary(dictionaryId);
        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

}
