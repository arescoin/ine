/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.funcswitch;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.funcswitch.FuncSwitchName;
import ru.effts.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link FuncSwitchNameBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchNameBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "FuncSwitchNameBean", mappedName = "stateless.FuncSwitchNameBean")
@Remote(FuncSwitchNameBean.class)
public class FuncSwitchNameBeanImpl<T extends FuncSwitchName> extends IdentifiableBeanImpl<T>
        implements FuncSwitchNameBean<T> {

    @Override
    public void init() throws GenericSystemException {
        init(FuncSwitchName.class);
    }
}
