/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import javax.annotation.Resource;
import javax.ejb.MessageDriven;
import javax.jms.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Bean слушает {@link Queue очередь} <code>(mappedName = "IneResponceQueue")</code> в ожидании
 * результатов обработки интерфейсов посредством соответствующих {@link GetterMDB bean}'ов.<br>
 * При получении сообщения, регистрируют результат операции соответствующм
 * {@link SystemState#registerResponce(String, boolean, String) методом}.<br>
 * Если метод вернул <code>true</code>, то в {@link Queue очередь} <code>(mappedName = "IneStartupQueue")</code>
 * посылается {@link SystemState#START_CMD команда} на обработку следующей порции интерфейсов.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ReceiverMDB.java 2135 2011-05-17 09:24:14Z dgomon $"
 */
@MessageDriven(mappedName = "IneResponceQueue")
public class ReceiverMDB implements MessageListener {

    private static final Logger logger = Logger.getLogger(ReceiverMDB.class.getName());

    @Resource(mappedName = "jms/InEQueueCF")
    private QueueConnectionFactory queueConnectionFactory;

    @Resource(mappedName = "IneStartupQueue")
    private Queue startupQueue;

    @Override
    public void onMessage(Message message) {
        synchronized (SystemBeanImpl.systemState) {
            String iFace = "";
            try {
                if (message instanceof TextMessage) {
                    iFace = ((TextMessage) message).getText();
                    boolean result = message.getBooleanProperty(SystemState.LOAD_SUCCESSFUL);
                    String errorMessage = message.getStringProperty(SystemState.ERROR_MESSAGE);

                    boolean proceedNext = SystemBeanImpl.systemState.registerResponce(iFace, result, errorMessage);
                    if (proceedNext) {
                        SystemState.sendStartCommand(queueConnectionFactory, startupQueue);
                    }
                }
            } catch (Throwable e) {
                logger.log(Level.WARNING, e.getMessage(), e);
                SystemBeanImpl.systemState.registerResponce(iFace, false, e.getMessage());
            }
        }
    }
}
