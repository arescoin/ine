/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.ejbs.core.IdentifiableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link LangNameBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LangNameBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "LangNameBean", mappedName = "stateless.LangNameBean")
@Remote(LangNameBean.class)
public class LangNameBeanImpl<T extends LangName> extends IdentifiableBeanImpl<T>
        implements LangNameBean<T> {
    
    @Override
    public void init() throws GenericSystemException {
        init(LangName.class);
    }
}
