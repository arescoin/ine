/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.userpermits.Permit;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link PermitBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: PermitBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "PermitBean", mappedName = "stateless.PermitBean")
@Remote(PermitBean.class)
public class PermitBeanImpl<T extends Permit> extends VersionableBeanImpl<T> implements PermitBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Permit.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
