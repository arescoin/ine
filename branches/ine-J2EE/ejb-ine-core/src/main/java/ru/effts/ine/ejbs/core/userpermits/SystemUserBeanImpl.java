/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.userpermits;

import ru.effts.ine.core.*;
import ru.effts.ine.core.userpermits.NoSuchUserException;
import ru.effts.ine.core.userpermits.SystemUser;
import ru.effts.ine.dbe.da.core.userpermits.SystemUserAccess;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.EJBException;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.userpermits.SystemUserBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserBeanImpl.java 3099 2011-10-27 10:41:08Z dgomon $"
 */
@Stateless(name = "SystemUserBean", mappedName = "stateless.SystemUserBean")
@Remote(SystemUserBean.class)
public class SystemUserBeanImpl<T extends SystemUser> extends VersionableBeanImpl<T> implements SystemUserBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(SystemUser.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public T login(String login, String md5) throws GenericSystemException, NoSuchUserException {
        try {
            SystemUser systemUser = new SystemUser() {
                @Override
                public Object clone() throws CloneNotSupportedException {
                    return super.clone();
                }

                @Override
                public boolean isActive() {
                    return true;
                }

                @Override
                public void setActive(boolean active) {
                }

                @Override
                public void setCoreFd(Date fd) throws IneIllegalArgumentException {
                }

                @Override
                public Date getCoreFd() {
                    return new Date();
                }

                @Override
                public void setCoreTd(Date td) throws IneIllegalArgumentException {
                }

                @Override
                public Date getCoreTd() {
                    return new Date();
                }

                @Override
                public void setCoreId(BigDecimal id) throws IllegalIdException {
                }

                @Override
                public BigDecimal getCoreId() {
                    return BigDecimal.ONE;
                }

                @Override
                public String getCoreDsc() {
                    return "TEST User";
                }

                @Override
                public void setCoreDsc(String dsc) {
                }

                @Override
                public String getLogin() {
                    return "root";
                }

                @Override
                public void setLogin(String login) {
                }

                @Override
                public String getMd5() {
                    return null;
                }

                @Override
                public void setMd5(String md5) {
                }
            };

            UserHolder.setUser(systemUser);
            if (access == null) {
                init();
            }

            return (T) ((SystemUserAccess<SystemUser>) access).login(login, md5);

        } catch (Exception e) {
            throw new EJBException(e.getMessage(), e);
        }
    }
}
