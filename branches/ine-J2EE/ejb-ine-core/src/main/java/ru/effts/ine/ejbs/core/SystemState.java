/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.ejbs.core.history.HistoryEventBean;

import javax.jms.*;
import java.util.*;

/**
 * Класс для работы с запуском системы.<br>
 * Содержит в себе состояние запуска и текст ошибок, в случае неудачного запуска.<br>
 * А также:
 * <ul>
 * <li> - подготавливает порции интерфейсов, которые необходимо загрузить;</li>
 * <li> - регистрирует ответ от бина об успешной/неуспешной загрузке интерфейса;</li>
 * <li> - выставляет состояние запуска систему на основе ответов о загруженных интерфейсах.</li>
 * </ul>
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemState.java 3798 2014-05-26 14:23:59Z DGomon $"
 */
public class SystemState {

    /** Команда на запуск для {@link StartupMDB} */
    static final String START_CMD = "System.Start";

    /** Команда на запуск для {@link StartupMDB} с LifecycleListener */
    static final String START_CMD_LCL = "System.Start.LCL";

    /** Имя булевого свойства {@link javax.jms.Message сообщения}, передающее результат загрузки данных интерфейса. */
    static final String LOAD_SUCCESSFUL = "LOAD_SUCCESSFUL";
    /** Текстовое свойство {@link javax.jms.Message сообщения}, передающее текст ошибки, при обломе с загрузкой */
    static final String ERROR_MESSAGE = "ERROR_MESSAGE";

    /** Признак того, что прошла команда на запуск системы */
    private boolean startCmdSent = false;

    /** {@link StartupState Состояние} запуска системы */
    private StartupState state = StartupState.IN_PROGRESS;
    /** Сообщение об ошибке при получении списка интерфейсов */
    private String errorMessage = "";

    /**
     * Номер порции интерфейсов, которые необходимо обработать.
     *
     * @see #interfaces
     */
    private int interfacesToProceed = 0;

    private Collection<String> systemObjectInterface = new ArrayList<>(2);
    private Collection<String> customAttributesInterface = new ArrayList<>(2);
    private Collection<String> otherSystemInterfaces = new ArrayList<>(30);
    private Collection<String> ossjValueInterfaces = new ArrayList<>(35);
    private Collection<String> associationRuleInterface = new ArrayList<>(2);
    private Collection<String> associationInterfaces = new ArrayList<>(10);

    /**
     * Порции интерфейсов, которые необходимо обработать.
     * Можно было бы и не выделять содержимое массива в отдельные переменные,
     * но так получается нагляднее и понятнее.
     */
    @SuppressWarnings({"unchecked"})
    private Collection<String>[] interfaces = new Collection[]{
            systemObjectInterface,
            customAttributesInterface,
            otherSystemInterfaces,
            associationRuleInterface,
            associationInterfaces,
            ossjValueInterfaces,
            new ArrayList<String>()
    };

    /** Общий список всех загруженных интерфейсов */
    private Map<String, Boolean> loadedInterfaces = new HashMap<>(75);
    /** Список интерфейсов, в процессе загрузки которых возникли ошибки, с сообщениеми об ошибках */
    private Map<String, String> failures = new HashMap<>();


    static void sendStartCommand(QueueConnectionFactory factory, javax.jms.Queue queue) throws JMSException {
        // Посылаем стартовую команду на запуск обработки порции интерфейсов.
        QueueConnection queueConnection = factory.createQueueConnection();
        queueConnection.start();
        QueueSession queueSession = queueConnection.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);

        QueueSender queueSender = queueSession.createSender(queue);
        queueSender.send(queueSession.createTextMessage(START_CMD));

        queueSender.close();
        queueSession.close();
        queueConnection.close();
    }

    /** Загружаем список интерфейсов всех объектов системы и формируем порции для параллельной загрузки. */
    void initInterfaces() {

        UserHolder.setUserProfile(new UserProfile(HistoryEventBean.SYSTEM_USER_SYS));

        try {
            for (String iFace : FieldAccess.getSupportedIntefaces()) {
                if (!iFace.contains("javax.oss.")) {
                    if (iFace.contains("SystemObject")) {
                        systemObjectInterface.add(iFace);
                    } else if (iFace.endsWith("CustomAttribute")) {
                        customAttributesInterface.add(iFace);
                    } else if (iFace.startsWith("ru.effts.ine.core.history")) {
                        continue;
                    } else {
                        otherSystemInterfaces.add(iFace);
                    }
                } else {
                    if (iFace.contains("AssociationRule")) {
                        associationRuleInterface.add(iFace);
                    } else if (iFace.contains("AssociationValue") || iFace.contains("RelationshipValue")) {
                        associationInterfaces.add(iFace);
                    } else {
                        ossjValueInterfaces.add(iFace);
                    }
                }
                loadedInterfaces.put(iFace, false);
            }
        } catch (GenericSystemException e) {
            state = StartupState.START_FAILED;
            errorMessage = e.getMessage();
        }
    }

    /**
     * Возвращает порцию интерфейсов, которые необходимо обработать
     *
     * @return порция интерфейсов
     */
    Collection<String> getInterfacesToProceed() {
        return Collections.unmodifiableCollection(interfaces[interfacesToProceed]);
    }

    /**
     * Регистрирует результат обработки интерфейса
     *
     * @param iFace        обработанный интерфейс
     * @param result       результат обработки. <code>true</code> данные успешно загружены, <code>false</code> иначе
     * @param errorMessage сообщение об ошибке, в случае неуспешной загрузки данных
     * @return признак запуска обработки следующей порции данных.<br>
     *         если текущая порция (interfaces[interfacesToProceed]) обработана,
     *         возвращается <code>true</code>, <code>false</code> иначе.
     */
    boolean registerResponce(String iFace, boolean result, String errorMessage) {

        if (iFace.isEmpty()) {
            failures.put(iFace + System.currentTimeMillis(), errorMessage);
        } else {
            loadedInterfaces.put(iFace, result);

            if (!result) {
                failures.put(iFace, errorMessage);
            }

            interfaces[interfacesToProceed].remove(iFace);
        }

        boolean proceedNextInterfaces = interfaces[interfacesToProceed].isEmpty();
        if (proceedNextInterfaces) {
            interfacesToProceed++;
        }

        return proceedNextInterfaces;
    }

    boolean isStartCmdSent() {
        return startCmdSent;
    }

    void setStartCmdSent(boolean startCmdSent) {
        this.startCmdSent = startCmdSent;
    }

    StartupState getState() {
        return state;
    }

    /**
     * Возвращает детальное сообщение об ошибках при загрузке данных интерфейсов объектов системы.<br>
     * Сообщение формируется следующим образом:<br>
     * если {@link #errorMessage} не пустое, то возвращается это сообщение об ошибке;<br>
     * иначе возвращается сообщение, сформированное из сообщений, содержащихся в этом {@link #failures списке}.<br>
     *
     * @return детальное сообщение об ошибках
     */
    String getErrorMessage() {

        if (!errorMessage.isEmpty()) {
            return errorMessage;
        }

        String notLoaded = "Next interfaces were not loaded:\n";
        for (String iFace : loadedInterfaces.keySet()) {
            if (!loadedInterfaces.get(iFace)) {
                notLoaded += iFace + "\n";
            }
        }

        String errorMessage = notLoaded + "caused by errors:\n";
        for (String key : failures.keySet()) {
            errorMessage += key + ": " + failures.get(key) + "\n";
        }

        return errorMessage;
    }

    /**
     * Выставляет состояние запуска системы.<br>
     * Если все интерфейсы в {@link #loadedInterfaces списке} успешно загружены,
     * то выставляется состояние {@link StartupState#STARTED}, иначе {@link StartupState#START_FAILED}.<br>
     * Детальное сообщение об ошибках при загрузке доступно через метод {@link #getErrorMessage()}.
     */
    void setState() {
        boolean result = true;

        for (String iFace : loadedInterfaces.keySet()) {
            if (!loadedInterfaces.get(iFace)) {
                result = false;
                break;
            }
        }

        state = result ? StartupState.STARTED : StartupState.START_FAILED;
    }

}
