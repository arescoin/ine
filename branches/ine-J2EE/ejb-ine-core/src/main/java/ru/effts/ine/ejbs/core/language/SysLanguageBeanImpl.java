/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.ejbs.core.language;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Имплементирует {@link ru.effts.ine.ejbs.core.language.SysLanguageBean}
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageBeanImpl.java 1465 2011-02-09 11:51:48Z sfilatov $"
 */
@Stateless(name = "SysLanguageBean", mappedName = "stateless.SysLanguageBean")
@Remote(SysLanguageBean.class)
public class SysLanguageBeanImpl<T extends SysLanguage> extends VersionableBeanImpl<T> implements SysLanguageBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<SyntheticId, Object>(100);

    @Override
    public void init() throws GenericSystemException {
        init(SysLanguage.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
