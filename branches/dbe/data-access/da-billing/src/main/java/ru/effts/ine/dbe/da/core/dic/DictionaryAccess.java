/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.dic;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.dbe.da.core.IdentifiableAccess;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к описаниям словарей
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryAccess.java 3407 2011-12-05 08:06:41Z dgomon $"
 */
public interface DictionaryAccess<T extends Dictionary> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент описания словарей
     *
     * @return коллекция описаний словарей
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
