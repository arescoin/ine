/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к значениям дополнительных атрибутов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueAccess.java 3418 2011-12-05 10:32:58Z dgomon $"
 */
public interface CustomAttributeValueAccess<T extends CustomAttributeValue> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент значения дополнительных атрибутов.
     * <p/>
     * <strong>НАСТОЯТЕЛЬНО РЕКОМЕНДУЕТСЯ ПОДУМАТЬ, ПРЕЖДЕ ЧЕМ ВЫЗЫВАТЬ ДАННЫЙ МЕТОД!!!</strong>
     * <p/>
     * Первый вызов метода до наполнения кеша будет выполняться <strong>ОЧЕНЬ ДОЛГО</strong>, т.к. потребуется выбрать
     * практически всю базу данных в память.
     *
     * @return коллекция значений произвольных атрибутов
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в данных
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Возвращает актуальный объект по набору идентификаторов
     * <p/>
     * Для получения значения custom-атрибута необходимо передать 2 индентификатора:
     * <ul>
     * <li>1-й id - идентификатор экземпляра объекта, для которого получается значение атрибута
     * <li>2-й id - идентификатор описателя custom-атрибута, по которому получается значение
     * </ul>
     * <b>Вместо прямого вызова этого метода настоятельно рекомендуется использовать
     * {@link #getValue(java.math.BigDecimal, ru.effts.ine.core.structure.CustomAttribute)}!</b>
     *
     * @param id перечисление идентификаторов
     * @return актуальный {@link T объект}
     * @throws ru.effts.ine.core.CoreException при возникновении ошибке в процессе получения и обработки данных
     * @see #getValue(java.math.BigDecimal, ru.effts.ine.core.structure.CustomAttribute)
     */
    @Override
    T getObjectById(BigDecimal... id) throws CoreException;

    /**
     * Получает значение конкретного атрибута
     *
     * @param identifiableId идентификатор экземпляра объекта, для которого получается значение атрибута
     * @param attribute      описатель дополнительного атрибута
     * @return значение атрибута
     * @throws CoreException при некорректных значениях в данных
     */
    T getValue(BigDecimal identifiableId, CustomAttribute attribute) throws CoreException;
}
