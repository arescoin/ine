/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.userpermits.Role;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к системным ролям
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RoleAccess.java 3797 2014-05-26 14:22:48Z DGomon $"
 */
public interface RoleAccess<T extends Role> extends VersionableAccess<T> {

    /**
     * Метод возвращает список всех системных ролей
     *
     * @return коллекция системных пользователей
     * @throws ru.effts.ine.core.CorruptedIdException
     *          при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException
     *          при некорректных значениях в датах системного объекта
     */
    @Override Collection<T> getAllObjects() throws CoreException;

    void addHelper(CRUDHelper<T> helper);
}
