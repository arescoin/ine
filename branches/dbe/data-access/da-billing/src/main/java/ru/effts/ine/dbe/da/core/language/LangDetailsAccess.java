/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.language;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.language.LangDetails;
import ru.effts.ine.dbe.da.core.IdentifiableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к описанию языков в стандарте iso639
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsAccess.java 3410 2011-12-05 09:29:03Z dgomon $"
 */
public interface LangDetailsAccess<T extends LangDetails> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент описания языков
     *
     * @return коллекция описаний словарей
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
