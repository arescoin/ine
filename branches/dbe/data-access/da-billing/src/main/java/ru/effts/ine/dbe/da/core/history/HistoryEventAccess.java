/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.history;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.history.HistoryEvent;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к данным по событиям в системных объектах.
 * <p/>
 * Расширене простого {@link VersionableHistoryAccess}, с добавлением признака обработки события.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEventAccess.java 3409 2011-12-05 09:23:43Z dgomon $"
 */
public interface HistoryEventAccess<T extends HistoryEvent> extends VersionableHistoryAccess<T> {

    /**
     * Возвращает колекцию событий требующих обработки
     *
     * @return колекция событий
     * @throws ru.effts.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    Collection<T> getEventsForProcess() throws CoreException;

    /**
     * Возвращает колекцию событий подверженных обработке
     *
     * @return колекция событий
     * @throws ru.effts.ine.core.CoreException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    Collection<T> getProcessedEvents() throws CoreException;

    /**
     * Отмечает событие как обработанное
     *
     * @param historyEvent событие
     * @throws ru.effts.ine.core.GenericSystemException при обнаружении инвалидных данных в БД,
     * или при возникновении системных ошибок
     */
    void markAsProcessed(T historyEvent) throws GenericSystemException;

}
