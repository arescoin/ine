/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core;

import ru.effts.ine.core.CoreException;

import java.util.Collection;
import java.util.Map;

/**
 * Описывает интерфейс доступа к системным данным в БД
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ObjectAccess.java 3407 2011-12-05 08:06:41Z dgomon $"
 */
public interface ObjectAccess {

    /**
     * Возвращает все актуальные объекты
     *
     * @return коллекция с {@link ru.effts.ine.core.Identifiable}, может быть пустой
     * @throws ru.effts.ine.core.CoreException при возникновении ошибке в процессе получения и обработки данных
     */
    Collection<?> getAllObjects() throws CoreException;

    /**
     * Возвращает название таблицы
     *
     * @return название таблицы
     */
    String getTableName();

    /**
     * Возвращает мапинг полей интерфейсов на имена столбцов
     *
     * @return ключ - имя поля класса, значение - имя столбца таблицы
     */
    Map<String, String> getInstanceFields();

    /**
     * Возвращает идентификатор основного региона размещения в кеше
     *
     * @return идентификатор региона
     */
    String getMainRegionKey();
}
