/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.dic;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.dbe.da.core.IdentifiableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к словарным статьям.
 * <br>
 * Доступны только операции чтения, модификация данных запрещена. Для этого необходимо использовать
 * {@link ru.effts.ine.dbe.da.core.dic.DictionaryTermAccess}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntryAccess.java 3407 2011-12-05 08:06:41Z dgomon $"
 */
public interface DictionaryEntryAccess<T extends DictionaryEntry> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент словарные статьи
     *
     * @return коллекция всех словарных статей
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает все действительные указанные словарные статьи для указанного словаря
     *
     * @param dictionaryId идентификатор словаря
     * @return коллекция словарных статей {@link ru.effts.ine.core.dic.DictionaryEntry}
     * @throws ru.effts.ine.core.GenericSystemException при ошибках получения данных
     */
    public Collection<T> getAllEntriesForDictionary(BigDecimal dictionaryId) throws GenericSystemException;

}
