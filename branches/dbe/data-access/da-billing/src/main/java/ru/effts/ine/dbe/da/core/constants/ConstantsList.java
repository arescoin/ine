/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.constants;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Утилитный интерфейс для объявления идентификаторов констант, используемых системой.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ConstantsList.java 3406 2011-12-05 08:03:16Z dgomon $"
 */
public interface ConstantsList extends Serializable {

    /** Язык системы по-умолчанию */
    BigDecimal DEF_SYS_LANG = new BigDecimal(3);
}
