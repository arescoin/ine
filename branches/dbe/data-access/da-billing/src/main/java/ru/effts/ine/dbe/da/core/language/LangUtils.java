/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.language;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.constants.ConstantValueAccess;
import ru.effts.ine.dbe.da.core.constants.ConstantsList;

import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Утилитный класс для работы с языками системы.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangUtils.java 3410 2011-12-05 09:29:03Z dgomon $"
 */
public class LangUtils {

    /** Название параметра, выставляемого в профиле пользователя для передачи языка, с которым работает система */
    public static final String SYSTEM_LANGUAGE_PARAM = "SYSTEM_LANGUAGE";

    private static Logger logger = Logger.getLogger(LangUtils.class.getName());

    /** Код системного яязыка */
    private static BigDecimal DEFAULT_LANGUAGE_ID;

    /**
     * Получаем из константы {@link ru.effts.ine.dbe.da.core.constants.ConstantsList#DEF_SYS_LANG} значение языка
     * системы по-умолчанию.
     *
     * @return идентификатор системного языка по-умолчанию
     */
    @SuppressWarnings({"unchecked"})
    public static BigDecimal getDefaultLanguageId() {
        if (DEFAULT_LANGUAGE_ID == null) {
            BigDecimal defaultLanguage = Identifiable.MIN_ALLOWABLE_VAL;
            try {
                // Проверяем, что у константы вообще есть значение
                ConstantValue constantValue = ((ConstantValueAccess<ConstantValue>) AccessFactory.getImplementation(
                        ConstantValue.class)).getObjectById(ConstantsList.DEF_SYS_LANG);
                if (constantValue == null) {
                    logger.log(Level.WARNING,
                            "Value of constant DEF_SYS_LANG[id=" + ConstantsList.DEF_SYS_LANG + "] not found");
                    return defaultLanguage;
                }
                // Проверяем, что значение - число
                try {
                    defaultLanguage = new BigDecimal(constantValue.getValue());
                } catch (NumberFormatException nfe) {
                    logger.log(Level.WARNING, "Value of constant DEF_SYS_LANG[id=" + ConstantsList.DEF_SYS_LANG +
                            "] is not a number: " + constantValue.getValue());
                    return defaultLanguage;
                }
                // Проверяем, что язык с полученным идентификатором определён в системе
                if (((SysLanguageAccess<SysLanguage>) AccessFactory.getImplementation(SysLanguage.class)).
                        getObjectById(defaultLanguage) == null) {
                    logger.log(Level.WARNING, "Value of constant DEF_SYS_LANG[id=" + ConstantsList.DEF_SYS_LANG +
                            "] is incorrect: SysLanguage[id=" + defaultLanguage + "] not found");
                    defaultLanguage = Identifiable.MIN_ALLOWABLE_VAL;
                }
            } catch (CoreException ce) {
                logger.log(Level.WARNING, "Failed to obtain DefaultSystemLanguage", ce);
                defaultLanguage = Identifiable.MIN_ALLOWABLE_VAL;
            }
            DEFAULT_LANGUAGE_ID = defaultLanguage;
        }

        return DEFAULT_LANGUAGE_ID;
    }

    /**
     * Получает из профиля пользователя идентификатор языка, на котором работает система.
     * Если значение на выставлено или некорректное, то возвращается язык системы по-умолчанию.
     *
     * @return идентификатор системного языка
     * @see #getDefaultLanguageId()
     */
    public static BigDecimal getSystemLanguageId() {
        BigDecimal langId = null;
        try {
            //TODO выставлять системный язык с презентационного уровня!!! : а вот нахуя???
            langId = (BigDecimal) UserHolder.getUser().getAttribute(LangUtils.SYSTEM_LANGUAGE_PARAM);
        } catch (Exception e) {
            logger.log(Level.INFO, "Incorrect value set for parameter " + SYSTEM_LANGUAGE_PARAM + ": " +
                    UserHolder.getUser().getAttribute(LangUtils.SYSTEM_LANGUAGE_PARAM));
        }
        if (langId == null) {
            langId = LangUtils.getDefaultLanguageId();
        }
        return langId;
    }

}
