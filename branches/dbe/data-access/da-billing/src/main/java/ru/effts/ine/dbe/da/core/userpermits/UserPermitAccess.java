/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.userpermits.UserPermit;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Описывает интерфейс доступа к доступам, назначенных системному пользователю
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitAccess.java 3797 2014-05-26 14:22:48Z DGomon $"
 */
public interface UserPermitAccess<T extends UserPermit> extends VersionableAccess<T> {

    /**
     * Метод возвращает список доступов, назначенных системному пользователю
     *
     * @return коллекция системных пользователей
     * @throws ru.effts.ine.core.CorruptedIdException
     *          при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException
     *          при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    void addHelper(CRUDHelper<T> helper);

    Map<BigDecimal, T> getPermitsByUserAndRoleId(BigDecimal userId, BigDecimal roleId);
}
