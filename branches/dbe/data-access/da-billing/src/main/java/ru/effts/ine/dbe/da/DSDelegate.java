/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DSDelegate.java 3789 2014-03-28 14:38:17Z DGomon $"
 */
public interface DSDelegate {

    /**
     * Инициализация пула соединений с БД. Для работы с пулом необходимо подлкючить драйвер БД, который будет
     * использоваться для создания соединения.
     * <p/>
     * //     * @param driver        экземпляр класса драйвера соединения с БД
     *
     * @param host     адрес сервера БД
     * @param port     порт сервера БД
     * @param db       имя базы
     * @param user     пользователь БД
     * @param password пароль пользователя БД
     * @throws java.sql.SQLException в случае ошибки при регистрации драйвера соединения с БД
     */
    void init(String host, String port, String db, String user, String password) throws SQLException;

    /**
     * Получает соединение из пула.
     *
     * @return соединение из пула
     * @throws SQLException ошибка получения соединения с БД
     */
    Connection getConnection() throws SQLException;
}
