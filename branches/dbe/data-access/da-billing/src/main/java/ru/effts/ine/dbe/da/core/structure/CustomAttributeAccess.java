/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к дополнительным атрибутам
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeAccess.java 3418 2011-12-05 10:32:58Z dgomon $"
 */
public interface CustomAttributeAccess<T extends CustomAttribute> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент описания дополнительных атрибутов
     *
     * @return коллекция описаний атрибутов
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в данных
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Получает список атрибутов конкретного объекта.
     *
     * @param identifiableClass класс интерфейса объекта, для которого получаются атрибуты.
     * @return коллекция описаний атрибутов
     * @throws ru.effts.ine.core.GenericSystemException при некорректных значениях в данных
     */
    Collection<T> getAttributesByObject(Class<? extends Identifiable> identifiableClass) throws GenericSystemException;
}
