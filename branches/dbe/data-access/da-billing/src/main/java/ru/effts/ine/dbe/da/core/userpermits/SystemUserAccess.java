/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.userpermits.NoSuchUserException;
import ru.effts.ine.core.userpermits.SystemUser;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к системным пользователям
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SystemUserAccess.java 3419 2011-12-05 10:35:12Z dgomon $"
 */
public interface SystemUserAccess<T extends SystemUser> extends VersionableAccess<T> {

    /**
     * Метод возвращает всех системных пользователей
     *
     * @return коллекция системных пользователей
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Возвращает пользователя системы по его системному имени и хешу пароля
     *
     * @param login системное имя пользователя
     * @param md5   хеш пароля пользователя
     * @return пользователь
     * @throws NoSuchUserException если пользователь с указанными данными не найден
     * @throws GenericSystemException при ошщибках доступа к данным
     */
    T login(String login, String md5) throws GenericSystemException;
}
