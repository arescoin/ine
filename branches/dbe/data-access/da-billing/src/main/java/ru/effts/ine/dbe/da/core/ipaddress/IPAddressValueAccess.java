/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.ipaddress;

import ru.effts.ine.core.ipaddress.IPAddressValue;
import ru.effts.ine.dbe.da.core.VersionableAccess;

/**
 * Базовый интерфейс для доступа к данным {@link IPAddressValue}-объектов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValueAccess.java 3219 2011-11-21 13:48:26Z ikulkov $"
 */
public interface IPAddressValueAccess<T extends IPAddressValue> extends VersionableAccess<T> {
}
