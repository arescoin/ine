/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.structure.CustomAttributeDefVal;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Интерфейс для доступа к значениям по-умолчанию для произвольных атрибутов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValAccess.java 3418 2011-12-05 10:32:58Z dgomon $"
 */
public interface CustomAttributeDefValAccess<T extends CustomAttributeDefVal> extends VersionableAccess<T> {

    /**
     * Метод возвращает все актуальные на текущий момент значения по-умолчанию для произвольных атрибутов
     *
     * @return коллекция значений по-умолчанию для произвольных атрибутов
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в данных
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
