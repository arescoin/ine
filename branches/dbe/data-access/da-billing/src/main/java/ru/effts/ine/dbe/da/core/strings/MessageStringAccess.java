/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.strings;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.strings.MessageString;
import ru.effts.ine.dbe.da.core.ObjectAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

/**
 * Описывает интерфейс доступа к строкам локализации
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringAccess.java 3412 2011-12-05 09:32:42Z dgomon $"
 */
public interface MessageStringAccess<T extends MessageString> extends ObjectAccess {

    /**
     * Метод возвращает все строки локализации
     *
     * @return коллекция строк локализации
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Возвращает содержимое локализованного сообщения по указанным ключу и коду языка
     *
     * @param stringKey ключ сообщения
     * @param langCode  код языка
     * @return локализованное сообщение
     * @throws ru.effts.ine.core.CoreException при возникновении ошибок в процессе получения данных
     */
    T getStringByKeyAndLang(String stringKey, BigDecimal langCode) throws CoreException;

    /**
     * Возвращает карту локализованных сообщений по указанным ключу в присутствующих языках
     *
     * @param stringKey ключ сообщения
     * @return карта локализованных сообщений
     * @throws ru.effts.ine.core.CoreException при возникновении ошибок в процессе получения данных
     */
    Map<BigDecimal, T> getStringsByKey(String stringKey) throws CoreException;

    /**
     * Создает в системе новый объект
     *
     * @param identifiable новый объект
     * @return созданный объект
     * @throws ru.effts.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.effts.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Изменяет свойства существующего в системе объекта
     *
     * @param identifiable    старый объект
     * @param newIdentifiable новый объект
     * @return модифицированный объект
     * @throws ru.effts.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.effts.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException;

    /**
     * Удаляет существующий объект
     *
     * @param identifiable удаляемый объект
     * @throws ru.effts.ine.core.IneIllegalArgumentException при попытке передать null или некорректный объект в
     * контексте операции
     * @throws ru.effts.ine.core.GenericSystemException при ошибке в процессе получения и обработки данных
     */
    void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException;
}
