/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Интерфейс доступа к описаниям модифицированных типов
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeAccess.java 3418 2011-12-05 10:32:58Z dgomon $"
 */
public interface CustomTypeAccess<T extends CustomType> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент кастомные типы
     *
     * @return коллекция модифицированные типов
     * @throws CoreException при некорректных значениях в данных
     */
    @Override
    Collection<? extends T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает базовые интерфейсы которые могут быть расширены
     *
     * @return коллекция интерфейсов которые могут быть установлены в качестве базовых
     * @throws CoreException при некорректных значениях в данных
     */
    Collection<String> getBaseObjects() throws CoreException;

    /**
     * Возвращает актуальный набор кастомных типов для указанного базового интерфейса
     *
     * @param baseObject базовый интерфейс
     * @return коллекция модифицированные типов
     * @throws ru.effts.ine.core.GenericSystemException при ошибке получения данных
     */
    Collection<T> getTypesForBaseObject(BigDecimal baseObject) throws GenericSystemException;

    /**
     * Возвращает коллекцию всех атрибутов кастомного типа, включая унаследованные
     *
     * @param customType модицированный тип
     * @return полный набор атрибутов
     * @throws GenericSystemException при ошибках доступа к данным или некорректных данных
     */
    Collection<CustomAttribute> getAllAttributes(T customType) throws GenericSystemException;

    /**
     * Возвращает коллекцию всех унаследованных атрибутов кастомного типа
     *
     * @param customType модицированный тип
     * @return набор унаследованных атрибутов
     * @throws GenericSystemException при ошибках доступа к данным или некорректных данных
     */
    Collection<CustomAttribute> getParentAttributes(T customType) throws GenericSystemException;

    /**
     * Проверяет, есть ли объекты указанного кастомного типа.
     *
     * @param typeId идентификатор модифицированного типа
     * @return true если есть объекты этого типа
     * @throws ru.effts.ine.core.GenericSystemException при ошибке получения данных
     */
    boolean isCustomTypeUsed(BigDecimal typeId) throws GenericSystemException;

}
