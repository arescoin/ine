/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.history;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.history.ActionAttribute;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.dbe.da.core.IdentifiableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к пользовательскими атрибутам истории изменений идентифицируемых объектов
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeAccess.java 1602 2011-03-11 15:30:21Z ikulkov $"
 */
public interface ActionAttributeAccess<T extends ActionAttribute> extends IdentifiableAccess<T> {

    /**
     * Метод возвращает все пользовательские атрибуты для зарегистрированных действий в системе
     *
     * @return коллекция пользовательских атрибутов
     * @throws ru.effts.ine.core.CoreException в случае ошибок при работе с БД, кешем и метаинформацией
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает пользовательские атрибуты для переданной записи об изменениях в идентифицируемых объектах
     *
     * @param history запись об изменениях в идентифицируемом объекте
     * @return коллекция пользовательских атрибутов
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некоррекного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException в случае ошибок при работе с БД, кешем и метаинформацией
     */
    Collection<T> getActionAttributes(IdentifiableHistory history) throws CoreException;
}
