/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.funcswitch;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.funcswitch.FuncSwitch;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к значениям переключателей функциональности
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchAccess.java 3408 2011-12-05 09:21:49Z dgomon $"
 */
public interface FuncSwitchAccess<T extends FuncSwitch> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент значения переключателей функциональности
     *
     * @return коллекция значений переключателей функциональности
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
