/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.history;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.history.VersionableHistory;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к данным по истории изменений версионного объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistoryAccess.java 3409 2011-12-05 09:23:43Z dgomon $"
 */
public interface VersionableHistoryAccess<T extends VersionableHistory> extends IdentifiableHistoryAccess<T> {

    /**
     * Метод возвращает все записи об изменениях в идентифицируемых объектах
     *
     * @param sysObjId идентификатор системного объекта
     * @param entityId идентификатор сущности
     * @return коллекция системных объектов
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некоррекного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректых значениях в датах системного объекта
     */
    @Override
    Collection<T> getActionsHistoryByIds(BigDecimal sysObjId, SyntheticId entityId) throws CoreException;
}
