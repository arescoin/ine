/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.oss.entity;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;
import ru.effts.ine.oss.entity.Person;

import java.util.Collection;

/**
 * Интерфейс для доступа к данным сущности "Персона".
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PersonAccess.java 3422 2011-12-05 10:39:43Z dgomon $"
 */
public interface PersonAccess<T extends Person> extends VersionableAccess<T> {

    /**
     * Метод возвращает все актуальные на текущий момент персоны
     *
     * @return коллекция персон
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
