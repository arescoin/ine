/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.links;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к описаниям связей.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkAccess.java 3411 2011-12-05 09:31:47Z dgomon $"
 */
public interface LinkAccess<T extends Link> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент описания связей
     *
     * @return коллекция описаний словарей
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает все описания связей, в которых участвует объект указанного типа.
     *
     * @param clazz класс объекта, для которого получаются описания связей
     * @return коллекция описаний связей
     * @throws GenericSystemException при ошибках в работе кеша или при некорректных данных
     */
    Collection<T> getLinksByObjType(Class clazz) throws GenericSystemException;
}
