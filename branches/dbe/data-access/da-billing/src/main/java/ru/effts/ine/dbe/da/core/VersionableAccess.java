/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Versionable;

import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableAccess.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public interface VersionableAccess<T extends Versionable> extends IdentifiableAccess<T> {

    /**
     * Получает предыдущие версии переданного объекта, оторбражающие историю его изменения.
     *
     * @param versionable объект, для которого получается история
     *
     * @return предыдущие версии объекта
     * @throws GenericSystemException в случае ошибок при работе с БД
     */
    Collection<T> getOldVersions(T versionable) throws GenericSystemException;

    /**
     * Получает предыдущие версии переданного объекта, оторбражающие историю его изменения.
     *
     * @param versionable объект, для которого получается история
     *
     * @return предыдущие версии объекта
     * @throws GenericSystemException в случае ошибок при работе с БД
     */
    Collection<T> getOldVersions(T versionable, Date from, Date to) throws GenericSystemException;
}
