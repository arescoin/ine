/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.dic;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.dic.DictionaryTerm;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к терминам словарных статей
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTermAccess.java 3407 2011-12-05 08:06:41Z dgomon $"
 */
public interface DictionaryTermAccess<T extends DictionaryTerm> extends DictionaryEntryAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент термины словарных статей
     *
     * @return коллекция всех терминов словарных статей
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Метод возвращает все действительные словарные термины указанного словаря на указанном языке
     *
     * @param dictionaryId идентификатор словаря
     * @param languageId   идентификатор  языка
     * @return коллекция словарных статей {@link ru.effts.ine.core.dic.DictionaryEntry}
     * @throws ru.effts.ine.core.GenericSystemException при ошибках получения данных
     */
    public Collection<T> getAllTermsForDictionaryAndLanguage(BigDecimal dictionaryId, BigDecimal languageId)
            throws GenericSystemException;

    /**
     * Метод возвращает все действительные словарные термины c указанным идентификтором словаря на всех языках
     *
     * @param dictionaryId идентификатор словаря
     * @param entryId      идентификатор словарной статьи
     * @return коллекция словарных статей {@link ru.effts.ine.core.dic.DictionaryEntry}
     * @throws ru.effts.ine.core.GenericSystemException при ошибках получения данных
     */
    public Collection<T> getAllTermsForDictionaryAndEntry(BigDecimal dictionaryId, BigDecimal entryId)
            throws GenericSystemException;

}
