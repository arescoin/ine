/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.links;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Описывает интерфейс доступа к связям.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataAccess.java 3411 2011-12-05 09:31:47Z dgomon $"
 */
public interface LinkDataAccess<T extends LinkData> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент связи
     *
     * @return коллекция связей
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;

    /**
     * Получает связные объекты для переданных {@link ru.effts.ine.core.Identifiable}-объекта и типа связи.
     *
     * @param objectToLink один из участников связи, для которого получаются привязанные объекты
     * @param linkId       идентификатор описателя связи {@link ru.effts.ine.core.links.Link}
     * @return коллекция идентификаторов объектов, привязанных к переданному
     *         {@link ru.effts.ine.core.Identifiable}-объекту
     * @throws ru.effts.ine.core.GenericSystemException в случае ошибки при работе с кешем
     */
    <E extends Identifiable> Collection<E> getLinkedObjects(Identifiable objectToLink, BigDecimal linkId)
            throws GenericSystemException;

    /**
     * Получает все связи в которых участвует переданный {@link ru.effts.ine.core.Identifiable}-объект.
     *
     * @param linkedObject один из участников связи
     * @return коллекция связей переданного {@link ru.effts.ine.core.Identifiable}-объекта
     * @throws ru.effts.ine.core.GenericSystemException в случае ошибки при работе с кешем
     */
    Collection<T> getLinkDataByObject(Identifiable linkedObject) throws GenericSystemException;

    /**
     * Получает все связи с указанным идентификатором описателя связи, в которых участвует переданный
     * {@link ru.effts.ine.core.Identifiable}-объект.
     *
     * @param linkedObject один из участников связи
     * @param linkId       идентификатор описателя связи
     * @return коллекция связей переданного {@link ru.effts.ine.core.Identifiable}-объекта
     * @throws ru.effts.ine.core.GenericSystemException в случае ошибки при работе с кешем
     */
    Collection<T> getLinkDataByObject(Identifiable linkedObject, BigDecimal linkId) throws GenericSystemException;

}
