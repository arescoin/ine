/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.constants;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Описывает интерфейс доступа к параметризованным константам
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantAccess.java 3406 2011-12-05 08:03:16Z dgomon $"
 */
public interface ParametrizedConstantAccess<T extends ParametrizedConstant> extends VersionableAccess<T> {

    /**
     * Метод возвращает все действительные на текущий момент параметризованные константы
     *
     * @return коллекция констант
     * @throws ru.effts.ine.core.CorruptedIdException при обнаружении некорректного идентификатора в системном объекте
     * @throws ru.effts.ine.core.CoreException при некорректных значениях в датах системного объекта
     */
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
