/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core;

import ru.effts.ine.core.*;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.cache.Producer;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Утилитный класс, обеспечивает разгрузку основного {@link ru.effts.ine.dbe.da.core.AbstractIdentifiableAccess}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableCreateAccess.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
class IdentifiableCreateAccess<T extends Identifiable> {

    /** Запросы на создание по классам */
    private static final Map<String, String> creationStringMap = new HashMap<String, String>();

    /** Запросы на создание в истории по классам */
    private static final Map<String, String> creationHistoryStringMap = new HashMap<String, String>();

    private AbstractIdentifiableAccess<T> access;

    private static final Object LOCK = new Object();

    IdentifiableCreateAccess(AbstractIdentifiableAccess<T> access) {
        this.access = access;
    }

    /**
     * Оболочка для вызова при создании нового объекта.
     * <p/>
     * <p/>
     * Последовательность работы метода:
     * <ul>
     * <li>Проводит проверку сохраняемого объекта</li>
     * <li>Блокирует по переданному идентификатору регион кеша</li>
     * <li>Создает объект</li>
     * <li>Обновляет коллекуию с идентификаторами</li>
     * <li>Размещает объект и коллекцию в кеше</li>
     * <li>Снимает блокировку на кеше</li>
     * </ul>
     * <br>
     * <b>Внимание! </b>Метод допустимо использовать толко для объектов использующих унифицированную
     * структуру хранения данных в кеше.<br>
     * В общем виде она выглядит как - <i>Один регион для типа, в котором под ключем имени региона содержится коллекция
     * идентификаторов, далее следуют объекты по своим идентификаторам</i>.
     *
     * @param identifiable   новый объект
     * @param allProducer    продюссер для конкретного типа, должен возвращать коллекцию идентификаторов
     *                       актуальных версий и заполнять кеш данными по самим объектам
     * @param region         имя региона, хранящего данный объект
     * @param autogenerateId признак использования стандартного механизма генерации идентификатора
     * @return идентификатор созданного объекта
     * @throws ru.effts.ine.core.GenericSystemException
     *          в случае возникновения ошибок при доступе к мета-информаци
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать
     *          null или некорректный объект в контексте операции
     */
    @SuppressWarnings({"unchecked"})
    protected T createObject(T identifiable, Producer allProducer, String region, boolean autogenerateId)
            throws IneIllegalArgumentException, GenericSystemException {

        access.checkApplicable(identifiable);

        try {
            access.beforeCreate(identifiable);

            BigDecimal[] newId = createObject(identifiable, autogenerateId, false);

            if (autogenerateId) {
                access.applyNewId(identifiable, newId);
            }

            SyntheticId cacheId = access.getSyntheticId(identifiable);

            if (!Cache.containsRegion(region)) {
                synchronized (LOCK) {
                    if (!Cache.containsRegion(region)) {
                        Cache.put(region, (Map) allProducer.get(region));
                    }
                }
            }

            Cache.put(region, cacheId, identifiable, false);

            access.afterCreate(identifiable);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return identifiable;
    }

    /**
     * Оболочка для вызова при создании объекта
     *
     * @param identifiable   создаваемый объект
     * @param autogenerateId признак использования стандартного механизма генерации идентификатора
     * @param history        признак использования исторической таблицы
     * @return массив идентификаторов созданного объекта
     * @throws CoreException               в случае возникновения ошибок при доступе к мета-информаци
     * @throws IneIllegalArgumentException при попытке передать null или некорректный объект в контексте операции
     */
    @SuppressWarnings({"unchecked"})
    protected BigDecimal[] createObject(T identifiable, boolean autogenerateId, boolean history)
            throws IneIllegalArgumentException, CoreException {

        Collection<String> columns = access.getColumnNames(autogenerateId);
        Object[] objects = access.getFieldValuesByColumns(identifiable, columns);

        String request;
        if (history) {
            request = obtainHistoryCreationRequestString((Class<T>) identifiable.getClass());
        } else {
            request = obtainCreationRequestString((Class<T>) identifiable.getClass(), autogenerateId);
        }

        String[] names = access.getIdColumns();

        return access.createObjectInDB(request, objects, names);
    }

    /**
     * Создает и возвращает запрос на создание нового объекта
     *
     * @param identifiableClass класс объекта
     * @param autogenerateId    признак использования стандартного механизма генерации идентификатора
     * @return строка с запросом
     * @throws ru.effts.ine.core.CoreException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    protected String obtainCreationRequestString(Class<T> identifiableClass, boolean autogenerateId)
            throws CoreException {

        synchronized (creationStringMap) {
            // такая конструкция клюа - access.getClass().getName() + access.getTableName() - обусловлена тем,
            // что у CustomAttributeValueAccessImpl'а getTableName зависит от данных, изменяемых в данный момент.
            if (!creationStringMap.containsKey(access.getClass().getName() + access.getTableName())) {
                creationStringMap.put(access.getClass().getName() + access.getTableName(),
                        obtainCreationRequestString(identifiableClass, autogenerateId, false));
            }
        }

        return creationStringMap.get(access.getClass().getName() + access.getTableName());
    }

    /**
     * Создает и возвращает запрос на создание нового объекта в "исторической" таблице
     *
     * @param identifiableClass класс объекта
     * @return строка с запросом
     * @throws ru.effts.ine.core.CoreException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    protected String obtainHistoryCreationRequestString(Class<T> identifiableClass) throws CoreException {

        synchronized (creationHistoryStringMap) {
            if (!creationHistoryStringMap.containsKey(access.getClass().getName() + access.getTableName())) {
                creationHistoryStringMap.put(access.getClass().getName() + access.getTableName(),
                        obtainCreationRequestString(identifiableClass, false, true));
            }
        }

        return creationHistoryStringMap.get(access.getClass().getName() + access.getTableName());
    }

    /**
     * Создает и возвращает запрос на создание нового объекта
     *
     * @param identifiableClass новый объект
     * @param autogenerateId    признак использования стандартного механизма генерации идентификатора
     * @param history           признак использования исторической таблицы
     * @return строка с запросом
     * @throws ru.effts.ine.core.CoreException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    protected String obtainCreationRequestString(Class<T> identifiableClass, boolean autogenerateId, boolean history)
            throws CoreException {

        Collection<String> columns = access.getColumnNames(autogenerateId);

        StringBuilder stringBuilderVals = new StringBuilder(250);
        StringBuilder stringBuilderNames = new StringBuilder(250);

        stringBuilderNames.append("INSERT INTO ").append(access.getTableName());

        if (history) {
            stringBuilderNames.append(AbstractAccess.HISTORY);
        }
        stringBuilderNames.append(" (");

        if (autogenerateId) {
            stringBuilderNames.append(access.getIdColumns()[0]).append(", ");
            stringBuilderVals.append(access.getSequencePart(identifiableClass)).append(", ");
        }

        for (String columnName : columns) {
            stringBuilderNames.append(columnName).append(", ");
            stringBuilderVals.append("?, ");
        }

        stringBuilderNames.delete(stringBuilderNames.length() - 2, stringBuilderNames.length());
        stringBuilderNames.append(") VALUES (");
        stringBuilderNames.append(stringBuilderVals);
        stringBuilderNames.delete(stringBuilderNames.length() - 2, stringBuilderNames.length());
        stringBuilderNames.append(")");

        return stringBuilderNames.toString();
    }

}
