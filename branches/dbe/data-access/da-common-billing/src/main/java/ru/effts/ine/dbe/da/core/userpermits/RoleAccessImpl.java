/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.*;
import ru.effts.ine.core.userpermits.Role;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RoleAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class RoleAccessImpl<T extends Role> extends AbstractVersionableAccess<T> implements RoleAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "ROLES";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_NAME = "role_name";

    /** Запрос на выборку всех системных ролей */
    public static final String ALL_SYS_ROLES_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_NAME +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.userpermits.Role} */
    public static final String CACHE_REGION_SYSTEM_ROLES = Role.class.getName();

    private static final Map<BigDecimal, Role> ROLE_MAP = new HashMap<BigDecimal, Role>();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(Role.NAME, COLUMN_NAME);
        setInstanceFieldsMapping(RoleAccessImpl.class, map);

        registerVersionableHelpers(RoleAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    public void addHelper(CRUDHelper<T> helper) {

        LinkedHashSet<CRUDHelper> helpers = new LinkedHashSet<CRUDHelper>();
        helpers.add(helper);

        registerVersionableHelpers(RoleAccessImpl.class, helpers);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SYSTEM_ROLES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(Role.class);

        this.setVersionableFields(resultSet, result);
        result.setName(this.getString(resultSet, COLUMN_NAME));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {

        Collection<T> roles = getAllObjects(CACHE_REGION_SYSTEM_ROLES, ALL_SYS_ROLES_SELECT);
        for (T role : roles) {
            processRole(role);
        }

        return roles;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {

        T role = (T) ROLE_MAP.get(id[0]);
        if (role == null) {
            role = getObjectById(CACHE_REGION_SYSTEM_ROLES, ALL_SYS_ROLES_SELECT, id);
            if (role != null) {
                processRole(role);
            }
        }

        return role;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        T role = this.createObject(identifiable, CACHE_REGION_SYSTEM_ROLES, ALL_SYS_ROLES_SELECT, true);
        processRole(role);

        return role;
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        T role = this.updateObject(identifiable, newIdentifiable, CACHE_REGION_SYSTEM_ROLES);
        processRole(role);

        return role;
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_SYSTEM_ROLES, ALL_SYS_ROLES_SELECT);
        ROLE_MAP.remove(identifiable.getCoreId());
    }


    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Role.class;
    }

    private void processRole(T t) {
        ROLE_MAP.put(t.getCoreId(), t);
    }

}
