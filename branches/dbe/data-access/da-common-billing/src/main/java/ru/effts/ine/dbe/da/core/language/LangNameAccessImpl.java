/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.language;

import ru.effts.ine.core.*;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.dbe.da.core.AbstractIdentifiableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangNameAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class LangNameAccessImpl<T extends LangName> extends AbstractIdentifiableAccess<T> implements LangNameAccess<T> {

    /** Таблица состояний переключателя и префикс для обращения к ее столбцам */
    public static final String TABLE = "LANGS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_LANG_NAME = "lng_name";

    /** Запрос на выборку всех описаний языков */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_LANG_NAME +
            " FROM " + TABLE;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.language.LangName} */
    public static final String CACHE_REGION_SYSTEM_LANGUAGE_NAMES = LangName.class.getName();

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(LangName.NAME, COLUMN_LANG_NAME);
        setInstanceFieldsMapping(LangNameAccessImpl.class, map);

        registerIdentifiableHelpers(LangNameAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SYSTEM_LANGUAGE_NAMES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(LangName.class);

        this.setIdentifiableFields(resultSet, value);
        value.setName(this.getString(resultSet, COLUMN_LANG_NAME));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_SYSTEM_LANGUAGE_NAMES, ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_SYSTEM_LANGUAGE_NAMES, ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_SYSTEM_LANGUAGE_NAMES, ALL_DATA_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_SYSTEM_LANGUAGE_NAMES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_SYSTEM_LANGUAGE_NAMES, ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return LangName.class;
    }

}
