/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: AbstractCRUDHelper.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public abstract class AbstractCRUDHelper<T extends Identifiable> implements CRUDHelper<T> {

    @Override
    public void beforeCreate(T identifiable, IdentifiableAccess<T> access) throws GenericSystemException {
    }

    @Override
    public void afterCreate(T identifiable) throws GenericSystemException {
    }

    @Override
    public void beforeUpdate(T identifiable, T newIdentifiable) throws GenericSystemException {
    }

    @Override
    public void afterUpdate(T identifiable, T newIdentifiable) throws GenericSystemException {
    }

    @Override
    public void beforeDelete(T identifiable) throws GenericSystemException {
    }

    @Override
    public void afterDelete(T identifiable) throws GenericSystemException {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        //noinspection SimplifiableIfStatement
        if (!(o instanceof AbstractCRUDHelper)) {
            return false;
        }
        return this.getClass().getName().equals(o.getClass().getName());
    }

    @Override
    public int hashCode() {
        return this.getClass().getName().hashCode();
    }

}
