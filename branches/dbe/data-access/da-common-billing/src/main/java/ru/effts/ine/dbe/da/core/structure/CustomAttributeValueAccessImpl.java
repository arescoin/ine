/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.core.structure.SystemObject;
import ru.effts.ine.dbe.da.core.*;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.cache.Producer;
import ru.effts.ine.utils.cache.StorageFilterImpl;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValueAccessImpl.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public class CustomAttributeValueAccessImpl<T extends CustomAttributeValue>
        extends AbstractVersionableAccess<T> implements CustomAttributeValueAccess<T> {

    @SuppressWarnings("UnusedDeclaration")
    private static final long serialVersionUID = 43L;

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "###_TABLE_NOT_DEFINED_###";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ATTR_N = "attr_n";
    public static final String COLUMN_ENT_N = "ent_n";
    public static final String COLUMN_VALUE = "attr_val";

    /** Запроса на выборку значений атрибутов по конкретному описателю */
    public static final String VALUES_BY_ATTR_SELECT = "SELECT " + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ATTR_N +
            ", " + TABLE_PREF + COLUMN_ENT_N +
            ", " + TABLE_PREF + COLUMN_VALUE +
            ", NULL as " + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD +
            " AND " + TABLE_PREF + COLUMN_ATTR_N + " = ?";

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.structure.CustomAttributeValue} */
    public static final String CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES = CustomAttributeValue.class.getName();

    private static final Logger logger = Logger.getLogger(CustomAttributeValue.class.getName());

    /** Вспомогательный объект, необходимый для получения актульного имени таблицы при модифицирующих БД операциях */
    private CustomAttribute attribute;

    /** Вспомогательная карта атрибутов, необходимая для оптимизации получения значений из кеша */
    private Map<BigDecimal, CustomAttribute> attributesMap;

    private static final Map<String, Map<String, BigDecimal>> ID_COLUMNS_MAP = new HashMap<>(200);


    static {
        final Map<String, String> map = new HashMap<>();
        map.put(CustomAttributeValue.CORE_DSC, null);
        map.put(CustomAttributeValue.CORE_ID, COLUMN_ATTR_N);
        map.put(CustomAttributeValue.ENT_N, COLUMN_ENT_N);
        map.put(CustomAttributeValue.VALUE, COLUMN_VALUE);
        setInstanceFieldsMapping(CustomAttributeValueAccessImpl.class, map);

        setIdColumns(CustomAttributeValueAccessImpl.class, new String[]{COLUMN_ENT_N, COLUMN_ATTR_N});

        Set<CRUDHelper> customAttributeValueHelpers = new LinkedHashSet<>();
        customAttributeValueHelpers.add(new CustomAttributeValueHelper());
        registerVersionableHelpers(CustomAttributeValueAccessImpl.class, customAttributeValueHelpers);
    }


    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES;
    }

    @Override
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(2, null, id);
        identifiable.setEntityId(id[0]);
        identifiable.setCoreId(id[1]);
    }

    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {

        String tableName = this.getTableName(this.getCustomAttribute(identifiable.getCoreId()));

        SyntheticId result = new SyntheticId();
        result.addIdEntry(getColumnId(tableName, COLUMN_ENT_N), identifiable.getEntityId());
        result.addIdEntry(getColumnId(tableName, COLUMN_ATTR_N), identifiable.getCoreId());

        return result;
    }

    @Override
    public String getTableName() {
        return this.attribute == null ? null : getTableName(this.attribute);
    }

    private static BigDecimal getColumnId(String tableName, String columnName) throws GenericSystemException {

        if (!ID_COLUMNS_MAP.containsKey(tableName)) {
            synchronized (ID_COLUMNS_MAP) {
                if (!ID_COLUMNS_MAP.containsKey(tableName)) {
                    ID_COLUMNS_MAP.put(tableName, new HashMap<String, BigDecimal>(2));
                }
            }
        }

        if (!ID_COLUMNS_MAP.get(tableName).containsKey(columnName)) {
            synchronized (ID_COLUMNS_MAP) {
                if (!ID_COLUMNS_MAP.get(tableName).containsKey(columnName)) {
                    ID_COLUMNS_MAP.get(tableName).put(columnName, FieldAccess.getColumnId(tableName, columnName));
                }
            }
        }

        return ID_COLUMNS_MAP.get(tableName).get(columnName);
    }

    private String getTableName(CustomAttribute attribute) {
        try {
            SystemObject systemObject = FieldAccess.getObjectById(attribute.getSystemObjectId());
            return systemObject.getName() + "_" + attribute.getDataType().getType().getSimpleName();
        } catch (CoreException e) {
            return null;
        }
    }

    private static CustomAttributeAccessImpl<CustomAttribute> getAttributeAccess() throws GenericSystemException {
        return (CustomAttributeAccessImpl) AccessFactory.getImplementation(CustomAttribute.class);
    }

    private CustomAttribute getCustomAttribute(BigDecimal customAttributeId) throws GenericSystemException {
        if (customAttributeId == null) {
            return null;
        }
        try {
            if (this.attributesMap != null) {
                return this.attributesMap.get(customAttributeId);
            } else {
                return getAttributeAccess().getObjectById(customAttributeId);
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        T value = (T) IdentifiableFactory.getImplementation(CustomAttributeValue.class);

        this.setVersionableFields(resultSet, value, COLUMN_ATTR_N);
        value.setEntityId(this.getId(resultSet, COLUMN_ENT_N));
        this.setAttributeValue(resultSet, value);

        return value;
    }

    private void setAttributeValue(ResultSet resultSet, T attributeValue) throws SQLException, CoreException {
        CustomAttribute attribute = this.getCustomAttribute(attributeValue.getCoreId());

        Object value;
        switch (attribute.getDataType()) {
            case stringType:
                attributeValue.setValue(this.getString(resultSet, COLUMN_VALUE));
                break;
            case longType:
                value = this.getBigDecimal(resultSet, COLUMN_VALUE);
                attributeValue.setValue(resultSet.wasNull() ? null : ((BigDecimal) value).longValue());
            case bigDecimalType:
                attributeValue.setValue(this.getBigDecimal(resultSet, COLUMN_VALUE));
                break;
            case dateType:
                attributeValue.setValue(this.getDate(resultSet, COLUMN_VALUE));
                break;
            case booleanType:
                value = this.getBigDecimal(resultSet, COLUMN_VALUE);
                attributeValue.setValue(!resultSet.wasNull() && ((BigDecimal) value).longValue() == 1);
                break;
            default:
                throw new GenericSystemException("Wrong dataType value in CustomAttribute, id = " + attribute.getCoreId());
        }
    }


    @Override
    public Collection<T> getAllObjects() throws CoreException, UnsupportedOperationException {

        Collection<String> interfaces = (Collection<String>) Cache.get(CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES,
                CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES, new AllValuesProducer());

        Collection<T> result = new ArrayList<>(1000000);
        for (String intfName : interfaces) {
            final Collection<T> allObjects = this.getAllObjects(
                    CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + '.' + intfName, new ValuesByTypeProducer(intfName));

            result.addAll(allObjects);
        }

        return result;
    }


    @Override
    public Collection<T> getOldVersions(T versionable) throws GenericSystemException {

        this.attribute = this.getCustomAttribute(versionable.getCoreId());
        Collection<T> versions = super.getOldVersions(versionable);
        this.attribute = null;

        return versions;
    }


    @Override
    public T getValue(BigDecimal identifiableId, CustomAttribute attribute) throws CoreException {

        if (attribute == null) {
            throw new IneIllegalArgumentException("attribute object must not be null.");
        }

        String interfaceName = FieldAccess.getInterface(attribute.getSystemObjectId());

        SearchCriteriaProfile profile = this.getSearchCriteriaProfile();
        Set<SearchCriterion> criteria = new HashSet<>();

        SearchCriterion<BigDecimal> criterionCoreId =
                profile.getSearchCriterion(T.CORE_ID, CriteriaRule.equals, attribute.getCoreId());

        criterionCoreId.addAdditionalParam(interfaceName);
        criteria.add(criterionCoreId);

        SearchCriterion<BigDecimal> criterionEntityId =
                profile.getSearchCriterion(T.ENT_N, CriteriaRule.equals, identifiableId);

        criterionEntityId.addAdditionalParam(interfaceName);
        criteria.add(criterionEntityId);

        Collection<T> result = this.searchObjects(criteria);
        if (result.size() > 1) {
            throw new IneCorruptedStateException("Too many objects[" + result.size() + "] found for attributeId[" +
                    attribute.getCoreId() + "] and entityId[" + identifiableId + "]");
        }

        return result.isEmpty() ? null : result.iterator().next();
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        this.checkIdParam(2, "id[0] - value.entityId, id[1] - value.coreId", id);

        return this.getValue(id[0], this.getCustomAttribute(id[1]));
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        try {
            this.attribute = this.getCustomAttribute(identifiable.getCoreId());
            UserHolder.getUser().setAttribute(UserProfile.TABLE_ID, attribute.getSystemObjectId());
            String identifiableName = this.getInterfaceName(identifiable.getCoreId());
            this.createObject(identifiable, new ValuesByTypeProducer(identifiableName),
                    CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + '.' + identifiableName, false);

            return identifiable;
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        } finally {
            attribute = null;
        }
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        try {
            attribute = this.getCustomAttribute(identifiable.getCoreId());
            UserHolder.getUser().setAttribute(UserProfile.TABLE_ID, attribute.getSystemObjectId());
            this.updateObject(identifiable, newIdentifiable, CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + '.' +
                    this.getInterfaceName(identifiable.getCoreId()));

            return newIdentifiable;
        } finally {
            attribute = null;
        }
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        try {
            attribute = this.getCustomAttribute(identifiable.getCoreId());
            UserHolder.getUser().setAttribute(UserProfile.TABLE_ID, attribute.getSystemObjectId());
            String identifiableName = this.getInterfaceName(identifiable.getCoreId());
            this.deleteObject(identifiable, new ValuesByTypeProducer(identifiableName),
                    CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + '.' + identifiableName);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        } finally {
            attribute = null;
        }
    }

    @Override
    protected Object getRegionKey(SearchCriterion criterion) {

        AbstractIdentifiable.checkNull(criterion, "criterion");

        return CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + DOT + this.getInterfaceName(criterion);
    }

    @Override
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {
        String regionKey = this.checkCacheAndGetRegionKey(searchCriteria);
        return (Collection<T>) Cache.search(regionKey, new StorageFilterImpl(searchCriteria, fieldName, regionKey));
    }

    @Override
    public boolean containsObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        String regionKey = this.checkCacheAndGetRegionKey(searchCriteria);
        return Cache.containsObjects(regionKey, new StorageFilterImpl(searchCriteria, regionKey));
    }

    @Override
    public int getObjectCount(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        String regionKey = this.checkCacheAndGetRegionKey(searchCriteria);
        return Cache.getObjectCount(regionKey, new StorageFilterImpl(searchCriteria, regionKey));
    }

    private String checkCacheAndGetRegionKey(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        String intfName = this.getInterfaceName(searchCriteria);
        this.checkCache(this, intfName);
        return CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + DOT + intfName;
    }

    /**
     * Проверяем заполнение кеша данными для переданного интерфейса.
     *
     * @param access акцесс, на котором проверяем кеш. В данном методе не используется.
     * @param param  дополнительный параметр для проверки. должен представлять собой имя интерфейса объекта,
     *               которому принадлежит искомый произвольный атрибут.
     * @throws ru.effts.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешем
     */
    @Override
    protected void checkCache(IdentifiableAccess access, Object param) throws GenericSystemException {
        try {
            String intfName = (String) param;
            if (!Cache.containsRegion(CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + DOT + intfName)) {
                Cache.getRegionContent(CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + DOT + intfName,
                        new ValuesByTypeProducer(intfName));
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    @Override
    protected void checkCache(StorageFilter[] filters, Object param) throws GenericSystemException {
        for (StorageFilter filter : filters) {
            SearchCriterion criterion = ((StorageFilterImpl) filter).getFirstSearchCriterion();
            AbstractIdentifiable.checkNull(criterion, "criterion");

            IdentifiableAccess access = (IdentifiableAccess)
                    AccessFactory.getImplementation(IdentifiableFactory.getInterface(criterion.getSearchClass()));
            if (CustomAttributeValue.class.isAssignableFrom(criterion.getSearchClass())) {
                this.checkCache(access, this.getInterfaceName(criterion));
            } else {
                super.checkCache(access, param);
            }
        }
    }

    /**
     * Получаем интерфейс объекта, которому принадлежит указанный произвольный атрибут
     *
     * @param customAttributeId идентификатор произвольного атрибута
     * @return имя интерфейса
     * @throws ru.effts.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешем
     */
    private String getInterfaceName(BigDecimal customAttributeId) throws GenericSystemException {
        CustomAttribute attribute = this.getCustomAttribute(customAttributeId);
        try {
            return FieldAccess.getInterface(attribute.getSystemObjectId());
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * В данный метод необходимо передавать список поисковых критериев, первый из которых содержит в AdditionalParams
     * имя класса-владельца атрибута
     *
     * @param searchCriteria список критериев поиска с дополнительным параметром
     * @return имя класс-владельца атрибута
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          в случае ошибок при проверке входного параметра
     */
    private String getInterfaceName(Set<SearchCriterion> searchCriteria) throws IneIllegalArgumentException {

        AbstractIdentifiable.checkNull(searchCriteria, "searchCriteria");

        SearchCriterion criterion = searchCriteria.iterator().next();
        AbstractIdentifiable.checkNull(criterion, "criterion");

        return this.getInterfaceName(criterion);
    }

    /**
     * В данный метод необходимо передавать SearchCriterion содержащий в AdditionalParams имя класса-владельца атрибута
     *
     * @param criterion критерий поиска с дополнительным параметром
     * @return имя класс-владельца атрибута
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          в случае ошибок при проверке входного параметра
     */
    private String getInterfaceName(SearchCriterion criterion) {
        List<Serializable> serializables = criterion.getAdditionalParams();
        if (serializables.isEmpty()) {
            throw new IneIllegalArgumentException("Parent class must not be null");
        }

        return (String) serializables.get(0);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return CustomAttributeValue.class;
    }

    /**
     * В виду особенностей работы со значениями произвольных атрибутов (нет точно заданного имени таблицы) приходится
     * по-особенному работать и с кешем.
     * <p/>
     * Основной способ работы - получение всех значений произвольных атрибутов для определённого базового объекта (или
     * другими словами - для конкретной таблицы).
     * <p/>
     * Поэтому структура данных в кеше получается следующего вида:
     * <ul>
     * <li>Под ключом {@link #CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES} будет лежать список интерфейсов, поддерживаемых
     * системой. Он нужен только для метода {@link #getAllObjects()}</li>
     * <li>Системная схема размещения данных вида (regionKey, regionKey, IdsCollection) и (regionKey, id, data)
     * применяется для каждой таблицы базового объекта индивидуально и выполняется данным продюссером. Ключ regionKey
     * формируется в виде "{@link #CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES} + '.' + identifiableInterface"
     * (интерфейс таблицы)</li>
     * </ul>
     */
    public final class ValuesByTypeProducer implements Producer {

        private Class<? extends Identifiable> identifiableClass;

        public ValuesByTypeProducer(String identifiableClass) {
            try {
                this.identifiableClass = (Class<? extends Identifiable>) Class.forName(identifiableClass);
            } catch (ClassNotFoundException e) {
                logger.log(Level.WARNING, e.getMessage(), e);
                this.identifiableClass = null;
            }
        }

        @Override
        public Object get(Object key) throws CoreException {

            // Коллекция составных идентификаторов значений произвольных атрибутов
            Map<SyntheticId, T> result = new HashMap<>(500000);

            Collection<CustomAttribute> attributes = CustomAttributeValueAccessImpl
                    .getAttributeAccess().getAllAttributesByObject(this.identifiableClass);
            CustomAttributeValueAccessImpl.this.attributesMap =
                    new HashMap<>(attributes.size());

            for (CustomAttribute attribute : attributes) {
                CustomAttributeValueAccessImpl.this.attributesMap.put(attribute.getCoreId(), attribute);
            }

            // Получаем все атрибуты, принадлежашие заданной таблице и получаем их значения
            for (CustomAttribute attribute : attributes) {
                BigDecimal attributeId = attribute.getCoreId();

                Map<SyntheticId, T> values = new HashMap<>(500000);
                for (T value : CustomAttributeValueAccessImpl.this.getObjects(false, VALUES_BY_ATTR_SELECT.replaceAll(
                        TABLE, CustomAttributeValueAccessImpl.this.getTableName(attribute)), attributeId)) {

                    // добавляем в коллекцию идентификаторов
                    SyntheticId cacheId = CustomAttributeValueAccessImpl.this.getSyntheticId(value);
                    values.put(cacheId, value);
                }

                result.putAll(values);
            }
            CustomAttributeValueAccessImpl.this.attributesMap = null;

            return result;
        }

    }


    public final class AllValuesProducer implements Producer {

        @Override
        public Object get(Object key) throws CoreException {

            Collection<String> result = new HashSet<>(50);
            for (String intfName : FieldAccess.getSupportedIntefaces()) {
                if (intfName.equals(CustomAttributeValue.class.getName()) ||
                        intfName.equals(CustomAttribute.class.getName())) {
                    // это приседание нужно, чтоб не было косяков в кеше с ключом равным null.
                    // т.к. у произвольных атрибутов не может быть произвольных атрибутов.
                    continue;
                }
                try {
                    Class.forName(intfName);
                    Cache.getRegionContent(CACHE_REGION_CUSTOM_ATTRIBUTE_VALUES + '.' + intfName,
                            new ValuesByTypeProducer(intfName));
                    result.add(intfName);
                } catch (Exception e) {
                    logger.log(Level.SEVERE,
                            "Failed to load attribute values for interface " + intfName + ", cause: ", e);
                }
            }

            return result;
        }

    }


    public static final class CustomAttributeValueHelper<E extends CustomAttributeValue>
            extends AbstractCRUDHelper<E> {

        private static CustomAttributeValueAccessImpl getValueAccess() throws GenericSystemException {
            return (CustomAttributeValueAccessImpl) AccessFactory.getImplementation(CustomAttributeValue.class);
        }

        @Override
        public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {
            CustomAttribute attribute = getValueAccess().getCustomAttribute(identifiable.getCoreId());

            // проверяем наличие значения для обязательного атрибута
            this.checkRequired(identifiable, attribute);

            // проверяем тип данных
            this.checkType(identifiable, attribute.getDataType());

            if (attribute.getDataType().equals(DataType.bigDecimalType)) {
                // проверяем наличие объекта, если в атрибуте задан паттерн объекта
                if (attribute.getPattern() != null && identifiable.getValue() != null) {
                    this.checkObjectExists(identifiable, attribute);
                }
            }
        }

        @Override
        public void beforeUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {
            CustomAttribute attribute = getValueAccess().getCustomAttribute(newIdentifiable.getCoreId());

            // проверяем наличие значения для обязательного атрибута
            this.checkRequired(newIdentifiable, attribute);

            // проверяем, тип данных
            this.checkType(newIdentifiable, attribute.getDataType());

            if (attribute.getDataType().equals(DataType.bigDecimalType)) {
                // проверяем наличие объекта, если в атрибуте задан паттерн объекта
                if (attribute.getPattern() != null && newIdentifiable.getValue() != null
                        && !newIdentifiable.getValue().equals(identifiable.getValue())) {
                    this.checkObjectExists(identifiable, attribute);
                }
            }
        }

        private void checkRequired(E identifiable, CustomAttribute attribute) {
            if (attribute.isRequired() && identifiable.getValue() == null) {
                throw new IneIllegalArgumentException("Attribute ["
                        + attribute.getAttributeName() + "] value is required, but was null");
            }
        }

        private void checkType(E identifiable, DataType dataType) throws GenericSystemException {
            Object value = identifiable.getValue();
            if (!dataType.getType().isAssignableFrom(value.getClass())) {
                throw new IneIllegalArgumentException("Incorrect dataType of value found [" +
                        value.getClass().getName() + "], but expected [" + dataType.getType() + "]");
            }
        }

        private void checkObjectExists(E identifiable, CustomAttribute attribute) throws GenericSystemException {
            BigDecimal value = (BigDecimal) identifiable.getValue();
            try {
                if (PatternUtils.getObjectByPattern(attribute.getPattern(), value) == null) {
                    throw new IneIllegalArgumentException(
                            "Can't find object by pattern " + attribute.getPattern() + " and value " + value);
                }
            } catch (CoreException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

    }

}
