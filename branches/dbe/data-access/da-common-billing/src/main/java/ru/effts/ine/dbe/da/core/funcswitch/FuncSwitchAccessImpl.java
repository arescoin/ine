/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.funcswitch;

import ru.effts.ine.core.*;
import ru.effts.ine.core.funcswitch.FuncSwitch;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitchAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class FuncSwitchAccessImpl<T extends FuncSwitch>
        extends AbstractVersionableAccess<T> implements FuncSwitchAccess<T> {

    /** Таблица состояний переключателя и префикс для обращения к ее столбцам */
    public static final String TABLE = "FUNC_STATE";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_FUNC_N = "func_n";
    public static final String COLUMN_STATE = "state";
    public static final String COLUMN_C_DATE = "c_date";
    public static final String COLUMN_CM = "cm";
    public static final String COLUMN_STATE_CODE = "state_code";

    /** Запрос на выборку всех переключателей функциональности */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_FUNC_N +
            ", " + TABLE_PREF + COLUMN_STATE +
            ", " + TABLE_PREF + COLUMN_C_DATE +
            ", " + TABLE_PREF + COLUMN_CM +
            ", " + TABLE_PREF + COLUMN_STATE_CODE +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", NULL AS " + COLUMN_DSC +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN "
            + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.funcswitch.FuncSwitch} */
    public static final String CACHE_REGION_FUNC_SWITCHES = FuncSwitch.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(FuncSwitch.CORE_ID, COLUMN_FUNC_N);
        map.put(FuncSwitch.STATE, COLUMN_STATE);
        map.put(FuncSwitch.C_DATE, COLUMN_C_DATE);
        map.put(FuncSwitch.MARK, COLUMN_CM);
        map.put(FuncSwitch.STATE_CODE, COLUMN_STATE_CODE);
        setInstanceFieldsMapping(FuncSwitchAccessImpl.class, map);

        setIdColumns(FuncSwitchAccessImpl.class, new String[]{COLUMN_FUNC_N});

        registerVersionableHelpers(FuncSwitchAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_FUNC_SWITCHES;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_FUNC_SWITCHES, ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_FUNC_SWITCHES, ALL_DATA_SELECT, id);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(FuncSwitch.class);

        this.setVersionableFields(resultSet, result, COLUMN_FUNC_N);
        result.setEnabled(this.getBoolean(resultSet, COLUMN_STATE));
        result.setChangeDate(this.getDate(resultSet, COLUMN_C_DATE));
        result.setMark(this.getId(resultSet, COLUMN_CM));
        result.setStateCode(this.getString(resultSet, COLUMN_STATE_CODE));

        return result;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_FUNC_SWITCHES, ALL_DATA_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_FUNC_SWITCHES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_FUNC_SWITCHES, ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return FuncSwitch.class;
    }

}
