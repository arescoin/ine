/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.strings;

import ru.effts.ine.core.*;
import ru.effts.ine.core.strings.MessageString;
import ru.effts.ine.dbe.da.core.AbstractAccess;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.cache.Producer;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class MessageStringAccessImpl<T extends MessageString> extends AbstractAccess implements MessageStringAccess<T> {

    /** Таблица состояний переключателя и префикс для обращения к ее столбцам */
    public static final String TABLE = "MESSAGE_STRINGS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_MSG_KEY = "msg_key";
    public static final String COLUMN_LANG_N = "lang_n";
    public static final String COLUMN_VAL = "val";

    /** Запрос на выборку всех строк локализации */
    public static final String ALL_STRINGS_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_MSG_KEY +
            ", " + TABLE_PREF + COLUMN_LANG_N +
            ", " + TABLE_PREF + COLUMN_VAL +
            " FROM " + TABLE;

    public static final String CREATE_STRING = "INSERT INTO " + TABLE +
            " (" + COLUMN_MSG_KEY +
            ", " + COLUMN_LANG_N +
            ", " + COLUMN_VAL +
            ") VALUES (?, ?, ?)";

    public static final String UPDATE_STRING = "UPDATE " + TABLE + " SET " + COLUMN_VAL + " = ? WHERE " +
            COLUMN_MSG_KEY + " = ? AND " + COLUMN_LANG_N + " = ? ";

    public static final String DELETE_STRING = "DELETE FROM " + TABLE + " WHERE " +
            COLUMN_MSG_KEY + " = ? AND " + COLUMN_LANG_N + " = ? ";

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.strings.MessageString} */
    public static final String CACHE_REGION_MESSAGE_STRINGS = MessageString.class.getName();
    public static final String INIT_MARK = CACHE_REGION_MESSAGE_STRINGS + ".initMark";

    protected static Map<String, String> INSTANCE_FIELDS_MAP = new HashMap<String, String>();


    static {
        INSTANCE_FIELDS_MAP.put(MessageString.KEY, COLUMN_MSG_KEY);
        INSTANCE_FIELDS_MAP.put(MessageString.LANG, COLUMN_LANG_N);
        INSTANCE_FIELDS_MAP.put(MessageString.MSG, COLUMN_VAL);
    }


    @Override
    public Map<String, String> getInstanceFields() {
        return Collections.unmodifiableMap(INSTANCE_FIELDS_MAP);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_MESSAGE_STRINGS;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementationSM(MessageString.class);

        result.setKey(this.getString(resultSet, COLUMN_MSG_KEY));
        result.setLanguageCode(this.getBigDecimal(resultSet, COLUMN_LANG_N));
        result.setMessage(this.getString(resultSet, COLUMN_VAL));

        return result;
    }


    public final class AllStringsProducer implements Producer {

        @SuppressWarnings({"unchecked"})
        @Override
        public Object get(Object key) throws CoreException {
            Collection<T> result = new HashSet<T>(10000);
            Map<String, Map<BigDecimal, T>> allByKey = new HashMap<String, Map<BigDecimal, T>>(5000);

            for (T messageString : (Collection<T>) getObjects(false, ALL_STRINGS_SELECT)) {
                result.add(messageString);

                if (!allByKey.containsKey(messageString.getKey())) {
                    allByKey.put(messageString.getKey(), new HashMap<BigDecimal, T>());
                }
                allByKey.get(messageString.getKey()).put(messageString.getLanguageCode(), messageString);
            }

            for (Map.Entry<String, Map<BigDecimal, T>> mapEntry : allByKey.entrySet()) {
                Cache.put(CACHE_REGION_MESSAGE_STRINGS, mapEntry.getKey(), mapEntry.getValue());
            }

            Cache.put(CACHE_REGION_MESSAGE_STRINGS, INIT_MARK, Boolean.TRUE);

            return result;
        }
    }

    /** Возвращает коллекцию с имеющимися локализационными ключами */
    @SuppressWarnings({"unchecked"})
    @Override
    public Collection<T> getAllObjects() throws CoreException {

        Collection<T> result = (Collection<T>) Cache.get(
                CACHE_REGION_MESSAGE_STRINGS, CACHE_REGION_MESSAGE_STRINGS, new Producer.NullProducer());
        if (result == null) {
            Cache.put(CACHE_REGION_MESSAGE_STRINGS, CACHE_REGION_MESSAGE_STRINGS, new AllStringsProducer().get(null));
        }

        return (Collection<T>) Cache.get(
                CACHE_REGION_MESSAGE_STRINGS, CACHE_REGION_MESSAGE_STRINGS, new Producer.NullProducer());
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public Map<BigDecimal, T> getStringsByKey(String stringKey) throws CoreException {
        this.checkInit();

        return (Map<BigDecimal, T>) Cache.get(CACHE_REGION_MESSAGE_STRINGS, stringKey, Producer.NULL_PRODUCER);
    }

    @Override
    public T getStringByKeyAndLang(String stringKey, BigDecimal langCode) throws CoreException {
        Map<BigDecimal, T> stringsByKey = getStringsByKey(stringKey);

        return stringsByKey != null ? stringsByKey.get(langCode) : null;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        this.checkApplicable(identifiable);

        try {
            this.createObjectInDB(CREATE_STRING, new Object[]{
                    identifiable.getKey(), identifiable.getLanguageCode(), identifiable.getMessage()}, null);

            Collection<T> strings = getAllObjects();
            strings.add(identifiable);
            Cache.put(CACHE_REGION_MESSAGE_STRINGS, CACHE_REGION_MESSAGE_STRINGS, strings);

            Map<BigDecimal, T> stringsByLang = getStringsByKey(identifiable.getKey());
            if (stringsByLang == null) {
                stringsByLang = new HashMap<BigDecimal, T>();
            }
            stringsByLang.put(identifiable.getLanguageCode(), identifiable);
            Cache.put(CACHE_REGION_MESSAGE_STRINGS, identifiable.getKey(), stringsByLang, false);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return identifiable;
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        this.checkApplicable(identifiable);
        this.checkApplicable(newIdentifiable);

        try {
            updateObjectInDB(UPDATE_STRING,
                    new Object[]{identifiable.getMessage(), identifiable.getKey(), identifiable.getLanguageCode()});

            Collection<T> strings = getAllObjects();
            strings.remove(identifiable);
            strings.add(newIdentifiable);
            Cache.put(CACHE_REGION_MESSAGE_STRINGS, CACHE_REGION_MESSAGE_STRINGS, strings);

            Map<BigDecimal, T> stringsByLang = this.getStringsByKey(identifiable.getKey());
            if (stringsByLang == null) {
                stringsByLang = new HashMap<BigDecimal, T>();
            }
            stringsByLang.put(identifiable.getLanguageCode(), newIdentifiable);
            Cache.put(CACHE_REGION_MESSAGE_STRINGS, identifiable.getKey(), stringsByLang, false);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return newIdentifiable;
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        this.checkApplicable(identifiable);

        try {
            this.deleteObjectFromDB(DELETE_STRING, identifiable.getKey(), identifiable.getLanguageCode());

            Collection<T> strings = this.getAllObjects();
            strings.remove(identifiable);
            Cache.put(CACHE_REGION_MESSAGE_STRINGS, CACHE_REGION_MESSAGE_STRINGS, strings);

            Map<BigDecimal, T> stringsByLang = this.getStringsByKey(identifiable.getKey());
            if (stringsByLang != null) {
                stringsByLang.remove(identifiable.getLanguageCode());
                if (stringsByLang.isEmpty()) {
                    Cache.remove(CACHE_REGION_MESSAGE_STRINGS, identifiable.getKey());
                } else {
                    Cache.put(CACHE_REGION_MESSAGE_STRINGS, identifiable.getKey(), stringsByLang, false);
                }
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Проверяет применимость поступившего объекта
     *
     * @param identifiable проверяемый объект
     */
    private void checkApplicable(T identifiable) {
        if (identifiable == null) {
            throw new IneIllegalArgumentException("The cource object can't be null");
        }
    }

    private void checkInit() throws CoreException {
        if (Cache.get(CACHE_REGION_MESSAGE_STRINGS, INIT_MARK, Producer.NULL_PRODUCER) == null) {
            this.getAllObjects();
        }
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return MessageString.class;
    }

}
