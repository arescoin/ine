/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.*;
import ru.effts.ine.core.userpermits.UserPermit;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class UserPermitAccessImpl<T extends UserPermit>
        extends AbstractVersionableAccess<T> implements UserPermitAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "USER_PERMITS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_PERMIT_N = "permit_n";
    public static final String COLUMN_CRUD_MASK = "crud_mask";
    public static final String COLUMN_USER_N = "user_n";
    public static final String COLUMN_ROLE_N = "role_n";
    public static final String COLUMN_PERMIT_VAL = "permit_val";
    public static final String COLUMN_IS_ACTIVE = "is_active";

    /** Запрос на выборку всех доступов, назначенных системным пользователям */
    public static final String ALL_USER_PERMITS_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_PERMIT_N +
            ", " + TABLE_PREF + COLUMN_CRUD_MASK +
            ", " + TABLE_PREF + COLUMN_USER_N +
            ", " + TABLE_PREF + COLUMN_ROLE_N +
            ", " + TABLE_PREF + COLUMN_PERMIT_VAL +
            ", " + TABLE_PREF + COLUMN_IS_ACTIVE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.userpermits.UserPermit} */
    public static final String CACHE_REGION_USER_PERMITS = UserPermit.class.getName();

    private static final Map<String, BigDecimal> ID_COLUMNS_MAP = new HashMap<String, BigDecimal>(3);

    /** userId - roleId - permitId */
    private static final Map<BigDecimal, Map<BigDecimal, Map<BigDecimal, UserPermit>>> USR_PERM_MAP =
            new HashMap<BigDecimal, Map<BigDecimal, Map<BigDecimal, UserPermit>>>();

    private static final String DELIMETER = ",";


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(UserPermit.CORE_ID, COLUMN_PERMIT_N);
        map.put(UserPermit.CRUD_MASK, COLUMN_CRUD_MASK);
        map.put(UserPermit.USER_ID, COLUMN_USER_N);
        map.put(UserPermit.ROLE_ID, COLUMN_ROLE_N);
        map.put(UserPermit.VALUES, COLUMN_PERMIT_VAL);
        map.put(UserPermit.ACTIVE, COLUMN_IS_ACTIVE);
        setInstanceFieldsMapping(UserPermitAccessImpl.class, map);

        setIdColumns(UserPermitAccessImpl.class, new String[]{COLUMN_PERMIT_N, COLUMN_USER_N, COLUMN_ROLE_N});

        registerVersionableHelpers(UserPermitAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    public void addHelper(CRUDHelper<T> helper) {

        LinkedHashSet<CRUDHelper> helpers = new LinkedHashSet<CRUDHelper>();
        helpers.add(helper);

        registerVersionableHelpers(UserPermitAccessImpl.class, helpers);
    }

    private static BigDecimal getColumnId(String columnName) throws GenericSystemException {

        if (!ID_COLUMNS_MAP.containsKey(columnName)) {
            ID_COLUMNS_MAP.put(columnName, FieldAccess.getColumnId(TABLE, columnName));
        }

        return ID_COLUMNS_MAP.get(columnName);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_USER_PERMITS;
    }

    @Override
    protected void checkIdParam(BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(3,
                "Requiered: id[0] - userPermit.id, id[1] - userPermit.userId, id[2] - userPermit.roleId", id);
    }

    @Override
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {

        this.checkIdParam(id);
        identifiable.setCoreId(id[0]);
        identifiable.setUserId(id[1]);
        identifiable.setRoleId(id[2]);
    }

    @Override
    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {

        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(getColumnId(COLUMN_PERMIT_N), identifiable.getCoreId());
        syntheticId.addIdEntry(getColumnId(COLUMN_USER_N), identifiable.getUserId());
        syntheticId.addIdEntry(getColumnId(COLUMN_ROLE_N), identifiable.getRoleId());

        return syntheticId;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(UserPermit.class);

        this.setVersionableFields(resultSet, result, COLUMN_PERMIT_N);
        result.setUserId(this.getId(resultSet, COLUMN_USER_N));
        result.setRoleId(this.getId(resultSet, COLUMN_ROLE_N));
        result.setCrudMask(this.getBigDecimal(resultSet, COLUMN_CRUD_MASK).intValue());
        result.setValues(this.parseArrayString(this.getString(resultSet, COLUMN_PERMIT_VAL)));
        result.setActive(this.getBoolean(resultSet, COLUMN_IS_ACTIVE));

        return result;
    }

    private BigDecimal[] parseArrayString(String arrayString) {

        if (arrayString == null || arrayString.trim().isEmpty()) {
            return null;
        }

        String[] strings = arrayString.split(DELIMETER);
        BigDecimal[] result = new BigDecimal[strings.length];

        for (int i = 0; i < strings.length; i++) {
            try {
                result[i] = new BigDecimal(strings[i]);
            } catch (Exception e) {
                throw new IneIllegalArgumentException("Failed to parse part of input string: '" + arrayString + "'", e);
            }
        }

        return result;
    }

    @Override
    protected void setNonPrimitveType(PreparedStatement preparedStatement, int i, Object param) throws SQLException {

        if (param instanceof BigDecimal[]) {
            BigDecimal[] decimals = (BigDecimal[]) param;
            int length = decimals.length;
            String result = length > 0 ? "" : null;
            for (int j = 0; j < length; j++) {
                result += decimals[j];
                if (j < length - 1) {
                    result += DELIMETER;
                }
            }
            preparedStatement.setString(i, result);
        } else {
            super.setNonPrimitveType(preparedStatement, i, param);
        }
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {

        Collection<T> userPermits = getAllObjects(CACHE_REGION_USER_PERMITS, ALL_USER_PERMITS_SELECT);
        for (T userPermit : userPermits) {
            processUserPermit(userPermit);
        }

        return userPermits;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {

        T userPermit = null;

        Map<BigDecimal, Map<BigDecimal, UserPermit>> map = USR_PERM_MAP.get(id[1]);
        if (map != null) {
            if (map.get(id[2]) != null) {
                userPermit = (T) map.get(id[2]).get(id[0]);
            }
        }

        if (userPermit == null) {
            userPermit = getObjectById(CACHE_REGION_USER_PERMITS, ALL_USER_PERMITS_SELECT, id);
        }

        if (userPermit != null) {
            processUserPermit(userPermit);
        }

        return userPermit;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        T userPermit = this.createObject(identifiable, CACHE_REGION_USER_PERMITS, ALL_USER_PERMITS_SELECT, false);
        this.processUserPermit(userPermit);

        return userPermit;
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        T userPermit = this.updateObject(identifiable, newIdentifiable, CACHE_REGION_USER_PERMITS);
        this.processUserPermit(userPermit);

        return userPermit;
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        this.deleteObject(identifiable, CACHE_REGION_USER_PERMITS, ALL_USER_PERMITS_SELECT);

        if (USR_PERM_MAP.containsKey(identifiable.getUserId())) {
            if (USR_PERM_MAP.get(identifiable.getUserId()).containsKey(identifiable.getRoleId())) {
                USR_PERM_MAP.get(identifiable.getUserId()).
                        get(identifiable.getRoleId()).remove(identifiable.getCoreId());
            }
        }
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return UserPermit.class;
    }

    private void processUserPermit(T userPermit) {

        BigDecimal userId = userPermit.getUserId();
        if (!USR_PERM_MAP.containsKey(userId)) {
            HashMap<BigDecimal, Map<BigDecimal, UserPermit>> map =
                    new HashMap<BigDecimal, Map<BigDecimal, UserPermit>>();
            HashMap<BigDecimal, UserPermit> permitHashMap = new HashMap<BigDecimal, UserPermit>();
            map.put(userPermit.getRoleId(), permitHashMap);

            USR_PERM_MAP.put(userId, map);
        } else if (!USR_PERM_MAP.get(userId).containsKey(userPermit.getRoleId())) {
            USR_PERM_MAP.get(userId).put(userPermit.getRoleId(), new HashMap<BigDecimal, UserPermit>());
        }

        USR_PERM_MAP.get(userId).get(userPermit.getRoleId()).put(userPermit.getCoreId(), userPermit);
    }

    @SuppressWarnings({"unchecked", "RedundantCast"})
    public Map<BigDecimal, T> getPermitsByUserAndRoleId(BigDecimal userId, BigDecimal roleId) {

        Map<BigDecimal, T> resultMap = null;

        Map<BigDecimal, Map<BigDecimal, UserPermit>> map = USR_PERM_MAP.get(userId);
        if (map != null) {
            if (map.get(roleId) != null) {
                resultMap = (Map<BigDecimal, T>) map.get(roleId);
            }
        }

        return resultMap;
    }

}
