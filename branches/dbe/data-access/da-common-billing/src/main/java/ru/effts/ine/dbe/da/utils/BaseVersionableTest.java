/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.utils;

import org.junit.Assert;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.Versionable;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.core.userpermits.CrudCode;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.VersionableAccess;
import ru.effts.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.effts.ine.dbe.da.core.history.IdentifiableHistoryAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: BaseVersionableTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
public abstract class BaseVersionableTest extends BaseIdentifiableTest {


    protected Identifiable testModification(BigDecimal tableId, Identifiable identifiable,
                                            IdentifiableHistoryAccess<IdentifiableHistory> access) throws CoreException {

        Versionable oldVersion = (Versionable) identifiable;

        Collection<IdentifiableHistory> preHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(identifiable));
        identifiable = testUpdate(identifiable);

        if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_HISTORY).isEnabled()) {
            return identifiable;
        }

        Collection<IdentifiableHistory> nowHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(identifiable));

        checkHistory(preHstrs, nowHstrs, CrudCode.update, 1);

        VersionableAccess versionableAccess = (VersionableAccess)
                AccessFactory.getImplementation(AccessFactory.extractInterface(identifiable.getClass()));

        Collection<Versionable> versionables = versionableAccess.getOldVersions(oldVersion);

        boolean versionCorrect = false;
        for (Versionable history : versionables) {
            if (history.getCoreTd().equals(((Versionable) identifiable).getCoreFd())
                    && history.getCoreFd().equals(oldVersion.getCoreFd())) {
                versionCorrect = true;
                break;
            }
        }

        Assert.assertTrue("Version FD-TD incorrect " + oldVersion, versionCorrect);

        return identifiable;
    }

    protected void testDeletion(BigDecimal tableId, Identifiable identifiable,
                                IdentifiableHistoryAccess<IdentifiableHistory> access) throws CoreException {

        Versionable versionable = (Versionable) identifiable;

        Collection<IdentifiableHistory> preHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(versionable));
        testDelete(versionable);

        if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_HISTORY).isEnabled()) {
            return;
        }

        Collection<IdentifiableHistory> nowHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(versionable));

        checkHistory(preHstrs, nowHstrs, CrudCode.delete, 1);

        VersionableAccess versionableAccess = (VersionableAccess)
                AccessFactory.getImplementation(AccessFactory.extractInterface(versionable.getClass()));
        Collection<Versionable> versionables = versionableAccess.getOldVersions(versionable);

        boolean versionCorrect = false;
        for (Versionable history : versionables) {
            if (history.getCoreFd().equals((versionable).getCoreFd())
                    && history.getCoreTd().equals(versionable.getCoreTd())) {
                versionCorrect = true;
                break;
            }
        }

        Assert.assertTrue("Version FD-TD incorrect " + versionable, versionCorrect);
    }

}
