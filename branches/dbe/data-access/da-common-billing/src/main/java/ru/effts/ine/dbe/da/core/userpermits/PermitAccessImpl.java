/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.*;
import ru.effts.ine.core.userpermits.Permit;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PermitAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class PermitAccessImpl<T extends Permit> extends AbstractVersionableAccess<T> implements PermitAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "PERMITS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_PERMIT_NAME = "permit_name";
    public static final String COLUMN_VAL_MASK = "val_mask";
    public static final String COLUMN_IS_VAL_REQ = "is_val_req";

    /** Запрос на выборку всех описаний доступов */
    public static final String ALL_PERMITS_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_PERMIT_NAME +
            ", " + TABLE_PREF + COLUMN_VAL_MASK +
            ", " + TABLE_PREF + COLUMN_IS_VAL_REQ +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.userpermits.Permit} */
    public static final String CACHE_REGION_PERMITS = Permit.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(Permit.NAME, COLUMN_PERMIT_NAME);
        map.put(Permit.MASK_VALUE, COLUMN_VAL_MASK);
        map.put(Permit.VALUE_REQUIRED, COLUMN_IS_VAL_REQ);
        setInstanceFieldsMapping(PermitAccessImpl.class, map);

        registerVersionableHelpers(PermitAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_PERMITS;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(Permit.class);

        this.setVersionableFields(resultSet, result);
        result.setName(this.getString(resultSet, COLUMN_PERMIT_NAME));
        result.setMaskValue(this.getString(resultSet, COLUMN_VAL_MASK));
        result.setValueRequired(this.getBoolean(resultSet, COLUMN_IS_VAL_REQ));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(CACHE_REGION_PERMITS, ALL_PERMITS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(CACHE_REGION_PERMITS, ALL_PERMITS_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_PERMITS, ALL_PERMITS_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_PERMITS);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_PERMITS, ALL_PERMITS_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return Permit.class;
    }

    public void addHelper(CRUDHelper<T> helper) {

        LinkedHashSet<CRUDHelper> permitHelpers = new LinkedHashSet<CRUDHelper>();
        permitHelpers.add(helper);

        AbstractVersionableAccess.registerVersionableHelpers(PermitAccessImpl.class, permitHelpers);
    }

}
