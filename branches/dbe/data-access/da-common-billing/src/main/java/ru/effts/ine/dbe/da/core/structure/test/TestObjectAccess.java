/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure.test;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * Доступ к тестовым объектам из таблицы TEST_TABLE.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: TestObjectAccess.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public interface TestObjectAccess<T extends TestObject> extends VersionableAccess<T> {

    /**
     * Возвращает все актуальные объекты
     *
     * @return коллекция с {@link ru.effts.ine.core.structure.test.TestObject}, может быть пустой
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибке в процессе получения и обработки данных
     */
    Collection<T> getAllObjects() throws CoreException;
}
