/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.constants;

import ru.effts.ine.core.*;
import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.dbe.da.core.structure.PatternUtils;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantAccessImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ParametrizedConstantAccessImpl<T extends ParametrizedConstant>
        extends ConstantAccessImpl<T> implements ParametrizedConstantAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "CONSTANTS_P";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_PARAM_MASK = "param_mask";

    /** Запрос на выборку */
    public static final String CONSTANTS_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_CONST_NAME +
//            ", " + TABLE_PREF + COLUMN_TYP +
            ", " + TABLE_PREF + COLUMN_PARAM_MASK +
            ", " + TABLE_PREF + COLUMN_DEF_VAL +
            ", " + TABLE_PREF + COLUMN_IS_NULLABLE +
            ", " + TABLE_PREF + COLUMN_IS_MODYFABLE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            //  ограничиваем типом - 2, парамтризованные константы
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.constants.ParametrizedConstant} */
    public static final String CACHE_REGION_PARAMS_CONSTANTS = ParametrizedConstant.class.getName();

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(ParametrizedConstant.MASK, COLUMN_PARAM_MASK);
        setInstanceFieldsMapping(ParametrizedConstantAccessImpl.class, map);

        registerConstantHelpers(ParametrizedConstantAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"})
        T result = (T) IdentifiableFactory.getImplementation(ParametrizedConstant.class);

        this.setVersionableFields(resultSet, result);

        result.setName(this.getString(resultSet, COLUMN_CONST_NAME));
//        result.setType(this.getId(resultSet, COLUMN_TYP));
        result.setDefaultValue(this.getString(resultSet, COLUMN_DEF_VAL));
        result.setNullable(this.getBoolean(resultSet, COLUMN_IS_NULLABLE));
        result.setModifiable(this.getBoolean(resultSet, COLUMN_IS_MODYFABLE));

        String mask = this.getString(resultSet, COLUMN_PARAM_MASK);
        if (mask != null) {
            result.setMask(PatternUtils.checkPattern(new SystemObjectPattern(mask)));
        }

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_PARAMS_CONSTANTS, CONSTANTS_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_PARAMS_CONSTANTS, CONSTANTS_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_PARAMS_CONSTANTS, CONSTANTS_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_PARAMS_CONSTANTS);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_PARAMS_CONSTANTS, CONSTANTS_SELECT);
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_PARAMS_CONSTANTS;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ParametrizedConstant.class;
    }

    @Override
    public String getTableName() {
        return TABLE;
    }
}
