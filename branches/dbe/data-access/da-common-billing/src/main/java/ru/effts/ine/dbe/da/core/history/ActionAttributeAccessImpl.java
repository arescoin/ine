/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.history;

import ru.effts.ine.core.*;
import ru.effts.ine.core.history.ActionAttribute;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.dbe.da.core.AbstractIdentifiableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Предоставляет методы для работы с пользовательскими атрибутами изменений идентифицируемых объектов на стороне БД
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttributeAccessImpl.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public class ActionAttributeAccessImpl<T extends ActionAttribute> extends AbstractIdentifiableAccess<T>
        implements ActionAttributeAccess<T> {

    public static final String TABLE = "OBJ_HISTORY_ATTRS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Колонки таблицы */
    public static final String COLUMN_ENT_ID = "ent_id";
    public static final String COLUMN_ACTION_DATE = "action_date";
    public static final String COLUMN_ATTR_NAME = "attr_name";
    public static final String COLUMN_ATTR_VALUE = "attr_value";

    /** Запрос на выборку всех значений из таблицы */
    public static final String SELECT_ALL_ATTRIBUTES = "SELECT " + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", NULL AS " + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_ENT_ID +
            ", " + TABLE_PREF + COLUMN_ACTION_DATE +
            ", " + TABLE_PREF + COLUMN_ATTR_NAME +
            ", " + TABLE_PREF + COLUMN_ATTR_VALUE +
            " FROM " + TABLE;

    /** Запрос на выборку атрибутов конкретной записи об изменении объекта */
    public static final String SELECT_ATTRIBUTES_BY_HISTORY = SELECT_ALL_ATTRIBUTES +
            " WHERE " + TABLE_PREF + COLUMN_ID + " = ? AND " + TABLE_PREF + COLUMN_ENT_ID + " = ? AND " +
            TABLE_PREF + COLUMN_ACTION_DATE + " = ?";


    static {
        Map<String, String> map = new HashMap<String, String>();
        map.put(Identifiable.CORE_DSC, null);
        map.put(ActionAttribute.ENTITY_ID, COLUMN_ENT_ID);
        map.put(ActionAttribute.ACTION_DATE, COLUMN_ACTION_DATE);
        map.put(ActionAttribute.ATTRIBUTE_NAME, COLUMN_ATTR_NAME);
        map.put(ActionAttribute.ATTRIBUTE_VALUE, COLUMN_ATTR_VALUE);
        setInstanceFieldsMapping(ActionAttributeAccessImpl.class, map);

        registerIdentifiableHelpers(ActionAttributeAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return "";
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(ActionAttribute.class);
        this.setIdentifiableFields(resultSet, value);

        String entityId = this.getString(resultSet, COLUMN_ENT_ID);
        this.wasNull(COLUMN_ENT_ID, resultSet);

        Date actionDate = this.getDate(resultSet, COLUMN_ACTION_DATE);
        this.wasNull(COLUMN_ACTION_DATE, resultSet);

        String attributeName = this.getString(resultSet, COLUMN_ATTR_NAME);
        this.wasNull(COLUMN_ATTR_NAME, resultSet);

        value.setEntityId(SyntheticId.parseString(entityId));
        value.setActionDate(actionDate);
        value.setAttributeName(attributeName);
        value.setAttributeValue(this.getString(resultSet, COLUMN_ATTR_VALUE));

        return value;
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        throw new UnsupportedOperationException(
                "Method getObjectById does not supported for class " + getClass().getName());
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getObjects(false, SELECT_ALL_ATTRIBUTES);
    }

    @Override
    public Collection<T> getActionAttributes(IdentifiableHistory history) throws CoreException {
        AbstractIdentifiable.checkNull(history, "history");

        return this.getObjects(false, SELECT_ATTRIBUTES_BY_HISTORY,
                history.getCoreId(), history.getEntityId().toString(), history.getActionDate());
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ActionAttribute.class;
    }

    @Override
    public boolean containsObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return false;
    }

    @Override
    public boolean containsObjects(StorageFilter[] filters) throws GenericSystemException {
        return false;
    }

    @Override
    public Collection searchObjects(StorageFilter[] filters) throws GenericSystemException {
        return Collections.emptyList();
    }

    @Override
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return Collections.emptyList();
    }

    @Override
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {
        return Collections.emptyList();
    }

    @Override
    public int getObjectCount(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return 0;
    }

    @Override
    public int getObjectCount(StorageFilter[] filters) throws GenericSystemException {
        return 0;
    }
}
