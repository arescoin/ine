/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.config;

import ru.effts.ine.utils.config.AbstractPropertiesBasedConfig;
import ru.effts.ine.utils.config.ConfigurationException;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertiesBasedDAConfig.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class PropertiesBasedDAConfig extends AbstractPropertiesBasedConfig {

    /** Имя системного свойства, устанавливающего имя колонки-идентификатора в БД */
    protected static final String ROW_ID_PROPERTY = "ROW_ID";

    public static final String PROC_END_PROPERTY = "PROC_END";

    public static final String CASE_CONVERTER = "CASE_CONVERTER";

    public static final String DB_TYPE = "DB_TYPE";

    private String rowId;
    private String procEnd;
    private String UpperLower;
    private DBType dbType;


    @Override public void read() throws ConfigurationException {
        this.rowId = properties.getProperty(ROW_ID_PROPERTY, "ROW_ID");
        this.procEnd = properties.getProperty(PROC_END_PROPERTY, "");

        if (properties.getProperty(DB_TYPE, "oracle").equals("oracle")) {
            this.dbType = DBType.oracle;
        } else {
            this.dbType = DBType.postgre;
        }


        this.UpperLower = properties.getProperty(CASE_CONVERTER, "UPPER");
        if (!this.UpperLower.equals("UPPER") && !this.UpperLower.equals("LOWER")) {
            this.UpperLower = "UPPER";
        }
    }


    public String getRowId() {
        return rowId;
    }

    public String getProcEnd() {
        return procEnd;
    }

    public String getUpperLower() {
        return UpperLower;
    }

    public DBType getDbType() {
        return dbType;
    }


    public enum DBType {
        oracle, postgre
    }
}
