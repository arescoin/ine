/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure.test;

import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: TestObjectAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class TestObjectAccessImpl<T extends TestObject>
        extends AbstractVersionableAccess<T> implements TestObjectAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "TEST_TABLE";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_VALUE = "value";

    /** Запрос на выборку всех описаний связей */
    public static final String ALL_VALUES_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_VALUE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + "BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;


    private static final String CACHE_REGION_TEST_OBJECT = TestObject.class.getName();

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(TestObject.VALUE, COLUMN_VALUE);
        setInstanceFieldsMapping(TestObjectAccessImpl.class, map);

        registerVersionableHelpers(TestObjectAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_TEST_OBJECT;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(TestObject.class);

        this.setVersionableFields(resultSet, value);
        value.setValue(getString(resultSet, COLUMN_VALUE));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(CACHE_REGION_TEST_OBJECT, ALL_VALUES_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(CACHE_REGION_TEST_OBJECT, ALL_VALUES_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_TEST_OBJECT, ALL_VALUES_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_TEST_OBJECT);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_TEST_OBJECT, ALL_VALUES_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return TestObject.class;
    }

}
