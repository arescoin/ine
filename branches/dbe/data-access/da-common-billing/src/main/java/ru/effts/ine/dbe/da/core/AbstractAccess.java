/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core;

import ru.effts.ine.core.*;
import ru.effts.ine.core.ipaddress.IPAddress;
import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.dbe.da.PoolManager;
import ru.effts.ine.dbe.da.config.PropertiesBasedDAConfig;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.cache.Producer;
import ru.effts.ine.utils.config.ConfigurationManager;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Общий механизм построения тривиальных запросов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractAccess.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public abstract class AbstractAccess {

    /** Имя конфика Датаакцессов */
    public final static String DA_CONFIG_NAME = "DA_CONFIG";

    /** Суффикс для стандартных таблиц с историей */
    public final static String HISTORY = "_HSTR";

    /** Просто точка для разделителя */
    public static final String DOT = ".";

    /** Регион кеша для хранения системных FD, TD */
    public static final String FDTD_REGINON_NAME = AbstractAccess.class.getName() + "FDTD_REGINON_NAME";

    /** Имя системного свойства, устанавливающего метод комита в БД */
    protected static final String MANUAL_COMMIT_PROPERTY = "MANUAL_COMMIT";

    /** Системное свойство, устанавливающее метод комита в БД */
    protected static final boolean MANUAL_COMMIT =
            Boolean.parseBoolean(ConfigurationManager.getManager().getRootConfig().getValueByKey(
                    AbstractAccess.class.getName() + DOT + MANUAL_COMMIT_PROPERTY, Boolean.toString(false)));


    /** Имя системного свойства, устанавливающего имя колонки-идентификатора в БД */
    protected static final String ROW_ID_PROPERTY = "ROW_ID";

    protected static final PropertiesBasedDAConfig.DBType DB_TYPE = ((PropertiesBasedDAConfig)
            ConfigurationManager.getManager().getConfigFor(DA_CONFIG_NAME)).getDbType();

    /** Название псевдо-колонки с rowid */
    public final static String COLUMN_ROWID = ((PropertiesBasedDAConfig)
            ConfigurationManager.getManager().getConfigFor(DA_CONFIG_NAME)).getRowId();

    protected static final String PRC_END = ((PropertiesBasedDAConfig)
            ConfigurationManager.getManager().getConfigFor(DA_CONFIG_NAME)).getProcEnd();

    public static final String SYS_CUR_DATE_PRC = "INE_CORE_SYS.getCurrentDate" + PRC_END;

    private static Date systemFD;
    private static Date systemTD;

    /** Логгер */
    private static Logger logger = Logger.getLogger(AbstractAccess.class.getName());


    /**
     * Возвращает имя таблицы для работы с данными. Должен быть переопределен потомками
     *
     * @return имя таблицы
     */
    public abstract String getTableName();

    /**
     * Метод должен быть переопределен, должен конструировать инстанс объекта по результату выборки
     *
     * @param resultSet "строка" выборки
     *
     * @return сконструированный объект
     * @throws java.sql.SQLException при некорректной обработке данных из выбоки
     * @throws ru.effts.ine.core.CoreException
     *                               при обнаружении некорректных значений в полях
     * @throws ru.effts.ine.core.RuntimeCoreException
     *                               при возникновении системных ошибок
     */
    protected abstract Object constructObject(ResultSet resultSet)
            throws SQLException, CoreException, RuntimeCoreException;

    /**
     * Выбирает данные и формирует коллекцию с объектами
     *
     * @param unique    признак уникальности значения, если обнаруживается более одного, выбрасывается исключение
     * @param sqlSelect выполняемое SQL-выражение
     * @param params    значения подставляемые в критерии поиска
     *
     * @return объекты из базы
     * @throws ru.effts.ine.core.CoreException
     *          при обнаружении инвалидных данных в БД
     * @throws ru.effts.ine.core.RuntimeCoreException
     *          при возникновении системных ошибок
     */
    protected Collection<?> getObjects(boolean unique, String sqlSelect, Object... params)
            throws CoreException, RuntimeCoreException {

        Collection<Object> result = new ArrayList<Object>();

        if (logger.isLoggable(Level.FINE)) {
            logger.log(Level.FINE, "unique: [" + unique + "], sqlSelect [" + sqlSelect +
                    "], params[" + Arrays.toString(params) + "]");
        }

        try {
            Connection connection = PoolManager.getConnection();
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
                this.prepareParams(preparedStatement, params);
                ResultSet resultSet = preparedStatement.executeQuery();

                // блок призван обеспечить минимальные потери по времени при обработке данных из БД
                if (unique) {
                    // если мы дожны получить не более одного объекта
                    if (resultSet.next()) {

                        // если мы имеем запись то создадим объект и пойдем на следующую,
                        this.addObject(result, resultSet);

                        // если запись существует, то это полохо, бросаем исключение
                        if (resultSet.next()) {
                            throw new GenericSystemException("Too many objects... params: " + Arrays.toString(params));
                        }
                    }
                } else {
                    // если мы получаем 0 и более
                    while (resultSet.next()) {
                        this.addObject(result, resultSet);
                    }
                }
            } catch (SQLException e) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.log(Level.SEVERE, "SQLException: ", e);
                }
                this.toRollback(connection);
                throw new GenericSystemException(e.getMessage(), e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            if (logger.isLoggable(Level.SEVERE)) {
                logger.log(Level.SEVERE, "SQLException: ", e);
            }
            throw new GenericSystemException(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Формирует объект и размещает его в переданную колекцию
     *
     * @param result    колекция для хранения объектов
     * @param resultSet источник объектов
     *
     * @throws java.sql.SQLException при ошибке разбора результата запроса
     * @throws ru.effts.ine.core.CoreException
     *                               при ошибке формирования объекта
     * @throws ru.effts.ine.core.RuntimeCoreException
     *                               при прочих ошибках
     */
    private void addObject(Collection<Object> result, ResultSet resultSet)
            throws SQLException, CoreException, RuntimeCoreException {

        try {
            result.add(this.constructObject(resultSet));
        } catch (CoreException e) {
            String rowid = resultSet.getString(1);
            if (logger.isLoggable(Level.SEVERE)) {
                logger.log(Level.SEVERE, "Failed to load object with rowid[" + rowid + "], cause:", e);
            }
            throw e;
        } catch (RuntimeCoreException e) {
            String rowid = resultSet.getString(1);
            if (logger.isLoggable(Level.SEVERE)) {
                logger.log(Level.SEVERE, "Failed to load object with rowid[" + rowid + "], cause:", e);
            }
            throw e;
        }
    }

    /**
     * Сохраняет новый объект в БД
     *
     * @param createQuery SQL-запрос для создания записи
     * @param params      параметры для подстановки в запрос
     * @param names       названия столбцов, в которых генерируются значения при создании записи
     *
     * @return идентификаторы созданного объекта
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке работы с БД
     */
    protected BigDecimal[] createObjectInDB(String createQuery, Object[] params, String[] names)
            throws IneIllegalArgumentException, GenericSystemException {

        if (createQuery == null || createQuery.trim().length() < 1) {
            throw new IneIllegalArgumentException("createQuery can't be null or zero length");
        }

//        createQuery = createQuery.toLowerCase();

        try {
            Connection connection = PoolManager.getConnection();
            try {
                PreparedStatement preparedStatement = names == null
                        ? connection.prepareStatement(createQuery) : connection.prepareStatement(createQuery, names);

                this.prepareParams(preparedStatement, (Object[]) params);
                preparedStatement.execute();

                BigDecimal[] result = extractGeneratedKeys(preparedStatement, names);

                this.toCommit(connection);
                preparedStatement.close();

                return result;
            } catch (SQLException e) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.log(Level.SEVERE, "Exception at query execution [" + createQuery + "]");
                    logger.log(Level.SEVERE, "With arguments               [" + Arrays.toString(params) + "]");
                    logger.log(Level.SEVERE, "With names                   [" + Arrays.toString(names) + "]");
                    logger.log(Level.SEVERE, "SQLException: ", e);
                }
                this.toRollback(connection);
                throw new GenericSystemException(e.getMessage(), e);
            } finally {
                connection.close();
            }

        } catch (SQLException e) {
            if (logger.isLoggable(Level.SEVERE)) {
                logger.log(Level.SEVERE, "SQLException: ", e);
            }
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    private BigDecimal[] extractGeneratedKeys(PreparedStatement preparedStatement, String[] names)
            throws SQLException, GenericSystemException {

        if (names == null) {
            return null;
        }

        ResultSet resultSet = preparedStatement.getGeneratedKeys();
        resultSet.next();

        BigDecimal[] result = new BigDecimal[names.length];
        for (int i = 0; i < names.length; i++) {
            result[i] = resultSet.getBigDecimal(i + 1);
            if (resultSet.wasNull()) {
                throw new GenericSystemException(
                        "Wrong insert result, one of generated values not found: " + names[i]);
            }
        }

        if (resultSet.next()) {
            throw new GenericSystemException("Unexpected result length...");
        }

        resultSet.close();

        return result;
    }

    /**
     * Изменяет объект в БД
     *
     * @param updateQuery SQL-запрос для изменения записи
     * @param params      параметры для подстановки в запрос
     *
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке работы с БД
     */
    protected void updateObjectInDB(String updateQuery, Object[] params)
            throws IneIllegalArgumentException, GenericSystemException {

        if (updateQuery == null || updateQuery.trim().length() < 1) {
            throw new IneIllegalArgumentException("updateQuery can't be null or zero length");
        }

        try {
            Connection connection = PoolManager.getConnection();
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
                this.prepareParams(preparedStatement, (Object[]) params);
                preparedStatement.execute();
                this.toCommit(connection);
                preparedStatement.close();
            } catch (SQLException e) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.log(Level.FINE, "Exception at query execution [" + updateQuery + "]");
                    logger.log(Level.FINE, "With arguments               [" + Arrays.toString(params) + "]");
                    logger.log(Level.SEVERE, "SQLException: ", e);
                }
                this.toRollback(connection);
                throw new GenericSystemException(e.getMessage(), e);
            } finally {
                connection.close();
            }

        } catch (SQLException e) {
            if (logger.isLoggable(Level.SEVERE)) {
                logger.log(Level.SEVERE, "SQLException: ", e);
            }
            throw new GenericSystemException(e.getMessage(), e);
        }

    }

    /**
     * Удаляет существующий объект
     *
     * @param deleteQuery SQL-запрос для удаления записи в БД
     * @param id          параметры удаляемого объекта
     *
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.effts.ine.core.GenericSystemException
     *          ошибка при удалении из БД
     */
    protected void deleteObjectFromDB(String deleteQuery, Object... id)
            throws IneIllegalArgumentException, GenericSystemException {

        if (deleteQuery == null || deleteQuery.trim().isEmpty()) {
            throw new IneIllegalArgumentException("deleteQuery can't be null or zero length");
        }

        try {
            Connection connection = PoolManager.getConnection();
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
                this.prepareParams(preparedStatement, (Object[]) id);
                preparedStatement.execute();
                this.toCommit(connection);
                preparedStatement.close();
            } catch (SQLException e) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.log(Level.SEVERE, "SQLException at deletion: ID [" + Arrays.toString(id) +
                            "]; sql [" + deleteQuery + "]; message: " + e.getMessage(), e);
                }
                this.toRollback(connection);
                throw new GenericSystemException(e.getMessage(), e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            if (logger.isLoggable(Level.SEVERE)) {
                logger.log(Level.SEVERE, "SQLException: ", e);
            }
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    public Date getSystemFd() throws GenericSystemException {

        try {

            if (AbstractAccess.systemFD == null) {
                synchronized (DOT) {
                    if (AbstractAccess.systemFD == null) {
                        AbstractAccess.systemFD = (Date) Cache.get(FDTD_REGINON_NAME, "FD", new Producer() {
                            @Override
                            public Object get(Object key) throws CoreException {
                                return getSystemDate("INE_CORE_SYS.get_System_FromDate" + PRC_END);
                            }
                        });
                    }
                }
            }

        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return AbstractAccess.systemFD;
    }

    public Date getSystemTd() throws GenericSystemException {

        try {
            if (AbstractAccess.systemTD == null) {
                synchronized (DOT) {
                    if (AbstractAccess.systemTD == null) {
                        AbstractAccess.systemTD = (Date) Cache.get(FDTD_REGINON_NAME, "TD", new Producer() {
                            @Override
                            public Object get(Object key) throws CoreException {
                                return getSystemDate("INE_CORE_SYS.get_System_ToDate" + PRC_END);
                            }
                        });
                    }
                }
            }

        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return AbstractAccess.systemTD;
    }

    public Date getCurrentDate() throws GenericSystemException {
        return getSystemDate(SYS_CUR_DATE_PRC);
    }

    private Date getSystemDate(String call) throws GenericSystemException {

        Date result = null;
        try {
            Connection connection = PoolManager.getConnection();
            try {
                CallableStatement callableStatement = connection.prepareCall("{? = call " + call + "}");
                callableStatement.registerOutParameter(1, Types.TIMESTAMP);
                callableStatement.execute();

                Timestamp sqlTimestamp = callableStatement.getTimestamp(1);

                if (sqlTimestamp != null) {
                    result = new Date(sqlTimestamp.getTime());
                }
            } catch (SQLException e) {
                if (logger.isLoggable(Level.SEVERE)) {
                    logger.log(Level.SEVERE, "SQLException: ", e);
                }
                this.toRollback(connection);
                throw new GenericSystemException(e.getMessage(), e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            if (logger.isLoggable(Level.SEVERE)) {
                logger.log(Level.SEVERE, "SQLException: ", e);
            }
            throw new GenericSystemException(e.getMessage(), e);
        }

        return result;
    }

    /**
     * Применяет параметры к запросу
     *
     * @param preparedStatement сюда устанавливаем
     * @param params            параметры для переданного SQL-выражения (могут отсутствовать)
     *
     * @throws java.sql.SQLException при передаче некорректных праметров
     */
    void prepareParams(PreparedStatement preparedStatement, Object... params) throws SQLException {
        if (params != null && params.length > 0) {
            int i = 0;
            for (Object param : params) {
                i++;
                if (param != null) {
                    this.setNotNullParam(preparedStatement, i, param);
                } else {
                    this.setNullParam(preparedStatement, i);
                }
            }
        }
    }

    /**
     * Устанавливает нуловое значение в указанную позицыю
     *
     * @param preparedStatement приемник значения
     * @param i                 позиция
     *
     * @throws java.sql.SQLException по цепочке
     */
    private void setNullParam(PreparedStatement preparedStatement, int i) throws SQLException {
        preparedStatement.setObject(i, null);
    }

    /**
     * Устанавливает указанное значение в указанную позицию на приемнике
     *
     * @param preparedStatement приемник
     * @param i                 позиция
     * @param param             значение
     *
     * @throws java.sql.SQLException по цепочке
     */
    private void setNotNullParam(PreparedStatement preparedStatement, int i, Object param) throws SQLException {

        if (param instanceof Long) {
            preparedStatement.setLong(i, (Long) param);
        } else if (param instanceof Integer) {
            preparedStatement.setInt(i, (Integer) param);
        } else if (param instanceof BigDecimal) {
            preparedStatement.setBigDecimal(i, (BigDecimal) param);
        } else if (param instanceof String) {
            preparedStatement.setString(i, (String) param);
        } else if (param instanceof Boolean) {
            preparedStatement.setLong(i, ((Boolean) param) ? 1 : 0);
        } else if (param instanceof Date) {
            preparedStatement.setTimestamp(i, new Timestamp(((Date) param).getTime()));
        } else if (param instanceof DataType) {
            preparedStatement.setLong(i, ((DataType) param).ordinal() + 1);
        } else if (param instanceof SystemObjectPattern) {
            preparedStatement.setString(i, param.toString());
        } else if (param instanceof IPAddress) {
            preparedStatement.setBytes(i, ((IPAddress) param).asBytes());
        } else {
            this.setNonPrimitveType(preparedStatement, i, param);
        }
    }

    /**
     * Устанавливает значение непримитивного типа в приемнике.
     * <br> По-умолчанию выкидывает {@link ru.effts.ine.core.IneIllegalArgumentException},
     * для использования необходимо переопределить в потомках.
     *
     * @param preparedStatement приемник
     * @param i                 позиция
     * @param param             значение
     *
     * @throws java.sql.SQLException по цепочке
     */
    protected void setNonPrimitveType(PreparedStatement preparedStatement, int i, Object param) throws SQLException {

        if (param instanceof BigDecimal[]) {
            BigDecimal[] decimals = (BigDecimal[]) param;
            if (decimals.length > 0) {
                if (decimals[0] != null) {
                    preparedStatement.setBigDecimal(i, decimals[0]);
                } else {
                    preparedStatement.setObject(i, null);
                }
            }
        } else {
            throw new IneIllegalArgumentException("Unsupported param type: " + param.getClass().getName());
        }
    }

    /**
     * Предназначен для проверки допустимости комита и его выполнения в случае положительного результата
     *
     * @param connection соединение с бд, на котором хотим закомититься
     *
     * @throws java.sql.SQLException при возникновении ошибки в работе с БД
     */
    protected void toCommit(Connection connection) throws SQLException {
        if (MANUAL_COMMIT) {
            connection.commit();
        }
    }

    /**
     * Предназначен для проверки допустимости отката и его выполнения в случае положительного результата
     *
     * @param connection соединение с бд, на котором хотим откатиться
     *
     * @throws java.sql.SQLException при возникновении ошибки в работе с БД
     */
    protected void toRollback(Connection connection) throws SQLException {
        if (MANUAL_COMMIT) {
            connection.rollback();
        }
    }

    /**
     * Получает значение даты из указанной колонки
     *
     * @param resultSet  результат выборки
     * @param columnName наименование колонки
     *
     * @return дата
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    protected Date getDate(ResultSet resultSet, String columnName) throws SQLException {
        Timestamp ts = resultSet.getTimestamp(columnName);
        if (ts != null) {
            return new Date(ts.getTime());
        }
        return null;
    }

    /**
     * Получает значение Boolean из указанной колонки "NUMBER" типа (1 - true, 0 - false)
     *
     * @param resultSet  результат выборки
     * @param columnName наименование колонки
     *
     * @return логическое значение
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    protected boolean getBoolean(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getInt(columnName) == 1;
    }

    /**
     * Получает значение Boolean из указанной колонки "NUMBER" типа (1 - true, 0 - false, null - null)
     *
     * @param resultSet  результат выборки
     * @param columnName наименование колонки
     *
     * @return логическое значение. Может быть null
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    protected Boolean getBooleanNullable(ResultSet resultSet, String columnName) throws SQLException {
        int result = resultSet.getInt(columnName);
        return resultSet.wasNull() ? null : result == 1;
    }

    /**
     * Получает значение строки из указанной колонки
     *
     * @param resultSet  результат выборки
     * @param columnName наименование колонки
     *
     * @return строка
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    protected String getString(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getString(columnName);
    }

    /**
     * Получает числа строки из указанной колонки
     *
     * @param resultSet  результат выборки
     * @param columnName наименование колонки
     *
     * @return число
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     */
    protected BigDecimal getBigDecimal(ResultSet resultSet, String columnName) throws SQLException {
        return resultSet.getBigDecimal(columnName);
    }

    /**
     * Проверяет результат последнего обращения к строке данных на null.<br>
     * Если значение было null, то будет выкинуто исключение с указанием имени столбца и значения rowid.
     *
     * @param columnName столбец, обращение к которому проверяется на null
     * @param resultSet  строка, из которой получено проверяемое значение.
     *
     * @throws ru.effts.ine.core.IneCorruptedStateException
     *                               в случае, если значение null
     * @throws java.sql.SQLException в случае ошибок при работе с БД
     */
    protected void wasNull(String columnName, ResultSet resultSet) throws IneCorruptedStateException, SQLException {
        if (resultSet.wasNull()) {
            throw new IneCorruptedStateException("Null value found in column [" + columnName + "], rowId["
                    + resultSet.getString(1) + "]");
        }
    }

    protected Class getSubjectClass() throws GenericSystemException {
        return Object.class;
    }


}
