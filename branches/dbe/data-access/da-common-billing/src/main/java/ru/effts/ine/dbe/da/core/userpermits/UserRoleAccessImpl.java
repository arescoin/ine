/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.*;
import ru.effts.ine.core.userpermits.UserRole;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.utils.cache.Producer;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class UserRoleAccessImpl<T extends UserRole> extends AbstractVersionableAccess<T> implements UserRoleAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "USER_ROLES";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ROLE_N = "role_n";
    public static final String COLUMN_USER_N = "user_n";
    public static final String COLUMN_IS_ACTIVE = "is_active";

    /** Запрос на выборку всех ролей, назначенных системным пользователям */
    public static final String ALL_USER_ROLES_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ROLE_N +
            ", " + TABLE_PREF + COLUMN_USER_N +
            ", " + TABLE_PREF + COLUMN_IS_ACTIVE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link UserRole} */
    public static final String CACHE_REGION_USER_ROLES = UserRole.class.getName();

    private static final Map<String, BigDecimal> ID_COLUMNS_MAP = new HashMap<String, BigDecimal>(3);

    /** userId - coreId (roleId) */
    private static final Map<BigDecimal, HashMap<BigDecimal, UserRole>> ROLE_MAP =
            new HashMap<BigDecimal, HashMap<BigDecimal, UserRole>>();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(UserRole.CORE_ID, COLUMN_ROLE_N);
        map.put(UserRole.USER_ID, COLUMN_USER_N);
        map.put(UserRole.ACTIVE, COLUMN_IS_ACTIVE);
        setInstanceFieldsMapping(UserRoleAccessImpl.class, map);

        setIdColumns(UserRoleAccessImpl.class, new String[]{COLUMN_ROLE_N, COLUMN_USER_N});

        AbstractVersionableAccess.registerVersionableHelpers(UserRoleAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    public void addHelper(CRUDHelper<T> helper) {

        LinkedHashSet<CRUDHelper> helpers = new LinkedHashSet<CRUDHelper>();
        helpers.add(helper);

        AbstractVersionableAccess.registerVersionableHelpers(UserRoleAccessImpl.class, helpers);
    }

    private static BigDecimal getColumnId(String columnName) throws GenericSystemException {

        if (!ID_COLUMNS_MAP.containsKey(columnName)) {
            ID_COLUMNS_MAP.put(columnName, FieldAccess.getColumnId(TABLE, columnName));
        }

        return ID_COLUMNS_MAP.get(columnName);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_USER_ROLES;
    }

    @Override
    protected void checkIdParam(BigDecimal... id) throws IneIllegalArgumentException {
        super.checkIdParam(2, "id[0] - userRole.id, id[1] - userRole.userId", id);
    }

    @Override
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {

        this.checkIdParam(id);
        identifiable.setCoreId(id[0]);
        identifiable.setUserId(id[1]);
    }

    @Override
    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {

        SyntheticId syntheticId = new SyntheticId();
        syntheticId.addIdEntry(getColumnId(COLUMN_ROLE_N), identifiable.getCoreId());
        syntheticId.addIdEntry(getColumnId(COLUMN_USER_N), identifiable.getUserId());

        return syntheticId;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(UserRole.class);

        this.setVersionableFields(resultSet, result, COLUMN_ROLE_N);
        result.setUserId(this.getId(resultSet, COLUMN_USER_N));
        result.setActive(this.getBoolean(resultSet, COLUMN_IS_ACTIVE));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return getAllObjects(CACHE_REGION_USER_ROLES, ALL_USER_ROLES_SELECT);
    }

    private void processRole(T role) {

        BigDecimal userId = role.getUserId();
        if (!ROLE_MAP.containsKey(userId)) {
            ROLE_MAP.put(userId, new HashMap<BigDecimal, UserRole>());
        }

        ROLE_MAP.get(userId).put(role.getCoreId(), role);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return getObjectById(CACHE_REGION_USER_ROLES, ALL_USER_ROLES_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        T t = this.createObject(identifiable, CACHE_REGION_USER_ROLES, ALL_USER_ROLES_SELECT, false);
        processRole(t);

        return t;
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        T t = this.updateObject(identifiable, newIdentifiable, CACHE_REGION_USER_ROLES);
        processRole(t);

        return t;
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        this.deleteObject(identifiable, CACHE_REGION_USER_ROLES, ALL_USER_ROLES_SELECT);
        ROLE_MAP.get(identifiable.getUserId()).remove(identifiable.getCoreId());
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return UserRole.class;
    }


    @Override
    @SuppressWarnings({"unchecked"})
    public Collection<T> getRolesByUserId(BigDecimal userId) throws CoreException {

        synchronized (ROLE_MAP) {
            if (ROLE_MAP.isEmpty()) {
                getAllObjects();
            }
        }

        return Collections.unmodifiableCollection((Collection<T>) ROLE_MAP.get(userId).values());
    }

    @Override
    protected Collection<T> getAllObjects(String region, Producer producer) throws CoreException {

        Collection<T> roles = super.getAllObjects(region, producer);

        if (ROLE_MAP.isEmpty()) {
            synchronized (ROLE_MAP) {
                if (ROLE_MAP.isEmpty()) {
                    for (T role : roles) {
                        this.processRole(role);
                    }
                }
            }
        }

        return roles;
    }

}
