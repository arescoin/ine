/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.userpermits.*;
import ru.effts.ine.dbe.da.core.AbstractCRUDHelper;
import ru.effts.ine.dbe.da.core.AccessFactory;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Предоставляет метод для получения агрегированного состояния доступов пользователя.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PermitUtils.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class PermitUtils {

    /** userId - roleId - permitId */
    private static final HashMap<BigDecimal, HashMap<BigDecimal, HashMap<BigDecimal, PermitView>>> MAP =
            new HashMap<BigDecimal, HashMap<BigDecimal, HashMap<BigDecimal, PermitView>>>();


    static {
        try {
            init();
        } catch (GenericSystemException e) {
            // не должно такого быть!!!
            e.printStackTrace();
        }
    }


    @SuppressWarnings({"unchecked"})
    private static void init() throws GenericSystemException {
        ((PermitAccess) AccessFactory.getImplementation(Permit.class)).addHelper(new ClearCRUDHelper());
//        PermitAccess.addHelper(new ClearCRUDHelper());

        ((RoleAccess) AccessFactory.getImplementation(Role.class)).addHelper(new ClearCRUDHelper());
//        RoleAccessImpl.addHelper(new ClearCRUDHelper());

        ((RolePermitAccess) AccessFactory.getImplementation(RolePermit.class)).addHelper(new ClearCRUDHelper());
//        RolePermitAccessImpl.addHelper(new ClearCRUDHelper());

        ((UserPermitAccess) AccessFactory.getImplementation(UserPermit.class)).addHelper(new ClearUserCRUDHelper());
//        UserPermitAccessImpl.addHelper(new ClearUserCRUDHelper());

        ((UserRoleAccess) AccessFactory.getImplementation(UserRole.class)).addHelper(new ClearUserCRUDHelper());
//        UserRoleAccessImpl.addHelper(new ClearUserCRUDHelper());
    }

    private PermitUtils() {
    }

    /**
     * метод для получения агрегированного состояния доступов пользователя
     *
     * @param userId идентификатор пользователя
     * @param roleId идентификатор роли
     * @return карта доступов
     * @throws ru.effts.ine.core.GenericSystemException
     *          если скрипнуло в ядре...
     */
    public static Map<BigDecimal, PermitView> getPermitView(BigDecimal userId, BigDecimal roleId)
            throws GenericSystemException {

        Map<BigDecimal, PermitView> result = null;

        if (!MAP.containsKey(userId)) {
            MAP.put(userId, new HashMap<BigDecimal, HashMap<BigDecimal, PermitView>>());
        }
        if (!MAP.get(userId).containsKey(roleId)) {
            MAP.get(userId).put(roleId, new HashMap<BigDecimal, PermitView>());
        }

        Map<BigDecimal, PermitView> viewMap;
        if (MAP.get(userId).get(roleId).isEmpty()) {
            viewMap = obtainPermitMapping(userId, roleId);
        } else {
            viewMap = MAP.get(userId).get(roleId);
        }

        if (viewMap != null) {
            result = Collections.unmodifiableMap(viewMap);
        }

        return result;
    }

    @SuppressWarnings({"unchecked"})
    private static Map<BigDecimal, PermitView> obtainPermitMapping(BigDecimal userId, BigDecimal roleId)
            throws GenericSystemException {

        Map<BigDecimal, PermitView> viewMap = MAP.get(userId).get(roleId);

        UserPermitAccess<UserPermit> userPermitAccess =
                (UserPermitAccess<UserPermit>) AccessFactory.getImplementation(UserPermit.class);
        RolePermitAccess<RolePermit> rolePermitAccess =
                (RolePermitAccess<RolePermit>) AccessFactory.getImplementation(RolePermit.class);

        try {

            Map<BigDecimal, RolePermit> rolePermitMap =
                    new HashMap<BigDecimal, RolePermit>(rolePermitAccess.getPremitsByRole(roleId));
            Map<BigDecimal, UserPermit> userPermits = userPermitAccess.getPermitsByUserAndRoleId(userId, roleId);

            if (userPermits != null) {
                for (UserPermit userPermit : userPermits.values()) {
                    rolePermitMap.remove(userPermit.getCoreId());
                    if (userPermit.isActive()) {
                        viewMap.put(userPermit.getCoreId(),
                                new PermitView(userId, userPermit.getCoreId(),
                                        roleId, userPermit.getValues(), true, userPermit.getCrudMask()));
                    }
                }
            }

            for (RolePermit rolePermit : rolePermitMap.values()) {
                viewMap.put(rolePermit.getPermitId(),
                        new PermitView(userId, rolePermit.getPermitId(), rolePermit.getCoreId(),
                                rolePermit.getPermitValues(), true, rolePermit.getCrudMask()));
            }


        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return viewMap;
    }

    private static void clear() {
        MAP.clear();
    }


    private static void clearUser(BigDecimal userId) {
        if (MAP.containsKey(userId)) {
            MAP.get(userId).clear();
        }
    }


    private static final class ClearUserCRUDHelper extends AbstractCRUDHelper {

        private void clear(Identifiable identifiable) {
            if (identifiable instanceof UserPermit) {
                clearUser(((UserPermit) identifiable).getUserId());
            } else if (identifiable instanceof UserRole) {
                clearUser(((UserRole) identifiable).getUserId());
            }
        }

        @Override
        public void afterCreate(Identifiable identifiable) throws GenericSystemException {
            clear(identifiable);
        }

        @Override
        public void afterUpdate(Identifiable identifiable, Identifiable newIdentifiable) throws GenericSystemException {
            clear(identifiable);

        }

        @Override
        public void afterDelete(Identifiable identifiable) throws GenericSystemException {
            clear(identifiable);
        }
    }


    private static final class ClearCRUDHelper<T extends Identifiable> extends AbstractCRUDHelper<T> {
        @Override
        public void afterCreate(T identifiable) throws GenericSystemException {
            clear();
        }

        @Override
        public void afterUpdate(T identifiable, T newIdentifiable) throws GenericSystemException {
            clear();
        }

        @Override
        public void afterDelete(T identifiable) throws GenericSystemException {
            clear();
        }
    }

}
