/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.links;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Утилитный класс для работы с линками и их описаниями
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: LinkUtils.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class LinkUtils {

    /**
     * Удаляет все линки объекта
     *
     * @param identifiable объект линки которого требуется удалить
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке удаления линка
     */
    public static void deleteLinks(final Identifiable identifiable) throws GenericSystemException {

        //получение DA для доступа к объектам LinkData
        @SuppressWarnings({"unchecked"})
        LinkDataAccess<LinkData> lda = (LinkDataAccess<LinkData>) AccessFactory.getImplementation(LinkData.class);

        Collection<LinkData> links = lda.getLinkDataByObject(identifiable);
        for (LinkData link : links) {
            lda.deleteObject(link);
        }
    }

    /**
     * Возвращает Id линка
     *
     * @param leftType  класс первого объекта линка
     * @param rightType класс второго объекта линка
     * @param type      тип линка, характеризующий взаимосвязь объектов
     * @return BigDecimal идентификатор линка или null если связь не найдена
     * @throws GenericSystemException при ошибках в данных объекта или в мета-информации
     */
    public static BigDecimal getLinkId(Class leftType, Class rightType, long type) throws GenericSystemException {

        BigDecimal rightId = FieldAccess.getTableId(rightType);
        //получение DA для доступа к объектам Link
        @SuppressWarnings({"unchecked"})
        LinkAccess<Link> la = (LinkAccess<Link>) AccessFactory.getImplementation(Link.class);

        //поиск описания линка с заданными типом, левой и правыми классами объектов
        for (Link link : la.getLinksByObjType(leftType)) {
            if (link.getRightType().getTableId().equals(rightId) && link.getTypeId().longValue() == type) {
                return link.getCoreId();
            }
        }

        //не нашли требуемое описание линка
        return null;
    }

}
