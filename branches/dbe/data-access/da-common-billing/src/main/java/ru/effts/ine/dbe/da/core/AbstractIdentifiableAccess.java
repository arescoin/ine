/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core;

import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.core.userpermits.CrudCode;
import ru.effts.ine.core.userpermits.PermitView;
import ru.effts.ine.core.userpermits.SystemUser;
import ru.effts.ine.dbe.da.PoolManager;
import ru.effts.ine.dbe.da.config.PropertiesBasedDAConfig;
import ru.effts.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.effts.ine.dbe.da.core.structure.CustomAttributeAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.core.userpermits.PermitUtils;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.cache.Producer;
import ru.effts.ine.utils.cache.StorageFilterImpl;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Заготовка для общих операций с идентифицируемыми объектами
 * <p/>
 * При расширении данного класса необходимо указать соответствие полей интерфейса-наследника
 * {@link ru.effts.ine.core.Identifiable} столбцам таблицы, содержащей значения.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AbstractIdentifiableAccess.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public abstract class AbstractIdentifiableAccess<T extends Identifiable> extends AbstractAccess
        implements IdentifiableAccess<T> {

    /** Название колонки с идентификатором */
    public final static String COLUMN_ID = "n";

    /** Название колонки с пояснением */
    public final static String COLUMN_DSC = "dsc";

    private static final HashMap<Class, SearchCriteriaProfile> PROFILE_MAPPING = new HashMap<>();

    /**
     * Соответствие полей интерфейса-наследника {@link ru.effts.ine.core.Identifiable} столбцам таблицы,
     * содержащей значения
     */
    private static final Map<Class, Map<String, String>> INSTANCE_FIELDS_MAP = new HashMap<>();

    /**
     * системный номер столбца, в котором содержится идентификатор. нужен для облегчения операции получения составного
     * идентификатора {@link #getSyntheticId(java.math.BigDecimal...)}.
     */
    private BigDecimal ID_COLUMN_SYS_ID;

    private static final Map<Class, String[]> ID_FIELDS_MAP = new HashMap<>();

    /** Карта для хранения методов модификации системных объектов */
    private static final Map<Class, Map<String, Method>> METHOD_MAPPING = new HashMap<>();

    /** колонки по классам */
    private final static Map<Class, Collection<String>> columnsMap = new HashMap<>();
    /** колонки с Id по классам */
    private final static Map<Class, Collection<String>> columnsWthIdMap = new HashMap<>();

    private final IdentifiableGetAccess<T> iGetAccess = new IdentifiableGetAccess<>(this);
    private final IdentifiableCreateAccess<T> iCreateAccess = new IdentifiableCreateAccess<>(this);
    private final IdentifiableUpdateAccess<T> iUpdateAccess = new IdentifiableUpdateAccess<>(this);

    /** Список классов, осуществляющих проверку данных перед модификацией */
    private final static Map<Class<? extends AbstractIdentifiableAccess>, Set<CRUDHelper>> HELPERS_MAP =
            new HashMap<>();

    private static final Logger logger = Logger.getLogger(AbstractIdentifiableAccess.class.getName());

    private static final Object LOCK = new Object();

    /** Заполнение мапингов */
    static {
        Map<String, String> map = new HashMap<>();
        map.put(Identifiable.CORE_ID, COLUMN_ID);
        map.put(Identifiable.CORE_DSC, COLUMN_DSC);
        INSTANCE_FIELDS_MAP.put(AbstractIdentifiableAccess.class, map);

        Set<CRUDHelper> baseHelpers = new LinkedHashSet<>(2);
        baseHelpers.add(new PatternHelper());
        HELPERS_MAP.put(AbstractIdentifiableAccess.class, baseHelpers);
    }

    /**
     * Сохраняет маппинг полей объекта InE на столбцы его таблицы. Маппинг сохраняется для каждого конкретного
     * дата-акцесса. Для корректной работы метод надо расширять в каждом классе иерархии дата-акцессов,
     * кроме конечных потребителей. См. {@link ru.effts.ine.dbe.da.core.AbstractVersionableAccess} и далее по иерархии.
     *
     * @param aClass         класс дата-акцесса
     * @param instanceFields маппинг полей
     */
    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {

        Map<String, String> map = new HashMap<>();
        map.putAll(INSTANCE_FIELDS_MAP.get(AbstractIdentifiableAccess.class));
        map.putAll(instanceFields);

        if (map.containsValue(null)) {
            Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, String> entry = iterator.next();
                if (entry.getValue() == null) {
                    iterator.remove();
                }
            }
        }
        INSTANCE_FIELDS_MAP.put(aClass, map);

        ID_FIELDS_MAP.put(aClass, new String[]{COLUMN_ID});
    }

    public Map<String, String> getInstanceFields() {
        return Collections.unmodifiableMap(INSTANCE_FIELDS_MAP.get(this.getClass()));
    }

    /**
     * Регистрирует цепочку CRUD-helper'ов. Цепочка сохраняется для каждого конкретного дата-акцесса. Для корректной
     * работы метод надо расширять в каждом классе иерархии дата-акцессов, кроме конечных потребителей. См.
     * {@link ru.effts.ine.dbe.da.core.AbstractVersionableAccess} и далее по иерархии.
     *
     * @param access  дата-акцесс
     * @param helpers цепочка хелперов
     */
    protected static void registerIdentifiableHelpers(Class<? extends AbstractIdentifiableAccess> access,
                                                      Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<>();
        helpersAll.addAll(helpers);
        helpersAll.addAll(HELPERS_MAP.get(AbstractIdentifiableAccess.class));

        if (HELPERS_MAP.containsKey(access)) {
            HELPERS_MAP.get(access).addAll(helpersAll);
        } else {
            HELPERS_MAP.put(access, helpersAll);
        }
    }

    protected abstract T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException;

    protected Class getSubjectClass() throws GenericSystemException {
        return Identifiable.class;
    }

    protected String getCommonSelect() {
        return "";
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        throw new UnsupportedOperationException("This method is not supported for [" + this.getClass().getName() + "]");
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        throw new UnsupportedOperationException("This method is not supported for [" + this.getClass().getName() + "]");
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        throw new UnsupportedOperationException("This method is not supported for [" + this.getClass().getName() + "]");
    }

    /* *********************
     * Получение объекта
     * *********************/

    @SuppressWarnings({"unchecked"})
    @Override
    protected Collection<T> getObjects(boolean unique, String sqlSelect, Object... params)
            throws CoreException, RuntimeCoreException {
        return (Collection<T>) super.getObjects(unique, sqlSelect, params);
    }

    /**
     * Возвращает актуальные объекты используя стандартный механизм доступа к данным
     *
     * @param region регион кеша
     * @param sql    запрос к БД
     *
     * @return актуальные объекты
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибке в процессе получения и обработки данных
     */
    protected Collection<T> getAllObjects(String region, String sql) throws CoreException {
        return this.getAllObjects(region, this.getAllProducer(region, sql));
    }

    @SuppressWarnings({"unchecked"})
    protected Collection<T> getAllObjects(String region, Producer producer) throws CoreException {
        return new ArrayList<T>(Cache.getRegionContent(region, producer).values());
    }

    protected T getObjectById(String region, String sql, BigDecimal... id) throws CoreException {
        return iGetAccess.getObjectById(region, this.getAllProducer(region, sql), this.getSyntheticId(id));
    }

    protected T getObjectById(String region, Producer producer, BigDecimal... id) throws CoreException {
        return iGetAccess.getObjectById(region, producer, this.getSyntheticId(id));
    }

    /* **********************
     * Поиск объектов
     * **********************/

    @Override
    public SearchCriteriaProfile getSearchCriteriaProfile() throws GenericSystemException {

        if (!PROFILE_MAPPING.containsKey(this.getClass())) {
            synchronized (PROFILE_MAPPING) {
                if (!PROFILE_MAPPING.containsKey(this.getClass())) {
                    PROFILE_MAPPING.put(this.getClass(), iGetAccess.getSearchCriteriaProfile());
                }
            }
        }

        return PROFILE_MAPPING.get(this.getClass());
    }

    @SuppressWarnings({"UnusedParameters"})
    protected Object getRegionKey(SearchCriterion criterion) {
        return this.getMainRegionKey();
    }

    @Override
    public StorageFilter createStorageFilter(
            Set<SearchCriterion> criteria, SearchCriterion criterion, String fieldName) {

        AbstractIdentifiable.checkNull(criteria, "criteria");

        return new StorageFilterImpl(criteria, criterion, fieldName,
                this.getRegionKey(criteria.isEmpty() ? null : criteria.iterator().next()));
    }

    @Override
    public StorageFilter createStorageFilter(Set<SearchCriterion> criteria, String fieldName) {
        return this.createStorageFilter(criteria, null, fieldName);
    }

    @SuppressWarnings({"unchecked"})
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return this.searchObjects(searchCriteria, null);
    }

    public Collection searchObjects(Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {
        this.checkCache(this, null);
        String regionKey = this.getMainRegionKey();
        return Cache.search(regionKey, new StorageFilterImpl(searchCriteria, fieldName, regionKey));
    }

    public Collection<T> searchObjects2(Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {

//        this.checkCache(this, null);
//        String regionKey = this.getMainRegionKey();
        String sql = "select * from " + this.getTableName();

        ArrayList params = new ArrayList();

        boolean firstTime = true;
        for (SearchCriterion searchCriterion : searchCriteria) {

            if (firstTime) {
                sql = sql + " where ";
                firstTime = false;
            } else {
                sql += " and ";
            }

            sql += new DataRuleMeasurer().measure(searchCriterion, params);
        }

        logger.fine(sql);

        try {
            return getObjects(false, sql, params.toArray());
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    public Collection searchObjects(StorageFilter[] filters) throws GenericSystemException {
        filters = this.recreateFilters(filters);
        checkCache(filters, null);
        return Cache.search(Arrays.asList(filters));
    }

    @Override
    public boolean containsObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        this.checkCache(this, null);
        String regionKey = this.getMainRegionKey();
        return Cache.containsObjects(regionKey, new StorageFilterImpl(searchCriteria, regionKey));
    }

    @Override
    public boolean containsObjects(StorageFilter[] filters) throws GenericSystemException {
        filters = this.recreateFilters(filters);
        this.checkCache(filters, null);
        return Cache.containsObjects(Arrays.asList(filters));
    }

    @Override
    public int getObjectCount(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        this.checkCache(this, null);
        String regionKey = this.getMainRegionKey();
        return Cache.getObjectCount(regionKey, new StorageFilterImpl(searchCriteria, regionKey));
    }

    @Override
    public int getObjectCount(StorageFilter[] filters) throws GenericSystemException {
        filters = this.recreateFilters(filters);
        this.checkCache(filters, null);
        return Cache.getObjectCount(Arrays.asList(filters));
    }

    @SuppressWarnings({"unchecked"})
    private StorageFilter[] recreateFilters(StorageFilter[] filters) throws GenericSystemException {
        StorageFilterImpl filter;
        AbstractIdentifiableAccess access;
        for (int i = 0; i < filters.length; i++) {
            filter = (StorageFilterImpl) filters[i];
            try {
                access = (AbstractIdentifiableAccess) AccessFactory.getImplementation(
                        Class.forName((String) filter.getRegionKey()));
                filters[i] = new StorageFilterImpl(filter, access.getRegionKey(filter.getFirstSearchCriterion()));
            } catch (ClassNotFoundException e) {
                //ничего не делаем, т.к. ключ уже модифицирован
            }
        }
        return filters;
    }

    /**
     * Проверяем заполнение кеша данными на переданном акцессе.
     * Если данных нет, то вызываем на нём метод getAllObjects().
     *
     * @param access акцесс, на котором проверяем кеш
     * @param param  дополнительный параметр для проверки. В данном методе не используется.
     *
     * @throws ru.effts.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешем
     */
    protected void checkCache(IdentifiableAccess access, Object param) throws GenericSystemException {
        try {
            if (!Cache.containsRegion(access.getMainRegionKey())) {
                access.getAllObjects();
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Проверяем заполнение кеша данными для переданных поисковых фильтров.
     * Проходим по массиву фильтров, получаем для каждого фильтра соответствующий акцесс и проверяем:
     * если данных нет, то вызываем на акцессе метод getAllObjects().
     *
     * @param filters список фильтров
     * @param param   дополнительный параметр для проверки. В данном методе не используется.
     *
     * @throws ru.effts.ine.core.GenericSystemException
     *          в случае ошибок при работе с кешем
     */
    protected void checkCache(StorageFilter[] filters, Object param) throws GenericSystemException {
        for (StorageFilter filter : filters) {
            SearchCriterion criterion = ((StorageFilterImpl) filter).getFirstSearchCriterion();
            if (criterion == null) {
                throw new IneIllegalArgumentException("SearchCriterion can't be null");
            }
            IdentifiableAccess access = (IdentifiableAccess)
                    AccessFactory.getImplementation(IdentifiableFactory.getInterface(criterion.getSearchClass()));
            this.checkCache(access, param);
        }
    }

    /* ******************
     * Создание объекта
     * ******************/

    /**
     * Оболочка для вызова при создании нового объекта.
     * <p/>
     * <p/>
     * Последовательность работы метода:
     * <ul>
     * <li>Проводит проверку сохраняемого объекта, вызовом метода
     * {@link #checkApplicable(ru.effts.ine.core.Identifiable)}</li>
     * <li>Блокирует по переданному идентификатору регион кеша</li>
     * <li>Создает объект</li>
     * <li>Обновляет коллекуию с идентификаторами</li>
     * <li>Размещает объект и коллекцию в кеше</li>
     * <li>Снимает блокировку на кеше</li>
     * </ul>
     * <br>
     * <b>Внимание! </b>Метод допустимо использовать толко для объектов использующих унифицированную
     * структуру хранения данных в кеше.<br>
     * В общем виде она выглядит как - <i>Один регион для типа, в котором под ключем имени региона содержится коллекция
     * идентификаторов, далее следуют объекты по своим идентификаторам</i>.
     *
     * @param identifiable   новый объект
     * @param allProducer    продюссер для конкретного типа, должен возвращать коллекцию идентификаторов
     *                       актуальных версий и заполнять кеш данными по самим объектам
     * @param region         имя региона, хранящего данный объект
     * @param autogenerateId признак использования стандартного механизма генерации идентификатора
     *
     * @return идентификатор созданного объекта
     * @throws GenericSystemException      в случае возникновения ошибок при доступе к мета-информаци
     * @throws IneIllegalArgumentException при попытке передать null или некорректный объект в контексте операции
     */
    protected T createObject(T identifiable, Producer allProducer, String region, boolean autogenerateId)
            throws IneIllegalArgumentException, GenericSystemException {
        return iCreateAccess.createObject(identifiable, allProducer, region, autogenerateId);
    }

    /**
     * Оболочка для вызова при создании нового объекта.
     * <p/>
     * <p/>
     * Последовательность работы метода:
     * <ul>
     * <li>Проводит проверку сохраняемого объекта, вызовом метода
     * {@link #checkApplicable(ru.effts.ine.core.Identifiable)}</li>
     * <li>Блокирует по переданному идентификатору регион кеша</li>
     * <li>Создает объект</li>
     * <li>Обновляет коллекуию с идентификаторами</li>
     * <li>Размещает объект и коллекцию в кеше</li>
     * <li>Снимает блокировку на кеше</li>
     * </ul>
     * <br>
     * <b>Внимание! </b>Метод допустимо использовать толко для объектов использующих унифицированную
     * структуру хранения данных в кеше.<br>
     * В общем виде она выглядит как - <i>Один регион для типа, в котором под ключем имени региона содержится коллекция
     * идентификаторов, далее следуют объекты по своим идентификаторам</i>.
     *
     * @param identifiable   новый объект
     * @param region         имя региона, хранящего данный объект
     * @param sql            запрос, выполняемый системным продюсером
     * @param autogenerateId признак использования стандартного механизма генерации идентификатора
     *
     * @return идентификатор созданного объекта
     * @throws GenericSystemException      в случае возникновения ошибок при доступе к мета-информаци
     * @throws IneIllegalArgumentException при попытке передать null или некорректный объект в контексте операции
     */
    protected T createObject(T identifiable, String region, String sql, boolean autogenerateId)
            throws IneIllegalArgumentException, GenericSystemException {
        return iCreateAccess.createObject(identifiable, this.getAllProducer(region, sql), region, autogenerateId);
    }

    /**
     * Оболочка для вызова при создании нового объекта
     *
     * @param identifiable   новый объект
     * @param autogenerateId признак использования стандартного механизма генерации идентификатора
     *
     * @return идентификаторы созданного объекта
     * @throws CoreException               в случае возникновения ошибок при доступе к мета-информаци
     * @throws IneIllegalArgumentException при попытке передать null или некорректный объект в контексте операции
     */
    protected BigDecimal[] createObject(T identifiable, boolean autogenerateId)
            throws IneIllegalArgumentException, CoreException {
        return this.createObject(identifiable, autogenerateId, false);
    }

    /**
     * Оболочка для вызова при создании объекта
     *
     * @param identifiable   создаваемый объект
     * @param autogenerateId признак использования стандартного механизма генерации идентификатора
     * @param history        признак использования исторической таблицы
     *
     * @return идентификаторы созданного объекта
     * @throws CoreException               в случае возникновения ошибок при доступе к мета-информаци
     * @throws IneIllegalArgumentException при попытке передать null или некорректный объект в контексте операции
     */
    protected BigDecimal[] createObject(T identifiable, boolean autogenerateId, boolean history)
            throws IneIllegalArgumentException, CoreException {
        return iCreateAccess.createObject(identifiable, autogenerateId, history);
    }

    /**
     * Метод выполняет действия необходимые перед созданием объекта
     *
     * @param identifiable создаваемый объект
     *
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок
     */
    @SuppressWarnings({"unchecked"})
    protected void beforeCreate(T identifiable) throws CoreException {
        if (HELPERS_MAP.containsKey(this.getClass())) {
            for (CRUDHelper<T> helper : HELPERS_MAP.get(this.getClass())) {
                helper.beforeCreate(identifiable, this);
            }
        }
    }

    /**
     * Метод выполняет действия необходимые для дополнения объекта данными после его создания
     *
     * @param identifiable созданный и дополненный данными объект
     *
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок
     */
    @SuppressWarnings({"unchecked"})
    protected void afterCreate(T identifiable) throws CoreException {
        if (HELPERS_MAP.containsKey(this.getClass())) {
            for (CRUDHelper<T> helper : HELPERS_MAP.get(this.getClass())) {
                helper.afterCreate(identifiable);
            }
        }
    }

    /* **********************
     * Модификация объекта
     * **********************/

    /**
     * Оболочка для вызова при изменении объекта.
     *
     * @param identifiable    исходный объект
     * @param newIdentifiable модифицированный объект
     * @param region          имя региона кеша, хранящего данный объект
     *
     * @return модифицированный объект
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке в процессе получения и обработки данных
     */
    protected T updateObject(T identifiable, T newIdentifiable, String region)
            throws IneIllegalArgumentException, GenericSystemException {
        return iUpdateAccess.updateObject(identifiable, newIdentifiable, region);
    }

    protected void updateObjectInDB(T t) throws IneIllegalArgumentException, GenericSystemException {
        iUpdateAccess.updateObjectInDB(t);
    }

    /**
     * Метод выполняет действия необходимые перед обновлением объекта
     *
     * @param identifiable    обновляемый объект
     * @param newIdentifiable обновленный объект
     *
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок
     */
    @SuppressWarnings({"unchecked"})
    protected void beforeUpdate(T identifiable, T newIdentifiable) throws CoreException {
        if (HELPERS_MAP.containsKey(this.getClass())) {
            for (CRUDHelper<T> helper : HELPERS_MAP.get(this.getClass())) {
                helper.beforeUpdate(identifiable, newIdentifiable);
            }
        }
    }

    /**
     * Метод выполняет действия необходимые после обновления объекта
     *
     * @param identifiable    обновляемый объект
     * @param newIdentifiable обновленный объект
     *
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок
     */
    @SuppressWarnings({"unchecked"})
    protected void afterUpdate(T identifiable, T newIdentifiable) throws CoreException {
        if (HELPERS_MAP.containsKey(this.getClass())) {
            for (CRUDHelper<T> helper : HELPERS_MAP.get(this.getClass())) {
                helper.afterUpdate(identifiable, newIdentifiable);
            }
        }
    }

    /* **********************
     * Удаление объекта
     * **********************/

    /**
     * Оболочка для вызова при удалении объекта.
     * <p/>
     * <p/>
     * Последовательность работы метода:
     * <ul>
     * <li>Созраняет объект в истории</li>
     * <li>Удаляет объект из БД</li>
     * <li>Обновляет коллекцию с идентификаторами</li>
     * <li>Обновляет коллекцию с идентификаторами в кеше</li>
     * <li>Удаляет объект из кеша</li>
     * </ul>
     * <br>
     *
     * @param identifiable удаляемый объект
     * @param allProducer  продюссер для конкретного типа, должен возвращать коллекцию идентификаторов
     *                     актуальных версий и заполнять кеш данными по самим объектам
     * @param region       имя региона кеша, хранящего данный объект
     *
     * @throws GenericSystemException      в случае возникновения ошибок при доступе к мета-информаци
     * @throws IneIllegalArgumentException при попытке передать null
     */
    protected void deleteObject(T identifiable, Producer allProducer, String region)
            throws IneIllegalArgumentException, GenericSystemException {

        try {
            this.beforeDelete(identifiable);

            if (!Cache.containsRegion(region)) {
                synchronized (LOCK) {
                    if (!Cache.containsRegion(region)) {
                        Cache.put(region, (Map) allProducer.get(region));
                    }
                }
            }

            SyntheticId cacheId = this.getSyntheticId(identifiable);
            this.deleteObjectFromDB(cacheId.getIdValues());

            Cache.remove(region, cacheId, false);

            this.afterDelete(identifiable);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    /**
     * Оболочка для вызова при удалении объекта.
     * <p/>
     * <p/>
     * Последовательность работы метода:
     * <ul>
     * <li>Созраняет объект в истории</li>
     * <li>Удаляет объект из БД</li>
     * <li>Обновляет коллекцию с идентификаторами</li>
     * <li>Обновляет коллекцию с идентификаторами в кеше</li>
     * <li>Удаляет объект из кеша</li>
     * </ul>
     * <br>
     *
     * @param identifiable удаляемый объект
     * @param region       имя региона кеша, хранящего данный объект
     * @param sql          запрос, выполняемый системным продюсером
     *
     * @throws GenericSystemException      в случае возникновения ошибок при доступе к мета-информаци
     * @throws IneIllegalArgumentException при попытке передать null
     */
    protected void deleteObject(T identifiable, String region, String sql)
            throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, this.getAllProducer(region, sql), region);
    }

    /**
     * Удаляет существующий объект
     *
     * @param id идентификаторы удаляемого объекта
     *
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          при попытке передать null или некорректный объект в
     *          контексте операции
     * @throws ru.effts.ine.core.GenericSystemException
     *          ошибка при удалении из БД
     */
    protected void deleteObjectFromDB(BigDecimal... id) throws IneIllegalArgumentException, GenericSystemException {
        if (id == null) {
            throw new IneIllegalArgumentException("Can't delete object by null identifiers");
        }
        this.deleteObjectFromDB(
                "DELETE FROM " + this.getTableName() + " WHERE " + this.getIdConditionSql(""), (Object[]) id);
    }

    /**
     * Метод выполняет действия необходимые перед удалением объекта
     *
     * @param identifiable удаляемый объект
     *
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок
     */
    @SuppressWarnings({"unchecked"})
    protected void beforeDelete(T identifiable) throws CoreException {
        if (HELPERS_MAP.containsKey(this.getClass())) {
            for (CRUDHelper<T> helper : HELPERS_MAP.get(this.getClass())) {
                helper.beforeDelete(identifiable);
            }
        }
    }

    /**
     * Метод выполняет действие необходимые после удаления объекта
     *
     * @param identifiable удаляемый объект
     *
     * @throws ru.effts.ine.core.CoreException
     *          при возникновении ошибок
     */
    @SuppressWarnings({"unchecked"})
    protected void afterDelete(T identifiable) throws CoreException {
        if (HELPERS_MAP.containsKey(this.getClass())) {
            for (CRUDHelper<T> helper : HELPERS_MAP.get(this.getClass())) {
                helper.afterDelete(identifiable);
            }
        }
    }

    /* **********************
     * Утилитная внутрянка
     * **********************/

    @SuppressWarnings({"UnusedParameters"})
    protected Producer getAllProducer(final String region, final String sql) {

        return new Producer() {

            @Override
            public Object get(Object key) throws CoreException {

                Map<SyntheticId, T> map = new HashMap<>(500000);

                initAdditionalData();

                for (T identifiable : getObjects(false, sql)) {
                    SyntheticId cacheId = getSyntheticId(identifiable);
                    map.put(cacheId, identifiable);
                }

                clearAdditionalData();

                return map;
            }

        };
    }

    /**
     * Подготавливает дополнительные данные, которые могут потребоваться в процессе конструирования объекта,
     * полученного из БД. Необходимо для улучшения производительности при старте системы.
     *
     * @throws GenericSystemException в случае различных ошибок при получении дополнительных данных
     */
    protected void initAdditionalData() throws GenericSystemException {
        // does nothing
    }

    /**
     * Очищает дополнительные данные после загрузки данных из БД.
     *
     * @throws GenericSystemException в случае различных ошибок при очистке дополнительных данных
     */
    protected void clearAdditionalData() throws GenericSystemException {
        // does nothing
    }

    /**
     * Устанавливает названия столбцов, являющиеся идентификаторами объекта.
     *
     * @param clazz класс, для которого устанавливаются названия столбцов
     * @param names назвния столбцов таблицы
     */
    protected static void setIdColumns(Class clazz, String[] names) {
        for (int i = 0; i < names.length - 1; i++) {
            if (names[i] != null) {
                names[i] = names[i].toLowerCase();
            }
        }
        ID_FIELDS_MAP.put(clazz, names);
    }

    /**
     * Возвращает названия колонок, являющиеся идентфикаторами объекта
     *
     * @return названия колонок
     */
    protected String[] getIdColumns() {
        return ID_FIELDS_MAP.get(this.getClass());
    }

    /**
     * Возвращает строку для условия в конструкции 'WHERE ...', для получения конкретного экземпляра объекта.
     *
     * @param tableName название таблицы, которое будет префиксом при обращении к id. Если пустое - то не используется.
     *
     * @return строка вида ' tableName.ID1 = ? AND tableName.ID2 = ? AND ... tableName.IDN = ? '
     */
    protected String getIdConditionSql(String tableName) {

        String result = " ";
        String prefix = (tableName == null || tableName.trim().isEmpty() ? "" : tableName.trim() + ".");
        String[] idColumns = this.getIdColumns();
        for (int i = 0; i < idColumns.length; i++) {
            result += prefix + idColumns[i] + " = ?" + (i < idColumns.length - 1 ? " AND " : " ");
        }

        return result;
    }

    /**
     * Возвращает строку с запросом очередного идентификатора для подстановки в запрос создания нового объекта
     *
     * @param identifiable создаваемый объект
     *
     * @return обращение к сиквенсу
     * @throws ru.effts.ine.core.CoreException
     *          в случае возникновения ошибок при доступе к мета-информации
     */
    protected String getSequencePart(Class<T> identifiable) throws CoreException {
        if (DB_TYPE.equals(PropertiesBasedDAConfig.DBType.oracle)) {
            return FieldAccess.getSequenceForIdentifiable(identifiable) + ".NEXTVAL";
        } else {
            return "nextval('" + FieldAccess.getSequenceForIdentifiable(identifiable) + "')";
        }
    }

    /**
     * возвращает именам столбцов
     *
     * @param autogenerateId признак использования стандартного механизма генерации идентификатора
     *
     * @return столбцы используемые для работы с объектом в БД
     */
    protected Collection<String> getColumnNames(boolean autogenerateId) {

        Collection<String> result;

        if (autogenerateId) {
            synchronized (columnsMap) {
                if (!columnsMap.containsKey(this.getClass())) {
                    Collection<String> columns = this.prepareColumns();
                    columnsMap.put(this.getClass(), columns);
                }
            }
            result = columnsMap.get(this.getClass());
        } else {
            synchronized (columnsWthIdMap) {
                if (!columnsWthIdMap.containsKey(this.getClass())) {
                    Collection<String> columns = this.prepareColumns();
                    columns.addAll(Arrays.asList(getIdColumns()));
                    columnsWthIdMap.put(this.getClass(), columns);
                }
            }
            result = columnsWthIdMap.get(this.getClass());
        }

        return result;
    }

    /**
     * Подготавливает общий набор колонок
     *
     * @return имена колонок
     */
    private Collection<String> prepareColumns() {

        Map<String, String> map = new HashMap<>(this.getInstanceFields());
        Collection<String> fieldNames = new ArrayList<>(map.size());

        fieldNames.addAll(map.values());
        fieldNames.removeAll(Arrays.asList(this.getIdColumns()));

        return fieldNames;
    }

    /**
     * Строит мапинг соответствия имени атрибута методу доступа
     *
     * @param clazz класс для анализа
     *
     * @return мапинг
     */
    protected Map<String, Method> obtainFieldMethodMapping(Class clazz) {

        synchronized (METHOD_MAPPING) {
            if (!METHOD_MAPPING.containsKey(clazz)) {
                Map<String, Method> result = new LinkedHashMap<>(this.getInstanceFields().size(), 1);
                for (String fieldName : this.getInstanceFields().keySet()) {
                    try {
                        BeanInfo beanInfo = Introspector.getBeanInfo(clazz, Object.class);
                        PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
                        for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                            if (fieldName.equals(propertyDescriptor.getName())) {
                                result.put(fieldName, propertyDescriptor.getReadMethod());
                            }
                        }
                    } catch (IntrospectionException e) {
                        e.printStackTrace();
                    }
                }
                METHOD_MAPPING.put(clazz, result);
            }
        }

        return METHOD_MAPPING.get(clazz);
    }

    /**
     * Создает массив объектов для применения в запрос
     *
     * @param t           донор значений
     * @param columnNames массив с именами столбцов на основе которого составляется массив объектов
     *
     * @return массив объектов для применения в запрос
     * @throws ru.effts.ine.core.GenericSystemException
     *          при некорректных попытках доступа к методам
     */
    protected Object[] getFieldValuesByColumns(T t, Collection<String> columnNames) throws GenericSystemException {

        Object[] result = new Object[columnNames.size()];

        @SuppressWarnings({"unchecked"})
        Class<? extends T> aClass = getSubjectClass();

        Map<String, Method> fieldMethodMapping =
                this.obtainFieldMethodMapping(IdentifiableFactory.getImplementation(aClass).getClass());

        Map<String, String> invertedMap = invertFieldMapping();

        int position = 0;
        for (String columnName : columnNames) {
            String fieldName = invertedMap.get(columnName);
            if (fieldName == null) {
                fieldName = invertedMap.get(columnName.toLowerCase());
            }
            if (fieldName == null) {
                fieldName = invertedMap.get(columnName.toUpperCase());
            }

            if (fieldName == null) {
                throw new GenericSystemException("Filed name not found: " + columnName);
            }

            Method method = fieldMethodMapping.get(fieldName);

            try {
                result[position++] = method.invoke(t);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw GenericSystemException.toGeneric(e);
            } catch (Throwable throwable) {
                logger.warning("FN: " + fieldName + "; method: " + method.getName()
                        + "; method Type: " + method.getReturnType()
                        + "; aClass: " + aClass.getName()
                        + "; t: " + t.toString());
                throw GenericSystemException.toGeneric(throwable);
            }
        }

        return result;
    }

    /**
     * Возвращает мапинг, обратный мапингу поле-столбец {@link #INSTANCE_FIELDS_MAP}}
     *
     * @return мапинг столбец-поле
     */
    private Map<String, String> invertFieldMapping() {

        Map<String, String> result = new HashMap<>();
        for (Map.Entry<String, String> entry : this.getInstanceFields().entrySet()) {
            result.put(entry.getValue(), entry.getKey());
        }
        return result;
    }

    /**
     * Проверяет применимость поступившего объекта
     *
     * @param identifiable проверяемый объект
     */
    protected void checkApplicable(T identifiable) {
        if (identifiable == null) {
            throw new IneIllegalArgumentException("The source object can't be null");
        }
        if (identifiable instanceof Versionable) {
            throw new IneIllegalArgumentException("The source object can't be instance of Versionable object ");
        }
    }

    /* ******************************************* *
     * Всякие разные приседания с идентификаторами *
     * ******************************************* */

    /**
     * Проверяет переданный массив id-параметров на не пустое значение и на требуемое количество элементов.
     *
     * @param requiredLength требуемое количество элементов в массиве
     * @param addMessage     дополнительное сообщение об ошибке для детализации
     * @param id             массив id-параметров
     *
     * @throws IneIllegalArgumentException если параметр id = null, или количество не равно requiredLength
     */
    protected void checkIdParam(int requiredLength, String addMessage, BigDecimal... id)
            throws IneIllegalArgumentException {
        if (id == null) {
            throw new IneIllegalArgumentException("id must be not null. " + (addMessage != null ? addMessage : ""));
        }
        if (id.length != requiredLength) {
            throw new IneIllegalArgumentException("id must have " + requiredLength +
                    " element length, but has [" + id.length + "]. " + (addMessage != null ? addMessage : ""));
        }
    }

    /**
     * Проверяет переданный массив id-параметров на наличие строго одного элемента.
     * <p/>
     * Метод может переопределяться потомками, для адаптации
     * к используемому способу идентификации (количества элементов)
     *
     * @param id массив id-параметров
     *
     * @throws IneIllegalArgumentException если параметр id = null, или количество не равно requiredLength
     */
    protected void checkIdParam(BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(1, null, id);
    }

    /**
     * Метод производит установку идентификатора на указанный объект.
     * Переопределение метода понадобится для работы с объектами имеющими составной идентификатор
     *
     * @param identifiable объект для применения идентификатора
     * @param id           идентификатор/Ы
     *
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          если параметр id = null,
     *          или количество не соответствеут требуемому
     */
    protected void applyNewId(T identifiable, BigDecimal... id) throws IneIllegalArgumentException {
        this.checkIdParam(id);
        identifiable.setCoreId(id[0]);
    }

    /**
     * Возвращает составной идентификатор объекта.
     * <p/>
     * По-умолчанию в составной идентификатор попадает 1 идентификатор, содержащийся в столбце N таблицы объекта. Если
     * идентификатор объекта содержится в нескольких столбцах - необходимо переопределить метод в соответствующем
     * дата-акцессе.
     *
     * @param identifiable объект с идентификаторами
     *
     * @return объект составного идентификатора.
     * @throws GenericSystemException при ошибках в работе с мета-информацией.
     */
    @Override
    public SyntheticId getSyntheticId(T identifiable) throws GenericSystemException {
        SyntheticId result = new SyntheticId();
        result.addIdEntry(getIdColumnSystemId(), identifiable.getCoreId());
        return result;
    }

    private BigDecimal getIdColumnSystemId() throws GenericSystemException {
        if (ID_COLUMN_SYS_ID == null) {
            ID_COLUMN_SYS_ID = FieldAccess.getColumnId(getTableName(), getIdColumns()[0]);
        }
        return ID_COLUMN_SYS_ID;
    }

    /**
     * Получаем составной идентификатор из переданного массива обычных идентификаторов.
     * <p/>
     * Массив идентификаторов должен быть отсортирован в соответствии с порядком следования идентификационных столбцов,
     * получаемых методом {@link @getIdColumns}
     *
     * @param id массив идентификаторов
     *
     * @return составной идентификатор
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибках в работе с мета-информацией.
     */
    private SyntheticId getSyntheticId(BigDecimal... id) throws GenericSystemException {

        this.checkIdParam(id);
        SyntheticId syntheticId = new SyntheticId();
        String[] idColumns = this.getIdColumns();
        String tableName = this.getTableName();
        for (int i = 0; i < idColumns.length; i++) {
            syntheticId.addIdEntry(FieldAccess.getColumnId(tableName, idColumns[i]), id[i]);
        }

        return syntheticId;
    }

    @Override
    public SyntheticId checkSyntheticId(SyntheticId id) throws GenericSystemException {

        SyntheticId result = new SyntheticId();
        BigDecimal columnId;
        for (String column : this.getIdColumns()) {
            columnId = FieldAccess.getColumnId(getTableName(), column);
            result.addIdEntry(columnId, id.getIdValue(columnId));
        }

        return result;
    }

    /* *************************************
     * Приседания для регистрации действий
     * *************************************/

    /**
     * инжектится в код производящий модификацию данных
     *
     * @param actionType тип регистрируемого действия
     *
     * @deprecated <b>Внимание: Метод не предназначен для вызова из кода !!!</b>
     */
    @SuppressWarnings({"UnusedDeclaration"})
    @Deprecated
    protected void constructAction(String actionType) {
        UserProfile userProfile = UserHolder.getUser();
        userProfile.setLastAction(
                new UserProfile.Action(CrudCode.valueOf(actionType), userProfile.getReason()));
    }

    @SuppressWarnings({"unchecked", "UnusedDeclaration"})
    protected void checkPermissions(T identifiable, String crudName) throws CoreException {

        try {
            UserProfile userProfile = UserHolder.getUser();
            SystemUser systemUser = userProfile.getSystemUser();

            final BigDecimal currentRole = userProfile.getCurrentRole();
            if (currentRole == null) {
                throw new IneIllegalAccessException("Access denied: role is null!");
            }

            Map<BigDecimal, PermitView> viewMap =
                    PermitUtils.getPermitView(systemUser.getCoreId(), currentRole);

            if (viewMap != null) {
                BigDecimal objPermId;
                if (identifiable instanceof CustomAttributeValue) {
                    CustomAttributeValue value = (CustomAttributeValue) identifiable;
                    BigDecimal ctId = value.getCoreId();
                    CustomAttributeAccess<CustomAttribute> access =
                            (CustomAttributeAccess) AccessFactory.getImplementation(CustomAttribute.class);
                    CustomAttribute customAttribute = access.getObjectById(ctId);
                    objPermId = customAttribute.getSystemObjectId();
                } else {
                    objPermId = FieldAccess.getTableId(IdentifiableFactory.getInterface(identifiable));
                }

                if (objPermId != null && viewMap.containsKey(objPermId)) {
                    PermitView view = viewMap.get(objPermId);
                    CrudCode crudCode = CrudCode.valueOf(crudName);
                    for (int code : CrudCode.getByMask(view.getCrudMask())) {
                        if (crudCode.getCode() == code) {
                            return;
                        }
                    }
                }
            }
        } catch (Throwable e) {
            throw new GenericSystemException("Error while permissions checking. Caused message: " + e.getMessage(), e);
        }

        throw new IneIllegalAccessException("Access denied " + crudName + ", [" + identifiable + "]");
    }

    /**
     * Метод инжектится в код производящий модификацию данных
     *
     * @param identifiable модифицируемый объект
     *
     * @throws ru.effts.ine.core.GenericSystemException
     *          при ошибке работы с БД или прочих ошибках
     * @deprecated <b>Внимание: Метод не предназначен для вызова из кода !!!</b>
     */
    @SuppressWarnings({"UnusedDeclaration", "ConstantConditions"})
    @Deprecated
    protected void registerAction(T identifiable) throws GenericSystemException {

        if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS).isEnabled()) {
            return;
        }

        if (identifiable == null) {
            throw new IneIllegalArgumentException("Can't register null");
        }

        UserProfile userProfile = UserHolder.getUser();
        SystemUser systemUser = userProfile.getSystemUser();
        UserProfile.Action action = userProfile.getLastAction();

        BigDecimal tableId = BigDecimal.ZERO;
        try {
            tableId = FieldAccess.getTableId(identifiable.getClass());
        } catch (CoreException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }
        if (tableId == null) {
            tableId = (BigDecimal) userProfile.getAttribute(UserProfile.TABLE_ID);
            userProfile.removeAttribute(UserProfile.TABLE_ID);
        }

        String entityId = identifiable.getCoreId().toString();
        try {
            entityId = getSyntheticId(identifiable).toString();
        } catch (CoreException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        Date fromDate = new Date();
        Date toDate = fromDate;
        if (identifiable instanceof Versionable) {
            fromDate = ((Versionable) identifiable).getCoreFd();
            toDate = ((Versionable) identifiable).getCoreTd();
        }

        try {
            Connection connection = PoolManager.getConnection();
            try {
                this.registerAction(connection, tableId, entityId, fromDate, toDate, systemUser.getCoreId(),
                        action.getDate(), action.getType().getCode(), action.getReason());

                if (FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.SAVE_ACTION_ATTRIBUTES).isEnabled()) {
                    this.registerAttributes(connection, tableId, entityId, action.getDate());
                }

                this.toCommit(connection);
            } catch (SQLException e) {
                this.toRollback(connection);
                logger.log(Level.SEVERE, "SQLException: ", e);
                throw new GenericSystemException(e.getMessage(), e);
            } finally {
                connection.close();
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQLException: ", e);
            throw new GenericSystemException(e.getMessage(), e);
        }
    }

    private void registerAction(Connection connection, Object... params) throws SQLException {

        String insertSql = "INSERT INTO OBJ_HISTORY (N, ENT_ID, FD, TD, USER_ID, ACTION_DATE, TYP, REASON) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement preparedStatement = connection.prepareStatement(insertSql);
        this.prepareParams(preparedStatement, params);

        preparedStatement.execute();
        preparedStatement.close();
    }

    private void registerAttributes(Connection connection, Object... params) throws SQLException {

        UserProfile userProfile = UserHolder.getUser();
        Map<String, String> attributes = userProfile.getUserAttributesMap();
        if (attributes.isEmpty()) {
            return;
        }

        String insertSql =
                "INSERT INTO OBJ_HISTORY_ATTRS (N, ENT_ID, ACTION_DATE, ATTR_NAME, ATTR_VALUE) VALUES (?, ?, ?, ?, ?)";

        PreparedStatement preparedStatement = connection.prepareStatement(insertSql);

        Object[] paramsLocal = Arrays.copyOf(params, params.length + 2);
        for (Map.Entry entry : attributes.entrySet()) {
            paramsLocal[params.length] = entry.getKey();
            paramsLocal[params.length + 1] = entry.getValue();
            this.prepareParams(preparedStatement, paramsLocal);
            preparedStatement.addBatch();
        }

        preparedStatement.executeBatch();
        preparedStatement.close();

        userProfile.clearUserAttributes();
    }

    /* **********************************
     * Общие операции с полями объектов
     * **********************************/

    /**
     * Получает значение идентификатора из колонки по умолчанию {@link #COLUMN_ID}. Для корректного формирования
     * сообщения об ошибке первый столбец в запросе должен содержать rowid.
     *
     * @param resultSet результат выборки
     *
     * @return идентификатор
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     * @throws ru.effts.ine.core.CorruptedIdException
     *                               при обнаружении некорректного идентификатора
     */
    protected BigDecimal getId(ResultSet resultSet) throws SQLException, CorruptedIdException {
        return this.getId(resultSet, COLUMN_ID);
    }

    /**
     * Получает значение идентификатора из указанной колонки. Для корректного формирования сообщения об ошибке первый
     * столбец в запросе должен содержать rowid.
     *
     * @param resultSet  результат выборки
     * @param columnName имя колонки
     *
     * @return идентификатор
     * @throws java.sql.SQLException при неверном названии колонки, при неверном типе или при закрытом ResultSet
     * @throws ru.effts.ine.core.CorruptedIdException
     *                               при обнаружении некорректного идентификатора
     */
    protected BigDecimal getId(ResultSet resultSet, String columnName) throws SQLException, CorruptedIdException {
        BigDecimal id = this.getBigDecimal(resultSet, columnName);
        if (id == null) {
            throw new CorruptedIdException("null id value found in column [" + columnName + "] with rowId["
                    + resultSet.getString(1) + "]");
        }
        return id;
    }

    protected void setIdentifiableFields(ResultSet resultSet, Identifiable identifiable)
            throws SQLException, CorruptedIdException, IneCorruptedVersionableDateException {
        this.setIdentifiableFields(resultSet, identifiable, COLUMN_ID);
    }

    protected void setIdentifiableFields(ResultSet resultSet, Identifiable identifiable, String idColumn)
            throws SQLException, CorruptedIdException, IneCorruptedVersionableDateException {
        this.setIdentifiableFields(resultSet, identifiable, idColumn, COLUMN_DSC);
    }

    protected void setIdentifiableFields(ResultSet resultSet, Identifiable identifiable, String idColumn,
                                         String dscColumn) throws SQLException, CorruptedIdException, IneCorruptedVersionableDateException {
        identifiable.setCoreId(getId(resultSet, idColumn));
        identifiable.setCoreDsc(getString(resultSet, dscColumn));
    }

    private class DataRuleMeasurer {

        public String measure(SearchCriterion searchCriterion, ArrayList params) {
            DataType dataType = searchCriterion.getCriteriaRuleSet().getDataType();

            switch (dataType) {

                case bigDecimalType:
                    return createNumber(searchCriterion, params);
                case longType:
                    return createNumber(searchCriterion, params);
                case dateType:
                    return createNumber(searchCriterion, params);
                default:
                    throw new IllegalArgumentException();
            }

        }

        @SuppressWarnings("unchecked")
        private String createNumber(SearchCriterion searchCriterion, ArrayList params) {

            String columnName = getInstanceFields().get(searchCriterion.getFieldName());

            switch (searchCriterion.getCriterionRule()) {

                case equals:
                    params.add(searchCriterion.getCriterionVals()[0]);
                    return columnName + " = ? ";
                case isNull:
                    return columnName + " is null ";
                case less:
                    params.add(searchCriterion.getCriterionVals()[0]);
                    return columnName + " < ? ";
                case greater:
                    params.add(searchCriterion.getCriterionVals()[0]);
                    return columnName + " > ? ";
                case between:
                    params.add(searchCriterion.getCriterionVals()[0]);
                    params.add(searchCriterion.getCriterionVals()[1]);
                    return columnName + " between ? and ?";
                case in:
                    String result = columnName + " in (";

                    for (Object o : searchCriterion.getCriterionVals()) {
                        result = result + " ?,";
                        params.add(o);
                    }

                    result = result.replaceAll(",^", "");

                    result += ")";

                    return result;
                default:
                    return " 1 = 1";
            }

        }


    }

}
