/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.structure.SystemObject;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Оболочка для размещения в кеше стуктуры данных по системным объектам
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SysObjCacheContainer.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
class SysObjCacheContainer<T extends SystemObject> implements Serializable {

    /*
     * В классе используется предоставление непосредственного доступа к полям по причине его
     * закрытости для внешних пакетов. Данное поведение считается безопасным в этих условиях.
     * /

    /** Простая мапа со всеми системными объектами */
    private Map<BigDecimal, T> objects = new HashMap<BigDecimal, T>(5000);

    /** Агрегация идентифиаторов по типу системных объектов */
    private Map<BigDecimal, Collection<BigDecimal>> idsByType = new HashMap<BigDecimal, Collection<BigDecimal>>(5);

    /** Маппинг идентификаторов таблиц по их имени */
    private Map<String, BigDecimal> tableIdsByTableName = new HashMap<String, BigDecimal>(500);

    /** Агрегация маппингов "название столбца - идентификатор столбца" по идентификатору таблицы */
    private Map<BigDecimal, Map<String, BigDecimal>> columnsByTable =
            new HashMap<BigDecimal, Map<String, BigDecimal>>(10);

    /** Маппинг идентификатора таблицы на имя класса интерфейса */
    private Map<BigDecimal, String> interfacesByTable = new HashMap<>(tableIdsByTableName.size());

    /** Маппинг дочерних объектов на парента */
    private Map<BigDecimal, Collection<BigDecimal>> childrenByParent =
            new HashMap<>(300);

    /** Маппинг объектов с паттернам по типу системных объектов */
    private Map<BigDecimal, Collection<T>> patternsByType = new HashMap<>(300);

    /** Маппинг паттернов по типу системных объектов-владельцев */
    private Map<BigDecimal, Collection<T>> patternsForType = new HashMap<>(300);


    Map<BigDecimal, T> getObjects() {
        return this.objects;
    }

    Map<BigDecimal, Collection<BigDecimal>> getIdsByType() {
        return this.idsByType;
    }

    Map<String, BigDecimal> getTableIdsByTableName() {
        return this.tableIdsByTableName;
    }

    Map<BigDecimal, Map<String, BigDecimal>> getColumnsByTable() {
        return this.columnsByTable;
    }

    Map<BigDecimal, String> getInterfacesByTable() {
        return this.interfacesByTable;
    }

    Map<BigDecimal, Collection<BigDecimal>> getChildrenByParent() {
        return this.childrenByParent;
    }

    Map<BigDecimal, Collection<T>> getPatternsByType() {
        return this.patternsByType;
    }

    Map<BigDecimal, Collection<T>> getPatternsForType() {
        return patternsForType;
    }

    @Override
    public String toString() {
        return "objects count[" + objects.size() + "]; types count[" + idsByType.size() + "]; tables count[" +
                tableIdsByTableName.size() + "]; columns count[" + columnsByTable.size() + "]; interfaces count[" +
                interfacesByTable.size() + "]; children count[" + childrenByParent.size() + "]; patterns count[" +
                patternsByType.size() + "]; patternsForType count [" + patternsForType.size() + "]";
    }

    String dupm() {
        return "DUMP... objects[" + objects + "];" + System.getProperty("line.separator") +
                "         types[" + idsByType + "];" + System.getProperty("line.separator") +
                "        tables[" + tableIdsByTableName + "]" + System.getProperty("line.separator") +
                "       columns[" + idsByType + "];" + System.getProperty("line.separator") +
                "    interfaces[" + interfacesByTable + "];" + System.getProperty("line.separator") +
                "      children[" + childrenByParent + "];" + System.getProperty("line.separator") +
                "      patterns[" + patternsByType + "];" + System.getProperty("line.separator") +
                "      patternsForType[" + patternsByType + "];" + System.getProperty("line.separator");
    }

}
