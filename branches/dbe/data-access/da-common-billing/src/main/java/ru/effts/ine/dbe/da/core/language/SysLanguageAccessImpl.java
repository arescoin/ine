/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.language;

import ru.effts.ine.core.*;
import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Реализация по-умолчанию
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageAccessImpl.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class SysLanguageAccessImpl<T extends SysLanguage>
        extends AbstractVersionableAccess<T> implements SysLanguageAccess<T> {

    /** Таблица системных языков и префикс для обращения к ее столбцам */
    public static final String TABLE = "LANG_MAPPING";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_LANG_N = "langs_n";
    public static final String COLUMN_LANG_CODES_N = "lang_codes_n";
    public static final String COLUMN_COUNTRY_CODES_CODE = "country_codes_code";

    /** Запрос на выборку всех описаний языков */
    public static final String ALL_SYS_LANGUAGES_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_LANG_N +
            ", " + TABLE_PREF + COLUMN_LANG_CODES_N +
            ", " + TABLE_PREF + COLUMN_COUNTRY_CODES_CODE +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            ", NULL AS " + COLUMN_DSC +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.language.SysLanguage} */
    public static final String CACHE_REGION_SYSTEM_LANGUAGES = SysLanguage.class.getName();

    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(SysLanguage.CORE_DSC, null);
        map.put(SysLanguage.CORE_ID, COLUMN_LANG_N);
        map.put(SysLanguage.LANG_DETAILS, COLUMN_LANG_CODES_N);
        map.put(SysLanguage.COUNTRY_DETAILS, COLUMN_COUNTRY_CODES_CODE);
        setInstanceFieldsMapping(SysLanguageAccessImpl.class, map);

        setIdColumns(SysLanguageAccessImpl.class, new String[]{COLUMN_LANG_N});

        registerVersionableHelpers(SysLanguageAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SYSTEM_LANGUAGES;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_SYSTEM_LANGUAGES, ALL_SYS_LANGUAGES_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_SYSTEM_LANGUAGES, ALL_SYS_LANGUAGES_SELECT, id);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(SysLanguage.class);

        this.setVersionableFields(resultSet, result, COLUMN_LANG_N);

        result.setLangDetails(this.getId(resultSet, COLUMN_LANG_CODES_N));
        result.setCountryDetails(this.getId(resultSet, COLUMN_COUNTRY_CODES_CODE));

        return result;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_SYSTEM_LANGUAGES, ALL_SYS_LANGUAGES_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_SYSTEM_LANGUAGES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_SYSTEM_LANGUAGES, ALL_SYS_LANGUAGES_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return SysLanguage.class;
    }

}
