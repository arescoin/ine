/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.*;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.dbe.da.core.*;
import ru.effts.ine.dbe.da.core.links.LinkAccess;
import ru.effts.ine.dbe.da.core.links.LinkDataAccess;
import ru.effts.ine.utils.cache.Cache;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Logger;

/**
 * реализация доступа к описаниям модифицированных типов
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeAccessImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class CustomTypeAccessImpl<T extends CustomType>
        extends AbstractVersionableAccess<T> implements CustomTypeAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "CUSTOM_TYPES_DSC";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_BASE = "base";
    public static final String COLUMN_PARENT_TYPE = "parent";
    public static final String COLUMN_TYPE_NAME = "type_name";

    /** Запрос на выборку всех атрибутов */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_BASE +
            ", " + TABLE_PREF + COLUMN_PARENT_TYPE +
            ", " + TABLE_PREF + COLUMN_TYPE_NAME +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.structure.CustomType} */
    public static final String CACHE_REGION_CUSTOM_TYPES = CustomType.class.getName();

    private Map<BigDecimal, Collection<CustomAttribute>> attributesByType;


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(CustomType.BASE_OBJECT, COLUMN_BASE);
        map.put(CustomType.PARENT_TYPE, COLUMN_PARENT_TYPE);
        map.put(CustomType.TYPE_NAME, COLUMN_TYPE_NAME);
        setInstanceFieldsMapping(CustomTypeAccessImpl.class, map);

        Set<CRUDHelper> customTypeHelpers = new LinkedHashSet<CRUDHelper>();
        customTypeHelpers.add(new CustomTypeHelper<CustomType>());
        registerVersionableHelpers(CustomTypeAccessImpl.class, customTypeHelpers);
    }


    @SuppressWarnings({"unchecked"})
    protected static CustomAttributeAccessImpl<CustomAttribute> getCustomAttributeAccess()
            throws GenericSystemException {

        return (CustomAttributeAccessImpl) AccessFactory.getImplementation(CustomAttribute.class);
    }

    @Override
    public String getTableName() {
        return CustomTypeAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_CUSTOM_TYPES;
    }

    @Override
    public Collection<String> getBaseObjects() throws CoreException {

        Collection<String> intefaces = FieldAccess.getSupportedIntefaces();
        Iterator<String> iterator = intefaces.iterator();
        while (iterator.hasNext()) {
            String str = iterator.next();
            if (!str.startsWith("javax.oss.")) {
                iterator.remove();
            }
        }

        return intefaces;
    }

    @Override
    @SuppressWarnings({"unchecked"})
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        T result = (T) IdentifiableFactory.getImplementation(CustomType.class);

        this.setVersionableFields(resultSet, result);

        result.setTypeName(this.getString(resultSet, COLUMN_TYPE_NAME));
        result.setBaseObject(this.getBigDecimal(resultSet, COLUMN_BASE));

        //установка родительского модифицированного типа
        result.setParentTypeId(this.getBigDecimal(resultSet, COLUMN_PARENT_TYPE));

        //загрузка описаний настраиваемых аттрибутов по линкам данного модифицированного типа
        if (this.attributesByType != null) {
            result.setCustomAttributes(this.attributesByType.containsKey(result.getCoreId())
                    ? this.attributesByType.get(result.getCoreId()) : new ArrayList<CustomAttribute>());
        } else {
            Class anInterface = this.getInterface(result.getBaseObject());
            if (anInterface != null) {
                result.setCustomAttributes(
                        getCustomAttributeAccess().getAttributesByObject(anInterface, result.getCoreId()));
            }
        }

        return result;
    }

    @Override
    protected void initAdditionalData() throws GenericSystemException {
        super.initAdditionalData();
        this.attributesByType = this.getAttributesByType();
    }

    @Override
    protected void clearAdditionalData() throws GenericSystemException {
        super.clearAdditionalData();
        this.attributesByType = null;
    }

    private Map<BigDecimal, Collection<CustomAttribute>> getAttributesByType() throws GenericSystemException {

        Map<BigDecimal, Collection<CustomAttribute>> result = new HashMap<BigDecimal, Collection<CustomAttribute>>(500);

        try {
            Collection<CustomAttribute> attributes = getCustomAttributeAccess().getAllObjects();
            BigDecimal customTypeId;
            for (CustomAttribute attribute : attributes) {
                customTypeId = attribute.getCustomTypeId();
                if (customTypeId != null) {
                    if (!result.containsKey(customTypeId)) {
                        result.put(customTypeId, new ArrayList<CustomAttribute>());
                    }
                    result.get(customTypeId).add(attribute);
                }
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return result;
    }

    @Override
    public Collection<? extends T> getAllObjects() throws CoreException {
        return this.getAllObjects(this.getMainRegionKey(), ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(this.getMainRegionKey(), ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, this.getMainRegionKey(), ALL_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, this.getMainRegionKey());
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, this.getMainRegionKey(), ALL_DATA_SELECT);
    }

    private Class getInterface(BigDecimal tableId) throws GenericSystemException {
        try {
            return Class.forName(FieldAccess.getInterface(tableId));
        } catch (ClassNotFoundException e) {
            Logger.getLogger(this.getClass().getName()).warning(
                    "Failed to get interface for tableId " + tableId + ", cause: " + e.getMessage());
            return null;
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    @Override
    public Collection<T> getTypesForBaseObject(BigDecimal baseObject) throws GenericSystemException {

        SearchCriteriaProfile profile = getSearchCriteriaProfile();
        Set<SearchCriterion> searchCriterias = new HashSet<SearchCriterion>();
        searchCriterias.add(profile.getSearchCriterion(T.BASE_OBJECT, CriteriaRule.equals, baseObject));

        return this.searchObjects(searchCriterias);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return CustomType.class;
    }

    @Override
    public Collection<CustomAttribute> getAllAttributes(T customType) throws GenericSystemException {

        //получение настраиваемых аттрибутов(custom) предка
        Collection<CustomAttribute> parentAttributesCollection = this.getParentAttributes(customType);
        //добавление своих настраиваемых аттрибутов
        parentAttributesCollection.addAll(customType.getCustomAttributes());

        return parentAttributesCollection;
    }

    @Override
    public Collection<CustomAttribute> getParentAttributes(T customType) throws GenericSystemException {

        Collection<CustomAttribute> attributes = new HashSet<CustomAttribute>();
        try {
            if (customType.getParentTypeId() != null) {
                //если предок есть
                T parent = this.getObjectById(customType.getParentTypeId());
                attributes.addAll(this.getAllAttributes(parent));
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return attributes;
    }

    @Override
    public boolean isCustomTypeUsed(BigDecimal typeId) throws GenericSystemException {

        SearchCriteriaProfile profile = this.getLinkDataAccess().getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<SearchCriterion>(2);
        BigDecimal[] linkIds = this.getLinkIds(true);
        if (linkIds.length > 0) {
            search.add(profile.getSearchCriterion(LinkData.LINK_ID, CriteriaRule.in, linkIds));
            search.add(profile.getSearchCriterion(LinkData.LEFT_ID, CriteriaRule.equals, typeId));

            if (this.getLinkDataAccess().containsObjects(search)) {
                return true;
            }
        }

        search.clear();

        linkIds = this.getLinkIds(false);
        if (linkIds.length > 0) {
            search.add(profile.getSearchCriterion(LinkData.LINK_ID, CriteriaRule.in, linkIds));
            search.add(profile.getSearchCriterion(LinkData.RIGHT_ID, CriteriaRule.equals, typeId));
        }

        return this.getLinkDataAccess().containsObjects(search);
    }

    @SuppressWarnings({"unchecked"})
    private BigDecimal[] getLinkIds(boolean left) throws GenericSystemException {

        Collection<Link> links =
                ((LinkAccess) AccessFactory.getImplementation(Link.class)).getLinksByObjType(CustomType.class);
        BigDecimal typeId = FieldAccess.getTableId(CustomType.class);

        Collection<BigDecimal> result = new ArrayList<BigDecimal>(links.size());
        for (Link link : links) {
            if ((left && link.getLeftType().getTableId().equals(typeId)) ||
                    (!left && link.getRightType().getTableId().equals(typeId))) {
                result.add(link.getCoreId());
            }
        }

        return result.toArray(new BigDecimal[result.size()]);
    }

    @SuppressWarnings({"unchecked"})
    private LinkDataAccess<LinkData> getLinkDataAccess() throws GenericSystemException {
        return (LinkDataAccess<LinkData>) AccessFactory.getImplementation(LinkData.class);
    }


    public static final class CustomTypeHelper<E extends CustomType> extends AbstractCRUDHelper<E> {

        @SuppressWarnings({"unchecked"})
        private CustomTypeAccessImpl<CustomType> getCustomTypeAccess() throws GenericSystemException {
            return (CustomTypeAccessImpl) AccessFactory.getImplementation(CustomType.class);
        }

        @Override
        public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {
            //проверим, что имя типа уникально в пределах наследников базового типа
            this.checkName(identifiable);
            //проверим атрибуты
            this.checkAttributes(identifiable, identifiable.getCustomAttributes());
        }

        @Override
        public void afterCreate(E identifiable) throws GenericSystemException {
            this.createAttributes(identifiable, new HashSet<CustomAttribute>(), identifiable.getCustomAttributes());
        }

        @Override
        public void beforeUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {

            //проверим, что имя типа не изменяется
            if (!newIdentifiable.getTypeName().equals(identifiable.getTypeName())) {
                throw new IneIllegalArgumentException("Change of typeName is prohibited");
            }

            //проверим, что базовый объект не изменяется
            if (!newIdentifiable.getBaseObject().equals(identifiable.getBaseObject())) {
                throw new IneIllegalArgumentException("Change of baseObject is prohibited");
            }

            //проверим, что родитель не изменяется
            if (newIdentifiable.getParentTypeId() == null ? identifiable.getParentTypeId() != null
                    : !newIdentifiable.getParentTypeId().equals(identifiable.getParentTypeId())) {
                throw new IneIllegalArgumentException("Change of parentTypeId is prohibited");
            }

            //проверим что нет объектов связанных с данным модифицированным типом
            this.checkLinkData(newIdentifiable, 0);
            //проверим, что нет потомков
            this.checkDescendants(newIdentifiable, 0);
            //проверим атрибуты
            this.checkAttributes(newIdentifiable, newIdentifiable.getCustomAttributes());
        }

        @Override
        public void afterUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {

            //находим добавленные/удаленные атрибуты
            Map<BigDecimal, CustomAttribute> oldAttributes = this.getAttributesMap(identifiable.getCustomAttributes());

            List<CustomAttribute> createdAttrs = new ArrayList<CustomAttribute>();
            List<CustomAttribute> updatedAttrs = new ArrayList<CustomAttribute>();
            List<CustomAttribute> savedAttrs = new ArrayList<CustomAttribute>();

            /* Разделяем коллекцию атрибутов изменённого объекта на части */
            for (CustomAttribute attribute : newIdentifiable.getCustomAttributes()) {
                if (oldAttributes.containsKey(attribute.getCoreId())) {
                    /* атрибут содержится в коллекции атрибутов исходного объекта*/
                    if (!oldAttributes.get(attribute.getCoreId()).equals(attribute)) {
                        /* атрибут изменён, помещаем его в коллекцию изменённых атрибутов */
                        updatedAttrs.add(attribute);
                    } else {
                        /* атрибут не изменялся */
                        savedAttrs.add(attribute);
                    }
                } else {
                    /* атрибута нет в коллекции атрибутов исходного объекта, значит он только что создан */
                    createdAttrs.add(attribute);
                }
            }

            /* Обрабатываем атрибуты. Сначала модификация. */
            for (CustomAttribute attribute : updatedAttrs) {
                attribute.setCoreFd(newIdentifiable.getCoreFd());
                attribute.setCoreTd(newIdentifiable.getCoreTd());
                savedAttrs.add(getCustomAttributeAccess().updateObject(
                        oldAttributes.get(attribute.getCoreId()), attribute));
                /* удаляем атрибут из вспомогательной мапы исходных атрибутов */
                oldAttributes.remove(attribute.getCoreId());
            }

            /* Потом удаление атрибутов */
            if (!oldAttributes.isEmpty()) {
                this.deleteAttributes(newIdentifiable, oldAttributes.values());
            }

            /* И только в самом конце - создание новых атрибутов */
            this.createAttributes(newIdentifiable, savedAttrs, createdAttrs);
        }

        @Override
        public void beforeDelete(E identifiable) throws GenericSystemException {

            //проверим что нет объектов связанных с данным модифицированным типом
            //this.checkLinkData(identifiable, 1); // проверяется в {@link PatternHelper}
            //проверим, что нет потомков
            this.checkDescendants(identifiable, 1);
            //удаляем все атрибуты
            this.deleteAttributes(identifiable, identifiable.getCustomAttributes());
        }

        private Map<BigDecimal, CustomAttribute> getAttributesMap(Collection<CustomAttribute> attributes) {

            Map<BigDecimal, CustomAttribute> result = new HashMap<BigDecimal, CustomAttribute>(attributes.size(), 1.1f);
            for (CustomAttribute attribute : attributes) {
                result.put(attribute.getCoreId(), attribute);
            }

            return result;
        }

        private void createAttributes(E type, Collection<CustomAttribute> savedAttributes,
                                      Collection<CustomAttribute> attributesToCreate) throws GenericSystemException {

            CustomTypeAccess<CustomType> access = this.getCustomTypeAccess();

            /**
             * Такое двойное приседание с кешом нужно для корректной работы проверки имени атрибута на дубликаты
             * см. {@link CustomAttributeAccessImpl.CustomAttributeHelper#beforeCreate(CustomAttribute)}
             */
            type.setCustomAttributes(savedAttributes);
            Cache.put(access.getMainRegionKey(), access.getSyntheticId(type), type, false);

            /** Не оптимизировать это!!! Иначе проверка имени атрибута навернётся! */
            Collection<CustomAttribute> result = new HashSet<CustomAttribute>(attributesToCreate.size());
            for (CustomAttribute attribute : attributesToCreate) {
                attribute.setSystemObjectId(type.getBaseObject());
                attribute.setCustomTypeId(type.getCoreId());
                attribute.setCoreFd(type.getCoreFd());
                attribute.setCoreTd(type.getCoreTd());
                result.add(getCustomAttributeAccess().createObject(attribute));
            }

            type.getCustomAttributes().addAll(result);
            Cache.put(access.getMainRegionKey(), access.getSyntheticId(type), type, false);
        }

        private void deleteAttributes(E type, Collection<CustomAttribute> attributes) throws GenericSystemException {

            for (CustomAttribute attribute : attributes) {
                attribute.setCustomTypeId(null);
                attribute.setCoreFd(type.getCoreFd());
                attribute.setCoreTd(type.getCoreTd());
                getCustomAttributeAccess().deleteObject(attribute);
            }
        }

        /**
         * Проверяем, что имя типа не используется в рамках потомков одного типа.
         *
         * @param identifiable проверяемый кастом-тип
         *
         * @throws ru.effts.ine.core.GenericSystemException
         *          в случае ошибки при работе с БД
         * @throws ru.effts.ine.core.IneIllegalArgumentException
         *          в результате проверки
         */
        private void checkName(E identifiable) throws GenericSystemException {

            SearchCriteriaProfile profile = this.getCustomTypeAccess().getSearchCriteriaProfile();
            Set<SearchCriterion> search = new LinkedHashSet<SearchCriterion>(2);
            search.add(profile.getSearchCriterion(E.BASE_OBJECT, CriteriaRule.equals, identifiable.getBaseObject()));
            search.add(profile.getSearchCriterion(E.TYPE_NAME, CriteriaRule.equals, identifiable.getTypeName()));
            if (this.getCustomTypeAccess().containsObjects(search)) {
                throw new IneIllegalArgumentException("TypeName [" + identifiable.getTypeName() + "] is already used");
            }
        }

        /**
         * Проверяет атрибуты, чтобы не было атрибутов с именами, как у предков. И чтобы не было одинаковых имён
         * в переданной коллекции.
         *
         * @param type       проверяемый тип, с которого получаются предки и их атрибуты
         * @param attributes атрибуты проверяемого типа
         *
         * @throws ru.effts.ine.core.GenericSystemException
         *          в случае ошибки при работе с БД
         * @throws ru.effts.ine.core.IneIllegalArgumentException
         *          в результате проверки
         */
        private void checkAttributes(E type, Collection<CustomAttribute> attributes) throws GenericSystemException {

            Collection<String> attributesNames = new HashSet<String>();
            // получаем список названий атрибутов в коллекции
            for (CustomAttribute attribute : attributes) {
                attributesNames.add(attribute.getAttributeName().trim());
            }
            if (attributesNames.size() < attributes.size()) {
                throw new IneIllegalArgumentException("duplicate attributeNames found in input collection");
            }

            attributesNames = new ArrayList<String>();
            // получаем список названий атрибутов предков
            for (CustomAttribute attribute : this.getCustomTypeAccess().getParentAttributes(type)) {
                attributesNames.add(attribute.getAttributeName());
            }
            if (attributesNames.isEmpty()) {
                return;
            }
            // проверяем переданную коллекцию атрибутов.
            for (CustomAttribute attribute : attributes) {
                if (attributesNames.contains(attribute.getAttributeName())) {
                    throw new IneIllegalArgumentException("attributeName '" + attribute.getAttributeName() +
                            "' is already used by some of parent types");
                }
            }
        }

        /**
         * Проверяем, что нет объектов связанных с данным модифицированным типом
         *
         * @param type   модифицированный тип
         * @param action проверяемое действие: <code>0 - update, 1 - delete</code>
         *
         * @throws ru.effts.ine.core.GenericSystemException
         *          в случае ошибки при работе с БД
         * @throws ru.effts.ine.core.IneIllegalArgumentException
         *          в результате проверки
         */
        private void checkLinkData(E type, long action) throws GenericSystemException {
            if (this.getCustomTypeAccess().isCustomTypeUsed(type.getCoreId())) {
                throw new IneIllegalArgumentException(this.constractMessage(action, "objects"));
            }
        }

        /**
         * Проверяем, что нет у данного модифицированного типа нет потомков
         *
         * @param identifiable модифицированный тип
         * @param action       проверяемое действие: <code>0 - update, 1 - delete</code>
         *
         * @throws ru.effts.ine.core.GenericSystemException
         *          в случае ошибки при работе с БД
         * @throws ru.effts.ine.core.IneIllegalArgumentException
         *          в результате проверки
         */
        private void checkDescendants(E identifiable, long action) throws GenericSystemException {

            SearchCriteriaProfile profile = this.getCustomTypeAccess().getSearchCriteriaProfile();
            Set<SearchCriterion> search = new LinkedHashSet<SearchCriterion>(1);
            search.add(profile.getSearchCriterion(E.PARENT_TYPE, CriteriaRule.equals, identifiable.getCoreId()));
            if (this.getCustomTypeAccess().containsObjects(search)) {
                throw new IneIllegalArgumentException(this.constractMessage(action, "descendants"));
            }
        }

        private String constractMessage(long action, String cause) {
            return "CustomType can't be " + (action == 0 ? "updated" : "deleted") +
                    ". There are exist some " + cause + " of this type.";
        }

    }

}
