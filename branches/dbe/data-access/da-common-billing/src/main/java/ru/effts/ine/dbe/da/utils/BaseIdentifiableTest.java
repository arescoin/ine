/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.utils;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.core.userpermits.CrudCode;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.funcswitch.FuncSwitchUtils;
import ru.effts.ine.dbe.da.core.history.IdentifiableHistoryAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * Базовый класс для модульных тестов потомков {@link ru.effts.ine.dbe.da.core.AbstractIdentifiableAccess},
 * для тестирования потомков {@link ru.effts.ine.dbe.da.core.AbstractVersionableAccess} следует воспользоваться
 * классом {@link BaseVersionableTest}.
 * <p/>
 * Применение этого теста обязательно для каждого ДА системы.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: BaseIdentifiableTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public abstract class BaseIdentifiableTest extends BaseTest {

    protected boolean manualSetId = false;
    protected boolean checkActionDate = true;
    protected boolean checkHistory = true;
    protected BigDecimal tableId;
    protected boolean testingRequired = true;//для отключения в наследниках тестирования

    protected static final String EXPECTED_EXCEPTION_WAS_NOT_THROWN = "Expected exception was not thrown";

    public abstract Identifiable testCreate();

    public abstract Identifiable testRetrieve(Identifiable identifiable);

    public abstract Identifiable testUpdate(Identifiable identifiable);

    public abstract void testDelete(Identifiable identifiable);


    private BigDecimal getTableId(Class identifiableClass) throws CoreException {
        BigDecimal tableId = FieldAccess.getTableId(identifiableClass);
        if (tableId == null) {
            tableId = this.tableId;
        }
        return tableId;
    }

    @Test
    public void testCRUD() {

        if (!testingRequired) {
            return;
        }

        try {
            @SuppressWarnings({"unchecked"})
            IdentifiableHistoryAccess<IdentifiableHistory> access = (IdentifiableHistoryAccess<IdentifiableHistory>)
                    AccessFactory.getImplementation(IdentifiableHistory.class);

            // create
            UserHolder.getUser().setReason("Common CRUD testing: create; for class: " + getClass().getName());
            Identifiable created = this.testCreation(access);

            BigDecimal tableId = this.getTableId(created.getClass());

            // retrieve
            Identifiable toUpdate = this.testRetrieve(created);
            Assert.assertEquals("Objects not equals", created, toUpdate);

            // update
            UserHolder.getUser().setReason("Common CRUD testing: update; for class: " + getClass().getName());
            Identifiable toDelete = this.testModification(tableId, toUpdate, access);

            // delete
            UserHolder.getUser().setReason("Common CRUD testing: delete; for class: " + getClass().getName());
            this.testDeletion(tableId, toDelete, access);
        } catch (CoreException e) {
            fail(e);
        }
    }

    protected Identifiable testCreation(IdentifiableHistoryAccess<IdentifiableHistory> access) throws CoreException {

        // создаем тестируемый объект
        Identifiable identifiable = this.testCreate();
        Assert.assertNotNull(identifiable);

        BigDecimal tableId = this.getTableId(identifiable.getClass());

        Collection<IdentifiableHistory> nowHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(identifiable));

        // Проверяем создание экшенса. Для объектов с ручным выставлением id-шников (зависимых объектов), экшенс будет
        // одинаковый за исключением даты проведения, потому его там надо будет найти. А для остальных объектов, у
        // которых id-шник генерится автоматически - должна быть ровно 1 запись.
        checkHistory(null, nowHstrs, CrudCode.create, manualSetId ? -1 : 0);

        return identifiable;
    }

    protected Identifiable testModification(BigDecimal tableId, Identifiable identifiable,
                                            IdentifiableHistoryAccess<IdentifiableHistory> access) throws CoreException {

        Collection<IdentifiableHistory> preHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(identifiable));
        identifiable = this.testUpdate(identifiable);
        Collection<IdentifiableHistory> nowHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(identifiable));

        // модифицируем объект и проверяем, что добавился ровно 1 экшенс
        checkHistory(preHstrs, nowHstrs, CrudCode.update, 1);

        return identifiable;
    }

    protected void testDeletion(BigDecimal tableId, Identifiable identifiable,
                                IdentifiableHistoryAccess<IdentifiableHistory> access) throws CoreException {

        Collection<IdentifiableHistory> preHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(identifiable));
        this.testDelete(identifiable);
        Collection<IdentifiableHistory> nowHstrs =
                access.getActionsHistoryByIds(tableId, FieldAccess.getSyntheticId(identifiable));

        // удаляем объект и проверяем, что добавился ровно 1 экшенс
        checkHistory(preHstrs, nowHstrs, CrudCode.delete, 1);
    }

    protected void checkHistory(Collection<? extends IdentifiableHistory> preHstrs,
                                Collection<? extends IdentifiableHistory> nowHstrs, CrudCode type, int diff) {

        if (!checkHistory) {
            return;
        }

        try {
            if (!FuncSwitchUtils.getFuncSwitch(FuncSwitchUtils.REGISTER_ACTIONS).isEnabled()) {
                return;
            }
        } catch (GenericSystemException e) {
            fail(e);
        }

        // наличие экшена
        if (diff > 0) {
            // количество экшенсов должно увеличиться на указанное число
            Assert.assertTrue(nowHstrs.removeAll(preHstrs));
            Assert.assertTrue("Size must increase for [" + diff + "]", nowHstrs.size() == diff);
        } else if (diff == 0) {
            // должен быть ровно 1 экшенс
            Assert.assertTrue("Size must equals 1", nowHstrs.size() == 1);
        }

        // наличие профиля
        UserProfile userProfile = UserHolder.getUser();
        Assert.assertNotNull("User profile must not be null", userProfile);

        // проверка корректности типа экшена в профиле
        UserProfile.Action lastAction = userProfile.getLastAction();
        CrudCode actionType = lastAction.getType();
        Assert.assertEquals("Action type must be [" + type + "], but found [" + actionType + "]", type, actionType);

        // ищем экшенс в коллекции по дате операции и он должен быть ровно 1
        IdentifiableHistory history = null;
        if (nowHstrs.size() == 1) {
            history = nowHstrs.iterator().next();
            if (checkActionDate) {
                Assert.assertEquals("Action Date is incorrect", history.getActionDate(), lastAction.getDate());
            }
        } else {
            for (IdentifiableHistory nowHstr : nowHstrs) {
                if (nowHstr.getActionDate().equals(lastAction.getDate())) {
                    if (history == null) {
                        history = nowHstr;
                    } else {
                        Assert.assertNull("Duplicate action found", history);
                    }
                }
            }
        }

        // Нашёлся нужный нам экшен
        Assert.assertNotNull("Action is not registered", history);
        // Id зверька
        Assert.assertEquals("User Id incorrect", userProfile.getSystemUser().getCoreId(), history.getUserId());
        // тип экшена в БД
        Assert.assertEquals("Wrong action type in DB", actionType, history.getType());
        // ризон
        Assert.assertEquals("Action Reason is not correct", lastAction.getReason(), history.getCoreDsc());
    }

}
