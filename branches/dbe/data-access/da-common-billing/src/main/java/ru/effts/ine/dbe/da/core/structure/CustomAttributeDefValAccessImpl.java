/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomAttributeDefVal;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.dbe.da.core.*;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefValAccessImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
@SuppressWarnings({"UnusedDeclaration"})
public class CustomAttributeDefValAccessImpl<T extends CustomAttributeDefVal>
        extends AbstractVersionableAccess<T> implements CustomAttributeDefValAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "CUSTOM_ATTRS_DEF_VALS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ATTR_N = "attr_n";
    public static final String COLUMN_VAL_STRING = "val_string";
    public static final String COLUMN_VAL_LONG = "val_long";
    public static final String COLUMN_VAL_BIGDECIMAL = "val_bigdecimal";
    public static final String COLUMN_VAL_BOOLEAN = "val_boolean";
    public static final String COLUMN_VAL_DATE = "val_date";

    /** Запрос на выборку всех атрибутов */
    public static final String ALL_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ATTR_N +
            ", " + TABLE_PREF + COLUMN_VAL_STRING +
            ", " + TABLE_PREF + COLUMN_VAL_LONG +
            ", " + TABLE_PREF + COLUMN_VAL_BIGDECIMAL +
            ", " + TABLE_PREF + COLUMN_VAL_BOOLEAN +
            ", " + TABLE_PREF + COLUMN_VAL_DATE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.structure.CustomAttributeDefVal} */
    public static final String CACHE_REGION_CUSTOM_ATTRIBUTE_DEF_VALUES = CustomAttributeDefVal.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(CustomAttributeDefVal.CORE_ID, COLUMN_ATTR_N);
        map.put(CustomAttributeDefVal.CORE_DSC, null);
        map.put(CustomAttributeDefVal.STRING, COLUMN_VAL_STRING);
        map.put(CustomAttributeDefVal.LONG, COLUMN_VAL_LONG);
        map.put(CustomAttributeDefVal.BIGDECIMAL, COLUMN_VAL_BIGDECIMAL);
        map.put(CustomAttributeDefVal.BOOLEAN, COLUMN_VAL_BOOLEAN);
        map.put(CustomAttributeDefVal.DATE, COLUMN_VAL_DATE);
        setInstanceFieldsMapping(CustomAttributeDefValAccessImpl.class, map);

        setIdColumns(CustomAttributeDefValAccessImpl.class, new String[]{COLUMN_ATTR_N});

        Set<CRUDHelper> helpers = new LinkedHashSet<CRUDHelper>(2);
        helpers.add(new DefValueHelper());
        registerVersionableHelpers(CustomAttributeDefValAccessImpl.class, helpers);
    }


    @Override
    public String getTableName() {
        return CustomAttributeDefValAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_CUSTOM_ATTRIBUTE_DEF_VALUES;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"})
        T result = (T) IdentifiableFactory.getImplementation(CustomAttributeDefVal.class);

        this.setVersionableFields(resultSet, result, COLUMN_ATTR_N);

        result.setString(this.getString(resultSet, COLUMN_VAL_STRING));
        long l = resultSet.getLong(COLUMN_VAL_LONG);
        result.setLong(resultSet.wasNull() ? null : l);
        result.setBigDecimal(this.getBigDecimal(resultSet, COLUMN_VAL_BIGDECIMAL));
        result.setBoolean(this.getBooleanNullable(resultSet, COLUMN_VAL_BOOLEAN));
        result.setDate(this.getDate(resultSet, COLUMN_VAL_DATE));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_CUSTOM_ATTRIBUTE_DEF_VALUES, ALL_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_CUSTOM_ATTRIBUTE_DEF_VALUES, ALL_DATA_SELECT, id);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_CUSTOM_ATTRIBUTE_DEF_VALUES, ALL_DATA_SELECT, false);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_CUSTOM_ATTRIBUTE_DEF_VALUES);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_CUSTOM_ATTRIBUTE_DEF_VALUES, ALL_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return CustomAttributeDefVal.class;
    }


    public static final class DefValueHelper<T extends CustomAttributeDefVal> extends AbstractCRUDHelper<T> {

        @Override
        public void beforeCreate(T identifiable, IdentifiableAccess<T> access) throws GenericSystemException {
            CustomAttribute attribute = this.getCustomAttribute(identifiable);

            if (identifiable.getValueByType(attribute.getDataType()) == null) {
                throw new IneIllegalArgumentException("Default " + attribute.getDataType() + " value of attribute['"
                        + attribute.getAttributeName() + ", id=" + attribute.getCoreId() + "'] can't be null");
            }
        }

        @Override
        public void beforeUpdate(T identifiable, T newIdentifiable) throws GenericSystemException {
            CustomAttribute attribute = this.getCustomAttribute(newIdentifiable);

            if (newIdentifiable.getValueByType(attribute.getDataType()) == null) {
                throw new IneIllegalArgumentException("Default " + attribute.getDataType() + " value of attribute['"
                        + attribute.getAttributeName() + ", id=" + attribute.getCoreId() + "'] can't be null");
            }
        }

        @SuppressWarnings({"unchecked"})
        @Override
        public void beforeDelete(T identifiable) throws GenericSystemException {
            /**
             * Нужно проверить, что если атрибут обязательный, то не должно быть значений этого атрибута.
             * Иначе удалить дефолтное значение нельзя.
             */
            CustomAttribute attribute = this.getCustomAttribute(identifiable);
            if (attribute.isRequired()) {
                CustomAttributeValueAccess<CustomAttributeValue> valueAccess =
                        (CustomAttributeValueAccess) AccessFactory.getImplementation(CustomAttributeValue.class);

                SearchCriteriaProfile profile = valueAccess.getSearchCriteriaProfile();
                Set<SearchCriterion> search = new LinkedHashSet<SearchCriterion>(2);
                SearchCriterion<BigDecimal> idSearch = profile.getSearchCriterion(
                        CustomAttributeValue.CORE_ID, CriteriaRule.equals, attribute.getCoreId());
                idSearch.addAdditionalParam(FieldAccess.getInterface(attribute.getSystemObjectId()));
                search.add(idSearch);

                if (valueAccess.containsObjects(search)) {
                    throw new IneIllegalArgumentException("Can't delete defaultValue, cause attibute['" +
                            attribute.getAttributeName() + "', id=" + attribute.getCoreId() +
                            "] is required and exist some objects with this attribute");
                }
            }
        }

        @SuppressWarnings({"unchecked"})
        private CustomAttribute getCustomAttribute(T identifiable) throws GenericSystemException {
            CustomAttributeAccess<CustomAttribute> attributeAccess =
                    (CustomAttributeAccess) AccessFactory.getImplementation(CustomAttribute.class);
            try {
                return attributeAccess.getObjectById(identifiable.getCoreId());
            } catch (CoreException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

    }

}
