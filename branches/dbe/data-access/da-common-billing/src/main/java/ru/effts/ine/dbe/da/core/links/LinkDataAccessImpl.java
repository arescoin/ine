/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.links;

import ru.effts.ine.core.*;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.dbe.da.core.*;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.core.structure.PatternUtils;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Реализация по-умолчанию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataAccessImpl.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
@SuppressWarnings({"unchecked", "UnusedDeclaration"})
public class LinkDataAccessImpl<T extends LinkData> extends AbstractVersionableAccess<T> implements LinkDataAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "LINK_DATA";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_LINK_N = "link_n";
    public static final String COLUMN_LEFT_ID = "left_id";
    public static final String COLUMN_RIGHT_ID = "right_id";

    /** Запрос на выборку всех связей */
    public static final String ALL_LINK_DATA_SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID + ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_LINK_N +
            ", " + TABLE_PREF + COLUMN_LEFT_ID +
            ", " + TABLE_PREF + COLUMN_RIGHT_ID +
            ", NULL AS " + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + "  AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.core.links.LinkData} */
    public static final String CACHE_REGION_LINK_DATA = LinkData.class.getName();


    static {
        final Map<String, String> map = new HashMap<String, String>();
        map.put(LinkData.CORE_DSC, null);
        map.put(LinkData.LINK_ID, COLUMN_LINK_N);
        map.put(LinkData.LEFT_ID, COLUMN_LEFT_ID);
        map.put(LinkData.RIGHT_ID, COLUMN_RIGHT_ID);
        setInstanceFieldsMapping(LinkDataAccessImpl.class, map);

        Set<CRUDHelper> linkDataHelpers = new LinkedHashSet<CRUDHelper>(1);
        linkDataHelpers.add(new LinkDataHelper());
        registerVersionableHelpers(LinkDataAccessImpl.class, linkDataHelpers);
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_LINK_DATA;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        T result = (T) IdentifiableFactory.getImplementation(LinkData.class);

        this.setVersionableFields(resultSet, result);
        result.setLinkId(this.getId(resultSet, COLUMN_LINK_N));
        result.setLeftId(this.getId(resultSet, COLUMN_LEFT_ID));
        result.setRightId(this.getId(resultSet, COLUMN_RIGHT_ID));

        return result;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(CACHE_REGION_LINK_DATA, ALL_LINK_DATA_SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(CACHE_REGION_LINK_DATA, ALL_LINK_DATA_SELECT, id);
    }

    @Override
    public <E extends Identifiable> Collection<E> getLinkedObjects(Identifiable objectToLink, BigDecimal linkId)
            throws GenericSystemException {

        if (linkId == null) {
            throw new IneIllegalArgumentException("linkId param is null");
        }

        // Получаем описатель связи, если такого нет - весело падаем.
        Link link;
        try {
            link = this.getLinkAccess().getObjectById(linkId);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
        if (link == null) {
            throw new IneIllegalAccessException(
                    "Incorrect data: specified link description [" + linkId + "] does not exists.");
        }

        // Составной идентификатор объекта и идентификатор типа объекта
        BigDecimal tableId = FieldAccess.getTableId(objectToLink.getClass());
        SyntheticId objectToLinkId = FieldAccess.getSyntheticId(objectToLink);

        Set<SearchCriterion> linkDataSearch;
        IdentifiableAccess<E> access;
        StorageFilter[] searchFilters = new StorageFilter[2];

        Collection<E> result = new HashSet<E>();
        // Сначала ищем по левой стороне и вытаскиваем объекты справа
        if (link.getLeftType().getTableId().equals(tableId)) {
            //формируем поисковое условие по левому объекту на equals
            linkDataSearch = this.getSearchCriteriaForLinkId(linkId);
            linkDataSearch.add(this.getSearchCriteriaProfile().getSearchCriterion(T.LEFT_ID,
                    CriteriaRule.equals, objectToLinkId.getIdValue(link.getLeftType().getFinalId())));
            searchFilters[0] = this.createStorageFilter(linkDataSearch, T.RIGHT_ID);

            //формируем поисковое условие на объекты по правому паттерну
            access = this.getAccess(link.getRightType().getTableId());
            searchFilters[1] = this.createObjectFilter(access, link.getRightType());

            result.addAll(access.searchObjects(searchFilters));
        }

        // Потом ищем по правой стороне и вытаскиваем объекты слева
        if (link.getRightType().getTableId().equals(tableId)) {
            //формируем поисковое условие на правому объекту на equals
            linkDataSearch = this.getSearchCriteriaForLinkId(linkId);
            linkDataSearch.add(this.getSearchCriteriaProfile().getSearchCriterion(T.RIGHT_ID,
                    CriteriaRule.equals, objectToLinkId.getIdValue(link.getRightType().getFinalId())));
            searchFilters[0] = this.createStorageFilter(linkDataSearch, T.LEFT_ID);

            //формируем поисковое условие на объекты по левому паттерну
            access = this.getAccess(link.getLeftType().getTableId());
            searchFilters[1] = this.createObjectFilter(access, link.getLeftType());

            result.addAll(access.searchObjects(searchFilters));
        }

        return result;
    }

    private Set<SearchCriterion> getSearchCriteriaForLinkId(BigDecimal linkId) throws GenericSystemException {
        SearchCriteriaProfile profile = this.getSearchCriteriaProfile();
        Set<SearchCriterion> criteria = new HashSet<SearchCriterion>();
        criteria.add(profile.getSearchCriterion(T.LINK_ID, CriteriaRule.equals, linkId));

        return criteria;
    }

    private IdentifiableAccess getAccess(BigDecimal tableId) throws GenericSystemException {
        try {
            String clazzName = FieldAccess.getInterface(tableId);
            return (IdentifiableAccess) AccessFactory.getImplementation(Class.forName(clazzName));
        } catch (ClassNotFoundException e) {
            throw new GenericSystemException(e.getMessage(), e);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    private StorageFilter createObjectFilter(IdentifiableAccess access, SystemObjectPattern pattern)
            throws GenericSystemException {
        try {
            SearchCriteriaProfile objectProfile = access.getSearchCriteriaProfile();

            Map<String, String> fieldsMap = PatternUtils.getInvertedFieldsMap(access);

            String columnName = PatternUtils.getAccess().getObjectById(pattern.getFinalId()).getName();
            SearchCriterion objectCriterion = objectProfile.getSearchCriterion(fieldsMap.get(columnName));
            objectCriterion.setCriterionRule(CriteriaRule.in);

            Set<SearchCriterion> objectSearch = PatternUtils.getSearchCriteria(pattern, objectProfile, fieldsMap);

            return access.createStorageFilter(objectSearch, objectCriterion, "");
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    @Override
    public Collection<T> getLinkDataByObject(Identifiable linkedObject) throws GenericSystemException {
        // получаем все типы связей, в которых может участвовать данный объект
        Collection<Link> links = this.getLinkAccess().getLinksByObjType(linkedObject.getClass());

        return this.getLinkDataByObjectAndLinks(linkedObject, links);
    }

    @Override
    public Collection<T> getLinkDataByObject(Identifiable linkedObject, BigDecimal linkId)
            throws GenericSystemException {
        try {
            Collection<Link> links = new ArrayList<Link>(1);
            links.add(this.getLinkAccess().getObjectById(linkId));

            return this.getLinkDataByObjectAndLinks(linkedObject, links);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    private Collection<T> getLinkDataByObjectAndLinks(Identifiable linkedObject, Collection<Link> links)
            throws GenericSystemException {
        if (linkedObject == null) {
            throw new IneIllegalArgumentException("linkedObject param can't be null.");
        }

        // Составной идентификатор объекта и идентификатор типа объекта
        BigDecimal tableId = FieldAccess.getTableId(linkedObject.getClass());
        SyntheticId linkedObjectId = FieldAccess.getSyntheticId(linkedObject);

        SearchCriteriaProfile profile = this.getSearchCriteriaProfile();

        SearchCriterion leftLinks = profile.getSearchCriterion(T.LINK_ID);
        leftLinks.setCriterionRule(CriteriaRule.in);
        SearchCriterion leftObj = profile.getSearchCriterion(T.LEFT_ID);
        leftObj.setCriterionRule(CriteriaRule.equals);

        SearchCriterion rightLinks = profile.getSearchCriterion(T.LINK_ID);
        rightLinks.setCriterionRule(CriteriaRule.in);
        SearchCriterion rightObj = profile.getSearchCriterion(T.RIGHT_ID);
        rightObj.setCriterionRule(CriteriaRule.equals);

        Collection<T> result = new HashSet<T>();
        for (Link link : links) {
            if (link.getLeftType().getTableId().equals(tableId)) {
                leftLinks.addCriteriaVal(link.getCoreId());
                if (leftObj.getCriterionVals().length < 1) {
                    leftObj.addCriteriaVal(linkedObjectId.getIdValue(link.getLeftType().getFinalId()));
                }
            }

            if (link.getRightType().getTableId().equals(tableId)) {
                rightLinks.addCriteriaVal(link.getCoreId());
                if (rightObj.getCriterionVals().length < 1) {
                    rightObj.addCriteriaVal(linkedObjectId.getIdValue(link.getRightType().getFinalId()));
                }
            }
        }

        if (leftLinks.getCriterionVals().length > 0) {
            Set<SearchCriterion> leftSearch = new LinkedHashSet<SearchCriterion>();
            leftSearch.add(leftObj);
            leftSearch.add(leftLinks);
            result.addAll(this.searchObjects(leftSearch));
        }

        if (rightLinks.getCriterionVals().length > 0) {
            Set<SearchCriterion> rightSearch = new LinkedHashSet<SearchCriterion>();
            rightSearch.add(rightObj);
            rightSearch.add(rightLinks);
            result.addAll(this.searchObjects(rightSearch));
        }

        return result;
    }

    private LinkAccess<Link> getLinkAccess() throws GenericSystemException {
        return (LinkAccess<Link>) AccessFactory.getImplementation(Link.class);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, CACHE_REGION_LINK_DATA, ALL_LINK_DATA_SELECT, true);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {

        return this.updateObject(identifiable, newIdentifiable, CACHE_REGION_LINK_DATA);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, CACHE_REGION_LINK_DATA, ALL_LINK_DATA_SELECT);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return LinkData.class;
    }


    public final static class LinkDataHelper<E extends LinkData> extends AbstractCRUDHelper<E> {

        private LinkAccess<Link> getLinkAccess() throws GenericSystemException {
            return (LinkAccess<Link>) AccessFactory.getImplementation(Link.class);
        }

        private LinkDataAccess<E> getDataAccess() throws GenericSystemException {
            return (LinkDataAccess<E>) AccessFactory.getImplementation(LinkData.class);
        }

        @Override
        public void beforeCreate(E identifiable, IdentifiableAccess<E> access) throws GenericSystemException {
            this.checkLinkedObjects(identifiable);
        }

        @Override
        public void beforeUpdate(E identifiable, E newIdentifiable) throws GenericSystemException {
            this.checkLinkedObjects(newIdentifiable);
        }

        private void checkDuplicates(E identifiable, boolean update) throws GenericSystemException {
            SearchCriteriaProfile profile = this.getDataAccess().getSearchCriteriaProfile();

            Set<SearchCriterion> criteria = new LinkedHashSet<SearchCriterion>(4);
            criteria.add(profile.getSearchCriterion(E.LINK_ID, CriteriaRule.equals, identifiable.getLinkId()));
            criteria.add(profile.getSearchCriterion(E.LEFT_ID, CriteriaRule.equals, identifiable.getLeftId()));
            criteria.add(profile.getSearchCriterion(E.RIGHT_ID, CriteriaRule.equals, identifiable.getRightId()));
            if (update) {
                SearchCriterion coreId =
                        profile.getSearchCriterion(E.CORE_ID, CriteriaRule.equals, identifiable.getCoreId());
                coreId.setInvert(true);
                criteria.add(coreId);
            }

            if (this.getDataAccess().containsObjects(criteria)) {
                throw new IneIllegalArgumentException("There are exists linkData with same attributes: " +
                        "linkId[" + identifiable.getLinkId() + "], " +
                        "leftId[" + identifiable.getLeftId() + "], " +
                        "rightId[" + identifiable.getRightId() + "]");
            }
        }

        private void checkLinkedObjects(E identifiable) throws GenericSystemException {
            try {
                Link link = this.getLinkAccess().getObjectById(identifiable.getLinkId());

                if (PatternUtils.getObjectByPattern(link.getLeftType(), identifiable.getLeftId()) == null) {
                    throw new IneIllegalArgumentException("Can't find object " +
                            FieldAccess.getInterface(link.getLeftType().getTableId()) +
                            ", id = " + identifiable.getLeftId());
                }

                if (PatternUtils.getObjectByPattern(link.getRightType(), identifiable.getRightId()) == null) {
                    throw new IneIllegalArgumentException("Can't find object " +
                            FieldAccess.getInterface(link.getRightType().getTableId()) +
                            ", id = " + identifiable.getRightId());
                }
            } catch (CoreException e) {
                throw GenericSystemException.toGeneric(e);
            }
        }

    }

}
