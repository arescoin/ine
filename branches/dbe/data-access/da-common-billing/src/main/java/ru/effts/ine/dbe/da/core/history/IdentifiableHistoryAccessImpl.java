/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.history;

import ru.effts.ine.core.*;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.core.userpermits.CrudCode;
import ru.effts.ine.dbe.da.core.AbstractIdentifiableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Предоставляет методы для работы с историей изменений идентифицируемых объектов на стороне БД
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableHistoryAccessImpl.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public class IdentifiableHistoryAccessImpl<T extends IdentifiableHistory>
        extends AbstractIdentifiableAccess<T> implements IdentifiableHistoryAccess<T> {

    public static final String TABLE = "OBJ_HISTORY";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Колонки таблицы */
    public static final String COLUMN_ENT_ID = "ent_id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_ACTION_DATE = "action_date";
    public static final String COLUMN_ACTION_TYP = "typ";
    public static final String COLUMN_REASON = "reason";


    public static final String SELECT_ALL_HISTORY = "SELECT " + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ENT_ID +
            ", " + TABLE_PREF + COLUMN_USER_ID +
            ", " + TABLE_PREF + COLUMN_ACTION_DATE +
            ", " + TABLE_PREF + COLUMN_ACTION_TYP +
            ", " + TABLE_PREF + COLUMN_REASON +
            " FROM " + TABLE;

    public static final String SELECT_HISTORY_BY_IDS = SELECT_ALL_HISTORY +
            " WHERE " + TABLE_PREF + COLUMN_ID + " = ? AND " + TABLE_PREF + COLUMN_ENT_ID + " = ?";

    public static final String SELECT_HISTORY_LIKE = SELECT_ALL_HISTORY +
            " WHERE " + TABLE_PREF + COLUMN_ID + " = ? AND " + TABLE_PREF + COLUMN_ENT_ID + " like ?";

    static {
        setInstanceFieldsMapping(IdentifiableHistoryAccessImpl.class, new HashMap<String, String>());

        registerIdentifiableHistoryHelpers(IdentifiableHistoryAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void registerIdentifiableHistoryHelpers(
            Class<? extends IdentifiableHistoryAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<CRUDHelper>();
        helpersAll.addAll(helpers);

        AbstractIdentifiableAccess.registerIdentifiableHelpers(access, helpersAll);
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(IdentifiableHistory.ENTITY_ID, COLUMN_ENT_ID);
        map.put(IdentifiableHistory.USER_ID, COLUMN_USER_ID);
        map.put(IdentifiableHistory.ACTION_DATE, COLUMN_ACTION_DATE);
        map.put(IdentifiableHistory.CORE_DSC, COLUMN_REASON);
        map.put(IdentifiableHistory.ACTION_TYPE, COLUMN_ACTION_TYP);
        map.putAll(instanceFields);

        AbstractIdentifiableAccess.setInstanceFieldsMapping(aClass, map);
    }

    public Collection<T> getActionsHistoryByIds(BigDecimal sysObjId, SyntheticId entityId)
            throws CoreException, RuntimeCoreException {

        String selectedSQL;

        if (entityId.isSearchId()) {
            selectedSQL = SELECT_HISTORY_LIKE;
        } else {
            selectedSQL = SELECT_HISTORY_BY_IDS;
        }

        return this.getObjects(false, selectedSQL, sysObjId, entityId.toString());
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getObjects(false, SELECT_ALL_HISTORY);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        throw new UnsupportedOperationException("Method getObjectById does not supported for class " +
                getClass().getName() + ". Use getActionsHistoryByIds method.");
    }

    @Override
    public String getTableName() {
        return IdentifiableHistoryAccessImpl.TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return "";
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"})
        T result = (T) IdentifiableFactory.getImplementation(IdentifiableHistory.class);

        setBaseFields(resultSet, result);

        return result;
    }

    protected void setBaseFields(ResultSet resultSet, T obj) throws SQLException, CoreException, RuntimeCoreException {

        String entityId = this.getString(resultSet, COLUMN_ENT_ID);
        this.wasNull(COLUMN_ENT_ID, resultSet);

        BigDecimal userId = this.getId(resultSet, COLUMN_USER_ID);
        this.wasNull(COLUMN_USER_ID, resultSet);

        Date actionDate = this.getDate(resultSet, COLUMN_ACTION_DATE);
        this.wasNull(COLUMN_ACTION_DATE, resultSet);

        BigDecimal typeCode = this.getBigDecimal(resultSet, COLUMN_ACTION_TYP);
        this.wasNull(COLUMN_ACTION_TYP, resultSet);

        this.setIdentifiableFields(resultSet, obj, COLUMN_ID, COLUMN_REASON);

        obj.setEntityId(SyntheticId.parseString(entityId));
        obj.setUserId(userId);
        obj.setActionDate(actionDate);
        obj.setType(CrudCode.getCrudCodeByIntVal(typeCode.intValue()));
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return IdentifiableHistory.class;
    }

    @Override
    public boolean containsObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return false;
    }

    @Override
    public boolean containsObjects(StorageFilter[] filters) throws GenericSystemException {
        return false;
    }

    @Override
    public Collection searchObjects(StorageFilter[] filters) throws GenericSystemException {
        return Collections.emptyList();
    }

    @Override
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return Collections.emptyList();
    }

    @Override
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {
        return Collections.emptyList();
    }

    @Override
    public int getObjectCount(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return 0;
    }

    @Override
    public int getObjectCount(StorageFilter[] filters) throws GenericSystemException {
        return 0;
    }
}
