package ru.effts.ine.dbe.da;


import org.postgresql.ds.PGPoolingDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PostgresPoolManager.java 3797 2014-05-26 14:22:48Z DGomon $"
 */
public class PostgresPoolManager implements DSDelegate {

    private PGPoolingDataSource dataSource;

    /**
     * Инициализация пула соединений с БД. Для работы с пулом необходимо подлкючить драйвер БД, который будет
     * использоваться для создания соединения.
     * <p/>
     * //     * @param driver        экземпляр класса драйвера соединения с БД
     *
     * @param host     адрес сервера БД
     * @param port     порт сервера БД
     * @param db       имя базы
     * @param user     пользователь БД
     * @param password пароль пользователя БД
     * @throws java.sql.SQLException в случае ошибки при регистрации драйвера соединения с БД
     */
    public void init(String host, String port, String db, String user, String password) throws SQLException {
        this.dataSource = new PGPoolingDataSource();
        this.dataSource.setServerName(host);
        this.dataSource.setPortNumber(Integer.parseInt(port));
        this.dataSource.setDatabaseName(db);
        this.dataSource.setUser(user);
        this.dataSource.setPassword(password);

//        final String connectString = "jdbc:postgres:thin:@" + host + ":" + port + ":" + db;

        this.dataSource.setMaxConnections(10);
    }

    /**
     * Получает соединение из пула.
     *
     * @return соединение из пула
     * @throws SQLException ошибка получения соединения с БД
     */
    public Connection getConnection() throws SQLException {
        return this.dataSource.getConnection();
    }

}
