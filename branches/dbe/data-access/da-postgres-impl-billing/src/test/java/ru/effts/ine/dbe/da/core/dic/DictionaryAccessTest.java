/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.dic;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.dic.Dictionary;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class DictionaryAccessTest extends BaseVersionableTest {

    private static DictionaryAccess<Dictionary> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (DictionaryAccess<Dictionary>) AccessFactory.getImplementation(Dictionary.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllDictionaries() {
        try {
            Collection result = access.getAllObjects();
            logger.info("Dic's:" + result.size());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            Dictionary obj1 = IdentifiableFactory.getImplementation(Dictionary.class);
            obj1.setCoreDsc("Test Dic 1");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setDicType(BigDecimal.ONE);
            obj1.setEntryCodePolicy(BigDecimal.ONE);
            obj1.setLocalizationPolicy(BigDecimal.ONE);
            obj1.setName("TST_DIC_JU 1");

            obj1 = access.createObject(obj1);
            Dictionary obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getDicType(), obj2.getDicType());
            Assert.assertEquals("Failed to save object in DB.", obj1.getEntryCodePolicy(), obj2.getEntryCodePolicy());
            Assert.assertEquals("Failed to save object in DB.",
                    obj1.getLocalizationPolicy(), obj2.getLocalizationPolicy());
            Assert.assertEquals("Failed to save object in DB.", obj1.getName(), obj2.getName());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Dictionary oldOne = (Dictionary) identifiable;
            Dictionary newOne = IdentifiableFactory.getImplementation(Dictionary.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(FieldAccess.getSystemTd());
            newOne.setCoreDsc(oldOne.getCoreDsc() + "_next");
            newOne.setName(oldOne.getName() + "_NEXT");
            newOne.setParentId(oldOne.getParentId());
            newOne.setDicType(oldOne.getDicType());
            newOne.setEntryCodePolicy(oldOne.getEntryCodePolicy());
            newOne.setLocalizationPolicy(oldOne.getLocalizationPolicy());

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(oldOne.getCoreId());

            Assert.assertEquals("Failed to update object in DB.", oldOne.getName(), newOne.getName());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Dictionary) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }
}
