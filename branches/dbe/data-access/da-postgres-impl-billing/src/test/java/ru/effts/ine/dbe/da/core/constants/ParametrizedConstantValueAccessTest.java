package ru.effts.ine.dbe.da.core.constants;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ParametrizedConstant;
import ru.effts.ine.core.constants.ParametrizedConstantValue;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.dic.DictionaryEntryAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.core.structure.PatternUtils;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValueAccessTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ParametrizedConstantValueAccessTest extends BaseVersionableTest {

    private static ParametrizedConstantValueAccess<ParametrizedConstantValue> access;
    private static BigDecimal CONST_ID = new BigDecimal(2);
    private static BigDecimal PARAM = new BigDecimal(1);

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (ParametrizedConstantValueAccess<ParametrizedConstantValue>)
                    AccessFactory.getImplementation(ParametrizedConstantValue.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public Identifiable testCreate() {
        try {

            ParametrizedConstant obj02 = IdentifiableFactory.getImplementation(ParametrizedConstant.class);

            obj02.setCoreDsc("TestObject for CRUD-operations testing");
            obj02.setCoreFd(FieldAccess.getSystemFd());
            obj02.setCoreTd(FieldAccess.getSystemTd());
            obj02.setName("TEST_PARAMETRIZED_CONSTANT");
            obj02.setModifiable(true);
            obj02.setNullable(true);
//            obj02.setType(BigDecimal.ONE);
            obj02.setDefaultValue("1");
            obj02.setMask(new SystemObjectPattern(FieldAccess.getColumnId("TEST_TABLE", "n")));

            obj02 = ((ParametrizedConstantAccess<ParametrizedConstant>)
                    AccessFactory.getImplementation(ParametrizedConstant.class)).createObject(obj02);

            ParametrizedConstantValue obj1 = IdentifiableFactory.getImplementation(ParametrizedConstantValue.class);

            obj1.setCoreId(obj02.getCoreId());
            obj1.setParam(PARAM);
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setValue("TEST_PARAMETRIZED_CONSTANT_VALUE");

            obj1 = access.createObject(obj1);
            ParametrizedConstantValue obj2 = access.getObjectById(access.getSyntheticId(obj1).getIdValues());

            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getValue(), obj2.getValue());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(access.getSyntheticId((ParametrizedConstantValue) identifiable).getIdValues());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            ParametrizedConstantValue oldOne = (ParametrizedConstantValue) identifiable;
            ParametrizedConstantValue newOne = IdentifiableFactory.getImplementation(ParametrizedConstantValue.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setParam(oldOne.getParam());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());
            newOne.setValue(oldOne.getValue() + "_UPD");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(access.getSyntheticId(oldOne).getIdValues());

            Assert.assertEquals("Failed to modify object on DB.", oldOne.getValue(), newOne.getValue());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((ParametrizedConstantValue) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getParametrizedParametrizedConstantsValues() {
        try {
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + access.getAllObjects());
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @SuppressWarnings({"unchecked"})
    @Test
    public void getParametrizedConstantValue() {
        try {
            ParametrizedConstant constant = ((ParametrizedConstantAccess<ParametrizedConstant>)
                    AccessFactory.getImplementation(ParametrizedConstant.class)).getObjectById(CONST_ID);

            Identifiable entryByConstant = PatternUtils.getObjectByPattern(constant.getMask(), PARAM);
            DictionaryEntry entryByAccess = ((DictionaryEntryAccess<DictionaryEntry>)
                    AccessFactory.getImplementation(DictionaryEntry.class)).getObjectById(new BigDecimal(5), PARAM);

            Assert.assertEquals("Failed to obtain object by constValue", entryByAccess, entryByConstant);
        } catch (Exception e) {
            fail(e);
        }
    }
}
