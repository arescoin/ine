/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.history;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.history.HistoryEvent;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.utils.BaseTest;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestHistoryEventAccess.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class TestHistoryEventAccess extends BaseTest {

    @Test
    public void testAllProcessed() {
        try {
            @SuppressWarnings({"unchecked"}) HistoryEventAccess<HistoryEvent> access =
                    (HistoryEventAccess<HistoryEvent>) AccessFactory.getImplementation(HistoryEvent.class);

            int totalProcessed = 0;
            Collection<HistoryEvent> eventsForProcess = access.getEventsForProcess();
            while (eventsForProcess.size() > 0) {
                System.out.println("events to proceed: " + eventsForProcess.size());
                for (HistoryEvent historyEvent : eventsForProcess) {
                    access.markAsProcessed(historyEvent);
                }
                totalProcessed += eventsForProcess.size();
                eventsForProcess = access.getEventsForProcess();
            }
            System.out.println("total events processed: " + totalProcessed);

            Collection<HistoryEvent> processed = access.getProcessedEvents();
            System.out.println("all processed events in DB: " + processed.size());

            Assert.assertTrue("There are more events for processing", access.getEventsForProcess().isEmpty());
        } catch (CoreException e) {
            fail(e);
        }
    }
}
