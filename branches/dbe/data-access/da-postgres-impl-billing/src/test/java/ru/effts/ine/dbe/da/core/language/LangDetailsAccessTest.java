/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.language;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.core.language.LangDetails;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.constants.ConstantValueAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseIdentifiableTest;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangDetailsAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class LangDetailsAccessTest extends BaseIdentifiableTest {

    private static LangDetailsAccess<LangDetails> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (LangDetailsAccess<LangDetails>) AccessFactory.getImplementation(LangDetails.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public void testCRUD() {
        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4L);
        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");
            super.testCRUD();
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            @SuppressWarnings({"unchecked"}) ConstantValueAccess<ConstantValue> constantValueAccess =
                    (ConstantValueAccess<ConstantValue>) AccessFactory.getImplementation(ConstantValue.class);
            ConstantValue constantValue = constantValueAccess.getObjectById(constantId);
            constantValue.setValue(value);
            constantValue.setCoreFd(FieldAccess.getCurrentDate());
            constantValueAccess.updateObject(constantValueAccess.getObjectById(constantId), constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            LangDetails obj1 = IdentifiableFactory.getImplementation(LangDetails.class);

            obj1.setCoreId(new BigDecimal(9999));
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setName("LANG NAME");
            obj1.setCode("zz");

            obj1 = access.createObject(obj1);
            LangDetails obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", null, obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getName(), obj2.getName());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCode(), obj2.getCode());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            LangDetails obj1 = (LangDetails) identifiable;
            LangDetails obj2 = IdentifiableFactory.getImplementation(LangDetails.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setName(obj1.getName() + " UPD");
            obj2.setCode(obj1.getCode());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to update object on DB.", obj1.getName(), obj2.getName());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((LangDetails) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testLangDetails() {
        try {
            for (LangDetails details : access.getAllObjects()) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + details);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testSCP() {
        try {
            SearchCriteriaProfile searchCriteriaProfile = access.getSearchCriteriaProfile();
            @SuppressWarnings({"unchecked"}) SearchCriterion<String> searchCriterionCode =
                    (SearchCriterion<String>) searchCriteriaProfile.getSearchCriterion(LangDetails.CODE);
            searchCriterionCode.setCriterionRule(CriteriaRule.contains);
            searchCriterionCode.addCriteriaVal("a");
            HashSet<SearchCriterion> searchCriterias = new HashSet<SearchCriterion>();
            searchCriterias.add(searchCriterionCode);
            long start = System.currentTimeMillis();
            Collection collection = access.searchObjects(searchCriterias);
            long end = System.currentTimeMillis();
            System.out.println("res: size [" + collection.size() + "]; time[" + (end - start) + "] ...");
        } catch (GenericSystemException e) {
            e.printStackTrace();
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }
}
