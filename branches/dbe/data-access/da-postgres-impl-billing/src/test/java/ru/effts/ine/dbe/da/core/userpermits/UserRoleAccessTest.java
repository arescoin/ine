package ru.effts.ine.dbe.da.core.userpermits;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.core.history.SearchId;
import ru.effts.ine.core.history.VersionableHistory;
import ru.effts.ine.core.userpermits.UserRole;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.history.VersionableHistoryAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserRoleAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class UserRoleAccessTest extends BaseVersionableTest {

    private static UserRoleAccess<UserRole> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (UserRoleAccess<UserRole>) AccessFactory.getImplementation(UserRole.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public Identifiable testCreate() {
        try {
            UserRole obj1 = IdentifiableFactory.getImplementation(UserRole.class);
            obj1.setCoreId(new BigDecimal(2)); // тестовая роль NOBODY
            obj1.setUserId(new BigDecimal(2)); // тестовый юзверь Test User
            obj1.setActive(false);
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            obj1 = access.createObject(obj1);
            UserRole obj2 = access.getObjectById(access.getSyntheticId(obj1).getIdValues());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreId(), obj2.getCoreId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getUserId(), obj2.getUserId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.isActive(), obj2.isActive());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(access.getSyntheticId((UserRole) identifiable).getIdValues());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            UserRole oldOne = (UserRole) identifiable;
            UserRole newOne = IdentifiableFactory.getImplementation(UserRole.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setUserId(oldOne.getUserId());
            newOne.setActive(!oldOne.isActive());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(access.getSyntheticId(oldOne).getIdValues());

            Assert.assertEquals("Failed to save update object in DB", oldOne.isActive(), newOne.isActive());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((UserRole) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllUserRoles() {
        try {
            Collection result = access.getAllObjects();
            for (Object o : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + o);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void tesActionsHistoryByIds() {
        try {
            Collection<UserRole> result = access.getAllObjects();
            UserRole userRole = result.iterator().next();
            SyntheticId syntheticId = access.getSyntheticId(userRole);

            VersionableHistoryAccess historyAccess =
                    (VersionableHistoryAccess) AccessFactory.getImplementation(VersionableHistory.class);

            BigDecimal entId = FieldAccess.getColumnId(
                    access.getTableName(),
                    access.getInstanceFields().get(UserRole.CORE_ID));

            BigDecimal userId = FieldAccess.getColumnId(
                    access.getTableName(),
                    access.getInstanceFields().get(UserRole.USER_ID));

            SearchId searchId = syntheticId.getSearchId();
            searchId.setSearchEntry(entId);
            searchId.setSearchEntry(userId);

            System.out.println("" +
                    historyAccess.getActionsHistoryByIds(FieldAccess.getTableId(UserRole.class), searchId).size());
        } catch (Exception e) {
            fail(e);
        }
    }
}
