package ru.effts.ine.dbe.da.core.structure;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomAttributeValue;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.test.TestObjectAccess;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: AttributesAccessTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
@SuppressWarnings({"unchecked"})
@Ignore
public class AttributesAccessTest extends BaseTest {

    private static TestObject testObject;
    private static CustomAttributeAccess<CustomAttribute> attributeAccess;

    @BeforeClass
    public static void initTest() {
        try {

            IdentifiableFactory.getImplementation(TestObject.class);

            testObject = ((TestObjectAccess<TestObject>) AccessFactory.getImplementation(TestObject.class)).
                    getObjectById(new BigDecimal(4));
            attributeAccess = (CustomAttributeAccess) AccessFactory.getImplementation(CustomAttribute.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    //    @Test
    public void testInOrder() throws Exception {
        testUpdateNonExistentValue();
        testCreate();
        testUpdate();
        testPartialDeleteWithError();
        testPartialDelete();
        testCreateWithNullValue();
        testCreateWithNotAllRequiredAttributes();
        testCreateWithWrongEntityId();
        testCreateWithWrongCoreId();
        testDelete();
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateWithNullValue() {
        try {
            CustomAttributeValue value = IdentifiableFactory.getImplementation(CustomAttributeValue.class);

            value.setEntityId(testObject.getCoreId());
            value.setCoreId(new BigDecimal(1));
            value.setCoreFd(FieldAccess.getSystemFd());
            value.setCoreTd(FieldAccess.getSystemTd());
            value.setValue("TestString create");

            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);
            values.add(value);
            values.add(null);

            AttributesAccess.createValues(testObject, null, values);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateWithNotAllRequiredAttributes() {
        try {
            CustomAttributeValue value = IdentifiableFactory.getImplementation(CustomAttributeValue.class);

            value.setEntityId(testObject.getCoreId());
            value.setCoreId(new BigDecimal(1));
            value.setCoreFd(FieldAccess.getSystemFd());
            value.setCoreTd(FieldAccess.getSystemTd());
            value.setValue("TestString create");

            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);
            values.add(value);

            AttributesAccess.createValues(testObject, null, values);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateWithWrongEntityId() {
        try {
            CustomAttributeValue value = IdentifiableFactory.getImplementation(CustomAttributeValue.class);

            value.setEntityId(testObject.getCoreId().add(testObject.getCoreId()));
            value.setCoreId(new BigDecimal(1));
            value.setCoreFd(FieldAccess.getSystemFd());
            value.setCoreTd(FieldAccess.getSystemTd());
            value.setValue("TestString create");

            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);
            values.add(value);

            AttributesAccess.createValues(testObject, null, values);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateWithWrongCoreId() {
        try {
            CustomAttributeValue value = IdentifiableFactory.getImplementation(CustomAttributeValue.class);

            value.setEntityId(testObject.getCoreId());
            value.setCoreId(new BigDecimal(6));
            value.setCoreFd(FieldAccess.getSystemFd());
            value.setCoreTd(FieldAccess.getSystemTd());
            value.setValue("TestString create");

            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);
            values.add(value);

            AttributesAccess.createValues(testObject, null, values);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testCreate() {
        try {
            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);

            CustomAttribute stringAttr = attributeAccess.getObjectById(new BigDecimal(1));
            CustomAttributeValue stringVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            stringVal.setEntityId(testObject.getCoreId());
            stringVal.setCoreId(stringAttr.getCoreId());
            stringVal.setCoreFd(FieldAccess.getSystemFd());
            stringVal.setCoreTd(FieldAccess.getSystemTd());
            stringVal.setValue("TestString create");
            values.add(stringVal);

            CustomAttribute longAttr = attributeAccess.getObjectById(new BigDecimal(2));
            CustomAttributeValue longVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            longVal.setEntityId(testObject.getCoreId());
            longVal.setCoreId(longAttr.getCoreId());
            longVal.setCoreFd(FieldAccess.getSystemFd());
            longVal.setCoreTd(FieldAccess.getSystemTd());
            longVal.setValue((long) 1);
            values.add(longVal);

            CustomAttribute decimalAttr = attributeAccess.getObjectById(new BigDecimal(3));
            CustomAttributeValue decimalVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            decimalVal.setEntityId(testObject.getCoreId());
            decimalVal.setCoreId(decimalAttr.getCoreId());
            decimalVal.setCoreFd(FieldAccess.getSystemFd());
            decimalVal.setCoreTd(FieldAccess.getSystemTd());
            decimalVal.setValue(new BigDecimal(1));
            values.add(decimalVal);

            CustomAttribute dateAttr = attributeAccess.getObjectById(new BigDecimal(4));
            CustomAttributeValue dateVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            dateVal.setEntityId(testObject.getCoreId());
            dateVal.setCoreId(dateAttr.getCoreId());
            dateVal.setCoreFd(FieldAccess.getSystemFd());
            dateVal.setCoreTd(FieldAccess.getSystemTd());
            dateVal.setValue(new Date());
            values.add(dateVal);

            AttributesAccess.createValues(testObject, null, values);

            Map<CustomAttribute, CustomAttributeValue> savedValues = AttributesAccess.getValues(testObject, null);

            Assert.assertEquals("Failed to save object in DB.", savedValues.get(stringAttr), stringVal);
            Assert.assertEquals("Failed to save object in DB.", savedValues.get(longAttr), longVal);
            Assert.assertEquals("Failed to save object in DB.", savedValues.get(decimalAttr), decimalVal);
            Assert.assertEquals("Failed to save object in DB.", savedValues.get(dateAttr), dateVal);

            testUpdate();
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testUpdateNonExistentValue() {
        try {
            final Date curDate = FieldAccess.getCurrentDate();

            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);

            CustomAttribute stringAttr = attributeAccess.getObjectById(new BigDecimal(1));
            CustomAttributeValue stringVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            stringVal.setCoreId(stringAttr.getCoreId());
            stringVal.setEntityId(testObject.getCoreId());
            stringVal.setCoreFd(curDate);
            stringVal.setCoreTd(FieldAccess.getSystemTd());
            stringVal.setValue("TestString update");
            values.add(stringVal);

            CustomAttribute booleanAttr = attributeAccess.getObjectById(new BigDecimal(5));
            CustomAttributeValue booleanVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            booleanVal.setCoreId(booleanAttr.getCoreId());
            booleanVal.setEntityId(testObject.getCoreId());
            booleanVal.setCoreFd(curDate);
            booleanVal.setCoreTd(FieldAccess.getSystemTd());
            booleanVal.setValue(true);
            values.add(booleanVal);

            AttributesAccess.updateValues(testObject, null, values);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

//    @Test
    public void testUpdate() {
        try {
            final Date curDate = FieldAccess.getCurrentDate();

            Map<CustomAttribute, CustomAttributeValue> savedValues = AttributesAccess.getValues(testObject, null);
            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);

            CustomAttribute stringAttr = attributeAccess.getObjectById(new BigDecimal(1));
            CustomAttributeValue stringVal = savedValues.get(stringAttr);
            stringVal.setCoreFd(curDate);
            stringVal.setValue("TestString update");
            values.add(stringVal);

            CustomAttribute dateAttr = attributeAccess.getObjectById(new BigDecimal(4));
            CustomAttributeValue dateVal = savedValues.get(dateAttr);
            dateVal.setCoreFd(curDate);
            dateVal.setValue(new Date());
            values.add(dateVal);

            AttributesAccess.updateValues(testObject, null, values);

            savedValues = AttributesAccess.getValues(testObject, null);

            Assert.assertEquals("Failed to save object in DB.", savedValues.get(stringAttr), stringVal);
            Assert.assertEquals("Failed to save object in DB.", savedValues.get(dateAttr), dateVal);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testPartialDeleteWithError() {
        try {
            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(2);

            CustomAttribute stringAttr = attributeAccess.getObjectById(new BigDecimal(1));
            CustomAttributeValue stringVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            stringVal.setCoreId(stringAttr.getCoreId());
            stringVal.setEntityId(testObject.getCoreId());
            stringVal.setCoreFd(FieldAccess.getSystemFd());
            stringVal.setCoreTd(FieldAccess.getSystemTd());
            stringVal.setValue("TestString update");
            values.add(stringVal);

            AttributesAccess.deleteValues(testObject, null, values);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testPartialDelete() {
        try {
            Collection<CustomAttributeValue> values = new ArrayList<CustomAttributeValue>(1);

            CustomAttribute booleanAttr = attributeAccess.getObjectById(new BigDecimal(5));
            CustomAttributeValue booleanVal = IdentifiableFactory.getImplementation(CustomAttributeValue.class);
            booleanVal.setCoreId(booleanAttr.getCoreId());
            booleanVal.setEntityId(testObject.getCoreId());
            booleanVal.setCoreFd(FieldAccess.getSystemFd());
            booleanVal.setCoreTd(FieldAccess.getSystemTd());
            booleanVal.setValue(true);
            values.add(booleanVal);

            AttributesAccess.createValues(testObject, null, values, true);

            try {
                // проверим, что нельзя сменить обязательность атрибута в true, если есть значения
                booleanAttr.setRequired(!booleanAttr.isRequired());
                attributeAccess.updateObject(attributeAccess.getObjectById(booleanAttr.getCoreId()), booleanAttr);
                Assert.fail("Required Attrinbute chnged!!!!");
            } catch (IneIllegalArgumentException e) {
                //does nothing...
            }

            AttributesAccess.deleteValues(testObject, null, values);

            Map<CustomAttribute, CustomAttributeValue> savedValues = AttributesAccess.getValues(testObject, null);
            Assert.assertNull("Failed to delete objects from DB", savedValues.get(booleanAttr));
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testDelete() {
        try {
            AttributesAccess.deleteValues(
                    testObject, null, AttributesAccess.getValues(testObject, null).values(), true);

            Map<CustomAttribute, CustomAttributeValue> savedValues = AttributesAccess.getValues(testObject, null);

            for (CustomAttributeValue value : savedValues.values()) {
                Assert.assertNull("Failed to delete objects from DB", value);
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
