/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.structure.SystemObject;
import ru.effts.ine.core.structure.SystemObjectType;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.test.TestObjectAccess;
import ru.effts.ine.utils.BaseTest;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemObjectAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class SystemObjectAccessTest extends BaseTest {

    private static SystemObjectAccess<SystemObject> objectAccess;
    private static TestObjectAccess<TestObject> testAccess;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            objectAccess = (SystemObjectAccess<SystemObject>) AccessFactory.getImplementation(SystemObject.class);
            testAccess = (TestObjectAccess<TestObject>) AccessFactory.getImplementation(TestObject.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getAllSystemObjects() {
        try {
            long time = System.currentTimeMillis();
            Collection systemObjects = objectAccess.getAllObjects();
            System.out.println("getAllObjects time = " + (System.currentTimeMillis() - time) + " millis");
            time = System.currentTimeMillis();
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "RESULT: " + systemObjects);
            }
            System.out.println("printAllObjects time = " + (System.currentTimeMillis() - time) + " millis");
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getIdByColumnName() {
        try {
            String tableName = testAccess.getTableName();
            Map<String, String> instanceFields = testAccess.getInstanceFields();
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "RESULT: " +
                        FieldAccess.getColumnId(tableName, instanceFields.get(TestObject.CORE_ID)));
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getSystemObjectById() {
        try {
            SystemObject systemObject = objectAccess.getObjectById(BigDecimal.ONE);
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "RESULT: " + systemObject);
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void getTestObjectById() {
        try {
            testAccess.getObjectById(new BigDecimal(1), new BigDecimal(2));
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getInterfaces() {
        try {
            Collection<String> interfaces = FieldAccess.getSupportedIntefaces();
            Assert.assertNotNull(interfaces);
            Assert.assertFalse(interfaces.isEmpty());
            if (logger.isLoggable(Level.INFO)) {
                for (String intf : interfaces) {
                    logger.log(Level.INFO, intf);
                }
            }
            BigDecimal tableId = FieldAccess.getTableId(TestObject.class);
            Assert.assertEquals(FieldAccess.getInterface(tableId), TestObject.class.getName());
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getChildren() {
        try {
            BigDecimal tableId = FieldAccess.getTableId(TestObject.class);
            Collection<SystemObject> children = FieldAccess.getChildren(tableId);
            Assert.assertEquals(children.size(), 13);
            Collection<SystemObject> tables = new HashSet<SystemObject>(6);
            Collection<SystemObject> columns = new HashSet<SystemObject>(5);
            Collection<SystemObject> sequnces = new HashSet<SystemObject>(1);
            Collection<SystemObject> interfaces = new HashSet<SystemObject>(2);
            for (SystemObject obj : children) {
                if (obj.getType().equals(SystemObjectType.table.getTypeCode())) {
                    tables.add(obj);
                } else if (obj.getType().equals(SystemObjectType.column.getTypeCode())) {
                    columns.add(obj);
                } else if (obj.getType().equals(SystemObjectType.sequence.getTypeCode())) {
                    sequnces.add(obj);
                } else if (obj.getType().equals(SystemObjectType.interace.getTypeCode())) {
                    interfaces.add(obj);
                }
            }
            Assert.assertEquals(tables.size(), 6);
            Assert.assertEquals(tables, FieldAccess.getChildren(tableId, SystemObjectType.table));
            Assert.assertEquals(columns.size(), 5);
            Assert.assertEquals(columns, FieldAccess.getChildren(tableId, SystemObjectType.column));
            Assert.assertEquals(sequnces.size(), 1);
            Assert.assertEquals(sequnces, FieldAccess.getChildren(tableId, SystemObjectType.sequence));
            Assert.assertEquals(interfaces.size(), 1);
            Assert.assertEquals(interfaces, FieldAccess.getChildren(tableId, SystemObjectType.interace));
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testSCP() {
        try {
            System.out.println(objectAccess.getAllObjects().size());

            SearchCriteriaProfile searchCriteriaProfile = objectAccess.getSearchCriteriaProfile();
            @SuppressWarnings({"unchecked"}) SearchCriterion<BigDecimal> searchCriterion =
                    (SearchCriterion<BigDecimal>) searchCriteriaProfile.getSearchCriterion(SystemObject.CORE_ID);

            searchCriterion.setCriterionRule(CriteriaRule.greater);
            searchCriterion.addCriteriaVal(new BigDecimal(48));

            Set<SearchCriterion> searchCriterias = new HashSet<SearchCriterion>();
            searchCriterias.add(searchCriterion);
            Collection collection = objectAccess.searchObjects2(searchCriterias, null);
            long start = System.currentTimeMillis();
            collection = objectAccess.searchObjects2(searchCriterias, null);
            long end = System.currentTimeMillis();
            System.out.println("res: size [" + collection.size() + "]; DB time[" + (end - start) + "] ...");

            collection = objectAccess.searchObjects(searchCriterias);
            start = System.currentTimeMillis();
            collection = objectAccess.searchObjects(searchCriterias);
            end = System.currentTimeMillis();
            System.out.println("res: size [" + collection.size() + "]; Cache time[" + (end - start) + "] ...");

        } catch (GenericSystemException e) {
            e.printStackTrace();
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }
}
