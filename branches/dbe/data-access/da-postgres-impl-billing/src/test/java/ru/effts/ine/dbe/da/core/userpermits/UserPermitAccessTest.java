/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.userpermits.UserPermit;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: UserPermitAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class UserPermitAccessTest extends BaseVersionableTest {

    private static UserPermitAccess<UserPermit> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (UserPermitAccess<UserPermit>) AccessFactory.getImplementation(UserPermit.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public Identifiable testCreate() {
        try {
            UserPermit obj1 = IdentifiableFactory.getImplementation(UserPermit.class);
            obj1.setCoreId(new BigDecimal(1)); // доступ System Access
            obj1.setUserId(new BigDecimal(2)); // тестовый юзверь Test User
            obj1.setRoleId(new BigDecimal(2)); // тестовая роль NOBODY
            obj1.setValues(new BigDecimal[]{new BigDecimal(1)}); // некое тестовое значение
            obj1.setActive(false);
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            obj1 = access.createObject(obj1);
            UserPermit obj2 = access.getObjectById(access.getSyntheticId(obj1).getIdValues());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreId(), obj2.getCoreId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getUserId(), obj2.getUserId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getRoleId(), obj2.getRoleId());
            Assert.assertEquals("Failed to save new object in DB", obj1.isActive(), obj2.isActive());
            Assert.assertArrayEquals("Failed to save new object in DB", obj1.getValues(), obj2.getValues());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(access.getSyntheticId((UserPermit) identifiable).getIdValues());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            UserPermit oldOne = (UserPermit) identifiable;
            UserPermit newOne = IdentifiableFactory.getImplementation(UserPermit.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setUserId(oldOne.getUserId());
            newOne.setRoleId(oldOne.getRoleId());
            newOne.setActive(!oldOne.isActive());
            newOne.setValues(oldOne.getValues());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(access.getSyntheticId(oldOne).getIdValues());

            Assert.assertEquals("Failed to save update object in DB", oldOne.isActive(), newOne.isActive());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((UserPermit) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllUserPermits() {
        try {
            Collection result = access.getAllObjects();
            for (Object o : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + o);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
