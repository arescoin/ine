package ru.effts.ine.dbe.da.core.strings;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.strings.MessageString;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: MessageStringAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class MessageStringAccessTest extends BaseTest {

    MessageStringAccess<MessageString> access;

    @SuppressWarnings({"unchecked"})
    @Before
    public void initTest() {
        try {
            access = (MessageStringAccess<MessageString>) AccessFactory.getImplementation(MessageString.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testCRUD() {
        try {
            MessageString newOne = IdentifiableFactory.getImplementationSM(MessageString.class);

            newOne.setKey("ru.effts.ine.testString");
            newOne.setLanguageCode(BigDecimal.ONE);
            newOne.setMessage("Test message for CRUD-operations");

            MessageString oldOne = access.createObject(newOne);
            newOne = access.getStringByKeyAndLang(oldOne.getKey(), oldOne.getLanguageCode());

            Assert.assertEquals("Failed to save object on DB.", oldOne.getKey(), newOne.getKey());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getMessage(), newOne.getMessage());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getLanguageCode(), newOne.getLanguageCode());

            newOne = access.getStringsByKey(oldOne.getKey()).get(oldOne.getLanguageCode());

            Assert.assertEquals("Failed to save object on DB.", oldOne.getKey(), newOne.getKey());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getMessage(), newOne.getMessage());
            Assert.assertEquals("Failed to save object on DB.", oldOne.getLanguageCode(), newOne.getLanguageCode());

            newOne.setMessage(oldOne.getMessage() + " updated");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getStringByKeyAndLang(oldOne.getKey(), oldOne.getLanguageCode());

            Assert.assertEquals("Failed to update object on DB.", oldOne.getMessage(), newOne.getMessage());

            access.deleteObject(newOne);
            oldOne = access.getStringByKeyAndLang(oldOne.getKey(), oldOne.getLanguageCode());

            Assert.assertNull("Failed to delete object from DB.", oldOne);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllStrings() {
        try {
            for (MessageString string : access.getAllObjects()) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + string);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
