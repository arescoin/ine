/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.history;

import org.junit.Test;
import ru.effts.ine.core.history.IdentifiableHistory;
import ru.effts.ine.core.history.VersionableHistory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestVersionableHistoryAccess.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class TestVersionableHistoryAccess extends BaseTest {

    @Test(expected = UnsupportedOperationException.class)
    public void testGetHistoryById() {
        try {
            ((IdentifiableHistoryAccess)
                    AccessFactory.getImplementation(IdentifiableHistory.class)).getObjectById(new BigDecimal(1));
        } catch (UnsupportedOperationException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getAllIdentifiableHistory() {
        try {
            @SuppressWarnings({"unchecked"})
            IdentifiableHistoryAccess<IdentifiableHistory> access = (IdentifiableHistoryAccess<IdentifiableHistory>)
                    AccessFactory.getImplementation(IdentifiableHistory.class);
            Collection<IdentifiableHistory> history = access.getAllObjects();
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }

            if (history.isEmpty()) {
                return;
            }
            IdentifiableHistory item1 = history.iterator().next();
            history = access.getActionsHistoryByIds(item1.getCoreId(), item1.getEntityId());
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void getAllVersionableHistory() {
        try {
            @SuppressWarnings({"unchecked"})
            VersionableHistoryAccess<VersionableHistory> access = (VersionableHistoryAccess<VersionableHistory>)
                    AccessFactory.getImplementation(VersionableHistory.class);
            Collection<VersionableHistory> history = access.getAllObjects();
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }

            if (history.isEmpty()) {
                return;
            }
            VersionableHistory item1 = history.iterator().next();
            history = access.getActionsHistoryByIds(item1.getCoreId(), item1.getEntityId());
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "" + history);
            }
        } catch (Exception e) {
            fail(e);
        }
    }

}
