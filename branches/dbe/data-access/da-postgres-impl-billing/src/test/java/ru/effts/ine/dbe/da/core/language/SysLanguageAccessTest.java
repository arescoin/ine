package ru.effts.ine.dbe.da.core.language;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.language.LangName;
import ru.effts.ine.core.language.SysLanguage;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: SysLanguageAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class SysLanguageAccessTest extends BaseVersionableTest {

    private static SysLanguageAccess<SysLanguage> access;
    private static final BigDecimal LANG_ID = new BigDecimal(9999);

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (SysLanguageAccess<SysLanguage>) AccessFactory.getImplementation(SysLanguage.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public void testCRUD() {
        try {
            @SuppressWarnings({"unchecked"}) LangNameAccess<LangName> access =
                    (LangNameAccess<LangName>) AccessFactory.getImplementation(LangName.class);

            LangName name = IdentifiableFactory.getImplementation(LangName.class);
            name.setCoreId(LANG_ID);
            name.setCoreDsc("TestObject for CRUD-operations testing");
            name.setName("LANG NAME");

            UserHolder.getUser().setReason("Common CRUD testing: create; for class: " + access.getClass().getName());
            access.createObject(name);

            super.testCRUD();

            UserHolder.getUser().setReason("Common CRUD testing: delete; for class: " + access.getClass().getName());
            access.deleteObject(name);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            SysLanguage obj1 = IdentifiableFactory.getImplementation(SysLanguage.class);

            obj1.setCoreId(LANG_ID);
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setLangDetails(new BigDecimal(107));  // sq Албанский
            obj1.setCountryDetails(new BigDecimal(8)); // AL Албания

            obj1 = access.createObject(obj1);
            SysLanguage obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", null, obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save object in DB.", obj1.getLangDetails(), obj2.getLangDetails());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCountryDetails(), obj2.getCountryDetails());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            SysLanguage obj1 = (SysLanguage) identifiable;
            SysLanguage obj2 = IdentifiableFactory.getImplementation(SysLanguage.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setLangDetails(obj1.getLangDetails());
            obj2.setCountryDetails(new BigDecimal(643)); // RU Россия

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to update object on DB.", obj1.getCountryDetails(), obj2.getCountryDetails());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((SysLanguage) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testSysLanguage() {
        try {
            for (SysLanguage details : access.getAllObjects()) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + details);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
