/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import ru.effts.ine.core.*;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.dic.DictionaryEntryAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeAccessTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
@Ignore
public class CustomAttributeAccessTest extends BaseVersionableTest {

    private static CustomAttributeAccess<CustomAttribute> access;
    private static SystemObjectPattern pattern;
    private static final String ATTRIBITE_NAME = "CustomAttributeAccessTest AttributeName";

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (CustomAttributeAccess<CustomAttribute>) AccessFactory.getImplementation(CustomAttribute.class);

            IdentifiableFactory.getImplementation(DictionaryEntry.class);

            DictionaryEntryAccess<DictionaryEntry> entryAccess =
                    (DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(DictionaryEntry.class);
            DictionaryEntry entry = entryAccess.getObjectById(new BigDecimal(6), new BigDecimal(1));

            pattern = PatternUtils.getPatternByObject(entry).keySet().iterator().next();
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            BigDecimal testObjId = FieldAccess.getTableId(TestObject.class);

            CustomAttribute obj1 = IdentifiableFactory.getImplementation(CustomAttribute.class);
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc("CustomAttribute CRUD-tests via data-access");
            obj1.setSystemObjectId(testObjId);
            obj1.setAttributeName(ATTRIBITE_NAME);
            obj1.setDataType(DataType.stringType);
            obj1.setRequired(false);
            obj1.setPattern(pattern);
            obj1.setLimitations("TEST");

            try {
                obj1 = access.createObject(obj1);
                fail(new IneCorruptedStateException(""));
            } catch (IneIllegalArgumentException e) {
                Assert.assertTrue(e.getMessage().contains(DataType.bigDecimalType.toString()));
            }
            obj1.setDataType(DataType.bigDecimalType);
            try {
                obj1.setAttributeName(CustomAttribute.CORE_ID);
                access.createObject(obj1);
            } catch (IneIllegalArgumentException e) {
                Assert.assertTrue(e.getMessage().contains(CustomAttribute.CORE_ID));
            }
            obj1.setAttributeName(ATTRIBITE_NAME);

            obj1 = access.createObject(obj1);
            CustomAttribute obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getSystemObjectId(), obj2.getSystemObjectId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getAttributeName(), obj2.getAttributeName());
            Assert.assertEquals("Failed to save new object in DB", obj1.getDataType(), obj2.getDataType());
            Assert.assertEquals("Failed to save new object in DB", obj1.isRequired(), obj2.isRequired());
            Assert.assertEquals("Failed to save new object in DB", obj1.getPattern(), obj2.getPattern());
            Assert.assertEquals("Failed to save new object in DB", obj1.getLimitations(), obj2.getLimitations());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            CustomAttribute obj1 = (CustomAttribute) identifiable;
            CustomAttribute obj2 = IdentifiableFactory.getImplementation(CustomAttribute.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setSystemObjectId(obj1.getSystemObjectId());
            obj2.setAttributeName(obj1.getAttributeName());
            obj2.setDataType(obj1.getDataType());
            obj2.setRequired(!obj1.isRequired());
            obj2.setPattern(obj1.getPattern());
            obj2.setLimitations(obj1.getLimitations());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.isRequired(), obj2.isRequired());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((CustomAttribute) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testCreateDuplicates() {
        Identifiable attribute = testCreate();
        try {
            testCreate();
            Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
        } catch (IneIllegalArgumentException e) {
            throw e;
        } catch (Exception e) {
            fail(e);
        } finally {
            testDelete(attribute);
        }
    }
}
