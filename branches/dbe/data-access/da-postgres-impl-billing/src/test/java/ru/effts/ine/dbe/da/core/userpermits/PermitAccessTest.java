package ru.effts.ine.dbe.da.core.userpermits;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.userpermits.Permit;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: PermitAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class PermitAccessTest extends BaseVersionableTest {

    private static PermitAccess<Permit> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (PermitAccess<Permit>) AccessFactory.getImplementation(Permit.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            Permit obj1 = IdentifiableFactory.getImplementation(Permit.class);
            obj1.setName("TEST_PERMIT");
            obj1.setMaskValue("TestMask");
            obj1.setValueRequired(false);
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            obj1 = access.createObject(obj1);
            Permit obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getName(), obj2.getName());
            Assert.assertEquals("Failed to save new object in DB", obj1.getMaskValue(), obj2.getMaskValue());
            Assert.assertEquals("Failed to save new object in DB", obj1.isValueRequired(), obj2.isValueRequired());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Permit obj1 = (Permit) identifiable;
            Permit obj2 = IdentifiableFactory.getImplementation(Permit.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setName(obj1.getName());
            obj2.setMaskValue(obj1.getMaskValue());
            obj1.setValueRequired(!obj1.isValueRequired());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.isValueRequired(), obj2.isValueRequired());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Permit) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllPermits() {
        try {
            Collection<Permit> result = access.getAllObjects();
            for (Permit permit : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + permit);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
