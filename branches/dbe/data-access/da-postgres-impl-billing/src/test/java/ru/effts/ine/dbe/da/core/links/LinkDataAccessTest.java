package ru.effts.ine.dbe.da.core.links;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.dic.DictionaryEntryAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkDataAccessTest.java 3855 2014-07-04 15:02:31Z DGomon $"
 */
public class LinkDataAccessTest extends BaseVersionableTest {

    private static LinkDataAccess<LinkData> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (LinkDataAccess<LinkData>) AccessFactory.getImplementation(LinkData.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    public void testGet() {
        try {
            Collection collection = access.getAllObjects();

            if (collection != null) {
                System.out.println(collection.size());
            }
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }


    @Override
    public Identifiable testCreate() {
        try {
            LinkData newData = IdentifiableFactory.getImplementation(LinkData.class);
            newData.setLinkId(BigDecimal.ONE);
            newData.setLeftId(new BigDecimal(2));
            newData.setRightId(new BigDecimal(2));
            newData.setCoreFd(FieldAccess.getSystemFd());
            newData.setCoreTd(FieldAccess.getSystemTd());
            newData.setCoreDsc("TestObject for CRUD-operations testing");

            newData = access.createObject(newData);
            LinkData data = access.getObjectById(newData.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", null, data.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", newData.getLinkId(), data.getLinkId());
            Assert.assertEquals("Failed to save new object in DB", newData.getLeftId(), data.getLeftId());
            Assert.assertEquals("Failed to save new object in DB", newData.getRightId(), data.getRightId());
            Assert.assertEquals("Failed to save new object in DB", newData.getCoreFd(), data.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", newData.getCoreTd(), data.getCoreTd());

            return data;
        } catch (GenericSystemException e) {
            fail(e, !(e.getCause() instanceof SQLException));
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            LinkData oldData = (LinkData) identifiable;
            LinkData newData = IdentifiableFactory.getImplementation(LinkData.class);

            newData.setCoreId(oldData.getCoreId());
            newData.setCoreFd(FieldAccess.getCurrentDate());
            newData.setCoreTd(oldData.getCoreTd());
            newData.setLinkId(oldData.getLinkId());
            newData.setLeftId(oldData.getLeftId());
            newData.setRightId(oldData.getRightId().add(BigDecimal.ONE));

            oldData = access.updateObject(oldData, newData);
            newData = access.getObjectById(oldData.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", oldData.getRightId(), newData.getRightId());

            return newData;
        } catch (GenericSystemException e) {
            fail(e, !(e.getCause() instanceof SQLException));
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((LinkData) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }


    public void aft(){
        this.testGetLinkByObject();
        this.testCreateDuplicates();
        this.testUpdateDuplicates();
    }

    @SuppressWarnings({"unchecked"})
    public void testGetLinkByObject() {
        try {
            DictionaryEntry entry = ((DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(
                    DictionaryEntry.class)).getObjectById(new BigDecimal(12), BigDecimal.ONE);
            LinkAccess<Link> la = (LinkAccess<Link>) AccessFactory.getImplementation(Link.class);
            Collection<Link> links = la.getLinksByObjType(entry.getClass());
            logger.log(Level.INFO, "\n");
            for (Link link : links) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + link);
                }
            }
            Collection<LinkData> linkDatas = access.getLinkDataByObject(entry);
            logger.log(Level.INFO, "\n");
            for (LinkData data : linkDatas) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + data);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    public void testCreateDuplicates() {
        Identifiable linkData = testCreate();
        try {
            testCreate();
            Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
        } catch (AssertionError e) {
            if (e != null && e.getCause() != null && e.getCause().getCause() != null) {
                Assert.assertTrue(e.getCause().getCause() instanceof SQLException);
            } else {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        } finally {
            testDelete(linkData);
        }
    }

    public void testUpdateDuplicates() {
        Identifiable linkData1 = testCreate();
        try {
            linkData1 = testUpdate(linkData1);
            Identifiable linkData2 = testCreate();
            try {
                linkData2 = testUpdate(linkData2);
                Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
            } catch (AssertionError e) {
                if (e != null && e.getCause() != null && e.getCause().getCause() != null) {
                    Assert.assertTrue(e.getCause().getCause() instanceof SQLException);
                } else {
                    logger.log(Level.WARNING, e.getMessage(), e);
                }
            } finally {
                testDelete(linkData2);
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            testDelete(linkData1);
        }
    }
}
