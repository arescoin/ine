/**********************************************************************************************************************
 * Copyright (c) 2014, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.links;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.structure.SystemObjectPattern;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LinkAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class LinkAccessTest extends BaseVersionableTest {

    private static LinkAccess<Link> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (LinkAccess<Link>) AccessFactory.getImplementation(Link.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testLinks() {
        try {
            for (Link link : access.getAllObjects()) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + link);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            BigDecimal testId = FieldAccess.getColumnId("TEST_TABLE", "n");

            Link newLink = IdentifiableFactory.getImplementation(Link.class);
            newLink.setTypeId(BigDecimal.ONE);
            newLink.setLeftType(new SystemObjectPattern(testId));
            newLink.setRightType(new SystemObjectPattern(testId));
            newLink.setCoreFd(FieldAccess.getSystemFd());
            newLink.setCoreTd(FieldAccess.getSystemTd());
            newLink.setCoreDsc("TestObject for CRUD-operations testing");

            newLink = access.createObject(newLink);
            Link link = access.getObjectById(newLink.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", null, link.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", newLink.getTypeId(), link.getTypeId());
            Assert.assertEquals("Failed to save new object in DB", newLink.getLeftType(), link.getLeftType());
            Assert.assertEquals("Failed to save new object in DB", newLink.getRightType(), link.getRightType());
            Assert.assertEquals("Failed to save new object in DB", newLink.getCoreFd(), link.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", newLink.getCoreTd(), link.getCoreTd());

            return link;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Link oldLink = (Link) identifiable;
            Link newLink = IdentifiableFactory.getImplementation(Link.class);

            newLink.setCoreId(oldLink.getCoreId());
            newLink.setCoreFd(FieldAccess.getCurrentDate());
            newLink.setCoreTd(oldLink.getCoreTd());
            newLink.setTypeId(oldLink.getTypeId());
            newLink.setLeftType(oldLink.getLeftType());
            newLink.setRightType(new SystemObjectPattern(FieldAccess.getColumnId("LINKS", "n")));

            oldLink = access.updateObject(oldLink, newLink);
            newLink = access.getObjectById(oldLink.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", oldLink.getRightType(), newLink.getRightType());

            return newLink;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Link) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }

    @Test
    public void testCreateDuplicates() {
        Identifiable link = testCreate();
        try {
            testCreate();
            Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
        } catch (Exception e) {
            Assert.assertTrue(e instanceof IneIllegalArgumentException);
        } finally {
            testDelete(link);
        }
    }

    @Test
    public void testUpdateDuplicates() {
        Identifiable link1 = testCreate();
        try {
            link1 = testUpdate(link1);
            Identifiable link2 = testCreate();
            try {
                link2 = testUpdate(link2);
                Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
            } catch (Exception e) {
                Assert.assertTrue(e instanceof IneIllegalArgumentException);
            } finally {
                testDelete(link2);
            }
        } catch (Exception e) {
            fail(e);
        } finally {
            testDelete(link1);
        }
    }

}
