package ru.effts.ine.dbe.da.oss.entity;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;
import ru.effts.ine.oss.entity.Company;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CompanyAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class CompanyAccessTest extends BaseVersionableTest {

    private static CompanyAccess<Company> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (CompanyAccess<Company>) AccessFactory.getImplementation(Company.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Test
    public void testGetCompanies() {
        try {
            Collection<Company> result = access.getAllObjects();
            for (Company company : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + company);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            Company obj1 = IdentifiableFactory.getImplementation(Company.class);
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());
            obj1.setCoreDsc("TestObject for CRUD-testing");
            obj1.setName("Test name");
            obj1.setType(BigDecimal.ONE);
            obj1.setPropertyType(BigDecimal.ONE);

            obj1 = access.createObject(obj1);
            Company obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getName(), obj2.getName());
            Assert.assertEquals("Failed to save new object in DB", obj1.getType(), obj2.getType());
            Assert.assertEquals("Failed to save new object in DB", obj1.getPropertyType(), obj2.getPropertyType());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Company obj1 = (Company) identifiable;
            Company obj2 = IdentifiableFactory.getImplementation(Company.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setCoreFd(FieldAccess.getCurrentDate());
            obj2.setCoreTd(obj1.getCoreTd());
            obj2.setCoreDsc(obj1.getCoreDsc());
            obj2.setName(obj1.getName());
            obj2.setType(obj1.getType());
            obj2.setPropertyType(new BigDecimal(2));

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save update object in DB", obj1.getPropertyType(), obj2.getPropertyType());

            return obj2;
        } catch (CoreException e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Company) identifiable);
        } catch (CoreException e) {
            fail(e);
        }
    }
}
