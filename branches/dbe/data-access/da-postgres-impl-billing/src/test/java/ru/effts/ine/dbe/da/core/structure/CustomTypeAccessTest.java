/**********************************************************************************************************************
 * Copyright (c) 2010, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.structure;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import ru.effts.ine.core.*;
import ru.effts.ine.core.structure.CustomAttribute;
import ru.effts.ine.core.structure.CustomType;
import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.core.structure.test.TestObject;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;

/**
 * класс для тестирования доступа к описаниям модифицированных типов
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: CustomTypeAccessTest.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
@Ignore
public class CustomTypeAccessTest extends BaseVersionableTest {

    private Class interfaceName;
    private static final String CORE_DSC = "CustomType CRUD-test via data-access";
    private static final String TYPE_NAME1 = "CustomTypeAccessTest TypeName1";
    private static final String TYPE_NAME2 = "CustomTypeAccessTest TypeName2";
    private static final String TEST_ATTR = "CustomTypeAccessTest AttributeName";

    private static CustomTypeAccess<CustomType> cta;
    private static CustomAttributeAccess<CustomAttribute> caa;

    @BeforeClass
    @SuppressWarnings({"unchecked"})
    public static void initAccess() {
        try {
            cta = (CustomTypeAccess<CustomType>) AccessFactory.getImplementation(CustomType.class);
            caa = (CustomAttributeAccess<CustomAttribute>) AccessFactory.getImplementation(CustomAttribute.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    private CustomAttribute createAndGet(String attrName) throws GenericSystemException {
        CustomAttribute ca = IdentifiableFactory.getImplementation(CustomAttribute.class);
        ca.setAttributeName(attrName);
        ca.setCoreFd(FieldAccess.getCurrentDate());
        ca.setCoreTd(FieldAccess.getSystemTd());
        ca.setDataType(DataType.stringType);
        ca.setRequired(false);
        return ca;
    }

    @Before
    public void init() throws CoreException {
        //данный интерфейс соответствует тестовой таблице
        interfaceName = TestObject.class;
    }

    @Override
    public Identifiable testCreate() {
        CustomType createdCT = null;
        try {
            CustomType ct = IdentifiableFactory.getImplementation(CustomType.class);
            ct.setCoreDsc(CORE_DSC);
            ct.setCoreFd(FieldAccess.getCurrentDate());
            ct.setCoreTd(FieldAccess.getSystemTd());
            ct.setBaseObject(FieldAccess.getTableId(interfaceName));
            ct.setTypeName(TYPE_NAME1);
            Collection<CustomAttribute> col = new HashSet<CustomAttribute>();
            col.add(createAndGet(TEST_ATTR));
            col.add(createAndGet(TEST_ATTR));
            ct.setCustomAttributes(col);
            try {
                // проверим что нельзя создать атрибуты с одинаковыми именами
                createdCT = cta.createObject(ct);
                Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
            } catch (IneIllegalArgumentException e) {
                Assert.assertTrue(e.getMessage().contains("duplicate"));
            }
            col.clear();
            col.add(createAndGet(TEST_ATTR + 1));
            col.add(createAndGet(TEST_ATTR + 2));
            ct.setCustomAttributes(col);
            createdCT = cta.createObject(ct);
            Assert.assertEquals(FieldAccess.getTableId(interfaceName), createdCT.getBaseObject());
            Assert.assertEquals(TYPE_NAME1, createdCT.getTypeName());
            Assert.assertNull(createdCT.getParentTypeId());
            Assert.assertEquals(2, createdCT.getCustomAttributes().size());
            Assert.assertEquals(CORE_DSC, createdCT.getCoreDsc());
        } catch (Exception e) {
            fail(e);
        }
        return createdCT;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        CustomType ct = null;
        try {
            ct = cta.getObjectById(identifiable.getCoreId());
            Assert.assertEquals(FieldAccess.getTableId(interfaceName), ct.getBaseObject());
            Assert.assertEquals(TYPE_NAME1, ct.getTypeName());
            Assert.assertNull(ct.getParentTypeId());
            Assert.assertEquals(2, ct.getCustomAttributes().size());
            Assert.assertEquals(CORE_DSC, ct.getCoreDsc());
        } catch (Exception e) {
            fail(e);
        }
        return ct;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        CustomType ct = (CustomType) identifiable;
        try {
            CustomType newCT = cta.getObjectById(identifiable.getCoreId());
            newCT.setCoreDsc(ct.getCoreDsc());
            newCT.setCoreFd(FieldAccess.getCurrentDate());
            newCT.setCoreTd(FieldAccess.getSystemTd());
            newCT.setTypeName(ct.getTypeName());
            Collection<CustomAttribute> col = new HashSet<CustomAttribute>();
            CustomAttribute ca = caa.getObjectById(ct.getCustomAttributes().iterator().next().getCoreId());
            ca.setLimitations("Test Updated");
            ca.setCoreFd(FieldAccess.getCurrentDate());
            col.add(ca);
            col.add(createAndGet(TEST_ATTR + 3));
            col.add(createAndGet(TEST_ATTR + 4));
            newCT.setCustomAttributes(col);
            ct = cta.updateObject(ct, newCT);
            Assert.assertEquals(FieldAccess.getTableId(interfaceName), ct.getBaseObject());
            Assert.assertEquals(CORE_DSC, ct.getCoreDsc());
            Assert.assertEquals(TYPE_NAME1, ct.getTypeName());
            Assert.assertNull(ct.getParentTypeId());
            Assert.assertEquals(3, ct.getCustomAttributes().size());
        } catch (Exception e) {
            fail(e);
        }
        return ct;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            cta.deleteObject((CustomType) identifiable);
            //проверяем что объекта нет в кеше
            Assert.assertNull(cta.getObjectById(identifiable.getCoreId()));
        } catch (Exception e) {
            fail(e);
        }
    }

//    @Test
    public void testCacheAndAttributes() {
        try {
            CustomType ctParent = (CustomType) testCreate();
            try {
                //создаем наследника от уже имеющегося модифицированного типа и расширяем его еще одним CustomAttribure
                CustomType ct = IdentifiableFactory.getImplementation(CustomType.class);
                BigDecimal usedInterface = ctParent.getBaseObject();
                int typesNumber = cta.getTypesForBaseObject(usedInterface).size();
                ct.setCoreDsc(CORE_DSC);
                ct.setCoreFd(FieldAccess.getCurrentDate());
                ct.setCoreTd(FieldAccess.getSystemTd());
                ct.setBaseObject(FieldAccess.getTableId(interfaceName));
                ct.setParentTypeId(ctParent.getCoreId());
                ct.setTypeName(TYPE_NAME2);
                Collection<CustomAttribute> col = new HashSet<CustomAttribute>();
                col.add(createAndGet(TEST_ATTR + 1));
                ct.setCustomAttributes(col);
                try {
                    //проверим, что нельзя создать атрибут с таким же именем, как и в родителе
                    ct = cta.createObject(ct);
                    Assert.fail(EXPECTED_EXCEPTION_WAS_NOT_THROWN);
                } catch (IneIllegalArgumentException e) {
                    Assert.assertTrue(e.getMessage().endsWith("is already used by some of parent types"));
                }
                col.clear();
                col.add(createAndGet(TEST_ATTR + 5));
                ct.setCustomAttributes(col);
                ct = cta.createObject(ct);
                Assert.assertEquals(2, ctParent.getCustomAttributes().size());
                Assert.assertEquals(usedInterface, ct.getBaseObject());
                Assert.assertEquals(TYPE_NAME2, ct.getTypeName());
                Assert.assertEquals(1, ct.getCustomAttributes().size());
                Assert.assertEquals(2, cta.getParentAttributes(ct).size());
                Assert.assertEquals(3, cta.getAllAttributes(ct).size());
                Assert.assertEquals(CORE_DSC, ct.getCoreDsc());
                Assert.assertEquals(typesNumber + 1, cta.getTypesForBaseObject(usedInterface).size());
                cta.deleteObject(ct);
                //проверяем что объекта нет в кеше
                Assert.assertNull(cta.getObjectById(ct.getCoreId()));
                Assert.assertEquals(typesNumber, cta.getTypesForBaseObject(usedInterface).size());
            } catch (Exception e) {
                fail(e);
            } finally {
                testDelete(ctParent);
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
