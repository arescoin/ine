/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.userpermits;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.userpermits.RolePermit;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: RolePermitAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class RolePermitAccessTest extends BaseVersionableTest {

    private static RolePermitAccess<RolePermit> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (RolePermitAccess<RolePermit>) AccessFactory.getImplementation(RolePermit.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
    }

    @Override
    public Identifiable testCreate() {
        try {
            RolePermit obj1 = IdentifiableFactory.getImplementation(RolePermit.class);
            obj1.setCoreId(new BigDecimal(2)); // тестовая роль NOBODY
            obj1.setPermitId(new BigDecimal(2)); // тестовый доступ System's Objects Access
            obj1.setPermitValues(new BigDecimal[]{new BigDecimal(1)}); // некое тестовое значение
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            obj1 = access.createObject(obj1);
            RolePermit obj2 = access.getObjectById(access.getSyntheticId(obj1).getIdValues());

            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreId(), obj2.getCoreId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getPermitId(), obj2.getPermitId());
            Assert.assertArrayEquals("Failed to save new object in DB", obj1.getPermitValues(), obj2.getPermitValues());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreFd(), obj2.getCoreFd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreTd(), obj2.getCoreTd());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(access.getSyntheticId((RolePermit) identifiable).getIdValues());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            RolePermit oldOne = (RolePermit) identifiable;
            RolePermit newOne = IdentifiableFactory.getImplementation(RolePermit.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setPermitId(oldOne.getPermitId());
            newOne.setPermitValues(new BigDecimal[]{new BigDecimal(2)});
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(access.getSyntheticId(oldOne).getIdValues());

            Assert.assertArrayEquals("Failed to save update object in DB",
                    oldOne.getPermitValues(), newOne.getPermitValues());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((RolePermit) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testGetAllRolePermits() {
        try {
            Collection<RolePermit> result = access.getAllObjects();
            for (RolePermit rolePermit : result) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + rolePermit);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
