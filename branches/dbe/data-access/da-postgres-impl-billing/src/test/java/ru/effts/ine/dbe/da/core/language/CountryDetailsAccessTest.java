/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.dbe.da.core.language;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.core.language.CountryDetails;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.constants.ConstantValueAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseIdentifiableTest;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CountryDetailsAccessTest.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class CountryDetailsAccessTest extends BaseIdentifiableTest {

    private static CountryDetailsAccess<CountryDetails> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (CountryDetailsAccess<CountryDetails>) AccessFactory.getImplementation(CountryDetails.class);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Before
    public void initTest() {
        manualSetId = true;
        //checkActionDate = false;
    }

    @Test
    public void testSCP() {
        try {
            SearchCriteriaProfile searchCriteriaProfile = access.getSearchCriteriaProfile();
            @SuppressWarnings({"unchecked"}) SearchCriterion<BigDecimal> searchCriterion =
                    (SearchCriterion<BigDecimal>) searchCriteriaProfile.getSearchCriterion(CountryDetails.CORE_ID);

            searchCriterion.setCriterionRule(CriteriaRule.greater);
            searchCriterion.setInvert(false);
            searchCriterion.addCriteriaVal(new BigDecimal(48));

            Set<SearchCriterion> searchCriterias = new HashSet<SearchCriterion>();
            searchCriterias.add(searchCriterion);
            Collection collection = access.searchObjects2(searchCriterias, null);
            long start = System.currentTimeMillis();
            collection = access.searchObjects2(searchCriterias, null);
            long end = System.currentTimeMillis();
            System.out.println("res: size [" + collection.size() + "]; DB time[" + (end - start) + "] ...");

            collection = access.searchObjects(searchCriterias);
            start = System.currentTimeMillis();
            collection = access.searchObjects(searchCriterias);
            end = System.currentTimeMillis();
            System.out.println("res: size [" + collection.size() + "]; Cache time[" + (end - start) + "] ...");

        } catch (GenericSystemException e) {
            e.printStackTrace();
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void testCRUD() {
        BigDecimal allowUpdateISOCodesConstant = new BigDecimal(4);
        try {
            updateConstantValue(allowUpdateISOCodesConstant, "1");
            super.testCRUD();
        } finally {
            updateConstantValue(allowUpdateISOCodesConstant, "0");
        }
    }

    private void updateConstantValue(BigDecimal constantId, String value) {
        try {
            @SuppressWarnings({"unchecked"}) ConstantValueAccess<ConstantValue> constantValueAccess =
                    (ConstantValueAccess<ConstantValue>) AccessFactory.getImplementation(ConstantValue.class);
            ConstantValue constantValue = constantValueAccess.getObjectById(constantId);
            constantValue.setValue(value);
            constantValue.setCoreFd(FieldAccess.getCurrentDate());
            constantValueAccess.updateObject(constantValueAccess.getObjectById(constantId), constantValue);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Override
    public Identifiable testCreate() {
        try {
            CountryDetails obj1 = IdentifiableFactory.getImplementation(CountryDetails.class);

            obj1.setCoreId(new BigDecimal(9999));
            obj1.setCoreDsc("TestObject for CRUD-operations testing");
            obj1.setName("countryDetails Test");
            obj1.setCodeA2("AA");
            obj1.setCodeA3("AA1");

            obj1 = access.createObject(obj1);
            CountryDetails obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save object in DB.", null, obj2.getCoreDsc());
            Assert.assertEquals("Failed to save object in DB.", obj1.getName(), obj2.getName());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCodeA2(), obj2.getCodeA2());
            Assert.assertEquals("Failed to save object in DB.", obj1.getCodeA3(), obj2.getCodeA3());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {
        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            CountryDetails obj1 = (CountryDetails) identifiable;
            CountryDetails obj2 = IdentifiableFactory.getImplementation(CountryDetails.class);

            obj2.setCoreId(obj1.getCoreId());
            obj2.setName(obj1.getName() + " Updated");
            obj2.setCodeA2(obj1.getCodeA2());
            obj2.setCodeA3(obj1.getCodeA3());

            obj1 = access.updateObject(obj1, obj2);
            obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to update object on DB.", obj1.getName(), obj2.getName());

            return obj2;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((CountryDetails) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

    @Test
    public void testCountryDetails() {
        try {
            for (CountryDetails details : access.getAllObjects()) {
                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "" + details);
                }
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
