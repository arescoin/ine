/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Исключение выбрасывается при попытке выполнения операций с инвалидным идентификатором.
 * <p/>
 * Идентификатор не может быть null, или пустым значением
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IllegalIdException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public class IllegalIdException extends IneIllegalArgumentException {

    /**
     * Конструирует новое исключение с указанием детального сообщения о причине
     *
     * @param message детализация ошибки
     */
    public IllegalIdException(String message) {
        super(message);
    }

    /**
     * Конструирует новое исключение с указанием детального сообщения о причине и с указанием инициирующего исключения
     *
     * @param message детализация ошибки
     * @param cause   инициирующее исключение
     */
    public IllegalIdException(String message, Throwable cause) {
        super(message, cause);
    }

}
