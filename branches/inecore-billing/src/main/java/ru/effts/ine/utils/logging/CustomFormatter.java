/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * Реализует форматер, предоставляющий более читабельный вид сообщений
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CustomFormatter.java 3381 2011-11-30 15:33:30Z dgomon $"
 */
public class CustomFormatter extends Formatter {

    public static final String MESSAGE_START = ">";
    public static final String BODY_START = "    ";

    public static final String LINE_SEPARATOR = System.getProperty("line.separator");

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss:S");

    @Override
    public synchronized String format(LogRecord record) {
        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append(MESSAGE_START);
        stringBuilder.append(' ');

        stringBuilder.append(DATE_FORMAT.format(new Date(record.getMillis())));
        stringBuilder.append(" [");

        stringBuilder.append(record.getLevel());
        stringBuilder.append("] ");

        stringBuilder.append(record.getSequenceNumber());
        stringBuilder.append(" [");

        stringBuilder.append(record.getThreadID());
        stringBuilder.append("] ");

        stringBuilder.append(record.getSourceClassName()).append(".").append(record.getSourceMethodName());

        stringBuilder.append(" ").append(MESSAGE_START).append(" ");
        stringBuilder.append(record.getMessage());

        @SuppressWarnings({"ThrowableResultOfMethodCallIgnored"})
        Throwable throwable = record.getThrown();
        if (throwable != null) {
            stringBuilder.append(LINE_SEPARATOR);
            try {
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                throwable.printStackTrace(pw);
                pw.close();
                stringBuilder.append(sw.toString());
            } catch (Exception ex) {
                stringBuilder.append("Can't dump exception! See System.err...");
                ex.printStackTrace(System.err);
            }
        }

        stringBuilder.append(LINE_SEPARATOR);

        return stringBuilder.toString();
    }
}
