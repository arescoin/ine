/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import java.util.Comparator;

/**
 * Простой матчер для сравнения на равенство.
 * Количество паттернов: строго один
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ComparatorEqualsRuleMatcher.java 3491 2011-12-13 11:38:42Z ikulkov $"
 */
public class ComparatorEqualsRuleMatcher extends AbstractComparatorRuleMatcher {

    public ComparatorEqualsRuleMatcher(Comparator comparator) {
        super(comparator);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        if (pattern.length == 0) {
            return false;
        }
        try {
            return comparator.compare(value, pattern[0]) == 0;
        } catch (ClassCastException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
