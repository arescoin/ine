/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.cache;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.utils.config.ConfigurationManager;
import ru.effts.rims.services.StorageFilter;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Утилитный класс для работы с кешем. Предоставляет доступ к реализации интерфейса {@link CacheAccess}. Повторяет все
 * его методы и делегирует вызов на конкретную реализацию.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: Cache.java 3378 2011-11-30 15:29:02Z dgomon $"
 */
public class Cache {

    private final static Logger LOGGER = Logger.getLogger(Cache.class.getName());

    /** Суффикс конфига для реализации кешакцесса */
    public static final String CACHE_IMPL_CLASS_NAME = "CACHE_IMPL_CLASS_NAME";

    /** Реализация используемая по-умолчанию */
    public static final Class CACHE_IMPL_DEF_CLASS = CacheAccessRiMSImpl.class;

    private static final CacheAccess cacheAccess = createImplementation();

//    private static CacheAccess cacheAccess = new CacheAccessEHImpl();
//    private static CacheAccess cacheAccess = new CacheAccessLocalMSImpl();
//    private static CacheAccess cacheAccess = new CacheAccessRiMSImpl();
//    private static CacheAccess cacheAccess = new CacheAccessDevImpl();


    /**
     * Производит инициализацию инстанса конкретной реализации  {@link CacheAccess}
     * <p/>
     * Получает из конфига, если случился затык, то выложит в лог и вернет {@link #CACHE_IMPL_DEF_CLASS}
     *
     * @return используемая реализация кешакцесса
     */
    private static CacheAccess createImplementation() {

        CacheAccess result = null;

        Class cacheClass = CACHE_IMPL_DEF_CLASS;

        final String stringValue = ConfigurationManager.getManager().getRootConfig()
                .getValueByKey(Cache.class.getName() + '.' + CACHE_IMPL_CLASS_NAME);

        if (stringValue != null && stringValue.length() > 0) {
            try {
                cacheClass = Class.forName(stringValue);
            } catch (ClassNotFoundException e) {
                LOGGER.log(Level.WARNING, "Can't crate cache implementation class [" + stringValue + "]");
            }

            try {
                result = (CacheAccess) cacheClass.newInstance();
            } catch (Throwable e) {
                LOGGER.log(Level.WARNING, "Can't crate cache implementation instance [" + stringValue + "]");
            }
        }

        if (result == null) {
            result = new CacheAccessRiMSImpl();
        }

        return result;
    }

    private Cache() {
    }

    /**
     * Получает по ключу объект из указанного региона кеша.
     *
     * @param region   регион кеша
     * @param key      ключ объекта
     * @param producer экземпляр класс {@link ru.effts.ine.utils.cache.Producer продюссера}, который используется для
     *                 получения объекта, если его не обнаружилось в кеше.
     * @return объект, содержимое кеша
     * @throws CoreException ошибка при извлечении объекта из кеша
     */
    public static Object get(String region, Object key, Producer producer) throws CoreException {
        return cacheAccess.get(region, key, producer, true);
    }

    /**
     * Получает по ключу объект из указанного региона кеша.
     *
     * @param region   регион кеша
     * @param key      ключ объекта
     * @param producer экземпляр класс {@link ru.effts.ine.utils.cache.Producer продюссера}, который используется для
     *                 получения объекта, если его не обнаружилось в кеше.
     * @param block    признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @return объект, содержимое кеша
     * @throws CoreException ошибка при извлечении объекта из кеша
     */
    public static Object get(String region, Object key, Producer producer, boolean block) throws CoreException {
        return cacheAccess.get(region, key, producer, block);
    }

    public static Map getRegionContent(String region, Producer producer) throws CoreException {
        return getRegionContent(region, producer, true);
    }

    public static Map getRegionContent(String region, Producer producer, boolean block) throws CoreException {
        return cacheAccess.getRegionContent(region, producer, block);
    }

    /**
     * Получает по идентификатору региона кеша набор ключей в регионе.
     *
     * @param region регион кеша
     * @return объект, содержимое кеша
     * @throws CoreException ошибка при получении набора ключей
     */
    public static Set<Object> getKeys(String region) throws CoreException {
        return cacheAccess.getKeys(region);
    }

    /**
     * Помещает объект в указанный регион под заданным ключом.
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @param value  кешируемый объект
     * @return предыдущий закешированный объект
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    public static Object put(String region, Object key, Object value) throws GenericSystemException {
        return cacheAccess.put(region, key, value, true);
    }

    /**
     * Помещает объект в указанный регион под заданным ключом.
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @param block  признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @param value  кешируемый объект
     * @return предыдущий закешированный объект
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    public static Object put(String region, Object key, Object value, boolean block) throws GenericSystemException {
        return cacheAccess.put(region, key, value, block);
    }

    /**
     * Помещает объекты в указанный регион.
     *
     * @param region регион кеша
     * @param vlues  содержимое
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    public static void put(String region, Map vlues) throws GenericSystemException {
        put(region, vlues, true);
    }

    /**
     * Помещает объекты в указанный регион.
     *
     * @param region регион кеша
     * @param vlues  содержимое
     * @param block  признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @throws GenericSystemException ошибка при помещении объекта в кеш
     */
    public static void put(String region, Map vlues, boolean block) throws GenericSystemException {
        cacheAccess.put(region, vlues, block);
    }

    /**
     * Удаляет из указанного региона кеша объект по его ключу
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @throws ru.effts.ine.core.GenericSystemException ошибка при удалении объекта из кеша
     */
    public static void remove(String region, Object key) throws GenericSystemException {
        cacheAccess.remove(region, key, true);
    }

    /**
     * Удаляет из указанного региона кеша объект по его ключу
     *
     * @param block  признак выставления блокировки на объекте кеша, true - блокировать, иначе - false
     * @param region регион кеша
     * @param key    ключ объекта
     * @throws ru.effts.ine.core.GenericSystemException ошибка при удалении объекта из кеша
     */
    public static void remove(String region, Object key, boolean block) throws GenericSystemException {
        cacheAccess.remove(region, key, block);
    }


    /**
     * Проверяет наличие региона
     *
     * @param region регион кеша
     * @return если регион присутствуе - <b>true</b>, иначе - <b>false</b>
     * @throws GenericSystemException ошибка при проверке наличия региона
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public static boolean containsRegion(String region) throws GenericSystemException {
        return cacheAccess.containsRegion(region);
    }

    /**
     * Сбрасывает актуальность всего кеша. <br> <b>Метод не предназначен для прикладной разработки!</b>
     *
     * @throws ru.effts.ine.core.GenericSystemException при ошибке обращения к кешу
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public static void invalidateAll() throws GenericSystemException {
        cacheAccess.invalidateAll();
    }

    /**
     * Сбрасывает актуальность региона кеша. При последующем обращении к региону данные будут актуализированы.
     *
     * @param region регион кеша
     * @throws ru.effts.ine.core.GenericSystemException при ошибке обращения к кешу
     */
    public static void invalidate(String region) throws GenericSystemException {
        cacheAccess.invalidate(region);
    }

    /**
     * Сбрасывает актуальность объекта в регионе кеша.
     *
     * @param region регион кеша
     * @param key    ключ объекта
     * @throws ru.effts.ine.core.GenericSystemException при ошибке обращения к кешу
     */
    public static void invalidate(String region, Object key) throws GenericSystemException {
        cacheAccess.invalidate(region, key);
    }

    /**
     * Производит поиск оюъектов в кеше с применением переданного фильтра
     *
     * @param region        регион поиска
     * @param storageFilter фильтр, устанавливающий условия соответствия критериям поиска
     * @return результат поиска
     * @throws ru.effts.ine.core.GenericSystemException ошибка в процессе поиска
     */
    public static Collection search(String region, StorageFilter storageFilter) throws GenericSystemException {
        return cacheAccess.search(region, storageFilter);
    }

    /**
     * Производит поиск оюъектов в кеше с применением переданного фильтра
     *
     * @param storageFilter фильтр, устанавливающий условия соответствия критериям поиска
     * @return результат поиска
     * @throws ru.effts.ine.core.GenericSystemException ошибка в процессе поиска
     */
    public static Collection search(Collection<StorageFilter> storageFilter) throws GenericSystemException {
        return cacheAccess.search(storageFilter);
    }

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param region регион для поиска
     * @param filter фильтр объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.effts.ine.core.GenericSystemException ошибка в процессе поиска
     */
    public static boolean containsObjects(Object region, StorageFilter filter) throws GenericSystemException {
        return cacheAccess.containsObjects(region, filter);
    }

    /**
     * Производит проверку наличия в хранилище объектов соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return признак наличия объектов соответствующим переданным критериям
     * @throws ru.effts.ine.core.GenericSystemException ошибка в процессе поиска
     */
    public static boolean containsObjects(Collection<StorageFilter> filters) throws GenericSystemException {
        return cacheAccess.containsObjects(filters);
    }

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param region регион для поиска
     * @param filter фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.effts.ine.core.GenericSystemException ошибка в процессе поиска
     */
    public static int getObjectCount(Object region, StorageFilter filter) throws GenericSystemException {
        return cacheAccess.getObjectCount(region, filter);
    }

    /**
     * Возвращает количество объектов в хранилище соответствующим
     * переданным критериям содержащимся в {@link StorageFilter}
     *
     * @param filters фильтры объектов
     * @return количество объектов в хранилище соответствующим переданным критериям
     * @throws ru.effts.ine.core.GenericSystemException ошибка в процессе поиска
     */
    public static int getObjectCount(Collection<StorageFilter> filters) throws GenericSystemException {
        return cacheAccess.getObjectCount(filters);
    }
}
