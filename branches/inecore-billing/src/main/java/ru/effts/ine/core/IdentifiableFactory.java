/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import ru.effts.ine.core.userpermits.SystemUser;

import java.util.HashMap;
import java.util.Map;

/**
 * Для корректной работы фабрики реализации интерфейсов. Фабрика предоставляет возможность инстанционирования сущностей
 * системы по интерфейсам, описывающем их.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IdentifiableFactory.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public final class IdentifiableFactory {

    private static final Map<Class, Class> MAP = new HashMap<Class, Class>();
    private static final Map<Class, Object> INSTANCES_MAP = new HashMap<Class, Object>();

    /** Регион для идентифецируемых */
    public static final String IMPL_MAPPING_REGION = IdentifiableFactory.class.getName() + ".IMPL_MAPPING";
    /** Регион для маркеров */
    public static final String IMPL_SM_MAPPING_REGION = IdentifiableFactory.class.getName() + ".IMPL_SM_MAPPING";

    private IdentifiableFactory() {
    }

    /**
     * Возвращает сконструированный объект, реализующий переданный интерфейс {@link Identifiable}
     *
     * @param identifiableClass интерфейс для конструирования
     * @return готовая реализация
     * @throws GenericSystemException при невозможности конструирования по причинам не зависящим от системы или при
     * передаче некорректного сласса
     */
    public static <T extends Identifiable> T getImplementation(Class<T> identifiableClass)
            throws GenericSystemException {

        return getImplementation(identifiableClass,
                identifiableClass.getName().replaceAll("javax.oss.", "ossj.common.") + "Impl", IMPL_MAPPING_REGION);
    }

    /**
     * Возвращает сконструированный объект, реализующий переданный интерфейс {@link SystemMarker}
     *
     * @param systemMarkerClass интерфейс для конструирования
     * @return готовая реализация
     * @throws GenericSystemException при невозможности конструирования по причинам не зависящим от системы или при
     * передаче некорректного сласса
     */
    public static <E extends SystemMarker> E getImplementationSM(Class<E> systemMarkerClass)
            throws GenericSystemException {

        return getImplementation(systemMarkerClass,
                systemMarkerClass.getName().replaceAll("javax.oss.", "ossj.common.") + "Impl", IMPL_SM_MAPPING_REGION);
    }

    /**
     * Возвращает сконструированный объект, реализующий переданный интерфейс (только поддерживаемые системой)
     *
     * @param otherClass интерфейс для конструирования
     * @return готовая реализация
     * @throws GenericSystemException при невозможности конструирования по причинам не зависящим от системы или при
     * передаче некорректного сласса
     */
//    public static Object getImplementation(Class<?> otherClass) throws GenericSystemException {
//
//        return getImplementation(otherClass,
//                otherClass.getName().replaceAll("javax.oss.", "ossj.common.") + "Impl", IMPL_MAPPING_REGION);
//    }

    @SuppressWarnings({"unchecked", "UnusedDeclaration"})
    private static <M> M getImplementation(Class<M> clazz, String accessClass, String region)
            throws GenericSystemException {

        checkUserIsSet();
        try {
            if (!INSTANCES_MAP.containsKey(clazz)) {
                synchronized (IdentifiableFactory.class) {
                    if (!INSTANCES_MAP.containsKey(clazz)) {
                        MAP.put(clazz, Class.forName(accessClass));
                        INSTANCES_MAP.put(clazz, MAP.get(clazz).newInstance());
                    }
                }
            }

            Object refObj = INSTANCES_MAP.get(clazz);
            if (refObj instanceof Identifiable) {
                return (M) ((Identifiable) INSTANCES_MAP.get(clazz)).clone();
            } else {
                return (M) MAP.get(clazz).newInstance();
            }

        } catch (ClassNotFoundException e) {
            throw new GenericSystemException("Can't Instantiate an Implementation for: [" + clazz.getName() + "]", e);
        } catch (IllegalAccessException e) {
            throw new GenericSystemException("Can't Instantiate an Implementation for: [" + clazz.getName() + "]", e);
        } catch (InstantiationException e) {
            throw new GenericSystemException("Can't Instantiate an Implementation for: [" + clazz.getName() + "]", e);
        } catch (CloneNotSupportedException e) {
            throw new GenericSystemException("Can't Instantiate an Implementation for: [" + clazz.getName() + "]", e);
        }
    }

    /**
     * Возвращает класс интерфейса, класс реализация которого передана методу
     *
     * @param someImplClass класс реализации
     * @return класс родительского интерфейса
     * @throws GenericSystemException при некорректном параметре, невозможности получения класса интейфейса
     * @throws IneIllegalArgumentException при передаче пустой ссылки
     */
    @SuppressWarnings({"unchecked"})
    public static Class getInterface(Class someImplClass) throws GenericSystemException, IneIllegalArgumentException {
        /*
        * Для корректной работы этого механизма обязательным условием является предварительный вызов методоа
        * getImplementation(..), что обязательно происходит при системном обращении к этой фабрике.
        * В сдучае попытки ручного создания экземпляра класса требуемой реализации данный метод
        * не вернет нужного класса интерфейса!
        */

        if (someImplClass == null) {
            throw new IneIllegalArgumentException("Null class not allowed");
        }

        Class result = null;
        try {
            synchronized (IdentifiableFactory.class) {
                if (Identifiable.class.isAssignableFrom(someImplClass)) {
                    result = getClassByImplFromRegion(someImplClass, MAP);
                } else if (SystemMarker.class.isAssignableFrom(someImplClass)) {
                    result = getClassByImplFromRegion(someImplClass, MAP);
                }
            }
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        if (result == null) {
            throw new GenericSystemException("Interface for [" + someImplClass.getName() + "] not found");
        }

        return result;
    }

    /**
     * Получаем класс из региона
     *
     * @param someImplClass реализация для поиска
     * @param map           регион для поиска
     * @return сласс интерфейса
     * @throws CoreException при неполадках в кеше
     */
    private static Class getClassByImplFromRegion(Class someImplClass, Map<Class, Class> map)
            throws CoreException {

        Class result = null;
        if (map != null) {
            for (Map.Entry<? extends Class, ? extends Class> classEntry : map.entrySet()) {
                if (classEntry.getValue().equals(someImplClass)) {
                    result = classEntry.getKey();
                    break;
                }
            }
        }

        return result;
    }

    /**
     * Возвращает класс интерфейса, реализация которого передана методу
     *
     * @param someImpl объект реализации
     * @return класс родительского интерфейса
     * @throws GenericSystemException при некорректном параметре, невозможности получения класса интейфейса
     * @throws IneIllegalArgumentException при передаче пустой ссылки
     */
    public static Class getInterface(Object someImpl) throws GenericSystemException {
        if (someImpl == null) {
            throw new IneIllegalArgumentException("Null object not allowed");
        }

        return getInterface(someImpl.getClass());
    }

    /**
     * Проверяем наличие пользователя и если нет зверька, то кидаем исключение
     *
     * @throws IneIllegalAccessException при отсутствие пользователя
     */
    private static void checkUserIsSet() throws IneIllegalAccessException {

        UserProfile userProfile = UserHolder.getUser();
        if (userProfile == null) {
            throw new IneIllegalAccessException("User not initialised: " + Thread.currentThread().getName());
        }

        SystemUser systemUser = userProfile.getSystemUser();
        if (systemUser == null || !systemUser.isActive()) {
            throw new IneIllegalAccessException("User not initialised");
        }

    }

}
