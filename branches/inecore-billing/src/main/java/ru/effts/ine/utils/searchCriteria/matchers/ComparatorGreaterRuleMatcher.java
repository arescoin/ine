/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import java.util.Comparator;

/**
 * Простой матчер для сравнения на "больше".
 * Количество паттернов: строго один
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ComparatorGreaterRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class ComparatorGreaterRuleMatcher extends AbstractComparatorRuleMatcher {

    public ComparatorGreaterRuleMatcher(Comparator comparator) {
        super(comparator);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        try {
            return comparator.compare(value, pattern[0]) > 0;
        } catch (ClassCastException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
