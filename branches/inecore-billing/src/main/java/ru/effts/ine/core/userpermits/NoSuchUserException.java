/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.GenericSystemException;

/**
 * Выбрасывается если запрашиваемый пользователь не найден
 *
 * @author sfilatov
 * @version 1.0
 * @SVNVersion "$Id: NoSuchUserException.java 3376 2011-11-30 15:10:34Z dgomon $"
 */
public class NoSuchUserException extends GenericSystemException {

    public NoSuchUserException(String message) {
        super(message);
    }

    public NoSuchUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
