/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.dic;

import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Предоставляет методы для описания термина словарной статьи
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: DictionaryTerm.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface DictionaryTerm extends DictionaryEntry {

    String LANG = "langCode";
    String TERM = "term";

    /**
     * Возвращает системный номер языка словарной статьи
     *
     * @return системный номер языка
     */
    BigDecimal getLangCode();

    /**
     * Устанавливает системный номер языка словарной статьи
     *
     * @param langCode системный номер языка
     * @throws ru.effts.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setLangCode(BigDecimal langCode) throws IneIllegalArgumentException;

    /**
     * Возвращает значение словарной статьи (термин)
     *
     * @return термин
     */
    String getTerm();

    /**
     * Устанавливает значение словарной статьи (термин)
     *
     * @param term термин
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setTerm(String term) throws IneIllegalArgumentException;

}
