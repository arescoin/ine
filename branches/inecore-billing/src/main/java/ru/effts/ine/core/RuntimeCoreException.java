/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Корневой класс исключения для всех <i>Runtime</i> исключений в системе.
 * <p/>
 * Все <i>Runtime</i> исключения системы должны расширять этот класс исключения или его наследников.<br> Спецификация
 * языка позволяет не объявлять <i>Runtime</i> исключения в сигнатуре метода, однако, в системе <b>ЛЮБОЙ</b> наследник
 * данного класса, выбрасываемый в методе, <b> должен быть явно оглашен</b> в сигнатуре этого метода!
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RuntimeCoreException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public abstract class RuntimeCoreException extends RuntimeException {

    /**
     * Конструирует новый инстанс {@link ru.effts.ine.core.RuntimeCoreException}, с указанным сообщением
     *
     * @param message детализированное сообщение об ошибке
     */
    public RuntimeCoreException(String message) {
        super(message);
    }

    /**
     * Конструирует новый инстанс {@link ru.effts.ine.core.RuntimeCoreException}, с указанным сообщением
     *
     * @param message детализированное сообщение об ошибке
     * @param cause   причина (более глубокая) возникновения исключения. Пустая ссылка допускается, однако, не
     *                целесообразно пользоваться данным конструктором в ситуации когда известно что ссылка всегда будет
     *                пустой
     */
    public RuntimeCoreException(String message, Throwable cause) {
        super(message, cause);
    }
}