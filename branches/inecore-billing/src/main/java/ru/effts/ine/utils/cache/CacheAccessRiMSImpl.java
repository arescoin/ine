/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.cache;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.utils.config.ConfigurationManager;
import ru.effts.rims.common.Configuration;
import ru.effts.rims.common.ServerArray;
import ru.effts.rims.ra.cci.RiMSLocalConnectionFactory;
import ru.effts.rims.services.RemoteStorage;
import ru.effts.rims.services.StorageFilter;
import ru.effts.rims.services.TimeoutException;

import java.io.*;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CacheAccessRiMSImpl.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public class CacheAccessRiMSImpl implements CacheAccess {

    /** Ключ конфига определяющий порог включения порционной передачи содержимого региона */
    public static final String REGION_SIZE_TRANSFER_THRESHOLD_KEY = "REGION_SIZE_TRANSFER_THRESHOLD";

    /** Значение по-умолчанию порога включения порционной передачи содержимого региона */
    public static final int DEFAULT_REGION_SIZE_TRANSFER_THRESHOLD = 40000;

    /** Ключ конфига определяющий размер фрагмента при порционной передачи содержимого региона */
    public static final String REGION_TRANSFER_SIZE_KEY = "REGION_TRANSFER_SIZE";

    /** Значение по-умолчанию для размера фрагмента при порционной передачи содержимого региона */
    public static final int DEFAULT_REGION_TRANSFER_SIZE = 20000;

    /** Потоковый холдер для соединения с кешом при автономной работе */
    private static ThreadLocal<RemoteStorage> connectionThreadLocal = new ThreadLocal<>();

    /** Ключ конфига определяющий признак автономной работы */
    public static final String STANDALONE_KEY = CacheAccessRiMSImpl.class.getName() + ".STANDALONE";

    /** Признак автономной работы */
    private static final boolean STANDALONE =
            Boolean.TRUE.toString().equals(System.getProperty(STANDALONE_KEY, "false"));


    private static Logger logger = Logger.getLogger(CacheAccessRiMSImpl.class.toString());

    private int regionSizeTransferThreshold;
    private int regionTransferSize;

    public CacheAccessRiMSImpl() {

        // зачитываем и устанавливаем значение для порога
        regionSizeTransferThreshold =
                prepareIntParam(REGION_SIZE_TRANSFER_THRESHOLD_KEY, DEFAULT_REGION_SIZE_TRANSFER_THRESHOLD);

        // зачитываем и устанавливаем значение для размера фрагмента
        regionTransferSize = prepareIntParam(REGION_TRANSFER_SIZE_KEY, DEFAULT_REGION_TRANSFER_SIZE);

        // если конфиг, в первом приближении смахивает на бред...
        if (regionSizeTransferThreshold < regionTransferSize || regionSizeTransferThreshold < 1000) {

            regionSizeTransferThreshold = DEFAULT_REGION_SIZE_TRANSFER_THRESHOLD;
            regionTransferSize = DEFAULT_REGION_TRANSFER_SIZE;

            logger.warning("Incorrect region transfering configuration, will use default values");
        }

    }

    private int prepareIntParam(String confgKey, int defaultValue) {

        int result = defaultValue;

        final String stringValue = ConfigurationManager.getManager().getRootConfig()
                .getValueByKey(this.getClass().getName() + '.' + confgKey);

        if (stringValue != null && stringValue.length() > 0) {

            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Reading config for [{0}]", confgKey);
            }

            try {
                result = Integer.parseInt(stringValue);

                if (logger.isLoggable(Level.FINE)) {
                    logger.log(Level.FINE, "Reading is done key [{0}], val [{1}]", new Object[]{confgKey, result});
                }
            } catch (NumberFormatException e) {
                if (logger.isLoggable(Level.WARNING)) {
                    logger.log(Level.WARNING,
                            "Wrong value for {0} [{1}], using default [{2}]",
                            new Object[]{confgKey, stringValue, DEFAULT_REGION_SIZE_TRANSFER_THRESHOLD});
                }
            }
        } else {
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "Config for [{0}], does not presented, using default [{1}]",
                        new Object[]{confgKey, defaultValue});
            }
        }

        return result;
    }

    private RemoteStorage obtainConnection() {
        RemoteStorage result;
        try {
            if (STANDALONE) {
                if (connectionThreadLocal.get() == null) {

                    boolean isInit = Configuration.isInitialized();
                    logger.config("Configuration.isInitialized(): " + isInit);

                    if (!isInit) {
                        String additionalPath = System.getProperty(ConfigurationManager.SYSTEM_CONFIC_PATH_PROPERY, "");
                        if (!additionalPath.isEmpty()) {
                            additionalPath = additionalPath + "/";
                        }
                        Configuration.initialize(additionalPath + "conf/servers.properties");
                    }

                    ServerArray.Server server = ServerArray.getArray().getCurrent();
                    server.initRegistry(true);
                    connectionThreadLocal.set(server.getRemoteCache());
                }
                result = connectionThreadLocal.get();
            } else {
                result = RiMSLocalConnectionFactory.getConnection();
            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }

        return result;
    }

    @Override
    public boolean containsRegion(String region) throws GenericSystemException {
        try {
            return obtainConnection().hasRegion(region);
        } catch (RemoteException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public Object get(String region, Object key, Producer producer) throws CoreException {
        try {
            Object result = obtainConnection().get(region, key);
            if (result == null) {
                result = producer.get(key);
                if (result != null) {
                    obtainConnection().put(region, key, result);   // todo: здесь повторное размещение данных !!!
                }
            }
            return result;
        } catch (Exception e) {
            throw new RuntimeException("Params: region[" + region
                    + "]; key[" + key + "]; producer["
                    + producer + "], and message: " + e.getMessage(), e);
        }
    }

    @Override
    public Map getRegionContent(String region, Producer producer) throws CoreException {
        return getRegionContent(region, producer, false);
    }

    @Override
    public Map getRegionContent(String region, Producer producer, boolean block) throws CoreException {

        boolean inLock = false;
        final RemoteStorage remoteStorage = obtainConnection();

        try {

            if (!STANDALONE) {
                RiMSLocalConnectionFactory.parkConnection(remoteStorage);
            }

            Map result = (Map) ByteConverter.bytesToObject(remoteStorage.getRegionContentAsBytes(region));
            if (result.isEmpty()) {
                remoteStorage.lock(region, region);
                inLock = true;
                result = (Map) ByteConverter.bytesToObject(remoteStorage.getRegionContentAsBytes(region));
                if (result.isEmpty()) {
                    result = (Map) producer.get(region);
                    if (result != null) {

                        if (result.size() > regionSizeTransferThreshold) {
                            putMapPartially(region, result, remoteStorage);
                        } else {
                            remoteStorage.putMap(region, ByteConverter.objectToBytes(result));
                        }
                    }

                }
            }

            return result;

        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            if (remoteStorage != null) {
                try {
                    if (inLock) {
                        remoteStorage.unlock(region, region);
                    }
                    if (!STANDALONE) {
                        RiMSLocalConnectionFactory.unParkConnection(remoteStorage);
                    }
                } catch (Exception e) {
                    logger.log(Level.WARNING, "Error while storage unlocking", e);
                }
            }
        }
    }

    @SuppressWarnings({"unchecked"})
    private void putMapPartially(CharSequence region, Map<?, ?> content, RemoteStorage remoteStorage)
            throws RemoteException, TimeoutException {

        if (logger.isLoggable(Level.INFO)) {
            logger.log(Level.INFO, "Put Partially cache: Region [{0}], Items count [{1}]",
                    new Object[]{region, content.size()});
        }

        Map partOfData = new HashMap();

        for (Map.Entry entry : content.entrySet()) {
            partOfData.put(entry.getKey(), entry.getValue());
            if (partOfData.size() >= regionTransferSize) {
                remoteStorage.putMap(region, ByteConverter.objectToBytes(partOfData));

                if (logger.isLoggable(Level.INFO)) {
                    logger.log(Level.INFO, "Put Partially cache: Region [{0}], Items count [{1}]",
                            new Object[]{region, partOfData.size()});
                }

                partOfData = new HashMap();
            }
        }

        if (partOfData.size() > 0) {
            remoteStorage.putMap(region, ByteConverter.objectToBytes(partOfData));
            if (logger.isLoggable(Level.INFO)) {
                logger.log(Level.INFO, "Put Partially cache: Region [{0}], Items count [{1}]",
                        new Object[]{region, partOfData.size()});
            }
        }

    }

    @Override
    public Set<Object> getKeys(String region) throws CoreException {
        try {
            return obtainConnection().getKeys(region);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public Object get(String region, Object key, Producer producer, boolean block) throws CoreException {
        return this.get(region, key, producer);
    }

    @Override
    public Object put(String region, Object key, Object value) throws GenericSystemException {
        try {
            obtainConnection().put(region, key, value);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        return null;
    }

    @Override
    public Object put(String region, Object key, Object value, boolean block) throws GenericSystemException {
        this.put(region, key, value);
        return null;
    }

    @Override
    public void put(String region, Map values) throws GenericSystemException {
        try {
            obtainConnection().putMap(region, ByteConverter.objectToBytes(values));
//            obtainConnection().put(region, values);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void put(String region, Map values, boolean block) throws GenericSystemException {
        put(region, values);
    }

    @Override
    public void remove(String region, Object key) throws GenericSystemException {
        try {
            obtainConnection().remove(region, key);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void remove(String region, Object key, boolean block) throws GenericSystemException {
        this.remove(region, key);
    }

    @Override
    public void invalidateAll() throws GenericSystemException {
        try {
            Set set = obtainConnection().getRegions();
            for (Object o : set) {
                obtainConnection().removeRegion(o);
            }
        } catch (RemoteException | TimeoutException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void invalidate(String region) throws GenericSystemException {
        try {
            obtainConnection().clearRegion(region);
        } catch (RemoteException | TimeoutException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public void invalidate(String region, Object key) throws GenericSystemException {
        this.remove(region, key);
    }

    public Collection search(String region, StorageFilter storageFilter) {
        try {
            return obtainConnection().searchObjects(region, storageFilter);
        } catch (RemoteException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    public Collection search(Collection<StorageFilter> storageFilters) {
        try {
            return obtainConnection().searchObjects(storageFilters);
        } catch (RemoteException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public boolean containsObjects(Object region, StorageFilter filter) throws GenericSystemException {
        try {
            return obtainConnection().containsObjects(region, filter);
        } catch (RemoteException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public boolean containsObjects(Collection<StorageFilter> filters) throws GenericSystemException {
        try {
            return obtainConnection().containsObjects(filters);
        } catch (RemoteException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public int getObjectCount(Object region, StorageFilter filter) throws GenericSystemException {
        try {
            return obtainConnection().getObjectCount(region, filter);
        } catch (RemoteException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    public int getObjectCount(Collection<StorageFilter> filters) throws GenericSystemException {
        try {
            return obtainConnection().getObjectCount(filters);
        } catch (RemoteException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    static class ByteConverter {

        public static byte[] objectToBytes(Object object) throws RemoteException {
            byte[] resultBytes;
            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
                objectOutputStream.writeObject(object);
                resultBytes = outputStream.toByteArray();

                objectOutputStream.flush();
                objectOutputStream.close();

                outputStream.flush();
                outputStream.close();

                return resultBytes;

            } catch (IOException e) {
                throw new RemoteException("Error while object preparation", e);
            }
        }

        public static Object bytesToObject(byte[] values) throws RemoteException {

            try {
                ByteArrayInputStream byteInputStream = new ByteArrayInputStream(values);
                ObjectInputStream objectInputStream = new ObjectInputStream(byteInputStream);

                return objectInputStream.readObject();
            } catch (IOException | ClassNotFoundException e) {
                throw new RemoteException("Can't read object", e);
            }
        }
    }
}
