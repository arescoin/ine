/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Сигнализирует о попытке модифицировать неактуальную версию объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneNotActualVersionModificationException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public class IneNotActualVersionModificationException extends GenericSystemException {

    private final Versionable currentVersion;

    /**
     * Конструирует инстанс, содержащий актуальную версию объекта
     *
     * @param versionable актуальная версия объекта, или его последняя реализация.
     * @param message     сообщение об ошибке
     */
    public IneNotActualVersionModificationException(Versionable versionable, String message) {
        super(message);
        this.currentVersion = versionable;
    }

    /**
     * Возвращает текущую версию объекта, послужившую причиной исключению
     *
     * @return текущая реализация
     */
    public Versionable getCurrentVersion() {
        return currentVersion;
    }

}
