/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Корневой класс для всех имеющих место быть исключений в системе.
 * <p/>
 * Все не <i>Runtime</i> исключения системы должны расширять этот класс исключения или его наследников.<br>
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CoreException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public abstract class CoreException extends Exception {


    /**
     * Конструирует новый инстанс {@link ru.effts.ine.core.CoreException}, с указанным сообщением
     *
     * @param message детализированное сообщение об ошибке
     */
    public CoreException(String message) {
        super(message);
    }

    /**
     * Конструирует новый инстанс {@link ru.effts.ine.core.CoreException}, с указанным сообщением
     *
     * @param message детализированное сообщение об ошибке
     * @param cause   причина (более глубокая) возникновения исключения. Пустая ссылка допускается, однако, не
     *                целесообразно пользоваться данным конструктором в ситуации когда известно что ссылка всегда будет
     *                пустой
     */
    public CoreException(String message, Throwable cause) {
        super(message, cause);
    }
}
