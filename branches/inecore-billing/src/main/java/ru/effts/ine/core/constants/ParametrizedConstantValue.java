/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Интерфейс описывает методы доступные для объекта-хранилища значения параметризованной константы
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstantValue.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface ParametrizedConstantValue extends ConstantValue {

    String PARAM = "param";

    /**
     * Возвращает идентификатор параметра константы
     *
     * @return иденитфикатор параметра константы
     */
    BigDecimal getParam();

    /**
     * Возвращает идентификатор параметра константы
     *
     * @param param идентификатор параметра константы
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setParam(BigDecimal param) throws IneIllegalArgumentException;

}
