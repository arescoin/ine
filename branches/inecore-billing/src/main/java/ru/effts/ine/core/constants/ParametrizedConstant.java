/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.structure.SystemObjectPattern;

import java.math.BigDecimal;

/**
 * Параметризованный конфигурационный параметр системы.
 * <p/>
 * Предоставляет возможности гибкой настройки значений по вариантам (паттерн системного объекта) применения.<br>
 * Применяется для настройки функциональности, изменения параметров работы программных модулей.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ParametrizedConstant.java 3861 2014-09-23 14:33:49Z DGomon $"
 */

public interface ParametrizedConstant extends Constant {

    BigDecimal TYPE = new BigDecimal(2);

    String MASK = "mask";

    /**
     * Возвращает паттерн системного объекта для параметризации константы
     *
     * @return паттерн системного объекта для параметризации константы
     */
    SystemObjectPattern getMask();

    /**
     * Возвращает паттерн системного объекта для параметризации константы
     *
     * @param mask паттерн системного объекта для параметризации константы
     *
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setMask(SystemObjectPattern mask) throws IneIllegalArgumentException;

}
