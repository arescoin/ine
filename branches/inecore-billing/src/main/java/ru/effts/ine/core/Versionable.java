/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import java.util.Date;

/**
 * Интерфейс предназначен для реализации поддержки версионных объектов.
 * <p/>
 * Все объекты поддерживающие версионность обязаны реализовывать данный интерфейс.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Versionable.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface Versionable extends Identifiable {

    String CORE_FD = "coreFd";
    String CORE_TD = "coreTd";

    /**
     * Устанавливает дату начала "жизни" версии объекта
     *
     * @param fd начало"жизни" версии объекта
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setCoreFd(Date fd) throws IneIllegalArgumentException;

    /**
     * Возвращает дату начала "жизни" версии объекта
     *
     * @return начало "жизни" версии объекта
     */
    Date getCoreFd();

    /**
     * Устанавливает дату завершения "жизни" версии объекта
     *
     * @param td окончание "жизни" версии объекта
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setCoreTd(Date td) throws IneIllegalArgumentException;

    /**
     * Возвращает дату завершения "жизни" версии объекта
     *
     * @return окончание "жизни" версии объекта
     */
    Date getCoreTd();

}
