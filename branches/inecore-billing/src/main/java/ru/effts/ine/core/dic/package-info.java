/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

/**
 * Пакет предоставляет набор интерфейсов для работы с подсистемой словарей.
 * <p>
 * <b>Словари</b> - это наборы терминов предоставляющие возможность хранить уникальные, в приделах набора,
 * словарные статьи и ассоциированные с ними коды словарных статей.
 *<p>
 * Словари подразделяются на несколько типов.<br>
 *
 * По роли в системе:
 * <ul>
 *  <li> <i>Системные</i>
 *  <li> <i>Административные</i>
 *  <li> <i>Пользовательские</i>
 * </ul>
 *
 * По типу присвоения идентификаторов словарным статьям:
 * <ul>
 *  <li> <i>Простые</i>
 *  <li> <i>Бинарные</i>
 * </ul>
 *
 * По уровню локализации
 * <ul>
 *  <li> <i>Требует заполнения словарных статей во всех системных языках</i>
 *  <li> <i>Достаточно указать термин в основном системном языке</i>
 * </ul>
 * @since 1.0
 * @SVNVersion "$Id: package-info.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
package ru.effts.ine.core.dic;
