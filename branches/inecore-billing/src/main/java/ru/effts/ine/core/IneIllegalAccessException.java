/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Выбрасывается при некорректном состоянии пользователя в системе.
 * <p/>
 * Основное предназначение - сигнализировать об отсутствие пользователя, либо о недостаточном наборе привилегий
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneIllegalAccessException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public class IneIllegalAccessException extends RuntimeCoreException {

    /**
     * Конструирует новое исключение с указанным сообщением
     *
     * @param message сообщение об ошибке
     */
    public IneIllegalAccessException(String message) {
        super(message);
    }

}
