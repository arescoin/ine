/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.Versionable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Значение атрибута.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeValue.java 3375 2011-11-30 14:21:39Z dgomon $"
 */
public interface CustomAttributeValue extends Versionable {

    ValueComparator VALUE_COMPARATOR = new ValueComparator();

    String ENT_N = "entityId";
    String VALUE = "value";

    /**
     * Получает идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     *
     * @return идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     */
    BigDecimal getEntityId();

    /**
     * Устанавливает идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     *
     * @param entityId идентификатор экземпляра {@link Versionable объекта} к которому привязан данный атрибут.
     * @throws ru.effts.ine.core.IllegalIdException если значение null или меньше
     * {@link #MIN_ALLOWABLE_VAL минимально допустимого значения}
     */
    void setEntityId(BigDecimal entityId) throws IllegalIdException;

    /**
     * Получает значение атрибута.
     *
     * @return значение атрибута.
     */
    Object getValue();

    /**
     * Устанавливает значение атрибута.
     *
     * @param value значение атрибута.
     */
    void setValue(Object value);

    class ValueComparator implements Comparator, Serializable {
        @SuppressWarnings({"unchecked"})
        @Override
        public int compare(Object o1, Object o2) {
            return ((Comparable) o1).compareTo(o2);
        }
    }
}
