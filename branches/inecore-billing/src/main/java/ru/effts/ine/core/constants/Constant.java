/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Конфигурационный параметр системы.
 * <p/>
 * Простой конфигурационный параметр,<br> не предоставляет возможности гибкой настройки значений по вариантам
 * применения.<br> Применяется для настройки функциональности, изменения параметров работы программных модулей.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Constant.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public interface Constant extends Versionable {

    String NAME = "name";
    String NULLABLE = "nullable";
    String MODIFABLE = "modifiable";
    String DEFAULT_VALUE = "defaultValue";

    BigDecimal TYPE = BigDecimal.ONE;


    /**
     * Возвращает читабельное наименование константы, используется исключительно для визуального представления, однако,
     * обязательно для заполнения.
     *
     * @return наименование константы
     */
    String getName();

    /**
     * Устанавливает читабельное наименование константы, используется исключительно для визуального представления,
     * однако, обязательно для заполнения.
     *
     * @param name наименование константы
     *
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          если name - NULL или пустая строка
     */
    void setName(String name) throws IneIllegalArgumentException;

    /**
     * Возвращает идентификатор типа константы
     *
     * @return идентфикатор типа константы
     */
    BigDecimal getType();

    /**
     * Устанавливает идентификатор типа константы
     *
     * @param type идентификатор типа константы
     *
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *          если type - NULL
     */
//    void setType(BigDecimal type) throws IneIllegalArgumentException;

    /**
     * Возвращает сконфигурированное в системе значение по умолчанию
     *
     * @return значение по умолчанию
     */
    String getDefaultValue();

    /**
     * Возвращает сконфигурированное в системе значение по умолчанию
     *
     * @param defaultValue значение по умолчанию
     */
    void setDefaultValue(String defaultValue);

    /**
     * Возвращает признак допустимости отсутствия значения для данной константы
     *
     * @return <code>true</code> - пустое значение допустимо, иначе - <code>false</code>
     */
    boolean isNullable();

    /**
     * Устанавливает признак допустимости отсутствия значения для данной константы
     *
     * @param nullable <code>true</code> - пустое значение допустимо, иначе - <code>false</code>
     */
    void setNullable(boolean nullable);

    /**
     * Возвращает признак допустимости изменения значения данной константы
     *
     * @return <code>true</code> - менять можно, иначе - <code>false</code>
     */
    boolean isModifiable();

    /**
     * Устанавливает признак допустимости изменения значения данной константы
     *
     * @param modifable <code>true</code> - менять можно, иначе - <code>false</code>
     */
    void setModifiable(boolean modifable);

}
