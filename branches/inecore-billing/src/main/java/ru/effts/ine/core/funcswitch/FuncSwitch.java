/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.funcswitch;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Значение переключателя состояния функциональности
 * <p/>
 * Предоставляет методы для работы с механизмом переключения функциональности
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: FuncSwitch.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface FuncSwitch extends Versionable {

    String C_DATE = "changeDate";
    String STATE = "enabled";
    String MARK = "mark";
    String STATE_CODE = "stateCode";

    /**
     * Возвращает признак активности переключателя (состояние)
     *
     * @return состояние, включено - <code><b>true</b></code>, иначе - <code><b>false</b></code>
     */
    boolean isEnabled();

    /**
     * Устанавливает признак активности переключателя (состояние)
     *
     * @param enabled состояние, включено - <code><b>true</b></code>, иначе - <code><b>false</b></code>
     */
    void setEnabled(boolean enabled);

    /**
     * Возвращает дату изменения состояния переключателя
     *
     * @return дата изменения состояния переключателя
     */
    public Date getChangeDate();

    /**
     * Устанавливает дату изменения состояния переключателя
     *
     * @param changeDate дата изменения состояния переключателя
     * @throws ru.effts.ine.core.IneIllegalArgumentException в случае null-значений
     */
    public void setChangeDate(Date changeDate) throws IneIllegalArgumentException;

    /**
     * Возвращает контрольную метку смены состояния переключателя.
     * <p/>
     * Контрольная метка состояния переключателя используется для верификации валидности состояния.
     *
     * @return контрольная метка смены состояния переключателя
     */
    BigDecimal getMark();

    /**
     * Устанавливает контрольную метку смены состояния переключателя.
     * <p/>
     * Контрольная метка состояния переключателя используется для верификации валидности состояния.
     *
     * @param mark контрольная метка смены состояния переключателя
     */
    void setMark(BigDecimal mark);

    /**
     * Возвращает код состояния переключателя
     * <p/>
     * Код состояния используется для валидации состояния
     *
     * @return код состояния переключателя
     */
    String getStateCode();

    /**
     * Устанавливает код состояния переключателя
     * <p/>
     * Код состояния используется для валидации состояния
     *
     * @param stateCode код состояния переключателя
     */
    void setStateCode(String stateCode);

}
