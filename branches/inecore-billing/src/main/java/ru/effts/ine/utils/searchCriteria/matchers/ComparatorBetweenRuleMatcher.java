/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import java.util.Comparator;

/**
 * Простой матчер для поиска в диапозоне.
 * Количество паттернов: строго два
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ComparatorBetweenRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class ComparatorBetweenRuleMatcher extends AbstractComparatorRuleMatcher{

    public ComparatorBetweenRuleMatcher(Comparator comparator) {
        super(comparator);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        try {
            return comparator.compare(value, pattern[0]) >= 0 && comparator.compare(value, pattern[1]) <= 0;
        } catch (ClassCastException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
