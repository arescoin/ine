/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Интерфейс предназначен для поддержки работы с уникально идентифицируемыми объектами.
 * <p/>
 * Все наследники должны предоставлять дополнение к перечислению содержащихся полей
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Identifiable.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface Identifiable extends Serializable, Cloneable {

    /** Минимально доустимый идентификатор в системе */
    BigDecimal MIN_ALLOWABLE_VAL = BigDecimal.ONE;

    /** Перечисление полей */
    String CORE_ID = "coreId";
    String CORE_DSC = "coreDsc";

    /**
     * Устанавливает уникальный идентификатор для объекта
     *
     * @param id уникальный идентификатор
     * @throws IllegalIdException при попытке передачи в качестве параметра пустой ссылки, пустого значения, или
     * значения не удовлетворяющего требованиям к идентификатору
     */
    void setCoreId(BigDecimal id) throws IllegalIdException;

    /**
     * Возвращает уникальный идентификатор объекта
     *
     * @return уникальный идентификатор объекта
     */
    BigDecimal getCoreId();

    /**
     * Возвращает описание для идентифицируемой сущности
     *
     * @return пояснение
     */
    String getCoreDsc();

    /**
     * Задает описание для идентифицируемой сущности
     *
     * @param dsc пояснение
     */
    void setCoreDsc(String dsc);

    Object clone() throws CloneNotSupportedException;
}
