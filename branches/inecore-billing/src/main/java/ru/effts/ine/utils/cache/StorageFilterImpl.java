/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.cache;

import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.RuleMatcher;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.ine.utils.searchCriteria.matchers.AbstractComparatorRuleMatcher;
import ru.effts.rims.services.StorageFilter;
import ru.effts.cache.IndexHashMap;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Реализация фильтра, отправляемого на строну RiMS
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: StorageFilterImpl.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public class StorageFilterImpl implements StorageFilter {

    /*
     ****************************************************************************************************
     * Внимание! При доработке данного класса не следует забывать о том что этот код будет отрабатывать
     * на стороне кеш-сервера, а по сему, не надо пихать в класс лишние объекты!!!
     ****************************************************************************************************
     */

    private static Logger logger = Logger.getLogger(StorageFilterImpl.class.getName());

    private Object regionKey;

    private boolean firstOnly = false;

    private SearchCriterion criterionArray[];

    private SearchCriterion criterion;

    /** Поле содержащее значение используемое для подготовки очередного поискового критерия в сложном запросе */
    private String fieldName;


    public StorageFilterImpl(Set<SearchCriterion> searchCriteria, Object regionKey) {
        this.criterionArray = searchCriteria.toArray(new SearchCriterion[searchCriteria.size()]);
        this.regionKey = regionKey;
    }

    public StorageFilterImpl(Set<SearchCriterion> searchCriteria, String fieldName, Object regionKey) {
        this.criterionArray = searchCriteria.toArray(new SearchCriterion[searchCriteria.size()]);
        this.fieldName = fieldName;
        this.regionKey = regionKey;
    }

    public StorageFilterImpl(Set<SearchCriterion> searchCriteria,
            SearchCriterion criterion, String fieldName, Object regionKey) {

        this.criterionArray = searchCriteria.toArray(new SearchCriterion[searchCriteria.size()]);
        this.criterion = criterion;
        this.fieldName = fieldName;
        this.regionKey = regionKey;
    }

    public StorageFilterImpl(StorageFilterImpl filter, Object regionKey) {
        this.criterionArray = filter.getCriterionArray();
        this.criterion = filter.getCriterion();
        this.fieldName = filter.getFieldName();
        this.regionKey = regionKey;

        this.firstOnly = filter.firstOnly;
    }

    private SearchCriterion[] getCriterionArray() {
        return criterionArray;
    }

    private SearchCriterion getCriterion() {
        return criterion;
    }

    private String getFieldName() {
        return fieldName;
    }

    @Override
    public void setFirstOnly(boolean firstOnly) {
        this.firstOnly = firstOnly;
    }

    @Override
    public Object getRegionKey() {
        return regionKey;
    }

    /**
     * Возвращает первый поисковый критерий.<br>
     * Метод вернёт либо первый критерий из коллекции критериев, если она не пуста, либо дополнительный критерий.<br>
     * Метод используется для получения поискового класса и дополнительных параметров.
     *
     * @return первый поисковый критерий
     */
    public SearchCriterion getFirstSearchCriterion() {

        if (this.criterionArray.length > 0) {
            return this.criterionArray[0];
        } else if (criterion != null) {
            return criterion;
        }

        return null;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void prepareCriterionValues(Collection collection) {

        final int size = this.criterionArray.length;

        for (Object o : collection) {
            this.criterion.addCriteriaVal(o);
        }
        if (size > 0) {
            SearchCriterion firstCriterion = this.criterionArray[0];
            if (!firstCriterion.getAdditionalParams().isEmpty() && this.criterion.getAdditionalParams().isEmpty()) {
                List<Serializable> params = firstCriterion.getAdditionalParams();
                for (Serializable param : params) {
                    this.criterion.addAdditionalParam(param);
                }
            }
        }

        SearchCriterion[] criteria = new SearchCriterion[size + 1];
        System.arraycopy(this.criterionArray, 0, criteria, 1, size);
        criteria[0] = this.criterion;
        this.criterionArray = criteria;
/*
        this.criterionArray = Arrays.copyOf(this.criterionArray, size + 1);
        this.criterionArray[size] = criterion;
*/
    }

    @Override
    public boolean isExtractionRequiered() {
        return fieldName != null && !fieldName.isEmpty();
    }

    @Override
    public Collection extractValues(Collection collection) {

        // todo: переделать на Extractor
        Collection<Object> result = new HashSet<>();

        if (fieldName == null) {
            // означает что фильтр сформирован некорректно
            throw new IllegalStateException("fieldName for extraction not defined");
        }

        if (fieldName.isEmpty()) {
            // если имя пустое, то не надо извлекать значения
            return result;
        }

        Method method = null;
        try {
            // Перебираем переданную коллекцию и выбираем значение необходимого атрибута
            for (Object o : collection) {
                if (method == null) {
                    BeanInfo beanInfo = Introspector.getBeanInfo(o.getClass(), Object.class);
                    PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
                    for (PropertyDescriptor propertyDescriptor : propertyDescriptors) {
                        if (fieldName.equals(propertyDescriptor.getName())) {
                            method = propertyDescriptor.getReadMethod();
                            break;
                        }
                    }

                    if (method == null) {
                        throw new IllegalStateException("Method accessor for [" + fieldName + "] not found, " +
                                "ObjectType [" + o.getClass().getName() + "]");
                    }
                }

                result.add(method.invoke(o));
            }
        } catch (IntrospectionException | InvocationTargetException | IllegalAccessException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return result;
    }

    @Override
    public Collection filter(Collection collection) {
        return this.filter(null, collection, new ArrayList(0), new ArrayList(0));
    }

    @Override
    public Collection filter(Map<String, Map<Object, List<Object>>> indexes,
            Collection source, Collection temporary, Collection exclusions) {

        Collection<Object> result = new LinkedList<>();

        if (indexes != null && !indexes.isEmpty()) {

            this.filterByIndexes(indexes, source, result);

            if (firstOnly) {
                if (result.isEmpty()) {
                    this.filterWithCheck(temporary, result, exclusions);
                }
            } else {
                this.filterNoCheck(temporary, result);
            }

            for (Object obj : exclusions) {
                if (result.contains(obj)) {
                    result.remove(obj);
                }
            }

        } else {
            if (firstOnly) {
                this.filterWithCheck(source, result, exclusions);
                if (result.isEmpty()) {
                    this.filterWithCheck(temporary, result, exclusions);
                }
            } else {
                this.filterNoCheck(source, result);
                this.filterNoCheck(temporary, result);

                for (Object obj : exclusions) {
                    if (result.contains(obj)) {
                        result.remove(obj);
                    }
                }
            }
        }

        return result;
    }

    private void filterByIndexes(Map<String, Map<Object, List<Object>>> indexes,
            Collection source, Collection<Object> result) {

        logger.config("filter by index");

        SearchCriterion searchCriterion;
        String fieldName;

        for (int i = 0, criterionArrayLength = criterionArray.length; i < criterionArrayLength; i++) {

            searchCriterion = criterionArray[i];
            fieldName = searchCriterion.getFieldName();

            if (indexes.containsKey(fieldName)
                    && CriteriaRule.equals.equals(searchCriterion.getCriterionRule()) && !searchCriterion.isInvert()) {

                this.processRuleEquals(result, searchCriterion, indexes.get(fieldName), i == 0);

            } else if (indexes.containsKey(fieldName) &&
                    CriteriaRule.in.equals(searchCriterion.getCriterionRule()) && !searchCriterion.isInvert()) {

                this.processRuleIn(result, searchCriterion, indexes.get(fieldName), i == 0);

            } else if (indexes.containsKey(fieldName) &&
                    CriteriaRule.isNull.equals(searchCriterion.getCriterionRule()) && !searchCriterion.isInvert()) {

                this.processRuleIsNull(result, indexes.get(fieldName), i == 0);

            } else {

                this.processRuleOthers(source, result, searchCriterion);

            }

            if (result.isEmpty()) {
                return;
            }
        }
    }

    private void processRuleEquals(Collection<Object> result, SearchCriterion criterion,
            Map<Object, List<Object>> objectsByIndexedValues, boolean firstCriterion) {

        if (objectsByIndexedValues == null || criterion.getCriterionVals().length == 0) {
            result.clear();
            return;
        }

        RuleMatcher ruleMatcher = this.getRuleMatcher(criterion);
        boolean hasComparator = AbstractComparatorRuleMatcher.class.isAssignableFrom(ruleMatcher.getClass());

        Collection<Object> temp = null;
        if (hasComparator && criterion.isUseRuleMatcher()) {
            for (Object key : objectsByIndexedValues.keySet()) {
                if (ruleMatcher.match(key, criterion.getCriterionVals())) {
                    temp = objectsByIndexedValues.get(key);
                }
            }
        } else {
            Object firstVal = criterion.getCriterionVals()[0];
            temp = objectsByIndexedValues.get(firstVal);
        }

        if (temp == null || temp.isEmpty()) {
            result.clear();
        } else {
            if (firstCriterion) {
                result.addAll(temp);
            } else {
                result.retainAll(temp);
            }
        }

    }

    @SuppressWarnings({"unchecked"})
    private void processRuleIn(Collection<Object> result, SearchCriterion criterion,
            Map<Object, List<Object>> objectsByIndexedValues, boolean firstCriterion) {

        if (objectsByIndexedValues == null || criterion.getCriterionVals().length == 0) {
            result.clear();
            return;
        }

        Object[] keys = ((IndexHashMap) objectsByIndexedValues).getKeys();

        RuleMatcher ruleMatcher = this.getRuleMatcher(criterion);
        boolean hasComparator = AbstractComparatorRuleMatcher.class.isAssignableFrom(ruleMatcher.getClass());

        Collection<Object> temp = new HashSet<>();
        if (hasComparator && criterion.isUseRuleMatcher()) {
            for (Object key : keys) {
                if (ruleMatcher.match(key, criterion.getCriterionVals())) {
                    temp.addAll(objectsByIndexedValues.get(key));
                }
            }
        } else {
            for (Object value : criterion.getCriterionVals()) {
                if (objectsByIndexedValues.containsKey(value)) {
                    temp.addAll(objectsByIndexedValues.get(value));
                }
            }
        }

        if (temp.isEmpty()) {
            result.clear();
        } else {
            if (firstCriterion) {
                result.addAll(temp);
            } else {
                result.retainAll(temp);
            }
        }
    }

    private void processRuleIsNull(Collection<Object> result,
            Map<Object, List<Object>> objectsByIndexedValues, boolean firstCriterion) {

        if (objectsByIndexedValues != null && objectsByIndexedValues.containsKey(null)) {
            if (firstCriterion) {
                result.addAll(objectsByIndexedValues.get(null));
            } else {
                result.retainAll(objectsByIndexedValues.get(null));
            }
        }
    }

    private void processRuleOthers(Collection source, Collection<Object> result, SearchCriterion searchCriterion) {

        if (result.isEmpty()) {
            for (Object o : source) {
                if (this.applyCriterionToElement(searchCriterion, o)) {
                    result.add(o);
                }
            }
        } else {
            for (Iterator<Object> iterator = result.iterator(); iterator.hasNext();) {
                if (!this.applyCriterionToElement(searchCriterion, iterator.next())) {
                    iterator.remove();
                }
            }
        }
    }

    private void filterWithCheck(Collection source, Collection<Object> destination, Collection exclusions) {
        for (Object obj : source) {
            if (!exclusions.contains(obj) && this.applyCriterianToElement(obj)) {
                destination.add(obj);
                break;
            }
        }
    }

    private void filterNoCheck(Collection source, Collection<Object> destination) {
        for (Object obj : source) {
            if (this.applyCriterianToElement(obj)) {
                destination.add(obj);
            }
        }
    }

    private boolean applyCriterionToElement(SearchCriterion criterion, Object obj) {

        boolean matched = false;
        Object fieldValue;
        RuleMatcher matcher;

        //noinspection unchecked
        if (obj != null && criterion != null && criterion.getSearchClass().isAssignableFrom(obj.getClass())) {
            try {

                fieldValue = criterion.extractValue(obj);

                /*
                * Если значение поля равно null, то оно не может участвовать ни в каких сравнениях,
                * кроме сравнения на null. Т.е. для данного поля применимо только условие CriteriaRule.isNull
                */
                if (fieldValue != null) {
                    matcher = this.getRuleMatcher(criterion);
                    matched = matcher.match(fieldValue, criterion.getCriterionVals());
                } else {
                    matched = criterion.getCriterionRule() == CriteriaRule.isNull;
                }

                /*
                * Используем XOR для получения результата сравнения.
                * При одинаковых значениях match и invert результат должен быть false.
                */
                matched ^= criterion.isInvert();

            } catch (Exception e) {
                logger.log(Level.WARNING, e.getMessage(), e);
            }
        }

        return matched;
    }

    private boolean applyCriterianToElement(Object obj) {

        boolean matched = false;

        for (SearchCriterion criterion : criterionArray) {

            matched = this.applyCriterionToElement(criterion, obj);

            if (!matched) {
                break;
            }
        }

        return matched;
    }

    private RuleMatcher getRuleMatcher(SearchCriterion criterion) {
        return criterion.getCriteriaRuleSet().getRuleMatcher(criterion.getCriterionRule());
    }

    @Override
    public String toString() {
        return Arrays.toString(this.criterionArray);
    }
}
