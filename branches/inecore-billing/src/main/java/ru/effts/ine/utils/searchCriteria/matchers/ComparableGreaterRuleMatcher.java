/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import ru.effts.ine.utils.searchCriteria.RuleMatcher;

/**
 * Простой мачер для сравнения, условие "больше". Применим исключитьельно для реализаций {@link Comparable}
 * Количество патернов: строго один
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ComparableGreaterRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class ComparableGreaterRuleMatcher implements RuleMatcher {

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        Comparable val = (Comparable) value;
        return val.compareTo(pattern[0]) > 0;
    }

}
