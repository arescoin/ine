/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.language;

import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Представляет описание языка в стандарте iso639
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: LangDetails.java 3370 2011-11-30 14:08:46Z dgomon $"
 */
public interface LangDetails extends Identifiable {

    String CODE = "code";
    String NAME = "name";

    /**
     * Возвращает буквенный код языка в соответствие с iso639-1
     *
     * @return буквенный код языка
     */
    String getCode();

    /**
     * Устанавливает буквенный код языка в соответствие с iso639-1
     *
     * @param code буквенный код языка
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setCode(String code) throws IneIllegalArgumentException;

    /**
     * Возвращает название языка в соответствие с iso639-1
     *
     * @return название языка
     */
    String getName();


    /**
     * Устанавливает название языка в соответствие с iso639-1
     *
     * @param name название языка
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setName(String name) throws IneIllegalArgumentException;
}
