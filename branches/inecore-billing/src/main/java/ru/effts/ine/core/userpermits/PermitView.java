/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import java.math.BigDecimal;

/**
 * Обертка для применения в недрах системы
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PermitView.java 3240 2011-11-23 12:15:41Z ikulkov $"
 */
public class PermitView {

    private final BigDecimal userId;
    private final BigDecimal permitId;
    private final BigDecimal roleId;
    private final BigDecimal[] values;
    private final boolean active;
    private final int crudMask;


    public PermitView(BigDecimal userId, BigDecimal permitId,
            BigDecimal roleId, BigDecimal[] values, boolean active, int crudMask) {
        this.userId = userId;
        this.permitId = permitId;
        this.roleId = roleId;
        this.values = values;
        this.active = active;
        this.crudMask = crudMask;
    }

    public BigDecimal getUserId() {
        return userId;
    }

    public BigDecimal getPermitId() {
        return permitId;
    }

    public BigDecimal getRoleId() {
        return roleId;
    }

    public BigDecimal[] getValues() {
        return values;
    }

    public boolean isActive() {
        return active;
    }

    public int getCrudMask() {
        return crudMask;
    }
}
