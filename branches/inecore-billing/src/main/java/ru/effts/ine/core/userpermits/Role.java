/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

/**
 * Интерфейс описывает набор методов для работы с описанием роли
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Role.java 3376 2011-11-30 15:10:34Z dgomon $"
 */
public interface Role extends Versionable {

    String NAME = "name";

    /**
     * Возвращает название роли.
     * <p/>
     * Во избежание недоразумений следует соблюдать уникальность имен, однако идентификатором роли имя не является
     *
     * @return название роли
     */
    String getName();

    /**
     * Устанавливает название роли.
     * <p/>
     * Во избежание недоразумений следует соблюдать уникальность имен, однако идентификатором роли имя не является
     *
     * @param name название роли
     * @throws ru.effts.ine.core.IneIllegalArgumentException если name - NULL или пустая строка
     */
    void setName(String name) throws IneIllegalArgumentException;

}
