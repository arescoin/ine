/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import java.io.Serializable;

/**
 * Простй маркерный интерфейс, предназначен для идентификации объекта не расширяющего Identifiable, но поддерживающих
 * соглашение об именовании для работы с фабрикой.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemMarker.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface SystemMarker extends Serializable {
}
