/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.dic;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Предоставляет методы для описания словаря.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Dictionary.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface Dictionary extends Versionable {

    String NAME = "name";
    String DIC_TYPE = "dicType";
    String CODE_POLICY = "entryCodePolicy";
    String L10N_POLICY = "localizationPolicy";
    String PARENT = "parentId";

    /**
     * Возвращает наименование словаря
     *
     * @return строковое представление (имя)
     */
    String getName();

    /**
     * Устанавливает наименование словаря
     *
     * @param name строковое представление (имя)
     * @throws ru.effts.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setName(String name) throws IneIllegalArgumentException;

    /**
     * Возвращает системный номер типа словаря {@link ru.effts.ine.core.dic.DictionaryEntry}
     *
     * @return тип словаря
     */
    BigDecimal getDicType();

    /**
     * Устанавливает системный номер типа словаря {@link ru.effts.ine.core.dic.DictionaryEntry}
     *
     * @param dicType тип словаря
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setDicType(BigDecimal dicType) throws IneIllegalArgumentException;

    /**
     * Возвращает системный номер типа формирования идентификатора {@link ru.effts.ine.core.dic.DictionaryEntry}
     *
     * @return тип формирования идентификатора
     */
    BigDecimal getEntryCodePolicy();

    /**
     * Устанавливает системный номер типа формирования идентификатора {@link ru.effts.ine.core.dic.DictionaryEntry}
     *
     * @param idPolicy тип формирования идентификатора
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setEntryCodePolicy(BigDecimal idPolicy) throws IneIllegalArgumentException;

    /**
     * Возвращает системный номер политики локализации {@link ru.effts.ine.core.dic.DictionaryEntry}
     *
     * @return идентификатор политики локализации
     */
    BigDecimal getLocalizationPolicy();

    /**
     * Устанавливает системный номер политики локализации {@link ru.effts.ine.core.dic.DictionaryEntry}
     *
     * @param localizationPolicy идентификатор политики локализации
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setLocalizationPolicy(BigDecimal localizationPolicy) throws IneIllegalArgumentException;

    /**
     * Возвращает системный номер верхнего словаря, если он есть, иначе <code>null</code>
     *
     * @return системный номер верхнего словаря
     */
    BigDecimal getParentId();

    /**
     * Устанавливает системный номер верхнего словаря, при его наличии
     *
     * @param parentId системный номер верхнего словаря
     */
    void setParentId(BigDecimal parentId);

}
