/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.GenericSystemException;

/**
 * Выбрасывается если запрашиваемый пользователь неактивный
 *
 * @author Nikolay Ivshin
 * @version 1.0
 * @SVNVersion "$Id: UserInactiveException.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public class UserInactiveException extends GenericSystemException {

    public UserInactiveException(String message) {
        super(message);
    }

    public UserInactiveException(String message, Throwable cause) {
        super(message, cause);
    }
}
