/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import ru.effts.ine.utils.searchCriteria.RuleMatcher;

/**
 * Обобщенный матчер для поиска во множестве по полному совподению
 * Количество патернов: один и более
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CommonInRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class CommonInRuleMatcher implements RuleMatcher {

    @Override
    public boolean match(Object value, Object... pattern) {
        boolean result = false;
        for (Object o : pattern) {
            if (value.equals(o)) {
                result = true;
                break;
            }
        }

        return result;
    }

}
