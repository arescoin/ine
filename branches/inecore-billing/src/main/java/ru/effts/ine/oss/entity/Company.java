/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.oss.entity;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Сущность "Организация". Представляет собой юридическое лицо.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: Company.java 3377 2011-11-30 15:14:26Z dgomon $"
 */
public interface Company extends Versionable {

    String NAME = "name";
    String TYPE = "type";
    String PROPERTY_TYPE = "propertyType";

    /**
     * Возвращает название организации.
     *
     * @return название организации.
     */
    String getName();

    /**
     * Устанавливает название организации.
     *
     * @param name название организации.
     * @throws ru.effts.ine.core.IneIllegalArgumentException если переданное название null или пустое
     */
    void setName(String name) throws IneIllegalArgumentException;

    /**
     * Получает тип организации (словарь 15).
     *
     * @return тип организации (словарь 15).
     */
    BigDecimal getType();

    /**
     * Устанавливает тип организации (словарь 15).
     *
     * @param type тип организации (словарь 15).
     * @throws ru.effts.ine.core.IneIllegalArgumentException если переданное значение null
     */
    void setType(BigDecimal type) throws IneIllegalArgumentException;

    /**
     * Получает тип собственности (словарь 16).
     *
     * @return тип собственности (словарь 16).
     */
    BigDecimal getPropertyType();

    /**
     * Устанавливает тип собственности (словарь 16).
     *
     * @param propertyType тип собственности (словарь 16).
     * @throws ru.effts.ine.core.IneIllegalArgumentException если переданное значение null
     */
    void setPropertyType(BigDecimal propertyType) throws IneIllegalArgumentException;
}
