/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import ru.effts.ine.core.history.SearchId;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * Синтетический идентификатор.
 * <p/>
 * Используется для идентификации ссылочных (составных) объектов, не имеющих собственных идентификаторов, но однозначно
 * идентифицируемых с помощью идентификаторов объектов, принадлижащих данному агрегату.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SyntheticId.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public class SyntheticId implements Serializable {

    private BigDecimal[] idOrder = new BigDecimal[0];
    private Map<BigDecimal, BigDecimal> idMap = new HashMap<BigDecimal, BigDecimal>(3, 1.1f);
    private int hashCode = 0;

    /**
     * Добавляет нового "участника" идентификатора.
     * <p/>
     * <strong> При работе с идентификатором в нескольких потоках необходимо учитывать что этот метод не
     * синхронизирован! </strong><br> Ключи, в синтетическом идентификаторе, должны быть уникальны и отличны от
     * <b>null</b>. Значения должны быть отличны от <b>null</b>.
     *
     * @param key   ключ для участника идентификатора
     * @param value значение участника идентификатора
     * @throws IneIllegalArgumentException при попытке передать пустую ссылку в ключе или значение. При попытке
     * продублировать значение ключа.
     */
    public void addIdEntry(BigDecimal key, BigDecimal value) throws IneIllegalArgumentException {
        if (key == null) {
            throw new IneIllegalArgumentException("The key must not be null.");
        }

        if (Identifiable.MIN_ALLOWABLE_VAL.compareTo(key) > 0) {
            throw new IneIllegalArgumentException("The key can't be less than " + Identifiable.MIN_ALLOWABLE_VAL);
        }

        if (value == null) {
            throw new IneIllegalArgumentException("The value to be add must not be null.");
        }
        if (Identifiable.MIN_ALLOWABLE_VAL.compareTo(value) > 0) {
            throw new IneIllegalArgumentException("The value can't be less than " + Identifiable.MIN_ALLOWABLE_VAL);
        }

        if (idMap.containsKey(key)) {
            throw new IneIllegalArgumentException("The key must be unique.");
        }

        idMap.put(key, value);

        idOrder = Arrays.copyOf(idOrder, idOrder.length + 1);
        idOrder[idOrder.length - 1] = key;

        calculateHashCode();
    }

    protected void calculateHashCode() {
        this.hashCode = this.idMap.hashCode();
    }

    /**
     * Возвращает признак использования для поиска
     * <p/>
     * При обнаружении в значениях {@link java.math.BigDecimal#ZERO} данный контейнер будет расценен как поисковый.
     * Ключ с этим значением будет рассматриваться как варируемый.
     *
     * @return признак использования для поиска: true - поисковой, иначе - false
     */
    public boolean isSearchId() {
        return idMap.containsValue(BigDecimal.ZERO);
    }

    /**
     * Возвращает немодифицируемую копию ключей для данного синтетического идентификатора
     *
     * @return копия ключей
     */
    public List<BigDecimal> getIdKeys() {
        return Collections.unmodifiableList(Arrays.asList(idOrder));
    }

    /**
     * Возвращает все значения идентификаторов в порядке их добавления.
     *
     * @return значения идентификаторов.
     */
    public BigDecimal[] getIdValues() {
        BigDecimal[] result = new BigDecimal[idOrder.length];
        int i = 0;
        for (BigDecimal key : idOrder) {
            result[i++] = idMap.get(key);
        }

        return result;
    }

    protected void setValue(BigDecimal key, BigDecimal value) {
        if (!idMap.containsKey(key)) {
            throw new IneIllegalArgumentException("Key not found [" + key + "]");
        }

        idMap.put(key, value);
        calculateHashCode();
    }

    /**
     * Возвращает значение идентификатора (конкретной части) по переданному ключу
     *
     * @param key ключ (идентификатор системного объекта)
     * @return значение идентификатора
     */
    public BigDecimal getIdValue(BigDecimal key) {
        return idMap.get(key);
    }

    @Override
    public int hashCode() {
        return this.hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof SyntheticId
                && this.hashCode() == obj.hashCode()
                && idMap.equals(((SyntheticId) obj).idMap);
    }

    @Override
    public String toString() {

        StringBuffer stringBuffer = new StringBuffer();
        for (BigDecimal key : idOrder) {
            stringBuffer.append("[").append(key).append(":").append(idMap.get(key)).append("]");
        }

        return stringBuffer.toString();
    }

    public static SyntheticId parseString(String value) {
        if (value == null) {
            throw new IneIllegalArgumentException("String value to parse must not be null.");
        }
        SyntheticId result = new SyntheticId();
        String[] ids;
        StringTokenizer tokenizer = new StringTokenizer(value, "[]");
        while (tokenizer.hasMoreTokens()) {
            ids = tokenizer.nextToken().split(":");
            result.addIdEntry(new BigDecimal(ids[0]), new BigDecimal(ids[1]));
        }

        return result;
    }

    /**
     * Конструирует расширение основного объекта, предоставляющее возможность устанавливать идентификаторы поиска
     *
     * @return поисковой SyntheticId
     */
    public SearchId getSearchId() {
        SearchId searchId = new SearchId();
        for (Map.Entry<BigDecimal, BigDecimal> entry : idMap.entrySet()) {
            searchId.addIdEntry(entry.getKey(), entry.getValue());
        }

        return searchId;
    }

}
