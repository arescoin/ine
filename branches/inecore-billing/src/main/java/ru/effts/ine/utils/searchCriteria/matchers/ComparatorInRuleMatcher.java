/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import java.util.Comparator;

/**
 * Матчер для поиска во множестве по полному совподению.
 * Количество паттернов: один и более
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ComparatorInRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class ComparatorInRuleMatcher extends AbstractComparatorRuleMatcher {

    public ComparatorInRuleMatcher(Comparator comparator) {
        super(comparator);
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) {
        try {
            for (Object o : pattern) {
                if (comparator.compare(value, o) == 0) {
                    return true;
                }
            }
        } catch (ClassCastException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        return false;
    }
}
