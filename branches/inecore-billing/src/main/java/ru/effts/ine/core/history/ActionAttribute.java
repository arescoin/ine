/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.SyntheticId;

import java.util.Date;

/**
 * Пользовательский атрибут действия над объектом системы
 * 
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: ActionAttribute.java 1602 2011-03-11 15:30:21Z ikulkov $"
 */
public interface ActionAttribute extends Identifiable {

    String ENTITY_ID = "entityId";
    String ACTION_DATE = "actionDate";
    String ATTRIBUTE_NAME = "attributeName";
    String ATTRIBUTE_VALUE = "attributeValue";

    /**
     * Получает идентификатор сущности, над которой было совершено действие
     *
     * @return идентификатор сущности, над которой было совершено действие
     */
    SyntheticId getEntityId();

    /**
     * Устанавливает идентификатор сущности, над которой было совершено действие
     *
     * @param entityId идентификатор сущности, над которой было совершено действие
     * @throws ru.effts.ine.core.IneIllegalArgumentException в случае, если переданное значение null
     */
    void setEntityId(SyntheticId entityId) throws IneIllegalArgumentException;

    /**
     * Получает дату совершённого действия
     *
     * @return дата совершённого действия
     */
    Date getActionDate();

    /**
     * Устанавливает дату совершённого действия
     *
     * @param actionDate дата совершённого действия
     * @throws ru.effts.ine.core.IneIllegalArgumentException в случае, если переданное значение null
     */
    void setActionDate(Date actionDate) throws IneIllegalArgumentException;

    /**
     * Получает название пользовательского атрибута действия
     *
     * @return название пользовательского атрибута действия
     */
    String getAttributeName();

    /**
     * Устанавливает название пользовательского атрибута действия
     *
     * @param attributeName название пользовательского атрибута действия
     * @throws ru.effts.ine.core.IneIllegalArgumentException в случае, если переданное значение null
     */
    void setAttributeName(String attributeName) throws IneIllegalArgumentException;

    /**
     * Получает значение пользовательского атрибута действия
     *
     * @return значение пользовательского атрибута действия
     */
    String getAttributeValue();

    /**
     * Устанавливает значение пользовательского атрибута действия
     *
     * @param attributeValue значение пользовательского атрибута действия
     * @throws ru.effts.ine.core.IneIllegalArgumentException в случае, если переданное значение null
     */
    void setAttributeValue(String attributeValue);

}
