/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria;

/**
 * Перечисление всех доступных правил
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CriteriaRule.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public enum CriteriaRule {
    /** Меньше */
    less,
    /** Больше */
    greater,
    /** Равно */
    equals,
    /** Диапазон */
    between,
    /** В числе */
    in,
    /** Начинасется с */
    startWith,
    /** Содержит */
    contains,
    /** знчение null */
    isNull
}
