/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.oss.entity;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Сущность "Персона". Представляет собой физическое лицо.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: Person.java 3377 2011-11-30 15:14:26Z dgomon $"
 */
public interface Person extends Versionable {

    String FAMILY_NAME = "familyName";
    String NAME = "name";
    String MIDDLE_NAME = "middleName";
    String ALIAS = "alias";
    String FAMILY_NAME_PREFIX = "familyNamePrefixCode";
    String FAMILY_GENERATION = "familyGeneration";
    String FORM_OF_ADDRESS = "formOfAddress";

    /**
     * Получает фамилию.
     *
     * @return фамилия
     */
    String getFamilyName();

    /**
     * Устанавливает фамилию
     *
     * @param familyName фамилия
     * @throws ru.effts.ine.core.IneIllegalArgumentException если переданное значение null или пустая строка
     */
    void setFamilyName(String familyName) throws IneIllegalArgumentException;

    /**
     * Получает имя
     *
     * @return имя
     */
    String getName();

    /**
     * Устанавливает имя
     *
     * @param givenName имя
     * @throws ru.effts.ine.core.IneIllegalArgumentException если переданное значение null или пустая строка
     */
    void setName(String givenName) throws IneIllegalArgumentException;

    /**
     * Получает отчество
     *
     * @return отчество
     */
    String getMiddleName();

    /**
     * Устанавливает отчество
     *
     * @param middleName отчество
     * @throws ru.effts.ine.core.IneIllegalArgumentException если переданное значение null или пустая строка
     */
    void setMiddleName(String middleName) throws IneIllegalArgumentException;

    /**
     * Получает псевдоним
     *
     * @return псевдоним
     */
    String getAlias();

    /**
     * Устанавливает псевдоним
     *
     * @param alias псевдоним
     */
    void setAlias(String alias);

    /**
     * Получает код словарной статьи предпочтительного обращения (Словарь 19)
     *
     * @return предпочтительное обращение (Словарь 19)
     */
    BigDecimal getFamilyNamePrefixCode();

    /**
     * Устанавливает код словарной статьи предпочтительного обращения (Словарь 19)
     *
     * @param familyNamePrefixCode код словарной стать предпочтительного обращения (Словарь 19)
     * @throws ru.effts.ine.core.IneIllegalArgumentException если переданное значение null
     */
    void setFamilyNamePrefixCode(BigDecimal familyNamePrefixCode) throws IneIllegalArgumentException;

    /**
     * Получает семейное поколение
     *
     * @return семейное поколение
     */
    String getFamilyGeneration();

    /**
     * Устанавливает семейное поколение
     *
     * @param familyGeneration семейное поколение
     */
    void setFamilyGeneration(String familyGeneration);

    /**
     * Получает тип прописки
     *
     * @return тип прописки
     */
    String getFormOfAddress();

    /**
     * Устанавливает тип прописки
     *
     * @param formOfAddress тип прописки
     */
    void setFormOfAddress(String formOfAddress);
}
