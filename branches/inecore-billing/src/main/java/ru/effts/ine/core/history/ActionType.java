/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

import ru.effts.ine.core.IneIllegalArgumentException;

import java.io.Serializable;

/**
 * Перечисления типов действий
 *
 * @SVNVersion "$Id: ActionType.java 3369 2011-11-30 13:59:59Z dgomon $"
 */
public enum ActionType implements Serializable {

    /* Создание*/
    create,
    /** Модификация */
    update,
    /** Удаление */
    delete;

    /**
     * Утилитный метод, по переданному коду возвращает значение enum
     *
     * @param code числовой код типа
     * @return enum-значение
     * @throws IneIllegalArgumentException при передаче в метод числового значения несуществующего кода
     */
    public static ActionType getType(int code) {

        switch (code) {
        case 0:
            return create;
        case 1:
            return update;
        case 2:
            return delete;
        default:
            throw new IneIllegalArgumentException("Incorrect type code [" + code + "]");
        }
    }

}
