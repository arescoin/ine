/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

import ru.effts.ine.core.userpermits.SystemUser;

/**
 * Утилитный класс, предоставляющий возможность работы с профайлом пользователя
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: UserHolder.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class UserHolder {

    /** Хранилище профайла */
    private static ThreadLocal<UserProfile> threadLocal = new ThreadLocal<>();

    private UserHolder() {
    }

    /**
     * Устанавливает профайл пользователя
     *
     * @param user текущий пользователь
     */
    public static void setUser(SystemUser user) {
        setUserProfile(new UserProfile(user));
    }

    /**
     * Устанавливает профайл пользователя
     *
     * @param userProfile профайл текущего пользователя
     */
    public static void setUserProfile(UserProfile userProfile) {
        threadLocal.set(userProfile);
    }

    /**
     * Возвращает профайл текущего пользователя
     *
     * @return профайл текущего пользователя
     */
    public static UserProfile getUser() {
        return threadLocal.get();
    }

}
