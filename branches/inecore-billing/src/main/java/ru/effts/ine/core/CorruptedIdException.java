/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Исключение выбрасывается при обнаружении некорректного идентификатора в процессе конструирования инстанса {@link
 * Identifiable}
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CorruptedIdException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public class CorruptedIdException extends IneCorruptedStateException {

    /**
     * Конструирует инстанс исключения с указанием детального сообщения о причине
     *
     * @param message детализированное сообщение об ошибке
     */
    public CorruptedIdException(String message) {
        super(message);
    }
}
