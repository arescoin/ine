/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Исключение выбрасывается при попытке установить невалидное значение имени пользователя системы.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: InvalidUserNameException.java 3376 2011-11-30 15:10:34Z dgomon $"
 */
public class InvalidUserNameException extends IneIllegalArgumentException {


    /**
     * Конструирует новое исключение с указанием детального сообщения о причине
     *
     * @param message детализация ошибки
     */
    public InvalidUserNameException(String message) {
        super(message);
    }

    /**
     * Конструирует новое исключение с указанием детального сообщения о причине и с указанием инициирующего исключения
     *
     * @param message детализация ошибки
     * @param cause   инициирующее исключение
     */
    public InvalidUserNameException(String message, Throwable cause) {
        super(message, cause);
    }
}
