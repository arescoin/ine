/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.ipaddress;

import ru.effts.ine.core.Versionable;

/**
 * Описывает IP-адрес для работы в системе
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: IPAddressValue.java 3219 2011-11-21 13:48:26Z ikulkov $"
 */
public interface IPAddressValue extends Versionable {

    /** Имя поля хранящего адрес */
    String IP_ADDRESS = "ipAddress";

    /**
     * Получает объект с адресом
     *
     * @return адрес
     */
    IPAddress getIpAddress();

    /**
     * Устанавливает адрес
     *
     * @param ipAddress адрес
     */
    void setIpAddress(IPAddress ipAddress);
}
