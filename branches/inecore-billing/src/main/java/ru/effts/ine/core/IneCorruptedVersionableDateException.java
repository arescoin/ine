/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Исключение выбрасывается при обнаружении некорректных значений в датах версионного объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneCorruptedVersionableDateException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public class IneCorruptedVersionableDateException extends IneCorruptedStateException {

    public IneCorruptedVersionableDateException(String message) {
        super(message);
    }

}
