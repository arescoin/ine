/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Значение по-умолчанию для произвольного атрибута
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CustomAttributeDefVal.java 3375 2011-11-30 14:21:39Z dgomon $"
 */
public interface CustomAttributeDefVal extends Versionable {

    String STRING = "string";
    String LONG = "long";
    String BIGDECIMAL = "bigDecimal";
    String BOOLEAN = "boolean";
    String DATE = "date";

    /**
     * Получает строковое значение
     *
     * @return строковое значение
     */
    String getString();

    /**
     * Устанавливает строковое значение.
     * <br>
     * Стирает остальные значения.
     *
     * @param aString строковое значение
     */
    void setString(String aString);

    /**
     * Получает целое числовое значение
     *
     * @return целое числовое значение
     */
    Long getLong();

    /**
     * Устанавливает целое числовое значение.
     * <br>
     * Стирает остальные значения.
     *
     * @param aLong целое числовое значение
     * @throws ru.effts.ine.core.IneIllegalArgumentException в случае дробного числа на входе
     */
    void setLong(Long aLong) throws IneIllegalArgumentException;

    /**
     * Получает дробное числовое значение
     *
     * @return дробное числовое значение
     */
    BigDecimal getBigDecimal();

    /**
     * Устанавливает дробное числовое значение.
     * <br>
     * Стирает остальные значения.
     *
     * @param aBigDecimal дробное числовое значение
     */
    void setBigDecimal(BigDecimal aBigDecimal);

    /**
     * Получает булевое значение
     *
     * @return булевое значение
     */
    Boolean getBoolean();

    /**
     * Устанавливает булевое значение.
     * <br>
     * Стирает остальные значения.
     *
     * @param aBoolean булевое значение
     */
    void setBoolean(Boolean aBoolean);

    /**
     * Получает значение дату
     *
     * @return значение дата
     */
    Date getDate();

    /**
     * Устанавливает значение дату.
     * <br>
     * Стирает остальные значения.
     *
     * @param aDate значение дата
     */
    void setDate(Date aDate);

    /**
     * Получает значение атрибута в соответствии с его типом
     *
     * @param type тип значения произвольного атрибута
     * @return значение по-умолчанию для произвольного атрибута
     * @throws ru.effts.ine.core.IneIllegalArgumentException в случае некорректного значения типа атрибута
     */
    Object getValueByType(DataType type) throws IneIllegalArgumentException;

    /**
     * Устанавливает значение атрибута по-умолчанию в соответствии с его типом.
     * <br>
     * Стирает остальные значения.

     * @param value новое значение по-умолчанию
     * @param type тип значения произвольного атрибута
     * @throws IneIllegalArgumentException в случае некорректного значения типа атрибута
     */
    void setValueByType(Object value, DataType type) throws IneIllegalArgumentException;
}
