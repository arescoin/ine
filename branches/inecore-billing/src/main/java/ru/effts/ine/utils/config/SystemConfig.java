/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.config;

/**
 * Описывает стандартный набор методов для работы с конфигурациями подсистем
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemConfig.java 3380 2011-11-30 15:32:42Z dgomon $"
 */
public interface SystemConfig {

    /**
     * Метод подготавливает конфигурацию к работе
     *
     * @param initialisationParam параметр интерпретируемый системой как ключевой атрибут конфигурации
     * @param additionalParams    дополнительные, необязательные параметры
     * @throws ru.effts.ine.utils.config.ConfigurationException при ошибках в процессе подготовки конфигурации
     */
    void init(String initialisationParam, String... additionalParams) throws ConfigurationException;

    /**
     * Метод зачитывает конфигурацию из хранилища
     *
     * @throws ru.effts.ine.utils.config.ConfigurationException при ошибках в процессе чтения конфигурации
     */
    void read() throws ConfigurationException;

    /**
     * Возвращает признак возможности сохранения конфигурации
     *
     * @return true - запись конфигурации поддерживается, иначе - false
     */
    boolean isWritable();

    /**
     * Сохраняет конфигурацию в хранилище
     *
     * @throws UnsupportedOperationException при попытке записи на объекте, не поддерживающем данную операцию
     * @throws ru.effts.ine.utils.config.ConfigurationException при ошибках в процессе записи конфигурации
     */
    void write() throws UnsupportedOperationException, ConfigurationException;
}
