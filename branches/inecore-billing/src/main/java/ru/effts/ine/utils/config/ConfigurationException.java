/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.config;

import ru.effts.ine.core.GenericSystemException;

/**
 * Данное исключение выбрасывается подсистемой конфигурации
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConfigurationException.java 3380 2011-11-30 15:32:42Z dgomon $"
 */
public class ConfigurationException extends GenericSystemException{

    public ConfigurationException(String message) {
        super(message);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
