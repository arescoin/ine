/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.links;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Значение конкретного линка. Для идентификации, помимо стандартных правил, необходимо учитывать идентификаторы
 * участников ассоциации
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: LinkData.java 3373 2011-11-30 14:13:53Z dgomon $"
 */
public interface LinkData extends Versionable {

    String LINK_ID = "linkId";
    String LEFT_ID = "leftId";
    String RIGHT_ID = "rightId";

    /**
     * Возвращает идентификатор описателя линка {@link ru.effts.ine.core.links.Link}
     *
     * @return идентификатор описателя линка
     */
    BigDecimal getLinkId();

    /**
     * Устанавливает идентификатор описателя линка {@link ru.effts.ine.core.links.Link}
     *
     * @param linkId идентификатор описателя линка
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setLinkId(BigDecimal linkId) throws IllegalIdException;

    /**
     * Возвращает идентификатор левого участника ассоциации
     *
     * @return идентификатор участника
     */
    BigDecimal getLeftId();

    /**
     * Устанавливает идентификатор левого участника ассоциации
     *
     * @param leftId идентификатор участника
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setLeftId(BigDecimal leftId) throws IllegalIdException;

    /**
     * Возвращает идентификатор правого участника ассоциации
     *
     * @return идентификатор участника
     */
    BigDecimal getRightId();

    /**
     * Устанавливает идентификатор правого участника ассоциации
     *
     * @param rightId идентификатор участника
     * @throws IllegalIdException при попытке установить null или некорректное значение.
     */
    void setRightId(BigDecimal rightId) throws IllegalIdException;
}
