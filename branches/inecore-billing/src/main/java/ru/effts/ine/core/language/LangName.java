/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.language;

import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Внутрисистемные названия языков.
 * <p>
 * Предназначен для визуализации объектов {@link ru.effts.ine.core.language.SysLanguage}
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: LangName.java 3371 2011-11-30 14:09:43Z dgomon $"
 */
public interface LangName extends Identifiable {

    String NAME = "name";

    /**
     * Возвращает системное название языка
     *
     * @return название языка
     */
    String getName();

    /**
     * Задает системное название языка
     *
     * @param name название языка
     * @throws ru.effts.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setName(String name) throws IneIllegalArgumentException;
}
