/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.structure;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Идентификатор атрибута однозначнго определяет его среди остальных атрибутов
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: CustomAttribute.java 3375 2011-11-30 14:21:39Z dgomon $"
 */
public interface CustomAttribute extends Versionable {

    String SYSTEM_OBJECT_ID = "systemObjectId";
    String CUSTOM_TYPE_ID = "customTypeId";
    String DATA_TYPE = "dataType";
    String ATTRIBITE_NAME = "attributeName";
    String REQUIRED = "required";
    String PATTERN = "pattern";

    String LIMITATIONS = "limitations";

    /**
     * Возвращает идентификатор системного объекта владеющего данным атрибутом
     *
     * @return идентификатор
     */
    BigDecimal getSystemObjectId();

    /**
     * Устанавливает идентификатор системного объекта владеющего данным атрибутом
     *
     * @param systemObjectId идентификатор
     * @throws ru.effts.ine.core.IllegalIdException если значение null или меньше
     * {@link #MIN_ALLOWABLE_VAL минимально допустимого значения}
     */
    void setSystemObjectId(BigDecimal systemObjectId) throws IllegalIdException;

    /**
     * Возвращает идентификатор custom-типа, в который входит данный атрибут.<br>
     * Может быть <code>null</code> -  в таком случае атрибут привязан непосредственно к системному объекту,
     * указанному в {@link #getSystemObjectId()}.
     *
     * @return идентификатор custom-типа
     */
    BigDecimal getCustomTypeId();

    /**
     * Устанавливает идентификатор custom-типа, в который входит данный атрибут.<br>
     * Может быть <code>null</code> -  в таком случае атрибут привязан непосредственно к системному объекту,
     * указанному в {@link #getSystemObjectId()}.
     *
     * @param customTypeId идентификатор custom-типа
     * @throws ru.effts.ine.core.IllegalIdException если значение меньше
     * {@link #MIN_ALLOWABLE_VAL минимально допустимого значения}
     */
    void setCustomTypeId(BigDecimal customTypeId) throws IllegalIdException;

    /**
     * Возвращает тип данных значения атрибута
     *
     * @return тип данных
     */
    DataType getDataType();

    /**
     * Устанавливает тип данных для атрибута
     *
     * @param dataType тип данных
     * @throws ru.effts.ine.core.IneIllegalArgumentException если передаваемое значение null
     */
    void setDataType(DataType dataType) throws IneIllegalArgumentException;

    /**
     * Возвращает имя атрибута, должно быть уникально в пределах типа объекта
     *
     * @return имя атрибута
     */
    String getAttributeName();

    /**
     * Устанавливает имя атрибута, должно быть уникально в пределах типа объекта
     *
     * @param attribiteName имя атрибута
     * @throws ru.effts.ine.core.IneIllegalArgumentException если передаваемое значение null
     */
    void setAttributeName(String attribiteName) throws IneIllegalArgumentException;

    /**
     * Возвращает признак обязательности заполнения атрибута значением отличным от <b>null</b>
     *
     * @return <b>true</b> - значение обязательно, иначе - <b>false</b>
     */
    boolean isRequired();

    /**
     * Устанавливает признак обязательности заполнения атрибута значением отличным от <b>null</b>
     *
     * @param required <b>true</b> - значение обязательно, иначе - <b>false</b>
     */
    void setRequired(boolean required);

    /**
     * Получает паттерн системного объекта для возможности работы произвольных атрибутов с объектами InE.
     *
     * @return паттерн системного объекта
     */
    SystemObjectPattern getPattern();

    /**
     * Устанавливает паттерн системного объекта.
     * <br>
     * Для корректного сохранения паттерна необходимо правильно выставить тип данных атрибута - требуется
     * {@link ru.effts.ine.core.structure.DataType#bigDecimalType}.
     *
     * @param pattern паттерн системного объекта
     * @throws ru.effts.ine.core.IneIllegalArgumentException если передаваемое значение null
     */
    void setPattern(SystemObjectPattern pattern) throws IneIllegalArgumentException;

    /**
     * Получает ограничения на значение атрибута
     *
     * @return ограничения на значение атрибута
     */
    String getLimitations();

    /**
     * Устанавливает ограничения на значение атрибута
     *
     * @param limitations ограничения на значение атрибута
     */
    void setLimitations(String limitations);
}
