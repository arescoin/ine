/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.ipaddress;

import ru.effts.ine.core.IneIllegalArgumentException;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Хранилище для битового представления IP-адресов (v4, v6)
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IPAddress.java 3370 2011-11-30 14:08:46Z dgomon $"
 */
public class IPAddress implements Serializable {

    public static final Comparator<IPAddress> IP_ADDRESS_COMPARATOR = new IPAddressComparator<IPAddress>();

    private byte[] bytes;

    /**
     * Конструирует новый объект, используя полное бинарное представление
     *
     * @param addressBytes байты со значением октетов
     */
    public IPAddress(byte[] addressBytes) {
        if (addressBytes == null) {
            throw new IneIllegalArgumentException("Argument addressBytes must not be null");
        }

        this.bytes = addressBytes;
    }

    /**
     * Формирует адрес на основание переданной строки
     *
     * @param addressString строковое представление адреса
     * @return адрес
     * @throws java.net.UnknownHostException при некорректной строке с адресом
     */
    public static IPAddress parse(String addressString) throws UnknownHostException {
        return new IPAddress(InetAddress.getByName(addressString).getAddress());
    }

    /**
     * Формирует массив адресо на основание переданной строки формата "192.168.1.0/24"
     *
     * @param cidr маска адресов в формате CIDR
     * @return адреса диапазона
     * @throws java.net.UnknownHostException при некорректной строке с адресом
     */
    public static IPAddress[] parseCIDR(String cidr) throws UnknownHostException {

        IPAddress[] ipAddresses;

        String[] strings = cidr.split("/");
        if (strings.length != 2) {
            throw new IneIllegalArgumentException("Wrong address mask [" + cidr + "]");
        }

        int mask = Integer.parseInt(strings[1]);
        InetAddress inetAddress = InetAddress.getByName(strings[0]);
        byte[] bytes = inetAddress.getAddress();

        // последний целый октет (по порядку)
        int lastOctet = mask / 8;
        // последний целый бит в октете (по порядку)
        int lastBit = mask % 8;

        ArrayList<byte[]> arrayList = new ArrayList<byte[]>();

        if (lastOctet < bytes.length) {

            int limitOperand = 255 >> (lastBit);
            int aByte = bytes[lastOctet] & 0xFF;
            int upLimit = aByte | limitOperand;
            int downLimit = upLimit ^ limitOperand;

            getByteSet(arrayList, bytes, lastOctet, downLimit, upLimit);

//            if (arrayList.size() > 2) {
//                arrayList.remove(0);
//                arrayList.remove(arrayList.size() - 1);
//            }

        } else {
            arrayList.add(bytes);
        }

        ipAddresses = new IPAddress[arrayList.size()];
        int i = 0;
        for (byte[] bytes1 : arrayList) {
            ipAddresses[i++] = new IPAddress(bytes1);
        }

        return ipAddresses;
    }

    /**
     * Заполняет коллекцию байтовым представлением адресов
     *
     * @param arrayList заполняемая коллекция
     * @param bytes     шаблон для заполнения
     * @param octet     заполняемый октет
     * @param from      начальное значение для октета
     * @param to        конечное значение для октета
     */
    private static void getByteSet(ArrayList<byte[]> arrayList, byte[] bytes, int octet, int from, int to) {

        byte[] workCopy = Arrays.copyOf(bytes, bytes.length);

        for (int byteValue = from; byteValue <= to; byteValue++) {

            int length = workCopy.length;

            workCopy[octet] = (byte) byteValue;
            if (length - octet > 1) {
                int nextOctet = octet + 1;

                int f = 0;
                int t = 255;

                getByteSet(arrayList, workCopy, nextOctet, f, t);
            } else {
                arrayList.add(Arrays.copyOf(workCopy, workCopy.length));
            }
        }

    }

    /**
     * Возвращает байтовое (битовое) представление адреса
     *
     * @return октеты в бинарном виде
     */
    public byte[] asBytes() {
        return Arrays.copyOf(this.bytes, bytes.length);
    }

    @Override
    public String toString() {

        String result = "";

        try {
            result = InetAddress.getByAddress(bytes).getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public boolean equals(Object obj) {

        boolean result = false;

        if (obj != null && obj instanceof IPAddress) {
            IPAddress otherIP = (IPAddress) obj;
            result = Arrays.equals(this.asBytes(), otherIP.asBytes());
        }

        return result;
    }

    @Override
    public int hashCode() {
        return bytes != null ? Arrays.hashCode(bytes) : 0;
    }

    /**
     * Производит сравнение двух байтовых представлений адреса,
     * результат формируется аналогично стандартному сравнению
     *
     * @param address1 адрес
     * @param address2 адрес
     * @return результат сравнения
     */
    public static int compareBytes(byte[] address1, byte[] address2) {

        int result = 0;
        int el1;
        int el2;
        if (address1.length == address2.length) {
            for (int i = 0; i < address1.length; i++) {
                el1 = address1[i] < 0 ? address1[i] + 256 : address1[i];
                el2 = address2[i] < 0 ? address2[i] + 256 : address2[i];
                if (el1 != el2) {
                    result = el1 < el2 ? -1 : 1;
                    break;
                }
            }
        } else {
            result = address1.length < address2.length ? -1 : 1;
        }

        return result;
    }

    /** Класс-компаратор для сравнения ипов по непосредственному содержимому (в байтах) */
    public static class IPAddressComparator<T extends IPAddress> implements Comparator<T>, Serializable {

        @Override
        public int compare(T o1, T o2) {
            byte[] address1 = o1.asBytes();
            byte[] address2 = o2.asBytes();

            return compareBytes(address1, address2);
        }
    }

}
