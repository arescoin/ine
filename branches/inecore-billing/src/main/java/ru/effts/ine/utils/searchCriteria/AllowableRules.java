/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria;

import ru.effts.ine.core.structure.DataType;
import ru.effts.ine.utils.searchCriteria.matchers.*;

import java.io.Serializable;
import java.util.*;

/**
 * Перечисление доступных правил в зависимости от типа данных
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: AllowableRules.java 2943 2011-10-05 08:28:01Z ikulkov $"
 */
public class AllowableRules implements Serializable {

    /** Контейнер для хранения сформированных правил */
    //TODO увеличивать размер при добавлении нового типа данных!
    private static final AllowableRules[] enumArray = new AllowableRules[5];

    /** Список контейнеров для различных компараторов */
    private static final Map<Class, AllowableRules> compMap = new HashMap<Class, AllowableRules>();

    /** Тип данных правила */
    private final DataType dataType;

    /** Набор правил */
    //TODO увеличивать размер при добавлении нового поискового критерия!
    private final CriteriaRule[] criteriaRules = new CriteriaRule[8];
    private final RuleMatcher[] ruleMatchers = new RuleMatcher[8];

    static {
        // Статическая инициялизация допустимых комбинаций java-тип - набор правил

        RuleMatcher commonEqualsMatcher = new CommonEqualsRuleMatcher();
        RuleMatcher commonIsNullMatcher = new CommonIsNullMatcher();
        RuleMatcher commonInRuleMatcher = new CommonInRuleMatcher();
        RuleMatcher commonLessRuleMatcher = new ComparableLessRuleMatcher();
        RuleMatcher commonGreaterRuleMatcher = new ComparableGreaterRuleMatcher();
        RuleMatcher commonBetweenRuleMatcher = new ComparableBetweenRuleMatcher();

        Map<CriteriaRule, RuleMatcher> booleanRules = new HashMap<CriteriaRule, RuleMatcher>(4);
        booleanRules.put(CriteriaRule.equals, commonEqualsMatcher);
        booleanRules.put(CriteriaRule.isNull, commonIsNullMatcher);
        enumArray[DataType.booleanType.ordinal()] = new AllowableRules(DataType.booleanType, booleanRules);

        Map<CriteriaRule, RuleMatcher> stringRules = new HashMap<CriteriaRule, RuleMatcher>(6);
        stringRules.put(CriteriaRule.equals, commonEqualsMatcher);
        stringRules.put(CriteriaRule.isNull, commonIsNullMatcher);
        stringRules.put(CriteriaRule.in, commonInRuleMatcher);
        stringRules.put(CriteriaRule.contains, new StringContainsRuleMatcher());
        stringRules.put(CriteriaRule.startWith, new StringStartWithRuleMatcher());
        enumArray[DataType.stringType.ordinal()] = new AllowableRules(DataType.stringType, stringRules);

        Map<CriteriaRule, RuleMatcher> numberRule = new HashMap<CriteriaRule, RuleMatcher>(8);
        numberRule.put(CriteriaRule.equals, commonEqualsMatcher);
        numberRule.put(CriteriaRule.isNull, commonIsNullMatcher);
        numberRule.put(CriteriaRule.greater, commonGreaterRuleMatcher);
        numberRule.put(CriteriaRule.less, commonLessRuleMatcher);
        numberRule.put(CriteriaRule.between, commonBetweenRuleMatcher);
        numberRule.put(CriteriaRule.in, commonInRuleMatcher);
        enumArray[DataType.longType.ordinal()] = new AllowableRules(DataType.longType, numberRule);
        enumArray[DataType.bigDecimalType.ordinal()] = new AllowableRules(DataType.bigDecimalType, numberRule);

        Map<CriteriaRule, RuleMatcher> dateRule = new HashMap<CriteriaRule, RuleMatcher>(6);
        dateRule.put(CriteriaRule.equals, commonEqualsMatcher);
        dateRule.put(CriteriaRule.isNull, commonIsNullMatcher);
        dateRule.put(CriteriaRule.greater, commonGreaterRuleMatcher);
        dateRule.put(CriteriaRule.less, commonLessRuleMatcher);
        dateRule.put(CriteriaRule.between, commonBetweenRuleMatcher);
        enumArray[DataType.dateType.ordinal()] = new AllowableRules(DataType.dateType, dateRule);
    }

    /**
     * Создает объект с укзанными правила для конкретного типа данных
     *
     * @param dataType        тип данных
     * @param criteriaRules правила
     */
    private AllowableRules(DataType dataType, Map<CriteriaRule, RuleMatcher> criteriaRules) {
        this.dataType = dataType;
        for (CriteriaRule criteriaRule : criteriaRules.keySet()) {
            this.criteriaRules[criteriaRule.ordinal()] = criteriaRule;
            this.ruleMatchers[criteriaRule.ordinal()] = criteriaRules.get(criteriaRule);
        }
    }

    /**
     * Предоставляет сформированный набор правил по переданному типу данных
     *
     * @param dataType тип данных
     * @return правила
     */
    public static AllowableRules getCriteriaRules(DataType dataType) {
        return enumArray[dataType.ordinal()];
    }

    /**
     * Возвращает сформированный набор правил для конкретного компаратора.
     *
     * @param comparator компаратор для гибкого сравнения объектов
     * @return набор правил
     */
    public static AllowableRules getRulesForComparator(Comparator comparator) {
        // TODO подумать, как здесь избавится от мапы и надо ли...
        Class compClass = comparator.getClass();
        if (!compMap.containsKey(compClass)) {
            Map<CriteriaRule, RuleMatcher> comparatorRules = new HashMap<CriteriaRule, RuleMatcher>(8);
            comparatorRules.put(CriteriaRule.between, new ComparatorBetweenRuleMatcher(comparator));
            comparatorRules.put(CriteriaRule.greater, new ComparatorGreaterRuleMatcher(comparator));
            comparatorRules.put(CriteriaRule.equals, new ComparatorEqualsRuleMatcher(comparator));
            comparatorRules.put(CriteriaRule.less, new ComparatorLessRuleMatcher(comparator));
            comparatorRules.put(CriteriaRule.in, new ComparatorInRuleMatcher(comparator));
            comparatorRules.put(CriteriaRule.isNull, new CommonIsNullMatcher());
            compMap.put(compClass, new AllowableRules(null, comparatorRules));
        }
        return compMap.get(compClass);
    }

    /**
     * Возвращает тип данных описываемый данным набором правил
     *
     * @return тип данных
     */
    public DataType getDataType() {
        return dataType;
    }

    /**
     * Возвращает набор правил допустимый к применению к конкретному типу данных
     *
     * @return набор правил
     */
    @SuppressWarnings({"UnusedDeclaration"})
    public Set<CriteriaRule> getCriteriaRuleSet() {
        Set<CriteriaRule> result = new HashSet<CriteriaRule>(criteriaRules.length, 1f);
        for (CriteriaRule criteriaRule : criteriaRules) {
            if (criteriaRule != null) {
                result.add(criteriaRule);
            }
        }
        return result;
    }

    /**
     * Возвращает объект производящий сравнение для переданного правила
     *
     * @param criteriaRule правило
     * @return матчер
     */
    public RuleMatcher getRuleMatcher(CriteriaRule criteriaRule) {
        return ruleMatchers[criteriaRule.ordinal()];
    }

}
