/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

/**
 * todo: Важно! Предоставить возможность создания параметризорованных доступов
 *
 *  Пакет содержит интерфейсы, предназначенные для описания системных пользователей, наборов их доступов и ролей.
 *
 * @since 1.0
 * @SVNVersion "$Id: package-info.java 3376 2011-11-30 15:10:34Z dgomon $"
 */

package ru.effts.ine.core.userpermits;
