/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

/**
 * Пакет предназначен для работы с кешем данных. Для получения данных из кеша необходимо использовать класс
 * {@link Cache}. Также этот класс используется для управления данными в кеше - удаление и добавление новых данных.
 * <br>
 * При получения необходимо реализовать интерфейс {@link Producer}, который будет получать объект, если его нет в кеше.
 * <br>
 * Пример использования кеша:
 * <code><pre>
 * BigDecimal key = new BigDecimal(1);
 * String someObj = Cache.get("someRegion", key, new Producer() {
 *     Object get(Object key) throws GenericSystemException {
 *         return "Some cached string with id = " + ((BigDecimal) key).toString();
 *     }
 * });
 * </pre></code>
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: package-info.java 3378 2011-11-30 15:29:02Z dgomon $"
 */
package ru.effts.ine.utils.cache;
