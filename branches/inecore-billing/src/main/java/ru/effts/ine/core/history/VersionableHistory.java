/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

import ru.effts.ine.core.IneIllegalArgumentException;

import java.util.Date;

/**
 * Запись истории изменений версионного объекта
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: VersionableHistory.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface VersionableHistory extends IdentifiableHistory {

    String FD = "fd";
    String TD = "td";

    /**
     * Устанавливает дату начала "жизни" версии объекта
     *
     * @param fd начало"жизни" версии объекта
     * @throws ru.effts.ine.core.IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setFd(Date fd) throws IneIllegalArgumentException;

    /**
     * Возвращает дату начала "жизни" версии объекта
     *
     * @return начало "жизни" версии объекта
     */
    Date getFd();

    /**
     * Устанавливает дату завершения "жизни" версии объекта
     *
     * @param td окончание "жизни" версии объекта
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setTd(Date td) throws IneIllegalArgumentException;

    /**
     * Возвращает дату завершения "жизни" версии объекта
     *
     * @return окончание "жизни" версии объекта
     */
    Date getTd();

}
