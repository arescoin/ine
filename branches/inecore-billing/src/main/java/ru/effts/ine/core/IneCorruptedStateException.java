/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core;

/**
 * Выбрасывается при обнаружении некорректных данных
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: IneCorruptedStateException.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public class IneCorruptedStateException extends CoreException {

    public IneCorruptedStateException(String message) {
        super(message);
    }

    public IneCorruptedStateException(String message, Throwable cause) {
        super(message, cause);
    }

}
