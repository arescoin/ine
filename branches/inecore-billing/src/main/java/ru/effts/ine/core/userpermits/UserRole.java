/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Содержит методы для описания состояния роли
 * <p/>
 * Метод {@link #getCoreId()} возвращает идентификатор роли
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: UserRole.java 3376 2011-11-30 15:10:34Z dgomon $"
 */
public interface UserRole extends Versionable {

    String USER_ID = "userId";
    String ACTIVE = "active";

    /**
     * Возвращает идентификатор системного пользователя, обладателя роли
     *
     * @return идентификатор пользователя
     */
    BigDecimal getUserId();

    /**
     * Устанавливает идентификатор системного пользователя, обладателя роли
     *
     * @param userId идентификатор пользователя
     * @throws ru.effts.ine.core.IllegalIdException при попытке передачи в качестве параметра пустой ссылки, пустого
     * значения, или значения не удовлетворяющего требованиям к идентификатору
     */
    void setUserId(BigDecimal userId) throws IllegalIdException;


    /**
     * Возвращает признак активности роли
     *
     * @return <b>true</b> - роль активна, иначе - <b>false</b>
     */
    boolean isActive();

    /**
     * Устанавливает признак активности роли
     *
     * @param active <b>true</b> - роль активна, иначе - <b>false</b>
     */
    void setActive(boolean active);
}
