/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.constants;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

/**
 * Интерфейс описывает методы доступные для объекта-хранилища значения константы
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConstantValue.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface ConstantValue extends Versionable {

    String VALUE = "value";

    /**
     * Возвращает значение константы
     *
     * @return значение константы
     */
    String getValue();

    /**
     * Устанавливает значение константы
     *
     * @param value значение константы
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setValue(String value) throws IneIllegalArgumentException;

}
