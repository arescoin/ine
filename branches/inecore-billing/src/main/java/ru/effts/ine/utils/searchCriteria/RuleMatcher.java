/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria;

import java.io.Serializable;

/**
 * Определяет методы для выполнения сопоставления значений с "эталонами", сравнения
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: RuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public interface RuleMatcher extends Serializable {

    /**
     * Выполняет проверку на соответствие патерну ("эталону")
     * в некоторых случаях, количество патернов может быть более одного
     *
     * @param value   сравниваемое значение
     * @param pattern эталон или их набор
     * @return результат проверки  true - положительный, иначе - fale
     */
    public boolean match(Object value, Object... pattern);
}
