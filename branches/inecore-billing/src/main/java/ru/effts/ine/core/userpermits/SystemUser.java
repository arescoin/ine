/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.userpermits;

import ru.effts.ine.core.Versionable;

/**
 * Интерфейс описывает системного пользователя
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: SystemUser.java 3376 2011-11-30 15:10:34Z dgomon $"
 */
public interface SystemUser extends Versionable {

    /** признак активности учетной записи пользователя */
    String ACTIVE = "active";
    /** имя пользователя под которым он входит в систему(системный логин) */
    String SYS_LOGIN = "login";
    /** хеш код от пароля пользователя */
    String MD5_HASH = "md5";

    /**
     * Возвращает признак активности учетной записи пользователя
     *
     * @return <b>true</b> - учетка активна, иначе - <b>false</b>
     */
    boolean isActive();

    /**
     * Устанавливает признак активности учетной записи пользователя
     *
     * @param active <b>true</b> - учетка активна, иначе - <b>false</b>
     */
    void setActive(boolean active);

    /**
     * Возвращает системное имя пользователя
     *
     * @return системное имя
     */
    String getLogin();

    /**
     * Устанавливает системное имя пользователя
     *
     * @param login системное имя
     */
    void setLogin(String login);

    /**
     * Возвращает хэш код пароля пользователя
     *
     * @return md5 hash code
     */
    String getMd5();

    /**
     * Устанавливает хэш код пароля пользователя
     *
     * @param md5 устнавливаемый хеш код
     */
    void setMd5(String md5);

}
