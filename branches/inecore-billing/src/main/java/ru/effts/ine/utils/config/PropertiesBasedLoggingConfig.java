/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.config;

import ru.effts.ine.core.IneIllegalArgumentException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.*;

/**
 * Конфигуратор для подсистемы логирования
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: PropertiesBasedLoggingConfig.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public class PropertiesBasedLoggingConfig extends AbstractPropertiesBasedConfig {

    /** суфикс ключа для параметра указывающего на кастомный конфиг в основном файле */
    public static final String CUSTOM_LOG_PROP_NAME = ".custom";

    public static final String STANDALONE = ".STANDALONE";

    /** Ключ перечисления логеров */
    public static final String LOGGERS_PARAM = "loggers";
    /** Ключ для уровня логирования по-умолчанию */
    public static final String LEVEL_PARAM = "level";
    /** Ключ для лимита на размер файла */
    public static final String LIMIT_PARAM = "limit";
    /** Ключ для патерна файла лога */
    public static final String PATTERN_PARAM = "pattern";
    /** Ключ для кодировки логфайла */
    public static final String ENCODING_PARAM = "encoding";
    /** Ключ для признака дописывания в существующий файл */
    public static final String APPEND_PARAM = "append";
    /** Ключ для количества логов */
    public static final String COUNT_PARAM = "count";
    /** Ключ для класса форматера */
    public static final String FORMATTER_PARAM = "formatter";
    /** Ключ для имени логгера */
    public static final String NAME_PARAM = "name";
    /** Ключ для признака активности логера */
    public static final String ENABLED_PARAM = "enabled";

    /** Карта для группировки параметров по логгерам */
    private final Map<String, Properties> propertiesMap = new HashMap<String, Properties>(100);

    /** Свойства для параметров настройки логгеров */
    private Properties customProperties = new Properties();

    private static Logger logger;

    /**
     * Возвращает свойства расширенного логирования
     *
     * @return свойства
     */
    public Properties getCustomProperties() {
        return customProperties;
    }

    @Override
    public void read() throws ConfigurationException {

        try {
            String customLoggingFilePath;
            FileInputStream fileInputStream = new FileInputStream(configParam);
            if (System.getProperty(this.getClass().getName() + STANDALONE, Boolean.FALSE.toString()).
                    equals(Boolean.TRUE.toString())) {
                LogManager logManager = LogManager.getLogManager();
                logManager.readConfiguration(new FileInputStream(configParam));

                customLoggingFilePath =
                        LogManager.getLogManager().getProperty(this.getClass().getName() + CUSTOM_LOG_PROP_NAME);
            } else {
                Properties properties = new Properties();
                properties.load(fileInputStream);
                customLoggingFilePath = properties.getProperty(this.getClass().getName() + CUSTOM_LOG_PROP_NAME);
            }

            logger = Logger.getLogger(this.getClass().getName());

            if (customLoggingFilePath != null && customLoggingFilePath.length() > 0) {

                File configFile = new File(customLoggingFilePath);
                if (configFile.exists() && configFile.isFile() && configFile.canRead()) {
                    parseConfig(configFile);
                    initLoggers();
                } else {
                    logger.config("Invalid custom-logging config file [" + configFile.getAbsolutePath() + "]");
                }

            } else {
                logger.config("Not specified a custom logging [" + customLoggingFilePath + "]");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /** Инициализирует логеры сконфигурированные в системе */
    private void initLoggers() {
        for (Map.Entry<String, Properties> propertiesEntry : propertiesMap.entrySet()) {
            initLogger(propertiesEntry);
        }
    }

    /**
     * Инициализирует логер конфигурацией содержащийся в параметре метода
     *
     * @param propertiesEntry параметры для инициализации логера
     */
    private void initLogger(Map.Entry<String, Properties> propertiesEntry) {

        Properties properties = propertiesEntry.getValue();

        String enabledString = properties.getProperty(ENABLED_PARAM, Boolean.TRUE.toString());
        boolean enabled = Boolean.parseBoolean(enabledString);
        if (enabled) {

            String loggerName = properties.getProperty(NAME_PARAM);
            if (loggerName != null) {

                loggerName = loggerName.trim();
                if (loggerName.length() > 0) {

                    try {
                        Logger logger = Logger.getLogger(loggerName);
                        logger.setUseParentHandlers(false);
                        Handler handler = constructHandler(propertiesEntry.getValue());

                        logger.addHandler(handler);
                    } catch (Exception e) {
                        logger.log(Level.CONFIG, "Error while initializing custom logger [" + loggerName + "]", e);
                    }
                }
            }
        }
    }


    /**
     * Считывает параметры для конфигурирования логеров и вызывает метод для их сортировки
     *
     * @param configFile путь к файлу конфигурации
     *
     * @throws java.io.IOException при невозможности прочесть указанный файл
     */
    private void parseConfig(File configFile) throws IOException {

        customProperties.load(new FileInputStream(configFile));
        String loggersString = customProperties.getProperty(LOGGERS_PARAM);

        if (loggersString != null) {

            loggersString = loggersString.trim();
            if (loggersString.length() > 0) {
                String[] loggers = loggersString.split(",");

                for (int i = 0; i < loggers.length; i++) {
                    loggers[i] = loggers[i].trim();
                }

                if (loggers.length > 0) {
                    constructPropertiesMap(loggers);

                    if (logger.isLoggable(Level.CONFIG)) {
                        logger.log(Level.CONFIG, "Custom Logging properties: " + propertiesMap);
                    }

                } else {
                    logger.config("No custom loggers");
                }

            } else {
                logger.config("No custom loggers");
            }

        } else {
            logger.config("No custom loggers");
        }

    }

    /**
     * Сортирует параметры по конкретным логерам
     *
     * @param loggers перечисление логеров
     */
    private void constructPropertiesMap(String[] loggers) {

        for (String logger : loggers) {

            synchronized (propertiesMap) {
                if (!propertiesMap.containsKey(logger)) {
                    propertiesMap.put(logger, new Properties());
                }
            }

            String keyStart = logger + '.';
            for (Object o : customProperties.keySet()) {

                String key = (String) o;
                if (key.startsWith(keyStart)) {
                    propertiesMap.get(logger).put(key.substring(keyStart.length()), customProperties.getProperty(key));
                }

            }

        }
    }

    /**
     * Конструирует обработчики записей на основе переданных свойств
     *
     * @param props параметры обработчиков
     *
     * @return сконструированный обработчик
     * @throws java.io.IOException при невозможности открыть файл протокола
     * @throws ru.effts.ine.core.IneIllegalArgumentException
     *                             при некоррекном значении параметров файла протокола
     */
    private Handler constructHandler(Properties props) throws IOException, IneIllegalArgumentException {

        String pattern = props.getProperty(PATTERN_PARAM);
        if (pattern == null || pattern.trim().length() < 1) {
            throw new IneIllegalArgumentException("Wrong pattern for FileHandler");
        }

        if ("console".equals(pattern)) {
            Handler handler = new ConsoleHandler();
            prepareHandler(props, handler);
            return handler;
        }

        String appendStr = props.getProperty(APPEND_PARAM, customProperties.getProperty(ENCODING_PARAM));
        Boolean append = false;
        if (appendStr != null) {
            append = Boolean.parseBoolean(appendStr);
        }

        int limit = 0;
        String stringLimit = props.getProperty(LIMIT_PARAM, customProperties.getProperty(LIMIT_PARAM));
        if (stringLimit != null) {
            try {
                limit = Integer.parseInt(stringLimit);
            } catch (NumberFormatException e) {
                logger.log(Level.CONFIG, "Unknown limit format [" + stringLimit + "], will use default", e);
            }
        }

        int count = 1;
        String stringCount = props.getProperty(COUNT_PARAM, customProperties.getProperty(COUNT_PARAM));
        if (stringCount != null && stringCount.length() > 0) {
            try {
                count = Integer.parseInt(stringCount);
            } catch (NumberFormatException e) {
                logger.log(Level.CONFIG, "Unknown count format, will use default");
            }
        }

        FileHandler fileHandler = new FileHandler(pattern, limit, count, append);
        prepareHandler(props, fileHandler);

        return fileHandler;
    }

    /**
     * Устанавливает необходимые параметры на переданном обработчике
     *
     * @param props       параметры для настройки
     * @param fileHandler настраеваемый обработчик
     */
    private void prepareHandler(Properties props, Handler fileHandler) {

        Level level = null;
        try {
            level = Level.parse(props.getProperty(LEVEL_PARAM, customProperties.getProperty(LEVEL_PARAM)));
        } catch (IllegalArgumentException e) {
            logger.log(Level.CONFIG, "Error level", e);
        }

        String encoding = props.getProperty(ENCODING_PARAM, customProperties.getProperty(ENCODING_PARAM));


        if (level != null) {
            fileHandler.setLevel(level);
        }

        if (encoding != null && encoding.trim().length() > 0) {
            try {
                fileHandler.setEncoding(encoding);
            } catch (Exception e) {
                logger.log(Level.CONFIG, "Wrong encoding", e);
            }
        }

        String formatterClassName = props.getProperty(FORMATTER_PARAM, customProperties.getProperty(FORMATTER_PARAM));
        if (formatterClassName != null && formatterClassName.length() > 0) {
            try {
                Formatter formatter = (Formatter) Class.forName(formatterClassName).newInstance();
                fileHandler.setFormatter(formatter);
            } catch (Exception e) {
                logger.log(Level.CONFIG, "Error while formatter creat", e);
            }
        }
    }

}
