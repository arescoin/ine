/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria;

/**
 * Болванка для генерации классов извлекающих значения миную рефлекшн.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: Extractor.java 3825 2014-05-28 14:18:17Z DGomon $"
 */
public abstract class Extractor implements Cloneable {

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * Метод извлекает значение из переданного объекта, должен быть реализован в генерированных классах
     *
     * @param o объект содержащий значение
     * @return извлеченое значение
     * @throws Exception при возникновении ошибки в процессе работы сгенерированного кода
     */
    public abstract Object extractValue(Object o) throws Exception;

}
