/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import ru.effts.ine.utils.searchCriteria.RuleMatcher;

/**
 * Матчер для строки на предмет вхождения значения в значение шаблонной строки.
 * <p/>
 * Количество патернов: строго один
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: StringContainsRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class StringContainsRuleMatcher implements RuleMatcher {

    @Override
    public boolean match(Object value, Object... pattern) {
        String val = (String) value;
        String pat = (String) pattern[0];
        return val.contains(pat);
    }

}
