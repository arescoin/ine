/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import ru.effts.ine.utils.searchCriteria.RuleMatcher;

import java.util.Comparator;

/**
 * Базовый класс для матчеров, использующих компаратор для гибкого сравнения объектов.
 *
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: AbstractComparatorRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public abstract class AbstractComparatorRuleMatcher implements RuleMatcher {

    /** Компаратор, выполняющий сравнение объектов */
    protected Comparator comparator;

    protected AbstractComparatorRuleMatcher(Comparator comparator) {
        this.comparator = comparator;
    }
}
