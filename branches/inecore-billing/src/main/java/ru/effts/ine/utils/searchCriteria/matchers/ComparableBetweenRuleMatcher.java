/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import ru.effts.ine.utils.searchCriteria.RuleMatcher;

/**
 * Простой мачер для поиска в диапазоне. Применим исключитьельно для реализаций {@link Comparable}
 * <p/>
 * Количество патернов: строго два
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ComparableBetweenRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class ComparableBetweenRuleMatcher implements RuleMatcher {

    @SuppressWarnings({"unchecked"})
    @Override
    public boolean match(Object value, Object... pattern) throws ClassCastException {
        Comparable val = (Comparable) value;
        return val.compareTo(pattern[1]) <= 0 && val.compareTo(pattern[0]) >= 0;
    }

}
