/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.dic;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Предоставляет методы для описания словарной статьи
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: DictionaryEntry.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface DictionaryEntry extends Versionable {

    String DIC_ID = "dictionaryId";
    String UP_TERM = "parentTermId";

    /**
     * Возвращает идентификатор словаря владеющего данной словарной статьей
     * <p/>
     * Метод {@link #getCoreId()} возвращает идентификатор (код) словарной статьи
     *
     * @return идентификатор словаря
     */
    BigDecimal getDictionaryId();

    /**
     * Устанавливает идентификатор словаря владеющего данной словарной статьей
     *
     * @param dictionaryId идентификатор словаря
     * @throws IneIllegalArgumentException при попытке установить null или некорректное значение.
     */
    void setDictionaryId(BigDecimal dictionaryId) throws IneIllegalArgumentException;

    /**
     * Возвращает код верхней словарной статьи
     *
     * @return код верхней словарной статьи
     */
    BigDecimal getParentTermId();

    /**
     * Возвращает код верхней словарной статьи
     *
     * @param upTerminCode код верхней словарной статьи
     */
    void setParentTermId(BigDecimal upTerminCode);

}
