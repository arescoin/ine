/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.core.history;

/**
 * Запись истории изменений с признаком ее обработки системой нотификации
 * <p/>
 * Расширене простого {@link VersionableHistory}, с добавлением признака обработки события.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: HistoryEvent.java 3122 2011-10-31 09:54:31Z dgomon $"
 */
public interface HistoryEvent extends VersionableHistory {

    /** Перечисление полей */
    String PROCESSED = "processed";

    /**
     * Возвращает признак обработки события
     *
     * @return обработано - true, иначе - false
     */
    boolean isProcessed();

    /**
     * Устанавливает признак обработки события
     *
     * @param processed обработано - true, иначе - false
     */
    void setProcessed(boolean processed);

}
