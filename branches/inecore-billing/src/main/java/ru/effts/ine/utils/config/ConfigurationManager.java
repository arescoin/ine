/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Класс предоставляет механизм иничиализации и доступа к системным конфигурациям.
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ConfigurationManager.java 3380 2011-11-30 15:32:42Z dgomon $"
 */
public class ConfigurationManager {
    private final static ConfigurationManager CONFIGURATION_MANAGER = new ConfigurationManager();

    /** Системная переменная для поиска основной конфигурации */
    public final static String SYSTEM_CONFIC_PATH_PROPERY = "ru.effts.ine.utils.config.ConfigurationManager.path";
    /** Путь к основной конфигурации по-умолчанию */
    public final static String DEFAULT_SYSTEM_CONFIC_PATH = "conf/system.properties";

    /** суфиксы для стандартных конфигураций */
    public final static String CONFIG_CLASS = "configClass";
    public final static String CONFIG_PARAM = "configParam";
    public final static String ADDITIONALS = "additionals";

    /** Основной конфиг */
    private RootConfig rootConfig;

    /**
     * Карта конфигов подсистем, в ключе находится псевдоним для подсистемы,
     * используемый в файле основной конфиги, в значении параметры конфиги
     */
    private Map<String, SystemConfig> subConfigs = new HashMap<String, SystemConfig>();

    /**
     * Возвращает инициализированный инстанс
     *
     * @return объект для доступа к системным конфигурациям
     */
    public static ConfigurationManager getManager() {
        return CONFIGURATION_MANAGER;
    }

    private ConfigurationManager() {

        rootConfig = new RootConfig();

        try {
            rootConfig.init(DEFAULT_SYSTEM_CONFIC_PATH);
            rootConfig.read();

            initOthers();
        } catch (ConfigurationException e) {
            e.printStackTrace(System.err);
        }

    }

    /**
     * Метод возвращает основной конфиг приложения
     *
     * @return асновной конфиг
     */
    public RootConfig getRootConfig() {
        return rootConfig;
    }

    /**
     * Возвращает конфигурацию по ее системному имени
     *
     * @param systemName системное имя конфигурации
     * @return конфигурация подсистемы
     */
    public SystemConfig getConfigFor(String systemName) {
        return subConfigs.get(systemName);
    }

    /** Инициализирует конфигурации подсистем */
    private void initOthers() {

        SystemConfig systemConfig;
        Properties subProperties;
        String cfgClassName;
        String additionals;
        String[] additionalStrings = null;

        for (Map.Entry<String, Properties> propertiesEntry : rootConfig.getSubSistemsProperties().entrySet()) {
            String systemName = propertiesEntry.getKey();

            subProperties = propertiesEntry.getValue();

            if (subProperties == null) {
                System.err.println("Invalid (null) system properties [" + systemName + "]");
                continue;
            }

            cfgClassName = subProperties.getProperty(CONFIG_CLASS);

            if (cfgClassName == null || cfgClassName.length() < 1) {
                System.err.println("Found wrong className for [" + systemName + "]");
                continue;
            }

            try {
                //noinspection UnusedAssignment
                systemConfig = (SystemConfig) Class.forName(cfgClassName).newInstance();
            } catch (Exception e) {
                System.err.println("Can't initialize configuration [" + systemName + "]");
                e.printStackTrace(System.err);
                continue;
            }

            additionals = subProperties.getProperty(ADDITIONALS);

            if (additionals != null && additionals.length() > 0) {
                additionalStrings = additionals.split(" ");
            }

            try {
                systemConfig.init(subProperties.getProperty(CONFIG_PARAM), additionalStrings);
                systemConfig.read();
            } catch (ConfigurationException e) {
                System.err.println("Can't initialize configuration [" + systemName + "]");
                e.printStackTrace(System.err);
                continue;
            }

            subConfigs.put(systemName, systemConfig);
        }
    }

}
