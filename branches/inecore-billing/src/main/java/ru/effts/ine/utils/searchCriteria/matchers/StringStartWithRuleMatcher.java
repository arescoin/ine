/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.searchCriteria.matchers;

import ru.effts.ine.utils.searchCriteria.RuleMatcher;

/**
 * Матчер для строки на предмет начала значения со значения шаблонной строки.
 * <p/>
 * Количество патернов: строго один
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: StringStartWithRuleMatcher.java 3382 2011-11-30 15:38:37Z dgomon $"
 */
public class StringStartWithRuleMatcher implements RuleMatcher {

    @Override
    public boolean match(Object value, Object... pattern) {
        String val = (String) value;
        String pat = (String) pattern[0];
        return val.startsWith(pat);
    }

}
