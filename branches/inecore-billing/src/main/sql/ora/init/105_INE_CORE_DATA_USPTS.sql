/*
"$Id: 105_INE_CORE_DATA_USPTS.sql 3576 2012-01-24 08:22:42Z ikulkov $"
*/

--  Создаем рутовую роль, пермиты и рутового пользователя + бесправного пользователя

/*
  SYS_ADMIN
  USER_ADMIN
  OBJECT_ADMIN
  EQUIPMENT_ADMIN
  BRANCH_OFFICER
  TECH_SPECIALIST
  TECH_SUPPORT_COORDINATOR
*/

DECLARE

  -- Роли
  ROLE_SYS_ADMIN                NUMBER;
  ROLE_USER_ADMIN               NUMBER;
  ROLE_OBJECT_ADMIN             NUMBER;
  ROLE_EQUIPMENT_ADMIN          NUMBER;
  ROLE_BRANCH_OFFICER           NUMBER;
  ROLE_TECH_SPECIALIST          NUMBER;
  ROLE_TECH_SUPPORT_COORDINATOR NUMBER;

  -- Доступы
  PERMIT_SYSTEM_ACCESS NUMBER;
  PERMIT_SYSOBJ_ACCESS NUMBER;
  PERMIT_PARTY_ACCESS  NUMBER;
  PERMIT_PARTY_MEMBER  NUMBER;

  -- Системные пользователи
  USER_ROOT    NUMBER;
  USER_ADAPTER NUMBER;
  USER_IMPORT  NUMBER;

  -- Web-пользователи
  USER_USER_ADMIN               NUMBER;
  USER_OBJECT_ADMIN             NUMBER;
  USER_EQUIPMENT_ADMIN          NUMBER;
  USER_BRANCH_OFFICER           NUMBER;
  USER_TECH_SPECIALIST          NUMBER;
  USER_TECH_SUPPORT_COORDINATOR NUMBER;

  -- Системные даты
  VFD TIMESTAMP := INE_CORE_SYS.GET_SYSTEM_FROMDATE;
  VTD TIMESTAMP := INE_CORE_SYS.GET_SYSTEM_TODATE;

BEGIN

  -----------------------------------------------------------------------------------------------------
  ------------------------------------------ Роли ----------------------------------------------------
  -----------------------------------------------------------------------------------------------------

  -- Роль SYS_ADMIN
  SELECT SEQ_ROLES.NEXTVAL INTO ROLE_SYS_ADMIN FROM DUAL;
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_SYS_ADMIN, 'SYS_ADMIN', 'Системный администратор', VFD, VTD);

  -- Роль USER_ADMIN
  SELECT SEQ_ROLES.NEXTVAL INTO ROLE_USER_ADMIN FROM DUAL;
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_USER_ADMIN, 'USER_ADMIN', 'Администратор пользователей', VFD, VTD);

  -- Роль OBJECT_ADMIN
  SELECT SEQ_ROLES.NEXTVAL INTO ROLE_OBJECT_ADMIN FROM DUAL;
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_OBJECT_ADMIN, 'OBJECT_ADMIN', 'Администратор объектов учета', VFD, VTD);

  -- Роль EQUIPMENT_ADMIN
  SELECT SEQ_ROLES.NEXTVAL INTO ROLE_EQUIPMENT_ADMIN FROM DUAL;
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_EQUIPMENT_ADMIN, 'EQUIPMENT_ADMIN', 'Администратор оборудования', VFD, VTD);

  -- Роль BRANCH_OFFICER
  SELECT SEQ_ROLES.NEXTVAL INTO ROLE_BRANCH_OFFICER FROM DUAL;
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_BRANCH_OFFICER, 'BRANCH_OFFICER', 'Сотрудник филиала', VFD, VTD);

  -- Роль TECH_SPECIALIST
  SELECT SEQ_ROLES.NEXTVAL INTO ROLE_TECH_SPECIALIST FROM DUAL;
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_TECH_SPECIALIST, 'TECH_SPECIALIST', 'Технический специалист', VFD, VTD);

  -- Роль TECH_SUPPORT_COORDINATOR
  SELECT SEQ_ROLES.NEXTVAL INTO ROLE_TECH_SUPPORT_COORDINATOR FROM DUAL;
  INSERT INTO ROLES (N, ROLE_NAME, DSC, FD, TD) VALUES (
    ROLE_TECH_SUPPORT_COORDINATOR, 'TECH_SUPPORT_COORDINATOR', 'Координатор техподдержки', VFD, VTD);

  COMMIT;

  -----------------------------------------------------------------------------------------------------
  --------------------------------------------- Доступы -----------------------------------------------
  -----------------------------------------------------------------------------------------------------

  -- Доступ к системе
  SELECT SEQ_PERMITS.NEXTVAL INTO PERMIT_SYSTEM_ACCESS FROM DUAL;
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_SYSTEM_ACCESS, 'System Access', null, 0, 'Разрешение на доступ к системе.', VFD, VTD);

  -- Доступ к системным объектам
  SELECT SEQ_PERMITS.NEXTVAL INTO PERMIT_SYSOBJ_ACCESS FROM DUAL;
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_SYSOBJ_ACCESS, 'System''s Objects Access', null, 0, 'Разрешение на доступ к системным объектам.', VFD, VTD);

  -- Доступ к объектам филиала
  SELECT SEQ_PERMITS.NEXTVAL INTO PERMIT_PARTY_ACCESS FROM DUAL;
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_PARTY_ACCESS, 'Object User Access by Party', null, 0, 'Разрешение на доступ к объектам филиала.', VFD, VTD);

  -- Доступ определяющий принадлежность пользователя филиалу
  SELECT SEQ_PERMITS.NEXTVAL INTO PERMIT_PARTY_MEMBER FROM DUAL;
  INSERT INTO PERMITS (N, PERMIT_NAME, VAL_MASK, IS_VAL_REQ, DSC, FD, TD) VALUES (
  PERMIT_PARTY_MEMBER, 'User to Party Relationship', null, 0, 'Принадлежность пользователя к филиалу.', VFD, VTD);

  COMMIT;

  -----------------------------------------------------------------------------------------------------
  ----------------------------------------- Настройка ролей -------------------------------------------
  -----------------------------------------------------------------------------------------------------

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_SYS_ADMIN, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_USER_ADMIN, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_OBJECT_ADMIN, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_EQUIPMENT_ADMIN, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_BRANCH_OFFICER, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_TECH_SPECIALIST, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  INSERT INTO ROLE_PERMITS (ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, DSC, FD, TD)
  VALUES (ROLE_TECH_SUPPORT_COORDINATOR, PERMIT_SYSTEM_ACCESS, 15, null, 'dsc', VFD, VTD);

  COMMIT;

  -----------------------------------------------------------------------------------------------------
  ------------------------------------------ Пользователи --------------------------------------------
  -----------------------------------------------------------------------------------------------------

  -- Пользователь root, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_ROOT FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, 'root', '63a9f0ea7bb98050796b649e85481845', 1, 'Root User', VFD, VTD);

  -- Пользователь apapter, пароль 'adapter'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_ADAPTER FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ADAPTER, 'adapter', '8a7a38cfa57d4ac98cced700b804556a', 1, 'Adapter User', VFD, VTD);

  -- Пользователь import, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_IMPORT FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_IMPORT, 'import', '63a9f0ea7bb98050796b649e85481845', 1, 'Data Importer', VFD, VTD);

  ----------------------------------------------- web --------------------------------------------------
  -- Пользователь user admin, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_USER_ADMIN FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, 'user_admin', '63a9f0ea7bb98050796b649e85481845', 1, 'User Admin', VFD, VTD);

  -- Пользователь object admin, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_OBJECT_ADMIN FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, 'obj_admin', '63a9f0ea7bb98050796b649e85481845', 1, 'Object Admin', VFD, VTD);

  -- Пользователь equipment admin, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_EQUIPMENT_ADMIN FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, 'eq_admin', '63a9f0ea7bb98050796b649e85481845', 1, 'Equipment Admin', VFD, VTD);

  -- Пользователь branch officer, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_BRANCH_OFFICER FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, 'officer', '63a9f0ea7bb98050796b649e85481845', 1, 'Branch (Party) Officer', VFD, VTD);

  -- Пользователь tech specialist, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_TECH_SPECIALIST FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, 'techspec', '63a9f0ea7bb98050796b649e85481845', 1, 'Tech Specialist', VFD, VTD);

  -- Пользователь tech support coordinator, пароль 'root'
  SELECT SEQ_SYS_USERS.NEXTVAL INTO USER_TECH_SUPPORT_COORDINATOR FROM DUAL;
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, 'techsupport',
          '63a9f0ea7bb98050796b649e85481845', 1, 'Techsupport Coordinator', VFD, VTD);

  COMMIT;

  -----------------------------------------------------------------------------------------------------
  ------------------------------------- Настройка пользователей ---------------------------------------
  -----------------------------------------------------------------------------------------------------

  ----------------------------------------- Роли пользователей ----------------------------------------

  -- все роли для пользователя root
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_USER_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_OBJECT_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_EQUIPMENT_ADMIN, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_TECH_SPECIALIST, 1, 'DSC', VFD, VTD);

  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_TECH_SUPPORT_COORDINATOR, 1, 'DSC', VFD, VTD);

  -- 'всемогущая' роль пользователю адаптер
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ADAPTER, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  -- 'всемогущая' роль пользователю import
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_IMPORT, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю user_admin
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, ROLE_USER_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю obj_admin
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, ROLE_OBJECT_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю eq_admin
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, ROLE_EQUIPMENT_ADMIN, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю officer
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю techspec
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, ROLE_TECH_SPECIALIST, 1, 'DSC', VFD, VTD);

  -- Соответствующая роль пользователю techsupport
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, ROLE_TECH_SUPPORT_COORDINATOR, 1, 'DSC', VFD, VTD);

  COMMIT;

  --------------------------------------- Доступы пользователей ---------------------------------------

  -- Пользовательский доступ по филиалу
  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_SYS_ADMIN, PERMIT_PARTY_ACCESS, 14, '1,2,3,4,5,6,7,8,9,10,11,12', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, ROLE_USER_ADMIN, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, ROLE_OBJECT_ADMIN, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, ROLE_EQUIPMENT_ADMIN, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, ROLE_BRANCH_OFFICER, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, ROLE_TECH_SPECIALIST, PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, ROLE_TECH_SUPPORT_COORDINATOR,
          PERMIT_PARTY_ACCESS, 14, '1', 1, 'DSC', VFD, VTD);

  -- Принадлежность пользователя филиалу
  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_ROOT, ROLE_SYS_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_USER_ADMIN, ROLE_USER_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_OBJECT_ADMIN, ROLE_OBJECT_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_EQUIPMENT_ADMIN, ROLE_EQUIPMENT_ADMIN, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_BRANCH_OFFICER, ROLE_BRANCH_OFFICER, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SPECIALIST, ROLE_TECH_SPECIALIST, PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  INSERT INTO USER_PERMITS (USER_N, ROLE_N, PERMIT_N, CRUD_MASK, PERMIT_VAL, IS_ACTIVE, DSC, FD, TD)
  VALUES (USER_TECH_SUPPORT_COORDINATOR, ROLE_TECH_SUPPORT_COORDINATOR,
          PERMIT_PARTY_MEMBER, 14, '1', 1, 'DSC', VFD, VTD);

  COMMIT;

  -----------------------------------------------------------------------------------------------------

END;
/
