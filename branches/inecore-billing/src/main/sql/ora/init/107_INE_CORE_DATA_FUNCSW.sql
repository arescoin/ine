/*
"$Id: 107_INE_CORE_DATA_FUNCSW.sql 3700 2012-03-01 14:22:58Z dgomon $"
*/
DECLARE
  VFD  TIMESTAMP := INE_CORE_SYS.GET_SYSTEM_FROMDATE;
  VTD  TIMESTAMP := INE_CORE_SYS.GET_SYSTEM_TODATE;

  NUM NUMBER;
BEGIN

  -- Переключатель регулирует возможность создания будущих версий сущностей
  SELECT SEQ_FUNC_SWITCH.NEXTVAL INTO NUM FROM DUAL;
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES
  (NUM, 'FUTURE_HISTORY', 'Переключатель регулирует возможность создания будущих версий сущностей');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 0, VFD, 0, 0, VFD, VTD);



  -- Переключатель регулирует запись изменяемого объекта в таблицу с историей изменения объекта
  SELECT SEQ_FUNC_SWITCH.NEXTVAL INTO NUM FROM DUAL;
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES
  (NUM, 'SAVE_HISTORY', 'Переключатель регулирует запись изменяемого объекта в таблицу с историей изменения объекта');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 0, VFD, 0, 0, VFD, VTD);



  -- Переключатель регулирует запись действий над изменяемым объектом
  SELECT SEQ_FUNC_SWITCH.NEXTVAL INTO NUM FROM DUAL;
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES
  (NUM, 'REGISTER_ACTIONS', 'Переключатель регулирует запись действий над изменяемым объектом');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 0, VFD, 0, 0, VFD, VTD);



  -- Переключатель регулирует запись пользовательских атрибутов действия над изменяемым объектом
  SELECT SEQ_FUNC_SWITCH.NEXTVAL INTO NUM FROM DUAL;
  INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES (NUM, 'SAVE_ACTION_ATTRIBUTES',
    'Переключатель регулирует запись пользовательских атрибутов действия над изменяемым объектом.'||
    'Работает только при включённом переключателе REGISTER_ACTIONS');

  INSERT INTO FUNC_STATE (FUNC_N, STATE, C_DATE, CM, STATE_CODE, FD, TD) VALUES (NUM, 0, VFD, 0, 0, VFD, VTD);

  COMMIT;

END;
/

