/*
"$Id: 012_INE_CORE_CUSTOM_ATTRS_DSC.sql 2674 2011-08-26 12:45:41Z dgomon $"
*/

-- Таблица описателей произвольных атрибутов.
CREATE TABLE CUSTOM_ATTRS_DSC
(
  N             NUMBER                 NOT NULL
, SYS_OBJ_N     NUMBER                 NOT NULL -- есть паттерн
, CUSTOM_TYPE   NUMBER                          -- есть паттерн
, ATTR_TYP      NUMBER                 NOT NULL -- есть паттерн
, ATTR_NAME     VARCHAR2(2000)         NOT NULL
, IS_REQUIRED   NUMBER                 NOT NULL
, PATTERN       VARCHAR2(2000)
, LIMITS        VARCHAR2(2000)
, DSC           VARCHAR2(2000)
, FD            TIMESTAMP              NOT NULL
, TD            TIMESTAMP              NOT NULL
, CONSTRAINT    UK_CUSTOM_ATTRS_DSC_ID UNIQUE (N) ENABLE
);

CREATE INDEX IND_CUSTOM_ATTRS_DSC_SYSOBJ_N   ON  CUSTOM_ATTRS_DSC(SYS_OBJ_N);
CREATE INDEX IND_CUSTOM_ATTRS_DSC_FDTD       ON  CUSTOM_ATTRS_DSC(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_CUSTOM_ATTRS_DSC INCREMENT BY 1 START WITH 100 ORDER;

-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER;
  DIC_N      NUMBER;
  DD_N       NUMBER := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMBER := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
  --DATA_TYPES
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'CUSTOM_ATTRS_DSC', null,
    'Таблица для описания характеристик произвольных атрибутов расширяемых объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
    'ru.effts.ine.core.structure.CustomAttribute', system_ID, 'Интерфейс описания произвольного атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'N', system_ID, 'Уникальный номер атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'SYS_OBJ_N', system_ID, 'Номер системного объекта, SYS_OBJ', INE_CORE_SYS_OBJ.get_Column_N('SYS_OBJ', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'CUSTOM_TYPE', system_ID,
    'Номер агрегирующего custom-типа, расширяющего базовый системный объект из SYS_OBJ_N. Может быть null',
    INE_CORE_SYS_OBJ.get_Column_N('CUSTOM_TYPES_DSC', 'N'));

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'DATA_TYPES';
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'ATTR_TYP', system_ID, 'Тип атрибута. Словарь №' || DIC_N,
    '[' ||DD_N || ':' || DIC_N || ']' || DD_CODE);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'ATTR_NAME', system_ID, 'Имя атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'IS_REQUIRED', system_ID, 'Признак обязательности атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'PATTERN', system_ID, 'Паттерн системного объекта для возможности работы с другими объектами InE');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'LIMITS', system_ID, 'Ограничения на значения атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'DSC', system_ID, 'Описание атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP,
    'SEQ_CUSTOM_ATTRS_DSC', system_ID, 'Последовательность для идентификатора');

  COMMIT;

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CUSTOM_ATTRS_DSC');

  COMMIT;

END;

/

-- Таблица значений по-умолчанию для произвольных атрибутов.
CREATE TABLE CUSTOM_ATTRS_DEF_VALS
(
  ATTR_N          NUMBER          NOT NULL -- есть паттерн
, VAL_STRING      VARCHAR2(2000)
, VAL_LONG        NUMBER
, VAL_BIGDECIMAL  NUMBER
, VAL_BOOLEAN     NUMBER(1)
, VAL_DATE        TIMESTAMP
, DSC             VARCHAR2(1)
, FD              TIMESTAMP       NOT NULL
, TD              TIMESTAMP       NOT NULL
, CONSTRAINT      UK_CUSTOM_ATTRS_DEF_VALS_ID UNIQUE (ATTR_N) ENABLE
);

CREATE INDEX IND_CUSTOM_ATTRS_DEF_VALS_FDTD     ON  CUSTOM_ATTRS_DEF_VALS(FD, TD);

-- Теперь заносим все в SYS_OBJ
DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'CUSTOM_ATTRS_DEF_VALS', null,
    'Таблица для хранения значений по-умолчанию для описателей произвольных атрибутов расширяемых объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
    'ru.effts.ine.core.structure.CustomAttributeDefVal', system_ID,
    'Интерфейс значения по-умолчанию произвольного атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'ATTR_N', system_ID, 'Номер описателя атрибута', INE_CORE_SYS_OBJ.get_Column_N('CUSTOM_ATTRS_DSC', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'VAL_STRING', system_ID, 'Строковое значение');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'VAL_LONG', system_ID, 'Числовое значение (целое)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'VAL_BIGDECIMAL', system_ID, 'Числовое значение (дробное)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'VAL_BOOLEAN', system_ID, 'Булевое значение');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'VAL_DATE', system_ID, 'Значение дата');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'DSC', system_ID, 'Описание-комментарий. Не используется');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  COMMIT;

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('CUSTOM_ATTRS_DEF_VALS');

  COMMIT;

END;

/