/*
"$Id: 013_INE_CORE_ENTITIES.sql 3576 2012-01-24 08:22:42Z ikulkov $"
*/


-- Организация (Company)
CREATE TABLE COMPANY
(
  N              NUMBER               NOT NULL
, NAME           VARCHAR2(500)        NOT NULL
, TYPE           NUMBER               NOT NULL -- есть паттерн
, PROPERTY_TYPE  NUMBER               NOT NULL -- есть паттерн
, DSC            VARCHAR2(2000)
, FD             TIMESTAMP            NOT NULL
, TD             TIMESTAMP            NOT NULL
, CONSTRAINT     UK_COMPANY_ID        UNIQUE (N) ENABLE
);

-- Индексы
CREATE INDEX IND_COMPANY_FDTD  ON   COMPANY(FD, TD);

-- Последовательность id-шников
CREATE SEQUENCE SEQ_COMPANY_N INCREMENT BY 1 START WITH 10 ORDER;

-- Теперь заносим все в SYS_OBJ
DECLARE
  VAR        NUMBER;
  system_ID  NUMBER := 0;
  DIC_N      NUMBER;
  DD_N       NUMBER := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMBER := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP,
    'COMPANY', null, 'Описание организации COMPANY');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
    'ru.effts.ine.oss.entity.Company', system_ID, 'Интерфейс организации');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'N', system_ID, 'Идентификатор организации');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'DSC', system_ID, 'Описание-комментарий организации');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'NAME', system_ID, 'Название организации');

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'COMPANY_TYPES_ABBR';
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'TYPE', system_ID, 'Тип организации, сокращённое название. Словарь №' || DIC_N,
    '['|| DD_N ||':'||DIC_N||']'|| DD_CODE);

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'COMPANY_TYPES_ABBR';
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'PROPERTY_TYPE', system_ID, 'Тип собственности организации. Словарь ' || DIC_N,
    '['||DD_N||':'|| DIC_N ||']'||DD_CODE);

  -- Последовательность номеров
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP,
    'SEQ_COMPANY_N', system_ID, 'Последовательность номеров для организаций');

  COMMIT;

  -- Добавляем историю
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('COMPANY');

  INE_CORE_SYS_OBJ.create_CustomAttrs_Tables('COMPANY');

  COMMIT;

END;

/

-- ====================================================================================================================

-- Персона (Person)
CREATE TABLE PERSON
(
  N                   NUMBER          NOT NULL
, FAMILY_NAME         VARCHAR2(200)   NOT NULL
, NAME                VARCHAR2(200)   NOT NULL
, MIDDLE_NAME         VARCHAR2(200)   NOT NULL
, ALIAS               VARCHAR2(200)
, FAMILY_NAME_PREFIX  NUMBER          NOT NULL -- есть паттерн
, FAMILY_GENERATION   VARCHAR2(200)
, ADDRESS_FORM        VARCHAR2(200)
, DSC                 VARCHAR2(2000)
, FD                  TIMESTAMP       NOT NULL
, TD                  TIMESTAMP       NOT NULL
, CONSTRAINT          UK_PERSON_ID    UNIQUE (N) ENABLE
);

-- Индексы
CREATE INDEX IND_PERSON_FDTD  ON   PERSON(FD, TD);

-- Последовательность id-шников
CREATE SEQUENCE SEQ_PERSON_N INCREMENT BY 1 START WITH 10 ORDER;

-- Теперь заносим все в SYS_OBJ
DECLARE
  VAR        NUMBER;
  DIC_N      NUMBER;
  system_ID  NUMBER := 0;
  DD_N       NUMBER := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'DIC_N');
  DD_CODE    NUMBER := INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'CODE');
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP,
    'PERSON', null, 'Описание персоны PERSON');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
    'ru.effts.ine.oss.entity.Person', system_ID, 'Интерфейс персоны');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'N', system_ID, 'Идентификатор персоны');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'DSC', system_ID, 'Описание-комментарий персоны');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'FAMILY_NAME', system_ID, 'Фамилия персоны');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'NAME', system_ID, 'Имя персоны');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'MIDDLE_NAME', system_ID, 'Отчество персоны');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'ALIAS', system_ID, 'Псевдоним, Ник-нейм');

  SELECT N INTO DIC_N FROM DIC WHERE DIC_NAME = 'PERSON_FAMILY_NAME_PREFIX_ABBR';
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'FAMILY_NAME_PREFIX', system_ID, 'Предпочтительное обращение. Словарь ' || DIC_N,
    '['|| DD_N ||':'|| DIC_N ||']'|| DD_CODE);

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'FAMILY_GENERATION', system_ID, 'Семейное поколение');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
    'ADDRESS_FORM', system_ID, 'Тип прописки');

  -- Последовательность номеров
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP,
     'SEQ_PERSON_N', system_ID, 'Последовательность номеров для персон');

  COMMIT;

  -- Добавляем историю
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('PERSON');

  INE_CORE_SYS_OBJ.create_CustomAttrs_Tables('PERSON');

  COMMIT;

END;

/

