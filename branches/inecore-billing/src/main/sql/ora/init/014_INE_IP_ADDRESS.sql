/*
* $Id: 014_INE_IP_ADDRESS.sql 3219 2011-11-21 13:48:26Z ikulkov $
*/


-- Логический Ресурс IP_ADDRESS
CREATE TABLE IP_ADDRESS
(
  N              NUMBER          NOT NULL
, IP_ADDR        RAW(128)        NOT NULL
, DSC            VARCHAR2(2000)
, FD             TIMESTAMP       NOT NULL
, TD             TIMESTAMP       NOT NULL
, CONSTRAINT     UK_IP_ADDRESS   UNIQUE (N) ENABLE
);

-- Последовательность id-шников
CREATE SEQUENCE SEQ_IP_ADDRESS_N INCREMENT BY 1 START WITH 10 ORDER;

-- Теперь заносим все в SYS_OBJ
DECLARE
  VAR        NUMBER;
  system_ID  NUMBER := 0;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP,
     'IP_ADDRESS', null, 'IP адрес в битовом представление');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
     'ru.effts.ine.core.ipaddress.IPAddressValue', system_ID, 'Интерфейс IP адреса');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'N', system_ID, 'Идентификатор');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
      'IP_ADDR', system_ID, 'значение IP-адреса');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'DSC', system_ID, 'Описание ресурса');

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP,
     'SEQ_IP_ADDRESS_N', system_ID, 'Последовательность для идентификаторов');

  COMMIT;

  -- Добавляем историю
  VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('IP_ADDRESS');

  -- Структура для кастом атрибутов
  INE_CORE_SYS_OBJ.create_CustomAttrs_Tables('IP_ADDRESS');

  COMMIT;

END;

/
