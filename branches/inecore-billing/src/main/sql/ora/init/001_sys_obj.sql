/*
"$Id: 001_sys_obj.sql 2674 2011-08-26 12:45:41Z dgomon $"
*/

-- Создаем корневую таблицу системы
CREATE TABLE SYS_OBJ
(
  N             NUMBER          NOT NULL
, TYP           NUMBER          NOT NULL -- есть паттерн
, OBJ_NAME      VARCHAR2(2000)  NOT NULL
, UP            NUMBER                   -- есть паттерн
, PATTERN       VARCHAR2(200)
, DSC           VARCHAR2(2000)
, FD            TIMESTAMP       NOT NULL
, TD            TIMESTAMP       NOT NULL
, CONSTRAINT PK_SYS_OBJ     PRIMARY KEY (N) ENABLE
, CONSTRAINT FK_SYS_OBJ_UPN FOREIGN KEY (UP) REFERENCES SYS_OBJ(N) ENABLE
);

  -- Накатываем индексы для SYS_OBJ
  CREATE INDEX IND_SYS_OBJ_TYP      ON   SYS_OBJ (TYP);
  CREATE INDEX IND_SYS_OBJ_NAME_UP  ON   SYS_OBJ (OBJ_NAME, UP);
  CREATE INDEX IND_SYS_OBJ_FDTD     ON   SYS_OBJ (FD, TD);

-- Теперь создаем сиквенс для идентификаторов в SYS_OBJ
CREATE SEQUENCE SEQ_SYS_OBJ_N INCREMENT BY 1 START WITH 1 ORDER;

START ./002_INE_CORE_SYS_pcg.sql
/
START ./003_INE_CORE_SYS_OBJ_pcg.sql
/

DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'SYS_OBJ', null,
     'Содержит описание существующих в системе таблиц, и прочих объектов БД');

  dbms_output.put_line('New Obj. Id: ' || system_ID);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
     'ru.effts.ine.core.structure.SystemObject', system_ID, 'Интерфейс системного объекта');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'N', system_ID, 'Cистемный номер элемента');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'TYP', system_ID, 'Тип элемента. Словарь №4');
  -- Паттерн будет добавлен позже, в скрипте 004_INE_CORE_DIC.sql

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'OBJ_NAME', system_ID, 'Системное название объекта');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'UP', system_ID, 'Ссылка на системный номер родительского элемента');
  -- Паттерн будет добавлен позже, в скрипте 004_INE_CORE_DIC.sql

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'PATTERN', system_ID, 'Паттерн для описания ограничений на данные в столбце');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'DSC', system_ID, 'Описание элемента');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);
  COMMIT;

  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('SYS_OBJ');

  COMMIT;

END;

/

-- создаем таблицу с историей пользовательской активности
CREATE TABLE OBJ_HISTORY
 (
   N            NUMBER               NOT NULL,
   ENT_ID       VARCHAR2(500)        NOT NULL,
   FD           TIMESTAMP            NOT NULL,
   TD           TIMESTAMP            NOT NULL,
   USER_ID      NUMBER               NOT NULL, -- есть паттерн
   ACTION_DATE  TIMESTAMP            NOT NULL,
   TYP          NUMBER               NOT NULL,
   REASON       VARCHAR2(2000),
   PROCESSED    NUMBER(1) DEFAULT 0  NOT NULL
 );

  -- Накатываем индексы для SYS_OBJ
  CREATE INDEX  IND_OBJ_HISTORY_N         ON   OBJ_HISTORY (N);
  CREATE INDEX  IND_OBJ_HISTORY_ENT       ON   OBJ_HISTORY (ENT_ID);
  CREATE INDEX  IND_OBJ_HISTORY_ACT_DATE  ON   OBJ_HISTORY (ACTION_DATE);
  CREATE INDEX  IND_OBJ_HISTORY_ACT_TYP   ON   OBJ_HISTORY (TYP);
  CREATE INDEX  IND_OBJ_HISTORY_FDTD      ON   OBJ_HISTORY (FD, TD);

DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'OBJ_HISTORY', null,
     'Содержит историю изменений системных объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
     'ru.effts.ine.core.history.VersionableHistory', system_ID, 'Интерфейс истории изменения версионного объекта');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'N', system_ID, 'Cистемный номер элемента (системного объекта)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'ENT_ID', system_ID, 'Идентификатор сущности (либо системный номер, либо составной номер)');

  -- эт мы ставим от и до
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'USER_ID', system_ID, 'Cистемный номер пользователя внесшего изменения');
  -- Паттерн будет добавлен позже, в скрипте 005_INE_CORE_USPTS.sql

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'ACTION_DATE', system_ID, 'Дата активного действия');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'TYP', system_ID, 'Тип действия');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'REASON', system_ID, 'Обоснование действия');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'PROCESSED', system_ID, 'Признак выполнения асинхронной обработки записи');
  COMMIT;

END;

/

-- создаем таблицу с пользовательскими атрибутами объектов истории пользовательской активности
CREATE TABLE OBJ_HISTORY_ATTRS
 (
   N            NUMBER               NOT NULL,
   ENT_ID       VARCHAR2(500)        NOT NULL,
   ACTION_DATE  TIMESTAMP            NOT NULL,
   ATTR_NAME    VARCHAR2(250)        NOT NULL,
   ATTR_VALUE   VARCHAR2(2000)       NOT NULL
 );

DECLARE
  FAKE_VAR   NUMBER;
  system_ID  NUMBER := 0;
BEGIN
  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP, 'OBJ_HISTORY_ATTRS', null,
     'Содержит пользовательские атрибуты истории изменений системных объектов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP,
     'ru.effts.ine.core.history.ActionAttribute', system_ID,
     'Интерфейс пользовательского атрибута истории изменений объектов системы');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'N', system_ID, 'Cистемный номер элемента (системного объекта)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'ENT_ID', system_ID, 'Идентификатор сущности (либо системный номер, либо составной номер)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'ACTION_DATE', system_ID, 'Дата активного действия');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'ATTR_NAME', system_ID, 'Имя пользовательского атрибута');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP,
     'ATTR_VALUE', system_ID, 'Значение пользовательского атрибута');

  COMMIT;

END;
/
