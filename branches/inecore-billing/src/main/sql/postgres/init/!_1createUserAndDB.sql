SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity
WHERE pg_stat_activity.datname = 'ine'
  AND pid <> pg_backend_pid();


\set INE_USR ine
DROP OWNED BY :INE_USR;
DROP DATABASE IF EXISTS :INE_USR;
DROP ROLE IF EXISTS :INE_USR;

CREATE ROLE :INE_USR WITH LOGIN PASSWORD 'ine';
CREATE DATABASE :INE_USR WITH OWNER = ine ENCODING = 'UTF8' TABLESPACE = pg_default LC_COLLATE = 'ru_RU.UTF-8'
                LC_CTYPE = 'ru_RU.UTF-8' CONNECTION LIMIT = -1;


DO $$
BEGIN
RAISE NOTICE 'Type password: ine';
END$$;


\c :INE_USR :INE_USR

