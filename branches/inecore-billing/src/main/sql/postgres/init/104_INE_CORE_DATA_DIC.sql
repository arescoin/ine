/*
"$Id: 104_INE_CORE_DATA_DIC.sql 3867 2014-10-16 07:34:01Z DGomon $"
*/

-- Создаем словари для системы...
DO $$

DECLARE
  VFD  TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD  TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

-- Системные словари

-- [1] Словарь типов словарей по ролям
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES ( 1, 'DICTIONARY_ROLE_TYPES', 1, 1, 2, NULL, 'Перечесление типов словарей (По роли в системе)', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(1, 1, 1, 'Системный', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(1, 2, 1, 'Административный', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(1, 3, 1, 'Пользовательский', null, VFD, VTD);

-- Термины на англицком
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(1, 1, 2, 'System', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(1, 2, 2, 'Administrative', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(1, 3, 2, 'User', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [2] Словарь типов словарей по типам идентификаторов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (2, 'DICTIONARY_NUMBER_TYPES', 1, 1, 2, NULL,
        'Перечесление типов словарей (По типу присвоения идентификаторов словарным статьям)', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(2, 1, 1, 'Простой', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(2, 2, 1, 'Двоичный', null, VFD, VTD);

-- Термины на англицком
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(2, 1, 2, 'Simple', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(2, 2, 2, 'Binary', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [3] Словарь типов словарей по типам идентификаторов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (3, 'DICTIONARY_FILL_TYPES', 1, 1, 2, NULL, 'Перечесление типов словарей (По уровню локализации)', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(3, 1, 1, 'Полное', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(3, 2, 1, 'Частичное', null, VFD, VTD);

-- Термины на англицком
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(3, 1, 2, 'Fully', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(3, 2, 2, 'Рartially', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [4] Словарь типов системных объектов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (4, 'SYS_OBJ_TYPES', 1, 1, 2, NULL, 'Перечесление типов системных объектов', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(4, 1, 1, 'Таблица', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(4, 2, 1, 'Столбец', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(4, 3, 1, 'Последовательность', null, VFD, VTD);

-- Термины на англицком
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(4, 1, 2, 'Table', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(4, 2, 2, 'Column', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(4, 3, 2, 'Sequence', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [5] Типы констант
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (5, 'CONSTANT_TYPES', 1, 1, 2, NULL, 'Перечесление типов констант', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(5, 1, 1, 'Простая', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(5, 2, 1, 'Параметризованная', null, VFD, VTD);

-- Термины на англицком
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(5, 1, 2, 'Simple', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(5, 2, 2, 'Parameterized', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [6] Типы данных для таблиц настраиваемых атрибутов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (6, 'DATA_TYPES', 1, 1, 2, NULL,
        'Типы данных для таблиц настраиваемых атрибутов (соответствуют enum''у ru.effts.ine.core.structure.DataType)', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 1, 1, 'java.lang.String', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 2, 1, 'java.lang.Long', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 3, 1, 'java.math.BigDecimal', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 4, 1, 'java.util.Date', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 5, 1, 'java.lang.Boolean', null, VFD, VTD);

-- Термины на англицком
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 1, 2, 'java.lang.String', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 2, 2, 'java.lang.Long', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 3, 2, 'java.math.BigDecimal', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 4, 2, 'java.util.Date', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(6, 5, 2, 'java.lang.Boolean', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [7] CRUDType core
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (7, 'CRUD_TYPE', 1, 2, 2, NULL, 'Типы CRUD-операций', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(7, 1, 1, 'Закрыто', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(7, 2, 1, 'Создание', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(7, 4, 1, 'Модификация', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(7, 8, 1, 'Удаление', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- словари для адресов
-- [8] страны
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (8, 'COUNTRIES', 1, 1, 1, NULL, 'Страны', VFD, VTD);

--данные словарной статьи для стран (на русском языке)
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(8, 643, 1, 'Российская Федерация', null, VFD, VTD);

--данные словарной статьи для стран (на английском языке)
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(8, 643, 2, 'Russian Federation', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [9] регионы OSS/J
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (9, 'ADDRESS_REGIONS', 2, 1, 2, 8, 'Регионы', VFD, VTD);

--данные словарной статьи для регионов
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 80, 1, 'Агинский Бурятский Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  1, 1, 'Адыгея Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  4, 1, 'Алтай Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 22, 1, 'Алтайский Край', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 28, 1, 'Амурская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 29, 1, 'Архангельская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 30, 1, 'Астраханская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 99, 1, 'Байконур Город', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  2, 1, 'Башкортостан Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 31, 1, 'Белгородская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 32, 1, 'Брянская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  3, 1, 'Бурятия Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 33, 1, 'Владимирская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 34, 1, 'Волгоградская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 35, 1, 'Вологодская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 36, 1, 'Воронежская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  5, 1, 'Дагестан Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 79, 1, 'Еврейская Автономная область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 37, 1, 'Ивановская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  6, 1, 'Ингушетия Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 38, 1, 'Иркутская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  7, 1, 'Кабардино-Балкарская Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 39, 1, 'Калининградская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  8, 1, 'Калмыкия Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 40, 1, 'Калужская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 41, 1, 'Камчатская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9,  9, 1, 'Карачаево-Черкесская Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 10, 1, 'Карелия Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 42, 1, 'Кемеровская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 43, 1, 'Кировская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 11, 1, 'Коми Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 82, 1, 'Корякский Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 44, 1, 'Костромская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 23, 1, 'Краснодарский Край', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 24, 1, 'Красноярский Край', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 45, 1, 'Курганская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 46, 1, 'Курская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 47, 1, 'Ленинградская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 48, 1, 'Липецкая Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 49, 1, 'Магаданская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 12, 1, 'Марий Эл Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 13, 1, 'Мордовия Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 77, 1, 'Москва Город', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 50, 1, 'Московская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 51, 1, 'Мурманская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 83, 1, 'Ненецкий Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 52, 1, 'Нижегородская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 53, 1, 'Новгородская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 54, 1, 'Новосибирская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 55, 1, 'Омская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 56, 1, 'Оренбургская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 57, 1, 'Орловская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 58, 1, 'Пензенская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 59, 1, 'Пермский Край', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 25, 1, 'Приморский Край', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 60, 1, 'Псковская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 61, 1, 'Ростовская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 62, 1, 'Рязанская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 63, 1, 'Самарская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 78, 1, 'Санкт-Петербург Город', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 64, 1, 'Саратовская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 14, 1, 'Саха /Якутия/ Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 65, 1, 'Сахалинская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 66, 1, 'Свердловская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 15, 1, 'Северная Осетия - Алания Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 67, 1, 'Смоленская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 26, 1, 'Ставропольский Край', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 84, 1, 'Таймырский (Долгано-Ненецкий) Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 68, 1, 'Тамбовская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 16, 1, 'Татарстан Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 69, 1, 'Тверская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 70, 1, 'Томская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 71, 1, 'Тульская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 17, 1, 'Тыва Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 72, 1, 'Тюменская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 18, 1, 'Удмуртская Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 73, 1, 'Ульяновская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 85, 1, 'Усть-Ордынский Бурятский Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 27, 1, 'Хабаровский Край', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 19, 1, 'Хакасия Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 86, 1, 'Ханты-Мансийский Автономный округ - Югра Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 74, 1, 'Челябинская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 20, 1, 'Чеченская Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 75, 1, 'Читинская Область', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 21, 1, 'Чувашская Республика', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 87, 1, 'Чукотский Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 89, 1, 'Ямало-Ненецкий Автономный округ', 643, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(9, 76, 1, 'Ярославская Область', 643, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [10] 'типы' населенных пунктов OSS/J
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (10, 'ADDRESS_LOCALITY_SUFFIXES', 2, 1, 2, NULL, 'Типы населенных пунктов', VFD, VTD);

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(10, 1, 1, 'город', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(10, 2, 1, 'село', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(10, 3, 1, 'деревня', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(10, 4, 1, 'поселок городского типа', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(10, 5, 1, 'поселок сельского типа', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [11] населенные пункты OSS/J
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (11, 'ADDRESS_LOCALITY_NAMES', 2, 1, 2, 9, 'Названия населенных пунктов', VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [12] 'типы' улиц OSS/J
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (12, 'ADDRESS_STREET_SUFFIXES', 2, 1, 2, NULL, 'Типы улиц', VFD, VTD);

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 1, 1, 'улица', null,VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 2, 1, 'проспект', null,VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 3, 1, 'проезд', null,VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 4, 1, 'тупик', null,VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 5, 1, 'переулок', null,VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 6, 1, 'бульвар', null,VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 7, 1, 'набережная', null,VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 8, 1, 'шоссе', null,VFD, VTD);

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(12, 1, 2, 'street', null,VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [13] улицы OSS/J
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (13, 'ADDRESS_STREET_NAMES', 2, 1, 2, 11, 'Названия улиц', VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [14] почтовые индексы OSS/J
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (14, 'ADDRESS_POSTCODES', 2, 1, 2, 13, 'Почтовые индексы', VFD, VTD);
-----------------------------------------------------------------------------------------------------------------------

-- [15] Тип организации
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (15, 'COMPANY_TYPES', 2, 1, 2, null, 'Тип организации. Полное название.', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(15, 1, 1, 'Общество с ограниченной ответственностью', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(15, 2, 1, 'Открытое акционерное общество', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(15, 3, 1, 'Закрытое акционерное общество', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(15, 4, 1, 'Предприниматель без образования юридического лица', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [16] Аббревиатура типа организации
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (16, 'COMPANY_TYPES_ABBR', 2, 1, 2, 15, 'Тип организации. Аббревиатура.', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(16, 1, 1, 'ООО', 1, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(16, 2, 1, 'ОАО', 2, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(16, 3, 1, 'ЗАО', 3, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(16, 4, 1, 'ПБОЮЛ', 4, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [17] Тип собственности организации
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (17, 'COMPANY_PROPERTY_TYPES', 2, 1, 2, null, 'Тип собственности организации.', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(17, 1, 1, 'Частная', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(17, 2, 1, 'Государственная', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [18] Предпочтительное обращение к персоне
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (18, 'PERSON_FAMILY_NAME_PREFIX', 2, 1, 2, null, 'Предпочтительное обращение к персоне. Полное название.', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(18, 1, 1, 'Товарищ', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [19] Предпочтительное обращение к персоне, сокращенное
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (19, 'PERSON_FAMILY_NAME_PREFIX_ABBR', 2, 1, 2, 18, 'Предпочтительное обращение к персоне. Сокращённое название.', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(19, 1, 1, 'Тов.', 1, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [20] серьезности тревог / alarm sevrity
-- глобальная спецификация - X.733
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (20, 'ALARM_PERCEIVED_SEVERITY', 2, 1, 2, NULL, 'Критичность тревог', VFD, VTD);

  -- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(20, 1, 1, 'Устраненная тревога (Cleared)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(20, 2, 1, 'Неопределенная тревога (Indeterminate)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(20, 3, 1, 'Критическая тревога (Critical)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(20, 4, 1, 'Значительная тревога (Major)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(20, 5, 1, 'Незначительная тревога (Minor)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(20, 6, 1, 'Предупреждение (Warning)', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [21] типы тревог / alarm type
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (21, 'ALARM_TYPE', 2, 1, 2, NULL, 'Типы тревог.', VFD, VTD);

  -- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(21, 1, 1, 'Тревога связи (Communications alarm)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(21, 2, 1, 'Тревога качества сервиса (Quality of service alarm)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(21, 3, 1, 'Тревога об ошибке обработки (Processing error alarm)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(21, 4, 1, 'Тревога оборудования (Equipment alarm)', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(21, 5, 1, 'Тревога окружения (Environmental alarm)', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [22] возможные причины тревог / alarm probable cause
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (22, 'ALARM_PROBABLE_CAUSE', 2, 1, 2, 21, 'Возможные причины тревог.', VFD, VTD);

  -- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(22, 1, 1, 'Потеря сигнала (Loss of signal)', 1, VFD, VTD);
/* нужно ли вносить как стандартные, если да забить в словарь, если нет убрать словарную статью
Loss of frame
Framing error
Local node transmission error
Remote node transmission error
Call establishment error
Degraded signal
Communications subsystem failure
Communications protocol error
LAN error
DTE-DCE interface error
*/
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(22, 2, 1, 'Превышено время ответа (Response time excessive)', 2, VFD, VTD);
/* нужно ли вносить как стандартные, если да забить в словарь, если нет убрать словарную статью
Queue size exceeded
Bandwidth reduced
Retransmission rate excessive
Threshold crossed
Performance degraded
Congestion
Resource at or nearing capacity
*/
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(22, 3, 1, 'Проблема ёмкости запоминающего устройства (Storage capacity problem)', 3, VFD, VTD);
/* нужно ли вносить как стандартные, если да забить в словарь, если нет убрать словарную статью
Version mismatch
Corrupt data
CPU cycles limit exceeded
Software error
Software program error
Software program abnormally terminated
File error
Out of memory
Underlying resource unavailable
Application subsystem failure
Configuration or customization error
*/
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(22, 4, 1, 'Проблема с энергопитанием (Power problem)', 4, VFD, VTD);
/* нужно ли вносить как стандартные, если да забить в словарь, если нет убрать словарную статью
Timing problem
Processor problem
Dataset or modem error
Multiplexer problem
Receiver failure
Transmitter failure
Receive failure
Transmit failure
Output device error
Input device error
I/O device error
Equipment malfunction
Adapter error
*/
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(22, 5, 1, 'Неприемлимая температура (Temperature unacceptable)', 5, VFD, VTD);
/* нужно ли вносить как стандартные, если да забить в словарь, если нет убрать словарную статью
Humidity unacceptable
Heating/ventilation/cooling system problem
Fire detected
Flood detected
Toxic leak detected
Leak detected
Pressure unacceptable
Excessive vibration
Material supply exhausted
Pump failure
Enclosure door open
*/

-----------------------------------------------------------------------------------------------------------------------

-- [23] статусы сервисов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (23, 'SERVICE_STATUS', 2, 1, 2, NULL, 'Статусы сервисов', VFD, VTD);

  -- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 1, 1, 'созданный', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 2, 1, 'активный', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 3, 1, 'блокированный', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 4, 1, 'архивный', NULL, VFD, VTD);

  -- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 1, 2, 'created', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 2, 2, 'active', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 3, 2, 'blocked', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(23, 4, 2, 'archive', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [24] DeviceType SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (24, 'DEVICE_TYPE', 2, 1, 2, NULL, 'Перечисление типов оборудования', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 1, 1, 'ATM-DSLAM', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 2, 1, 'IP-DSLAM', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 3, 1, 'BRAS', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 4, 1, 'Router', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 5, 1, 'ATM-SW', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 6, 1, 'ETH-SW', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 7, 1, 'ETH-HUB', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 8, 1, 'IP-DSLAM-LITE', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 9, 1, 'OLT', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 10, 1, 'IP-DSLAM-LITE-TV', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 11, 1, 'Unknown', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 1, 2, 'ATM-DSLAM', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 2, 2, 'IP-DSLAM', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 3, 2, 'BRAS', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 4, 2, 'Router', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 5, 2, 'ATM-SW', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 6, 2, 'ETH-SW', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 7, 2, 'ETH-HUB', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 8, 2, 'IP-DSLAM-LITE', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 9, 2, 'OLT', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 10, 2, 'IP-DSLAM-LITE-TV', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(24, 11, 2, 'Unknown', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [25] CPEType SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (25, 'CPE_TYPE', 2, 1, 2, NULL, 'Перечисление типов CPE', VFD, VTD);

-- Термины на русском
--WARNING данное значение 'ONU' hardcoded в бизнес процедуре №149... так как только для этого типа нужно устанавливать
--режим работы
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(25, 1, 1, 'Unknown', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(25, 2, 1, 'ONU', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(25, 3, 1, 'ADSL modem', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(25, 1, 2, 'Unknown', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(25, 2, 2, 'ONU', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(25, 3, 2, 'ADSL modem', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [26] CPEMode SZT
--WARNING значения данных словарных статей hardcoded в бизнес процедуре №149... так как присылается на вход
-- БП именно значение режима работы
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (26, 'CPE_MODE', 2, 1, 2, NULL, 'Перечисление режимов CPE', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(26, 1, 1, 'BRIDGE', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(26, 2, 1, 'ROUTE', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(26, 3, 1, 'UNKNOWN', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(26, 1, 2, 'BRIDGE', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(26, 2, 2, 'ROUTE', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(26, 3, 2, 'UNKNOWN', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [27] EXTServiceStatus SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (27, 'EXT_SERVICE_STATUS', 2, 1, 2, NULL, 'Внешние статусы сервисов', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 1, 1, 'BILL-SUSPEND', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 2, 1, 'BILL-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 3, 1, 'SECURITY-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 4, 1, 'SPAM-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 5, 1, 'VIRUS-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 6, 1, 'CHANGING', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 7, 1, 'Unknown', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 1, 2, 'BILL-SUSPEND', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 2, 2, 'BILL-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 3, 2, 'SECURITY-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 4, 2, 'SPAM-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 5, 2, 'VIRUS-BLOCK', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 6, 2, 'CHANGING', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(27, 7, 2, 'Unknown', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [28] SubscriberType SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (28, 'SUBSCRIBER_TYPE', 2, 1, 2, NULL, 'Типы абонентов', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(28, 1, 1, 'юридическое лицо', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(28, 2, 1, 'физическое лицо', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(28, 1, 2, 'corporate', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(28, 2, 2, 'individual', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [30] Technology SZT
-- WARNING! то что словарь имеет именно такие значения hardcoded в БП№162
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (30, 'TECHNOLOGY', 2, 1, 2, NULL, 'Типы абонентов', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(30, 1, 1, 'ADSL', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(30, 2, 1, 'PON', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(30, 3, 1, 'ETHERNET', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(30, 1, 2, 'ADSL', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(30, 2, 2, 'PON', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(30, 3, 2, 'ETHERNET', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [31] UVOType SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (31, 'UVO_TYPE', 2, 1, 2, NULL, 'Типы UVOT (охранка)', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 1, 1, 'Тип 1 А', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 2, 1, 'Тип 2 Б', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 3, 1, 'Тип 2 А', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 4, 1, 'Тип 3', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 5, 1, 'Тип 4', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 6, 1, 'Отсутствует', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 1, 2, 'Typ 1 А', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 2, 2, 'Typ 2 Б', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 3, 2, 'Typ 2 А', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 4, 2, 'Typ 3', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 5, 2, 'Typ 4', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(31, 6, 2, 'None', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [33] Классы ролей участников ассоциаций OSS/J объектов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (33, 'OSSJ_ASSOCIATION_CLASSES', 1, 1, 1, NULL, 'Классы ролей участников ассоциаций OSS/J объектов', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 1, 1, 'Роли объектов ''Сервис'' (ServiceValue)', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 2, 1, 'Роли объектов ''Продукт'' (ProductValue)', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 3, 1, 'Роли объектов ''Оборудование'' (ResourceValue::Device)', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 4, 1, 'Роли объектов ''IP-адрес'' (ResourceValue::IPAddress)', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 5, 1, 'Роли объектов ''Порт'' (ResourceValue::Port)', null, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 1, 2, 'Roles for ServiceValue', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 2, 2, 'Roles for ProductValue', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 3, 2, 'Roles for ResourceValue::Device', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 4, 2, 'Roles for ResourceValue::IPAddress', null, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES (33, 5, 2, 'Roles for ResourceValue::Port', null, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [34] Роли участников ассоциаций OSS/J объектов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (34, 'OSSJ_ASSOCIATION_ROLES', 1, 1, 1, 33, 'Роли участников ассоциаций OSS/J объектов', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 1, 1, 'Поставщик услуги', 1, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 2, 1, 'Потребитель услуги', 1, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 3, 1, 'Родитель', 2, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 4, 1, 'Наследник', 2, VFD, VTD);
-- WARNING!!!! следующие 2 роли (5-ая и 6-ая) захаркожены в CommonUtils проекта pluginCommon, менять только совместно
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 5, 1, 'Управляемое оборудование', 3, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 6, 1, 'Управляющий адрес', 4, VFD, VTD);
-- WARNING!!!! следующие 2 роли (7-ая и 8-ая) захаркожены в CommonUtils проекта pluginCommon, менять только совместно
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 7, 1, 'Оборудование', 3, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 8, 1, 'Привязанный к оборудованию IP-адрес', 4, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 9, 1, 'IP-адрес для маршрутизации (Routing)', 4, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 10, 1, 'Выделенный IP-адрес', 4, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 11, 1, 'Управляющий порт (aggregationPort)', 5, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 12, 1, 'Управляемый порт (servicePort)', 5, VFD, VTD);

-- Термины на англицком
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 1, 2, 'Service provider', 1, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 2, 2, 'Service consumer', 1, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 3, 2, 'Parent', 2, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 4, 2, 'Child', 2, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 5, 2, 'Manageable equipment', 3, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 6, 2, 'Managing address', 4, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 7, 2, 'Pool client', 3, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 8, 2, 'Pool element', 4, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 9, 2, 'Routing address', 4, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 10, 2, 'Subnet addreess', 4, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 11, 2, 'Aggregation Port', 5, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(34, 12, 2, 'Service Port', 5, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [35] Типы VPN'ов
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (35, 'VPN_TYPE', 1, 2, 1, NULL, 'Типы VPN', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(35, 1, 1, 'L2', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(35, 2, 1, 'L3', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [36] Типы VPN топологии
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (36, 'VPN_TOPOLOGY', 1, 2, 1, NULL, 'Топология VPN', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(36, 1, 1, 'Звезд', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(36, 2, 1, 'Каждый с каждым', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [37] Типы VPN топологии
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (37, 'QOS_TYPE', 1, 2, 1, NULL, 'Типы QOS', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(37, 1, 1, 'Time critical', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(37, 2, 1, 'Best effort', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [38] DeviceType SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (38, 'EXT_DEVICE_TYPE', 2, 1, 2, NULL, 'Перечисление дополнительных типов оборудования', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(38, 1, 1, 'DSLAM Lite', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(38, 1, 2, 'DSLAM Lite', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [39] Branch SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (39, 'BRANCH', 2, 1, 2, NULL, 'Филиалы СЗТ', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39,   1, 1, 'СЗТ', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 401, 1, 'Калининград', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 811, 1, 'Псков', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 812, 1, 'Санкт-Петербург', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 813, 1, 'Ленинградская область', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 814, 1, 'Карелия', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 815, 1, 'Мурманск', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 816, 1, 'Новгород', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 817, 1, 'Вологда', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 818, 1, 'Архангельск', NULL, VFD, VTD);
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(39, 821, 1, 'Коми', NULL, VFD, VTD);

-----------------------------------------------------------------------------------------------------------------------

-- [40] Security Service Type SZT
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (40, 'SECUR_SERVICE_TYPE', 2, 1, 2, NULL, 'Типов услуги SECURService', VFD, VTD);

-- Термины на русском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(40, 1, 1, 'Unknown', NULL, VFD, VTD);

-- Термины на английском
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(40, 1, 2, 'Unknown', NULL, VFD, VTD);

------------------------------------------------------------------------------------------------------------------------

-- [41] Типы кратности линков (LINKS - ru.effts.ine.core.links.Link)
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (41, 'MULTIPLICITY', 1, 1, 2, NULL, 'Типы кратности линков', VFD, VTD);

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(41, 1, 1, '1:1',   null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(41, 2, 1, '1:N',   null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(41, 3, 1, 'N:N',   null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

------------------------------------------------------------------------------------------------------------------------

-- [42] Сервисбандлы
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (42, 'SERVICE_BUNDLE', 2, 1, 2, NULL, 'Сервисбандлы (бизнес-услуги)', VFD, VTD);

------------------------------------------------------------------------------------------------------------------------

-- [43] Прайслист
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (43, 'PRICE_LIST', 2, 1, 2, NULL, 'Перечисление прайслистов', VFD, VTD);

------------------------------------------------------------------------------------------------------------------------

-- [44] Тип клиента
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (44, 'CLIENT_TYPE', 2, 1, 2, NULL, 'Тип клиента', VFD, VTD);

------------------------------------------------------------------------------------------------------------------------
-- [45] Статус услуг
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (45, 'SERVICE_STATUS', 2, 1, 2, NULL, 'Статус услуги', VFD, VTD);

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(45, 1, 1, 'Активна',             null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(45, 2, 1, 'Блокировка',          null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(45, 3, 1, 'Блокировка клиентом', null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

------------------------------------------------------------------------------------------------------------------------
-- [46] Статус услуг
INSERT INTO DIC (N, DIC_NAME, DIC_TYPE, CODE_POLICY, FULLY_FILL, UP, DSC, FD, TD)
VALUES (46, 'SERVICE_TYPE', 2, 1, 2, NULL, 'Тип услуги', VFD, VTD);

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(46, 1, 1, 'Разовая',             null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(46, 2, 1, 'Периодическая',       null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

------------------------------------------------------------------------------------------------------------------------

END$$;