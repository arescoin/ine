/*
"$Id: 002_INE_CORE_SYS_pcg.sql 3771 2013-10-30 15:20:05Z DGomon $"
*/

CREATE SCHEMA ine_core_sys AUTHORIZATION :INE_USR;
GRANT ALL ON SCHEMA ine_core_sys TO :INE_USR;

-- Возвращает системное FromDate
CREATE OR REPLACE FUNCTION INE_CORE_SYS.get_System_FromDate() RETURNS timestamp with time zone AS $func$
  DECLARE
    fd DATE := to_date('01-01-2010 00:00:01', 'DD-MM-YYYY HH24:MI:SS');
  BEGIN
    RETURN fd;
  END;
$func$ LANGUAGE plpgsql;

  -- Возвращает системное ToDate
CREATE OR REPLACE FUNCTION INE_CORE_SYS.get_System_ToDate() RETURNS timestamp with time zone AS $func$
  DECLARE
  td DATE := to_date('01-01-3010 00:00:01', 'DD-MM-YYYY HH24:MI:SS');
  BEGIN
    RETURN td;
  END;
$func$ LANGUAGE plpgsql;

  -- Обрезает переданный timestamp - убирает timezone
CREATE OR REPLACE FUNCTION INE_CORE_SYS.trunc_timestamp(in_date timestamp with time zone) RETURNS timestamp with time zone AS $$
  BEGIN
    RETURN(to_timestamp(to_char(in_date, 'DD.MM.YYYY HH24:MI:SS.FF3'), 'DD.MM.YYYY HH24:MI:SS.FF3'));
  END;
$$ LANGUAGE plpgsql;

  -- Возвращает текущую дату, получаемую через INE_CORE_SYS.getCurrentDate и пропущенную через функцию trunc этого пакета
CREATE OR REPLACE FUNCTION INE_CORE_SYS.getCurrentDate() RETURNS timestamp with time zone AS $$
  BEGIN
    RETURN INE_CORE_SYS.trunc_timestamp(NOW());
  END;
$$ LANGUAGE plpgsql;


