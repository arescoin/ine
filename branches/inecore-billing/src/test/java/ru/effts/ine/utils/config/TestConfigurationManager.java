/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.config;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: TestConfigurationManager.java 3385 2011-12-01 11:42:45Z dgomon $"
 */
public class TestConfigurationManager {

    @Test
    public void testApplicationName() {
        String appName = ConfigurationManager.getManager().getRootConfig().getApplicationName();
        Assert.assertNotNull("Application name is null", appName);
    }

    @Test
    public void testGetConfigFor() {
        SystemConfig systemConfig = ConfigurationManager.getManager().getConfigFor("logging");
        Assert.assertNotNull("Logging configuration not found", systemConfig);

    }

}
