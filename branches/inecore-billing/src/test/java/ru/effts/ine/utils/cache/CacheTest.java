/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.cache;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.CoreException;

/**
 * @author Ivan Kulkov
 * @version 1.0
 * @SVNVersion "$Id: CacheTest.java 3385 2011-12-01 11:42:45Z dgomon $"
 */
public class CacheTest {
    private final String TEST_CACHE = "test_cache";
    private final String TEST_KEY = "test_key";
    private final String TEST_VALUE = "test_value";

    static {
        System.setProperty("ru.effts.ine.utils.cache.CacheAccessRiMSImpl.STANDALONE", "true");
    }

    @Test
    public void testGet() {
        try {
            Assert.assertEquals("Wrong cached value", TEST_VALUE,
                    Cache.get(TEST_CACHE, TEST_KEY, new Producer() {
                        @Override
                        public Object get(Object key) throws CoreException {
                            return TEST_VALUE;
                        }
                    }));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPut() {
        try {
            Cache.put(TEST_CACHE, TEST_KEY, TEST_VALUE);

            Assert.assertNotNull("Failed to cache value, null value get.",
                    Cache.get(TEST_CACHE, TEST_KEY, new Producer.NullProducer()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testRemove() {
        try {
            Cache.put(TEST_CACHE, TEST_KEY, TEST_VALUE);

            Cache.remove(TEST_CACHE, TEST_KEY);

            Assert.assertNull("Failed to remove cached value.",
                    Cache.get(TEST_CACHE, TEST_KEY, new Producer.NullProducer()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInvalidateOne() {
        try {
            Cache.put(TEST_CACHE, TEST_KEY + "1", TEST_VALUE + "1");
            Assert.assertNotNull("Failed to cache value, null value get.",
                    Cache.get(TEST_CACHE, TEST_KEY + "1", new Producer.NullProducer()));

            Cache.put(TEST_CACHE, TEST_KEY + "2", TEST_VALUE + "2");
            Assert.assertNotNull("Failed to cache value, null value get.",
                    Cache.get(TEST_CACHE, TEST_KEY + "2", new Producer.NullProducer()));

            Cache.invalidate(TEST_CACHE, TEST_KEY + "1");
            Assert.assertNull("Failed to invalidate cached value.",
                    Cache.get(TEST_CACHE, TEST_KEY + "1", new Producer.NullProducer()));
            Assert.assertNotNull("Failed to cache value, null value get.",
                    Cache.get(TEST_CACHE, TEST_KEY + "2", new Producer.NullProducer()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInvalidateAll() {
        try {
            Cache.put(TEST_CACHE, TEST_KEY + "1", TEST_VALUE + "1");
            Assert.assertNotNull("Failed to cache value, null value get.",
                    Cache.get(TEST_CACHE, TEST_KEY + "1", new Producer.NullProducer()));

            Cache.put(TEST_CACHE, TEST_KEY + "2", TEST_VALUE + "2");
            Assert.assertNotNull("Failed to cache value, null value get.",
                    Cache.get(TEST_CACHE, TEST_KEY + "2", new Producer.NullProducer()));

            Cache.invalidate(TEST_CACHE);
            Assert.assertNull("Failed to invalidate cached values.",
                    Cache.get(TEST_CACHE, TEST_KEY + "1", new Producer.NullProducer()));
            Assert.assertNull("Failed to invalidate cached values.",
                    Cache.get(TEST_CACHE, TEST_KEY + "2", new Producer.NullProducer()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
