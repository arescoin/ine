/**********************************************************************************************************************
 * Copyright (c) 2011, MaximaTelecom, effts. Все права защищены.                                                      *
 **********************************************************************************************************************/

package ru.effts.ine.utils.logging;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.utils.config.ConfigurationManager;
import ru.effts.ine.utils.config.PropertiesBasedLoggingConfig;

import java.util.logging.Handler;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: LoggerTest.java 3385 2011-12-01 11:42:45Z dgomon $"
 */
public class LoggerTest {

    @Test
    public void test() {
        ConfigurationManager configurationManager = ConfigurationManager.getManager();
        configurationManager.getRootConfig();

        PropertiesBasedLoggingConfig propertiesBasedLoggingConfig =
                (PropertiesBasedLoggingConfig) configurationManager.getConfigFor("logging");

        String packName = propertiesBasedLoggingConfig.getCustomProperties().getProperty("test1.name");

        Handler[] handlers = Logger.getLogger(packName).getHandlers();

        boolean formatterOk = false;

        if (handlers != null) {
            System.out.println("BLIA VAASHE!!!!!!!");
            for (Handler handler : handlers) {
                System.out.println("Handler:" + handler +
                        "; Formatter: " + handler.getFormatter().getClass().getName());
                if (CustomFormatter.class.getName().equals(handler.getFormatter().getClass().getName())) {
                    formatterOk = true;
                    break;
                }
            }
        }else{
            System.out.println("Handlers not found...");
        }

        Assert.assertTrue("Wrong formatter", formatterOk);

    }

}
