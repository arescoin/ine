package ru.effts.ine.dbe.da.billing.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountAccess.java 3839 1970-01-01 00:00:00Z DGomon $"
 */
public interface AccountAccess<T extends Account> extends VersionableAccess<T> {
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
