package ru.effts.ine.dbe.da.billing.accounting;

import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsAccess.java 3839 2014-06-16 07:47:35Z DGomon $"
 */
public interface CompanyDetailsAccess<T extends CompanyDetails> extends VersionableAccess<T> {
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
