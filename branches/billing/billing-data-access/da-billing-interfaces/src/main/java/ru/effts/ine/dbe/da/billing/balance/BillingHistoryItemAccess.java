package ru.effts.ine.dbe.da.billing.balance;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.IdentifiableAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemAccess.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public interface BillingHistoryItemAccess<T extends BillingHistoryItem> extends IdentifiableAccess<T> {

    @Override
    Collection<T> getAllObjects() throws CoreException;

    Collection<T> getHistoryByAccId(BigDecimal accId, Date fd, Date td) throws CoreException;

    Collection<T> getHistoryForSrvByAccId(BigDecimal accId, BigDecimal srvId, Date fd, Date td) throws CoreException;
}
