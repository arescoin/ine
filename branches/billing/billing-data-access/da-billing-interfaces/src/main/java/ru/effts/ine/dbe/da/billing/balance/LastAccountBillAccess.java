package ru.effts.ine.dbe.da.billing.balance;

import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillAccess.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
public interface LastAccountBillAccess<T extends LastAccountBill> extends VersionableAccess<T> {

    @Override
    Collection<T> getAllObjects() throws CoreException;
}
