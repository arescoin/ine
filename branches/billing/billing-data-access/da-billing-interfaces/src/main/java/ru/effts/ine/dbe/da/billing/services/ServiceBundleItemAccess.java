package ru.effts.ine.dbe.da.billing.services;

import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemAccess.java 3839 2014-06-16 07:47:35Z DGomon $"
 */
public interface ServiceBundleItemAccess<T extends ServiceBundleItem> extends VersionableAccess<T> {
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
