package ru.effts.ine.dbe.da.billing.services;

import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceAccess.java 3838 2014-06-11 13:23:40Z DGomon $"
 */
public interface ServicePriceAccess<T extends ServicePrice> extends VersionableAccess<T> {
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
