package ru.effts.ine.dbe.da.billing.accounting;

import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsAccess.java 3839 2014-06-16 07:47:35Z DGomon $"
 */
public interface BankingDetailsAccess<T extends BankingDetails> extends VersionableAccess<T> {
    @Override
    Collection<T> getAllObjects() throws CoreException;
}
