package ru.effts.ine.dbe.da.billing.balance;

import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemAccess.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public interface BalanceItemAccess<T extends BalanceItem> extends VersionableAccess<T> {
    @Override
    Collection<T> getAllObjects() throws CoreException;

    Collection<T> getOldVersionsByAccId(BigDecimal accId, Date fd, Date td) throws GenericSystemException;
}
