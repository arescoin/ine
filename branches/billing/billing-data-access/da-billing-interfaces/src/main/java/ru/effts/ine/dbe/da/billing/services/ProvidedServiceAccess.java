package ru.effts.ine.dbe.da.billing.services;

import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.dbe.da.core.VersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceAccess.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public interface ProvidedServiceAccess<T extends ProvidedService> extends VersionableAccess<T> {
    @Override
    Collection<T> getAllObjects() throws CoreException;

    Collection<T> getHistoryServicesForAccAtDate(BigDecimal accId, Date date) throws CoreException;
}
