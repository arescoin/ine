/*
"$Id: 001_SERVICE_SPEC.sql 3853 2014-07-04 13:42:57Z DGomon $"
*/


CREATE TABLE SERVICE_SPEC
(
  N              NUMERIC                  NOT NULL
, SS_NAME        CHARACTER VARYING(2000)
, DSC            CHARACTER VARYING(2000)
, FD             TIMESTAMP WITH TIME ZONE NOT NULL
, TD             TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_SRV_SPEC_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_SRV_SPEC_FDTD   ON SERVICE_SPEC(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_SRV_SPEC INCREMENT BY 1 START WITH 1000;


begin;
-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'SERVICE_SPEC', null, 'Описание услуги/тех.возможности, на основе которого строится бизнес-услуга');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.services.ServiceSpec', system_ID, 'Интерфейс ServiceSpec');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'n', system_ID, 'Cистемный номер описания услуги');
  INSERT INTO LINKS (N, LEFT_OBJ, RIGHT_OBJ, TYP, FD, TD) VALUES (nextval('SEQ_LINKS'), ''||FAKE_VAR, ''||FAKE_VAR, 2, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SS_NAME', system_ID, 'Наименование описания услуги');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию услуги');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_SRV_SPEC', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('SERVICE_SPEC');


END$$;

commit;
