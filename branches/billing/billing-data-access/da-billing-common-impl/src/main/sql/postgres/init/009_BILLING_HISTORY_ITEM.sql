/*
"$Id: 009_BILLING_HISTORY_ITEM.sql 3873 2014-10-23 13:19:10Z DGomon $"
*/


CREATE TABLE BILLING_HISTORY_ITEM
(
  N                  NUMERIC                   NOT NULL
, ACC_ID             NUMERIC                   NOT NULL -- есть паттерн
, SERVICE_ID         NUMERIC                   NOT NULL -- есть паттерн
, BILL_DATE          TIMESTAMP WITH TIME ZONE  NOT NULL
, DSC                CHARACTER VARYING(2000)
, CONSTRAINT   UK_BILL_HSTR_ITM_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_BILL_HSTR_ITM_ACC   ON BILLING_HISTORY_ITEM(ACC_ID);
CREATE INDEX IND_BILL_HSTR_ITM_SRV   ON BILLING_HISTORY_ITEM(SERVICE_ID);
CREATE INDEX IND_BILL_HSTR_ITM_DAT   ON BILLING_HISTORY_ITEM(BILL_DATE);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_BILL_HSTR_ITM INCREMENT BY 1 START WITH 1000;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN


  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'BILLING_HISTORY_ITEM', null, 'Записи биллинга по ЛС и услуге');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.balance.BillingHistoryItem', system_ID, 'Интерфейс BillingHistoryItem в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер BillingHistoryItem в биллинге');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ACC_ID', system_ID, 'Ссылка на ЛС (Account)',
    ''||INE_CORE_SYS_OBJ.get_Column_N('BILL_ACCOUNT', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SERVICE_ID', system_ID, 'Ссылка на ID придоставленного сервиса',
  ''||INE_CORE_SYS_OBJ.get_Column_N('PROVIDED_SERVICE', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'BILL_DATE', system_ID, 'Дата проведения биллинга');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию ProvidedService');


  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_BILL_HSTR_ITM', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
--   FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('BILLING_HISTORY_ITEM');

END$$;
