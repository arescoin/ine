/*
"$Id: 005_SERVICE_BUNDLE.sql 3867 2014-10-16 07:34:01Z DGomon $"
*/


CREATE TABLE SERVICE_BUNDLE
(
  N                  NUMERIC                  NOT NULL
, BUNDLE_ID          NUMERIC                  NOT NULL -- есть паттерн (словарь 42 "SERVICE_BUNDLE")
, SPEC_ID            NUMERIC                  NOT NULL -- есть паттерн (SERVICE_SPEC#N)
, TYP                NUMERIC                  NOT NULL -- есть паттерн (SERVICE_SPEC#N)
, SRV_NAME           CHARACTER VARYING(2000)
, DSC                CHARACTER VARYING(2000)
, FD                 TIMESTAMP WITH TIME ZONE NOT NULL
, TD                 TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_SRV_BNDL_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_SRV_BNDL_FDTD   ON SERVICE_BUNDLE(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_SRV_BNDL INCREMENT BY 1 START WITH 1000;


-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'SERVICE_BUNDLE', null, 'Описание ServiceBundleItem');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.services.ServiceBundleItem', system_ID, 'Интерфейс ServiceBundleItem в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер ServiceBundleItem в биллинге');

  INSERT INTO LINKS (N, LEFT_OBJ, RIGHT_OBJ, TYP, FD, TD) VALUES (nextval('SEQ_LINKS'),
    ''||FAKE_VAR, ''||FAKE_VAR, 2, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'BUNDLE_ID', system_ID, 'Ссылка на словарную статью',
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':42]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'TYP', system_ID, 'Ссылка на словарную статью',
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':46]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SPEC_ID', system_ID, 'Ссылка на описание сервиса',
    ''||INE_CORE_SYS_OBJ.get_Column_N('SERVICE_SPEC', 'n'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SRV_NAME', system_ID, 'Наименование сервиса');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию ServiceBundleItem');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_SRV_BNDL', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('SERVICE_BUNDLE');



END$$;
