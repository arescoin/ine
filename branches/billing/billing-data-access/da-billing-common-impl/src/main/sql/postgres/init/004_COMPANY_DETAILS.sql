/*
"$Id: 004_COMPANY_DETAILS.sql 3853 2014-07-04 13:42:57Z DGomon $"
*/


CREATE TABLE COMPANY_DETAILS
(
  N                  NUMERIC                  NOT NULL
, ACC_ID             NUMERIC                  NOT NULL -- есть паттерн
, COMPANY_ID         NUMERIC                  NOT NULL -- есть паттерн
, PHONE_N            CHARACTER VARYING(2000)
, LEGAL_ADDRESS      CHARACTER VARYING(2000)
, PHYSICAL_ADDRESS   CHARACTER VARYING(2000)
, DSC                CHARACTER VARYING(2000)
, FD                 TIMESTAMP WITH TIME ZONE NOT NULL
, TD                 TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_CPN_DTLS_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_CPN_DTLS_FDTD   ON COMPANY_DETAILS(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_CPN_DTLS INCREMENT BY 1 START WITH 1000;


-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'COMPANY_DETAILS', null, 'Описание компании');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.accounting.CompanyDetails', system_ID, 'Интерфейс CompanyDetails в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер CompanyDetails в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ACC_ID', system_ID, 'Идентификатор Account, ссылка на клиента',
    ''||INE_CORE_SYS_OBJ.get_Column_N('BILL_ACCOUNT', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'COMPANY_ID', system_ID, 'Идентификатор компании',
    ''||INE_CORE_SYS_OBJ.get_Column_N('COMPANY', 'n'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PHONE_N', system_ID, 'Контактный телефон');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'LEGAL_ADDRESS', system_ID, 'Юридический адрес');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PHYSICAL_ADDRESS', system_ID, 'Физический адрес');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию компании');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_CPN_DTLS', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('COMPANY_DETAILS');

END$$;
