/*
"$Id: 002_BILL_ACCOUNT.sql 3853 2014-07-04 13:42:57Z DGomon $"
*/


CREATE TABLE BILL_ACCOUNT
(
  N              NUMERIC                  NOT NULL
, PL_ID          NUMERIC                  NOT NULL -- есть паттерн
, CL_TYP         NUMERIC                  NOT NULL -- есть паттерн
, CL_ID          NUMERIC                  NOT NULL
, DISCOUNT       NUMERIC                  NOT NULL
, DSC            CHARACTER VARYING(2000)
, FD             TIMESTAMP WITH TIME ZONE NOT NULL
, TD             TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_BACC_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_BACC_FDTD   ON BILL_ACCOUNT(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_BACC INCREMENT BY 1 START WITH 1000;


begin;
-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'BILL_ACCOUNT', null, 'Описание лицевого счета в биллинге');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.accounting.Account', system_ID, 'Интерфейс Account в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер Account в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PL_ID', system_ID, 'Идентификатор прайс-листа для данного Account',
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':43]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CL_TYP', system_ID, 'Тип клиента (Словарь 44)',
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':44]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'CL_ID', system_ID, 'Идентификатор клиента');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DISCOUNT', system_ID, 'Индивидуальный коэффициент скидки для данного Account');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию Account');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_BACC', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('BILL_ACCOUNT');

END$$;

commit;