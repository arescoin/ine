/*
"$Id: 003_BANKING_DETAILS.sql 3861 2014-09-23 14:33:49Z DGomon $"
*/


CREATE TABLE BANKING_DETAILS
(
  N               NUMERIC                  NOT NULL
, ACC_ID          NUMERIC                  NOT NULL -- есть паттерн
, INN             CHARACTER VARYING(2000)  NOT NULL
, KPP             CHARACTER VARYING(2000)  NOT NULL
, BANK_NAME       CHARACTER VARYING(2000)  NOT NULL
, BIK             CHARACTER VARYING(2000)  NOT NULL
, BANK_ACC_N      CHARACTER VARYING(2000)  NOT NULL
, COR_BANK_ACC_N  CHARACTER VARYING(2000)  NOT NULL
, DSC             CHARACTER VARYING(2000)
, FD              TIMESTAMP WITH TIME ZONE NOT NULL
, TD              TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_BNK_DTLS_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_BNK_DTLS_FDTD   ON BANKING_DETAILS(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_BNK_DTLS INCREMENT BY 1 START WITH 1000;


-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'BANKING_DETAILS', null, 'Описание банковских реквизитов');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.accounting.BankingDetails', system_ID, 'Интерфейс BankingDetails в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер BankingDetails в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ACC_ID', system_ID, 'Идентификатор Account, ссылка на клиента',
    ''||INE_CORE_SYS_OBJ.get_Column_N('BILL_ACCOUNT', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'INN', system_ID, 'Значение ИНН');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'KPP', system_ID, 'Значение КПП');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'BANK_NAME', system_ID, 'Наименование банка');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'BIK', system_ID, 'Значение БИК');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'BANK_ACC_N', system_ID, 'Номер банковского лицевого счета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию банковских реквизитов');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_BNK_DTLS', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('BANKING_DETAILS');

END$$;
