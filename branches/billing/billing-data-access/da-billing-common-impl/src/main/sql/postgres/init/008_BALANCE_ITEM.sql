/*
"$Id: 008_BALANCE_ITEM.sql 3864 2014-10-13 09:16:38Z DGomon $"
*/


CREATE TABLE BALANCE_ITEM
(
  N                  NUMERIC                  NOT NULL
, ACC_ID             NUMERIC                  NOT NULL -- есть паттерн
, ADDITION           NUMERIC                  NOT NULL
, AMOUNT             NUMERIC                  NOT NULL
, BALANCE            NUMERIC                  NOT NULL
, REASON_TYP         NUMERIC                           -- есть паттерн
, REASON_ID          NUMERIC
, OP_DATE            TIMESTAMP WITH TIME ZONE NOT NULL
, FD                 TIMESTAMP WITH TIME ZONE NOT NULL
, TD                 TIMESTAMP WITH TIME ZONE NOT NULL
, DSC                CHARACTER VARYING(2000)
, CONSTRAINT   UK_BALANCE_ITEM_ID       UNIQUE (N)
, CONSTRAINT   UK_BALANCE_ITEM_ACC_ID   UNIQUE (ACC_ID)
);

-- Индексы на таблы
CREATE INDEX IND_BALANCE_ITEM_DATE   ON BALANCE_ITEM(OP_DATE);
CREATE INDEX IND_BALANCE_ITEM_FDTD   ON BALANCE_ITEM(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_BALANCE_ITEM INCREMENT BY 1 START WITH 1000;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN


  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'BALANCE_ITEM', null, 'Записи изменения состояния лицевого счета');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.balance.BalanceItem', system_ID, 'Интерфейс BalanceItem в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер BalanceItem в биллинге');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ACC_ID', system_ID, 'Ссылка на ЛС (Account)',
    ''||INE_CORE_SYS_OBJ.get_Column_N('BILL_ACCOUNT', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ADDITION', system_ID, 'Тип изменения (пополнение/списание) : 1/0');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'AMOUNT', system_ID, 'Размер изменения');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'BALANCE', system_ID, 'Сам баланс ЛС');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'REASON_TYP', system_ID, 'Ссылка на тип объекта системы, если он инициировал изменение баланса',
      ''||INE_CORE_SYS_OBJ.get_Column_N('SYS_OBJ', 'n'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'REASON_ID', system_ID, 'Ссылка на объект системы, если он инициировал изменение баланса');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'OP_DATE', system_ID, 'Дата изменения баланса');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию ProvidedService');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_BALANCE_ITEM', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('BALANCE_ITEM');

END$$;
