/*
 * $Id: 999_TEST_DATA.sql 3878 2014-11-10 15:20:51Z DGomon $
 */
/*
 * Создание различных тестовых данных
 * НЕ ДЛЯ ВЫКЛАДЫВАНИЯ НА PRODUCTION!
*/

/* ================================================================================================================== */

-- 013_INE_CORE_ENTITIES.sql
DO $$

DECLARE
  VFD  TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD  TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (1, 'DSC1',  VFD, VTD, 'Test Custom Company', 1, 1);

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (2, 'DSC2',  VFD, VTD, 'Test State Company', 2, 2);

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (3, 'DSC2',  VFD, VTD, 'Customer Company', 2, 2);

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (4, 'DSC4',  VFD, VTD, 'Customer Company4', 2, 2);

  insert into COMPANY (N, DSC, FD, TD, NAME, TYPE, PROPERTY_TYPE)
    values (5, 'DSC5',  VFD, VTD, 'Customer Company4', 2, 2);


  INSERT INTO PERSON (N, DSC, FD, TD, FAMILY_NAME, NAME, MIDDLE_NAME, ALIAS,
                      FAMILY_NAME_PREFIX, FAMILY_GENERATION, ADDRESS_FORM)
  VALUES (1, 'DSC1',  VFD, VTD, 'Family1', 'Name1', 'Middle1', 'Alias1', 1, 'Generation1', 'AddressForm1');

  INSERT INTO PERSON (N, DSC, FD, TD, FAMILY_NAME, NAME, MIDDLE_NAME, ALIAS,
                      FAMILY_NAME_PREFIX, FAMILY_GENERATION, ADDRESS_FORM)
  VALUES (2, 'DSC2',  VFD, VTD, 'Family2', 'Name2', 'Middle2', 'Alias2', 1, 'Generation2', 'AddressForm2');

 INSERT INTO PERSON (N, DSC, FD, TD, FAMILY_NAME, NAME, MIDDLE_NAME, ALIAS,
                      FAMILY_NAME_PREFIX, FAMILY_GENERATION, ADDRESS_FORM)
  VALUES (3, 'DSC3',  VFD, VTD, 'Family3', 'Name3', 'Middle3', 'Alias3', 1, 'Generation3', 'AddressForm3');

 INSERT INTO PERSON (N, DSC, FD, TD, FAMILY_NAME, NAME, MIDDLE_NAME, ALIAS,
                      FAMILY_NAME_PREFIX, FAMILY_GENERATION, ADDRESS_FORM)
  VALUES (4, 'DSC4',  VFD, VTD, 'Family4', 'Name4', 'Middle4', 'Alias4', 1, 'Generation4', 'AddressForm4');

 INSERT INTO PERSON (N, DSC, FD, TD, FAMILY_NAME, NAME, MIDDLE_NAME, ALIAS,
                      FAMILY_NAME_PREFIX, FAMILY_GENERATION, ADDRESS_FORM)
  VALUES (5, 'DSC5',  VFD, VTD, 'Family5', 'Name5', 'Middle5', 'Alias5', 1, 'Generation5', 'AddressForm5');

END$$;


/* ================================================================================================================== */

-- 104_INE_CORE_DATA_DIC.sql
DO $$
DECLARE
  LINK_ID NUMERIC;

  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  -- [11] населенные пункты OSS/J
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(11, 1, 1, 'Москва', 77, VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(11, 2, 1, 'Санкт-Петербург', 78, VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(11, 3, 1, 'Выборг', 47, VFD, VTD);

  -- [13] улицы OSS/J
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(13, 1, 1, 'Новокосинская', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(13, 2, 1, '1905 года', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(13, 3, 1, '40 лет Комсомола', 3,VFD, VTD);

  -- [14] почтовые индексы OSS/J
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(14, 1, 1, '111672', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(14, 2, 1, '123100', 1,VFD, VTD);
  INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(14, 3, 1, '188800', 3,VFD, VTD);


  --связь названий и типов населенных пунктов
  SELECT N INTO LINK_ID FROM LINKS WHERE TYP = 1
  AND LEFT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':10]'
                       ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code')
  AND RIGHT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':11]'
                        ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code');

  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(1,LINK_ID,1,1,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(2,LINK_ID,1,2,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(3,LINK_ID,1,3,VFD,VTD);

  --связь названий и типов улиц
  SELECT N INTO LINK_ID FROM LINKS WHERE TYP = 2
  AND LEFT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':12]'
                       ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code')
  AND RIGHT_OBJ LIKE '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':13]'
                        ||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code');

  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(4,LINK_ID,1,1,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(5,LINK_ID,1,2,VFD,VTD);
  insert into LINK_DATA (N,LINK_N,LEFT_ID,RIGHT_ID,FD,TD) values(6,LINK_ID,7,3,VFD,VTD);


END$$;


/* ================================================================================================================== */

-- 105_INE_CORE_DATA_USPTS.sql
DO $$
DECLARE

  ROLE_SYS_ADMIN NUMERIC ;
  ROLE_BRANCH_OFFICER NUMERIC ;

  NUM NUMERIC ;

  VFD TIMESTAMP WITH TIME ZONE  := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE  := INE_CORE_SYS.GET_SYSTEM_TODATE();

BEGIN

  SELECT N INTO ROLE_SYS_ADMIN FROM ROLES WHERE ROLE_NAME='SYS_ADMIN';
  SELECT N INTO ROLE_BRANCH_OFFICER FROM ROLES WHERE ROLE_NAME='BRANCH_OFFICER';

  -- Пользователи
  -- Зверек root1, пароль 'root'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'root1', '63a9f0ea7bb98050796b649e85481845', 1, 'Root User 1', VFD, VTD);
  -- Роль: Админ
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_SYS_ADMIN, 1, 'DSC', VFD, VTD);

  -- Зверек никто 1, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody2', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 2', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 2, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody3', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 3', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 3, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody4', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 4', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 4, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody5', '6e854442cd2a940c9e95941dce4ad598', 1, 'Test User 5', VFD, VTD);
  -- Роль: Работник
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

  -- Зверек никто 5 неактивный, пароль 'nobody'
  NUM := nextval('SEQ_SYS_USERS');
  INSERT INTO SYS_USERS (N, SYS_LOGIN, MD5_HASH, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, 'nobody6', '6e854442cd2a940c9e95941dce4ad598', 0, 'Test User 6', VFD, VTD);
  -- Роль: Работник (блокированый)
  INSERT INTO USER_ROLES (USER_N, ROLE_N, IS_ACTIVE, DSC, FD, TD)
  VALUES (NUM, ROLE_BRANCH_OFFICER, 1, 'DSC', VFD, VTD);

END$$;


/* ================================================================================================================== */

-- 106_INE_CORE_DATA_CONSTANTS.sql
DO $$
DECLARE
  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  -- константы для тестов
  INSERT INTO CONSTANTS (N, CONST_NAME, DEF_VAL, IS_NULLABLE, IS_MODYFABLE, FD, TD)
  VALUES(1, 'TST_CONSTANT', '1', 0, 1, VFD, VTD);

  INSERT INTO CONSTANTS_P (N, CONST_NAME, PARAM_MASK, DEF_VAL, IS_NULLABLE, IS_MODYFABLE, FD, TD)
  VALUES(2, 'TST_PARAMS_CONSTANT',
  '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':5]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code'),
  '1', 0, 1, VFD, VTD);


END$$;


/* ================================================================================================================== */

-- 107_INE_CORE_DATA_FUNCSW.sql
INSERT INTO FUNC_SWITCH (N, FUNC_NAME, DSC) VALUES(1, 'TEST_SWITCH', 'Тестовй переключатель. Не менять!');


/* ================================================================================================================== */


-- 111_INE_CORE_DATA_CUSTOM_ATTRS_DSC.sql
DO $$
DECLARE
  SYS_VAR NUMERIC;
  VFD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_FROMDATE();
  VTD TIMESTAMP WITH TIME ZONE := INE_CORE_SYS.GET_SYSTEM_TODATE();
BEGIN

  SYS_VAR := INE_CORE_SYS_OBJ.get_Table_N('TEST_TABLE');

  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    1, 'Test value #1', 'Test data #1. Do not edit or remove !!!', VFD, VTD);
  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    2, 'Test value #2', 'Test data #2. Do not edit or remove !!!', VFD, VTD);
  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    3, 'Test value #3', 'Test data #3. Do not edit or remove !!!', VFD, VTD);
  insert into TEST_TABLE (N, VALUE, DSC, FD, TD) values (
    4, 'Test value #4', 'Test data #4. Do not edit or remove !!!', VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    1, SYS_VAR, 1, 'testStringAttribute', 1, 'String attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_STRING (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 1, 'Test attribute value #1', VFD, VTD);
  insert into TEST_TABLE_STRING (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 1, 'Test attribute value #2', VFD, VTD);
  insert into TEST_TABLE_STRING (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 1, 'Test attribute value #3', VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    2, SYS_VAR, 2, 'testLongAttribute', 1, 'Long attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_LONG (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 2, 1, VFD, VTD);
  insert into TEST_TABLE_LONG (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 2, 2, VFD, VTD);
  insert into TEST_TABLE_LONG (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 2, 3, VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    3, SYS_VAR, 3, 'testBigDecimalAttribute', 1, 'BigDecimal attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_BIGDECIMAL (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 3, 1.1, VFD, VTD);
  insert into TEST_TABLE_BIGDECIMAL (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 3, 2.2, VFD, VTD);
  insert into TEST_TABLE_BIGDECIMAL (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 3, 3.3, VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    4, SYS_VAR, 4, 'testDateAttribute', 1, 'Date attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_DATE (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 4, CURRENT_TIMESTAMP + time '20:00', VFD, VTD);
  insert into TEST_TABLE_DATE (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 4, CURRENT_TIMESTAMP + time '20:00', VFD, VTD);
  insert into TEST_TABLE_DATE (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (3, 4, CURRENT_TIMESTAMP + time '20:00', VFD, VTD);


  insert into CUSTOM_ATTRS_DSC (N, SYS_OBJ_N, ATTR_TYP, ATTR_NAME, IS_REQUIRED, DSC, FD, TD) values (
    5, SYS_VAR, 5, 'testBooleanAttribute', 0, 'Boolean attribute for tests. Do not edit or remove !!!', VFD, VTD);

  insert into TEST_TABLE_BOOLEAN (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (1, 5, 1, VFD, VTD);
  insert into TEST_TABLE_BOOLEAN (ENT_N, ATTR_N, ATTR_VAL, FD, TD) values (2, 5, 0, VFD, VTD);


END$$;


-- test data: DIC_DATA
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 1, 1, 'SB-1',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 2, 1, 'SB-2',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 3, 1, 'SB-3',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 4, 1, 'SB-4',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 5, 1, 'SB-5',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 6, 1, 'SB-6',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 7, 1, 'SB-7',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(42, 8, 1, 'SB-8',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(43, 1, 1, 'PL-1',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(43, 2, 1, 'PL-2',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(43, 3, 1, 'PL-3',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(43, 4, 1, 'PL-4',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(44, 1, 1, 'Юрик',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(44, 2, 1, 'Физик', null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
INSERT INTO DIC_DATA(DIC_N, CODE, LANG, TERM, UP, FD, TD) VALUES(44, 3, 1, 'Иное',  null, INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());



-- test data: SERVICE_SPEC
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (1, 'Test SS-1', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (2, 'Test SS-2', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (3, 'Test SS-3', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (4, 'Test SS-4', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (5, 'Test SS-5', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (6, 'Test SS-6', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (7, 'Test SS-7', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_SPEC (N, SS_NAME, DSC, FD, TD) VALUES (8, 'Test SS-8', 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());


-- test data: SERVICE_BUNDLE
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (1, 1, 1, 1, 'Test srv', 'DSC1', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (2, 1, 2, 1, 'Test srv2', 'DSC2', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (3, 1, 3, 1, 'Test srv3', 'DSC3', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (4, 1, 2, 1, 'Test srv4', 'DSC4', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (5, 1, 3, 1, 'Test srv5', 'DSC5', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (6, 1, 4, 1, 'Test srv6', 'DSC6', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (7, 1, 5, 1, 'Test srv7', 'DSC7', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (8, 1, 6, 1, 'Test srv8', 'DSC8', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_BUNDLE (N, BUNDLE_ID, SPEC_ID, TYP, SRV_NAME, DSC, FD, TD)
                   values (9, 1, 7, 1, 'Test srv9', 'DSC9', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());


-- test data: SERVICE_PRICE
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (1,  1, 1, 30,  1,   'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (5,  1, 2, 90,  3,   'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (6,  1, 3, 120, 4,   'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (7,  1, 4, 3,   .1,  'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (8,  1, 5, 6,   .2,  'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (9,  1, 6, 9,   .3,  'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (10, 1, 7, 33,  1.1, 'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (11, 1, 8, 66,  2.2, 'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (12, 1, 9, 99,  3.3, 'Test Data',  INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (2,  1, 2, 15,  .5,  'Test Data2', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (3,  2, 1,  9,  .3,  'Test Data3', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into SERVICE_PRICE (N, PL_ID, SRV_BNDL_ID, PRICE, DAY_PRICE, DSC, FD, TD)
                   values (4,  2, 2,  21, .7,  'Test Data4', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());



-- test data: BILL_ACCOUNT
insert into BILL_ACCOUNT (N, PL_ID, CL_TYP, CL_ID, DISCOUNT, DSC, FD, TD)
                   VALUES(1, 1, 1, 1, .9, 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into BILL_ACCOUNT (N, PL_ID, CL_TYP, CL_ID, DISCOUNT, DSC, FD, TD)
                   VALUES(2, 2, 2, 1, 1, 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into BILL_ACCOUNT (N, PL_ID, CL_TYP, CL_ID, DISCOUNT, DSC, FD, TD)
                   VALUES(3, 2, 1, 2, 1, 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into BILL_ACCOUNT (N, PL_ID, CL_TYP, CL_ID, DISCOUNT, DSC, FD, TD)
                   VALUES(4, 1, 2, 2, 1, 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into BILL_ACCOUNT (N, PL_ID, CL_TYP, CL_ID, DISCOUNT, DSC, FD, TD)
                   VALUES(5, 3, 1, 3, 1, 'Test data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());


-- test data: BANKING_DETAILS
insert into BANKING_DETAILS (N, ACC_ID, INN, KPP, BANK_NAME, BIK, BANK_ACC_N, COR_BANK_ACC_N, DSC, FD, TD)
                   values (1, 1, '2342342341', '2342342341', 'asdasdasd1', '234345361', '2342342341', '2342342342', 'Test data1', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into BANKING_DETAILS (N, ACC_ID, INN, KPP, BANK_NAME, BIK, BANK_ACC_N, COR_BANK_ACC_N, DSC, FD, TD)
                   values (2, 2, '2342342342', '2342342342', 'asdasdasd2', '234345362', '2342342342', '2342342343', 'Test data2', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into BANKING_DETAILS (N, ACC_ID, INN, KPP, BANK_NAME, BIK, BANK_ACC_N, COR_BANK_ACC_N, DSC, FD, TD)
                   values (3, 3, '2342342343', '2342342343', 'asdasdasd3', '234345363', '2342342343', '2342342344', 'Test data3', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());

-- test data: COMPANY_DETAILS
insert into COMPANY_DETAILS (N, ACC_ID, COMPANY_ID, PHONE_N, LEGAL_ADDRESS, PHYSICAL_ADDRESS, DSC, FD, TD)
                   values (1, 1, 1, '94599944211', 'l-address 1', 'p-address', 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into COMPANY_DETAILS (N, ACC_ID, COMPANY_ID, PHONE_N, LEGAL_ADDRESS, PHYSICAL_ADDRESS, DSC, FD, TD)
                   values (2, 3, 2, '94599944211', 'l-address 1', 'p-address', 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into COMPANY_DETAILS (N, ACC_ID, COMPANY_ID, PHONE_N, LEGAL_ADDRESS, PHYSICAL_ADDRESS, DSC, FD, TD)
                   values (3, 5, 3, '94599944211', 'l-address 1', 'p-address', 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());


-- test data: PROVIDED_SERVICE
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (1, 1, 1, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (2, 1, 2, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (3, 1, 3, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (4, 1, 4, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (5, 1, 5, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (6, 1, 6, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (7, 1, 7, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (8, 1, 8, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());
insert into PROVIDED_SERVICE(N, ACC_ID, SRV_BNDL_ID, PARENT_ID, SRV_NAME, STATUS, DSC, FD, TD)
                   values (9, 1, 9, null, 'Test Srv Name', 1, 'Test Data', INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE());


-- test data: PROVIDED_SERVICE
insert into BALANCE_ITEM(N, ACC_ID, ADDITION, AMOUNT, BALANCE, REASON_TYP, REASON_ID, OP_DATE, FD, TD, DSC)
                   values (1, 1, 1, 9999, 9999, null, null, now(), INE_CORE_SYS.GET_SYSTEM_FROMDATE(), INE_CORE_SYS.GET_SYSTEM_TODATE(), 'Test balance item');

