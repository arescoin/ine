/*
"$Id: 010_LAST_ACCOUNT_BILL.sql 3885 2014-11-28 08:47:28Z DGomon $"
*/


CREATE TABLE LAST_ACCOUNT_BILL
(
  N                  NUMERIC                  NOT NULL -- есть паттерн
, BILL_DATE          DATE                     NOT NULL -- есть паттерн
, DSC                CHARACTER VARYING(2000)
, FD                 TIMESTAMP WITH TIME ZONE NOT NULL
, TD                 TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_LST_ACC_BLL_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_LST_ACC_BLL_FDTD   ON LAST_ACCOUNT_BILL(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_LST_ACC_BLL INCREMENT BY 1 START WITH 1000;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN


  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'LAST_ACCOUNT_BILL', null, 'Запись о полном биле по ЛС за сутки');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.balance.LastAccountBill', system_ID, 'Интерфейс LastAccountBill в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер LastAccountBill в биллинге, всегда равен ЛС',
      ''||INE_CORE_SYS_OBJ.get_Column_N('BILL_ACCOUNT', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'BILL_DATE', system_ID, 'Дата билла (не текущая, а дата пользования)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию LastAccountBill');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_LST_ACC_BLL', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('LAST_ACCOUNT_BILL');

END$$;
