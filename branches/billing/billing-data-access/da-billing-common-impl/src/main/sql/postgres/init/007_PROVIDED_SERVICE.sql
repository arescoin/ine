/*
"$Id: 007_PROVIDED_SERVICE.sql 3865 2014-10-13 13:33:58Z DGomon $"
*/


CREATE TABLE PROVIDED_SERVICE
(
  N                  NUMERIC                  NOT NULL
, ACC_ID             NUMERIC                  NOT NULL -- есть паттерн
, SRV_BNDL_ID        NUMERIC                  NOT NULL -- есть паттерн
, PARENT_ID          NUMERIC                           -- есть паттерн
, SRV_NAME           CHARACTER VARYING(2000)
, STATUS             NUMERIC                  NOT NULL -- есть паттерн
, DSC                CHARACTER VARYING(2000)
, FD                 TIMESTAMP WITH TIME ZONE NOT NULL
, TD                 TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_PRVD_SRV_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_PRVD_SRV_FDTD   ON PROVIDED_SERVICE(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_PRVD_SRV INCREMENT BY 1 START WITH 1000;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN


  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'PROVIDED_SERVICE', null, 'Описание предоставленного сервиса (ProvidedService)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.services.ProvidedService', system_ID, 'Интерфейс ProvidedService в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер ProvidedService в биллинге');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'ACC_ID', system_ID, 'Ссылка на ЛС (Account)',
    ''||INE_CORE_SYS_OBJ.get_Column_N('BILL_ACCOUNT', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SRV_BNDL_ID', system_ID, 'Ссылка на описание сервиса (элемент бандла)',
    ''||INE_CORE_SYS_OBJ.get_Column_N('SERVICE_BUNDLE', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PARENT_ID', system_ID, 'Ссылка верхнюю услугу ProvidedService',
    ''||INE_CORE_SYS_OBJ.get_Column_N('PROVIDED_SERVICE', 'N'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SRV_NAME', system_ID, 'Наименование инстанса');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'STATUS', system_ID, 'Ссылка на словарную статью со статусом услуги',
      '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':45]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code'));

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию ProvidedService');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_PRVD_SRV', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('PROVIDED_SERVICE');

END$$;
