/*
"$Id: 006_SERVICE_PRICE.sql 3865 2014-10-13 13:33:58Z DGomon $"
*/


CREATE TABLE SERVICE_PRICE
(
  N                  NUMERIC                  NOT NULL
, PL_ID              NUMERIC                  NOT NULL -- есть паттерн
, SRV_BNDL_ID        NUMERIC                  NOT NULL -- есть паттерн
, PRICE              NUMERIC                  NOT NULL
, DAY_PRICE          NUMERIC(5,2)             NOT NULL
, DSC                CHARACTER VARYING(2000)
, FD                 TIMESTAMP WITH TIME ZONE NOT NULL
, TD                 TIMESTAMP WITH TIME ZONE NOT NULL
, CONSTRAINT   UK_SRV_PRC_ID       UNIQUE (N)
);

-- Индексы на таблы
CREATE INDEX IND_SRV_PRCL_FDTD   ON SERVICE_PRICE(FD, TD);

-- Теперь создаем сиквенс для идентификаторов
CREATE SEQUENCE SEQ_SRV_PRC INCREMENT BY 1 START WITH 1000;

-- Теперь заносим все в SYS_OBJ
DO $$
DECLARE
  FAKE_VAR   NUMERIC;
  system_ID  NUMERIC := 0;
  DD_N       NUMERIC;
  DD_CODE    NUMERIC;
  DIC_N      NUMERIC;
BEGIN

  system_ID := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.TABLE_TYP(), 'SERVICE_PRICE', null, 'Описание ценника (ServicePrice)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.INTERFACE_TYP(), 'ru.effts.ine.billing.services.ServicePrice', system_ID, 'Интерфейс ServicePrice в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'N', system_ID, 'Cистемный номер ServicePrice в биллинге');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PL_ID', system_ID, 'Ссылка на прайс-лист (словарь)',
    '['||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'dic_n')||':43]'||INE_CORE_SYS_OBJ.get_Column_N('DIC_DATA', 'code'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'SRV_BNDL_ID', system_ID, 'Ссылка на описание сервиса (элемент бандла)',
    ''||INE_CORE_SYS_OBJ.get_Column_N('SERVICE_BUNDLE', 'N'));
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'PRICE', system_ID, 'Цена на услугу');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DAY_PRICE', system_ID, 'Цена на услугу в сутки(зи расчета 30 дней в мес.)');

  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.COLUMN_TYP(), 'DSC', system_ID, 'Коментарий к описанию ServicePrice');
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_FDTD(system_ID);

  -- Добавляем сиквенс
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj(INE_CORE_SYS_OBJ.SEQUENCE_TYP(),'SEQ_SRV_PRC', system_ID, 'Последовательность для идентификатора');


  -- Добавляем историю
  FAKE_VAR := INE_CORE_SYS_OBJ.add_Sys_Obj_HSTR('SERVICE_PRICE');

END$$;
