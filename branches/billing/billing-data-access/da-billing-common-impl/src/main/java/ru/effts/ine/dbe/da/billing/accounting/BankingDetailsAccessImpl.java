package ru.effts.ine.dbe.da.billing.accounting;

import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsAccessImpl.java 3863 2014-10-07 14:09:32Z DGomon $"
 */
public class BankingDetailsAccessImpl<T extends BankingDetails>
        extends CommonAccessImpl<T> implements BankingDetailsAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "BANKING_DETAILS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ACC_ID = "acc_id";
    public static final String COLUMN_INN = "inn";
    public static final String COLUMN_KPP = "kpp";
    public static final String COLUMN_BANK_NAME = "bank_name";
    public static final String COLUMN_BIK = "bik";
    public static final String COLUMN_BANK_ACC_N = "bank_acc_n";
    public static final String COLUMN_COR_BANK_ACC_N = "cor_bank_acc_n";

    /** Запрос на выборку */
    public static final String BANKING_DETAILS_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ACC_ID +
            ", " + TABLE_PREF + COLUMN_INN +
            ", " + TABLE_PREF + COLUMN_KPP +
            ", " + TABLE_PREF + COLUMN_BANK_NAME +
            ", " + TABLE_PREF + COLUMN_BIK +
            ", " + TABLE_PREF + COLUMN_BANK_ACC_N +
            ", " + TABLE_PREF + COLUMN_COR_BANK_ACC_N +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.accounting.BankingDetails} */
    public static final String CACHE_REGION_BANKING_DETAILS = BankingDetailsAccess.class.getName();


    static {
        setInstanceFieldsMapping(BankingDetailsAccessImpl.class, new HashMap<String, String>());
        registerAHelpers(BankingDetailsAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void registerAHelpers(Class<? extends BankingDetailsAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<>();
        map.put(BankingDetails.ACCOUNT_ID, COLUMN_ACC_ID);
        map.put(BankingDetails.INN, COLUMN_INN);
        map.put(BankingDetails.KPP, COLUMN_KPP);
        map.put(BankingDetails.BANK_NAME, COLUMN_BANK_NAME);
        map.put(BankingDetails.BIK, COLUMN_BIK);
        map.put(BankingDetails.BANK_ACCOUNT_NUMBER, COLUMN_BANK_ACC_N);
        map.put(BankingDetails.COR_ACCOUNT_NUMBER, COLUMN_COR_BANK_ACC_N);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_BANKING_DETAILS;
    }

    @Override
    protected String getCommonSelect() {
        return BANKING_DETAILS_SELECT;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return BankingDetails.class;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(BankingDetails.class);

        this.setVersionableFields(resultSet, result);

        result.setAccountId(this.getId(resultSet, COLUMN_ACC_ID));
        result.setInn(this.getString(resultSet, COLUMN_INN));
        result.setKpp(this.getString(resultSet, COLUMN_KPP));
        result.setBankName(this.getString(resultSet, COLUMN_BANK_NAME));
        result.setBik(this.getString(resultSet, COLUMN_BIK));
        result.setBankAccountNumber(this.getString(resultSet, COLUMN_BANK_ACC_N));
        result.setCorAccountNumber(this.getString(resultSet, COLUMN_COR_BANK_ACC_N));

        return result;
    }

}
