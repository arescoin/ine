package ru.effts.ine.dbe.da.billing.balance;

import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemAccessImpl.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public class BalanceItemAccessImpl<T extends BalanceItem> extends CommonAccessImpl<T> implements BalanceItemAccess<T> {

    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "BALANCE_ITEM";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ACCOUNT_ID = "acc_id";
    public static final String COLUMN_ADDITION = "addition";
    public static final String COLUMN_AMOUNT = "amount";
    public static final String COLUMN_BALANCE = "balance";
    public static final String COLUMN_REASON_TYPE = "reason_typ";
    public static final String COLUMN_REASON_ID = "reason_id";
    public static final String COLUMN_DATE = "op_date";

    /** Запрос на выборку */
    public static final String ALL_DATA_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ACCOUNT_ID +
            ", " + TABLE_PREF + COLUMN_ADDITION +
            ", " + TABLE_PREF + COLUMN_AMOUNT +
            ", " + TABLE_PREF + COLUMN_BALANCE +
            ", " + TABLE_PREF + COLUMN_REASON_TYPE +
            ", " + TABLE_PREF + COLUMN_REASON_ID +
            ", " + TABLE_PREF + COLUMN_DATE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.balance.BalanceItem} */
    public static final String CACHE_REGION = BalanceItem.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(BalanceItem.ACCOUNT_ID, COLUMN_ACCOUNT_ID);
        map.put(BalanceItem.ADDITION, COLUMN_ADDITION);
        map.put(BalanceItem.AMOUNT, COLUMN_AMOUNT);
        map.put(BalanceItem.BALANCE, COLUMN_BALANCE);
        map.put(BalanceItem.REASON_TYPE, COLUMN_REASON_TYPE);
        map.put(BalanceItem.REASON_ID, COLUMN_REASON_ID);
        map.put(BalanceItem.DATE, COLUMN_DATE);
        setInstanceFieldsMapping(BalanceItemAccessImpl.class, map);

        registerIdentifiableHelpers(BalanceItemAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION;
    }


    @Override
    protected String getCommonSelect() {
        return ALL_DATA_SELECT;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(BalanceItem.class);

        this.setVersionableFields(resultSet, value);

        this.setIdentifiableFields(resultSet, value);
        value.setAccountId(this.getBigDecimal(resultSet, COLUMN_ACCOUNT_ID));
        value.setAddition(this.getBoolean(resultSet, COLUMN_ADDITION));
        value.setAmount(this.getBigDecimal(resultSet, COLUMN_AMOUNT));
        value.setBalance(this.getBigDecimal(resultSet, COLUMN_BALANCE));
        value.setReasonType(this.getBigDecimal(resultSet, COLUMN_REASON_TYPE));
        value.setReasonId(this.getBigDecimal(resultSet, COLUMN_REASON_ID));
        value.setDate(this.getDate(resultSet, COLUMN_DATE));

        return value;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return BalanceItem.class;
    }

    @Override
    public Collection<T> getOldVersionsByAccId(BigDecimal accId, Date fd, Date td) throws GenericSystemException {

        try {
            return getObjects(
                    false,
                    "SELECT TBL_HSTR." + COLUMN_ROWID + ", TBL_HSTR.* FROM " + getTableName() + HISTORY
                            + " TBL_HSTR WHERE " + "TBL_HSTR." + COLUMN_ACCOUNT_ID + " = ?"
                            + " AND TBL_HSTR." + COLUMN_FD + " between ? and ?"
                    , accId
                    , fd
                    , td
            );
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

    }

    @Override
    public Collection<T> getOldVersions(T versionable, Date from, Date to) throws GenericSystemException {
        return this.getOldVersionsByAccId(versionable.getAccountId(), from, to);
    }

}
