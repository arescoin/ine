package ru.effts.ine.dbe.da.billing;

import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CommonAccessImpl.java 3850 2014-07-03 11:09:41Z DGomon $"
 */
public abstract class CommonAccessImpl<T extends Versionable> extends AbstractVersionableAccess<T> {

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        return this.getObjectById(getMainRegionKey(), getCommonSelect(), id);
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getAllObjects(getMainRegionKey(), getCommonSelect());
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return this.createObject(identifiable, getMainRegionKey(), getCommonSelect(), true);
    }


    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        this.deleteObject(identifiable, getMainRegionKey(), getCommonSelect());
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable)
            throws IneIllegalArgumentException, GenericSystemException {
        return this.updateObject(identifiable, newIdentifiable, getMainRegionKey());
    }

}
