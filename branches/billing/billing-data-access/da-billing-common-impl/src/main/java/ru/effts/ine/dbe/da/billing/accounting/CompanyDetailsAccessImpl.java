package ru.effts.ine.dbe.da.billing.accounting;

import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsAccessImpl.java 3863 2014-10-07 14:09:32Z DGomon $"
 */
public class CompanyDetailsAccessImpl<T extends CompanyDetails>
        extends CommonAccessImpl<T> implements CompanyDetailsAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "COMPANY_DETAILS";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ACC_ID = "acc_id";
    public static final String COLUMN_COMPANY_ID = "company_id";
    public static final String COLUMN_PHONE_N = "phone_n";
    public static final String COLUMN_LEGAL_ADDRESS = "legal_address";
    public static final String COLUMN_PHYSICAL_ADDRESS = "physical_address";

    /** Запрос на выборку */
    public static final String COMPANY_DETAILS_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ACC_ID +
            ", " + TABLE_PREF + COLUMN_COMPANY_ID +
            ", " + TABLE_PREF + COLUMN_PHONE_N +
            ", " + TABLE_PREF + COLUMN_LEGAL_ADDRESS +
            ", " + TABLE_PREF + COLUMN_PHYSICAL_ADDRESS +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.accounting.CompanyDetails} */
    public static final String CACHE_REGION_COMPANY_DETAILS = CompanyDetails.class.getName();


    static {
        setInstanceFieldsMapping(CompanyDetailsAccessImpl.class, new HashMap<String, String>());
        registerSSAHelpers(CompanyDetailsAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void registerSSAHelpers(Class<? extends CompanyDetailsAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<>();
        map.put(CompanyDetails.ACCOUNT_ID, COLUMN_ACC_ID);
        map.put(CompanyDetails.COMPANY_ID, COLUMN_COMPANY_ID);
        map.put(CompanyDetails.PHONE_N, COLUMN_PHONE_N);
        map.put(CompanyDetails.LEGAL_ADDRESS, COLUMN_LEGAL_ADDRESS);
        map.put(CompanyDetails.PHYSICAL_ADDRESS, COLUMN_PHYSICAL_ADDRESS);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_COMPANY_DETAILS;
    }

    @Override
    protected String getCommonSelect() {
        return COMPANY_DETAILS_SELECT;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return CompanyDetails.class;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(CompanyDetails.class);

        this.setVersionableFields(resultSet, result);

        result.setAccountId(this.getId(resultSet, COLUMN_ACC_ID));
        result.setCompanyId(this.getId(resultSet, COLUMN_COMPANY_ID));
        result.setPhoneNumber(this.getString(resultSet, COLUMN_PHONE_N));
        result.setLegalAddress(this.getString(resultSet, COLUMN_LEGAL_ADDRESS));
        result.setPhysicalAddress(this.getString(resultSet, COLUMN_PHYSICAL_ADDRESS));

        return result;
    }


}
