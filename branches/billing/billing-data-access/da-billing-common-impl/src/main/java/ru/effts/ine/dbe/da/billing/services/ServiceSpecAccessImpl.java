package ru.effts.ine.dbe.da.billing.services;

import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpecAccessImpl.java 3863 2014-10-07 14:09:32Z DGomon $"
 */
public class ServiceSpecAccessImpl<T extends ServiceSpec>
        extends CommonAccessImpl<T> implements ServiceSpecAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "SERVICE_SPEC";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_SS_NAME = "ss_name";
//    public static final String COLUMN_TYP = "typ";
//    public static final String COLUMN_DEF_VAL = "def_val";
//    public static final String COLUMN_IS_NULLABLE = "is_nullable";
//    public static final String COLUMN_IS_MODYFABLE = "is_modyfable";

    /** Запрос на выборку */
    public static final String SERVICE_SPEC_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_SS_NAME +
//            ", " + TABLE_PREF + COLUMN_TYP +
//            ", " + TABLE_PREF + COLUMN_DEF_VAL +
//            ", " + TABLE_PREF + COLUMN_IS_NULLABLE +
//            ", " + TABLE_PREF + COLUMN_IS_MODYFABLE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.services.ServiceSpec} */
    public static final String CACHE_REGION_SRV_SPEC = ServiceSpec.class.getName();


    static {
        setInstanceFieldsMapping(ServiceSpecAccessImpl.class, new HashMap<String, String>());
        registerSSAHelpers(ServiceSpecAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<String, String>();
        map.put(ServiceSpec.NAME, COLUMN_SS_NAME);
//        map.put(Constant.TYPE, COLUMN_TYP);
//        map.put(Constant.DEFAULT_VALUE, COLUMN_DEF_VAL);
//        map.put(Constant.NULLABLE, COLUMN_IS_NULLABLE);
//        map.put(Constant.MODIFABLE, COLUMN_IS_MODYFABLE);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerSSAHelpers(Class<? extends ServiceSpecAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<CRUDHelper>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SRV_SPEC;
    }

    @Override
    protected String getCommonSelect() {
        return SERVICE_SPEC_SELECT;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ServiceSpec.class;
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(ServiceSpec.class);

        this.setVersionableFields(resultSet, result);

        result.setName(this.getString(resultSet, COLUMN_SS_NAME));

        return result;
    }

}
