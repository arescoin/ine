package ru.effts.ine.dbe.da.billing.balance;

import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillAccessImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class LastAccountBillAccessImpl<T extends LastAccountBill>
        extends CommonAccessImpl<T> implements LastAccountBillAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "LAST_ACCOUNT_BILL";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_BILL_DATE = "bill_date";

    /** Запрос на выборку */
    public static final String ALL_DATA_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_BILL_DATE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.balance.BalanceItem} */
    public static final String CACHE_REGION = LastAccountBill.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(LastAccountBill.BILL_DATE, COLUMN_BILL_DATE);
        setInstanceFieldsMapping(LastAccountBillAccessImpl.class, map);

        registerIdentifiableHelpers(LastAccountBillAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T value = (T) IdentifiableFactory.getImplementation(getSubjectClass());

        this.setVersionableFields(resultSet, value);

        this.setIdentifiableFields(resultSet, value);
        value.setBillDate(resultSet.getDate(COLUMN_BILL_DATE));

        return value;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        try {
            this.createObject(identifiable, getMainRegionKey(), getCommonSelect(), false);
//            BigDecimal[] bigDecimals = super.createObject(identifiable, false, false);
//            applyNewId(identifiable, bigDecimals);
            return identifiable;
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION;
    }

    @Override
    protected String getCommonSelect() {
        return ALL_DATA_SELECT;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return LastAccountBill.class;
    }
}
