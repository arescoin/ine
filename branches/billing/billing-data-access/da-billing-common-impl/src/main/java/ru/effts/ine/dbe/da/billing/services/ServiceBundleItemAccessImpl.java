package ru.effts.ine.dbe.da.billing.services;

import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemAccessImpl.java 3867 2014-10-16 07:34:01Z DGomon $"
 */
public class ServiceBundleItemAccessImpl<T extends ServiceBundleItem>
        extends CommonAccessImpl<T> implements ServiceBundleItemAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "SERVICE_BUNDLE";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_BUNDLE_ID = "bundle_id";
    public static final String COLUMN_SPEC_ID = "spec_id";
    public static final String COLUMN_TYP = "typ";
    public static final String COLUMN_SRV_NAME = "srv_name";

    /** Запрос на выборку */
    public static final String SERVICE_BUNDLE_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_BUNDLE_ID +
            ", " + TABLE_PREF + COLUMN_SPEC_ID +
            ", " + TABLE_PREF + COLUMN_TYP +
            ", " + TABLE_PREF + COLUMN_SRV_NAME +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.services.ServiceBundleItem} */
    public static final String CACHE_REGION_SERVICE_BUNDLE = ServiceBundleItem.class.getName();


    static {
        setInstanceFieldsMapping(ServiceBundleItemAccessImpl.class, new HashMap<String, String>());
        registerSSAHelpers(ServiceBundleItemAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<>();
        map.put(ServiceBundleItem.BUNDLE_ID, COLUMN_BUNDLE_ID);
        map.put(ServiceBundleItem.SERVICE_SPEC_ID, COLUMN_SPEC_ID);
        map.put(ServiceBundleItem.TYPE, COLUMN_TYP);
        map.put(ServiceBundleItem.SERVICE_NAME, COLUMN_SRV_NAME);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerSSAHelpers(Class<? extends ServiceBundleItemAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }


    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(ServiceBundleItem.class);

        this.setVersionableFields(resultSet, result);

        result.setBundleId(this.getId(resultSet, COLUMN_BUNDLE_ID));
        result.setServiceSpecId(this.getId(resultSet, COLUMN_SPEC_ID));
        result.setType(this.getId(resultSet, COLUMN_TYP));
        result.setServiceName(this.getString(resultSet, COLUMN_SRV_NAME));

        return result;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ServiceBundleItem.class;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SERVICE_BUNDLE;
    }

    @Override
    protected String getCommonSelect() {
        return SERVICE_BUNDLE_SELECT;
    }

}
