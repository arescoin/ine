package ru.effts.ine.dbe.da.billing.balance;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.core.AbstractIdentifiableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemAccessImpl.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
@SuppressWarnings("UnusedDeclaration")
public class BillingHistoryItemAccessImpl<T extends BillingHistoryItem>
        extends AbstractIdentifiableAccess<T> implements BillingHistoryItemAccess<T> {

    /** Таблица состояний переключателя и префикс для обращения к ее столбцам */
    public static final String TABLE = "BILLING_HISTORY_ITEM";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ACC_ID = "acc_id";
    public static final String COLUMN_SRV_ID = "service_id";

    public static final String COLUMN_BILL_DATE = "bill_date";

    /** Запрос на выборку всех билей... */
    public static final String SELECT = "SELECT " +
            TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ACC_ID +
            ", " + TABLE_PREF + COLUMN_SRV_ID +
            ", " + TABLE_PREF + COLUMN_BILL_DATE +
            ", " + TABLE_PREF + COLUMN_DSC +
            " FROM " + TABLE;


    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.balance.BillingHistoryItem} */
    public static final String CACHE_REGION = BillingHistoryItem.class.getName();

    static {
        final Map<String, String> map = new HashMap<>();
        map.put(BillingHistoryItem.ACCOUNT_ID, COLUMN_ACC_ID);
        map.put(BillingHistoryItem.SERVICE_ID, COLUMN_SRV_ID);
        map.put(BillingHistoryItem.BILLING_DATE, COLUMN_BILL_DATE);
        setInstanceFieldsMapping(BillingHistoryItemAccessImpl.class, map);

        registerIdentifiableHelpers(BillingHistoryItemAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }


    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return BillingHistoryItem.class;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {

        checkApplicable(identifiable);

        try {
            beforeCreate(identifiable);

            BigDecimal[] newId = createObject(identifiable, true, false);
            applyNewId(identifiable, newId);

            afterCreate(identifiable);
        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }

        return identifiable;
    }

    @Override
    public Collection<T> searchObjects(Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return super.searchObjects2(searchCriteria, null);
    }

    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {
        //noinspection unchecked
        T value = (T) IdentifiableFactory.getImplementation(BillingHistoryItem.class);
        this.setIdentifiableFields(resultSet, value);
        value.setAccountId(getBigDecimal(resultSet, COLUMN_ACC_ID));
        value.setServiceId(getBigDecimal(resultSet, COLUMN_SRV_ID));
        value.setBillingDate(getDate(resultSet, COLUMN_BILL_DATE));

        return value;
    }

    @Override
    public Collection<T> getAllObjects() throws CoreException {
        return this.getObjects(false, SELECT);
    }

    @Override
    public T getObjectById(BigDecimal... id) throws CoreException {
        Collection<T> collection = this.getObjects(true, "select * from " + TABLE + " where " + COLUMN_ID + " = ?", id);
        if (null != collection && !collection.isEmpty()) {

            return collection.iterator().next();
        }

        return null;
    }

    @Override
    public Collection<T> getHistoryByAccId(BigDecimal accId, Date fd, Date td) throws CoreException {

        SearchCriteriaProfile profile = getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(4);
        SearchCriterion accCrit = profile.getSearchCriterion(
                BillingHistoryItem.ACCOUNT_ID, CriteriaRule.equals, accId);
        search.add(accCrit);
        SearchCriterion datCrit = profile.getSearchCriterion(
                BillingHistoryItem.BILLING_DATE, CriteriaRule.between, fd, td);
        search.add(datCrit);

        return searchObjects(search);


//        Collection<T> collection =
// this.getObjects(true, "select * from " + TABLE + " where " + COLUMN_ACC_ID + " = ? and " + COLUMN_BILL_DATE + " between ? and ?", accId, fd, td);
//        return collection;
    }

    @Override
    public Collection<T> getHistoryForSrvByAccId(BigDecimal accId, BigDecimal srvId, Date fd, Date td)
            throws CoreException {

        SearchCriteriaProfile profile = getSearchCriteriaProfile();
        Set<SearchCriterion> search = new LinkedHashSet<>(4);
        SearchCriterion accCrit = profile.getSearchCriterion(
                BillingHistoryItem.ACCOUNT_ID, CriteriaRule.equals, accId);
        search.add(accCrit);
        SearchCriterion srvCrit = profile.getSearchCriterion(
                BillingHistoryItem.SERVICE_ID, CriteriaRule.equals, srvId);
        search.add(srvCrit);
        SearchCriterion datCrit = profile.getSearchCriterion(
                BillingHistoryItem.BILLING_DATE, CriteriaRule.between, fd, td);
        search.add(datCrit);

        return searchObjects(search);


//        Collection<T> collection =
// this.getObjects(true, "select * from " + TABLE + " where " + COLUMN_ACC_ID + " = ? and " + COLUMN_BILL_DATE + " between ? and ?", accId, fd, td);
//        return collection;
    }


}
