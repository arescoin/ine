package ru.effts.ine.dbe.da.billing.services;

import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceAccessImpl.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public class ServicePriceAccessImpl<T extends ServicePrice>
        extends CommonAccessImpl<T> implements ServicePriceAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "SERVICE_PRICE";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_PL_ID = "pl_id";
    public static final String COLUMN_SRV_BNDL_ID = "srv_bndl_id";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_DAY_PRICE = "day_price";

    /** Запрос на выборку */
    public static final String SERVICE_PRICE_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_PL_ID +
            ", " + TABLE_PREF + COLUMN_SRV_BNDL_ID +
            ", " + TABLE_PREF + COLUMN_PRICE +
            ", " + TABLE_PREF + COLUMN_DAY_PRICE +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.services.ServicePrice} */
    public static final String CACHE_REGION_SERVICE_PRICE = ServicePrice.class.getName();


    static {
        setInstanceFieldsMapping(ServicePriceAccessImpl.class, new HashMap<String, String>());
        registerSSAHelpers(ServicePriceAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void registerSSAHelpers(Class<? extends ServicePriceAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<>();
        map.put(ServicePrice.PRICE_LIST_ID, COLUMN_PL_ID);
        map.put(ServicePrice.SERVICE_BUNDLE_ITEM_ID, COLUMN_SRV_BNDL_ID);
        map.put(ServicePrice.PRICE, COLUMN_PRICE);
        map.put(ServicePrice.DAY_PRICE, COLUMN_DAY_PRICE);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }


    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(ServicePrice.class);

        this.setVersionableFields(resultSet, result);

        result.setPriceListID(this.getId(resultSet, COLUMN_PL_ID));
        result.setServiceBundleItemID(this.getId(resultSet, COLUMN_SRV_BNDL_ID));
        result.setPrice(this.getBigDecimal(resultSet, COLUMN_PRICE));
        result.setDayPrice(this.getBigDecimal(resultSet, COLUMN_DAY_PRICE));

        return result;
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ServicePrice.class;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_SERVICE_PRICE;
    }

    @Override
    protected String getCommonSelect() {
        return SERVICE_PRICE_SELECT;
    }

}
