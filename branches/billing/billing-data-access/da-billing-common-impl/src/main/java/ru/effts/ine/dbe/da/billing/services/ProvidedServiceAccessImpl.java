package ru.effts.ine.dbe.da.billing.services;

import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.CommonAccessImpl;
import ru.effts.ine.dbe.da.core.AbstractVersionableAccess;
import ru.effts.ine.dbe.da.core.CRUDHelper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceAccessImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class ProvidedServiceAccessImpl<T extends ProvidedService>
        extends CommonAccessImpl<T> implements ProvidedServiceAccess<T> {


    /** Таблица и префикс для обращения к ее столбцам */
    public static final String TABLE = "PROVIDED_SERVICE";
    public static final String TABLE_PREF = TABLE + DOT;

    /** Перечисление колонок таблицы */
    public static final String COLUMN_ACC_ID = "acc_id";
    public static final String COLUMN_BUNDLE_ID = "srv_bndl_id";
    public static final String COLUMN_PARENT_ID = "parent_id";
    public static final String COLUMN_SRV_NAME = "srv_name";
    public static final String COLUMN_STATUS = "status";

    /** Запрос на выборку */
    public static final String PROVIDED_SERVICE_SELECT = "SELECT " + TABLE_PREF + COLUMN_ROWID +
            ", " + TABLE_PREF + COLUMN_ID +
            ", " + TABLE_PREF + COLUMN_ACC_ID +
            ", " + TABLE_PREF + COLUMN_BUNDLE_ID +
            ", " + TABLE_PREF + COLUMN_PARENT_ID +
            ", " + TABLE_PREF + COLUMN_SRV_NAME +
            ", " + TABLE_PREF + COLUMN_STATUS +
            ", " + TABLE_PREF + COLUMN_DSC +
            ", " + TABLE_PREF + COLUMN_FD +
            ", " + TABLE_PREF + COLUMN_TD +
            " FROM " + TABLE +
            " WHERE " + SYS_CUR_DATE_PRC + " BETWEEN " + TABLE_PREF + COLUMN_FD + " AND " + TABLE_PREF + COLUMN_TD;

    /** Регион кэша для хранения данных типа {@link ru.effts.ine.billing.services.ProvidedService} */
    public static final String CACHE_REGION_PROVIDED_SERVICE = ProvidedService.class.getName();


    static {
        setInstanceFieldsMapping(ProvidedServiceAccessImpl.class, new HashMap<String, String>());
        registerSSAHelpers(ProvidedServiceAccessImpl.class, new LinkedHashSet<CRUDHelper>());
    }

    protected static void setInstanceFieldsMapping(Class aClass, Map<String, String> instanceFields) {
        Map<String, String> map = new HashMap<>();
        map.put(ProvidedService.ACCOUNT_ID, COLUMN_ACC_ID);
        map.put(ProvidedService.SERVICE_BUNDLE_ID, COLUMN_BUNDLE_ID);
        map.put(ProvidedService.PARENT_ID, COLUMN_PARENT_ID);
        map.put(ProvidedService.SERVICE_NAME, COLUMN_SRV_NAME);
        map.put(ProvidedService.STATUS, COLUMN_STATUS);
        map.putAll(instanceFields);

        AbstractVersionableAccess.setInstanceFieldsMapping(aClass, map);
    }

    protected static void registerSSAHelpers(Class<? extends ProvidedServiceAccessImpl> access, Set<CRUDHelper> helpers) {

        Set<CRUDHelper> helpersAll = new LinkedHashSet<>();
        helpersAll.addAll(helpers);
        AbstractVersionableAccess.registerVersionableHelpers(access, helpersAll);
    }


    @Override
    protected T constructObject(ResultSet resultSet) throws SQLException, CoreException, RuntimeCoreException {

        @SuppressWarnings({"unchecked"}) T result = (T) IdentifiableFactory.getImplementation(ProvidedService.class);

        this.setVersionableFields(resultSet, result);

        result.setAccountId(this.getId(resultSet, COLUMN_ACC_ID));
        result.setServiceBundleId(this.getId(resultSet, COLUMN_BUNDLE_ID));
        result.setParentId(this.getBigDecimal(resultSet, COLUMN_PARENT_ID));
        result.setServiceName(this.getString(resultSet, COLUMN_SRV_NAME));
        result.setStatus(this.getBigDecimal(resultSet, COLUMN_STATUS));

        return result;
    }

    public Collection<T> getHistoryServicesForAccAtDate(BigDecimal accId, Date date) throws CoreException {
        String select = "select * from " + TABLE + HISTORY +" where acc_id = ? and status = 1 and ? between fd and td";
        return getObjects(false, select, accId, date);
    }

    @Override
    public T createObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.createObject(identifiable);
    }

    @Override
    public void deleteObject(T identifiable) throws IneIllegalArgumentException, GenericSystemException {
        super.deleteObject(identifiable);
    }

    @Override
    public T updateObject(T identifiable, T newIdentifiable) throws IneIllegalArgumentException, GenericSystemException {
        return super.updateObject(identifiable, newIdentifiable);
    }

    @Override
    public String getTableName() {
        return TABLE;
    }

    @Override
    protected Class getSubjectClass() throws GenericSystemException {
        return ProvidedService.class;
    }

    @Override
    public String getMainRegionKey() {
        return CACHE_REGION_PROVIDED_SERVICE;
    }

    @Override
    protected String getCommonSelect() {
        return PROVIDED_SERVICE_SELECT;
    }

}
