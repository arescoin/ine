package ru.effts.ine.dbe.da.billing.services;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceAccessTest.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public class ProvidedServiceAccessTest extends BaseVersionableTest {

    public static final String PN = "13468236453248361";
    public static final BigDecimal PARENT = BigDecimal.ONE;

    private static ProvidedServiceAccess<ProvidedService> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (ProvidedServiceAccess<ProvidedService>)
                    AccessFactory.getImplementation(ProvidedService.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        ProvidedService details;
        try {
            details = IdentifiableFactory.getImplementation(ProvidedService.class);

            details.setAccountId(BigDecimal.ONE);
            details.setServiceBundleId(BigDecimal.ONE);
            details.setServiceName("Auto-Test srv name");
            details.setServiceName(PN);
            details.setStatus(BigDecimal.ONE);

            details.setCoreFd(FieldAccess.getSystemFd());
            details.setCoreTd(FieldAccess.getSystemTd());

            details = access.createObject(details);

            details.setAccountId(BigDecimal.ONE);
            details.setServiceBundleId(BigDecimal.ONE);
            details.setParentId(details.getParentId());
            details.setServiceName("Auto-Test srv name");
            details.setServiceName(PN);
            details.setStatus(BigDecimal.ONE);

            details.setCoreFd(FieldAccess.getSystemFd());
            details.setCoreTd(FieldAccess.getSystemTd());

            access.createObject(details);

            ProvidedService details1 = access.getObjectById(details.getCoreId());

            Assert.assertEquals("Failed to save object in DB. Wrong Account",
                    details.getAccountId(), details1.getAccountId());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceBundleId",
                    details.getServiceBundleId(), details1.getServiceBundleId());
            Assert.assertEquals("Failed to save object in DB. Wrong Parent",
                    details.getParentId(), details1.getParentId());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceName",
                    details.getServiceName(), details1.getServiceName());
            Assert.assertEquals("Failed to save object in DB. Wrong Status",
                    details.getStatus(), details1.getStatus());

            return details1;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            ProvidedService oldOne = (ProvidedService) identifiable;
            ProvidedService newOne = IdentifiableFactory.getImplementation(ProvidedService.class);

            BigDecimal coreId = oldOne.getCoreId();

            newOne.setCoreId(coreId);
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            newOne.setAccountId(oldOne.getAccountId().add(BigDecimal.ONE));
            newOne.setServiceBundleId((oldOne.getServiceBundleId().add(BigDecimal.ONE)));
            newOne.setParentId(oldOne.getParentId());
            newOne.setServiceName(oldOne.getServiceName() + "-1");
            newOne.setStatus(new BigDecimal(2));

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(coreId);

            Assert.assertEquals("Failed to save object in DB. Wrong Account",
                    oldOne.getAccountId(), newOne.getAccountId());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceBundleId",
                    oldOne.getServiceBundleId(), newOne.getServiceBundleId());
            Assert.assertEquals("Failed to save object in DB. Wrong Parent",
                    oldOne.getParentId(), newOne.getParentId());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceName",
                    oldOne.getServiceName(), newOne.getServiceName());
            Assert.assertEquals("Failed to save object in DB. Wrong Status",
                    oldOne.getStatus(), newOne.getStatus());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((ProvidedService) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

}