package ru.effts.ine.dbe.da.billing.accounting;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsAccessTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class CompanyDetailsAccessTest extends BaseVersionableTest {

    public static final String PN = "13468236453248361";
    public static final String LA = "23468236455248362";
    public static final String PS = "33468236456248363";

    private static CompanyDetailsAccess<CompanyDetails> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (CompanyDetailsAccess<CompanyDetails>) AccessFactory.getImplementation(CompanyDetails.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        CompanyDetails details;
        try {
            details = IdentifiableFactory.getImplementation(CompanyDetails.class);

            details.setAccountId(BigDecimal.ONE);
            details.setCompanyId(BigDecimal.ONE);
            details.setPhoneNumber(PN);
            details.setLegalAddress(LA);
            details.setPhysicalAddress(PS);


            details.setCoreFd(FieldAccess.getSystemFd());
            details.setCoreTd(FieldAccess.getSystemTd());

            access.createObject(details);

            CompanyDetails details1 = access.getObjectById(details.getCoreId());

            Assert.assertEquals("Failed to save object in DB. Wrong AccountId",
                    details.getAccountId(), details1.getAccountId());
            Assert.assertEquals("Failed to save object in DB. Wrong CompanyId",
                    details.getCompanyId(), details1.getCompanyId());
            Assert.assertEquals("Failed to save object in DB. Wrong PhoneNumber",
                    details.getPhoneNumber(), details1.getPhoneNumber());
            Assert.assertEquals("Failed to save object in DB. Wrong LegalAddress",
                    details.getLegalAddress(), details1.getLegalAddress());
            Assert.assertEquals("Failed to save object in DB. Wrong PhysicalAddress",
                    details.getPhysicalAddress(), details1.getPhysicalAddress());

            return details1;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            CompanyDetails oldOne = (CompanyDetails) identifiable;
            CompanyDetails newOne = IdentifiableFactory.getImplementation(CompanyDetails.class);

            BigDecimal coreId = oldOne.getCoreId();

            newOne.setCoreId(coreId);
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            newOne.setAccountId(BigDecimal.TEN);
            newOne.setCompanyId((oldOne.getCompanyId().add(BigDecimal.ONE)));
            newOne.setPhoneNumber(oldOne.getPhoneNumber() + "-1");
            newOne.setLegalAddress(oldOne.getLegalAddress() + "-1");
            newOne.setPhysicalAddress(oldOne.getPhysicalAddress() + "-1");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(coreId);

            Assert.assertEquals("Failed to modify object on DB.  Wrong AccountId",
                    oldOne.getAccountId(), newOne.getAccountId());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((CompanyDetails) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }


}
