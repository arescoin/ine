package ru.effts.ine.dbe.da.billing.balance;

import org.junit.Test;
import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.utils.BaseTest;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemAccessTest.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
public class BillingHistoryItemAccessTest extends BaseTest {

    @Test
    public void testCreate() {
        try {

            BillingHistoryItem item = IdentifiableFactory.getImplementation(BillingHistoryItem.class);
            item.setAccountId(BigDecimal.ONE);
            item.setServiceId(BigDecimal.ONE);
            item.setBillingDate(new Date());

            //noinspection unchecked
            BillingHistoryItemAccess<BillingHistoryItem> access = (BillingHistoryItemAccess<BillingHistoryItem>)
                    AccessFactory.getImplementation(BillingHistoryItem.class);
            item = access.createObject(item);

            item = access.getObjectById(item.getCoreId());

            SearchCriteriaProfile profile = access.getSearchCriteriaProfile();
            SearchCriterion criterion = profile.getSearchCriterion(
                    BillingHistoryItem.ACCOUNT_ID, CriteriaRule.equals, item.getAccountId());
            Set<SearchCriterion> search = new LinkedHashSet<>(2);
            search.add(criterion);
            access.searchObjects(search);

        } catch (CoreException e) {
            fail(e);
        }
    }

}
