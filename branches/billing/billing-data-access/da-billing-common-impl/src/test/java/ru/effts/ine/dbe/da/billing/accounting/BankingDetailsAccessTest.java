package ru.effts.ine.dbe.da.billing.accounting;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsAccessTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class BankingDetailsAccessTest extends BaseVersionableTest {

    public static final String INN = "13468236453248361";
    public static final String KPP = "23468236455248362";
    public static final String BIK = "33468236456248363";
    public static final String B_ACC_N = "434682377459248364";
    public static final String B_COR_N = "4346646464646";
    public static final String B_NAME = "Test BAN";

    private static BankingDetailsAccess<BankingDetails> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (BankingDetailsAccess<BankingDetails>) AccessFactory.getImplementation(BankingDetails.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        BankingDetails details;
        try {
            details = IdentifiableFactory.getImplementation(BankingDetails.class);

            details.setAccountId(BigDecimal.ONE);
            details.setInn(INN);
            details.setKpp(KPP);
            details.setBankAccountNumber(B_ACC_N);
            details.setCorAccountNumber(B_COR_N);
            details.setBik(BIK);
            details.setBankName(B_NAME);


            details.setCoreFd(FieldAccess.getSystemFd());
            details.setCoreTd(FieldAccess.getSystemTd());

            access.createObject(details);

            BankingDetails details1 = access.getObjectById(details.getCoreId());

            Assert.assertEquals("Failed to save object in DB. Wrong AccountId",
                    details.getAccountId(), details1.getAccountId());
            Assert.assertEquals("Failed to save object in DB. Wrong BankAccountNumber",
                    details.getBankAccountNumber(), details1.getBankAccountNumber());
            Assert.assertEquals("Failed to save object in DB. Wrong BankAccountNumber",
                    details.getCorAccountNumber(), details1.getCorAccountNumber());
            Assert.assertEquals("Failed to save object in DB. Wrong INN",
                    details.getInn(), details1.getInn());
            Assert.assertEquals("Failed to save object in DB. Wrong KPP",
                    details.getKpp(), details1.getKpp());
            Assert.assertEquals("Failed to save object in DB. Wrong BIK",
                    details.getBik(), details1.getBik());
            Assert.assertEquals("Failed to save object in DB. Wrong BankName",
                    details.getBankName(), details1.getBankName());

            return details1;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            BankingDetails oldOne = (BankingDetails) identifiable;
            BankingDetails newOne = IdentifiableFactory.getImplementation(BankingDetails.class);

            BigDecimal coreId = oldOne.getCoreId();

            newOne.setCoreId(coreId);
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            newOne.setAccountId(BigDecimal.TEN);
            newOne.setInn(oldOne.getInn() + "-1");
            newOne.setKpp(oldOne.getKpp() + "-1");
            newOne.setBankName(oldOne.getBankName() + "-1");
            newOne.setBik(oldOne.getBik() + "-1");
            newOne.setBankAccountNumber(oldOne.getBankAccountNumber() + "-1");
            newOne.setCorAccountNumber(oldOne.getCorAccountNumber() + "-1");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(coreId);

            Assert.assertEquals("Failed to modify object on DB.  Wrong AccountId",
                    oldOne.getAccountId(), newOne.getAccountId());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((BankingDetails) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }


}
