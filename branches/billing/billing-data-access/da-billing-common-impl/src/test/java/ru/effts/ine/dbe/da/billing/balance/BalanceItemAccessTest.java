package ru.effts.ine.dbe.da.billing.balance;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemAccessTest.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public class BalanceItemAccessTest extends BaseVersionableTest {

    private static BalanceItemAccess<BalanceItem> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (BalanceItemAccess<BalanceItem>) AccessFactory.getImplementation(BalanceItem.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        try {
            BalanceItem obj1 = IdentifiableFactory.getImplementation(BalanceItem.class);
            obj1.setAccountId(BigDecimal.ONE);
            obj1.setAddition(true);
            obj1.setAmount(new BigDecimal(2));
            obj1.setBalance(new BigDecimal(3));
            obj1.setReasonId(new BigDecimal(5));
            obj1.setReasonType(BigDecimal.TEN);
            obj1.setDate(new Date());
            obj1.setCoreDsc("TestObject (BalanceItem) for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            BalanceItem obj2 = access.createObject(obj1);

            Assert.assertEquals("Failed to save new object in DB", obj1.getAccountId(), obj2.getAccountId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getAmount(), obj2.getAmount());
            Assert.assertEquals("Failed to save new object in DB", obj1.getBalance(), obj2.getBalance());
            Assert.assertEquals("Failed to save new object in DB", obj1.getDate(), obj2.getDate());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonId(), obj2.getReasonId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonType(), obj2.getReasonType());

            return obj2;

        } catch (CoreException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            BalanceItem obj1 = (BalanceItem) identifiable;
            BalanceItem obj2 = access.getObjectById(obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getAccountId(), obj2.getAccountId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getAmount(), obj2.getAmount());
            Assert.assertEquals("Failed to save new object in DB", obj1.getBalance(), obj2.getBalance());
            Assert.assertEquals("Failed to save new object in DB", obj1.getDate(), obj2.getDate());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonId(), obj2.getReasonId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonType(), obj2.getReasonType());

            return obj2;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {

        try {
            BalanceItem obj1 = (BalanceItem) identifiable;
            BalanceItem obj2 = access.getObjectById(obj1.getCoreId());

            obj2.setAccountId(BigDecimal.ONE);
            obj2.setAddition(false);
            obj2.setAmount(new BigDecimal(1));
            obj2.setBalance(new BigDecimal(2));
            obj2.setReasonId(new BigDecimal(3));
            obj2.setReasonType(new BigDecimal(4));
            obj2.setDate(new Date());
            obj2.setCoreDsc("TestObject 2 (BalanceItem) for CRUD-operations testing");
            obj2.setCoreFd(obj1.getCoreFd());
            obj2.setCoreTd(obj1.getCoreTd());

            BalanceItem obj3 = access.updateObject(obj1, obj2);

            Assert.assertEquals("Failed to save new object in DB", obj2.getAccountId(), obj3.getAccountId());
            Assert.assertEquals("Failed to save new object in DB", obj2.getAmount(), obj3.getAmount());
            Assert.assertEquals("Failed to save new object in DB", obj2.getBalance(), obj3.getBalance());
            Assert.assertEquals("Failed to save new object in DB", obj2.getDate(), obj3.getDate());
            Assert.assertEquals("Failed to save new object in DB", obj2.getCoreDsc(), obj3.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj2.getReasonId(), obj3.getReasonId());
            Assert.assertEquals("Failed to save new object in DB", obj2.getReasonType(), obj3.getReasonType());

            return obj2;

        } catch (CoreException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {

            access.getOldVersions((BalanceItem) identifiable);
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.add(Calendar.MONTH, -2);

            access.getOldVersions((BalanceItem) identifiable, calendar.getTime(), new Date());
            access.getOldVersionsByAccId(BigDecimal.ONE, calendar.getTime(), new Date());

            access.deleteObject((BalanceItem) identifiable);
            BalanceItem obj2 = access.getObjectById(identifiable.getCoreId());
            Assert.assertNull(obj2);
        } catch (Exception e) {
            fail(e);
        }

    }
}
