package ru.effts.ine.dbe.da.billing.accounting;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountAccessTest.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public class AccountAccessTest extends BaseVersionableTest {

    private static AccountAccess<Account> access;


    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (AccountAccess<Account>) AccessFactory.getImplementation(Account.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        Account account;
        try {
            account = IdentifiableFactory.getImplementation(Account.class);

            account.setPriceListId(BigDecimal.ONE);
            account.setClientType(new BigDecimal(2));
            account.setClientId(BigDecimal.ONE);
            account.setDiscount(BigDecimal.ONE);
            account.setCoreFd(FieldAccess.getSystemFd());
            account.setCoreTd(FieldAccess.getSystemTd());

            access.createObject(account);

            Account account1 = access.getObjectById(account.getCoreId());

            Assert.assertEquals("Failed to save object in DB. Wrong Price List",
                    account.getPriceListId(), account1.getPriceListId());
            Assert.assertEquals("Failed to save object in DB. Wrong Client Type",
                    account.getClientType(), account1.getClientType());
            Assert.assertEquals("Failed to save object in DB. Client Id",
                    account.getClientId(), account1.getClientId());
            Assert.assertEquals("Failed to save object in DB. Wrong Discount",
                    account.getDiscount(), account1.getDiscount());

            return account1;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            Account oldOne = (Account) identifiable;
            Account newOne = IdentifiableFactory.getImplementation(Account.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            newOne.setPriceListId(new BigDecimal(2));
            newOne.setClientType(new BigDecimal(1));
            newOne.setClientId(new BigDecimal(4));
            newOne.setDiscount(new BigDecimal(3));

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(oldOne.getCoreId());

            Assert.assertEquals("Failed to modify object on DB.  Wrong Price List",
                    oldOne.getPriceListId(), newOne.getPriceListId());
            Assert.assertEquals("Failed to save object in DB. Wrong Client Type",
                    oldOne.getClientType(), newOne.getClientType());
            Assert.assertEquals("Failed to save object in DB. Client Id",
                    oldOne.getClientId(), newOne.getClientId());
            Assert.assertEquals("Failed to modify object on DB. Wrong Discount",
                    oldOne.getDiscount(), newOne.getDiscount());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((Account) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }
}
