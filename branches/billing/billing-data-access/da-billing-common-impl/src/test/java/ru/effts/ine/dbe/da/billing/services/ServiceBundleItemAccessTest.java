package ru.effts.ine.dbe.da.billing.services;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemAccessTest.java 3867 2014-10-16 07:34:01Z DGomon $"
 */
public class ServiceBundleItemAccessTest extends BaseVersionableTest {

    public static final String PN = "13468236453248361";

    private static ServiceBundleItemAccess<ServiceBundleItem> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (ServiceBundleItemAccess<ServiceBundleItem>)
                    AccessFactory.getImplementation(ServiceBundleItem.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        ServiceBundleItem details;
        try {
            details = IdentifiableFactory.getImplementation(ServiceBundleItem.class);

            details.setBundleId(BigDecimal.ONE);
            details.setServiceSpecId(BigDecimal.ONE);
            details.setType(BigDecimal.ONE);
            details.setServiceName(PN);

            details.setCoreFd(FieldAccess.getSystemFd());
            details.setCoreTd(FieldAccess.getSystemTd());

            access.createObject(details);

            ServiceBundleItem details1 = access.getObjectById(details.getCoreId());

            Assert.assertEquals("Failed to save object in DB. Wrong BundleId",
                    details.getBundleId(), details1.getBundleId());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceSpecId",
                    details.getServiceSpecId(), details1.getServiceSpecId());
            Assert.assertEquals("Failed to save object in DB. Wrong TypeId",
                    details.getType(), details1.getType());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceName",
                    details.getServiceName(), details1.getServiceName());

            return details1;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            ServiceBundleItem oldOne = (ServiceBundleItem) identifiable;
            ServiceBundleItem newOne = IdentifiableFactory.getImplementation(ServiceBundleItem.class);

            BigDecimal coreId = oldOne.getCoreId();

            newOne.setCoreId(coreId);
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            newOne.setBundleId(oldOne.getBundleId().add(BigDecimal.ONE));
            newOne.setServiceSpecId((oldOne.getServiceSpecId().add(BigDecimal.ONE)));
            newOne.setType(new BigDecimal(2));
            newOne.setServiceName(oldOne.getServiceName() + "-1");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(coreId);

            Assert.assertEquals("Failed to modify object on DB.  Wrong AccountId",
                    oldOne.getBundleId(), newOne.getBundleId());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceSpecId",
                    oldOne.getServiceSpecId(), newOne.getServiceSpecId());
            Assert.assertEquals("Failed to save object in DB. Wrong TypeId",
                    oldOne.getType(), newOne.getType());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceName",
                    oldOne.getServiceName(), newOne.getServiceName());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((ServiceBundleItem) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

}
