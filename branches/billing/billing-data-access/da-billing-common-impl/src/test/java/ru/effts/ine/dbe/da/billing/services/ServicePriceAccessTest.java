package ru.effts.ine.dbe.da.billing.services;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceAccessTest.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public class ServicePriceAccessTest extends BaseVersionableTest {

    public static final BigDecimal PRICE = new BigDecimal(90);

    private static ServicePriceAccess<ServicePrice> access;

    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (ServicePriceAccess<ServicePrice>) AccessFactory.getImplementation(ServicePrice.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        ServicePrice details;
        try {
            details = IdentifiableFactory.getImplementation(ServicePrice.class);

            details.setPriceListID(BigDecimal.ONE);
            details.setServiceBundleItemID(BigDecimal.ONE);
            details.setPrice(new BigDecimal(15));

            details.setCoreFd(FieldAccess.getSystemFd());
            details.setCoreTd(FieldAccess.getSystemTd());

            access.createObject(details);

            ServicePrice details1 = access.getObjectById(details.getCoreId());

            Assert.assertEquals("Failed to save object in DB. Wrong PriceListID",
                    details.getPriceListID(), details1.getPriceListID());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceBundleItemID",
                    details.getServiceBundleItemID(), details1.getServiceBundleItemID());
            Assert.assertEquals("Failed to save object in DB. Wrong Price",
                    details.getPrice(), details1.getPrice());

            return details1;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            ServicePrice oldOne = (ServicePrice) identifiable;
            ServicePrice newOne = IdentifiableFactory.getImplementation(ServicePrice.class);

            BigDecimal coreId = oldOne.getCoreId();

            newOne.setCoreId(coreId);
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());

            newOne.setPriceListID(oldOne.getPriceListID().add(BigDecimal.ONE));
            newOne.setServiceBundleItemID((oldOne.getServiceBundleItemID().add(BigDecimal.ONE)));
            newOne.setPrice(oldOne.getPrice().add(new BigDecimal(30)));

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(coreId);

            Assert.assertEquals("Failed to save object in DB. Wrong PriceListID",
                    oldOne.getPriceListID(), newOne.getPriceListID());
            Assert.assertEquals("Failed to save object in DB. Wrong ServiceBundleItemID",
                    oldOne.getServiceBundleItemID(), newOne.getServiceBundleItemID());
            Assert.assertEquals("Failed to save object in DB. Wrong Price",
                    oldOne.getPrice(), newOne.getPrice());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((ServicePrice) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }

}
