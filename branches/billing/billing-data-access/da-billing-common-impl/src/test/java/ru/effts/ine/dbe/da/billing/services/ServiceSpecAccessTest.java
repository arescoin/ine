package ru.effts.ine.dbe.da.billing.services;

import org.junit.Assert;
import org.junit.BeforeClass;
import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.utils.BaseVersionableTest;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpecAccessTest.java 3850 2014-07-03 11:09:41Z DGomon $"
 */
public class ServiceSpecAccessTest extends BaseVersionableTest {

    private static ServiceSpecAccess<ServiceSpec> access;


    @SuppressWarnings({"unchecked"})
    @BeforeClass
    public static void initAccess() {
        try {
            access = (ServiceSpecAccess<ServiceSpec>) AccessFactory.getImplementation(ServiceSpec.class);
        } catch (Exception e) {
            fail(e);
        }
    }


    @Override
    public Identifiable testCreate() {

        ServiceSpec serviceSpec;
        try {
            serviceSpec = IdentifiableFactory.getImplementation(ServiceSpec.class);

            serviceSpec.setName("Test Service Spec");
            serviceSpec.setCoreFd(FieldAccess.getSystemFd());
            serviceSpec.setCoreTd(FieldAccess.getSystemTd());

            access.createObject(serviceSpec);

            ServiceSpec serviceSpec2 = access.getObjectById(serviceSpec.getCoreId());

            Assert.assertEquals("Failed to save object in DB. Wrong NAME",
                    serviceSpec.getName(), serviceSpec2.getName());

            return serviceSpec2;

        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testRetrieve(Identifiable identifiable) {

        try {
            return access.getObjectById(identifiable.getCoreId());
        } catch (Exception e) {
            fail(e);
        }

        return null;
    }

    @Override
    public Identifiable testUpdate(Identifiable identifiable) {
        try {
            ServiceSpec oldOne = (ServiceSpec) identifiable;
            ServiceSpec newOne = IdentifiableFactory.getImplementation(ServiceSpec.class);

            newOne.setCoreId(oldOne.getCoreId());
            newOne.setCoreFd(FieldAccess.getCurrentDate());
            newOne.setCoreTd(oldOne.getCoreTd());
            newOne.setCoreDsc(oldOne.getCoreDsc());
            newOne.setName(oldOne.getName() + " UPD");

            oldOne = access.updateObject(oldOne, newOne);
            newOne = access.getObjectById(oldOne.getCoreId());

            Assert.assertEquals("Failed to modify object on DB.", oldOne.getName(), newOne.getName());

            return newOne;
        } catch (Exception e) {
            fail(e);
        }
        return null;
    }

    @Override
    public void testDelete(Identifiable identifiable) {
        try {
            access.deleteObject((ServiceSpec) identifiable);
        } catch (Exception e) {
            fail(e);
        }
    }
}
