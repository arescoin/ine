package ru.effts.ine.billing.accounting;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Описание сомпании
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetails.java 3845 2014-06-26 11:53:06Z DGomon $"
 */
public interface CompanyDetails extends Versionable {

    String ACCOUNT_ID = "accountId";
    String COMPANY_ID = "companyId";
    String PHONE_N = "phoneNumber";
    String LEGAL_ADDRESS = "legalAddress";
    String PHYSICAL_ADDRESS = "physicalAddress";


    /** Возвращает идентификатор компании */
    BigDecimal getAccountId();

    /**
     * Устанавливает идентификатор компании
     *
     * @throws IllegalIdException при попытке передать <b>null<b/>
     */
    void setAccountId(BigDecimal accountId) throws IllegalIdException;


    /** Возвращает идентификатор компании {@link ru.effts.ine.oss.entity.Company} */
    BigDecimal getCompanyId();

    /**
     * Устанавливает идентификатор компании {@link ru.effts.ine.oss.entity.Company}
     *
     * @throws IllegalIdException ри попытке передать <b>null<b/>
     */
    void setCompanyId(BigDecimal companyId) throws IllegalIdException;

    /** Возвращает контактный номер */
    String getPhoneNumber();

    /** Устанавливает контактный номер */
    void setPhoneNumber(String phoneNumber);

    /** Устанавливает юридический адрес */
    String getLegalAddress();

    /** Возвращает юридический адрес */
    void setLegalAddress(String address);

    /** Возвращает фактический адрес */
    String getPhysicalAddress();

    /** Устанавливает фактический адрес */
    void setPhysicalAddress(String address);

}
