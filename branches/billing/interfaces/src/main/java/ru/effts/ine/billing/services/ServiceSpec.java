package ru.effts.ine.billing.services;


import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

/**
 * Низкоуровнево (техническое) описание услуги/тех.возможности, на основе которого строится бизнес-услуга
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpec.java 3845 2014-06-26 11:53:06Z DGomon $"
 */
public interface ServiceSpec extends Versionable {

    String NAME = "name";

    /** Возвращает название услуги/тех.возможности */
    String getName();

    /**
     * Задаёт название услуги/тех.возможности
     *
     * @throws IneIllegalArgumentException при попытке передать <b>null</b> или <b>пустую строку</b>
     */
    void setName(String name) throws IneIllegalArgumentException;
}
