package ru.effts.ine.billing.balance;

import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBill.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
public interface LastAccountBill extends Versionable {

    String BILL_DATE = "billDate";

    Date getBillDate();

    void setBillDate(Date billDate) throws IneIllegalArgumentException;
}
