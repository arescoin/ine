package ru.effts.ine.billing.balance;

import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItem.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
public interface BillingHistoryItem extends Identifiable {

    String ACCOUNT_ID = "accountId";
    String SERVICE_ID = "serviceId";
    String BILLING_DATE = "billingDate";


    BigDecimal getServiceId();

    void setServiceId(BigDecimal serviceId) throws IllegalIdException;

    BigDecimal getAccountId();

    void setAccountId(BigDecimal accountId) throws IllegalIdException;


    Date getBillingDate();

    void setBillingDate(Date billingDate) throws IneIllegalArgumentException;
}
