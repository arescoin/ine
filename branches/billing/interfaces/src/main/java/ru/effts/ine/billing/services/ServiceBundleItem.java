package ru.effts.ine.billing.services;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Элемент сервисбандла (услуги в тарифе). Бизнес-услуга.
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItem.java 3867 2014-10-16 07:34:01Z DGomon $"
 */
public interface ServiceBundleItem extends Versionable {

    String BUNDLE_ID = "bundleId";
    String SERVICE_SPEC_ID = "serviceSpecId";
    String TYPE = "type";
    String SERVICE_NAME = "serviceName";

    BigDecimal getBundleId();

    void setBundleId(BigDecimal id) throws IllegalIdException;

    BigDecimal getServiceSpecId();

    void setServiceSpecId(BigDecimal id) throws IllegalIdException;

    String getServiceName();

    void setServiceName(String name) throws IneIllegalArgumentException;

    BigDecimal getType();

    void setType(BigDecimal type) throws IllegalIdException;
}
