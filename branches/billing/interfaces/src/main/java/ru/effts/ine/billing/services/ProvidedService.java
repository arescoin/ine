package ru.effts.ine.billing.services;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Предоставленная клиенту услуга
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedService.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public interface ProvidedService extends Versionable {

    String ACCOUNT_ID = "accountId";
    String SERVICE_BUNDLE_ID = "serviceBundleId";
    String SERVICE_NAME = "serviceName";
    String PARENT_ID = "parentId";
    String STATUS = "status";

    BigDecimal getAccountId();

    void setAccountId(BigDecimal accountId) throws IllegalIdException;

    BigDecimal getServiceBundleId();

    void setServiceBundleId(BigDecimal serviceBundleId) throws IllegalIdException;

    String getServiceName();

    void setServiceName(String serviceName);

    BigDecimal getParentId();

    void setParentId(BigDecimal parentId);

    BigDecimal getStatus();

    void setStatus(BigDecimal status) throws IllegalIdException;

}
