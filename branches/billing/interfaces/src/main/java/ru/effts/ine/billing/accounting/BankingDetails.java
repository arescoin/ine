package ru.effts.ine.billing.accounting;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Описание детальной информации о банковских реквизитах компании
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetails.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public interface BankingDetails extends Versionable {

    String ACCOUNT_ID = "accountId";
    String INN = "inn";
    String KPP = "kpp";
    String BANK_NAME = "bankName";
    String BIK = "bik";
    String BANK_ACCOUNT_NUMBER = "bankAccountNumber";
    String COR_ACCOUNT_NUMBER = "corAccountNumber";


    /** Возвращает идентификатор {@link Account счёта} */
    BigDecimal getAccountId();

    /**
     * Устанавливает идентификатор {@link Account счёта}
     *
     * @throws IllegalIdException при попытке передать <b>null<b/>
     */
    void setAccountId(BigDecimal accountId) throws IllegalIdException;

    /** Возвращает ИНН */
    String getInn();

    /** Устанавливает ИНН */
    void setInn(String inn);

    /** Возвращает КПП */
    String getKpp();

    /** Устанавливает КПП */
    void setKpp(String kpp);

    /** Возвращает наименование банка */
    String getBankName();

    /** Устанавливает наименование банка */
    void setBankName(String bankName);

    /** Возвращает БИК */
    String getBik();

    /** Устанавливает БИК */
    void setBik(String bik);

    /** Возвращает номер банковского лицевого счета */
    String getBankAccountNumber();

    /** Устанавливает номер банковского лицевого счета */
    void setBankAccountNumber(String bankAcc);

    /** Возвращает номер банковского кор.счета */
    String getCorAccountNumber();

    /** Устанавливает номер банковского кор.счета */
    void setCorAccountNumber(String bankAcc);



}
