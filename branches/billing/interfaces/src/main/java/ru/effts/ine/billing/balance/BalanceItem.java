package ru.effts.ine.billing.balance;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItem.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public interface BalanceItem extends Versionable {

    String ACCOUNT_ID = "accountId";
    String ADDITION = "addition";
    String AMOUNT = "amount";
    String BALANCE = "balance";
    String REASON_TYPE = "reasonType";
    String REASON_ID = "reasonId";
    String DATE = "date";


    BigDecimal getAccountId();

    void setAccountId(BigDecimal accountId) throws IllegalIdException;

    boolean isAddition();

    void setAddition(boolean addition);

    BigDecimal getBalance();

    void setBalance(BigDecimal balance) throws IneIllegalArgumentException;

    BigDecimal getAmount();

    void setAmount(BigDecimal amount) throws IneIllegalArgumentException;

    BigDecimal getReasonType();

    void setReasonType(BigDecimal reasonType) throws IllegalIdException;

    BigDecimal getReasonId();

    void setReasonId(BigDecimal reasonId) throws IllegalIdException;

    Date getDate();

    void setDate(Date date) throws IneIllegalArgumentException;
}
