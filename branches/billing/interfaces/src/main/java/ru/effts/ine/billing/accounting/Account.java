package ru.effts.ine.billing.accounting;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.Versionable;

import java.math.BigDecimal;

/**
 * Описание учетки в биллинге
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: Account.java 3858 1970-01-01 00:00:00Z DGomon $"
 */
public interface Account extends Versionable {

    String CL_TYPE = "clientType";
    String CL_ID = "clientId";
    String PRICE_LIST_ID = "priceListId";
    String DISCOUNT = "discount";

    /** Возвращает идентификатор актуального для ЛС прайс-листа (Словарь 43) */
    BigDecimal getPriceListId();

    /**
     * Устанавливает идентификатор актуального для ЛС прайс-листа (Словарь 43)
     *
     * @throws IllegalIdException при попытке передать <b>null<b/>
     */
    void setPriceListId(BigDecimal priceListID) throws IllegalIdException;

    /** Возвращает коэффициетн индивидуальной скидки, по умолччанию - 1 */
    BigDecimal getDiscount();

    /** Устанавливает коэффициетн индивидуальной скидки, по умолччанию - 1 */
    void setDiscount(BigDecimal discount);

    /** Возвращает Тип клиента (Словарь 44) */
    BigDecimal getClientType();

    /** Устанавливает Тип клиента (Словарь 44) */
    void setClientType(BigDecimal type) throws IllegalIdException;

    /** Идентификатор клиента */
    BigDecimal getClientId();

    /***/
    void setClientId(BigDecimal clientId) throws IllegalIdException;

}
