package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillActionBean.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
public interface LastAccountBillActionBean {

    LastAccountBill getByAccId(UserProfile user, BigDecimal accountId) throws GenericSystemException;

    Collection<LastAccountBill> getOldBillsForAcc(UserProfile user, BigDecimal accountId) throws GenericSystemException;
}
