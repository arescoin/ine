package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsActionBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface BankingDetailsActionBean {

    Map<String, Object> getById(UserProfile user, BigDecimal accountId, boolean byAccount) throws GenericSystemException;

    BankingDetails create(UserProfile user, BankingDetails bankingDetails) throws GenericSystemException;

    BankingDetails update(UserProfile user, BankingDetails bankingDetails) throws GenericSystemException;

    void delete(UserProfile user, Long detailsId) throws GenericSystemException;
}
