package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemActionBean.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public interface BalanceItemActionBean {

    BalanceItem getBalanceItemForAccount(
            UserProfile userProfile, BigDecimal accountId) throws GenericSystemException;

    Collection<BalanceItem> getBalanceHistoryForAccount(
            UserProfile userProfile, BigDecimal accountId, Date from, Date to) throws GenericSystemException;
}
