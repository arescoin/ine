package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemActionBean.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
public interface BillingHistoryItemActionBean {

    BillingHistoryItem getHistoryById(UserProfile user, BigDecimal id) throws GenericSystemException;

    Collection<BillingHistoryItem> getHistoryByAccId(UserProfile user, BigDecimal accId, Date fd, Date td) throws GenericSystemException;

    Collection<BillingHistoryItem> getHistoryBySrvId(UserProfile user, BigDecimal srvId, Date fd, Date td) throws GenericSystemException;
}
