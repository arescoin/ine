package ru.effts.ine.ejbs.billing.business;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.GenericSystemException;

/**
 * Интерфейс для j2ee методов бизнес-логики биллинга
 */
public interface BusinessActions {

    public static final String TYPE_ATTRIBUTE = "type";
    public static final String BALANCE_ATTRIBUTE = "balance";
    public static final String NUMBER_ATTRIBUTE = "number";
    public static final String DISCOUNT_ATTRIBUTE = "discount";

    public static final String COMPANY_NAME_ATTRIBUTE = "companyName";
    public static final String COMPANY_PHONES_ATTRIBUTE = "companyPhones";
    public static final String COMPANY_ADDRESSES_ATTRIBUTE = "companyAddresses";
    public static final String COMPANY_BANK_NAME_ATTRIBUTES = "companyBankName";
    public static final String COMPANY_BANK_ACCOUNT_ATTRIBUTES = "companyBankAccount";

    /**
     * Создание лицевого счета ФЛ
     * @return идентификатор лицевого счета
     */
    String createPersonAccount();

    /**
     * Создание лицевого счета компании
     *
     * @param attributes карта с атрибутами компании и банковскими атрибутами
     * @return идентификатор лицевого счета
     */
    String createCompanyAccount(Map<String, String> attributes);

    /**
     * Получение данных по идентификатору лицевого счета
     *
     * @param accountId идентификатор лицевого счета
     * @throws GenericSystemException если лицевой счет не найден
     */
    Account getAccountInfo(BigDecimal accountId) throws GenericSystemException;

    /**
     * Получение элемента цены услуги в тарифе по идентификатору
     *
     * @param id идентификатор элемента цены услуги в тарифе
     * @throws GenericSystemException если элемент цены услуги в тарифе не найден
     */
     ServicePrice getServicePriceInfo(BigDecimal id) throws GenericSystemException;

    /**
     * Получение услуги по идентификатору
     *
     * @param id идентификатор услуги
     * @throws GenericSystemException если услуга не найдена
     */
     ServiceBundleItem getServiceBundleItemInfo(BigDecimal id) throws GenericSystemException;

    /**
     * Получение описания техю возможности по идентификатору
     *
     * @param id идентификатор описания
     * @throws GenericSystemException если описание не найдено
     */
     ServiceSpec getServiceSpecInfo(BigDecimal id) throws GenericSystemException;

    /**
     * Восзращает значение из словаря для прайс-листа
     *
     * @param id идентификатор словарной статьи
     * @return знаачение в словаре
     * @throws GenericSystemException если словарная статья не найдена
     */
     String getServicePriceDescription(BigDecimal id) throws GenericSystemException;

    /**
     * Восзращает значение из словаря для услуги
     *
     * @param id идентификатор словарной статьи
     * @return знаачение в словаре
     * @throws GenericSystemException если словарная статья не найдена
     */
     String getServiceBundleItemDescription(BigDecimal id) throws GenericSystemException;

    /**
     * Восзращает значение из словаря для типа клиента
     *
     * @param id идентификатор словарной статьи
     * @return знаачение в словаре
     * @throws GenericSystemException если словарная статья не найдена
     */
     String getClientTypeDescription(BigDecimal id) throws GenericSystemException;


    /**
     * Возвращает банковские реквизиты относящиеся к лицевом счету, идентификатор которого передан
     *
     * @param accountId идентификатор лицевого счета
     * @return коллекция банковских реквизитов для данного лицевого счета
     * @throws GenericSystemException при ошибках выполнения поиска
     */
    Collection<BankingDetails> getBankingDetailsForAccount(BigDecimal accountId) throws GenericSystemException;

    /**
     * Возвращает реквизиты компании(адреса(а), телефон(ы) и пр) относящиеся к лицевом счету,
     * идентификатор которого передан
     *
     * @param accountId идентификатор лицевого счета
     * @return коллекция реквизитов для данного лицевого счета
     * @throws GenericSystemException при ошибках выполнения поиска
     */
    Collection<CompanyDetails> getCompanyDetailsForAccount(BigDecimal accountId) throws GenericSystemException;
}
