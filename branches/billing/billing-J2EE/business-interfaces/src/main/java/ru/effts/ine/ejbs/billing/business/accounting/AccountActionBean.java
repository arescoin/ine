package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountActionBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface AccountActionBean {

    Map getAccountById(UserProfile user, BigDecimal accountId, HashSet<String> objectTypes) throws GenericSystemException;

    Account createAccount(UserProfile user, Account account) throws GenericSystemException;

    Account updateAccount(UserProfile user, Account account) throws GenericSystemException;

    void deleteAccount(UserProfile user, Long accountId) throws GenericSystemException;
}
