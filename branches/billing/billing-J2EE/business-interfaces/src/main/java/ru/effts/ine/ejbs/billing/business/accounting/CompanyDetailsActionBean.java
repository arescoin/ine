package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;

import java.math.BigDecimal;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsActionBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface CompanyDetailsActionBean {

    Map<String, Object> getById(UserProfile user, BigDecimal accountId, boolean getCompany) throws GenericSystemException;

    CompanyDetails create(UserProfile user, CompanyDetails details) throws GenericSystemException;

    CompanyDetails update(UserProfile user, CompanyDetails details) throws GenericSystemException;

    void delete(UserProfile user, Long detailsId) throws GenericSystemException;

}
