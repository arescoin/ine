package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.billing.accounting.Account} объектам
 *
 * @author sfilatov
 * @SVNVersion "$Id: AccountBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface AccountBean<T extends Account> extends VersionableBean<T> {
}
