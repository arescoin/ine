package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.billing.services.ServicePrice} объектам
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface ServicePriceBean<T extends ServicePrice> extends VersionableBean<T> {
}
