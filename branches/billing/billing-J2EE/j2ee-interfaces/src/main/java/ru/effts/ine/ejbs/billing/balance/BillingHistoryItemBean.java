package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.ejbs.core.IdentifiableBean;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemBean.java 3868 1970-01-01 00:00:00Z DGomon $"
 */
public interface BillingHistoryItemBean<T extends BillingHistoryItem> extends IdentifiableBean<T>{

}
