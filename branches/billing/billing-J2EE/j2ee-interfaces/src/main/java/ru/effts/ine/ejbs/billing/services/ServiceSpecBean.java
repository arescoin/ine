package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.billing.services.ServiceSpec} объектам
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpecBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface ServiceSpecBean<T extends ServiceSpec> extends VersionableBean<T> {
}
