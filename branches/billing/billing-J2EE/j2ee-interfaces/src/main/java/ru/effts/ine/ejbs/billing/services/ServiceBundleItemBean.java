package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.billing.services.ServiceBundleItem} объектам
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface ServiceBundleItemBean<T extends ServiceBundleItem> extends VersionableBean<T> {
}
