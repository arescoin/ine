package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * Интерфейс для j2ee доступа к {@link ru.effts.ine.billing.accounting.CompanyDetails} объектам
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsBean.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public interface CompanyDetailsBean<T extends CompanyDetails> extends VersionableBean<T> {
}
