package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.ejbs.core.VersionableBean;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillBean.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
public interface LastAccountBillBean<T extends LastAccountBill> extends VersionableBean<T> {
}
