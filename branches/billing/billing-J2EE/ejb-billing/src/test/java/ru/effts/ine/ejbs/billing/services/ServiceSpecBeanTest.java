package ru.effts.ine.ejbs.billing.services;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.utils.BaseTest;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpecBeanTest.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public class ServiceSpecBeanTest extends BaseTest {


    @Test
    @SuppressWarnings({"unchecked"})
    public void testAccountBeanLocal() {
        try {

            final ServiceSpec object = IdentifiableFactory.getImplementation(ServiceSpec.class);

            object.setName("23423423");

            ServiceSpecBean<ServiceSpec> bean = new ServiceSpecBeanImpl<>();
            final ServiceSpec object2 = bean.createObject(user, object);

            Assert.assertEquals(object.getName(), object2.getName());

            final ServiceSpec object3 = bean.getObjectById(user, object2.getCoreId());

            Assert.assertEquals(object2.getName(), object3.getName());

            object3.setName("2342342345");

            final ServiceSpec object4 = bean.updateObject(user, object3);

            Assert.assertEquals(object3.getName(), object4.getName());
            //удаление
            bean.deleteObject(user, object4);

            if (bean.getObjectById(user, object4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }
    }

}