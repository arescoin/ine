package ru.effts.ine.ejbs.billing.balance;

import org.junit.Test;
import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.CoreException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.utils.BaseTest;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemBeanTest.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
public class BillingHistoryItemBeanTest extends BaseTest {

    @Test
    public void test() {
        try {

            BillingHistoryItem item = IdentifiableFactory.getImplementation(BillingHistoryItem.class);
            item.setAccountId(BigDecimal.ONE);
            item.setServiceId(BigDecimal.ONE);
            item.setBillingDate(new Date());

            //noinspection unchecked
            BillingHistoryItemBean<BillingHistoryItem> bean = new BillingHistoryItemBeanImpl<>();
            item = bean.createObject(user, item);

            item = bean.getObjectById(user, item.getCoreId());

            SearchCriteriaProfile profile = bean.getSearchCriteriaProfile(user);
            SearchCriterion criterion = profile.getSearchCriterion(
                    BillingHistoryItem.ACCOUNT_ID, CriteriaRule.equals, item.getAccountId());
            Set<SearchCriterion> search = new LinkedHashSet<>(2);
            search.add(criterion);
            bean.searchObjects(user, search);

        } catch (CoreException e) {
            fail(e);
        }
    }
}
