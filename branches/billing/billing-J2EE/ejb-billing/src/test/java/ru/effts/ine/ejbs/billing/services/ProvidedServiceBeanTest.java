package ru.effts.ine.ejbs.billing.services;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceBeanTest.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public class ProvidedServiceBeanTest extends BaseTest {


    @Test
    @SuppressWarnings({"unchecked"})
    public void testAccountBeanLocal() {
        try {

            final ProvidedService object = IdentifiableFactory.getImplementation(ProvidedService.class);
            object.setAccountId(BigDecimal.ONE);
            object.setServiceBundleId(new BigDecimal(2));
            object.setParentId(BigDecimal.ONE);
            object.setServiceName("234234");
            object.setStatus(BigDecimal.ONE);

            ProvidedServiceBean<ProvidedService> bean = new ProvidedServiceBeanImpl<>();
            final ProvidedService object2 = bean.createObject(user, object);

            Assert.assertEquals(object.getAccountId(), object2.getAccountId());
            Assert.assertEquals(object.getServiceBundleId(), object2.getServiceBundleId());
            Assert.assertEquals(object.getParentId(), object2.getParentId());
            Assert.assertEquals(object.getServiceName(), object2.getServiceName());

            final ProvidedService object3 = bean.getObjectById(user, object2.getCoreId());

            Assert.assertEquals(object2.getAccountId(), object3.getAccountId());
            Assert.assertEquals(object2.getServiceBundleId(), object3.getServiceBundleId());
            Assert.assertEquals(object2.getParentId(), object3.getParentId());
            Assert.assertEquals(object2.getServiceName(), object3.getServiceName());

            object3.setAccountId(new BigDecimal(3));
            object3.setServiceBundleId(new BigDecimal(4));
            object3.setParentId(BigDecimal.ZERO);
            object3.setServiceName("23423422");

            final ProvidedService object4 = bean.updateObject(user, object3);

            Assert.assertEquals(object3.getAccountId(), object4.getAccountId());
            Assert.assertEquals(object3.getServiceBundleId(), object4.getServiceBundleId());
            Assert.assertEquals(object3.getParentId(), object4.getParentId());
            Assert.assertEquals(object3.getServiceName(), object4.getServiceName());

            //удаление
            bean.deleteObject(user, object4);

            if (bean.getObjectById(user, object4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }
    }

}