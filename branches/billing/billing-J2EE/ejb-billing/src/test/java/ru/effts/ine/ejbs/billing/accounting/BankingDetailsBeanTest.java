package ru.effts.ine.ejbs.billing.accounting;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsBeanTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class BankingDetailsBeanTest extends BaseTest {


    @Test
    @SuppressWarnings({"unchecked"})
    public void testAccountBeanLocal() {
        try {

            final BankingDetails object = IdentifiableFactory.getImplementation(BankingDetails.class);
            object.setAccountId(BigDecimal.ONE);
            object.setBankAccountNumber("11111111");
            object.setCorAccountNumber("5555555");
            object.setBankName("wwwwwwwww");
            object.setBik("222222");
            object.setKpp("3333333333333333");
            object.setInn("55555555");

            BankingDetailsBean<BankingDetails> bean = new BankingDetailsBeanImpl<>();
            final BankingDetails object2 = bean.createObject(user, object);

            Assert.assertEquals(object.getBankAccountNumber(), object2.getBankAccountNumber());
            Assert.assertEquals(object.getCorAccountNumber(), object2.getCorAccountNumber());
            Assert.assertEquals(object.getBankName(), object2.getBankName());
            Assert.assertEquals(object.getBik(), object2.getBik());
            Assert.assertEquals(object.getKpp(), object2.getKpp());
            Assert.assertEquals(object.getInn(), object2.getInn());

            final BankingDetails object3 = bean.getObjectById(user, object2.getCoreId());

            Assert.assertEquals(object2.getBankAccountNumber(), object3.getBankAccountNumber());
            Assert.assertEquals(object2.getCorAccountNumber(), object3.getCorAccountNumber());
            Assert.assertEquals(object2.getBankName(), object3.getBankName());
            Assert.assertEquals(object2.getBik(), object3.getBik());
            Assert.assertEquals(object2.getKpp(), object3.getKpp());
            Assert.assertEquals(object2.getInn(), object3.getInn());

            object3.setAccountId(new BigDecimal(2));
            object3.setBankAccountNumber("11111111");
            object3.setBankName("zzz");
            object3.setBik("4444");
            object3.setKpp("11111");
            object3.setInn("666666");

            final BankingDetails object4 = bean.updateObject(user, object3);

            Assert.assertEquals(object3.getCorAccountNumber(), object4.getCorAccountNumber());
            Assert.assertEquals(object3.getBankAccountNumber(), object4.getBankAccountNumber());
            Assert.assertEquals(object3.getBankName(), object4.getBankName());
            Assert.assertEquals(object3.getBik(), object4.getBik());
            Assert.assertEquals(object3.getKpp(), object4.getKpp());
            Assert.assertEquals(object3.getInn(), object4.getInn());

            //удаление
            bean.deleteObject(user, object4);

            if (bean.getObjectById(user, object4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }
    }

}
