package ru.effts.ine.ejbs.billing.accounting;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsBeanTest.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public class CompanyDetailsBeanTest extends BaseTest {


    @Test
    @SuppressWarnings({"unchecked"})
    public void testAccountBeanLocal() {
        try {

            final CompanyDetails object = IdentifiableFactory.getImplementation(CompanyDetails.class);
            object.setAccountId(BigDecimal.ONE);
            object.setCompanyId(new BigDecimal(2));
            object.setPhoneNumber("23424234234");
            object.setLegalAddress("222222");
            object.setPhysicalAddress("3333333333333333");

            CompanyDetailsBean<CompanyDetails> bean = new CompanyDetailsBeanImpl<>();
            final CompanyDetails object2 = bean.createObject(user, object);

            Assert.assertEquals(object.getAccountId(), object2.getAccountId());
            Assert.assertEquals(object.getCompanyId(), object2.getCompanyId());
            Assert.assertEquals(object.getPhoneNumber(), object2.getPhoneNumber());
            Assert.assertEquals(object.getLegalAddress(), object2.getLegalAddress());
            Assert.assertEquals(object.getPhysicalAddress(), object2.getPhysicalAddress());

            final CompanyDetails object3 = bean.getObjectById(user, object2.getCoreId());

            Assert.assertEquals(object2.getAccountId(), object3.getAccountId());
            Assert.assertEquals(object2.getCompanyId(), object3.getCompanyId());
            Assert.assertEquals(object2.getPhoneNumber(), object3.getPhoneNumber());
            Assert.assertEquals(object2.getLegalAddress(), object3.getLegalAddress());
            Assert.assertEquals(object2.getPhysicalAddress(), object3.getPhysicalAddress());


            object3.setAccountId(new BigDecimal(3));
            object3.setCompanyId(new BigDecimal(4));
            object3.setPhoneNumber("12341124");
            object3.setLegalAddress("234234");
            object3.setPhysicalAddress("234123");

            final CompanyDetails object4 = bean.updateObject(user, object3);

            Assert.assertEquals(object3.getAccountId(), object4.getAccountId());
            Assert.assertEquals(object3.getCompanyId(), object4.getCompanyId());
            Assert.assertEquals(object3.getPhoneNumber(), object4.getPhoneNumber());
            Assert.assertEquals(object3.getLegalAddress(), object4.getLegalAddress());
            Assert.assertEquals(object3.getPhysicalAddress(), object4.getPhysicalAddress());

            //удаление
            bean.deleteObject(user, object4);

            if (bean.getObjectById(user, object4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }
    }


}
