package ru.effts.ine.ejbs.billing.services;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.utils.BaseTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemBeanTest.java 3867 2014-10-16 07:34:01Z DGomon $"
 */
public class ServiceBundleItemBeanTest extends BaseTest {


    @Test
    @SuppressWarnings({"unchecked"})
    public void testAccountBeanLocal() {
        try {

            final ServiceBundleItem object = IdentifiableFactory.getImplementation(ServiceBundleItem.class);
            object.setServiceSpecId(BigDecimal.ONE);
            object.setBundleId(new BigDecimal(2));
            object.setType(BigDecimal.ONE);
            object.setServiceName("234234");

            ServiceBundleItemBean<ServiceBundleItem> bean = new ServiceBundleItemBeanImpl<>();
            final ServiceBundleItem object2 = bean.createObject(user, object);

            Assert.assertEquals(object.getServiceSpecId(), object2.getServiceSpecId());
            Assert.assertEquals(object.getBundleId(), object2.getBundleId());
            Assert.assertEquals(object.getServiceName(), object2.getServiceName());
            Assert.assertEquals(object.getType(), object2.getType());

            final ServiceBundleItem object3 = bean.getObjectById(user, object2.getCoreId());

            Assert.assertEquals(object2.getServiceSpecId(), object3.getServiceSpecId());
            Assert.assertEquals(object2.getBundleId(), object3.getBundleId());
            Assert.assertEquals(object2.getType(), object3.getType());
            Assert.assertEquals(object2.getServiceName(), object3.getServiceName());

            object3.setServiceSpecId(BigDecimal.ONE);
            object3.setBundleId(new BigDecimal(2));
            object3.setType(new BigDecimal(2));
            object3.setServiceName("2342342");

            final ServiceBundleItem object4 = bean.updateObject(user, object3);

            Assert.assertEquals(object3.getServiceSpecId(), object4.getServiceSpecId());
            Assert.assertEquals(object3.getBundleId(), object4.getBundleId());
            Assert.assertEquals(object3.getType(), object4.getType());
            Assert.assertEquals(object3.getServiceName(), object4.getServiceName());

            //удаление
            bean.deleteObject(user, object4);

            if (bean.getObjectById(user, object4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }
    }

}