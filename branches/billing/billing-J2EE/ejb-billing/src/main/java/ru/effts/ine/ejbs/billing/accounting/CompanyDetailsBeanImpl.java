package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Реализует {@link CompanyDetailsBean}
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "CompanyDetailsBean", mappedName = "stateless.CompanyDetailsBean")
@Remote(CompanyDetailsBean.class)
public class CompanyDetailsBeanImpl<T extends CompanyDetails>
        extends VersionableBeanImpl<T> implements CompanyDetailsBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    public void init() throws GenericSystemException {
        init(CompanyDetails.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}