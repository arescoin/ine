package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.*;
import ru.effts.ine.core.constants.ConstantValue;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.ejbs.billing.accounting.AccountBean;
import ru.effts.ine.ejbs.billing.balance.BalanceItemBean;
import ru.effts.ine.ejbs.billing.balance.BillingHistoryItemBean;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;
import ru.effts.ine.ejbs.core.constants.ConstantValueBean;
import ru.effts.ine.ejbs.core.structure.FieldAccessBean;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Класс представлющий собой stateless ejb. Реализует {@link ProvidedServiceBean}
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceBeanImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
@Stateless(name = "ProvidedServiceBean", mappedName = "stateless.ProvidedServiceBean")
@Remote(ProvidedServiceBean.class)
public class ProvidedServiceBeanImpl<T extends ProvidedService>
        extends VersionableBeanImpl<T> implements ProvidedServiceBean<T> {

    Logger logger = Logger.getLogger(ProvidedServiceBeanImpl.class.getName());

//    @EJB(name = "ServiceBundleItem", mappedName = "stateless.ServiceBundleItemBean")
//    ServiceBundleItemBean<ServiceBundleItem> serviceBundleItemBean;

    @EJB(mappedName = "stateless.ServicePriceBean", name = "ServicePriceBean")
    ServicePriceBean<ServicePrice> servicePriceBean;

    @EJB(mappedName = "stateless.AccountBean", name = "AccountBean")
    AccountBean<Account> accountBean;

    @EJB(mappedName = "stateless.BillingHistoryItemBean", name = "BillingHistoryItemBean")
    BillingHistoryItemBean<BillingHistoryItem> billingHistoryItemBean;

    @EJB(mappedName = "stateless.BalanceItemBean", name = "BalanceItemBean")
    BalanceItemBean<BalanceItem> balanceItemBean;

    @EJB(mappedName = "stateless.FieldAccessBean", name = "FieldAccessBean")
    FieldAccessBean fieldAccessBean;

    @EJB(mappedName = "stateless.ConstantValueBean", name = "ConstantValueBean")
    ConstantValueBean<ConstantValue> constantValueBean;

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ProvidedService.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @Override
    public T createObject(UserProfile user, T versionable) throws GenericSystemException {

        Account account = checkParams(user, versionable);

        versionable.setCoreFd(FieldAccess.getCurrentDate());

        if (versionable.getStatus().intValue() == 1) {
            InnerVO innerVO = constructInnerVO(user, versionable, account);
            if (innerVO.getAccBal().compareTo(innerVO.getSrvPrDisc()) >= 1) {

                // терь точно добавим услугу!!!
                logger.log(Level.FINE, "New Acc[{4}] Balance  {0} - ({1} * {2}) = {3}",
                        new Object[]{innerVO.getAccBal(), innerVO.getDayPrice(),
                                innerVO.getAccDisc(), innerVO.getNewAccBal(), account.getCoreId()});

                T t = super.createObject(user, versionable);

                saveBalanceAndBill(user, innerVO, t);

                return t;
            } else {

                SearchCriteriaProfile profile = getSearchCriteriaProfile(user);
                Set<SearchCriterion> criteria = new HashSet<>();
                criteria.add(profile.getSearchCriterion(
                        ProvidedService.ACCOUNT_ID, CriteriaRule.equals, versionable.getAccountId()));
                criteria.add(profile.getSearchCriterion(
                        ProvidedService.STATUS, CriteriaRule.greater, new BigDecimal(2)));

                //noinspection unchecked
                Collection<ProvidedService> services =
                        (Collection<ProvidedService>) this.searchObjects(user, criteria);
                ConstantValue constantValue = constantValueBean.getObjectById(user, new BigDecimal(10));
                int threshold = Integer.parseInt(constantValue.getValue());
                if (services.size() > threshold) {
                    throw new BalanceLackException("Actual balance [" + innerVO.getAccBal() + "]");
                }

                versionable.setStatus(new BigDecimal(3));

                return super.createObject(user, versionable);
            }
        }

        return super.createObject(user, versionable);
    }

    @Override
    public T updateObject(UserProfile user, T newVersionable) throws GenericSystemException {

        // Если статус "1 - не блокированно", то начнем проверки и подготовку к подсчету...
        if (newVersionable.getStatus().intValue() == 1) {

            // поолучим актуальную вурсию для сравнения
            T actualProvSrv = getObjectById(user, newVersionable.getCoreId());
            if (null == actualProvSrv) {
                // если ее нет, то матернемся и пошлем на ...
                throw GenericSystemException.toGeneric(
                        new IneCorruptedStateException("Object [" + newVersionable + "] corrupted")
                );
            }

            // проверим совпадение акаунтов, если разные - то напшли...
            if (!newVersionable.getAccountId().equals(actualProvSrv.getAccountId())) {
                String message = "Account for service are modified! Current["
                        + actualProvSrv.getAccountId().longValue() +
                        "]; New [" + newVersionable.getAccountId().longValue() + "].";

                logger.log(Level.SEVERE, message);
                throw GenericSystemException.toGeneric(
                        new IllegalAccessException(message));
            }

            // если статус актуального объекта отличен от "1"
            if (!actualProvSrv.getStatus().equals(newVersionable.getStatus())) {
                // начнем уточнять обстоятельства и считать денюжку...

                // Получим записи по билам нашего сервиса...
                SearchCriteriaProfile profile = billingHistoryItemBean.getSearchCriteriaProfile(user);
                Set<SearchCriterion> criteria = new HashSet<>();
                criteria.add(profile.getSearchCriterion(
                        BillingHistoryItem.SERVICE_ID, CriteriaRule.equals, newVersionable.getCoreId()));
                criteria.add(profile.getSearchCriterion(
                        BillingHistoryItem.ACCOUNT_ID, CriteriaRule.equals, newVersionable.getAccountId()));
                Collection<BillingHistoryItem> items = billingHistoryItemBean.searchObjects(user, criteria);

                // признак необходимости списать денег
                boolean doBill = true;
                if (!items.isEmpty()) {
                    // если были билы на услугу проверим их даты
                    BHIDateComparator bhiDateComparator = new BHIDateComparator();
                    for (BillingHistoryItem item : items) {
                        if (bhiDateComparator.compare(item.getBillingDate(), new Date()) == 0) {
                            // если есть билл в текущих сутках, то денег брать ненада...
                            doBill = false;
                        }// иначе - будем списывать, если они (денюжки) есть
                    }
                }

                // билить / не билить...
                if (doBill) {
                    // билить...
                    Account account = checkParams(user, newVersionable);
                    InnerVO innerVO = constructInnerVO(user, newVersionable, account);

                    // если денег хватает на сегодня под эту услугу, то мы их спишем
                    if (innerVO.getAccBal().compareTo(innerVO.getSrvPrDisc()) >= 1) {
                        saveBalanceAndBill(user, innerVO, actualProvSrv);
                    } else {
                        // иначе - выставим статус в текущий статус актуального объетка...
                        newVersionable.setStatus(actualProvSrv.getStatus());
                    }
                }

            }
        }

        return super.updateObject(user, newVersionable);
    }

    /** первичная проверка на корректность переданного сервиса */
    private Account checkParams(UserProfile user, T versionable) throws GenericSystemException {

        if (null == versionable) {
            throw new IneIllegalArgumentException("Null SERVICE provided");
        }

        BigDecimal accountId = versionable.getAccountId();
        if (null == accountId) {
            throw new IneIllegalArgumentException("Null SERVICE." + ProvidedService.ACCOUNT_ID);
        }

        if (null == versionable.getServiceBundleId()) {
            throw new IneIllegalArgumentException("Null SERVICE." + ProvidedService.SERVICE_BUNDLE_ID);
        }

        if (null == versionable.getStatus()) {
            throw new IneIllegalArgumentException("Null SERVICE." + ProvidedService.STATUS);
        }

        final Account account = accountBean.getObjectById(user, accountId);
        if (null == account) {
            throw new IneIllegalArgumentException("Incorrect Account ID");
        }

        return account;
    }

    private void saveBalanceAndBill(UserProfile user, InnerVO innerVO, T t) throws GenericSystemException {

        try {
            Date itemDate = new Date();

            BalanceItem balanceItem = innerVO.getBalanceItem();
            balanceItem.setBalance(innerVO.getNewAccBal());
            balanceItem.setAmount(innerVO.getSrvPrDisc());
            balanceItem.setDate(itemDate);
            balanceItem.setAddition(false);
            balanceItem.setReasonType(fieldAccessBean.getTableId(ProvidedService.class));
            balanceItem.setReasonId(t.getCoreId());
            balanceItem.setCoreDsc("New service instance create by ["
                    + user.getSystemUser().getLogin() + " id:[ " + user.getSystemUser().getCoreId() + "] ]");

            balanceItemBean.updateObject(user, balanceItem);

            BillingHistoryItem billingHistoryItem = IdentifiableFactory.getImplementation(BillingHistoryItem.class);
            billingHistoryItem.setBillingDate(itemDate);
            billingHistoryItem.setAccountId(innerVO.getBalanceItem().getAccountId());
            billingHistoryItem.setServiceId(t.getCoreId());
            billingHistoryItem.setCoreDsc("New service instance create by ["
                    + user.getSystemUser().getLogin() + " id:[ " + user.getSystemUser().getCoreId() + "] ]");
            billingHistoryItemBean.createObject(user, billingHistoryItem);

        } catch (CoreException e) {
            throw GenericSystemException.toGeneric(e);
        }
    }

    private InnerVO constructInnerVO(UserProfile user, T versionable, Account account)
            throws GenericSystemException {

        BalanceItem balanceItem = balanceItemBean.getBalanceItemForAccount(user, account.getCoreId());

        final BigDecimal plId = account.getPriceListId();
        if (null == plId) {
            throw new IneIllegalArgumentException("Incorrect value at attribute: " + Account.PRICE_LIST_ID);
        }

        final BigDecimal srvBndlId = versionable.getServiceBundleId();

        SearchCriteriaProfile profile = servicePriceBean.getSearchCriteriaProfile(user);
        Set<SearchCriterion> criteria = new HashSet<>();
        criteria.add(profile.getSearchCriterion(
                ServicePrice.SERVICE_BUNDLE_ITEM_ID, CriteriaRule.equals, srvBndlId)
        );
        criteria.add(profile.getSearchCriterion(
                ServicePrice.PRICE_LIST_ID, CriteriaRule.equals, plId)
        );

        Collection<ServicePrice> prices = servicePriceBean.searchObjects(user, criteria);

        if (null == prices || prices.isEmpty() || prices.size() > 1) {
            throw new GenericSystemException("Can't resolve ServicePrice for "
                    + "ServicePrice.SERVICE_BUNDLE_ITEM_ID[" + srvBndlId
                    + "]; ServicePrice.PRICE_LIST_ID[" + plId + "] " + (null != prices ? prices.size() : ""));
        }
        ServicePrice price = prices.iterator().next();

        BigDecimal dayPrice = price.getDayPrice();
        BigDecimal accBal = balanceItem.getBalance();
        BigDecimal accDisc = account.getDiscount();
        BigDecimal srvPrDisc = dayPrice.multiply(accDisc).setScale(2, BigDecimal.ROUND_UP);

        return new InnerVO(dayPrice, accBal, accDisc, srvPrDisc, balanceItem);
    }

    private class InnerVO {

        private BigDecimal dayPrice;
        private BigDecimal accBal;
        private BigDecimal accDisc;
        private BigDecimal srvPrDisc;
        private BalanceItem balanceItem;
        private BigDecimal newAccBal;

        private InnerVO(BigDecimal dayPrice, BigDecimal accBal,
                        BigDecimal accDisc, BigDecimal srvPrDisc, BalanceItem balanceItem) {
            this.dayPrice = dayPrice;
            this.accBal = accBal;
            this.accDisc = accDisc;
            this.srvPrDisc = srvPrDisc;
            this.balanceItem = balanceItem;
            this.newAccBal = accBal.subtract(srvPrDisc);
        }

        private BigDecimal getDayPrice() {
            return dayPrice;
        }

        private BigDecimal getAccBal() {
            return accBal;
        }

        private BigDecimal getAccDisc() {
            return accDisc;
        }

        private BigDecimal getSrvPrDisc() {
            return srvPrDisc;
        }

        private BalanceItem getBalanceItem() {
            return balanceItem;
        }

        private BigDecimal getNewAccBal() {
            return newAccBal;
        }
    }

}
