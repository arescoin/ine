package ru.effts.ine.ejbs.billing.services;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
* @author Denis Gomon
* @SVNVersion "$Id: BHIDateComparator.java 3873 2014-10-23 13:19:10Z DGomon $"
*/
class BHIDateComparator implements Comparator<Date>, Serializable {

    //                                   mil    sec  min  hr
    public static final int MILLIS_DAY = 1000 * 60 * 60 * 24;

    @Override
    public int compare(Date o1, Date o2) {
        return new Long(o1.getTime() / MILLIS_DAY - o2.getTime() / MILLIS_DAY).intValue();
    }
}
