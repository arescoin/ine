package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IneIllegalAccessException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.core.IdentifiableBeanImpl;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemBeanImpl.java 3868 1970-01-01 00:00:00Z DGomon $"
 */
@Stateless(name = "BillingHistoryItemBean", mappedName = "stateless.BillingHistoryItemBean")
@Remote(BillingHistoryItemBean.class)
public class BillingHistoryItemBeanImpl<T extends BillingHistoryItem>
        extends IdentifiableBeanImpl<T> implements BillingHistoryItemBean<T> {

    @Override
    protected void init() throws GenericSystemException {
        init(BillingHistoryItem.class);
    }

    @Override
    public T updateObject(UserProfile user, T newIdentifiable) throws GenericSystemException {
        throw new IneIllegalAccessException("Operation not allowed");
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new IneIllegalAccessException("Operation not allowed");
    }

    @Override
    public int getObjectCount(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return -1;
    }

    @Override
    public Collection searchObjects(UserProfile user, Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {

        return new ArrayList();
    }

    @Override
    public boolean containsObjects(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        return false;
    }

    @Override
    public boolean containsObjects(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return false;
    }

    @Override
    public Collection searchObjects(UserProfile user, StorageFilter[] storageFilters) throws GenericSystemException {
        return new ArrayList();
    }

    @Override
    public int getObjectCount(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        return -1;
    }
}
