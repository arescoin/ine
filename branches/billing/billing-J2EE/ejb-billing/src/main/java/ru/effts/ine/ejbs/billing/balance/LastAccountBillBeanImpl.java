package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillBeanImpl.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
@Stateless(name = "LastAccountBillBean", mappedName = "stateless.LastAccountBillBean")
@Remote(LastAccountBillBean.class)
public class LastAccountBillBeanImpl<T extends LastAccountBill> extends VersionableBeanImpl<T> implements LastAccountBillBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    protected void init() throws GenericSystemException {
        init(LastAccountBill.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
