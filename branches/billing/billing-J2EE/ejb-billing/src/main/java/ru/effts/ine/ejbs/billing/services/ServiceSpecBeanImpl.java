package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Реализует {@link ServiceSpecBean}
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpecBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "ServiceSpecBean", mappedName = "stateless.ServiceSpecBean")
@Remote(ServiceSpecBean.class)
public class ServiceSpecBeanImpl<T extends ServiceSpec> extends VersionableBeanImpl<T> implements ServiceSpecBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ServiceSpec.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}