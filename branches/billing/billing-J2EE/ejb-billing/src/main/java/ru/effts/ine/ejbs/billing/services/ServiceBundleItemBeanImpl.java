package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Реализует {@link ServiceBundleItemBean}
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "ServiceBundleItemBean", mappedName = "stateless.ServiceBundleItemBean")
@Remote(ServiceBundleItemBean.class)
public class ServiceBundleItemBeanImpl<T extends ServiceBundleItem>
        extends VersionableBeanImpl<T> implements ServiceBundleItemBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ServiceBundleItem.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}