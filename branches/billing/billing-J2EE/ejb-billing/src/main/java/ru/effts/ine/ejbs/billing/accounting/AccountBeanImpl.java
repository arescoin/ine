package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Реализует {@link AccountBean}
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "AccountBean", mappedName = "stateless.AccountBean")
@Remote(AccountBean.class)
public class AccountBeanImpl<T extends Account> extends VersionableBeanImpl<T> implements AccountBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    public void init() throws GenericSystemException {
        init(Account.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}
