package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.core.GenericSystemException;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceLackException.java 3873 2014-10-23 13:19:10Z DGomon $"
 */
public class BalanceLackException extends GenericSystemException {
    public BalanceLackException(String message) {
        super(message);
    }
}
