package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.SyntheticId;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * Класс представлющий собой stateless ejb. Реализует {@link ServicePriceBean}
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "ServicePriceBean", mappedName = "stateless.ServicePriceBean")
@Remote(ServicePriceBean.class)
public class ServicePriceBeanImpl<T extends ServicePrice>
        extends VersionableBeanImpl<T> implements ServicePriceBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    public void init() throws GenericSystemException {
        init(ServicePrice.class);
    }

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }
}