package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.*;
import ru.effts.ine.dbe.da.billing.balance.BalanceItemAccess;
import ru.effts.ine.ejbs.core.VersionableBeanImpl;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemBeanImpl.java 3872 2014-10-22 12:46:44Z DGomon $"
 */
@Stateless(name = "BalanceItemBean", mappedName = "stateless.BalanceItemBean")
@Remote(BalanceItemBean.class)
public class BalanceItemBeanImpl<T extends BalanceItem> extends VersionableBeanImpl<T> implements BalanceItemBean<T> {

    private static final WeakHashMap<SyntheticId, Object> syncMap = new WeakHashMap<>(100);

    @Override
    protected Map getSyncMap() {
        return syncMap;
    }

    @Override
    protected void init() throws GenericSystemException {
        init(BalanceItem.class);
    }

    @Override
    public Collection<T> getOldVersionsByAccId(UserProfile user, BigDecimal accId, Date fd, Date td)
            throws GenericSystemException {
        UserHolder.setUserProfile(user);
        return ((BalanceItemAccess<T>) getAccess()).getOldVersionsByAccId(accId, fd, td);
    }

    @Override
    public BalanceItem getBalanceItemForAccount(
            UserProfile userProfile, BigDecimal accountId) throws GenericSystemException {

        SearchCriteriaProfile profile = getSearchCriteriaProfile(userProfile);

        Set<SearchCriterion> criteria = new LinkedHashSet<>();
        criteria.add(profile.getSearchCriterion(BalanceItem.ACCOUNT_ID, CriteriaRule.equals, accountId));
        Collection<T> items = this.searchObjects(userProfile, criteria);

        if (items.size() == 1) {
            return items.iterator().next();
        } else if (items.isEmpty()) {
            return null;
        }

        throw GenericSystemException.toGeneric(new IneCorruptedVersionableDateException(items.toString()));
    }
}
