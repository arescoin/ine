package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountDelegate.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class AccountDelegate<T extends Account> extends VersionableDelegate<T>
        implements AccountBean<T> {

    public AccountDelegate() {
        init("stateless.AccountBean");
    }
}