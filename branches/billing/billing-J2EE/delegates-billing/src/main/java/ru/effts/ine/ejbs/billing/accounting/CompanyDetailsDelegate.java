package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsDelegate.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class CompanyDetailsDelegate<T extends CompanyDetails> extends VersionableDelegate<T>
        implements CompanyDetailsBean<T> {

    public CompanyDetailsDelegate() {
        init("stateless.CompanyDetailsBean");
    }
}
