package ru.effts.ine.ejbs.billing.accounting;

import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsDelegate.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class BankingDetailsDelegate<T extends BankingDetails> extends VersionableDelegate<T>
        implements BankingDetailsBean<T> {

    public BankingDetailsDelegate() {
        init("stateless.BankingDetailsBean");
    }
}