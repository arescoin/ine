package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceDelegate.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ServicePriceDelegate<T extends ServicePrice> extends VersionableDelegate<T>
        implements ServicePriceBean<T> {

    public ServicePriceDelegate() {
        init("stateless.ServicePriceBean");
    }
}