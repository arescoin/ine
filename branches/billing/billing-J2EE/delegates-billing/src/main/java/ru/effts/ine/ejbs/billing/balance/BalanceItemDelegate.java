package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.core.VersionableDelegate;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemDelegate.java 3872 2014-10-22 12:46:44Z DGomon $"
 */
public class BalanceItemDelegate<T extends BalanceItem> extends VersionableDelegate<T> implements BalanceItemBean<T> {

    public BalanceItemDelegate() {
        init("stateless.BalanceItemBean");
    }

    public Collection<T> getOldVersionsByAccId(UserProfile user, BigDecimal accId, Date fd, Date td)
            throws GenericSystemException {
        //noinspection unchecked
        return ((BalanceItemBean<T>) lookup()).getOldVersionsByAccId(user, accId, fd, td);
    }

    public BalanceItem getBalanceItemForAccount(UserProfile userProfile, BigDecimal accountId)
            throws GenericSystemException {
        //noinspection unchecked
        return ((BalanceItemBean<T>) lookup()).getBalanceItemForAccount(userProfile, accountId);
    }
}
