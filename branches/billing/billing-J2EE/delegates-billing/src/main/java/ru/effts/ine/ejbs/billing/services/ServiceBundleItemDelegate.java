package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemDelegate.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ServiceBundleItemDelegate<T extends ServiceBundleItem> extends VersionableDelegate<T>
        implements ServiceBundleItemBean<T> {

    public ServiceBundleItemDelegate() {
        init("stateless.ServiceBundleItemBean");
    }
}