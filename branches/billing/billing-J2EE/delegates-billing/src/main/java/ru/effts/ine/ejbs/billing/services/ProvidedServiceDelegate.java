package ru.effts.ine.ejbs.billing.services;

import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceDelegate.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class ProvidedServiceDelegate<T extends ProvidedService> extends VersionableDelegate<T>
        implements ProvidedServiceBean<T> {

    public ProvidedServiceDelegate() {
        init("stateless.ProvidedServiceBean");
    }
}