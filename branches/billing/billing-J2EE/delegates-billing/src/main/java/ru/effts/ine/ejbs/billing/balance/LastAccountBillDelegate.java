package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.ejbs.core.VersionableDelegate;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillDelegate.java 3871 2014-10-21 11:00:12Z DGomon $"
 */
public class LastAccountBillDelegate<T extends LastAccountBill> extends VersionableDelegate<T> {
    public LastAccountBillDelegate() {
        init("stateless.LastAccountBillBean");
    }
}
