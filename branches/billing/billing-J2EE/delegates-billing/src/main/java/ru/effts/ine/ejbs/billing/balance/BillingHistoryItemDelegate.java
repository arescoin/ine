package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IneIllegalAccessException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.core.IdentifiableDelegate;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;
import ru.effts.rims.services.StorageFilter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemDelegate.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
public class BillingHistoryItemDelegate<T extends BillingHistoryItem> extends IdentifiableDelegate<T> {

    public BillingHistoryItemDelegate() {
        init("stateless.BillingHistoryItemBean");
    }

    @Override
    public T updateObject(UserProfile user, T newIdentifiable) throws GenericSystemException {
        throw new IneIllegalAccessException("Operation not allowed");
    }

    @Override
    public void deleteObject(UserProfile user, T identifiable) throws GenericSystemException {
        throw new IneIllegalAccessException("Operation not allowed");
    }

    @Override
    public int getObjectCount(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return -1;
    }

    @Override
    public Collection searchObjects(UserProfile user, Set<SearchCriterion> searchCriteria, String fieldName)
            throws GenericSystemException {

        return new ArrayList();
    }

    @Override
    public boolean containsObjects(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        return false;
    }

    @Override
    public boolean containsObjects(UserProfile user, Set<SearchCriterion> searchCriteria) throws GenericSystemException {
        return false;
    }

    @Override
    public Collection searchObjects(UserProfile user, StorageFilter[] storageFilters) throws GenericSystemException {
        return new ArrayList();
    }

    @Override
    public int getObjectCount(UserProfile user, StorageFilter[] filters) throws GenericSystemException {
        return -1;
    }

}
