package ru.effts.ine.ejbs.billing.balance;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemDelegateTest.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
public class BillingHistoryItemDelegateTest<T extends BillingHistoryItem> extends BaseDelegateTest {
    @Override
    protected void testDelegate() {
        try {

            BillingHistoryItem item = IdentifiableFactory.getImplementation(BillingHistoryItem.class);
            item.setAccountId(BigDecimal.ONE);
            item.setServiceId(BigDecimal.ONE);
            item.setBillingDate(new Date());

            //noinspection unchecked
            BillingHistoryItemDelegate<BillingHistoryItem> bean = DelegateFactory.obtainDelegateByInterface(BillingHistoryItem.class);
            item = bean.createObject(user, item);

            item = bean.getObjectById(user, item.getCoreId());

            SearchCriteriaProfile profile = bean.getSearchCriteriaProfile(user);
            SearchCriterion criterion = profile.getSearchCriterion(
                    BillingHistoryItem.ACCOUNT_ID, CriteriaRule.equals, item.getAccountId());
            Set<SearchCriterion> search = new LinkedHashSet<SearchCriterion>(2);
            search.add(criterion);
            bean.searchObjects(user, search);

        } catch (Exception e) {
            fail(e);
        }
    }
}
