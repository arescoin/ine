package ru.effts.ine.ejbs.billing.accounting;

import org.junit.Assert;
import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountDelegateTest.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public class AccountDelegateTest extends BaseDelegateTest {

    @Override
    protected void testDelegate() {
        try {

            final Account account = IdentifiableFactory.getImplementation(Account.class);
            account.setPriceListId(BigDecimal.ONE);
            account.setClientType(BigDecimal.ONE);
            account.setClientId(new BigDecimal(2));
            account.setDiscount(new BigDecimal(.95));

            AccountDelegate<Account> delegate = DelegateFactory.obtainDelegateByInterface(Account.class);

            final Account account2 = delegate.createObject(user, account);

            Assert.assertEquals(account.getDiscount(), account2.getDiscount());
            Assert.assertEquals(account.getPriceListId(), account2.getPriceListId());

            final Account account3 = delegate.getObjectById(user, account2.getCoreId());

            Assert.assertEquals(account2.getDiscount(), account3.getDiscount());
            Assert.assertEquals(account2.getPriceListId(), account3.getPriceListId());

            account3.setDiscount(BigDecimal.ONE);

            final Account account4 = delegate.updateObject(user, account3);

            Assert.assertEquals(account3.getDiscount(), account4.getDiscount());
            Assert.assertEquals(account3.getPriceListId(), account4.getPriceListId());

            //удаление
            delegate.deleteObject(user, account4);

            if (delegate.getObjectById(user, account4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }
    }
}
