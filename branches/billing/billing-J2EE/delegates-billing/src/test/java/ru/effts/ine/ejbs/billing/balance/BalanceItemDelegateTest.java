package ru.effts.ine.ejbs.billing.balance;

import org.junit.Assert;
import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemDelegateTest.java 3864 1970-01-01 00:00:00Z DGomon $"
 */
public class BalanceItemDelegateTest extends BaseDelegateTest {


    @Override
    protected void testDelegate() {
        try {

            BalanceItem obj1 = IdentifiableFactory.getImplementation(BalanceItem.class);
            obj1.setAccountId(BigDecimal.ONE);
            obj1.setAddition(true);
            obj1.setAmount(new BigDecimal(2));
            obj1.setBalance(new BigDecimal(3));
            obj1.setReasonId(new BigDecimal(5));
            obj1.setReasonType(BigDecimal.TEN);
            obj1.setDate(new Date());
            obj1.setCoreDsc("TestObject (BalanceItem) for CRUD-operations testing");
            obj1.setCoreFd(FieldAccess.getSystemFd());
            obj1.setCoreTd(FieldAccess.getSystemTd());

            BalanceItemDelegate<BalanceItem> delegate = DelegateFactory.obtainDelegateByInterface(BalanceItem.class);

            BalanceItem obj2 = delegate.createObject(user, obj1);

            Assert.assertEquals("Failed to save new object in DB", obj1.getAccountId(), obj2.getAccountId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getAmount(), obj2.getAmount());
            Assert.assertEquals("Failed to save new object in DB", obj1.getBalance(), obj2.getBalance());
            Assert.assertEquals("Failed to save new object in DB", obj1.getDate(), obj2.getDate());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonId(), obj2.getReasonId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonType(), obj2.getReasonType());

            obj1 = obj2;

            obj2 = delegate.getObjectById(user, obj1.getCoreId());

            Assert.assertEquals("Failed to save new object in DB", obj1.getAccountId(), obj2.getAccountId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getAmount(), obj2.getAmount());
            Assert.assertEquals("Failed to save new object in DB", obj1.getBalance(), obj2.getBalance());
            Assert.assertEquals("Failed to save new object in DB", obj1.getDate(), obj2.getDate());
            Assert.assertEquals("Failed to save new object in DB", obj1.getCoreDsc(), obj2.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonId(), obj2.getReasonId());
            Assert.assertEquals("Failed to save new object in DB", obj1.getReasonType(), obj2.getReasonType());


            obj2.setAccountId(BigDecimal.ONE);
            obj2.setAddition(false);
            obj2.setAmount(new BigDecimal(1));
            obj2.setBalance(new BigDecimal(2));
            obj2.setReasonId(new BigDecimal(3));
            obj2.setReasonType(new BigDecimal(4));
            obj2.setDate(new Date());
            obj2.setCoreDsc("TestObject 2 (BalanceItem) for CRUD-operations testing");
            obj2.setCoreFd(obj1.getCoreFd());
            obj2.setCoreTd(obj1.getCoreTd());

            BalanceItem obj3 = delegate.updateObject(user, obj2);

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.add(Calendar.MONTH, -2);

            delegate.getOldVersions(user, obj3);
            delegate.getOldVersions(user, obj3, calendar.getGregorianChange(), new Date());

            Assert.assertEquals("Failed to save new object in DB", obj2.getAccountId(), obj3.getAccountId());
            Assert.assertEquals("Failed to save new object in DB", obj2.getAmount(), obj3.getAmount());
            Assert.assertEquals("Failed to save new object in DB", obj2.getBalance(), obj3.getBalance());
            Assert.assertEquals("Failed to save new object in DB", obj2.getDate(), obj3.getDate());
            Assert.assertEquals("Failed to save new object in DB", obj2.getCoreDsc(), obj3.getCoreDsc());
            Assert.assertEquals("Failed to save new object in DB", obj2.getReasonId(), obj3.getReasonId());
            Assert.assertEquals("Failed to save new object in DB", obj2.getReasonType(), obj3.getReasonType());

            delegate.deleteObject(user, obj3);


            delegate.getOldVersionsByAccId(user, BigDecimal.ONE, calendar.getTime(), new Date());

            if (delegate.getObjectById(user, obj3.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove BalanceItem");
            }
        } catch (Exception e) {
            fail(e);
        }
    }
}
