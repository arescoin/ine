package ru.effts.ine.ejbs.billing.services;

import org.junit.Assert;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.ejbs.BaseDelegateTest;
import ru.effts.ine.ejbs.DelegateFactory;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceDelegateTest.java 3873 2014-10-23 13:19:10Z DGomon $"
 */
public class ProvidedServiceDelegateTest extends BaseDelegateTest {

    @Override
    protected void testDelegate() {
        try {
            final ProvidedService object = IdentifiableFactory.getImplementation(ProvidedService.class);
            object.setAccountId(BigDecimal.ONE);
            object.setServiceBundleId(new BigDecimal(2));
            object.setParentId(BigDecimal.ONE);
            object.setStatus(BigDecimal.ONE);
            object.setServiceName("234234");

            ProvidedServiceDelegate<ProvidedService> bean =
                    DelegateFactory.obtainDelegateByInterface(ProvidedService.class);

            final ProvidedService object2 = bean.createObject(user, object);

            Assert.assertEquals(object.getAccountId(), object2.getAccountId());
            Assert.assertEquals(object.getServiceBundleId(), object2.getServiceBundleId());
            Assert.assertEquals(object.getParentId(), object2.getParentId());
            Assert.assertEquals(object.getStatus(), object2.getStatus());
            Assert.assertEquals(object.getServiceName(), object2.getServiceName());

            final ProvidedService object3 = bean.getObjectById(user, object2.getCoreId());

            Assert.assertEquals(object2.getAccountId(), object3.getAccountId());
            Assert.assertEquals(object2.getServiceBundleId(), object3.getServiceBundleId());
            Assert.assertEquals(object2.getParentId(), object3.getParentId());
            Assert.assertEquals(object2.getStatus(), object3.getStatus());
            Assert.assertEquals(object2.getServiceName(), object3.getServiceName());

            object3.setStatus(new BigDecimal(2));
            object3.setServiceName("23423422");

            ProvidedService object4 = bean.updateObject(user, object3);

            Assert.assertEquals(object3.getAccountId(), object4.getAccountId());
            Assert.assertEquals(object3.getServiceBundleId(), object4.getServiceBundleId());
            Assert.assertEquals(object3.getParentId(), object4.getParentId());
            Assert.assertEquals(object3.getStatus(), object4.getStatus());
            Assert.assertEquals(object3.getServiceName(), object4.getServiceName());

            object4.setStatus(new BigDecimal(1));
            object4.setServiceName("23423423");

            object4 = bean.updateObject(user, object4);

            //удаление
            bean.deleteObject(user, object4);

            if (bean.getObjectById(user, object4.getCoreId()) != null) {
                Assert.fail("method deleteObject() has failed to remove Account");
            }

        } catch (Exception e) {
            fail(e);
        }
    }
}
