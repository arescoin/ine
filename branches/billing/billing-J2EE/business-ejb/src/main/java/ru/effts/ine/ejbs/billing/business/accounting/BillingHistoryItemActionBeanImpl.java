package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.billing.balance.BillingHistoryItemBean;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemActionBeanImpl.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
@Stateless(name = "BillingHistoryItemActionBean", mappedName = "stateless.BillingHistoryItemActionBean")
@Remote(BillingHistoryItemActionBean.class)
public class BillingHistoryItemActionBeanImpl {

    @EJB(mappedName = "stateless.BillingHistoryItemBean", name = "BillingHistoryItemBean")
    BillingHistoryItemBean<BillingHistoryItem> billingHistoryItemBean;


    public BillingHistoryItem getHistoryById(UserProfile user, BigDecimal id) throws GenericSystemException {
        return billingHistoryItemBean.getObjectById(user);
    }


    public Collection<BillingHistoryItem> getHistoryByAccId(UserProfile user, BigDecimal accId, Date fd, Date td)
            throws GenericSystemException {

        SearchCriteriaProfile profile = billingHistoryItemBean.getSearchCriteriaProfile(user);
        Set<SearchCriterion> search = new LinkedHashSet<>(4);
        SearchCriterion accCrit = profile.getSearchCriterion(
                BillingHistoryItem.ACCOUNT_ID, CriteriaRule.equals, accId);
        search.add(accCrit);
        SearchCriterion datCrit = profile.getSearchCriterion(
                BillingHistoryItem.BILLING_DATE, CriteriaRule.between, fd, td);
        search.add(datCrit);

        return billingHistoryItemBean.searchObjects(user, search);
    }


    public Collection<BillingHistoryItem> getHistoryBySrvId(UserProfile user, BigDecimal srvId, Date fd, Date td)
            throws GenericSystemException {

        SearchCriteriaProfile profile = billingHistoryItemBean.getSearchCriteriaProfile(user);
        Set<SearchCriterion> search = new LinkedHashSet<>(4);
        SearchCriterion accCrit = profile.getSearchCriterion(
                BillingHistoryItem.SERVICE_ID, CriteriaRule.equals, srvId);
        search.add(accCrit);
        SearchCriterion datCrit = profile.getSearchCriterion(
                BillingHistoryItem.BILLING_DATE, CriteriaRule.between, fd, td);
        search.add(datCrit);

        return billingHistoryItemBean.searchObjects(user, search);
    }

}
