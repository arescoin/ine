package ru.effts.ine.ejbs.billing.business;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.*;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.core.userpermits.SystemUser;
import ru.effts.ine.ejbs.billing.accounting.AccountBean;
import ru.effts.ine.ejbs.billing.accounting.BankingDetailsBean;
import ru.effts.ine.ejbs.billing.accounting.CompanyDetailsBean;
import ru.effts.ine.ejbs.billing.services.ServiceBundleItemBean;
import ru.effts.ine.ejbs.billing.services.ServicePriceBean;
import ru.effts.ine.ejbs.billing.services.ServiceSpecBean;
import ru.effts.ine.ejbs.core.dic.DictionaryTermBean;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.*;


/** Класс представлющий собой stateless ejb. Реализует {@link ru.effts.ine.ejbs.billing.business.BusinessActions} */
@SuppressWarnings("EjbEnvironmentInspection")
@Stateless(name = "BusinessActions", mappedName = "stateless.BusinessActions")
@Remote(BusinessActions.class)
public class BusinessActionsBean implements BusinessActions {

    private static final BigDecimal SERVICE_PRICE_DICTIONARY_ID = new BigDecimal(43);
    private static final BigDecimal SERVICE_BUNDLE_DICTIONARY_ID = new BigDecimal(42);
    private static final BigDecimal CLIENT_TYPE_DICTIONARY_ID = new BigDecimal(44);

    @EJB(mappedName = "stateless.Account", name = "AccountBean")
    AccountBean accountBean;
    @EJB(mappedName = "stateless.BankingDetails", name = "BankingDetails")
    BankingDetailsBean bankingDetailsBean;
    @EJB(mappedName = "stateless.CompanyDetails", name = "CompanyDetails")
    CompanyDetailsBean companyDetailsBean;
    @EJB(mappedName = "stateless.ServicePrice", name = "ServicePrice")
    ServicePriceBean servicePriceBean;
    @EJB(name = "ServiceBundleItem", mappedName = "stateless.ServiceBundleItemBean")
    ServiceBundleItemBean serviceBundleItemBean;
    @EJB(name = "ServiceSpec", mappedName = "stateless.ServiceSpec")
    ServiceSpecBean serviceSpecBean;
    @EJB(name = "DictionaryTermBean", mappedName = "stateless.DictionaryTermBean")
    DictionaryTermBean dictionaryTermBean;


    private static final SystemUser systemUser;

    static {

        /** Заглушка пользователя для прохождения автотестов */
        systemUser = new SystemUser() {
            @Override
            public Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            @Override
            public boolean isActive() {
                return true;
            }

            @Override
            public void setActive(boolean active) {
            }

            @Override
            public void setCoreFd(Date fd) throws IneIllegalArgumentException {
            }

            @Override
            public Date getCoreFd() {
                return new Date();
            }

            @Override
            public void setCoreTd(Date td) throws IneIllegalArgumentException {
            }

            @Override
            public Date getCoreTd() {
                return new Date();
            }

            @Override
            public void setCoreId(BigDecimal id) throws IllegalIdException {
            }

            @Override
            public BigDecimal getCoreId() {
                return BigDecimal.ONE;
            }

            @Override
            public String getCoreDsc() {
                return "business user";
            }

            @Override
            public void setCoreDsc(String dsc) {
            }

            @Override
            public String getLogin() {
                return "root";
            }

            @Override
            public void setLogin(String login) {
            }

            @Override
            public String getMd5() {
                return null;
            }

            @Override
            public void setMd5(String md5) {
            }
        };

    }

    private UserProfile getDefaultUser() {
        //todo: костыль
        UserHolder.setUser(systemUser);
        UserHolder.getUser().setCurrentRole(BigDecimal.ONE);
        UserProfile user = UserHolder.getUser();
        user.setLastAction(new UserProfile.Action(null, " business user"));
        return user;
    }

    @Override
    public String createPersonAccount() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String createCompanyAccount(Map<String, String> attributes) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Account getAccountInfo(BigDecimal accountId) throws GenericSystemException {
        return (Account) accountBean.getObjectById(getDefaultUser(), accountId);
    }

    @Override
    public ServicePrice getServicePriceInfo(BigDecimal id) throws GenericSystemException {
        return (ServicePrice) servicePriceBean.getObjectById(getDefaultUser(), id);
    }

    @Override
    public ServiceBundleItem getServiceBundleItemInfo(BigDecimal id) throws GenericSystemException {
        return (ServiceBundleItem) serviceBundleItemBean.getObjectById(getDefaultUser(), id);
    }

    @Override
    public ServiceSpec getServiceSpecInfo(BigDecimal id) throws GenericSystemException {
        return (ServiceSpec) serviceSpecBean.getObjectById(getDefaultUser(), id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getServicePriceDescription(BigDecimal id) throws GenericSystemException {

        Collection<DictionaryTerm> terms = dictionaryTermBean
                .getAllTermsForDictionaryAndEntry(getDefaultUser(), SERVICE_PRICE_DICTIONARY_ID, id);
        if (terms.size() != 1) {
            throw new GenericSystemException(
                    "A DictionaryTerm is not found for ServicePrice and id=" + id.toPlainString());
        }
        return terms.iterator().next().getTerm();
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getServiceBundleItemDescription(BigDecimal id) throws GenericSystemException {
        Collection<DictionaryTerm> terms = dictionaryTermBean
                .getAllTermsForDictionaryAndEntry(getDefaultUser(), SERVICE_BUNDLE_DICTIONARY_ID, id);
        if (terms.size() != 1) {
            throw new GenericSystemException(
                    "A DictionaryTerm is not found for ServiceBundleItem and id=" + id.toPlainString());
        }
        return terms.iterator().next().getTerm();
    }

    @Override
    @SuppressWarnings("unchecked")
    public String getClientTypeDescription(BigDecimal id) throws GenericSystemException {
        Collection<DictionaryTerm> terms = dictionaryTermBean
                .getAllTermsForDictionaryAndEntry(getDefaultUser(), CLIENT_TYPE_DICTIONARY_ID, id);
        if (terms.size() != 1) {
            throw new GenericSystemException(
                    "A DictionaryTerm is not found for ClientType and id=" + id.toPlainString());
        }
        return terms.iterator().next().getTerm();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<BankingDetails> getBankingDetailsForAccount(BigDecimal accountId) throws GenericSystemException {

        SearchCriteriaProfile profile = bankingDetailsBean.getSearchCriteriaProfile(getDefaultUser());
        Set<SearchCriterion> critera = new LinkedHashSet<>();
        critera.add(profile.getSearchCriterion(BankingDetails.ACCOUNT_ID, CriteriaRule.equals, accountId));

        return bankingDetailsBean.searchObjects(getDefaultUser(), critera);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<CompanyDetails> getCompanyDetailsForAccount(BigDecimal accountId) throws GenericSystemException {

        SearchCriteriaProfile profile = companyDetailsBean.getSearchCriteriaProfile(getDefaultUser());
        Set<SearchCriterion> critera = new LinkedHashSet<SearchCriterion>();
        critera.add(profile.getSearchCriterion(CompanyDetails.ACCOUNT_ID, CriteriaRule.equals, accountId));

        return companyDetailsBean.searchObjects(getDefaultUser(), critera);
    }
}
