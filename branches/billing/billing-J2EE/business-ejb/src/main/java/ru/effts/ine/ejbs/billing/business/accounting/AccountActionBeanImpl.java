package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.ejbs.billing.accounting.AccountBean;
import ru.effts.ine.ejbs.billing.accounting.BankingDetailsBean;
import ru.effts.ine.ejbs.billing.accounting.CompanyDetailsBean;
import ru.effts.ine.ejbs.billing.services.ProvidedServiceBean;
import ru.effts.ine.ejbs.billing.services.ServicePriceBean;
import ru.effts.ine.ejbs.core.dic.DictionaryTermBean;
import ru.effts.ine.ejbs.core.structure.SystemObjectBean;
import ru.effts.ine.ejbs.oss.entity.CompanyBean;
import ru.effts.ine.ejbs.oss.entity.PersonBean;
import ru.effts.ine.oss.entity.Company;
import ru.effts.ine.oss.entity.Person;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountActionBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "AccountActionBean", mappedName = "stateless.AccountActionBean")
@Remote(AccountActionBean.class)
public class AccountActionBeanImpl implements AccountActionBean {


    @EJB(mappedName = "stateless.SystemObjectBean", name = "SystemObjectBean")
    SystemObjectBean systemObjectBean;

    @EJB(mappedName = "stateless.AccountBean", name = "AccountBean")
    AccountBean<Account> accountBean;

    @EJB(mappedName = "stateless.BankingDetailsBean", name = "BankingDetailsBean")
    BankingDetailsBean bankingDetailsBean;

    @EJB(mappedName = "stateless.CompanyDetailsBean", name = "CompanyDetailsBean")
    CompanyDetailsBean<CompanyDetails> companyDetailsBean;

    @EJB(mappedName = "stateless.CompanyBean", name = "CompanyBean")
    CompanyBean<Company> companyBean;

    @EJB(mappedName = "stateless.PersonBean", name = "PersonBean")
    PersonBean<Person> personBean;

    @EJB(mappedName = "stateless.ProvidedServiceBean", name = "ProvidedServiceBean")
    ProvidedServiceBean<ProvidedService> providedServiceBean;

    @EJB(mappedName = "stateless.ServicePriceBean", name = "ServicePriceBean")
    ServicePriceBean<ServicePrice> servicePriceBean;

    @EJB(mappedName = "stateless.DictionaryTermBean", name = "DictionaryTermBean")
    DictionaryTermBean<DictionaryTerm> dictionaryTermBean;


    @Override
    public Map<String, Object> getAccountById(UserProfile user, BigDecimal accountId, HashSet<String> objectTypes)
            throws GenericSystemException {

        return new AccountRoutine(systemObjectBean, accountBean, bankingDetailsBean, companyDetailsBean, companyBean,
                personBean, providedServiceBean, servicePriceBean, dictionaryTermBean
        ).getAccountById(user, accountId, objectTypes);
    }

    @Override
    public Account createAccount(UserProfile user, Account account) throws GenericSystemException {
        return accountBean.createObject(user, account);
    }

    @Override
    public Account updateAccount(UserProfile user, Account account) throws GenericSystemException {
        return accountBean.updateObject(user, account);
    }

    @Override
    public void deleteAccount(UserProfile user, Long accountId) throws GenericSystemException {
        accountBean.deleteObject(user, accountBean.getObjectById(user, new BigDecimal(accountId)));
    }

}
