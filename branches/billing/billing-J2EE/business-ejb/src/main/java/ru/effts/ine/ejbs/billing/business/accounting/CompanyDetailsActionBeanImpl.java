package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.billing.accounting.CompanyDetailsBean;
import ru.effts.ine.ejbs.oss.entity.CompanyBean;
import ru.effts.ine.oss.entity.Company;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsActionBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "CompanyDetailsActionBean", mappedName = "stateless.CompanyDetailsActionBean")
@Remote(CompanyDetailsActionBean.class)
public class CompanyDetailsActionBeanImpl implements CompanyDetailsActionBean {

    @EJB(mappedName = "stateless.CompanyDetailsBean", name = "CompanyDetailsBean")
    CompanyDetailsBean<CompanyDetails> companyDetailsBean;

    @EJB(mappedName = "stateless.CompanyBean", name = "CompanyBean")
    CompanyBean<Company> companyBean;

    @Override
    public Map<String, Object> getById(UserProfile user, BigDecimal accountId, boolean getCompany)
            throws GenericSystemException {

        Map<String, Object> result = new HashMap<>();

        CompanyDetails details = companyDetailsBean.getObjectById(user, accountId);
        result.put(CompanyDetails.class.getName(), details);

        if (getCompany && null != details) {
            result.put(Company.class.getName(), companyBean.getObjectById(user, details.getCompanyId()));
        }

        return result;
    }

    @Override
    public CompanyDetails create(UserProfile user, CompanyDetails details) throws GenericSystemException {
        return companyDetailsBean.createObject(user, details);
    }

    @Override
    public CompanyDetails update(UserProfile user, CompanyDetails details) throws GenericSystemException {
        return companyDetailsBean.updateObject(user, details);
    }

    @Override
    public void delete(UserProfile user, Long detailsId) throws GenericSystemException {
        companyDetailsBean.deleteObject(user, companyDetailsBean.getObjectById(user, new BigDecimal(detailsId)));
    }

}
