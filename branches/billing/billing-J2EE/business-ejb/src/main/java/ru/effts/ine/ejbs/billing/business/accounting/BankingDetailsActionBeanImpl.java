package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.billing.accounting.AccountBean;
import ru.effts.ine.ejbs.billing.accounting.BankingDetailsBean;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsActionBeanImpl.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
@Stateless(name = "BankingDetailsActionBean", mappedName = "stateless.BankingDetailsActionBean")
@Remote(BankingDetailsActionBean.class)
public class BankingDetailsActionBeanImpl implements BankingDetailsActionBean {


//    @EJB(mappedName = "stateless.SystemObjectBean", name = "SystemObjectBean")
//    SystemObjectBean systemObjectBean;

    @EJB(mappedName = "stateless.AccountBean", name = "AccountBean")
    AccountBean<Account> accountBean;

    @EJB(mappedName = "stateless.BankingDetailsBean", name = "BankingDetailsBean")
    BankingDetailsBean<BankingDetails> bankingDetailsBean;


    @Override
    public Map<String, Object> getById(UserProfile user, BigDecimal accountId, boolean byAccount)
            throws GenericSystemException {

        Map<String, Object> map = new HashMap<>();
        if (!byAccount) {
            ArrayList<BankingDetails> details = new ArrayList<>();
            details.add(bankingDetailsBean.getObjectById(user, accountId));
            map.put(BankingDetails.class.getName(), details);
        } else {
            SearchCriteriaProfile profile = bankingDetailsBean.getSearchCriteriaProfile(user);
            Set<SearchCriterion> criteria = new LinkedHashSet<>();
            criteria.add(profile.getSearchCriterion(BankingDetails.ACCOUNT_ID, CriteriaRule.equals, accountId));
            map.put(BankingDetails.class.getName(), bankingDetailsBean.searchObjects(user, criteria));
        }

        return map;
    }

    @Override
    public BankingDetails create(UserProfile user, BankingDetails bankingDetails)
            throws GenericSystemException {
        return bankingDetailsBean.createObject(user, bankingDetails);
    }

    @Override
    public BankingDetails update(UserProfile user, BankingDetails bankingDetails)
            throws GenericSystemException {
        return bankingDetailsBean.updateObject(user, bankingDetails);
    }

    @Override
    public void delete(UserProfile user, Long detailsId)
            throws GenericSystemException {
        bankingDetailsBean.deleteObject(user, bankingDetailsBean.getObjectById(user, new BigDecimal(detailsId)));
    }

}
