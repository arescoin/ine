package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.billing.balance.BalanceItemBean;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemActionBeanImpl.java 3872 2014-10-22 12:46:44Z DGomon $"
 */
@Stateless(name = "BalanceItemActionBean", mappedName = "stateless.BalanceItemActionBean")
@Remote(BalanceItemActionBean.class)
public class BalanceItemActionBeanImpl implements BalanceItemActionBean {

    @EJB(mappedName = "stateless.BalanceItemBean", name = "BalanceItemBean")
    BalanceItemBean<BalanceItem> balanceItemBean;


    @Override
    public BalanceItem getBalanceItemForAccount(
            UserProfile userProfile, BigDecimal accountId) throws GenericSystemException {

        return balanceItemBean.getBalanceItemForAccount(userProfile, accountId);
    }

    @Override
    public Collection<BalanceItem> getBalanceHistoryForAccount(
            UserProfile userProfile, BigDecimal accountId, Date from, Date to) throws GenericSystemException {

        return balanceItemBean.getOldVersionsByAccId(userProfile, accountId, from, to);
    }
}
