package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.ejbs.billing.balance.LastAccountBillBean;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import java.math.BigDecimal;
import java.util.Collection;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillActionBeanImpl.java 3871 2014-10-21 11:00:12Z DGomon $"
 */

@Stateless(name = "LastAccountBillActionBean", mappedName = "stateless.LastAccountBillActionBean")
@Remote(LastAccountBillActionBean.class)
public class LastAccountBillActionBeanImpl implements LastAccountBillActionBean {

    @EJB(mappedName = "stateless.LastAccountBillBean", name = "LastAccountBillBean")
    LastAccountBillBean<LastAccountBill> lastAccountBillBean;


    @Override
    public LastAccountBill getByAccId(UserProfile user, BigDecimal accountId) throws GenericSystemException {
        return lastAccountBillBean.getObjectById(user, accountId);
    }

    @Override
    public Collection<LastAccountBill> getOldBillsForAcc(UserProfile user, BigDecimal accountId)
            throws GenericSystemException {
        return lastAccountBillBean.getOldVersions(user, getByAccId(user, accountId));
    }
}
