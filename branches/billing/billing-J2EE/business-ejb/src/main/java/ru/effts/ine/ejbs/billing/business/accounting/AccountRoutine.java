package ru.effts.ine.ejbs.billing.business.accounting;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.billing.accounting.CompanyDetails;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.UserProfile;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.ejbs.billing.accounting.AccountBean;
import ru.effts.ine.ejbs.billing.accounting.BankingDetailsBean;
import ru.effts.ine.ejbs.billing.accounting.CompanyDetailsBean;
import ru.effts.ine.ejbs.billing.services.ProvidedServiceBean;
import ru.effts.ine.ejbs.billing.services.ServicePriceBean;
import ru.effts.ine.ejbs.core.dic.DictionaryTermBean;
import ru.effts.ine.ejbs.core.structure.SystemObjectBean;
import ru.effts.ine.ejbs.oss.entity.CompanyBean;
import ru.effts.ine.ejbs.oss.entity.PersonBean;
import ru.effts.ine.oss.entity.Company;
import ru.effts.ine.oss.entity.Person;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountRoutine.java 3859 2014-09-15 13:08:28Z DGomon $"
 */
public class AccountRoutine {


    public static final BigDecimal SB_DIC = new BigDecimal(42);
    public static final BigDecimal PL_DIC = new BigDecimal(43);
    public static final BigDecimal CT_DIC = new BigDecimal(44);

    private static ArrayList<String> types = new ArrayList<>();

    SystemObjectBean systemObjectBean;

    AccountBean accountBean;

    BankingDetailsBean bankingDetailsBean;

    CompanyDetailsBean companyDetailsBean;

    CompanyBean<Company> companyBean;

    PersonBean<Person> personBean;

    ProvidedServiceBean<ProvidedService> providedServiceBean;

    ServicePriceBean<ServicePrice> servicePriceBean;

    DictionaryTermBean<DictionaryTerm> dictionaryTermBean;

    static {
        types.add(BankingDetails.class.getName());
        types.add(CompanyDetails.class.getName());
        types.add(ProvidedService.class.getName());
        types.add(ServicePrice.class.getName());
    }

    public AccountRoutine(SystemObjectBean systemObjectBean,
                          AccountBean accountBean,
                          BankingDetailsBean bankingDetailsBean,
                          CompanyDetailsBean<CompanyDetails> companyDetailsBean,
                          CompanyBean<Company> companyBean,
                          PersonBean<Person> personBean,
                          ProvidedServiceBean<ProvidedService> providedServiceBean,
                          ServicePriceBean<ServicePrice> servicePriceBean,
                          DictionaryTermBean<DictionaryTerm> dictionaryTermBean) {

        this.systemObjectBean = systemObjectBean;
        this.accountBean = accountBean;
        this.bankingDetailsBean = bankingDetailsBean;
        this.companyDetailsBean = companyDetailsBean;
        this.companyBean = companyBean;
        this.personBean = personBean;
        this.providedServiceBean = providedServiceBean;
        this.servicePriceBean = servicePriceBean;
        this.dictionaryTermBean = dictionaryTermBean;
    }

    public Map<String, Object> getAccountById(UserProfile user, BigDecimal accountId, HashSet<String> objectTypes)
            throws GenericSystemException {

        HashMap<String, Object> result = new HashMap<>();
        HashMap<BigDecimal, Account> accMap = new HashMap<>();

        Account account = (Account) accountBean.getObjectById(user, accountId);

        if (null == account) {
            return null;
        }

        accMap.put(account.getCoreId(), account);
        result.put(Account.class.getName(), accMap);

// pricelist
        HashMap<BigDecimal, Collection<DictionaryTerm>> dicHashMap = new HashMap<>();
        dicHashMap.put(PL_DIC, dictionaryTermBean.getAllTermsForDictionaryAndEntry(
                user, PL_DIC, account.getPriceListId()));

// clientType
        dicHashMap.put(CT_DIC, dictionaryTermBean.getAllTermsForDictionaryAndEntry(
                user, CT_DIC, account.getClientType()));

        result.put(DictionaryTerm.class.getName(), dicHashMap);

        if (null != objectTypes && !objectTypes.isEmpty()) {
            processTypes(user, objectTypes, account, result);
        }

        return result;
    }

    private void processTypes(UserProfile user, HashSet<String> objectTypes, Account account,
                              HashMap<String, Object> resultMap) throws GenericSystemException {

        Set ifsSet = systemObjectBean.getSupportedIntefaces(user);

        for (String objectType : objectTypes) {
            if (ifsSet.contains(objectType)) {
                processType(user, objectType, account, resultMap);
            }
        }

    }

    private void processType(UserProfile user, String objectType, Account account,
                             HashMap<String, Object> resultMap) throws GenericSystemException {

        int index = types.indexOf(objectType);

        if (index > -1) {
            switch (index) {
                case 0:
                    getBankingDetails(user, account.getCoreId(), resultMap);
                    break;
                case 1:
                    if (account.getClientType().equals(BigDecimal.ONE)) {
                        getCompanyDetails(user, account.getCoreId(), resultMap);
                    } else if (account.getClientType().equals(new BigDecimal(2))) {
                        getPerson(user, account.getClientId(), resultMap);
                    }
                    break;
                case 2:
                    getProvidedService(user, account.getCoreId(), resultMap);
                    break;
                case 3:
                    getServicePrice(user, account.getPriceListId(), resultMap);
                    break;
                default:
            }
        }

    }

    private void getBankingDetails(UserProfile user, BigDecimal accountId,
                                   HashMap<String, Object> resultMap) throws GenericSystemException {
        SearchCriteriaProfile profile = bankingDetailsBean.getSearchCriteriaProfile(user);
        Set<SearchCriterion> criteria = new LinkedHashSet<>();
        criteria.add(profile.getSearchCriterion(BankingDetails.ACCOUNT_ID, CriteriaRule.equals, accountId));

        resultMap.put(BankingDetails.class.getName(), bankingDetailsBean.searchObjects(user, criteria));
    }

    private void getCompanyDetails(UserProfile user, BigDecimal accountId,
                                   HashMap<String, Object> resultMap) throws GenericSystemException {
        SearchCriteriaProfile profile = companyDetailsBean.getSearchCriteriaProfile(user);
        Set<SearchCriterion> criteria = new LinkedHashSet<>();
        criteria.add(profile.getSearchCriterion(CompanyDetails.ACCOUNT_ID, CriteriaRule.equals, accountId));

        Collection<CompanyDetails> companyDetailz = companyDetailsBean.searchObjects(user, criteria);
        resultMap.put(CompanyDetails.class.getName(), companyDetailz);

        if (!companyDetailz.isEmpty()) {
            Map<BigDecimal, Company> companyMap = new HashMap<>();
            for (CompanyDetails companyDetails : companyDetailz) {
                Company company = companyBean.getObjectById(user, companyDetails.getCompanyId());
                companyMap.put(company.getCoreId(), company);
            }

            resultMap.put(Company.class.getName(), companyMap);

        }
    }

    private void getPerson(UserProfile user, BigDecimal personId,
                           HashMap<String, Object> resultMap) throws GenericSystemException {

        Person person = personBean.getObjectById(user, personId);

        Map<BigDecimal, Identifiable> personMap = new HashMap<>();
        personMap.put(person.getCoreId(), person);
        resultMap.put(Person.class.getName(), personMap);

    }

    private void getProvidedService(UserProfile user, BigDecimal accountId,
                                    HashMap<String, Object> resultMap) throws GenericSystemException {
        SearchCriteriaProfile profile = providedServiceBean.getSearchCriteriaProfile(user);
        Set<SearchCriterion> criteria = new LinkedHashSet<>();
        criteria.add(profile.getSearchCriterion(ProvidedService.ACCOUNT_ID, CriteriaRule.equals, accountId));
        Collection<ProvidedService> services = providedServiceBean.searchObjects(user, criteria);
        resultMap.put(ProvidedService.class.getName(), services);

        if (null != services && !services.isEmpty()) {
            HashMap<BigDecimal, Collection<DictionaryTerm>> dicHashMap =
                    (HashMap<BigDecimal, Collection<DictionaryTerm>>) resultMap.get(DictionaryTerm.class.getName());
            dicHashMap.put(SB_DIC, dictionaryTermBean.getAllTermsForDictionaryAndEntry(
                    user, SB_DIC, services.iterator().next().getServiceBundleId()));
        }
    }

    private void getServicePrice(UserProfile user, BigDecimal priceListId,
                                 HashMap<String, Object> resultMap) throws GenericSystemException {
        SearchCriteriaProfile profile = servicePriceBean.getSearchCriteriaProfile(user);
        Set<SearchCriterion> criteria = new LinkedHashSet<>();
        criteria.add(profile.getSearchCriterion(ServicePrice.PRICE_LIST_ID, CriteriaRule.equals, priceListId));
        resultMap.put(ServicePrice.class.getName(), servicePriceBean.searchObjects(user, criteria));
    }


}
