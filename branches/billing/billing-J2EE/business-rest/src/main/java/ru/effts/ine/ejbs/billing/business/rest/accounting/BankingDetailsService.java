package ru.effts.ine.ejbs.billing.business.rest.accounting;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.billing.accounting.BankingDetails;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.accounting.BankingDetailsActionBean;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsService.java 3889 2014-12-09 13:06:04Z DGomon $"
 */
@Path("/banking_details")
@Stateless
public class BankingDetailsService {

    @EJB(mappedName = "stateless.BankingDetailsActionBean", name = "BankingDetailsActionBean")
    BankingDetailsActionBean bankingDetailsActionBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") String id, @QueryParam("byAcc") boolean byAcc) {

        try {
            return SrvUtils.createMapResponse(
                    bankingDetailsActionBean.getById(UserHolder.getUser(), new BigDecimal(id), byAcc));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response add(@FormParam("details") String details) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(BankingDetails.class).getClass());
            BankingDetails o = objectReader.readValue(details);
            o = bankingDetailsActionBean.create(UserHolder.getUser(), o);
            return Response.status(Response.Status.CREATED).entity(o).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response edit(@FormParam("details") String details) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(BankingDetails.class).getClass());
            BankingDetails o = objectReader.readValue(details);
            o = bankingDetailsActionBean.update(UserHolder.getUser(), o);
            return Response.status(Response.Status.OK).entity(o).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response delete(@QueryParam("id") Long details) {

        try {
            bankingDetailsActionBean.delete(UserHolder.getUser(), details);
            return Response.status(Response.Status.OK).build();
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }


}
