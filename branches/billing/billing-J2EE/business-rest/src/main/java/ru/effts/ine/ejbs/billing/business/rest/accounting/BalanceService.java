package ru.effts.ine.ejbs.billing.business.rest.accounting;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.accounting.BalanceItemActionBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceService.java 3889 2014-12-09 13:06:04Z DGomon $"
 */
@Path("/balance")
@Stateless
public class BalanceService {

    @EJB(mappedName = "stateless.BalanceItemActionBean", name = "BalanceItemActionBean")
    BalanceItemActionBean balanceItemActionBean;

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getBalance(@QueryParam("accId") BigDecimal accId) {
        try {
            return Response.status(Response.Status.OK).entity(
                    balanceItemActionBean.getBalanceItemForAccount(UserHolder.getUser(), accId)
            ).build();
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getHistory")
    @GET
    public Response getBalanceHistory(@QueryParam("accId") BigDecimal accId,
                                      @QueryParam("fd") Long fd,
                                      @QueryParam("td") Long td) {
        try {

            Date fdt = new Date();
            fdt.setTime(fd);
            Date tdt = new Date();
            tdt.setTime(td);
            return Response.status(Response.Status.OK).entity(
                    balanceItemActionBean.getBalanceHistoryForAccount(UserHolder.getUser(), accId, fdt, tdt)
            ).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
