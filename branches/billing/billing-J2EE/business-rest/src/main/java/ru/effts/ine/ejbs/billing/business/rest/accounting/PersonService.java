package ru.effts.ine.ejbs.billing.business.rest.accounting;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;
import ru.effts.ine.ejbs.oss.entity.PersonBean;
import ru.effts.ine.oss.entity.Person;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: PersonService.java 3889 2014-12-09 13:06:04Z DGomon $"
 */
@Path("/person")
@Stateless
public class PersonService {

    @EJB(mappedName = "stateless.PersonBean", name = "PersonBean")
    PersonBean<Person> personBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    personBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response add(@FormParam("person") String company) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(Person.class).getClass());
            Person o = objectReader.readValue(company);
            o = personBean.createObject(UserHolder.getUser(), o);
            return Response.status(Response.Status.CREATED).entity(o).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response edit(@FormParam("person") String details) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(Person.class).getClass());
            Person o = objectReader.readValue(details);
            o = personBean.updateObject(UserHolder.getUser(), o);
            return Response.status(Response.Status.OK).entity(o).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response delete(@QueryParam("id") Long personId) {


        try {
            personBean.deleteObject(
                    UserHolder.getUser(),
                    personBean.getObjectById(
                            UserHolder.getUser(), new BigDecimal(personId)
                    )
            );

            return Response.status(Response.Status.OK).build();
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }


}
