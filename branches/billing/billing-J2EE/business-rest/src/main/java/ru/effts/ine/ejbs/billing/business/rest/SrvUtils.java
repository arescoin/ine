package ru.effts.ine.ejbs.billing.business.rest;

import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Map;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: SrvUtils.java 3862 2014-09-23 14:35:05Z DGomon $"
 */
public class SrvUtils {

    public static Response createObjectResponse(Object o) {

        if (null != o) {
            return Response.status(Response.Status.OK).entity(o).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    public static Response createCollectionResponse(Collection collection) {

        if (null != collection && !collection.isEmpty()) {
            return Response.status(Response.Status.OK).entity(collection).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    public static Response createMapResponse(Map map) {

        if (null != map && !map.isEmpty()) {
            return Response.status(Response.Status.OK).entity(map).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
