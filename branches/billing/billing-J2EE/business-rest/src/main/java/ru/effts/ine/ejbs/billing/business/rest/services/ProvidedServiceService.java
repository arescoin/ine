package ru.effts.ine.ejbs.billing.business.rest.services;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;
import ru.effts.ine.ejbs.billing.services.ProvidedServiceBean;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceService.java 3891 2014-12-09 13:06:24Z DGomon $"
 */
@Path("/provided_service")
@Stateless
public class ProvidedServiceService {

    @EJB(mappedName = "stateless.ProvidedServiceBean", name = "ProvidedServiceBean")
    ProvidedServiceBean<ProvidedService> providedServiceBean;

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") Long id, @QueryParam("byAcc") boolean byAcc) {

        Map<String, Object> resultMap = new HashMap<>();
        Map<BigDecimal, ProvidedService> serviceMap = new HashMap<>();

        try {
            if (!byAcc) {
                ProvidedService providedService =
                        providedServiceBean.getObjectById(UserHolder.getUser(), new BigDecimal(id));

                serviceMap.put(providedService.getCoreId(), providedService);
                resultMap.put(ProvidedService.class.getName(), serviceMap);
            } else {
                SearchCriteriaProfile profile =
                        providedServiceBean.getSearchCriteriaProfile(UserHolder.getUser());
                Set<SearchCriterion> criteria = new LinkedHashSet<>();
                criteria.add(profile.getSearchCriterion(
                        ProvidedService.ACCOUNT_ID, CriteriaRule.equals, new BigDecimal(id))
                );

                Collection<ProvidedService> providedServices =
                        providedServiceBean.searchObjects(UserHolder.getUser(), criteria);

                for (ProvidedService providedService : providedServices) {
                    serviceMap.put(providedService.getCoreId(), providedService);
                }

                resultMap.put(ProvidedService.class.getName(), serviceMap);

            }


            return SrvUtils.createMapResponse(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response add(@FormParam("service") String service) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ProvidedService.class).getClass());
            ProvidedService o = objectReader.readValue(service);
            o = providedServiceBean.createObject(UserHolder.getUser(), o);
            return Response.status(Response.Status.CREATED).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response edit(@FormParam("service") String service) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ProvidedService.class).getClass());
            ProvidedService o = objectReader.readValue(service);
            o = providedServiceBean.updateObject(UserHolder.getUser(), o);
            return Response.status(Response.Status.OK).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response delete(@QueryParam("id") Long id) {

        try {
            providedServiceBean.deleteObject(UserHolder.getUser(),
                    providedServiceBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));

            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

}
