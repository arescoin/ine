package ru.effts.ine.ejbs.billing.business.rest.auth;

import ru.effts.ine.core.UserHolder;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AuthService.java 3895 2014-12-09 13:08:53Z DGomon $"
 */
@Path("/authService")
@Stateless
public class AuthService {

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById() {
        return Response.status(Response.Status.OK).
                entity("Logged in as [" + UserHolder.getUser().getSystemUser().getLogin() + "]").build();
    }
}
