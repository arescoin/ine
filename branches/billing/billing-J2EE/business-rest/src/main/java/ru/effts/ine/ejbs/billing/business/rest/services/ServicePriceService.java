package ru.effts.ine.ejbs.billing.business.rest.services;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;
import ru.effts.ine.ejbs.billing.services.ServicePriceBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceService.java 3891 2014-12-09 13:06:24Z DGomon $"
 */
@Path("/service_price")
@Stateless
public class ServicePriceService {

    @EJB(mappedName = "stateless.ServicePriceBean", name = "ServicePriceBean")
    ServicePriceBean<ServicePrice> servicePriceBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") Long id) {

//        Map<String, Map<BigDecimal, Identifiable>> resultMap = new HashMap<>();

        try {
            ServicePrice serviceSpec = servicePriceBean.getObjectById(UserHolder.getUser(), new BigDecimal(id));
            serviceSpec.getPriceListID();

            return SrvUtils.createObjectResponse(
                    servicePriceBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response add(@FormParam("servicePrice") String servicePrice) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ServicePrice.class).getClass());
            ServicePrice o = objectReader.readValue(servicePrice);
            o = servicePriceBean.createObject(UserHolder.getUser(), o);
            return Response.status(Response.Status.CREATED).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response edit(@FormParam("servicePrice") String servicePrice) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ServicePrice.class).getClass());
            ServicePrice o = objectReader.readValue(servicePrice);
            o = servicePriceBean.updateObject(UserHolder.getUser(), o);
            return Response.status(Response.Status.OK).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response delete(@QueryParam("id") Long id) {

        try {
            servicePriceBean.deleteObject(UserHolder.getUser(),
                    servicePriceBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));

            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }
}
