package ru.effts.ine.ejbs.billing.business.rest.services;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.dic.DictionaryTerm;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;
import ru.effts.ine.ejbs.billing.services.ServiceBundleItemBean;
import ru.effts.ine.ejbs.billing.services.ServiceSpecBean;
import ru.effts.ine.ejbs.core.dic.DictionaryTermBean;
import ru.effts.ine.ejbs.core.links.LinkBean;
import ru.effts.ine.ejbs.core.links.LinkDataBean;
import ru.effts.ine.ejbs.core.structure.FieldAccessBean;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemService.java 3891 2014-12-09 13:06:24Z DGomon $"
 */
@Path("/service_bundle_item")
@Stateless
public class ServiceBundleItemService {


    public static final BigDecimal SB_DIC = new BigDecimal(42);

    private static Link link;

    @EJB(name = "ServiceBundleItemBean", mappedName = "stateless.ServiceBundleItemBean")
    ServiceBundleItemBean<ServiceBundleItem> serviceBundleItemBean;

    @EJB(mappedName = "stateless.ServiceSpecBean", name = "ServiceSpecBean")
    ServiceSpecBean<ServiceSpec> serviceSpecBean;

    @EJB(mappedName = "stateless.DictionaryTermBean", name = "DictionaryTermBean")
    DictionaryTermBean<DictionaryTerm> dictionaryTermBean;

    @EJB(name = "LinkBean", mappedName = "stateless.LinkBean")
    LinkBean<Link> linkBean;

    @EJB(name = "FieldAccessBean", mappedName = "stateless.FieldAccessBean")
    FieldAccessBean fieldAccessBean;

    @EJB(name = "LinkDataBean", mappedName = "stateless.LinkDataBean")
    LinkDataBean<LinkData> linkDataBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") Long id, @QueryParam("byBundle") boolean byBundle) {

        Map<String, Object> resultMap = new HashMap<>();
        try {

            Collection<ServiceSpec> serviceSpecs = new HashSet<>();

            if (!byBundle) {
                ServiceBundleItem item =
                        serviceBundleItemBean.getObjectById(UserHolder.getUser(), new BigDecimal(id));
                Collection<ServiceBundleItem> bundleItems = new HashSet<>();
                bundleItems.add(item);
                resultMap.put(ServiceBundleItem.class.getName(), bundleItems);

                HashMap<BigDecimal, Collection<DictionaryTerm>> dicHashMap = new HashMap<>();
                dicHashMap.put(SB_DIC, dictionaryTermBean.getAllTermsForDictionaryAndEntry(
                        UserHolder.getUser(), SB_DIC, item.getBundleId()));
                resultMap.put(DictionaryTerm.class.getName(), dicHashMap);

                serviceSpecs.add(serviceSpecBean.getObjectById(UserHolder.getUser(), item.getServiceSpecId()));
                resultMap.put(ServiceSpec.class.getName(), serviceSpecs);

            } else {
                SearchCriteriaProfile profile =
                        serviceBundleItemBean.getSearchCriteriaProfile(UserHolder.getUser());
                Set<SearchCriterion> criteria = new LinkedHashSet<>();
                criteria.add(profile.getSearchCriterion(
                        ServiceBundleItem.BUNDLE_ID, CriteriaRule.equals, new BigDecimal(id)));

                Collection<ServiceBundleItem> bundleItems =
                        serviceBundleItemBean.searchObjects(UserHolder.getUser(), criteria);
                resultMap.put(ServiceBundleItem.class.getName(), bundleItems);

                for (ServiceBundleItem bundleItem : bundleItems) {
                    serviceSpecs.add(
                            serviceSpecBean.getObjectById(UserHolder.getUser(), bundleItem.getServiceSpecId())
                    );
                }
                resultMap.put(ServiceSpec.class.getName(), serviceSpecs);
            }

            return SrvUtils.createMapResponse(resultMap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response add(@FormParam("serviceBundleItem") String serviceBundleItem, @FormParam("req") Set<Long> req) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {

            if (null != req && !req.isEmpty()) {
                if (null == getLink()) {
                    return Response.status(Response.Status.NOT_MODIFIED).build();
                }
            }

            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ServiceBundleItem.class).getClass());
            ServiceBundleItem o = objectReader.readValue(serviceBundleItem);
            o = serviceBundleItemBean.createObject(UserHolder.getUser(), o);

            if (null != req && !req.isEmpty()) {
                for (Long aLong : req) {
                    LinkData linkData = IdentifiableFactory.getImplementation(LinkData.class);
                    linkData.setLinkId(link.getCoreId());
                    linkData.setLeftId(new BigDecimal(aLong));
                    linkData.setRightId(o.getCoreId());
                    linkDataBean.createObject(UserHolder.getUser(), linkData);
                }
            }

            return Response.status(Response.Status.CREATED).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response edit(@FormParam("serviceBundleItem") String serviceBundleItem, @FormParam("req") Set<Long> req) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {

            if (null != req && !req.isEmpty()) {
                if (null == getLink()) {
                    return Response.status(Response.Status.NOT_MODIFIED).build();
                }
            }

            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ServiceBundleItem.class).getClass());
            ServiceBundleItem o = objectReader.readValue(serviceBundleItem);
            o = serviceBundleItemBean.updateObject(UserHolder.getUser(), o);

            Collection<LinkData> linksData = linkDataBean.getLinkDataByObject(
                    UserHolder.getUser(), o, link.getCoreId());

            if (null != req && !req.isEmpty()) {
                HashSet<Long> toAdd = new HashSet<>(req);

                Collection<LinkData> toDelete = new ArrayList<>();
                for (LinkData data : linksData) {
                    Long aLong = data.getLeftId().longValue();
                    // перебираем линки на удаление
                    if (!req.contains(aLong)) {
                        toDelete.add(data);
                    } else {
                        // и на создание
                        toAdd.remove(aLong);
                    }
                }

                // удаляем линки
                for (LinkData data : toDelete) {
                    linkDataBean.deleteObject(UserHolder.getUser(), data);
                }

                // создаем линки
                for (Long aLong : toAdd) {
                    LinkData linkData = IdentifiableFactory.getImplementation(LinkData.class);
                    linkData.setLinkId(link.getCoreId());
                    linkData.setLeftId(new BigDecimal(aLong));
                    linkData.setRightId(o.getCoreId());
                    linkDataBean.createObject(UserHolder.getUser(), linkData);
                }

            } else {
                // удаляем линки
                for (LinkData data : linksData) {
                    linkDataBean.deleteObject(UserHolder.getUser(), data);
                }
            }

            return Response.status(Response.Status.OK).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response delete(@QueryParam("id") Long id) {

        try {
            serviceBundleItemBean.deleteObject(UserHolder.getUser(),
                    serviceBundleItemBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));

            return Response.status(Response.Status.OK).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }


    private Link getLink() throws GenericSystemException {

        if (null != ServiceBundleItemService.link) {
            return ServiceBundleItemService.link;
        }

        // займемся линками
        // тут мы ищем нужный нам тип линка
        SearchCriteriaProfile profile = linkBean.getSearchCriteriaProfile(UserHolder.getUser());
        Set<SearchCriterion> criteria = new LinkedHashSet<>();
        criteria.add(profile.getSearchCriterion(Link.LEFT_TYPE, CriteriaRule.equals,
                fieldAccessBean.getFieldId(ServiceBundleItem.class, ServiceBundleItem.CORE_ID))
        );
        criteria.add(profile.getSearchCriterion(Link.RIGHT_TYPE, CriteriaRule.equals,
                fieldAccessBean.getFieldId(ServiceBundleItem.class, ServiceBundleItem.CORE_ID))
        );
        criteria.add(profile.getSearchCriterion(Link.TYPE, CriteriaRule.equals, new BigDecimal(2)));
        Collection<Link> links = linkBean.searchObjects(UserHolder.getUser(), criteria);

        // если не нашли, то свалим...
        if (links.size() != 1) {
            return null;
        }

        ServiceBundleItemService.link = links.iterator().next();

        return ServiceBundleItemService.link;
    }

}
