package ru.effts.ine.ejbs.billing.business.rest.accounting;

import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.accounting.LastAccountBillActionBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillService.java 3889 2014-12-09 13:06:04Z DGomon $"
 */
@Path("/account_bill")
@Stateless
public class LastAccountBillService {

    @EJB(mappedName = "stateless.LastAccountBillActionBean", name = "LastAccountBillActionBean")
    LastAccountBillActionBean lastAccountBillActionBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getAccBill(@QueryParam("accId") BigDecimal accId) {
        try {

            return Response.status(Response.Status.OK).entity(
                    lastAccountBillActionBean.getByAccId(UserHolder.getUser(), accId)
            ).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getOldBills")
    @GET
    public Response getBalanceHistory(@QueryParam("accId") BigDecimal accId) {
        try {

            return Response.status(Response.Status.OK).entity(
                    lastAccountBillActionBean.getOldBillsForAcc(UserHolder.getUser(), accId)
            ).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
