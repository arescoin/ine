package ru.effts.ine.ejbs.billing.business.rest.accounting;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;
import ru.effts.ine.ejbs.oss.entity.CompanyBean;
import ru.effts.ine.oss.entity.Company;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyService.java 3889 2014-12-09 13:06:04Z DGomon $"
 */
@Path("/company")
@Stateless
public class CompanyService {

    @EJB(mappedName = "stateless.CompanyBean", name = "CompanyBean")
    CompanyBean<Company> companyBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") Long id) {

        try {
            return SrvUtils.createObjectResponse(
                    companyBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));
        } catch (GenericSystemException e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response add(@FormParam("company") String company) {

        ObjectMapper objectMapper = new ObjectMapper();

        try {
            Class<? extends Company> aClass = IdentifiableFactory.getImplementation(Company.class).getClass();
            ObjectReader objectReader = objectMapper.reader(aClass);
            Company o = objectReader.readValue(company);

            Collection<Company> companies = getByName(o.getName());
            if (!companies.isEmpty()) {
                return Response.status(Response.Status.NOT_MODIFIED).header("Exists:", companies).build();
            }

            o = companyBean.createObject(UserHolder.getUser(), o);

            return Response.status(Response.Status.CREATED).entity(o).build();

        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response edit(@FormParam("company") String company) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(Company.class).getClass());
            Company o = objectReader.readValue(company);

            Collection<Company> companies = getByName(o.getName());
            for (Company company1 : companies) {
                if (company1.getName().equals(o.getName()) && !company1.getCoreId().equals(o.getCoreId())) {
                    Response.status(Response.Status.NOT_MODIFIED).entity("Duplicate Company Name").build();
                }
            }

            o = companyBean.updateObject(UserHolder.getUser(), o);
            return Response.status(Response.Status.OK).entity(o).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response delete(@QueryParam("id") Long companyId) {

        try {
            companyBean.deleteObject(UserHolder.getUser(), companyBean.getObjectById(
                    UserHolder.getUser(), new BigDecimal(companyId))
            );

            return Response.status(Response.Status.OK).build();
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/getByName")
    @GET
    public Collection<Company> getByName(@QueryParam("name") String companyName) throws GenericSystemException {

        SearchCriteriaProfile profile = companyBean.getSearchCriteriaProfile(UserHolder.getUser());
        SearchCriterion searchCriterion = profile.getSearchCriterion(Company.NAME, CriteriaRule.equals, companyName);
        Set<SearchCriterion> criterionSet = new HashSet<>();
        criterionSet.add(searchCriterion);

        return companyBean.searchObjects(UserHolder.getUser(), criterionSet);
    }

}
