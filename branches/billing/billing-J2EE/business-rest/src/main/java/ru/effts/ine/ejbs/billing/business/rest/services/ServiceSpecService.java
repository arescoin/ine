package ru.effts.ine.ejbs.billing.business.rest.services;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.billing.services.ServiceSpec;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.core.links.Link;
import ru.effts.ine.core.links.LinkData;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;
import ru.effts.ine.ejbs.billing.services.ServiceSpecBean;
import ru.effts.ine.ejbs.core.links.LinkBean;
import ru.effts.ine.ejbs.core.links.LinkDataBean;
import ru.effts.ine.ejbs.core.structure.FieldAccessBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpecService.java 3891 2014-12-09 13:06:24Z DGomon $"
 */
@Path("/service_spec")
@Stateless
public class ServiceSpecService {

    private static BigDecimal linkId;

    @EJB(mappedName = "stateless.ServiceSpecBean", name = "ServiceSpecBean")
    ServiceSpecBean<ServiceSpec> serviceSpecBean;

    @EJB(name = "LinkBean", mappedName = "stateless.LinkBean")
    LinkBean<Link> linkBean;

    @EJB(name = "FieldAccessBean", mappedName = "stateless.FieldAccessBean")
    FieldAccessBean fieldAccessBean;

    @EJB(name = "LinkDataBean", mappedName = "stateless.LinkDataBean")
    LinkDataBean<LinkData> linkDataBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getById(@QueryParam("id") Long id) {

        try {
            BigDecimal linkId = getLink();
            if (null == linkId) {
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }

            Map<Object, Object> resultMap = new HashMap<>();

            ServiceSpec serviceSpec = serviceSpecBean.getObjectById(UserHolder.getUser(), new BigDecimal(id));

            if (null == serviceSpec) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }

            resultMap.put(ServiceSpec.class.getName(), serviceSpec);

            Collection<LinkData> linksData = linkDataBean.getLinkDataByObject(
                    UserHolder.getUser(), serviceSpec, linkId);

            BigDecimal leftId;
            for (LinkData linkData : linksData) {
                leftId = linkData.getLeftId();
                resultMap.put(leftId, serviceSpecBean.getObjectById(UserHolder.getUser(), leftId));
            }

            return SrvUtils.createMapResponse(resultMap);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response add(@FormParam("serviceSpec") String serviceSpec, @FormParam("req") Set<Long> req) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {

            BigDecimal linkId = getLink();
            if (null == linkId) {
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }

            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ServiceSpec.class).getClass());
            ServiceSpec o = objectReader.readValue(serviceSpec);
            o = serviceSpecBean.createObject(UserHolder.getUser(), o);

            if (null != req && !req.isEmpty()) {
                for (Long aLong : req) {
                    LinkData linkData = IdentifiableFactory.getImplementation(LinkData.class);
                    linkData.setLinkId(linkId);
                    linkData.setLeftId(new BigDecimal(aLong));
                    linkData.setRightId(o.getCoreId());
                    linkDataBean.createObject(UserHolder.getUser(), linkData);
                }
            }

            return Response.status(Response.Status.CREATED).entity(o).build();

        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response edit(@FormParam("serviceSpec") String serviceSpec, @FormParam("req") Set<Long> req) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {

            BigDecimal link = getLink();
            if (null == link) {
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }

            // сохраним сам объект
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(ServiceSpec.class).getClass());
            ServiceSpec o = objectReader.readValue(serviceSpec);
            o = serviceSpecBean.updateObject(UserHolder.getUser(), o);


            Collection<LinkData> linksData = linkDataBean.getLinkDataByObject(
                    UserHolder.getUser(), o, linkId);

            HashSet<BigDecimal> decimals = new HashSet<>();
            for (long l : req) {
                decimals.add(new BigDecimal(l));
            }
            HashSet<BigDecimal> toAdd = new HashSet<>(decimals);

            Collection<LinkData> toDelete = new ArrayList<>();
            for (LinkData data : linksData) {
                BigDecimal aLong = data.getLeftId();
                // перебираем линки на удаление
                if (!decimals.contains(aLong)) {
                    toDelete.add(data);
                } else {
                    // и на создание
                    toAdd.remove(aLong);
                }
            }

            // удаляем линки
            for (LinkData data : toDelete) {
                linkDataBean.deleteObject(UserHolder.getUser(), data);
            }

            // создаем линки
            for (BigDecimal decimal : toAdd) {
                LinkData linkData = IdentifiableFactory.getImplementation(LinkData.class);
                linkData.setLinkId(linkId);
                linkData.setLeftId(decimal);
                linkData.setRightId(o.getCoreId());
                linkDataBean.createObject(UserHolder.getUser(), linkData);
            }

            return Response.status(Response.Status.OK).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    private BigDecimal getLink() throws GenericSystemException {

        if (null != ServiceSpecService.linkId) {
            return ServiceSpecService.linkId;
        }

        linkId = linkBean.getLinkId(UserHolder.getUser(), ServiceSpec.class, ServiceSpec.class, 2);

        return linkId;
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response delete(@QueryParam("id") Long id) {


        try {

            BigDecimal link = getLink();
            if (null == link) {
                return Response.status(Response.Status.NOT_MODIFIED).build();
            }

            ServiceSpec o = serviceSpecBean.getObjectById(UserHolder.getUser(), new BigDecimal(id));

            if (null != o) {
                Collection<LinkData> linksData =
                        linkDataBean.getLinkDataByObject(UserHolder.getUser(), o, linkId);

                for (LinkData linkData : linksData) {
                    linkDataBean.deleteObject(UserHolder.getUser(), linkData);
                }

                serviceSpecBean.deleteObject(UserHolder.getUser(),
                        serviceSpecBean.getObjectById(UserHolder.getUser(), new BigDecimal(id)));


                return Response.status(Response.Status.OK).build();
            }

            return Response.status(Response.Status.NOT_MODIFIED).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

}
