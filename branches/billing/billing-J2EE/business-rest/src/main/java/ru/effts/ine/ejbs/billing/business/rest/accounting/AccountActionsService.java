package ru.effts.ine.ejbs.billing.business.rest.accounting;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectReader;
import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.IdentifiableFactory;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.BusinessActions;
import ru.effts.ine.ejbs.billing.business.accounting.AccountActionBean;
import ru.effts.ine.ejbs.billing.business.rest.SrvUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/** Класс обработчик http-запросов приходящих на rest-сервис */
@Path("/account")
@Stateless
public class AccountActionsService {

    private static final Logger logger = Logger.getLogger(AccountActionsService.class.getName());
    @EJB(mappedName = "stateless.BusinessActions", name = "BusinessActions")
    BusinessActions businessActions;


    @EJB(mappedName = "stateless.AccountActionBean", name = "AccountActionBean")
    AccountActionBean accountActionBean;


    /**
     * Возвращает json-представление, содержащее описание клиента и его лицевого счета,
     * если параметр <b>types<b/> содержит типы связанных объектов, то вернутся и они
     *
     * @param types перечисление типов связанных объектов
     */
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getAccountById(@QueryParam("id") String id, @QueryParam("extInfo") Set<String> types) {

        try {
            return SrvUtils.createMapResponse(accountActionBean.getAccountById(UserHolder.getUser(),
                    new BigDecimal(id), new HashSet<>(types)));
        } catch (GenericSystemException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
        }

        return Response.status(Response.Status.NOT_FOUND).build();
    }

    /**
     * Возвращает json-представление, содержащее описание клиента и его лицевого счета,
     * если параметр <b>types<b/> содержит типы связанных объектов, то вернутся и они
     */
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @POST
    public Response addAccount(@FormParam("account") String account) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(Account.class).getClass());
            Account o = objectReader.readValue(account);
            o = accountActionBean.createAccount(UserHolder.getUser(), o);
            return Response.status(Response.Status.CREATED).entity(o).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    /**
     * Возвращает json-представление, содержащее описание клиента и его лицевого счета,
     * если параметр <b>types<b/> содержит типы связанных объектов, то вернутся и они
     */
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @PUT
    public Response editAccount(@FormParam("account") String account) {

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            ObjectReader objectReader =
                    objectMapper.reader(IdentifiableFactory.getImplementation(Account.class).getClass());
            Account o = objectReader.readValue(account);
            o = accountActionBean.updateAccount(UserHolder.getUser(), o);
            return Response.status(Response.Status.OK).entity(o).build();
        } catch (IOException | GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

    /**
     * Возвращает json-представление, содержащее описание клиента и его лицевого счета,
     * если параметр <b>types<b/> содержит типы связанных объектов, то вернутся и они
     */
    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @DELETE
    public Response deleteAccount(@QueryParam("id") Long accountId) {

        try {
            accountActionBean.deleteAccount(UserHolder.getUser(), accountId);
            return Response.status(Response.Status.OK).build();
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_MODIFIED).build();
        }
    }

}
