package ru.effts.ine.ejbs.billing.business.rest.accounting;

import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.UserHolder;
import ru.effts.ine.ejbs.billing.business.accounting.BillingHistoryItemActionBean;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemService.java 3889 2014-12-09 13:06:04Z DGomon $"
 */
@Path("/billingHistory")
@Stateless
public class BillingHistoryItemService {

    @EJB(mappedName = "stateless.BillingHistoryItemActionBean", name = "BillingHistoryItemActionBean")
    BillingHistoryItemActionBean billingHistoryItemActionBean;


    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @GET
    public Response getBalance(@QueryParam("id") BigDecimal id) {
        try {
            return Response.status(Response.Status.OK).entity(
                    billingHistoryItemActionBean.getHistoryById(UserHolder.getUser(), id)
            ).build();
        } catch (GenericSystemException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/byAccId")
    @GET
    public Response getHistoryByAccId(@QueryParam("accId") BigDecimal accId,
                                      @QueryParam("fd") Long fd,
                                      @QueryParam("td") Long td) {
        try {

            Date fdt = new Date();
            fdt.setTime(fd);
            Date tdt = new Date();
            tdt.setTime(td);
            return Response.status(Response.Status.OK).entity(
                    billingHistoryItemActionBean.getHistoryByAccId(UserHolder.getUser(), accId, fdt, tdt)
            ).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
    @Path("/bySrvId")
    @GET
    public Response getHistorySrvAccId(@QueryParam("bySrvId") BigDecimal srvId,
                                       @QueryParam("fd") Long fd,
                                       @QueryParam("td") Long td) {
        try {

            Date fdt = new Date();
            fdt.setTime(fd);
            Date tdt = new Date();
            tdt.setTime(td);
            return Response.status(Response.Status.OK).entity(
                    billingHistoryItemActionBean.getHistoryBySrvId(UserHolder.getUser(), srvId, fdt, tdt)
            ).build();
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
