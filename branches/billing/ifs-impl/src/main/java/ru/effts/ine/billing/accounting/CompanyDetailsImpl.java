package ru.effts.ine.billing.accounting;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class CompanyDetailsImpl extends AbstractVersionable implements CompanyDetails {

    private BigDecimal accountId;
    private BigDecimal companyId;

    private String phone;
    private String legalAddress;
    private String physicalAddress;

    @Override
    public BigDecimal getAccountId() {
        return this.accountId;
    }

    @Override
    public void setAccountId(BigDecimal accountId) throws IllegalIdException {

        if (null == accountId) {
            throw new IllegalIdException("Account ID can't be null");
        }

        this.accountId = accountId;
    }

    @Override
    public BigDecimal getCompanyId() {
        return this.companyId;
    }

    @Override
    public void setCompanyId(BigDecimal companyId) throws IllegalIdException {

        if (null == companyId) {
            throw new IllegalIdException("Company ID can't be null");
        }

        this.companyId = companyId;
    }

    @Override
    public String getPhoneNumber() {
        return this.phone;
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        this.phone = phoneNumber;
    }

    @Override
    public String getLegalAddress() {
        return this.legalAddress;
    }

    @Override
    public void setLegalAddress(String address) {
        this.legalAddress = address;
    }

    @Override
    public String getPhysicalAddress() {
        return this.physicalAddress;
    }

    @Override
    public void setPhysicalAddress(String address) {
        this.physicalAddress = address;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(ACCOUNT_ID, getAccountId())
                + fieldToString(COMPANY_ID, getCompanyId())
                + fieldToString(PHONE_N, getPhoneNumber())
                + fieldToString(LEGAL_ADDRESS, getLegalAddress())
                + fieldToString(PHYSICAL_ADDRESS, getPhysicalAddress());
    }

}
