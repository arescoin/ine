package ru.effts.ine.billing.balance;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class BalanceItemImpl extends AbstractVersionable implements BalanceItem {

    private BigDecimal accountId;
    private boolean addition;
    private BigDecimal balance;
    private BigDecimal amount;
    private BigDecimal reasonType;
    private BigDecimal reasonId;
    private Date date;

    @Override
    public BigDecimal getAccountId() {
        return this.accountId;
    }

    @Override
    public void setAccountId(BigDecimal accountId) throws IllegalIdException {
        AbstractIdentifiable.checkSystemId(accountId, BalanceItem.ACCOUNT_ID);
        this.accountId = accountId;
    }

    @Override
    public boolean isAddition() {
        return this.addition;
    }

    @Override
    public void setAddition(boolean addition) {
        this.addition = addition;
    }

    @Override
    public BigDecimal getBalance() {
        return this.balance;
    }

    @Override
    public void setBalance(BigDecimal balance) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(balance, BalanceItem.BALANCE);
        this.balance = balance;
    }

    @Override
    public BigDecimal getAmount() {
        return this.amount;
    }

    @Override
    public void setAmount(BigDecimal amount) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(amount, BalanceItem.AMOUNT);
        this.amount = amount;
    }

    @Override
    public BigDecimal getReasonType() {
        return this.reasonType;
    }

    @Override
    public void setReasonType(BigDecimal reasonType) throws IllegalIdException {

        if (null != reasonType) {
            AbstractIdentifiable.checkSystemId(reasonType, BalanceItem.REASON_TYPE);
        }

        this.reasonType = reasonType;
    }

    @Override
    public BigDecimal getReasonId() {
        return this.reasonId;
    }

    @Override
    public void setReasonId(BigDecimal reasonId) throws IllegalIdException {

        if (null != reasonId) {
            AbstractIdentifiable.checkSystemId(reasonId, BalanceItem.REASON_ID);
        }

        this.reasonId = reasonId;
    }

    @Override
    public Date getDate() {
        return this.date;
    }

    @Override
    public void setDate(Date date) throws IneIllegalArgumentException {
        AbstractIdentifiable.checkNull(date, BalanceItem.DATE);
        this.date = date;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(ACCOUNT_ID, getAccountId())
                + fieldToString(ADDITION, isAddition())
                + fieldToString(BALANCE, getBalance())
                + fieldToString(AMOUNT, getAmount())
                + fieldToString(REASON_TYPE, getReasonType())
                + fieldToString(REASON_ID, getReasonId())
                + fieldToString(DATE, getDate());
    }

}
