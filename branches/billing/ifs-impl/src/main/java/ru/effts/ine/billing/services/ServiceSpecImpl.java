package ru.effts.ine.billing.services;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @version 1.0
 * @SVNVersion "$Id: ServiceSpecImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class ServiceSpecImpl extends AbstractVersionable implements ServiceSpec {

    private String name;

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) throws IneIllegalArgumentException {

        if (null == name) {
            throw new IneIllegalArgumentException("The Name can't be null");
        }

        if (name.isEmpty()) {
            throw new IneIllegalArgumentException("The Name can't has zero length");
        }

        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString() + fieldToString(NAME, getName());
    }

}
