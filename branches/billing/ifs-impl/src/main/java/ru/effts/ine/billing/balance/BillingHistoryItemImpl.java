package ru.effts.ine.billing.balance;

import ru.effts.ine.core.AbstractIdentifiable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class BillingHistoryItemImpl extends AbstractIdentifiable implements BillingHistoryItem {

    private BigDecimal serviceId;
    private BigDecimal accountId;
    private Date billingDate;

    @Override
    public BigDecimal getServiceId() {
        return this.serviceId;
    }

    @Override
    public void setServiceId(BigDecimal serviceId) throws IllegalIdException {
        checkSystemId(serviceId, SERVICE_ID);
        this.serviceId = serviceId;
    }

    @Override
    public BigDecimal getAccountId() {
        return this.accountId;
    }

    @Override
    public void setAccountId(BigDecimal accountId) throws IllegalIdException {
        checkSystemId(accountId, ACCOUNT_ID);
        this.accountId = accountId;
    }

    @Override
    public Date getBillingDate() {
        return this.billingDate;
    }

    @Override
    public void setBillingDate(Date billingDate) throws IneIllegalArgumentException {
        checkNull(billingDate, BILLING_DATE);
        this.billingDate = billingDate;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(SERVICE_ID, getServiceId())
                + fieldToString(ACCOUNT_ID, getAccountId())
                + fieldToString(BILLING_DATE, getBillingDate());
    }

}
