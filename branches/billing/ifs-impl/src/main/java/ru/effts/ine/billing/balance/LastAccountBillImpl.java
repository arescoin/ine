package ru.effts.ine.billing.balance;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: LastAccountBillImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class LastAccountBillImpl extends AbstractVersionable implements LastAccountBill {

    private Date billDate;

    @Override
    public Date getBillDate() {
        return this.billDate;
    }

    @Override
    public void setBillDate(Date billDate) throws IneIllegalArgumentException {
        checkNull(billDate, BILL_DATE);
        this.billDate = billDate;
    }

    @Override
    public String toString() {
        return super.toString() + fieldToString(BILL_DATE, getBillDate());
    }

}
