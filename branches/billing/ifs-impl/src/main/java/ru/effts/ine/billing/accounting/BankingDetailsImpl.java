package ru.effts.ine.billing.accounting;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class BankingDetailsImpl extends AbstractVersionable implements BankingDetails {

    private BigDecimal accountId;

    private String inn;
    private String kpp;
    private String bankName;
    private String bik;
    private String bankAcc;
    private String corAcc;

    @Override
    public BigDecimal getAccountId() {
        return this.accountId;
    }

    @Override
    public void setAccountId(BigDecimal accountId) throws IllegalIdException {

        if (null == accountId) {
            throw new IllegalIdException("Account Id can't be null");
        }

        this.accountId = accountId;
    }

    @Override
    public String getInn() {
        return this.inn;
    }

    @Override
    public void setInn(String inn) {
        this.inn = inn;
    }

    @Override
    public String getKpp() {
        return this.kpp;
    }

    @Override
    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    @Override
    public String getBankName() {
        return this.bankName;
    }

    @Override
    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    @Override
    public String getBik() {
        return this.bik;
    }

    @Override
    public void setBik(String bik) {
        this.bik = bik;
    }

    @Override
    public String getBankAccountNumber() {
        return this.bankAcc;
    }

    @Override
    public void setBankAccountNumber(String bankAcc) {
        this.bankAcc = bankAcc;
    }

    @Override
    public String getCorAccountNumber() {
        return this.corAcc;
    }

    @Override
    public void setCorAccountNumber(String corAcc) {
        this.corAcc = corAcc;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(ACCOUNT_ID, getAccountId())
                + fieldToString(INN, getInn())
                + fieldToString(KPP, getKpp())
                + fieldToString(BANK_NAME, getBankName())
                + fieldToString(BIK, getBik())
                + fieldToString(BANK_ACCOUNT_NUMBER, getBankAccountNumber())
                + fieldToString(COR_ACCOUNT_NUMBER, getCorAccountNumber());
    }

}
