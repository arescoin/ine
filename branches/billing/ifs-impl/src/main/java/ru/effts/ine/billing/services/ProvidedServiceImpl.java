package ru.effts.ine.billing.services;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class ProvidedServiceImpl extends AbstractVersionable implements ProvidedService {

    private BigDecimal accountId;
    private BigDecimal serviceBundleId;
    private BigDecimal parentId;
    private String serviceName;
    private BigDecimal status;


    @Override
    public BigDecimal getAccountId() {
        return this.accountId;
    }

    @Override
    public void setAccountId(BigDecimal accountId) throws IllegalIdException {
        checkSystemId(accountId, ACCOUNT_ID);
        this.accountId = accountId;
    }

    @Override
    public BigDecimal getServiceBundleId() {
        return this.serviceBundleId;
    }

    @Override
    public void setServiceBundleId(BigDecimal serviceBundleId) throws IllegalIdException {
        checkSystemId(serviceBundleId, SERVICE_BUNDLE_ID);
        this.serviceBundleId = serviceBundleId;
    }

    @Override
    public String getServiceName() {
        return this.serviceName;
    }

    @Override
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public BigDecimal getParentId() {
        return this.parentId;
    }

    @Override
    public void setParentId(BigDecimal parentId) {
        this.parentId = parentId;
    }

    public BigDecimal getStatus() {
        return status;
    }

    public void setStatus(BigDecimal status) throws IllegalIdException {
        checkSystemId(status, STATUS);
        this.status = status;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(ACCOUNT_ID, getAccountId())
                + fieldToString(SERVICE_BUNDLE_ID, getServiceBundleId())
                + fieldToString(PARENT_ID, getParentId())
                + fieldToString(SERVICE_NAME, getServiceName())
                + fieldToString(STATUS, getStatus());
    }

}
