package ru.effts.ine.billing.services;

import ru.effts.ine.core.AbstractVersionable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;

import java.math.BigDecimal;

/**
 * Реализация по-умолчанию
 *
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemImpl.java 3878 2014-11-10 15:20:51Z DGomon $"
 */
public class ServiceBundleItemImpl extends AbstractVersionable implements ServiceBundleItem {

    private BigDecimal bundleId;
    private BigDecimal serviceSpecId;
    private BigDecimal type;
    private String serviceName;

    @Override
    public BigDecimal getBundleId() {
        return this.bundleId;
    }

    @Override
    public void setBundleId(BigDecimal id) throws IllegalIdException {

        if (null == id) {
            throw new IllegalIdException("Bundle ID can't be null");
        }

        this.bundleId = id;
    }

    @Override
    public BigDecimal getServiceSpecId() {
        return this.serviceSpecId;
    }

    @Override
    public void setServiceSpecId(BigDecimal id) throws IllegalIdException {

        if (null == id) {
            throw new IllegalIdException("ServiceSpec ID can't be null");
        }

        this.serviceSpecId = id;
    }

    @Override
    public String getServiceName() {
        return this.serviceName;
    }

    @Override
    public void setServiceName(String name) throws IneIllegalArgumentException {

        if (null == name) {
            throw new IneIllegalArgumentException("Service Name can't be null");
        }

        name = name.trim();
        if (name.isEmpty()) {
            throw new IneIllegalArgumentException("Service Name can't be empty");
        }

        this.serviceName = name;
    }

    @Override
    public BigDecimal getType() {
        return type;
    }

    @Override
    public void setType(BigDecimal type) throws IllegalIdException {
        checkSystemId(type, TYPE);
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString()
                + fieldToString(BUNDLE_ID, getBundleId())
                + fieldToString(SERVICE_SPEC_ID, getServiceSpecId())
                + fieldToString(TYPE, getType())
                + fieldToString(SERVICE_NAME, getServiceName());
    }

}
