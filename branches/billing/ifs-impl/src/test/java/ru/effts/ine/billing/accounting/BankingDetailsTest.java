package ru.effts.ine.billing.accounting;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.VersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BankingDetailsTest.java 3861 2014-09-23 14:33:49Z DGomon $"
 */
public class BankingDetailsTest extends VersionableTest {

    public static final BigDecimal ACC_ID = BigDecimal.ONE;

    public static final String BAN = "BAN";
    public static final String CN = "CN";
    public static final String BN = "BN";
    public static final String BIK = "BIK";
    private static final String INN = "INN";
    private static final String KPP = "KPP";

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return BankingDetails.class;
    }

    @Test
    public void testCommonValid() {

        try {
            BankingDetails details = (BankingDetails) getInstance();

            details.setAccountId(ACC_ID);
            details.setCorAccountNumber(CN);
            details.setBankAccountNumber(BAN);
            details.setBankName(BN);
            details.setBik(BIK);
            details.setInn(INN);
            details.setKpp(KPP);


            Assert.assertEquals("Wrong Account Id", ACC_ID, details.getAccountId());
            Assert.assertEquals("Wrong Bank Account Number", BAN, details.getBankAccountNumber());
            Assert.assertEquals("Wrong Bank Account Number", CN, details.getCorAccountNumber());
            Assert.assertEquals("Wrong Bank Name", BN, details.getBankName());
            Assert.assertEquals("Wrong BIK", BIK, details.getBik());
            Assert.assertEquals("Wrong INN", INN, details.getInn());
            Assert.assertEquals("Wrong KPP", KPP, details.getKpp());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullAccountId() {

        try {
            BankingDetails details = (BankingDetails) getInstance();
            details.setAccountId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
