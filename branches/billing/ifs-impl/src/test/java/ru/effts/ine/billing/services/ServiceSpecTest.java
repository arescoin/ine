package ru.effts.ine.billing.services;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceSpecTest.java 3832 2014-06-03 08:42:09Z DGomon $"
 */
public class ServiceSpecTest extends VersionableTest {

    public static final String SRV_SPEC_NAME = "Test Name For Service Spec";

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ServiceSpec.class;
    }

    @Test
    public void testName() {
        try {
            ServiceSpec serviceSpec = (ServiceSpec) IdentifiableFactory.getImplementation(idClass);

            serviceSpec.setName(SRV_SPEC_NAME);
            Assert.assertEquals("Wrong ServiceSpec name", SRV_SPEC_NAME, serviceSpec.getName());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ServiceSpec serviceSpec = (ServiceSpec) IdentifiableFactory.getImplementation(idClass);
            serviceSpec.setName(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }

    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ServiceSpec serviceSpec = (ServiceSpec) IdentifiableFactory.getImplementation(idClass);
            serviceSpec.setName("");
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }

    }
}
