package ru.effts.ine.billing.balance;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingHistoryItemTest.java 3868 2014-10-17 09:19:51Z DGomon $"
 */
public class BillingHistoryItemTest extends IdentifiableTest {
    @Override
    protected Class<? extends Identifiable> getClassType() {
        return BillingHistoryItem.class;
    }

    @Test
    public void test() {
        Date date = new Date();
        try {
            BillingHistoryItem item = (BillingHistoryItem) getInstance();
            item.setAccountId(BigDecimal.ONE);
            item.setServiceId(BigDecimal.TEN);
            item.setBillingDate(date);

            Assert.assertEquals(BigDecimal.ONE, item.getAccountId());
            Assert.assertEquals(BigDecimal.TEN, item.getServiceId());
            Assert.assertEquals(date, item.getBillingDate());

        } catch (GenericSystemException e) {
            e.printStackTrace();
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullAcc() {
        try {
            ((BillingHistoryItem) getInstance()).setAccountId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullSrv() {
        try {
            ((BillingHistoryItem) getInstance()).setServiceId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullDate() {
        try {
            ((BillingHistoryItem) getInstance()).setBillingDate(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
