package ru.effts.ine.billing.services;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServiceBundleItemTest.java 3867 2014-10-16 07:34:01Z DGomon $"
 */
public class ServiceBundleItemTest extends VersionableTest {

    public static final String SRV_NAME = "TestSrvName";

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ServiceBundleItem.class;
    }

    @Test
    public void testCommonValid() {
        try {
            ServiceBundleItem serviceBundleItem = (ServiceBundleItem) IdentifiableFactory.getImplementation(idClass);

            serviceBundleItem.setServiceSpecId(BigDecimal.TEN);
            serviceBundleItem.setBundleId(BigDecimal.ONE);
            serviceBundleItem.setType(new BigDecimal(3));
            serviceBundleItem.setServiceName(SRV_NAME);


            Assert.assertEquals("Wrong Spec ID", BigDecimal.TEN, serviceBundleItem.getServiceSpecId());
            Assert.assertEquals("Wrong Bundle ID", BigDecimal.ONE, serviceBundleItem.getBundleId());
            Assert.assertEquals("Wrong Type ID", new BigDecimal(3), serviceBundleItem.getType());
            Assert.assertEquals("Wrong Name", SRV_NAME, serviceBundleItem.getServiceName());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullBundleId() {
        try {
            ServiceBundleItem serviceBundleItem = (ServiceBundleItem) IdentifiableFactory.getImplementation(idClass);

            serviceBundleItem.setBundleId(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullSpec() {
        try {
            ServiceBundleItem serviceBundleItem = (ServiceBundleItem) IdentifiableFactory.getImplementation(idClass);

            serviceBundleItem.setServiceSpecId(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullName() {
        try {
            ServiceBundleItem serviceBundleItem = (ServiceBundleItem) IdentifiableFactory.getImplementation(idClass);

            serviceBundleItem.setServiceName(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullType() {
        try {
            ServiceBundleItem serviceBundleItem = (ServiceBundleItem) IdentifiableFactory.getImplementation(idClass);

            serviceBundleItem.setType(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testEmptyName() {
        try {
            ServiceBundleItem serviceBundleItem = (ServiceBundleItem) IdentifiableFactory.getImplementation(idClass);

            serviceBundleItem.setServiceName("");

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
