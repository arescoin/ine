package ru.effts.ine.billing.services;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.VersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ServicePriceTest.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public class ServicePriceTest extends VersionableTest {

    public static final BigDecimal plId = BigDecimal.ONE;
    public static final BigDecimal sbIId = BigDecimal.TEN;
    public static final BigDecimal price = new BigDecimal(30);
    public static final BigDecimal dayPrice = BigDecimal.ONE;

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ServicePrice.class;
    }

    @Test
    public void testCommonValid() {
        try {
            ServicePrice servicePrice = (ServicePrice) getInstance();

            servicePrice.setPriceListID(plId);
            servicePrice.setServiceBundleItemID(sbIId);
            servicePrice.setPrice(price);
            servicePrice.setDayPrice(dayPrice);

            Assert.assertEquals("Wrong PriceListID", plId, servicePrice.getPriceListID());
            Assert.assertEquals("Wrong ServiceBundleItemID", sbIId, servicePrice.getServiceBundleItemID());
            Assert.assertEquals("Wrong Price", price, servicePrice.getPrice());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testInvalidSP() {
        try {
            ServicePrice servicePrice = (ServicePrice) getInstance();
            servicePrice.setPriceListID(plId);
            servicePrice.setServiceBundleItemID(sbIId);
            servicePrice.setPrice(new BigDecimal(5));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }


}
