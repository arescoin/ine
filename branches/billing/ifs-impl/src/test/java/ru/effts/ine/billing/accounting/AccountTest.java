package ru.effts.ine.billing.accounting;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.VersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: AccountTest.java 3837 2014-06-11 11:34:44Z DGomon $"
 */
public class AccountTest extends VersionableTest {

    public static final BigDecimal PL_ID = BigDecimal.TEN;
    public static final BigDecimal DISCOUNT = new BigDecimal(.8);

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return Account.class;
    }

    @Test
    public void testCommonValid() {
        try {
            Account account = (Account) getInstance();

            account.setPriceListId(PL_ID);
            account.setDiscount(DISCOUNT);

            Assert.assertEquals("Wrong PriceList", PL_ID, account.getPriceListId());
            Assert.assertEquals("Wrong Discount", DISCOUNT, account.getDiscount());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }


    @Test(expected = IllegalIdException.class)
    public void testNullPriceId() {
        try {
            Account account = (Account) getInstance();

            account.setPriceListId(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
