package ru.effts.ine.billing.accounting;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.VersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: CompanyDetailsTest.java 3837 2014-06-11 11:34:44Z DGomon $"
 */
public class CompanyDetailsTest extends VersionableTest {

    public static final BigDecimal ACC_ID = BigDecimal.ONE;
    public static final BigDecimal CMP_ID = BigDecimal.TEN;
    public static final String PHONE_N = "+7 (495) 641-12-12";
    public static final String LEGAL_ADDRESS = "LA Россия, 115054, Москва, ул. Дубининская, д.53, стр.5";
    public static final String P_ADDRESS = "Россия, 115054, Москва, ул. Дубининская, д.53, стр.5";

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return CompanyDetails.class;
    }

    @Test
    public void testCommonValid() {
        try {
            CompanyDetails details = (CompanyDetails) getInstance();

            details.setAccountId(ACC_ID);
            details.setCompanyId(CMP_ID);
            details.setPhoneNumber(PHONE_N);
            details.setLegalAddress(LEGAL_ADDRESS);
            details.setPhysicalAddress(P_ADDRESS);


            Assert.assertEquals("Wrong Acc Id", ACC_ID, details.getAccountId());
            Assert.assertEquals("Wrong Company Id", CMP_ID, details.getCompanyId());
            Assert.assertEquals("Wrong Company Id", CMP_ID, details.getCompanyId());
            Assert.assertEquals("Wrong Phone Number", PHONE_N, details.getPhoneNumber());
            Assert.assertEquals("Wrong Legal Address", LEGAL_ADDRESS, details.getLegalAddress());
            Assert.assertEquals("Wrong Physical Address", P_ADDRESS, details.getPhysicalAddress());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullAccount() {
        try {
            CompanyDetails details = (CompanyDetails) getInstance();

            details.setAccountId(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullCompany() {
        try {
            CompanyDetails details = (CompanyDetails) getInstance();

            details.setCompanyId(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
