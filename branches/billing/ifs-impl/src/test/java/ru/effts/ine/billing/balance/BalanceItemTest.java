package ru.effts.ine.billing.balance;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.*;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BalanceItemTest.java 3864 2014-10-13 09:16:38Z DGomon $"
 */
public class BalanceItemTest extends VersionableTest {

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return BalanceItem.class;
    }

    @Test(expected = IllegalIdException.class)
    public void testNullAccountId() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setAccountId(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroAccountId() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setAccountId(BigDecimal.ZERO);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegAccountId() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setAccountId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullBalance() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setBalance(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullAmount() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setAmount(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IneIllegalArgumentException.class)
    public void testNullDate() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setDate(null);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroReasonType() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setReasonType(BigDecimal.ZERO);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegReasonType() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setReasonType(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testZeroReasonId() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setReasonId(BigDecimal.ZERO);
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNegReasonId() {
        try {
            IdentifiableFactory.getImplementation(BalanceItem.class).setReasonId(new BigDecimal(-1));
        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test
    public void testSetFields() {

        try {
            BalanceItem obj = IdentifiableFactory.getImplementation(BalanceItem.class);
            obj.setAccountId(Identifiable.MIN_ALLOWABLE_VAL);
            obj.setAddition(true);
            obj.setAmount(new BigDecimal(5));
            obj.setBalance(new BigDecimal(7));
            obj.setReasonId(new BigDecimal(9));
            obj.setReasonType(new BigDecimal(17));

            Assert.assertEquals("Getter-Setter failed", Identifiable.MIN_ALLOWABLE_VAL, obj.getAccountId());
            Assert.assertEquals("Getter-Setter failed", new BigDecimal(5), obj.getAmount());
            Assert.assertEquals("Getter-Setter failed", new BigDecimal(7), obj.getBalance());
            Assert.assertEquals("Getter-Setter failed", new BigDecimal(9), obj.getReasonId());
            Assert.assertEquals("Getter-Setter failed", new BigDecimal(17), obj.getReasonType());
            Assert.assertTrue("Getter-Setter failed", obj.isAddition());

            obj.setAddition(false);
            Assert.assertFalse("Getter-Setter failed", obj.isAddition());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
