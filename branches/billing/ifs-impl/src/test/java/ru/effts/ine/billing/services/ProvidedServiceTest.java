package ru.effts.ine.billing.services;

import org.junit.Assert;
import org.junit.Test;
import ru.effts.ine.core.GenericSystemException;
import ru.effts.ine.core.Identifiable;
import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.VersionableTest;

import java.math.BigDecimal;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: ProvidedServiceTest.java 3866 2014-10-14 14:33:13Z DGomon $"
 */
public class ProvidedServiceTest extends VersionableTest {

    public static final String SRV_NAME = "Test Service Item";

    @Override
    protected Class<? extends Identifiable> getClassType() {
        return ProvidedService.class;
    }

    @Test
    public void testCommonValid() {
        try {
            ProvidedService service = (ProvidedService) getInstance();

            service.setServiceName(SRV_NAME);
            service.setAccountId(BigDecimal.ONE);
            service.setParentId(BigDecimal.TEN);
            service.setServiceBundleId(BigDecimal.ONE);

            Assert.assertEquals("Wrong Name", SRV_NAME, service.getServiceName());
            Assert.assertEquals("Wrong NLS ID", BigDecimal.ONE, service.getAccountId());
            Assert.assertEquals("Wrong Parent ID", BigDecimal.TEN, service.getParentId());
            Assert.assertEquals("Wrong Bundle ID", BigDecimal.ONE, service.getServiceBundleId());

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

    @Test(expected = IllegalIdException.class)
    public void testNullBundleId() {
        try {
            ProvidedService service = (ProvidedService) getInstance();

            service.setServiceBundleId(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }


    @Test(expected = IllegalIdException.class)
    public void testNullNLSId() {
        try {
            ProvidedService service = (ProvidedService) getInstance();

            service.setAccountId(null);

        } catch (GenericSystemException e) {
            Assert.fail(e.getMessage() + " ==> " + e.toString());
        }
    }

}
