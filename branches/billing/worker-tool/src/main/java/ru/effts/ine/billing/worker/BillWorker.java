package ru.effts.ine.billing.worker;

import ru.effts.ine.billing.accounting.Account;
import ru.effts.ine.billing.balance.BalanceItem;
import ru.effts.ine.billing.balance.BillingHistoryItem;
import ru.effts.ine.billing.balance.LastAccountBill;
import ru.effts.ine.billing.services.ProvidedService;
import ru.effts.ine.billing.services.ServiceBundleItem;
import ru.effts.ine.billing.services.ServicePrice;
import ru.effts.ine.core.*;
import ru.effts.ine.core.dic.DictionaryEntry;
import ru.effts.ine.core.userpermits.SystemUser;
import ru.effts.ine.dbe.da.billing.accounting.AccountAccess;
import ru.effts.ine.dbe.da.billing.balance.BalanceItemAccess;
import ru.effts.ine.dbe.da.billing.balance.BillingHistoryItemAccess;
import ru.effts.ine.dbe.da.billing.balance.LastAccountBillAccess;
import ru.effts.ine.dbe.da.billing.services.ProvidedServiceAccess;
import ru.effts.ine.dbe.da.billing.services.ServiceBundleItemAccess;
import ru.effts.ine.dbe.da.billing.services.ServicePriceAccess;
import ru.effts.ine.dbe.da.core.AccessFactory;
import ru.effts.ine.dbe.da.core.dic.DictionaryEntryAccess;
import ru.effts.ine.dbe.da.core.structure.FieldAccess;
import ru.effts.ine.dbe.da.core.userpermits.SystemUserAccess;
import ru.effts.ine.ejbs.DelegateFactory;
import ru.effts.ine.ejbs.billing.balance.BalanceItemDelegate;
import ru.effts.ine.ejbs.billing.balance.BillingHistoryItemDelegate;
import ru.effts.ine.ejbs.billing.balance.LastAccountBillDelegate;
import ru.effts.ine.ejbs.billing.services.ProvidedServiceDelegate;
import ru.effts.ine.utils.config.ConfigurationManager;
import ru.effts.ine.utils.searchCriteria.CriteriaRule;
import ru.effts.ine.utils.searchCriteria.SearchCriteriaProfile;
import ru.effts.ine.utils.searchCriteria.SearchCriterion;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillWorker.java 3885 2014-11-28 08:47:28Z DGomon $"
 */
public class BillWorker {

    private static final Logger logger = Logger.getLogger(BillWorker.class.getName());

    public static final BigDecimal PERIODIC_SRV_TYPE = new BigDecimal(2);

    public static final String USER_NAME_KEY = BillWorker.class.getName() + ".BW_USER_NAME_KEY";
    public static final String USER_PWD_KEY = BillWorker.class.getName() + ".BW_USER_PWD_KEY";

    // <typeId, <accId, acc>>
    private Map<BigDecimal, Collection<Account>> accountMap = new HashMap<>();

    // <bundleId, <priceListId, price>>
    private Map<BigDecimal, Map<BigDecimal, ServicePrice>> priceGrpMap;
    private Map<BigDecimal, ServiceBundleItem> bundleMap = new HashMap<>();
    private BigDecimal[] clientTypes;

    private int inProcess = 0;

    static {
        System.setProperty("ru.effts.ine.utils.cache.CacheAccessRiMSImpl.STANDALONE", "true");
    }

    public static void main(String[] args) {
        try {
            logger.log(Level.INFO, "Start bill worker...");
            new BillWorker().bill();
        } catch (CoreException e) {
            e.printStackTrace();
        }
    }

    public BillWorker() {

        try {
            logger.log(Level.INFO, "Initialise system user");

            String userName = ConfigurationManager.getManager().getRootConfig().getValueByKey(USER_NAME_KEY);
            String userPwd = ConfigurationManager.getManager().getRootConfig().getValueByKey(USER_PWD_KEY);

            if (null == userName || null == userPwd || userName.isEmpty() || userPwd.isEmpty()) {
                throw new IllegalStateException("Invalid User Params");
            }

            UserHolder.setUser(new BillingUser());

            UserHolder.getUser().setCurrentRole(BigDecimal.ONE);

            //noinspection unchecked
            SystemUserAccess<SystemUser> access =
                    (SystemUserAccess<SystemUser>) AccessFactory.getImplementation(SystemUser.class);
            logger.log(Level.INFO, "Initialise system user [" + userName + "]");
            SystemUser systemUser = access.login(userName, userPwd);
            UserHolder.setUser(systemUser);
            UserHolder.getUser().setCurrentRole(BigDecimal.ONE);
            UserHolder.getUser().setLastAction(new UserProfile.Action(null, "START BILLING"));
            UserHolder.getUser().setReason("START BILLING");
            logger.log(Level.INFO, "Initialisation system user [" + userName + "] success done");

        } catch (GenericSystemException e) {
            logger.log(Level.SEVERE, "ERROR: Initialisation error", e);
            throw new IllegalStateException("User not initialised", e);
        }
    }

    private void incProcess() {
        synchronized (PERIODIC_SRV_TYPE) {
            inProcess++;
        }
    }

    private void decProcess() {
        synchronized (PERIODIC_SRV_TYPE) {
            inProcess--;
        }
    }

    private void bill() throws CoreException {

        preparePriceMap();
        prepareClientTypes();
        groupAccountsByType();

        logger.log(Level.INFO, "Start client type iteration...");
        if (null != clientTypes && clientTypes.length > 0) {
            for (final BigDecimal clientType : clientTypes) {
                processClientType(clientType);
            }
        } else {
            logger.log(Level.SEVERE, "ERROR: the are no client types!!!");
        }
    }


    private void preparePriceMap() throws CoreException {

        logger.log(Level.INFO, "Price preparation...");

        //noinspection unchecked
        ServicePriceAccess<ServicePrice> priceAccess =
                (ServicePriceAccess<ServicePrice>) AccessFactory.getImplementation(ServicePrice.class);

        Collection<ServicePrice> prices = priceAccess.getAllObjects();
        priceGrpMap = new HashMap<>(prices.size(), 1);
        for (ServicePrice price : prices) {
            if (!priceGrpMap.containsKey(price.getPriceListID())) {
                HashMap<BigDecimal, ServicePrice> priceMap = new HashMap<>();
                priceGrpMap.put(price.getPriceListID(), priceMap);
            }


            priceGrpMap.get(price.getPriceListID()).put(price.getServiceBundleItemID(), price);
        }

        {   // logging block
            logger.log(Level.INFO, "Price Map prepared. PriceList types: " + priceGrpMap.size());

            for (Map.Entry<BigDecimal, Map<BigDecimal, ServicePrice>> bigDecimalMapEntry : priceGrpMap.entrySet()) {
                logger.log(Level.INFO, "   Price ID : " + bigDecimalMapEntry.getKey());
                Map<BigDecimal, ServicePrice> priceMap = bigDecimalMapEntry.getValue();

                for (ServicePrice servicePrice : priceMap.values()) {
                    logger.log(Level.INFO, servicePrice.toString());
                }
            }
        }


        logger.log(Level.INFO, "ServiceBundle preparation...");

        //noinspection unchecked
        ServiceBundleItemAccess<ServiceBundleItem> itemAccess =
                (ServiceBundleItemAccess<ServiceBundleItem>) AccessFactory.getImplementation(ServiceBundleItem.class);
        Collection<ServiceBundleItem> serviceBundleItems = itemAccess.getAllObjects();
        for (ServiceBundleItem serviceBundleItem : serviceBundleItems) {
            bundleMap.put(serviceBundleItem.getCoreId(), serviceBundleItem);
        }
        logger.log(Level.INFO, "Using bundles: " + bundleMap);

    }


    private void prepareClientTypes() throws GenericSystemException {

        logger.log(Level.INFO, "Client types preparation...");

        //noinspection unchecked
        DictionaryEntryAccess<DictionaryEntry> entryAccess =
                (DictionaryEntryAccess<DictionaryEntry>) AccessFactory.getImplementation(DictionaryEntry.class);

        Collection<DictionaryEntry> entries = entryAccess.getAllEntriesForDictionary(new BigDecimal(44));

        clientTypes = new BigDecimal[entries.size()];
        int i = 0;
        for (DictionaryEntry entry : entries) {
            logger.log(Level.INFO, entry.toString());
            clientTypes[i] = entry.getCoreId();
            ++i;
        }

        logger.log(Level.INFO, "final size: " + clientTypes.length);
    }

    private void groupAccountsByType() throws GenericSystemException {

        logger.log(Level.INFO, "Group accounts by type...");

        //noinspection unchecked
        AccountAccess<Account> accountAccess =
                (AccountAccess<Account>) AccessFactory.getImplementation(Account.class);
        SearchCriteriaProfile profile = accountAccess.getSearchCriteriaProfile();
        Set<SearchCriterion> criteria = new HashSet<>();

        for (BigDecimal clientType : clientTypes) {
            criteria.clear();
            criteria.add(profile.getSearchCriterion(Account.CL_TYPE, CriteriaRule.equals, clientType));
            Collection<Account> accounts = accountAccess.searchObjects(criteria);
            if (null != accounts && !accounts.isEmpty()) {
                accountMap.put(clientType, accounts);
            }
        }

        for (Map.Entry<BigDecimal, Collection<Account>> bigDecimalCollectionEntry : accountMap.entrySet()) {
            logger.log(Level.INFO, bigDecimalCollectionEntry.toString());
        }
    }


    private void processClientType(BigDecimal clientType) throws CoreException {

        logger.log(Level.INFO, "Process clients for type: " + clientType);

        Collection<Account> accounts = accountMap.get(clientType);

        if (null != accounts) {

            ThreadGroup threadGroup = new ThreadGroup(Thread.currentThread().getThreadGroup(), "AccountBill");

            logger.log(Level.INFO, "Accounts for type: " + accounts.size());

            final UserProfile userProfile = UserHolder.getUser();

            for (final Account account : accounts) {
                logger.log(Level.INFO, "    >>>>   Start bill for account [" + account + "]");

                while (inProcess > 2) {
                    try {
                        Thread.sleep(100l);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                incProcess();
                Thread thread = new Thread(threadGroup, new Runnable() {
                    @Override
                    public void run() {
                        try {
                            UserHolder.setUserProfile(userProfile);
                            billAccount(account);
                        } catch (CoreException e) {
                            logger.log(Level.ALL, "ERROR: account[" + account.getCoreId() + "] bill corrupted! ", e);
                            e.printStackTrace();
                        } finally {
                            logger.log(Level.INFO, "Try next account");
                            decProcess();
                            // next...
                        }
                    }
                },
                        "BILLACC [" + account.getCoreId() + "]");
                thread.start();
            }

        } else {
            logger.log(Level.WARNING, " No one Accounts for type: " + clientType);
        }
    }

    private void billAccount(Account account) throws CoreException {

        Date billingDate = FieldAccess.getCurrentDate();
        logger.log(Level.INFO, "Using billing date [" + billingDate + "]");


        //noinspection unchecked
        LastAccountBillAccess<LastAccountBill> lastAccountBillAccess =
                (LastAccountBillAccess) AccessFactory.getImplementation(LastAccountBill.class);
        LastAccountBill lastAccountBill = lastAccountBillAccess.getObjectById(account.getCoreId());

        int daysOut = -1;
        if (lastAccountBill != null) {
            logger.log(Level.INFO, lastAccountBill.toString());
            daysOut = getDayDiff(billingDate, lastAccountBill.getBillDate());
        }

        if (daysOut == 0) {
            logger.log(Level.INFO, "Account actually billed at [" + lastAccountBill + "]");
            return;
        }

        if (daysOut != 1) {
            logger.log(Level.INFO, "Problem Account detected [" + account.getCoreId() + "]");

            if (daysOut == -1) {

                logger.log(Level.INFO, "No one bill for account [" + account.getCoreId() + "]");
                daysOut = 10;
                logger.log(Level.INFO, "Set days out  [" + daysOut + "]");

            } else {
                logger.log(Level.INFO, "Days out  [" + daysOut + "]");
            }

            billProblemAcc(account, daysOut, billingDate);
        }

        logger.log(Level.INFO, "Bill actual services for account [" + account.getCoreId() + "]");

        Collection<ProvidedService> providedServices = getProvidedServices(account);
        try {

            if (!providedServices.isEmpty()) {

                logger.log(Level.WARNING, "Number Of Actual services [" + providedServices.size() + "]");

                for (ProvidedService providedService : providedServices) {
                    billService(account, billingDate, providedService);
                }
            } else {
                logger.log(Level.WARNING, "No one service for account [" + account.getCoreId() + "]");
            }

            createAccountBill(account, billingDate);

        } catch (IllegalAccessException | ClassNotFoundException | InstantiationException e) {
            throw GenericSystemException.toGeneric(e);
        }

    }


    private void billService(Account account, Date billingDate, ProvidedService providedService)
            throws GenericSystemException {

        try {
            checkProvidedService(providedService);

            logger.log(Level.INFO, "Bill actual service [" + providedService + "]");

            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(billingDate);

            //noinspection MagicConstant
            calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), 0, 0, 0);
            Date from = calendar.getTime();
            calendar.add(Calendar.DAY_OF_MONTH, 1);
            Date to = calendar.getTime();

            BigDecimal serviceBundleId = providedService.getServiceBundleId();

            //noinspection unchecked
            BillingHistoryItemAccess<BillingHistoryItem> access =
                    (BillingHistoryItemAccess<BillingHistoryItem>) AccessFactory.getImplementation(BillingHistoryItem.class);
            Collection<BillingHistoryItem> items = access.getHistoryForSrvByAccId(
                    account.getCoreId(), providedService.getCoreId(), from, to);

            if (!items.isEmpty()) {

                if (items.size() == 1) {
                    logger.log(Level.WARNING, "This service [" + providedService.getCoreId() + "] already billed");
                } else {
                    logger.log(Level.ALL, "WARNING!!!!! Multiply bill for [" + providedService + "]");
                }

                return;
            }

            BigDecimal plId = account.getPriceListId();

            ServicePrice servicePrice = priceGrpMap.get(plId).get(serviceBundleId);

            logger.log(Level.INFO, "Current Service price [" + servicePrice + "]");

            BigDecimal dayPrice = servicePrice.getDayPrice();
            BigDecimal discount = account.getDiscount();
            BigDecimal srvPrDisc = dayPrice.multiply(discount).setScale(2, BigDecimal.ROUND_UP);

            logger.log(Level.WARNING, "Day price [{0}], Discount [{1}], Final service price [{2}]",
                    new Object[]{dayPrice, discount, srvPrDisc});

            updateBillingInfo(account, providedService, srvPrDisc, billingDate);

        } catch (CoreException e) {
            e.printStackTrace();
        }
    }

    private void billProblemAcc(Account account, int days, Date billingDate)
            throws CoreException {

        logger.log(Level.WARNING, "Start bills for old services [" + account.getCoreId() + "]");

        for (int i = days - 1; i > 0; --i) {

            logger.log(Level.WARNING, "Bill out day [" + i + "]");

            Calendar calendar = GregorianCalendar.getInstance();
            calendar.setTime(billingDate);
            calendar.add(Calendar.DAY_OF_MONTH, -i);

            Date retroDate = calendar.getTime();

            //noinspection unchecked
            ProvidedServiceAccess<ProvidedService> providedSrvAccess = (ProvidedServiceAccess<ProvidedService>)
                    AccessFactory.getImplementation(ProvidedService.class);
            Collection<ProvidedService> providedServices =
                    providedSrvAccess.getHistoryServicesForAccAtDate(account.getCoreId(), retroDate);

            logger.log(Level.WARNING, "Number of Closed ProvidedService ["
                    + providedServices.size() + "], at Date [" + retroDate + "]");

            billProblemSrv(account, retroDate, providedServices);

            SearchCriteriaProfile profile = providedSrvAccess.getSearchCriteriaProfile();

            Set<SearchCriterion> criteria = new HashSet<>();
            criteria.add(
                    profile.getSearchCriterion(ProvidedService.ACCOUNT_ID, CriteriaRule.equals, account.getCoreId()));
            criteria.add(profile.getSearchCriterion(ProvidedService.STATUS, CriteriaRule.equals, BigDecimal.ONE));
            criteria.add(profile.getSearchCriterion(ProvidedService.CORE_FD, CriteriaRule.less, billingDate));
            criteria.add(profile.getSearchCriterion(ProvidedService.CORE_TD, CriteriaRule.greater, billingDate));


            providedServices = providedSrvAccess.searchObjects(criteria);

            billProblemSrv(account, retroDate, providedServices);

            try {
                createAccountBill(account, retroDate);
            } catch (IllegalAccessException | ClassNotFoundException | InstantiationException e) {
                logger.log(Level.WARNING, "ERROR! Account bill not saved! Account ["
                        + account.getCoreId() + "] Date [" + retroDate + "]");
            }
        }

        logger.log(Level.WARNING, "Old services bills done [" + account.getCoreId() + "]");
    }

    private void billProblemSrv(Account account, Date retroDate, Collection<ProvidedService> providedServices) throws GenericSystemException {
        for (ProvidedService providedService : providedServices) {

            logger.log(Level.WARNING, "Bill [" + providedService + "]");

            ServiceBundleItem serviceBundleItem = bundleMap.get(providedService.getServiceBundleId());
            if (null != serviceBundleItem) {
                BigDecimal type = serviceBundleItem.getType();
                if (null != type) {
                    if (type.equals(PERIODIC_SRV_TYPE)) {
                        logger.log(Level.WARNING, "Bill Old Service [" + providedService.getCoreId() + "]");
                        billService(account, retroDate, providedService);
                    } else {
                        logger.log(Level.WARNING, "Service is once-type.");
                    }
                } else {
                    logger.log(Level.WARNING, "ERROR! NULL ServiceBundleItem.TYPE [" + serviceBundleItem + "]");
                }
            } else {
                logger.log(Level.WARNING,
                        "ERROR! No one ServiceBundleItem ID [" + providedService.getServiceBundleId() + "]");
            }
            logger.log(Level.WARNING, "End Of Service Bill [" + providedService + "]");
        }
    }

    int getDayDiff(Date date1, Date date2) {

        Calendar calendar1 = GregorianCalendar.getInstance();
        Calendar calendar2 = GregorianCalendar.getInstance();
        calendar1.setTime(date1);
        calendar2.setTime(date2);

        return calendar1.get(Calendar.DAY_OF_YEAR) - calendar2.get(Calendar.DAY_OF_YEAR);
    }

    private void updateBillingInfo(Account account, ProvidedService providedService, BigDecimal amount, Date itemDate)
            throws GenericSystemException {

        BalanceItem item = getBalance(account);
        BigDecimal actualBall = item.getBalance();

        logger.info("Actual balance [" + actualBall + "]");

        try {
            if (actualBall.compareTo(amount) >= 1) {

                BigDecimal newBall = actualBall.subtract(amount);

                logger.info("New balance [" + newBall + "]");

                item.setAmount(amount);
                item.setAddition(false);
                item.setBalance(newBall);
                item.setDate(itemDate);
                item.setReasonType(FieldAccess.getTableId(ProvidedService.class));
                item.setReasonId(providedService.getCoreId());
                item.setCoreDsc("Common billing [" + itemDate + "]");

                BalanceItemDelegate<BalanceItem> balanceItemDelegate =
                        DelegateFactory.obtainDelegateByInterface(BalanceItem.class);
                UserHolder.getUser().setReason("Billing update");
                BalanceItem newBalance = balanceItemDelegate.updateObject(UserHolder.getUser(), item);

                BillingHistoryItem billingHistoryItem = IdentifiableFactory.getImplementation(BillingHistoryItem.class);
                billingHistoryItem.setBillingDate(itemDate);
                billingHistoryItem.setAccountId(account.getCoreId());
                billingHistoryItem.setServiceId(providedService.getCoreId());
                billingHistoryItem.setCoreDsc("New service instance create by ["
                        + UserHolder.getUser().getSystemUser().getLogin() + " id:[ "
                        + UserHolder.getUser().getSystemUser().getCoreId() + "], "
                        + " new balance item id [" + newBalance.getBalance() + "], "
                        + " balance date [" + itemDate + "] ]");

                BillingHistoryItemDelegate<BillingHistoryItem> historyItemDelegate =
                        DelegateFactory.obtainDelegateByInterface(BillingHistoryItem.class);
                UserHolder.getUser().setReason("Service bill created");
                historyItemDelegate.createObject(UserHolder.getUser(), billingHistoryItem);

            } else {
                logger.log(Level.WARNING, "Account balance deficiency");

                providedService.setStatus(new BigDecimal(2));
                ProvidedServiceDelegate<ProvidedService> providedServiceDelegate =
                        DelegateFactory.obtainDelegateByInterface(ProvidedService.class);
                UserHolder.getUser().setReason("Service blocked by billing");
                providedServiceDelegate.updateObject(UserHolder.getUser(), providedService);

                logger.log(Level.WARNING, "Service blocked [" + providedService.getCoreId() + "]");
            }
        } catch (IllegalAccessException | InstantiationException | ClassNotFoundException e) {
            throw GenericSystemException.toGeneric(e);
        }

    }

    private void createAccountBill(Account account, Date itemDate)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException, GenericSystemException {
        LastAccountBillDelegate<LastAccountBill> accountBillDelegate =
                DelegateFactory.obtainDelegateByInterface(LastAccountBill.class);
        LastAccountBill accountBill =
                accountBillDelegate.getObjectById(UserHolder.getUser(), account.getCoreId());
        if (null == accountBill) {
            accountBill = IdentifiableFactory.getImplementation(LastAccountBill.class);
            accountBill.setCoreId(account.getCoreId());
            accountBill.setCoreFd(itemDate);
            accountBill.setCoreTd(FieldAccess.getSystemTd());
            accountBill.setBillDate(itemDate);

            UserHolder.getUser().setReason("Account bill created");
            accountBill = accountBillDelegate.createObject(UserHolder.getUser(), accountBill);
        } else {
            accountBill.setBillDate(itemDate);
            UserHolder.getUser().setReason("Account bill created");
            accountBill = accountBillDelegate.updateObject(UserHolder.getUser(), accountBill);
        }

        logger.log(Level.WARNING, "Account Bill saved [" + accountBill + "]");
    }

    private BalanceItem getBalance(Account account) throws GenericSystemException {

        //noinspection unchecked
        BalanceItemAccess<BalanceItem> balanceItemAccess =
                (BalanceItemAccess<BalanceItem>) AccessFactory.getImplementation(BalanceItem.class);
        SearchCriteriaProfile profile = balanceItemAccess.getSearchCriteriaProfile();

        Set<SearchCriterion> criteria = new HashSet<>();
        criteria.add(profile.getSearchCriterion(BalanceItem.ACCOUNT_ID, CriteriaRule.equals, account.getCoreId()));
        Collection<BalanceItem> items = balanceItemAccess.searchObjects(criteria);

        if (items.size() == 1) {
            return items.iterator().next();
        } else if (items.isEmpty()) {
            return null;
        }

        logger.log(Level.ALL, "Too many actual balance records. " + account);
        throw GenericSystemException.toGeneric(new IneCorruptedVersionableDateException(items.toString()));
    }

    private Collection<ProvidedService> getProvidedServices(Account account) throws GenericSystemException {
        //noinspection unchecked
        ProvidedServiceAccess<ProvidedService> providedSrvAccess = (ProvidedServiceAccess<ProvidedService>)
                AccessFactory.getImplementation(ProvidedService.class);
        Set<SearchCriterion> criteria = new HashSet<>();
        SearchCriteriaProfile criteriaProfile = providedSrvAccess.getSearchCriteriaProfile();

        criteria.add(criteriaProfile
                .getSearchCriterion(ProvidedService.ACCOUNT_ID, CriteriaRule.equals, account.getCoreId())
        );
        criteria.add(criteriaProfile
                .getSearchCriterion(ProvidedService.STATUS, CriteriaRule.equals, BigDecimal.ONE)
        );

        return providedSrvAccess.searchObjects(criteria);
    }

    /** первичная проверка на корректность переданного сервиса */
    private void checkProvidedService(ProvidedService providedService) throws GenericSystemException {

        if (null == providedService) {
            throw new IneIllegalArgumentException("Null SERVICE provided");

        }

        if (null == providedService.getServiceBundleId()) {
            throw new IneIllegalArgumentException("Null SERVICE." + ProvidedService.SERVICE_BUNDLE_ID);
        }
    }


}
