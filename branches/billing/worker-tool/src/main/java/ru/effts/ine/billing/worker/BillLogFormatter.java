package ru.effts.ine.billing.worker;

import java.text.MessageFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillLogFormatter.java 3886 2014-11-28 08:49:52Z DGomon $"
 */
public class BillLogFormatter extends Formatter {

    private static final String STRING_SEPARATOR = System.getProperty("line.separator");

    private static final MessageFormat messageFormat =
            new MessageFormat("[{0,date,YYYY.MM.dd HH:mm:ss}] {1} {2} [{3} , {4}] {5} " + STRING_SEPARATOR);

    public BillLogFormatter() {
        super();
    }

    @Override
    public String format(LogRecord record) {
        Object[] arguments = new Object[6];
        arguments[0] = new Date(record.getMillis());
        arguments[1] = record.getLevel();
        arguments[2] = record.getSourceClassName();
        arguments[3] = Thread.currentThread().getId();
        arguments[4] = Thread.currentThread().getName();
        arguments[5] = record.getMessage();
        record.setMessage(messageFormat.format(arguments));
        return formatMessage(record);
    }
}
