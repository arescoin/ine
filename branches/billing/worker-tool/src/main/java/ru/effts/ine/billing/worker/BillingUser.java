package ru.effts.ine.billing.worker;

import ru.effts.ine.core.IllegalIdException;
import ru.effts.ine.core.IneIllegalArgumentException;
import ru.effts.ine.core.userpermits.SystemUser;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Denis Gomon
 * @SVNVersion "$Id: BillingUser.java 3876 2014-11-10 15:08:59Z DGomon $"
 */
public class BillingUser implements SystemUser {
    @Override
            public Object clone() throws CloneNotSupportedException {
                return super.clone();
            }

            @Override
            public boolean isActive() {
                return true;
            }

            @Override
            public void setActive(boolean active) {
            }

            @Override
            public void setCoreFd(Date fd) throws IneIllegalArgumentException {
            }

            @Override
            public Date getCoreFd() {
                return new Date();
            }

            @Override
            public void setCoreTd(Date td) throws IneIllegalArgumentException {
            }

            @Override
            public Date getCoreTd() {
                return new Date();
            }

            @Override
            public void setCoreId(BigDecimal id) throws IllegalIdException {
            }

            @Override
            public BigDecimal getCoreId() {
                return BigDecimal.ONE;
            }

            @Override
            public String getCoreDsc() {
                return "Billing";
            }

            @Override
            public void setCoreDsc(String dsc) {
            }

            @Override
            public String getLogin() {
                return "root";
            }

            @Override
            public void setLogin(String login) {
            }

            @Override
            public String getMd5() {
                return null;
            }

            @Override
            public void setMd5(String md5) {
            }
}
